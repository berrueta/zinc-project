contiene :: Eq a => [a] -> a -> Bool
contiene []     _ = False
contiene (x:xs) y = (x == y) || contiene xs y
