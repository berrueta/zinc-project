class Functor m where
  fmap :: (a -> b) -> m a -> m b

instance Functor [] where
  fmap = map

instance Functor Maybe where
  fmap f Nothing  = Nothing
  fmap f (Just x) = Just (f x)

ofuscar :: Functor f => f Char -> f Char
ofuscar = fmap (chr . (+1) . ord)
