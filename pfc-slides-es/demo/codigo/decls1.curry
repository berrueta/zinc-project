module M where

class MiEq a where
  miIgualdad :: a -> a -> Bool

class MiEq a => MiOrd a where
  miMenor :: a -> a -> Bool

instance MiEq Int where
  miIgualdad x y = False

instance MiOrd Int where
  miMenor x y = False
