header {
    package org.asturlinux.frade.currin;
    import java.util.Vector;
    import java.io.FileInputStream;
    import java.io.DataInputStream;
    import java.io.FileNotFoundException;

    import org.asturlinux.frade.currin.program.Checker;
    import org.asturlinux.frade.currin.program.Program;
    import org.asturlinux.frade.currin.program.Module;
    import org.asturlinux.frade.currin.program.Function;
    import org.asturlinux.frade.currin.program.InstrBlock;
    import org.asturlinux.frade.currin.program.instruction.*;
    import org.asturlinux.frade.currin.program.type.*;
    import org.asturlinux.frade.currin.program.type.CurrinExpr;
    import org.asturlinux.frade.currin.exceptions.*;
    
    import org.apache.log4j.Logger;
}

class L extends Lexer;

options {
    charVocabulary = '\0'..'\377' ;
    testLiterals = false;
    k = 2;
}

COMA : ',';
PTO_COMA : ';';
CORCH_ABRIR : '[' ;
CORCH_CERRAR : ']';
LLAVE_ABRIR : '{';
LLAVE_CERRAR : '}';
PUNTO : '.';
DOS_PUNTOS : ':';
IGUAL : '=';
PIPE : '|';
FLECHA_IZQ : "<-";

// Whitespace -- ignored
WS
  : ( ' '
    | '\t'
    | '\f'
    
    // handle newlines
    | ( "\r\n"  // Evil DOS
      | '\r'    // Macintosh
      | '\n'    // Unix (the right way)
      )
      { newline(); }

    )
    { $setType(Token.SKIP); }
  ;



IDENT
options {testLiterals = true;}
    : ('a'..'z'|'A'..'Z'|'_')('a'..'z'|'A'..'Z'|'0'..'9'|'_'|'/')+
    ;


INT
    : ('0'..'9')+
   ;


class P extends Parser;

options { k = 3;}

{
}

startRule 
returns [Program p]
 { p = new Program();
 }
    : program[p,"main"]
    ;

program[Program p, String module]
{ Function f = null; }
: (import_[p])* { p.addModule(new Module(module)); }
                (  f = function 
                { p.getCurrentModule().addFunction(f); } 
                | global )+
    ;

import_[Program p]
: PUNTO "import" id:IDENT PTO_COMA
        {
            String file = new String(id.getText() + ".cam");
            L lexer = new L(new DataInputStream( new FileInputStream(file)));
            P parser = new P(lexer);
            parser.program(p, id.getText());
        }
    ;
exception 
catch [FileNotFoundException fnf] 
    { 
        Logger logger = Logger.getLogger("Main");
        logger.error("File " + id.getText() + ".cam not found");
    }


function
returns [Function f]
{f = null; 
 Instruction i;}
: 
        f = function_def 
        LLAVE_ABRIR
        ( i = instruccion { f.addInstruction(i); } )+ 
        LLAVE_CERRAR 
        (PTO_COMA)?
     ;

function_def
returns [Function f]
{ f = null;
  Vector parameters; }
    :
        PUNTO "function" 
        name:IDENT 
        parameters = params { f = new Function(name.getText(), parameters); }
    ;

global : PUNTO "data" IDENT (IGUAL IDENT (PIPE IDENT)*)? 
        PTO_COMA
    ;

instruccion
returns [Instruction i]
{ i = null; 
 Instruction let, returni, execi, enteri, assigni, switchi, calli; }
    : let = let_ { i = let; }
    | returni = return_ { i = returni; }
    | execi = exec_ { i = execi; }
    | enteri = enter_ { i = enteri; }
    | assigni = asignacion_ { i = assigni; }
    | switchi = switch_  { i = switchi; }
    | lock_
    | update_
    | choices_
    ;


let_
returns [LetInstruction li]
{ li = null; 
  CurrinExpr ct, cs;
  Function fd;
}
: PUNTO "let" 
        LLAVE_ABRIR 
        where:IDENT 
        IGUAL ( ct = def { 
                    try {
                        li = new LetInstruction(where.getText(), ct);
                    } catch (NumberFormatException n) {
                        System.out.println("Error en la posicion de memoria");
                    }
                }
                | fd = function_def {
                    CurrinFunction cf = new CurrinFunction(fd);
                    li = new LetInstruction(where.getText(), cf);
                } 
                ) 
        LLAVE_CERRAR 
        PTO_COMA
    ;

return_ 
returns [ReturnInstruction ri]
{ ri = null; }
: PUNTO "return" id:IDENT 
        {
          ri = new ReturnInstruction(id.getText());
        }
    ;

exec_ 
returns [ExecInstruction ei]
{ ei = null; 
    Vector parameters; }
: PUNTO "exec" id:IDENT parameters = params
        {
            ei = new ExecInstruction(id.getText(), parameters);
        }
    ;

enter_ 
returns [EnterInstruction ei]
{ ei = null; }
    : PUNTO "enter" id:IDENT
        {
            ei = new EnterInstruction(id.getText());
        }
    ;


asignacion_
returns [AssignInstruction ai]
{ai = null; 
 InstrBlock ib = new InstrBlock();
 Instruction ienter,iexec,ii;
 }
 : id:IDENT FLECHA_IZQ 
   (ienter = enter_ { ib.addInstruction(ienter); }
        | iexec = exec_  { ib.addInstruction(iexec); }
        | LLAVE_ABRIR 
        (ii = instruccion { ib.addInstruction(ii); })+ 
        LLAVE_CERRAR) PTO_COMA
        { ai = new AssignInstruction(id.getText(), ib);}
    ;


def 
returns [CurrinExpr readed]
{ readed = null; 
  Vector parameters; }
    : PUNTO "data" command:IDENT parameters = params
    {
         readed = new CurrinConstr(command.getText(), parameters);
    }
    | PUNTO "char" i:INT 
        { readed = new CurrinChar(Integer.parseInt(i.getText()));
        }
    | PUNTO "int" j:INT
        { readed = new CurrinInt(Integer.parseInt(j.getText()));
        }
    | PUNTO "float" k1:INT PUNTO k2:INT
        { readed = new CurrinFloat(Float.parseFloat(k1.getText() + "." 
                                                    + k2.getText()));
        }
    | PUNTO "suspend" id:IDENT
        { readed = new CurrinSuspend(id.getText());
        }
    ;


switch_ 
returns [SwitchInstruction si]
{ si = null;
  int switch_type;
  Vector cases = new Vector();
  SwitchCase sc, sc_default = null;
}
    :
       PUNTO "switch" selector:IDENT PUNTO 
        ("flex" { switch_type = SwitchInstruction.FLEX; }
        |"rigid"{ switch_type = SwitchInstruction.RIGID; }) 
        LLAVE_ABRIR
        sc = switch_case { cases.add(sc); }
        (PIPE sc = switch_case { cases.add(sc); })* 
        (PIPE sc_default = switch_default)?
        LLAVE_CERRAR
        { 
            si = new SwitchInstruction(switch_type, selector.getText(), 
                                       cases, sc_default);
            
        }
    ;

switch_case
returns[SwitchCase sc] 
{sc = null;
 InstrBlock instr = new InstrBlock();
 Vector p; 
 Instruction i;
 CurrinExpr ce = null;}
    :       ce = def DOS_PUNTOS 
            LLAVE_ABRIR
            (i = instruccion { instr.addInstruction(i); })+
            LLAVE_CERRAR
        {
            CurrinRef condition = new CurrinRef(ce);
            sc = new SwitchCase(condition, instr);
        }
    ;

switch_default
returns[SwitchCase sc]
{ sc = null;
  InstrBlock instr = new InstrBlock();
  Instruction i;
}
 : PUNTO "default" DOS_PUNTOS
        LLAVE_ABRIR
        (i = instruccion { instr.addInstruction(i); })+
        LLAVE_CERRAR
        {
            sc = new SwitchCase(null, instr);
        }
    ;

lock_ : PUNTO "lock" IDENT PTO_COMA
    ;

update_ : PUNTO "update" IDENT IDENT PTO_COMA
    ;

choices_ : PUNTO "choices" 
        LLAVE_ABRIR 
        choices_statement (PIPE choices_statement)*
        LLAVE_CERRAR
    ;

choices_statement : LLAVE_ABRIR (instruccion)+ LLAVE_CERRAR
    ;

params
returns [Vector v]
{ v = new Vector(); }
    : CORCH_ABRIR 
      ( parameter1:IDENT { v.add(parameter1.getText()); }  
            (COMA parametern:IDENT { v.add(parametern.getText()); } )*)? 
      CORCH_CERRAR
    ;
