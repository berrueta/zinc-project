import java.io.*;

public class Zinc {

    private static final int lines_to_ignore_in_cam_file = 2;

    String _cyc_ = "cyc";
    String _filename = null;

    public void obtainCamCode( String curry_file ) {

	try {
	    
	    String line;
	    boolean error = false;

	    int termination = curry_file.indexOf(".curry");

	    if ( termination == -1 ) {
		System.out.println("File termination must be .curry");
		return;
	    }
	    
	    String filename = curry_file.substring(0, termination);

	    /*
	     * Checkin if recompilation is necesary
	     *
	     */
	    File destiny = new File(filename + ".cam");
	    File source = new File(curry_file);

	    if ( destiny.exists() &&
		 source.lastModified() <= destiny.lastModified() ) {
		return;
	    }

	    FileWriter output = new FileWriter(new String(filename + ".cam"));

	    String command = new String( _cyc_ 
					 + " --dump-cam -C " 
					 + curry_file);
	    Process p = Runtime.getRuntime().exec(command);

	    BufferedReader cam = new BufferedReader
		(new InputStreamReader(p.getInputStream()));
	    

	    BufferedReader error_out = new BufferedReader
		(new InputStreamReader(p.getErrorStream()));

	    while ((line = error_out.readLine()) != null) {
		System.out.println(line);
		error = true;
	    }
	    error_out.close();

	    if ( error ) {
		System.out.println("Some errors ocurred compiling zinc file. " 
				   + "See messages above for more information");
		return;
	    }

	    for ( int i = 0; i < lines_to_ignore_in_cam_file; i++ )
		cam.readLine();

	    while ((line = cam.readLine()) != null) {
		output.write(line+"\n");
	    }
	    cam.close();
	    output.close();

	} catch ( Exception err ) {
	    err.printStackTrace();
	}
    }

    private  void parseArgs(String arg[])
	throws StreamCorruptedException {

	for ( int i = 0; i < arg.length; i++ ) {
	    String argument = arg[i];

	    /* 
	     *  Option --use-cyc=
	     */
	    if ( argument.startsWith("--use-cyc=")) {
		if ( argument.endsWith("/cyc") ) 
		    _cyc_ = new String(argument.substring(10));
		else
		    throw new StreamCorruptedException("Include cyc executable name in --use-cyc param, for example /usr/bin/cyc");

		File f = new File(_cyc_);
		if ( !f.exists()) 
		    throw new StreamCorruptedException("Not exist " 
						       + _cyc_);
		continue;
	    }

	    /*
	     * filename
	     */
	    if ( argument.endsWith(".curry") ) {
		_filename = new String(argument);
		continue;
	    }

	    /*
	     * Unreconigzed option
	     */
	    throw new StreamCorruptedException("Unknow option: " + argument);

	}

	if ( _filename == null )
	    throw new StreamCorruptedException("Required a .curry file");

    }

    public void run(String argv[]) {
	
	try {
	    parseArgs(argv);
	    obtainCamCode( _filename );

	} catch (ArrayIndexOutOfBoundsException out) {
	    System.err.println("Required a .curry file");
	} catch ( StreamCorruptedException sce) {
	    System.err.println(sce.getMessage());
	    printUsage();
	}
	
    }

    void printUsage() {
	System.err.println("Usage: java Zinc [--use-cyc={path/cyc}] filename");
    }


    public static void main(String argv[]) {
	Zinc app = new Zinc();
	app.run(argv);
    }
}