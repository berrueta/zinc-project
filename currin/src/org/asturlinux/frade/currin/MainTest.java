package org.asturlinux.frade.currin;

import junit.framework.TestCase;
import java.lang.Float;
//import org.asturlinux.frade.currin.program.Program;
//import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.apache.log4j.Logger;

public class MainTest extends TestCase {

    AstLogger al = new AstLogger();
    RuntimeLogger rl = new RuntimeLogger();
    
    
    public void testFunctionCallingWithoutParameters() {
	String file = "prog-test/extra.cam";
	Main m = new Main();
	assertEquals("3 nested function calls",
		     "Aqui una megaparrafada",
		     m.testingProgram(file, "main__main"));
    }
    
    public void testOnlyInteger() {
	String file = "prog-test/cuatro.cam";
	Main m = new Main();
	assertEquals("Printing integer",
		     "4 ",
		     m.testingProgram(file, "main__cuatro"));
    }

    public void testNegativeInteger() {
	String file = "prog-test/int-lt0.cam";
	Main m = new Main();
	assertEquals("Printing integer less than 0",
		     "-1 ",
		     m.testingProgram(file, "main__main"));
    }
    
    public void testOnlyCharChain() {
	String file = "prog-test/holamundo.cam";
	Main m = new Main();
	assertEquals("Printing char chain",
		     "AB",
		     m.testingProgram(file, "main__hola"));
    }
    
    public void testFunctionInvocation() {
    	String file = "prog-test/functInvocation.cam";
    	Main m = new Main();
	assertEquals("Invocating function without params",
		     "hola",
		     m.testingProgram(file, "main__main"));
    }
   
    public void testPassingUnusedParameter() {
	String file = "prog-test/unusedparameter.cam";
	Main m = new Main();
	assertEquals("Passing param (but not user inside function)",
		     "hola",
		     m.testingProgram(file, "main__main"));
    }
    
    
    public void testPassingUsedParameter() {
	String file = "prog-test/usedparameter.cam";
	Main m = new Main();
	assertEquals("Passing and using parameters: ",
		     "amigo",
		     m.testingProgram(file, "main__main"));
    }
    
    public void testSwitch() {
	String file = "prog-test/switch.cam";
	Main m = new Main();
	assertEquals("Switch in function empty list",
		     "0 ",
		     m.testingProgram(file, "main__main"));
    }
    
    public void testSwitchList() {
	String file = "prog-test/switch4.cam";
	Main m = new Main();
	assertEquals("Switch by integer and recursive call: ",
		     "uno",
		     m.testingProgram(file, "main__main"));
	
    }
    
    public void testSwitchMatchingList() {
	String file = "prog-test/switch2-1.cam";
	Main m = new Main();
	assertEquals("Switch with matching: " ,
		     "Lista de varios elementos",
		     m.testingProgram(file, "main__main"));
    }

    public void testSwitchCountingList() {
	String file = "prog-test/switch2-2.cam";
	Main m = new Main();
	assertEquals("Using Switch to count list elements",
		     "4 ",
		     m.testingProgram(file, "main__main"));
    }

    public void testAddInt() {
	String file = "prog-test/suma.cam";
	Main m = new Main();
	assertEquals("Adding 2 integers: ",
		     "4 ",
		     m.testingProgram(file, "main__main"));
    }

    public void testAddFloat() {
	String file = "prog-test/sum-float.cam";
	Main m = new Main();
	assertEquals("Adding two floats",
		     "5.8",
		     m.testingProgram(file, "main__main"));
    }

    public void testMultiplyIntegers() {
	String file = "prog-test/mul-int.cam";
	Main m = new Main();
	assertEquals("Multiply two integers",
		     "8 ",
		     m.testingProgram(file, "main__main"));
    }

    public void testMultiplyNegInteger() {
	String file = "prog-test/mul-int-neg.cam";
	Main m = new Main();
	assertEquals("Multiply neg - pos integers (-2) * 4 ",
		     "-8 ",
		     m.testingProgram(file, "main__primero"));
	assertEquals("Multiply pos - neg integers 2 * -4",
		     "-8 ",
		     m.testingProgram(file, "main__segundo"));
	assertEquals("Multiply neg - neg integers -2 * -4",
		     "8 ",
		     m.testingProgram(file, "main__ambos"));
    }

    public void testMultiplyFloat() {
	String file = "prog-test/mul-float.cam";
	Main m = new Main();
	float result = (float)4.5 * (float)2.2;
	assertEquals("Multiply two floats",
		     new Float(result).toString(),
		     m.testingProgram(file, "main__main"));
    }
    
    public void testLetFunction() {
	String file = "prog-test/letfunction.cam";
	Main m = new Main();
	assertEquals("Apply function with retarded second param",
		     "10 ",
		     m.testingProgram(file, "main__main"));
    }

    public void testMapFunction() {
	String file = "prog-test/map.cam";
	Main m = new Main();
	assertEquals("Appling function to list of numbers",
		     "2 4 6 ",
		     m.testingProgram(file, "main__main"));
    }

    public void testMap2Function() {
	String file = "prog-test/map2.cam";
	Main m = new Main();
	assertEquals("Appling map with \\ x ",
		     "10 ",
		     m.testingProgram(file, "main__main"));
    }
    public void testBasicMap0elementsFunction() {
	String file = "prog-test/map-facil0.cam";
	Main m = new Main();
	assertEquals("Appling map to empty list",
		     "",
		     m.testingProgram(file, "main__main"));
		     
    }
    
    public void testBasicMap1elementFunction() {
	String file = "prog-test/map-facil1.cam";
	Main m = new Main();
	assertEquals("Appling map to list with 1 element",
		     "2 ",
		     m.testingProgram(file, "main__main"));
    }
    
    public void testBasicMap2elementsFunction() {
	String file = "prog-test/map-facil.cam";
	Main m = new Main();
	assertEquals("Appling map to list",
		      "2 4 ",
		      m.testingProgram(file, "main__main"));
    }
    
    public void testBasicMapNelementsFunction() {
	String file = "prog-test/map-completo.cam";
	Main m = new Main();
	assertEquals("Appling map to list",
		      "10 12 14 16 ",
		      m.testingProgram(file, "main__main"));
    }

    
    public void testComparationsLessThan() {
	String file = "prog-test/compareVarious.cam";
	Main m = new Main();

	assertEquals(" 25 < 30",
		     "si",
		     m.testingProgram(file, "main__sayTrueAgain"));
	assertEquals(" 35 < 30",
		     "F",
		     m.testingProgram(file, "main__sayFalseAgain"));
    }
    public void testComparationsEquals() {
	String file = "prog-test/compareVarious.cam";
	Main m = new Main();

	assertEquals(" 1 = 1 ",
		     "si",
		     m.testingProgram(file, "main__sayTrueTwice"));
	assertEquals(" 1 = 2 ",
		     "F",
		     m.testingProgram(file, "main__sayFalseTwice")); 
    }

    public void testComparationsGreaterThan() {
	String file = "prog-test/compareVarious.cam";
	Main m = new Main();
	assertEquals(" 2 > 0",
		     "si",
		     m.testingProgram(file, "main__sayTrue"));
	assertEquals(" -1 > 0",
		     "F",
		     m.testingProgram(file, "main__sayFalse"));
    }
    

    
    public void testFactorial0() {
	String file = "prog-test/factorial.cam";
	Main m = new Main();
	assertEquals("Calculating factorial 0",
		     "1 ",
		     m.testingProgram(file, "main__cero"));
    }
    
    public void testFactorial1() {
	String file = "prog-test/factorial.cam";
	Main m = new Main();
	assertEquals("Calculating factorial 1",
		     "1 ",
		     m.testingProgram(file, "main__uno"));
    }
    
    public void testFactorialn() {
	String file = "prog-test/factorial.cam";
	Main m = new Main();
	assertEquals("Calculating factorial 2",
		     "2 ",
		     m.testingProgram(file, "main__dos"));
	assertEquals("Calculating factorial 3",
		     "6 ",
		     m.testingProgram(file, "main__tres"));
    }

    public void testLetOperation() {
	String file = "prog-test/let-op.cam";
	Main m = new Main();
	assertEquals("Calculating x = 2*2 -> x + x ",
		     "8 ",
		     m.testingProgram(file, "main__main"));
    }

    public void testPreludeHead() {
	String file = "prog-test/head.cam";
	Main m = new Main();
	assertEquals("Head of \"algo\"",
		     "a",
		     m.testingProgram(file, "main__main"));
    }

    public void testPreludeTail() {
	String file = "prog-test/tail.cam";
	Main m = new Main();
	assertEquals("Tail of \"algo\"",
		     "lgo",
		     m.testingProgram(file, "main__main"));
    }
    
    public void testPreludeTailTail() {
	String file = "prog-test/tailtail.cam";
	Main m = new Main();
	assertEquals("Combining 2 tails: tail (tail \"algo\")",
		     "go",
		     m.testingProgram(file, "main__main"));
    }

    public void testPreludeNull() {
	String file = "prog-test/null.cam";
	Main m = new Main();
	assertEquals("Testing null primitive",
		     "T",
		     m.testingProgram(file, "main__main"));
	assertEquals("Testing null in not null list",
		     "F",
		     m.testingProgram(file, "main__notnull"));
    }


    public void testFoldR() {
	String file = "prog-test/foldr.cam";
	Main m = new Main();
	assertEquals("Testing foldr application",
		     "10 ",
		     m.testingProgram(file, "main__main"));
    }

    public void testFoldL() {
	String file = "prog-test/foldl.cam";
	Main m = new Main();
	assertEquals("Testing foldl application",
		     "12 ",
		     m.testingProgram(file, "main__main"));
    }

    public void testDivInteger() {
	String file = "prog-test/div-int.cam";
	Main m = new Main();
	assertEquals("Testing integer division",
		     new Float(2).toString(),
		     m.testingProgram(file, "main__main"));
	assertEquals("Testing integer division by 0",
		     "Integer division: 0 in denominator!",
		     m.testingProgram(file, "main__main2"));
    }

    public void testDivFloat() {
	String file = "prog-test/div-float.cam";
	Main m = new Main();
	assertEquals("Testing float division",
		     new Float(3).toString(),
		     m.testingProgram(file, "main__main"));
	assertEquals("Testing float division by 0",
		     "Float division: 0 in denominator!",
		     m.testingProgram(file, "main__cero"));

    }


    public void testPutChar() {
	String file = "prog-test/putchar.cam";
	Main m = new Main();
	System.out.println("**** Imprimir 'a' *****");
	assertEquals("Testing putchar",
		     "",
		     m.testingProgram(file, "main__main"));
	System.out.println("***********************");
	//FIXME A�adir como prueba main = putChar (toUpper 'a')
    }

    public void testPutStr() {
	String file = "prog-test/putstr.cam";
	Main m = new Main();
	System.out.println("***** Imprimir 'cadena de caracteres' ******");
	assertEquals("Testing putstr",
		     "",
		     m.testingProgram(file, "main__main"));
	System.out.println("********");
    }

    public void testPutStrLn() {
	String file = "prog-test/putstrln.cam";
	Main m = new Main();
	System.out.println("***** Imprimir 'salto de linea\\n' ******");
	assertEquals("Testing putstrln",
		     "",
		     m.testingProgram(file, "main__main"));
	System.out.println("********");
    }

   /*
    public void testPutStrLn() {
	String file = "prog-test/putstrln.cam";
	Main m = new Main();
	assertEquals("Using prelude PutStrLn",
		     "hola, mundo",
		     m.testingProgram(file, "main__main"));
    }
    */
    
}