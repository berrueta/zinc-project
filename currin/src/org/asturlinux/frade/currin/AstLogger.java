package org.asturlinux.frade.currin;

import org.apache.log4j.Logger;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;

import java.io.IOException;
import java.io.FileOutputStream;

public class AstLogger {
    static Logger logger = Logger.getLogger(AstLogger.class);

    AstLogger() {
	try {
	    Logger log = Logger.getLogger("AstLogger");
	    SimpleLayout format = new SimpleLayout();
	    WriterAppender fa = new WriterAppender(format,
						 new FileOutputStream("ast.log"));
	    log.addAppender(fa);
	    log.setAdditivity(false);
	    log.setLevel((Level) Level.INFO);
	} catch (IOException io) {
	    BasicConfigurator.configure();
	    Logger main = Logger.getLogger("Main");
	    logger.error("Cannot create AST logger output file");
	}    }
}