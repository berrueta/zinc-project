package org.asturlinux.frade.currin;

import org.asturlinux.frade.currin.program.Program;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.exceptions.ParsingException;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.SimpleLayout;


public class Main {

    static Logger logger = Logger.getLogger(Main.class);


    public Main (String file, String function) {
	BasicConfigurator.configure();
	try {
	    Logger log = Logger.getLogger("Main");
	    log.debug("En la ejecucion de Main");
	    Program _p = parseFile(file);
	    _p.debug();
	    Machine _m = new Machine(_p);
      	    _m.evaluate(function);
	    System.out.println(_m.printResult());
	} catch ( ParsingException pe ) {
	    System.out.println("ERROR parseando el fichero");
	} catch ( InterpreterException ie) {
	    System.out.println("ERROR ejecutando funcion");
	    System.out.println(ie.getMessage());
	}
    }

    public Main() {
	try {
	    Logger log = Logger.getLogger("Main");
	    SimpleLayout format = new SimpleLayout();
	    WriterAppender fa = new WriterAppender(format,
						 new FileOutputStream("main.log"));
	    log.addAppender(fa);
	    log.setAdditivity(false);
	    log.setLevel((Level) Level.DEBUG);

	} catch (IOException io) {
	    BasicConfigurator.configure();
	    Logger main = Logger.getLogger("Main");
	    logger.error("Cannot create AST logger output file");
	}
    }

    public String testingProgram(String file, String function) {
	try {
	    Logger log = Logger.getLogger("RuntimeLogger");
	    log.debug("================================================");
	    log.debug("  File: " + file + " function: " + function);
	    log.debug("================================================");
	    Program _p = parseFile(file);
	    _p.debug();
	    Machine _m = new Machine(_p);
      	    _m.evaluate(function);
	    return _m.printResult();
	} catch ( ParsingException pe ) {
	    System.out.println("ERROR parseando el fichero");
	} catch ( InterpreterException ie) {
	    System.out.println("ERROR ejecutando funcion");
	    System.out.println(ie.getMessage());
	    return ie.getMessage();
	}

	return new String();
    }


    /*
     * Parse file with antlr analizers.
     */
    static public Program parseFile(String file) 
	throws ParsingException {

	try {

	    L lexer = new L(new DataInputStream( new FileInputStream(file)));
	    P parser = new P(lexer);
	    return parser.startRule();

	} catch (Exception e) {
	    System.out.println(e.getMessage());
	    throw new ParsingException();
	}

    }

}