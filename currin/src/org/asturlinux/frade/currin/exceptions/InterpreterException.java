package org.asturlinux.frade.currin.exceptions;
import java.lang.Exception;

public class InterpreterException extends Exception {
    String _why;

    public  InterpreterException(String message) {
	_why = message;
    }

    public String getMessage() { return _why; }
}