package org.asturlinux.frade.currin.machine;

import junit.framework.TestCase;

import java.lang.ClassCastException;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.ComparePrimitive;
import org.asturlinux.frade.currin.program.builtin.AddIntPrimitive;
import org.asturlinux.frade.currin.program.builtin.AddFloatPrimitive;
import org.asturlinux.frade.currin.exceptions.InterpreterException;


public class BuiltInRepositoryTest extends TestCase {

    public void testGetPrimitiveSimple()
	throws InterpreterException, ClassCastException {
	Function result = BuiltInRepository.getInstance().getBuiltInFunction("compare");
	assertEquals("Solicited compare primitive",
		     "org.asturlinux.frade.currin.program.builtin.ComparePrimitive",
		     result.getClass().getName());
		     
    }

    public void testGetPrimitiveWithVariants() 
    throws InterpreterException {
	Function result = BuiltInRepository.getInstance().getBuiltInFunction("_43_");

	assertEquals("Solicited Add int primitive",
		     "org.asturlinux.frade.currin.program.builtin.AddIntPrimitive",
		     result.getClass().getName());

	Function result2 = BuiltInRepository.getInstance().getBuiltInFunction("_43__46_");
	assertEquals("Solicited Add float primitive",
		     "org.asturlinux.frade.currin.program.builtin.AddFloatPrimitive",
		     result2.getClass().getName());
    }

    public void testGetInexistentPrimitive() {
	try {
	    Function result = BuiltInRepository.getInstance().getBuiltInFunction("NOEXISTE");
	    fail("Must throw InterpreterException, function not exist");
	} catch (InterpreterException ie) {
	}
	
    }
    
    public void testGet64Variants()
	throws InterpreterException {
	Function ascii64 = BuiltInRepository.getInstance().getBuiltInFunction("_64_");
	assertEquals("Solicited _64_ primitive",
		     "org.asturlinux.frade.currin.program.builtin.Ascii64Primitive",
		     ascii64.getClass().getName());
	
	Function ascii64_2 = BuiltInRepository.getInstance().getBuiltInFunction("_64_2");
	assertEquals("Solicited _64_2 primitive",
		     "org.asturlinux.frade.currin.program.builtin.Ascii64Primitive",
		     ascii64_2.getClass().getName());

	Function ascii64_9 = BuiltInRepository.getInstance().getBuiltInFunction("_64_9");
	assertEquals("Solicited _64_9 primitive",
		     "org.asturlinux.frade.currin.program.builtin.Ascii64Primitive",
		     ascii64_9.getClass().getName());
    }
    

}