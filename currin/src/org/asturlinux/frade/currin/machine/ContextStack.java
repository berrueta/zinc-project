package org.asturlinux.frade.currin.machine;

import java.util.Stack;
//import org.asturlinux.frade.currin.program.Context;

public class ContextStack {
    Stack _contextStack;

    public ContextStack() {
	_contextStack = new Stack();
    }

    public boolean isEmpty() {
	return _contextStack.empty();
    }

    public void pushContext(Context c) {
	_contextStack.push(c);
    }

    public void popContext() {
	_contextStack.pop();
    }

    public Context currentContext() {
	return (Context)_contextStack.peek();
    }

}