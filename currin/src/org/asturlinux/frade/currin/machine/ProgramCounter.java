package org.asturlinux.frade.currin.machine;

import org.asturlinux.frade.currin.program.Program;
import org.asturlinux.frade.currin.program.instruction.Instruction;
import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.program.InstrBlock;


public class ProgramCounter {

    int _counter = 0;
    InstrBlock _function_ib;

    public ProgramCounter(InstrBlock function_ib) {
	_function_ib = function_ib;
    }

    /*
     * Returns next instruction in function. If it doesn't exist
     * (i.e. after an "exec") returns an special "EndBlockInstrucion"
     * that close current context
     *
     */
    public Instruction getInstruction() {
	return (Instruction) _function_ib.getInstruction(_counter);
    }


    public boolean hasNext() {
	return ( _counter < _function_ib.size());
    }

    public void next() { _counter++; }

}