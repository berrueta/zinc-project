package org.asturlinux.frade.currin.machine;

import java.util.Stack;

public class PCStack {
    Stack _pcstack = new Stack();

    public PCStack() {}

    public boolean isEmpty() {
	return _pcstack.empty();
    }

    public void pushPc(ProgramCounter pc) {
	_pcstack.push(pc);
    }

    public ProgramCounter popPc() {
	return (ProgramCounter)_pcstack.pop();
    }

    public ProgramCounter currentPc() {
	return (ProgramCounter)_pcstack.peek();
    }

}