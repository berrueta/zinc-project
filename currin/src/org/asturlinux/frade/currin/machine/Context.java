package org.asturlinux.frade.currin.machine;

import org.asturlinux.frade.currin.program.Program;
import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.program.InstrBlock;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinConstr;

import java.util.Vector;
import java.util.Iterator;

import org.apache.log4j.Logger;

public class Context {

    Memory _memory;
    PCStack _pcStack = new PCStack();
    Vector _running_params;

    public Context(Function function, Vector running_params)
	throws InterpreterException {
	
	ProgramCounter pc;
	_memory = new Memory();

	if ( function.getNumberOfParameters() != running_params.size()) {
	    
	    if ( function.getName().endsWith("IO") ) {
		CurrinRef cr = new CurrinRef(new CurrinConstr("empty",
							      new Vector()));
		running_params.add(cr);
	    } else
		throw new InterpreterException("ERROR: Function " 
					       + function.getName() 
					       + " require " 
					       + function.getNumberOfParameters()
					       + " and called only with "
					       + running_params.size());
	    
	}
	putParamsInMemory(function.getSymbolicParams(), running_params);
	pc = new ProgramCounter(function.getInstructionsBlock());
        _pcStack.pushPc(pc);
    }


    private void putParamsInMemory(Vector symb_names, Vector references) {
	Iterator local_dir = symb_names.iterator();
	Iterator ref = references.iterator();

	while ( local_dir.hasNext() && ref.hasNext() ) {
	    _memory.setElementAt((String)local_dir.next(),
				 (CurrinRef)ref.next());
	}
    }

    public ProgramCounter getCurrentPc() {return _pcStack.currentPc();}
    public void popPc() { _pcStack.popPc(); }
    public boolean hasNoMorePc() { return _pcStack.isEmpty(); }

    public Memory getMemory() { return _memory; }

    public void runInstrBlock(InstrBlock ib) {
	ProgramCounter pc = new ProgramCounter(ib);
	_pcStack.pushPc(pc);
    }

}