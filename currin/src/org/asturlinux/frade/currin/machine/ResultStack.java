package org.asturlinux.frade.currin.machine;

import java.util.Stack;
import org.asturlinux.frade.currin.program.type.CurrinExpr;
import java.lang.Integer;

public class ResultStack {
    Stack _resultStack;

    public ResultStack() {
	_resultStack = new Stack();
    }

    public boolean isEmpty() {
	return _resultStack.empty();
    }

    public void pushResult(int c) {
	_resultStack.push(new Integer(c));
    }

    public int popResult() {
	return ((Integer)_resultStack.pop()).intValue();
    }

    public int elementos() {
	return _resultStack.size();
    }

}