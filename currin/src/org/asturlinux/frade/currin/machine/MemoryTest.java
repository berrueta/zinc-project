package org.asturlinux.frade.currin.machine;

import junit.framework.TestCase;
import org.asturlinux.frade.currin.program.type.CurrinChar;
import org.asturlinux.frade.currin.program.type.CurrinExpr;
import org.asturlinux.frade.currin.program.type.CurrinRef;

public class MemoryTest extends TestCase{

    public void testMemoryAllocation() {
 	CurrinChar cc = new CurrinChar(65);
 	CurrinExpr result;
 	Memory m = new Memory();

	CurrinRef cr_char = new CurrinRef(cc);

 	m.setElementAt("_1", cr_char);
 	CurrinRef cr_result = m.getElementAt("_1");
 	assertEquals("Escritura/lectura memoria", cr_result, cr_char);
    }
}