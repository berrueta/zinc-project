package org.asturlinux.frade.currin.machine;

import org.asturlinux.frade.currin.program.builtin.AddIntPrimitive;
import org.asturlinux.frade.currin.program.builtin.AddFloatPrimitive;
import org.asturlinux.frade.currin.program.builtin.MulIntPrimitive;
import org.asturlinux.frade.currin.program.builtin.MulFloatPrimitive;
import org.asturlinux.frade.currin.program.builtin.Ascii64Primitive;
import org.asturlinux.frade.currin.program.builtin.Ascii45Primitive;
import org.asturlinux.frade.currin.program.builtin.ComparePrimitive;
import org.asturlinux.frade.currin.program.builtin.FailedPrimitive;
import org.asturlinux.frade.currin.program.builtin.DivPrimitive;
import org.asturlinux.frade.currin.program.builtin.DivFloatPrimitive;
import org.asturlinux.frade.currin.program.builtin.PutCharPrimitive;
import org.asturlinux.frade.currin.program.builtin.PutStrPrimitive;
import org.asturlinux.frade.currin.program.builtin.Ascii6262Primitive;
import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;


import java.util.Vector;
import java.util.Iterator;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.apache.log4j.Logger;

public class BuiltInRepository {
    
    Vector _primitives = new Vector();


    protected BuiltInRepository() {
	//inicializar();
    }


    static private BuiltInRepository _instance = null;

    static public BuiltInRepository getInstance() { 
	if ( _instance == null ) _instance = new BuiltInRepository();
	return _instance;
    }

    /*
    private void inicializar() {
	_primitives = new Vector();
	_primitives.add(new AddIntPrimitive());
	_primitives.add(new AddFloatPrimitive());
	_primitives.add(new MulIntPrimitive());
	_primitives.add(new MulFloatPrimitive());
	_primitives.add(new Ascii64Primitive());
	_primitives.add(new ComparePrimitive());
	_primitives.add(new FailedPrimitive());
	_primitives.add(new Ascii45Primitive());
	Iterator i = _primitives.iterator();
	while ( i.hasNext() ) {
	    System.out.println(((BuiltInFunction)i).getName());
	}
	System.out.println("FIN INICIALIZAR");

    }
    
    public Function getBuiltInFunction(String id) 
	throws InterpreterException {
	Iterator i = _primitives.iterator();
	while ( i.hasNext() ) {
	    i.next();
	    System.out.println(((BuiltInFunction)i).getClass().getName());
	    //	    if ( ((BuiltInFunction)i).recognizedIn(id) )
	    //	return (Function)i;
	    return null;
	}
	throw new InterpreterException("Cannot find " + id + " in repository or program");
    }

    */
    public Function getBuiltInFunction(String id) 
    throws InterpreterException {

	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.info("BuiltInRepository: solicited function " + id);
	
	if ( id.equals("_43_") ) {
	    return new AddIntPrimitive();
	}
	
	if ( id.equals("_43__46_") ) {
	    return new AddFloatPrimitive();
	}

	if ( id.equals("_42_") ) {
	    return new MulIntPrimitive();
	}

	if ( id.equals("_42__46_") ) {
	    return new MulFloatPrimitive();
	}

	if ( id.equals("div") ) {
	    return new DivPrimitive();
	}

	if ( id.equals("_47__46_")) {
	    return new DivFloatPrimitive();
	}

	if ( id.equals("compare") ) {
	    return new ComparePrimitive();
	}

	if ( id.equals("_45_") ) {
	    return new Ascii45Primitive();
	}

	if ( id.equals("failed") ) {
	    return new FailedPrimitive();
	}

	if ( id.equals("putChar") ) {
	    return new PutCharPrimitive();
	}

	if ( id.equals("putStr") ) {
	    return new PutStrPrimitive();
	}

	if ( id.equals("_62__62_") ) {
	    return new Ascii6262Primitive();
	}

	Pattern pattern = Pattern.compile("_64_?");
	Matcher matcher = pattern.matcher(new String(id));

	if ( matcher.find() ) {
	    return new Ascii64Primitive();
	}

	

	throw new InterpreterException("Cannot find " + id + " function");
	}
}