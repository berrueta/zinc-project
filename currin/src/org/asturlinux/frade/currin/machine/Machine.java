package org.asturlinux.frade.currin.machine;

import org.asturlinux.frade.currin.program.Program;
import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.instruction.Instruction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
//DEBUG
import org.asturlinux.frade.currin.program.type.CurrinExpr;


import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.BuiltInRepository;

import java.util.Vector;
import java.util.Iterator;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

public class Machine {

    ContextStack _contextStack = new ContextStack();
    Program _program;
    CurrinRef _result;

    public Machine(Program p) {
	_program = p;
    }

    public ContextStack getContextStack() { return _contextStack; }

    public Program getProgram() { return _program; }


    public Machine getNewMachine() {
	return new Machine(_program);
    }
    /*
     * Control method that runs the function function_name
     *  and print the result
     *
     */
    public void evaluate (String function_name )
	throws InterpreterException {

	run( function_name, new Vector() );
    }


    public void run( String function_name, Vector run_parameters ) 
	throws InterpreterException {

	Function main;
	try {
	    main = _program.findFunction(function_name);
	} catch (InterpreterException ie) {
	    main = BuiltInRepository.getInstance().getBuiltInFunction(function_name);
	}

	//Function loads her own code (to allow builtin functions)
	main.run(this, run_parameters);

      	Logger logger = Logger.getLogger("Runtime");
	logger.info("Begins execution of function " + function_name);

	Instruction i;
      	while (!_contextStack.isEmpty()) {

	    if ( _contextStack.currentContext().getCurrentPc().hasNext()) {
		i =_contextStack.currentContext().getCurrentPc().getInstruction();
		_contextStack.currentContext().getCurrentPc().next();
		i.exec(this);
	    } else {
		_contextStack.currentContext().popPc();
		if ( _contextStack.currentContext().hasNoMorePc() )
		    _contextStack.popContext();
	    }
	}

	logger.info("Ends execution of function " + function_name);
    }

    public String printResult() {
	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("End Program. Begin result evaluation");
	CurrinExpr ce = getResult().evaluate(this);
	for (int i = 0; !ce.inHNF(); i++) {
	    ce = ce.evaluate(this);
	    ce.print();
	}
	logger.debug("Return evaluated result");
	return ce.print();
    }

    public void setResult(CurrinRef cr) { _result = cr; }

    public CurrinRef getResult() { return _result; }

}
