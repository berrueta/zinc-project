package org.asturlinux.frade.currin.machine;

import java.util.HashMap;
import org.asturlinux.frade.currin.program.type.CurrinExpr;
import org.asturlinux.frade.currin.program.type.CurrinRef;

import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.Level;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;

public class Memory {

    static Logger logger = Logger.getLogger(Memory.class);
    HashMap  _memory;

    public Memory() {
	try {
	    Logger log = Logger.getLogger("Memory");
	    SimpleLayout format = new SimpleLayout();
	    WriterAppender fa = new WriterAppender(format,
						   new FileOutputStream("memory.log"));
	    log.addAppender(fa);
	    log.setAdditivity(false);
	    log.setLevel((Level) Level.DEBUG);
	    _memory = new HashMap();
	} catch ( FileNotFoundException fnfe ){
	    BasicConfigurator.configure();
	    Logger main = Logger.getLogger("Main");
	    main.error("Cannot create file appender in Memory logger");
	}
    }

    public CurrinRef getElementAt(String position)
    {
	return (CurrinRef)_memory.get(position);
    }

    public void setElementAt(String position, CurrinRef element) {
	Logger log = Logger.getLogger("Memory");
	log.debug("Alloc element in " + position);
	_memory.put(position, element);
    }


}