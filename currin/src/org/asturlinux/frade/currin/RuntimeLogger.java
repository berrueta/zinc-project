package org.asturlinux.frade.currin;

import org.apache.log4j.Logger;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;

import java.io.IOException;
import java.io.FileOutputStream;

public class RuntimeLogger {
    static Logger logger = Logger.getLogger(RuntimeLogger.class);

    RuntimeLogger() {
	try {
	    Logger log = Logger.getLogger("RuntimeLogger");
	    SimpleLayout format = new SimpleLayout();
	    WriterAppender fa = new WriterAppender(format,
						 new FileOutputStream("runtime.log"));
	    log.addAppender(fa);
	    log.setAdditivity(false);
	    log.setLevel((Level) Level.DEBUG);
	} catch (IOException io) {
	    BasicConfigurator.configure();
	    Logger main = Logger.getLogger("Main");
	    logger.error("Cannot create AST logger output file");
	}
    }
}