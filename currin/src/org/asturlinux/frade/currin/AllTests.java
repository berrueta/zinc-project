package org.asturlinux.frade.currin;

import junit.framework.*;
import org.asturlinux.frade.currin.program.*;
import org.asturlinux.frade.currin.program.instruction.*;
import org.asturlinux.frade.currin.program.type.*;
import org.asturlinux.frade.currin.machine.*;


public class AllTests {

    public static Test suite() {

        TestSuite suite = new TestSuite("Testing All");
	suite.addTestSuite(CheckerTest.class);
	suite.addTestSuite(ProgramTest.class);
	suite.addTestSuite(ModuleTest.class);
	suite.addTestSuite(CurrinCharTest.class);
	suite.addTestSuite(MemoryTest.class);
	suite.addTestSuite(BuiltInRepositoryTest.class);
       	suite.addTestSuite(MainTest.class); 
        return suite;
    }

    public static void main(String args[]) {
        junit.textui.TestRunner.run(suite());
    }
}
