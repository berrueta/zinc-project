package org.asturlinux.frade.currin.program.instruction;

import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.machine.Context;
import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import java.util.Vector;

import org.apache.log4j.Logger;

public class ExecInstruction extends Instruction {

    String _function_name;
    Vector _concrete_params;

    public ExecInstruction(String function_name, Vector params) {
	_function_name = function_name;
	_concrete_params = params;
    }

    public void exec(Machine m)
	throws InterpreterException {

	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Begin execution of function: " + _function_name);
	m.run(_function_name, _concrete_params);
	logger.debug("End execution of function " + _function_name);
    }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("EXEC " + _function_name);
    }
}