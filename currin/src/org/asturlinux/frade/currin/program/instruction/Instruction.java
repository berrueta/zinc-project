package org.asturlinux.frade.currin.program.instruction;

import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.exceptions.InterpreterException;

public abstract class Instruction {

    /*
     * The instruction do their job
     *
     */
    public abstract void exec(Machine m)
	throws InterpreterException;


    /*
     * For debuging. Used to check the AST:
     *
     *  Print instruction name and other usefull information
     *
     */
    public abstract void debug();



}