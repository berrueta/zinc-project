package org.asturlinux.frade.currin.program.instruction;

import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Memory;

import org.apache.log4j.Logger;

public class StoreInstruction extends Instruction {

    String _where;
    public StoreInstruction(String where) {
	_where = where;
    }
    
    public void debug() { 
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("STORE in " + _where);
    }
    
    public void exec(Machine m) { 
	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Running Store in (FIXME) " + _where);
	Memory mem = m.getContextStack().currentContext().getMemory();
	mem.setElementAt(_where, m.getResult());
    }
}