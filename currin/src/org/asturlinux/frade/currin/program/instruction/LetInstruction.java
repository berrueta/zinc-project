package org.asturlinux.frade.currin.program.instruction;

import org.asturlinux.frade.currin.program.type.CurrinExpr;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.machine.Memory;


import java.util.Vector;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class LetInstruction extends Instruction{

    String  _where;
    CurrinExpr _what;

    public LetInstruction(String where, CurrinExpr what)
	throws NumberFormatException {
	_where = where;
	_what = what;
    }

    public void exec(Machine m) {
	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Run LET ");

	Memory mem = m.getContextStack().currentContext().getMemory();
	CurrinRef cr = _what.getMemoryForm(mem);

	mem.setElementAt(_where, cr);
    }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("LET " + _where + " --data: ");
	_what.debug();
    }

}