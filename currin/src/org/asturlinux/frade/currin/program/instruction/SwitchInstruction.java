package org.asturlinux.frade.currin.program.instruction;

import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.Context;
import java.util.Iterator;

import org.apache.log4j.Logger;
import java.util.Vector;

public class SwitchInstruction extends Instruction {

    public static final int FLEX = 0;
    public static final int RIGID = 1;
    Vector _cases;
    int _type;
    String _selector;
    SwitchCase _default;


    public SwitchInstruction(int type, String selector, 
			     Vector cases, SwitchCase default_case) {
	_type = type;
	_selector = selector;
	_cases = cases;
	_default = default_case;
    }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("SWITCH ");
	Iterator i = _cases.iterator();
	while ( i.hasNext() ) {
	    ((SwitchCase)i.next()).debug();
	}
    }

    public void exec(Machine m)
	throws InterpreterException {
	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Running Switch ");

	Memory mem = m.getContextStack().currentContext().getMemory();
	CurrinRef switcher = mem.getElementAt(_selector);
	if ( switcher == null ) {
	    throw new InterpreterException("Switcher nulo en Switch");
	}

	Iterator i = _cases.iterator();
	SwitchCase sc;
	while ( i.hasNext() ) {
	    sc = ((SwitchCase)i.next());

	    if (sc.getSwitcher().equals(switcher, mem)) {
		logger.debug("Matched switch Option");
		m.getContextStack().currentContext().runInstrBlock(sc.getInstrToRun());
		return; /* Unreachable state */
	    }
	}
	logger.debug("In switch default case");
	if ( _default == null ) 
	    throw new InterpreterException("Required default case in switch" 
					   + " but is not defined");
	m.getContextStack().currentContext().runInstrBlock(_default.getInstrToRun());
	logger.debug("Not found valid option in switch");
    }

}