package org.asturlinux.frade.currin.program.instruction;

import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.program.InstrBlock;

import org.apache.log4j.Logger;

public class AssignInstruction extends Instruction {

    InstrBlock _instructions;

    public AssignInstruction(String where, InstrBlock instructions) {
	_instructions = instructions;
	_instructions.addInstruction(new StoreInstruction(where));
    }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("ASSIGN --------");
	_instructions.debug();
	logger.debug("---------------");
	
    }

    public void exec(Machine m) {
	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Running Assign");
	m.getContextStack().currentContext().runInstrBlock(_instructions);
    }
}