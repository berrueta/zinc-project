package org.asturlinux.frade.currin.program.instruction;

import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.InstrBlock;

import org.apache.log4j.Logger;

public class SwitchCase {

    CurrinRef _condition = null;

    InstrBlock _instructions;
    public SwitchCase(CurrinRef condition, InstrBlock instructions) {
	_instructions = instructions;
	_condition = condition;
    }

    public CurrinRef getSwitcher() { 
	return _condition; }

    public InstrBlock getInstrToRun() { return _instructions; }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("-- SwithCase ");
	_instructions.debug();
    }

}