package org.asturlinux.frade.currin.program.instruction;

import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.program.type.CurrinRef;

import org.apache.log4j.Logger;

public class EnterInstruction extends Instruction {

    String _what;

    public EnterInstruction(String what) {
	_what = what;
    }

    public void exec(Machine m) {
	Logger logger = Logger.getLogger("Runtime");
	logger.debug("Running Enter instruction");
	Memory mem = m.getContextStack().currentContext().getMemory();
	CurrinRef cr = mem.getElementAt(_what);
	if ( cr == null ) 
	    logger.debug("Chungo");
	cr.evaluate(m);
	m.setResult(cr);

    }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("ENTER " + _what);
    }
}