package org.asturlinux.frade.currin.program.instruction;

import org.asturlinux.frade.currin.program.type.CurrinExpr;
import org.asturlinux.frade.currin.program.Checker;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.machine.Memory;

import org.apache.log4j.Logger;

public class ReturnInstruction extends Instruction {

    String _what;

    public ReturnInstruction(String something) {
	_what = something;
    }

    /*
     */
    public void exec(Machine m) {
	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Run RETURN");
	
	Memory mem = m.getContextStack().currentContext().getMemory();
	m.setResult(mem.getElementAt(_what));
    }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("RETURN " + _what); 
    }

}