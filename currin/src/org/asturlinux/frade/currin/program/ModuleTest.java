package org.asturlinux.frade.currin.program;

import junit.framework.TestCase;
import org.asturlinux.frade.currin.exceptions.InterpreterException;

public class ModuleTest extends TestCase {

    public void testFunctionSearch() 
    throws InterpreterException {
	Module m = new Module("test");
	Function f1 = new Function("f1", null);
	Function f2 = new Function("f2", null);
	Function result;

	m.addFunction(f1);
	m.addFunction(f2);


	result = m.getFunction("f1");
	assertEquals("Searching function f1", f1, result);

	result = m.getFunction("f2");
	assertEquals("Searching function f2", f2, result);

    }
}