package org.asturlinux.frade.currin.program;

import junit.framework.TestCase;
import org.asturlinux.frade.currin.exceptions.InterpreterException;

public class ProgramTest extends TestCase {
    public void testCurrentModule() {
	Module m = new Module("main");
	Program p = new Program();
	p.addModule(m);

	assertEquals("Insercion de modulo en programa",
		     p.getCurrentModule().getName(),"main");

	Module m2 = new Module("main2");
	p.addModule(m2);
	
	assertEquals("Insercion de un segundo modulo",
		     p.getCurrentModule().getName(),"main2");
    }

    public void testFindingFunction()
    throws InterpreterException {
	Module m = new Module("main");
	Module m2 = new Module("prelude");

	Function f = new Function("main__a", null);
	Function f2 = new Function("main__b", null);

	Function f3 = new Function("prelude__a", null);
	Function f4 = new Function("prelude__b", null);

	m.addFunction(f);
	m.addFunction(f2);
	
	m2.addFunction(f3);
	m2.addFunction(f4);

	Program p = new Program();
	p.addModule(m);
	p.addModule(m2);

	Function result_f2 = p.findFunction("main__b");
       	assertEquals("Looking for function in program ", f2, result_f2);

	Function result_f3 = p.findFunction("prelude__a");
	assertEquals("Looking for function in program ", f3, result_f3);

	try {
	    Function result_false = p.findFunction("bad_module_name");
	    fail("Should raise InterpreterException");
	} catch (InterpreterException ie) {
	    // good
	}
    }

}