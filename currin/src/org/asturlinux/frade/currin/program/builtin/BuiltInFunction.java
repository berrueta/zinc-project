package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinFunction;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.exceptions.InterpreterException;


public class BuiltInFunction extends Function {
    
    public BuiltInFunction(String id, Vector params) {
	super(id,params);
    }

    public boolean recognizedIn(String name) {
	System.out.println("Comparando " + name + " con mi nombre " + _name);
	return _name.equals(name); //_name inherited attribute
    }


    public CurrinRef evaluateSubExpr(Machine m, CurrinRef cr) 
	throws InterpreterException {

	if (cr.inHNF()) {
	    return cr;
	}

	Machine local = m.getNewMachine();
	local.run(((CurrinFunction)cr.getReference()).getId(), 
		  ((CurrinFunction)cr.getReference()).getRefParameters());
	cr.setReference(local.getResult().getReference());
	return cr;
    }
}