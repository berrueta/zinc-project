package org.asturlinux.frade.currin.program.builtin;

import  org.asturlinux.frade.currin.exceptions.InterpreterException;
import  org.asturlinux.frade.currin.program.type.CurrinRef;
import  org.asturlinux.frade.currin.program.type.CurrinFloat;
import  org.asturlinux.frade.currin.machine.Machine;

import java.util.Vector;

public class AddFloatPrimitive extends BuiltInFunction {

    public AddFloatPrimitive() {
	super("_43__46_", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {

	Vector _param_refs = preparingParameters(m, run_parameters);

	CurrinRef first_op = (CurrinRef)_param_refs.get(0);
	CurrinRef second_op = (CurrinRef)_param_refs.get(1);

	float a = ((CurrinFloat)first_op.getReference()).getValue();
	float b = ((CurrinFloat)second_op.getReference()).getValue();

	m.setResult( new CurrinRef(new CurrinFloat(a + b)));
    }

}