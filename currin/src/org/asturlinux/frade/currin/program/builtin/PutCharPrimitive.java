package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinChar;
import org.asturlinux.frade.currin.program.type.CurrinConstr;

import org.apache.log4j.Logger;

public class PutCharPrimitive extends BuiltInFunction {

    public PutCharPrimitive() {
	super("putChar", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {

	Vector _param_refs = preparingParameters(m, run_parameters);

	CurrinRef _char_to_print = (CurrinRef)_param_refs.get(0);

	_char_to_print = evaluateSubExpr(m, _char_to_print);

	char printme = ((CurrinChar)_char_to_print.getReference()).getChar(); 
	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Primitive put char: " + printme );
	System.out.print(printme);
	System.out.flush();
	m.setResult( new CurrinRef(new CurrinConstr("EMPTY", new Vector())));
    }

}