package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinChar;


public class FailedPrimitive extends BuiltInFunction {

    public FailedPrimitive() {
	super("failed", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {

	m.setResult(new CurrinRef(new CurrinChar('F')));
	}
    

}