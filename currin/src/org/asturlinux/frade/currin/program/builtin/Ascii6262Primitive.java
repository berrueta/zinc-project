package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinChar;
import org.asturlinux.frade.currin.program.type.CurrinConstr;

import org.apache.log4j.Logger;

public class Ascii6262Primitive extends BuiltInFunction {

    public Ascii6262Primitive() {
	super("_62__62_", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {

	Vector _param_refs = preparingParameters(m, run_parameters);

	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Primitive 62 62 ");

	CurrinRef action1 = (CurrinRef)_param_refs.get(0);
	//evaluateSubExpr(m, action1);
	while (!action1.inHNF() ) {
	    action1.evaluate(m);
	}
	CurrinRef action2 = (CurrinRef)_param_refs.get(1);
	//evaluateSubExpr(m, action2);
	while (!action2.inHNF() ) {
	    action2.evaluate(m);
	}

	m.setResult( new CurrinRef(new CurrinConstr("EMPTY", new Vector())));
    }

}