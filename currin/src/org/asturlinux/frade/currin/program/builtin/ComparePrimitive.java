package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinConstr;
import org.asturlinux.frade.currin.program.type.CurrinFunction;


public class ComparePrimitive extends BuiltInFunction {

    public ComparePrimitive() {
	super("compare", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {


	Vector _param_refs = preparingParameters(m, run_parameters);

	CurrinRef first_op = (CurrinRef)_param_refs.get(0);
	CurrinRef second_op = (CurrinRef)_param_refs.get(1);

	
	first_op = evaluateSubExpr(m, first_op);

	second_op = evaluateSubExpr(m, second_op);

	Memory mem = m.getContextStack().currentContext().getMemory();

	if ( first_op.equals(second_op,mem)) {
	    m.setResult(new CurrinRef(new CurrinConstr("prelude__EQ",
						       new Vector() ))); 

	} else {
	    if ( first_op.gt(second_op) )
		m.setResult(new CurrinRef(new CurrinConstr("prelude__GT",
							   new Vector())));
	    else
		m.setResult(new CurrinRef(new CurrinConstr("prelude__LT",
							   new Vector())));
	}
    }

}