package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinInt;

import org.apache.log4j.Logger;

public class MulIntPrimitive extends BuiltInFunction {

    public MulIntPrimitive() {
	super("_42_", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {

	Vector _param_refs = preparingParameters(m, run_parameters);

	CurrinRef first_op = (CurrinRef)_param_refs.get(0);
	CurrinRef second_op = (CurrinRef)_param_refs.get(1);

	first_op = evaluateSubExpr(m, first_op);
	second_op = evaluateSubExpr(m, second_op);

	int a = ((CurrinInt)first_op.getReference()).getValue();
	int b = ((CurrinInt)second_op.getReference()).getValue();

	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Primitiva Mul Enteros: " + a + " * " + b);
	m.setResult( new CurrinRef(new CurrinInt(a * b)));
    }

}