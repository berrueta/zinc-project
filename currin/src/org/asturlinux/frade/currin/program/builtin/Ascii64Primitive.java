package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
//import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinFunction;
import org.asturlinux.frade.currin.program.type.CurrinExpr;
import java.lang.ClassCastException;

import org.apache.log4j.Logger;

public class Ascii64Primitive extends BuiltInFunction {

    public Ascii64Primitive() {
	super("_64_", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {

	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Prepare parameters in ascii 64 Primitive");
	
	Vector _param_refs = new Vector();
	_param_refs = preparingParameters(m, run_parameters);
	logger.debug("We have " + _param_refs.size() + " params");

	CurrinRef f = (CurrinRef)_param_refs.get(0);
	CurrinFunction function = (CurrinFunction)f.getReference();

	String function_to_run = function.getId();
	Vector all_params_ref = new Vector(function.getRefParameters());
	logger.debug("Ready to run " + function_to_run);

	for ( int i = 1; i <= _param_refs.size()-1; i++)
	    all_params_ref.add(_param_refs.get(i));

	m.run(function_to_run, all_params_ref);
    }

}