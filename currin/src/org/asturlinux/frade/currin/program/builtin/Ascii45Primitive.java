package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinInt;
import java.lang.ClassCastException;


public class Ascii45Primitive extends BuiltInFunction {

    public Ascii45Primitive() {
	super("_45_", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {


	Vector _param_refs = preparingParameters(m, run_parameters);

	CurrinRef first_op = (CurrinRef)_param_refs.get(0);
	CurrinRef second_op = (CurrinRef)_param_refs.get(1);

	first_op.evaluate(m);
	second_op.evaluate(m);

	int a = ((CurrinInt)first_op.getReference()).getValue();
	int b = ((CurrinInt)second_op.getReference()).getValue();

	m.setResult( new CurrinRef(new CurrinInt(a - b)));
	//System.out.println("Ejecutado 45 (-) y resultado en la maquina ");
    }

}