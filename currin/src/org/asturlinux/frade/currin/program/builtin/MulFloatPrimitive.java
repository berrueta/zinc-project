package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinInt;
import org.asturlinux.frade.currin.program.type.CurrinFloat;
import org.asturlinux.frade.currin.program.type.CurrinExpr;
import java.lang.ClassCastException;


public class MulFloatPrimitive extends BuiltInFunction {

    public MulFloatPrimitive() {
	super("_42__46_", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {

	Vector _param_refs = preparingParameters(m, run_parameters);

	CurrinRef first_op = (CurrinRef)_param_refs.get(0);
	CurrinRef second_op = (CurrinRef)_param_refs.get(1);

	float a = ((CurrinFloat)first_op.getReference()).getValue();
	float b = ((CurrinFloat)second_op.getReference()).getValue();

	m.setResult( new CurrinRef(new CurrinFloat(a * b)));
    }

}