package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinInt;
import org.asturlinux.frade.currin.program.type.CurrinFloat;
import org.asturlinux.frade.currin.program.type.CurrinExpr;

import org.apache.log4j.Logger;

public class DivPrimitive extends BuiltInFunction {

    public DivPrimitive() {
	super("div", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {

	Vector _param_refs = preparingParameters(m, run_parameters);

	CurrinRef first_op = (CurrinRef)_param_refs.get(0);
	CurrinRef second_op = (CurrinRef)_param_refs.get(1);

	first_op = evaluateSubExpr(m, first_op);
	second_op = evaluateSubExpr(m, second_op);


	int a = ((CurrinInt)first_op.getReference()).getValue();
	int b = ((CurrinInt)second_op.getReference()).getValue();

	if ( b == 0 ) 
	    throw new InterpreterException("Integer division: 0 in denominator!");

	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Primitiva Division enteros: " + a + " " + b);
	m.setResult( new CurrinRef(new CurrinFloat(a / b)));
    }

}