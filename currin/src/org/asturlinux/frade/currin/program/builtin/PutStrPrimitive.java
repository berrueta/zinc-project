package org.asturlinux.frade.currin.program.builtin;

import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.builtin.BuiltInFunction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.type.CurrinChar;
import org.asturlinux.frade.currin.program.type.CurrinConstr;

import org.apache.log4j.Logger;

public class PutStrPrimitive extends BuiltInFunction {

    public PutStrPrimitive() {
	super("putStr", new Vector());
    }

    public void run( Machine m , Vector run_parameters ) 
	throws InterpreterException {

	Vector _param_refs = preparingParameters(m, run_parameters);

	CurrinRef _str_to_print = (CurrinRef)_param_refs.get(0);

	//FIXME!!!! Si los parametros estan sin evaluar.... 
	//_str_to_print = evaluateSubExpr(m, _str_to_print);

	String printme = _str_to_print.print();
	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Primitive put str: " + printme );
	System.out.print(printme);
	System.out.flush();
	m.setResult( new CurrinRef(new CurrinConstr("EMPTY", new Vector())));
    }

}