package org.asturlinux.frade.currin.program;

import java.util.Vector;
import java.util.Iterator;
import org.asturlinux.frade.currin.exceptions.InterpreterException;

import org.apache.log4j.Logger;

public class Program {

    //FIXME: Borrar static Logger logger = Logger.getLogger(Program.class);
    Vector _modules = new Vector();

    public Program() {
	//BasicConfigurator.configure();
    }

    /*
     * Invoque "debug" in all modules to print their contents
     *
     */
    public void debug() {
	Logger log = Logger.getLogger("AstLogger");
	log.debug("Program: ");

	for (int i = 0; i < _modules.size(); i++ )
	    ((Module) _modules.elementAt(i)).debug();
    }

    public void addModule(Module m) {
	_modules.addElement(m);
    }

    public Module getCurrentModule() {
	return (Module)_modules.lastElement();
    }


    /*
     * Search a function through modules using the cualified_name
     *
     */
    public Function findFunction( String cualified_name )
	throws InterpreterException {
	int underscore = cualified_name.indexOf("_");

	if ( underscore < 1 ) {
	    throw new InterpreterException("Function " + cualified_name 
					   + " not found in Program");
	}
	String module_to_find = cualified_name.substring(0, underscore);

	Iterator i = _modules.iterator();

	while ( i.hasNext() ) {
	    Module m = (Module)i.next();
	    if ( m.getName().toUpperCase().equals(module_to_find.toUpperCase()))
		return m.getFunction(cualified_name);
	}
	
	throw new InterpreterException("Function " + cualified_name 
				       + " not found in Program");
    }


}