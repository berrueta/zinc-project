package org.asturlinux.frade.currin.program;

import java.lang.String;
import java.util.Vector;
import java.util.Iterator;
import org.asturlinux.frade.currin.exceptions.InterpreterException;

import org.apache.log4j.Logger;

public class Module {
    String _name;
    Vector _functions = new Vector();

    public Module(String name) {
	_name = name;
    }

    public String getName() { return _name; }

    public void addFunction(Function f) {
	_functions.addElement(f);
    }

    /*
     * Returns the code of function called cualified_name
     *
     */
    public Function getFunction(String cualified_name) 
	throws InterpreterException {

	Iterator i = _functions.iterator();

	while (i.hasNext()) {
	    Function f = (Function)i.next();
	    if (f.getName().equals(cualified_name)) 
	    	return f;
	}

	throw new InterpreterException("Unable to find the function " 
				       + cualified_name 
				       + " in module "
				       + _name );
    }


    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");

	logger.debug("Module: " + _name);
	for ( int i = 0; i < _functions.size(); i++ )
	    ((Function)_functions.elementAt(i)).debug();
    }
}