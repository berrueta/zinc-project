package org.asturlinux.frade.currin.program.type;

import org.asturlinux.frade.currin.program.instruction.Instruction;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.Machine;

public abstract class CurrinExpr {
    
    protected boolean inHeadNormalForm;

    public boolean inHNF() { return inHeadNormalForm; }
    public abstract CurrinExpr evaluate(Machine m);
    public abstract void debug();
    public abstract String print();
    public abstract CurrinRef getMemoryForm(Memory mem);
    public abstract boolean currin_equals(CurrinExpr e, Memory m);
    public abstract boolean currin_gt(CurrinExpr e);

}