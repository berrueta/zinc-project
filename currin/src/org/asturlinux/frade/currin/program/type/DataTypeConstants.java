package org.asturlinux.frade.currin.program.type;

public interface DataTypeConstants {
    public final static int NEW_DATA = 0;
    public final static int CONCAT_DATA = 1;
    public final static int BOOLEAN_TRUE = 2;
    public final static int BOOLEAN_FALSE = 3;
}