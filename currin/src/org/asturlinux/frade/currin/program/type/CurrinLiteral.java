package org.asturlinux.frade.currin.program.type;


import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.Machine;

public abstract class CurrinLiteral extends CurrinExpr {
    public CurrinLiteral() {
	super();
	inHeadNormalForm = true;
    }
    public abstract CurrinExpr evaluate(Machine m);
    public abstract void debug();
    public abstract String print();
    public abstract boolean currin_equals(CurrinExpr ce, Memory m);
    public abstract boolean currin_gt(CurrinExpr ce);
    //public boolean inHNF() { return true; }

}