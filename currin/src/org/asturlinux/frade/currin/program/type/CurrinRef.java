package org.asturlinux.frade.currin.program.type;

import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.Machine;
import org.apache.log4j.Logger;

public class CurrinRef extends CurrinExpr {
    CurrinExpr _reference;

    public CurrinRef(CurrinExpr ref) {
	_reference = ref;
    }

    public void setReference(CurrinExpr ref) {
	_reference = ref;
    }

    public CurrinExpr getReference() {
	return _reference;
    }

    public CurrinExpr evaluate(Machine m) {
	
	if ( _reference.inHNF() ) {
	    return _reference;
	}
	_reference = _reference.evaluate(m);
	return _reference;
    };
    public void debug() {
	_reference.debug();
    };
    public String print() {
	return _reference.print();
    };

    public CurrinRef getMemoryForm(Memory mem) {
	return _reference.getMemoryForm(mem);
    };

    public boolean currin_equals(CurrinExpr ce, Memory m) {
	return (_reference.currin_equals(ce,m));
    }

   public boolean currin_gt(CurrinExpr ce) {
       return (_reference.currin_gt(ce));
   }

    public boolean gt(CurrinRef cr) {
	return currin_gt(cr.getReference());
    }

    public boolean equals(CurrinRef cr, Memory m) {
	//	Logger logger = Logger.getLogger("RuntimeLogger");
	//logger.debug(" Testing equals ");
	//_reference.debug();
	//cr.getReference().debug();
	return currin_equals(cr.getReference(),m);
    }

    public boolean inHNF() {
	return _reference.inHNF(); //FIXmE??
    }

}