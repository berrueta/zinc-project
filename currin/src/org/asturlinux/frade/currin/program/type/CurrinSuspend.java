package org.asturlinux.frade.currin.program.type;

import org.apache.log4j.Logger;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.Machine;

public class CurrinSuspend extends CurrinExpr {

    String _what;
    CurrinRef _suspended_ref = null;

    public CurrinSuspend(String what) {
	_what = what;
    }

    public CurrinExpr evaluate(Machine m) {
	return _suspended_ref.evaluate(m);
    }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug(" CurrinSuspend "+ _what);
    }
    public String print() {

	return new String("Mandenme imprimime CurrinSuspend FIXME"
			  + _suspended_ref.print());
    }

    public CurrinRef getMemoryForm(Memory mem) {
	_suspended_ref = mem.getElementAt(_what);
	return new CurrinRef(_suspended_ref.getReference());
    }

    public boolean currin_equals(CurrinExpr e, Memory m) {
	System.out.println("Equals a CurrinSuspend FIXME");
	return false;
    }

    public boolean currin_gt(CurrinExpr ce) {
	System.out.println("FIXME Greater Than between CurrinConstr????");
	return false;
    }

}