package org.asturlinux.frade.currin.program.type;

import org.asturlinux.frade.currin.program.instruction.Instruction;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.Machine;

import java.util.Vector;

import org.apache.log4j.Logger;

public class CurrinFloat extends CurrinLiteral {
    private float _value;

    public CurrinFloat(float value) {
	_value = value;
    }

    public float getValue() { return _value;}

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("    CurrinFloat (" + _value + ")");
    }

    public CurrinExpr evaluate(Machine m) {
	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug(" it not must be readed! evaluating CurrinFloat");
	return this;
    }

    public String print( ) {
	return new Float(_value).toString();
    }

    public CurrinRef getMemoryForm(Memory mem) {
	return new CurrinRef(new CurrinFloat(_value));
    }

    public boolean currin_equals(CurrinExpr ce, Memory m) {
	return _value == ((CurrinFloat)ce).getValue();
    }

    public boolean currin_gt(CurrinExpr ce) {
	return _value > ((CurrinFloat)ce).getValue();
    }
}