package org.asturlinux.frade.currin.program.type;

import org.asturlinux.frade.currin.program.instruction.Instruction;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.Machine;

import java.util.Vector;

import org.apache.log4j.Logger;

public class CurrinChar extends CurrinLiteral {
    private char _char;

    public CurrinChar(int unicode) {
	_char = (char)unicode;
    }

    public char getChar() { return _char;}

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("   CurrinChar (" + _char + ")");
    }

    //FIXME False
    public CurrinExpr evaluate(Machine m) {
	return this;
    }

    public String print() {
	return new Character(_char).toString();
    }

    public CurrinRef getMemoryForm(Memory mem) {
	return new CurrinRef(new CurrinChar(_char));
    }

    public boolean currin_equals(CurrinExpr ce, Memory m) {
	return _char == ((CurrinChar)ce).getChar();
    }

    public boolean currin_gt(CurrinExpr ce) {
	System.out.println("FIXME �un char mayor que otro?");
	return _char < ((CurrinChar)ce).getChar();
    }

    public boolean inHNF() { return true; }
}