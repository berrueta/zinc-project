package org.asturlinux.frade.currin.program.type;

import org.asturlinux.frade.currin.program.Function;
import org.asturlinux.frade.currin.program.type.CurrinExpr;
import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import java.lang.RuntimeException;

import java.util.Vector;
import java.util.Iterator;

import org.apache.log4j.Logger;

public class CurrinFunction extends CurrinExpr {

    String _function_to_run;
    Vector _param_names;
    Vector _param_refs;

    public CurrinFunction(Function f_def) {
	inHeadNormalForm = false;
	_function_to_run = f_def.getName();
	_param_names = f_def.getSymbolicParams();
    }

    CurrinFunction(String id, Vector _param_references) {
	inHeadNormalForm = false;
	_function_to_run = id;
	_param_names = null;
	_param_refs = _param_references;
    }

    public String getId() { return _function_to_run; }
    public Vector getRefParameters() { return _param_refs; }

    public CurrinExpr evaluate(Machine m) {

	Logger logger = Logger.getLogger("RuntimeLogger");
	logger.debug("Evaluating CurrinFuntion " + _function_to_run);

	try {
	    m.run(_function_to_run, _param_refs);
	} catch (InterpreterException ie) {
	    throw new RuntimeException(ie.getMessage());
	}

	return m.getResult().getReference(); //FIXME
    }

    public String print() {
	return new String(" Print de Function " + _function_to_run);
    }

    public CurrinRef getMemoryForm(Memory m) {	

	_param_refs = new Vector();
	Iterator i = _param_names.iterator();
	while ( i.hasNext() ) {
	    String where = (String)i.next();
	    CurrinRef param = m.getElementAt(where);
	    _param_refs.add(param);
	}
	return new CurrinRef(new CurrinFunction(_function_to_run, _param_refs));
    }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("    CurrinFunction " + _function_to_run);
    }

    public boolean currin_equals(CurrinExpr e, Memory m) {
	System.out.println("FIXME, currin-equals between functions");
	System.out.println(getId() + " - " + e.print());
	return false;
    }

    public boolean currin_gt(CurrinExpr ce) {
	System.out.println("FIXME Greater Than between CurrinConstr????");
	return false;
    }

}