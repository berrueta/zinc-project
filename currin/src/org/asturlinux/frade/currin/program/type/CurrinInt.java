package org.asturlinux.frade.currin.program.type;

import org.asturlinux.frade.currin.program.instruction.Instruction;
import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.Machine;

import java.util.Vector;

import org.apache.log4j.Logger;

public class CurrinInt extends CurrinLiteral {
    private int _value;

    public CurrinInt(int value) {
	_value = value;
    }

    public int getValue() { return _value;}

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("    CurrinInt (" + _value + ")");
    }

    public CurrinExpr evaluate(Machine m) {
	return this;
    }

    public String print( ) {
	return new Integer(_value).toString().concat(" ");
    }

    public CurrinRef getMemoryForm(Memory mem) {
	return new CurrinRef(new CurrinInt(_value));
    }

    public boolean currin_equals(CurrinExpr ce, Memory m) {
	return _value == ((CurrinInt)ce).getValue();
    }

    public boolean currin_gt(CurrinExpr ce) {
	return _value > ((CurrinInt)ce).getValue();
    }
}