package org.asturlinux.frade.currin.program.type;

import junit.framework.TestCase;


public class CurrinCharTest extends TestCase {
    
    public void testCharType() {
	CurrinChar cc = new CurrinChar(65);
	assertEquals("Decode unicode",'A',cc.getChar());
    }
}