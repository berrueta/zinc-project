package org.asturlinux.frade.currin.program.type;

import org.asturlinux.frade.currin.machine.Memory;
import org.asturlinux.frade.currin.machine.Machine;

import java.util.Vector;
import java.util.Iterator;
	

import org.apache.log4j.Logger;

public class CurrinConstr extends CurrinExpr {
    String _id;
    Vector _params; // CurrinRef

    public CurrinConstr(String id, Vector parameters) {
	_id = id;

	if ( _id.equals("_91__93_") || _id.equals("EMPTY") )
	    inHeadNormalForm = true;
	else {
	    inHeadNormalForm = false;
	}

	_params = parameters;
    }
    
    public String getId() { return _id; }

    public CurrinExpr evaluate(Machine m) {
	//Logger logger = Logger.getLogger("RuntimeLogger");
	Vector _evaluated_params = new Vector();
	
	inHeadNormalForm = true;
	Iterator i = _params.iterator();
	while ( i.hasNext() ) {
	    CurrinRef cr = new CurrinRef(((CurrinRef)i.next()).evaluate(m));
	    _evaluated_params.add(cr);
	    inHeadNormalForm = inHeadNormalForm && cr.inHNF();
	}
	_params = new Vector(_evaluated_params);
	return this;
    }


    public String print() {

	if ( _id.equals("prelude__True")) {
	    return new String("T");
	} 
	if ( _id.equals("prelude__False")) {
	    return new String("F");
	}

	Iterator i = _params.iterator();
	String result = new String();
	while ( i.hasNext() ) {
	    result = new String(result + ((CurrinRef)i.next()).print());
	}
	return result;
    }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");

	String p = new String();
	Iterator i = _params.iterator();
	while ( i.hasNext() ) {
	    p = new String((String)i.next() + " " + p);
	}

	logger.debug("   OpConstr " + _id + " args: " + p);
	
    }

    public CurrinRef getMemoryForm(Memory mem) { 
	String map_key;
	Iterator i = _params.iterator();
	Vector runtime_refs = new Vector();

	while ( i.hasNext() ) {
	    map_key = (String)i.next();
	    CurrinRef cr = mem.getElementAt(map_key);
	    runtime_refs.add(cr);
	}

	return new CurrinRef(new CurrinConstr(_id, runtime_refs));

    }

    /*
     * _id = _58_ could be necesary match some parameters
     *
     * .data _58_ [_1,_2] =:= .data _58_ [_95_1_95_1,_95_1_55_2]
     *
     *         e                            this
     *
     *
     */
    public boolean currin_equals(CurrinExpr e, Memory m){

	CurrinConstr cc = (CurrinConstr)e;

	if( !getId().equals(((CurrinConstr)e).getId())) 
	    return false;
	   
	// Necesary???
	if ( _params.size() != cc._params.size() )
	    return false;


	if ( getId().equals("_58_") ){

	    Iterator new_names = _params.iterator();
	    Iterator refs = cc._params.iterator();
	    while ( new_names.hasNext() ) {
		String req_name = (String)new_names.next();
		CurrinRef element = (CurrinRef)refs.next();
		m.setElementAt(req_name, element);
	    }
	}
	return true;
	
    }

    public boolean currin_gt(CurrinExpr ce) {
	System.out.println("FIXME Greater Than between CurrinConstr????");
	return false;
    }
}