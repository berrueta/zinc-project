package org.asturlinux.frade.currin.program;

import java.util.Vector;
import java.util.Iterator;
import org.asturlinux.frade.currin.program.instruction.Instruction;

import org.apache.log4j.Logger;

public class InstrBlock {

    static int block_counter = 0;

    int id;
    Vector _instructions = new Vector();

    public InstrBlock() {
	id = block_counter++;
    }

    public void addInstruction(Instruction i) {
	_instructions.addElement(i);
    }

    public Instruction getInstruction(int pc) {
	return (Instruction)_instructions.elementAt(pc);
    }

    public int size() { return _instructions.size(); }

    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("Block Instruction " + id);

	Iterator i = _instructions.iterator();
	while ( i.hasNext() ) {
	    ((Instruction)i.next()).debug();
	}
    }
}