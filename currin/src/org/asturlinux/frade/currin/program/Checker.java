package org.asturlinux.frade.currin.program;

import org.asturlinux.frade.currin.exceptions.UnknowDataCommandException;
import org.asturlinux.frade.currin.program.type.DataTypeConstants;

public class Checker {
    public static int getMemPosition(String id)
	throws NumberFormatException 
    {
	if (id.startsWith("_")) {
	    return Integer.decode(id.substring(1)).intValue();
	}

	throw new NumberFormatException();
    }


}