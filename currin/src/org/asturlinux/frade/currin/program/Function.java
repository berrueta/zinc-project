package org.asturlinux.frade.currin.program;

import java.lang.String;
import java.lang.ClassCastException;
import java.util.Vector;
import java.util.Iterator;

import org.asturlinux.frade.currin.program.type.CurrinRef;
import org.asturlinux.frade.currin.program.instruction.Instruction;
import org.asturlinux.frade.currin.exceptions.InterpreterException;
import org.asturlinux.frade.currin.machine.Machine;
import org.asturlinux.frade.currin.machine.Context;
import org.asturlinux.frade.currin.machine.Memory;

import org.apache.log4j.Logger;

public class Function {
    protected String _name;
    InstrBlock _ib = new InstrBlock();
    protected Vector _params;

    public Function(String name, Vector params) {
	_name = name;
	_params = params;
    }

    public int getNumberOfParameters() { return _params.size(); }

    public Vector getSymbolicParams() { return _params; }

    public InstrBlock getInstructionsBlock() { return _ib; }
    public void setInstructionsBlock(InstrBlock ib) { _ib = ib; }

    public String getName() { return _name; }

    public void addInstruction(Instruction i) {
	_ib.addInstruction(i);
    }


    public void debug() {
	Logger logger = Logger.getLogger("AstLogger");
	logger.debug("  Function: " + _name);
	_ib.debug();
    }

    public void run(Machine m, Vector _runtime_params) 
	throws InterpreterException {
	Vector _param_refs = preparingParameters(m,_runtime_params);
	
	Context current = new Context(this, _param_refs);
	m.getContextStack().pushContext(current);
    }

    /* 
     * Take a Vector of memory positions, 
     * and create new Vector with consecuent CurrinRefs
     *
     */ 
    protected Vector preparingParameters(Machine m, Vector _running_dirs) {

	if ( m.getContextStack().isEmpty() ) 
	    return _running_dirs;

	Vector references = new Vector();
	Iterator i = _running_dirs.iterator();
	Memory mem = m.getContextStack().currentContext().getMemory();
	
	try {

	    while ( i.hasNext() ) {
		CurrinRef cr = mem.getElementAt((String)i.next());
		references.add(cr);
	    }
	    
	    return references;
	} catch (ClassCastException cce) {
	    return _running_dirs;
	}
    }

}