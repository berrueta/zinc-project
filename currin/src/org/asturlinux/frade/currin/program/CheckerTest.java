package org.asturlinux.frade.currin.program;

import junit.framework.TestCase;

public class CheckerTest extends TestCase {

    public void testCorrectUnicodeConversion() {
	String ident = new String("_15");
	int deducted = Checker.getMemPosition(ident);

	assertEquals("Conversion de identificador a posicion de memoria",
		     15, deducted);
    }

    public void testIncorrectUnicodeConversion() {
	String ident = new String("_15_");

	try {
	    int deducted = Checker.getMemPosition(ident);
	    fail("Should raise a NumberBlaBlaBlaException");
	} catch ( NumberFormatException success){}
    }
}