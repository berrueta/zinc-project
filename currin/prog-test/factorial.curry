factorial x |  x == 0 = 1
	    |  x > 0 = x * (factorial (x-1))

cero = factorial 0
uno = factorial 1
dos = factorial 2
tres = factorial 3
