positivo x | x > 0 = "si"
menor30 x | x < 30 = "si"
es1 x | x == 1 = "si"

sayTrue = positivo 2
sayFalse = positivo (-1)
sayTrueAgain = menor30 25
sayFalseAgain = menor30 35
sayTrueTwice = es1 1
sayFalseTwice = es1 2
