module KcFail007 where

type T i = i
newtype M a = Mdc (a ())

class C a where
   foo :: a -> M T -> Bool
