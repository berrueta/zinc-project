module ShouldFail where

class MyEq a where
  (===) :: a -> a -> Bool

class MyFunctor f where
  functorMap :: (a -> b) -> (f a -> f b)

-- incompatible kinds

class (MyFunctor a, MyEq a) => NewClass a
