module ShouldFail where

-- kind error

class MyFunctor f where
  myMap :: (a -> b) -> (f a -> f b)
  myEq :: f -> f -> Bool
