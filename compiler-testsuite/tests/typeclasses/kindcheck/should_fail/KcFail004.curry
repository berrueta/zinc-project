module ShouldFail where

-- kind error

class MyFunctor f where
  myEq :: f -> f -> Bool
  myMap :: (a -> b) -> (f a -> f b)
