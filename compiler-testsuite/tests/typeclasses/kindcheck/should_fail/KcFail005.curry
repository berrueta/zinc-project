module ShouldFail where

class MyEq a where
  myEq :: a -> a -> Bool

class MyFunctor f where
  myMap :: (a -> b) -> (f a -> f b)

-- kind error

instance MyEq Maybe where
  myEq = undefined
