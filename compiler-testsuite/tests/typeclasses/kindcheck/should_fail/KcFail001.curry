module ShouldFail where

class MyEq a where
  (===) :: a -> a -> Bool

class MyFunctor f where
  functorMap :: (a -> b) -> (f a -> f b)

-- incompatible kinds

class (MyEq a, MyFunctor a) => NewClass a
