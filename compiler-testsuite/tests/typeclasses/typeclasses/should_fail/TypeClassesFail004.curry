-- cyclic dependency

module ShouldFail where

class Bar a => Foo a where
  foo :: a -> a

class Foo a => Bar a where
  bar :: a -> a
