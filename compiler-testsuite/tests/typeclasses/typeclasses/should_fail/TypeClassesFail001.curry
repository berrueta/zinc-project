module ShouldFail where

class C a where
  met eval rigid
  -- eval annotations not allowed in type class declaration
