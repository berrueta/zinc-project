-- repeated method

module ShouldFail where

class Foo a where
  foo :: a -> a

class Bar a where
  foo :: a -> a
