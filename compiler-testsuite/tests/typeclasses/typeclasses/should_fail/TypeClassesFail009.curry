-- this is a circular-dependency in Zinc, but not in Haskell

module ShouldCompile where

class Foo a where
  foo :: Bar a => a -> a -> a

class Bar a where
  bar :: Foo a => a -> a -> a
