-- this is a cyclic dependency in Zinc, but not in Haskell

module ShouldCompile where

class Bar a => Foo a where
  foo :: a -> a

class Bar a where
  bar :: Foo a => a -> a
