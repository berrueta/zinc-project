-- collision between method and function

module ShouldFail where

class Foo a where
  foo :: a -> Int

foo :: Int
foo = 10

