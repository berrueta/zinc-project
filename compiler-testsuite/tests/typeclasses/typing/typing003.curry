class MyEq a where
  myEq :: a -> a -> Bool

class MyEq a => MyOrd a where
  myLt :: a -> a -> Bool

class Foo a where
  one :: a -> b -> a
  two :: a -> b -> b

class MyFunctor f where
  myMap :: (a -> b) -> f a -> f b

f1 x = myEq x x
