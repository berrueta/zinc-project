module ScFail002 where

class MyEq a where
  myEq :: a -> a -> Bool

class MyEq Int => MyOrd a where
  myOrd :: a -> a -> Bool