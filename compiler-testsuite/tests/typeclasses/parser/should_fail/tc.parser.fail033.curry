module ShouldFail where

class C a where
  -- infix declarations inside class declarations
  infixr 5 ===
  (===) :: a -> a -> Bool
