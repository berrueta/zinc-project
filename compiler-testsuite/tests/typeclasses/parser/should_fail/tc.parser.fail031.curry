module ShouldFail where

class C a where
  met :: a -> a -> Bool

instance C Int where
  -- pattern declarations not allowed in instance declarations
  met x y = (x>y)
  (Just (Just z)) = undefined

