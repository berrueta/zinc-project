-- data type declarations not allowed in a class declaration

module ShouldFail where

class Foo a where
  type AnotherInt = Int

