-- multiparameter type classes are not allowed (yet)

module ShouldFail where

class Foo a b where
  foo :: a -> a -> Int
  bar :: a -> b -> b

