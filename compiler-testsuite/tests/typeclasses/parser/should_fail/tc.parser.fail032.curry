module ShouldFail where

class C a where
  met :: a -> a -> Bool

instance C Int where
  -- fixity declarations not allowed in instance declarations
  infixl 6 `met`
  met x y = (x>y)
