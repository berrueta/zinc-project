-- anonymous type variables are not allowed in contexts

module ShouldFail where

class Foo a

class Foo _ => Eq a
