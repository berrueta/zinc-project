-- context needs parenthesis

f :: Eq a, Ord b => a -> a -> b -> b -> Bool
f x x' y y' = (x == x') && (y > y')