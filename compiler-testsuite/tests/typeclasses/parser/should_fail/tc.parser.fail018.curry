-- anonymous type variables are not allowed in type class decl

module ShouldFail where

class Bar _ where bar :: a -> a -> Bool
