module ShouldFail where

import Maybe

class MyEq a where
  (===) :: a -> a -> Bool

-- constructor expected
instance MyEq a => MyEq Maybe a where
  (===) = (==)
