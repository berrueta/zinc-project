module ShouldFail where

-- this works fine, but compare with Intffail006.curry

data SomeIdent

class SomeIdent a

f :: SomeIdent
f = undefined
