module ShouldFail where

class C a where
  -- eval annotations not allowed in non-default methods
  met eval rigid
  met :: a -> a -> Bool

instance C Int where
  met x y = (x>y)
