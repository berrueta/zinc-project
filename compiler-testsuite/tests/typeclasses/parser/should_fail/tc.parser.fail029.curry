module ShouldFail where

class C a where
  met :: a -> a -> Bool

instance C Int where
  -- type signatures not allowed in instance declarations
  met :: Int -> Int -> Bool
  met x y = (x>y)
