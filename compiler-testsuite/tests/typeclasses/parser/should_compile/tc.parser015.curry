module ShouldCompile where

class C a where
  met :: a -> a -> Int

instance C Int where
  met eval rigid
  met x y = x + y
