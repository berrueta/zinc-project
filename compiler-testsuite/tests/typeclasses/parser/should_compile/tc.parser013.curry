module ShouldFail where

class MyEq a where
  (===) :: a -> a -> Bool

-- constructor expected
instance (MyEq a, MyEq b) => MyEq (Either a b) where
  (Left x) === (Left y) = (x === y)
  (Right x) === (Right y) = (x === y)
  _ === _ = False
