-- some type classes and instances taken from Jones95

module ShouldCompile where

class MyEq a where
  (===) :: a -> a -> Bool

instance MyEq a => MyEq [a] where
  [] === []  = True
  (x:xs) === (y:ys) = (x === y) && (xs === ys)
  _ === _ = False

instance MyEq Int where
  (===) = (==)

instance MyEq a => MyEq (Maybe a) where
  Nothing === Nothing = True
  Just x  === Just y  = x === y
  _ === _ = False

{------------------------------------------------------------}

data Tree a = Leaf a | Branch (Tree a) (Tree a)

class MyFunctor f where
  functorMap :: (a -> b) -> (f a -> f b)

-- The parser doesn't allow to write the list constructor
-- instance MyFunctor [] where
--   functorMap f []     = []
--   functorMap f (x:xs) = f x : functorMap f xs

--instance MyFunctor Tree where
--  functorMap f (Leaf x    ) = Leaf (f x)
--  functorMap f (Branch x y) = Branch (functorMap f x) (functorMap f y)

instance MyFunctor Maybe where
  functorMap f (Just x)  = Just (f x)
  functorMap f (Nothing) = Nothing

