class MyEq a where
 (===) :: a -> a -> Bool

f :: (MyEq a) => a -> a -> Bool
f x y = x === y
