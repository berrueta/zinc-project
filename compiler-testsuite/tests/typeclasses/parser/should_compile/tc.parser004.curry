class TestClass a where
  met :: a -> Int

f :: TestClass z => z -> z -> Int
f x y = (met x) + (met y)
