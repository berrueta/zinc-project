class MyOrd a

class TestClass a where
  met :: (MyOrd a) => a -> b

f x y = (met x, met y)
