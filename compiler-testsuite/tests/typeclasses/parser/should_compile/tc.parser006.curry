class MyEq a

class (MyEq a') => TestClass a' where
  met :: a' -> (Int,Int)
  bar :: a' -> Int

class TestClass a => FooClass a where
  foo :: a -> (b,b)
