class MyEq a where
  (===) :: a -> a -> Bool

class MyEq a => MyOrd a where
  (>>>) :: a -> a -> Bool

f :: (MyEq a, MyOrd b) => a -> a -> b -> b -> Bool
f x x' y y' = (x === x') && (y >>> y')
