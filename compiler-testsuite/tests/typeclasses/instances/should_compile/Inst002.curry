module Inst002 where

class MyEq a where
  (===) :: a -> a -> Bool

instance MyEq Int where
  (===) = (==)

instance MyEq Char where
  (===) = (==)

-- FIXME: member :: MyEq a => [a] -> [a] -> Bool
member []     y   = False
member (x:xs) y   = (x === y) || member xs y

instance (MyEq a, MyEq b) => MyEq (a,b) where
  (u,v) === (x,y)  = (u === x) && (v === y)

instance MyEq a => MyEq [a] where
  []     === []       = True
  []     === (y:ys)   = False
  (x:xs) === []       = False
  (x:xs) === (y:ys)   = (x === y) && (xs === ys)

data MySet a = MkSet [a]

instance MyEq a => MyEq (MySet a) where
   MkSet xs === MkSet ys  = and (map (member xs) ys) &&
                            and (map (member ys) xs)
