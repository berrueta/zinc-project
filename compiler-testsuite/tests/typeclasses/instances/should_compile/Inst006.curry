module Inst006 where

class A a where
  foo :: a -> Bool

class (A a, A a) => B a where
  bar :: a -> Bool

instance A Char where
  foo x = x /= 'a'

instance B Char where
  bar y = notElem y "curry"

f = foo 'd'