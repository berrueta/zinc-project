module Inst001 where

class MyClass1 a

class MyClass2 a

class (MyClass1 a,MyClass2 a) => MyClass3 a

class MyClass3 a => MyClass4 a

data TC1 = A | B | C

data TC2 a b = TC2 a b

-- some complex instances

instance MyClass1 TC1

instance MyClass2 TC1

instance MyClass3 TC1

instance (MyClass1 b, MyClass1 a) => MyClass1 (TC2 a b)

instance (MyClass2 a, MyClass2 b) => MyClass2 (TC2 a b)

instance (MyClass1 a, MyClass2 a, MyClass1 b, MyClass2 b) => MyClass3 (TC2 a b)