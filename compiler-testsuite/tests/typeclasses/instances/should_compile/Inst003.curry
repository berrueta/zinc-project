module Inst003 where

class A a where
  met :: (A b) => a -> b -> Bool

instance A Char where
  met x y = (x=='a') && (met y y)

instance A Bool where
  met x y = x || (met y y)

instance A a => A [a] where
  met xs y = all (\d -> met d d) xs && met y y

f = met False "dssd"