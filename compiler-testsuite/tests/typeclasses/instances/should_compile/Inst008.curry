module Inst007 where

class C1 a where
  met1 :: a -> b -> Bool

class C1 a => C2 a where
  met2 :: a -> b -> Bool

class (C2 a, C1 a) => C3 a where
  met3 :: a -> b -> Bool

f :: C3 a => a -> Bool
f x = met3 x 'a'
