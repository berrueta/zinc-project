module Inst004 where

class A a where
  foo :: a -> Bool

class B a where
  bar :: a -> Bool

instance (B a, A a) => A [a] where
  foo xs = all (\d -> foo d && bar d) xs

instance A Char where
  foo x = x /= 'a'

instance B Char where
  bar y = notElem y "curry"

f = foo "diego"