module InstFail009 where

class MyEq a where
  (===) :: a -> a -> Bool

-- non-existent type constructor C

instance MyEq C where
  (===) = undefined
