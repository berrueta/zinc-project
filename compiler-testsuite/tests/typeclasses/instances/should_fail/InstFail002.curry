module InstFail002 where

class MyEq a where
  myEq :: a -> a -> Bool

class MyEq a => MyOrd a where
  myGt :: a -> a -> Bool

data TC = A | B | C

-- error: needed instance of MyEq

instance MyOrd TC where
  myGt x y = undefined
