module InstFail001 where

class MyEq a where
  myEq :: a -> a -> Bool

instance MyEq Int where
  myEq = undefined

-- repeated instantiation

instance MyEq Int where
  myEq = undefined
