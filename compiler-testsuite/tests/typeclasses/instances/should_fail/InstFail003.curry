module InstFail003 where

class A a where
  met :: a -> b -> Bool

instance A Bool where
  -- not general enough
  met x y = x && y
