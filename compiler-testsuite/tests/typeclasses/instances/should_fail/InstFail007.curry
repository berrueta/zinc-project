module InstFail007 where

import Maybe

class MyEq a where
  (===) :: a -> a -> Bool

-- non-existent class
instance NoEq a => MyEq (Maybe a) where
