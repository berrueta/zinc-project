module InstFail004 where

class C a where
  m :: a -> a
  n :: a -> a

-- lacks implementation for method n
instance C Char where
  m x = x
