module InstFail008 where

-- non-existent type class MyEq

instance MyEq Int where
  (===) = undefined
