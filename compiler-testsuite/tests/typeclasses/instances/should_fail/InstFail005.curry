module InstFail005 where

class C a where
  m :: a -> a

-- n is not a method of C
instance C Char where
  m x = x
  n x = x