module InstFail010 where

class Foo a

type Ali = Int

instance Foo Ali -- instance of an alias type
