module InstFail006 where

class C a where
  m :: a
  n :: a

-- pattern declarations not allowed at root-level
instance C Char where
  (m,n) = ('a','b')
