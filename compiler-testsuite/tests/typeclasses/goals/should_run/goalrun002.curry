import prelude

type MyShowS = String -> String

class MyShow a where
  showsPrec :: Int -> a -> MyShowS
  showList :: [a] -> MyShowS

stdShowList  :: MyShow a => [a] -> MyShowS
stdShowList eval rigid
stdShowList [] = showString "[]"
stdShowList (x:xs) = showChar '[' . shows x . showTail xs
  where showTail [] = showChar ']'
        showTail (x:xs) = showChar ',' . shows x . showTail xs

show' :: MyShow a => a -> String
show' x = shows x ""

shows :: MyShow a => a -> MyShowS
shows x = showsPrec 0 x

showChar :: Char -> MyShowS 
showChar = (:)

showString :: String -> MyShowS
showString = (++)

showParen :: Bool -> MyShowS -> MyShowS
showParen b x = if b then showChar '(' . x . showChar ')' else x

instance MyShow Bool where
  showsPrec eval rigid
  showsPrec _ False = showString "False"
  showsPrec _ True = showString "True"
  showList = stdShowList

instance MyShow Char where
  showsPrec _ c = showString (prelude.show c)
  showList cs = showString (prelude.show cs)

instance MyShow Int where
  showsPrec _ c = showString (prelude.show c)
  showList = stdShowList

instance MyShow Float where
  showsPrec _ c = showString (prelude.show c)
  showList = stdShowList

instance MyShow a => MyShow (Maybe a) where
  showsPrec _ Nothing = showString "Nothing"
  showsPrec p (Just x) = showParen (p >= 10) (showString "Just " . shows x)
  showList = stdShowList

g1 = show' (Just $ 1+1)
g2 = showsPrec 1 'a' ""
--g3 = showsPrec 1 [] ""

main = g1 ++ g2