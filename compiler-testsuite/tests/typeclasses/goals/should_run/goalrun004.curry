class Enum' a where
  toEnum' :: Int -> a
  fromEnum' :: a -> Int

instance Enum' Int where
  toEnum' = id
  fromEnum' = id

