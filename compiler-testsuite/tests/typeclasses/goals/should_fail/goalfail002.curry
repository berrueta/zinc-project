class Foo a where
  foo :: a -> Bool

instance Foo Int where
  foo x = True

instance Foo Float where
  foo y = False
