class MyClass a where
  myMethod :: a -> a -> String

instance MyClass Char where
  myMethod x y = x:y:[]

f x []    = reverse (myMethod x x)
f x (_:_) = myMethod x x

g = f 'a' []


