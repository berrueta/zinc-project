class Foo a

instance Foo Int

class Bar a where
  method :: Foo a => a -> Bool

instance Bar Int where
  method x = undefined

--f :: (Foo a, Bar a) => a -> Bool
--f x = method x

g :: Bool
g = method 5

-- main = f 10
