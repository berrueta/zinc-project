module main where

class C a where

class D a where
  n  :: C b => a -> b -> String
  n' :: C b => a -> b -> String

class D a => E a where

