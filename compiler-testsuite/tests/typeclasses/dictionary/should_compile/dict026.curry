class Foo a

instance Foo Int

class Bar a where
  method :: Foo a => a -> b -> Bool

instance Bar Int where
  method x y = undefined

-- f :: (Foo b, Bar a) => a -> b -> Bool
f x y = method x y

main = f 10 20
