class Fractional' a where
   quotRem'   :: a -> a -> (a,a)
   quot', rem' :: a -> a -> a

external primitive primQuotRemInt :: Int -> Int -> (Int,Int)

instance Fractional' Int where
   quotRem'  = primQuotRemInt
   quot' n d = q where (q,_) = quotRem' n d
   rem'  n d = r where (_,r) = quotRem' n d