import List

class MyClass a where
  myMethod :: a -> a -> [Char]

instance MyClass Char where
  myMethod x y = x:y:[]

instance MyClass a => MyClass [a] where
  myMethod xs ys = concat (zipWith myMethod xs ys)

-- list comprehension
f x = [myMethod x y | y <- inits x, length (myMethod x y) > 2]

