module ShouldCompile where

-- checking fixity declarations

infixl 7 +++
infixl 3 ***
infixl 5 !!!

(!!!) :: a -> b -> b
x !!! y = y

class C a where
  (+++) :: a -> a -> Int
  (***) :: a -> a -> Int

f :: C a => a -> a -> b -> b
f x y z =  x +++ y  !!! z
--        (x +++ y) !!! z

g :: C a => a -> b -> a -> Int
g x y z =  x ***  y !!! z
--         x *** (y !!! z)