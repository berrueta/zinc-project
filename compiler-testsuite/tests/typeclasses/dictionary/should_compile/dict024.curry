class Foo a where
  foo :: a -> a -> Bool

class Bar a where
  bar :: Foo a => a -> a -> Bool

instance Foo Int where
  foo x y = False

instance Bar Int where
  bar x y = False
