module ShouldCompile where

class C a where
  (===) :: a -> a -> Bool

f x = (g,h x,i x)
    where g = Just (x === x)
          -- no need to pass an extra dict to h
          h y = y === x
          -- but i requires a dictionary variable
          i z = z === z
