class MyClass a where
  myMethod :: a -> a -> [Char]

instance MyClass Char where
  myMethod x y = x:y:[]

instance MyClass a => MyClass [a] where
  myMethod xs ys = concat (zipWith myMethod xs ys)

-- let free
f x = let Y free
      in ((myMethod Y Y) == (myMethod x Y)) =:= True

