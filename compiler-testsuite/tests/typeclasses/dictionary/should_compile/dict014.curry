module dict014 where

class C a where
  met :: a -> a -> Bool

f x y z = do fi <- if (met x y) then readFile "a" else readFile "b"
             let fi2 z' = met z' z'
             return (fi,fi2 z)
