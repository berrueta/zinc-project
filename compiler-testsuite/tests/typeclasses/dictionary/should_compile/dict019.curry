module ShouldCompile where

class C a where
  met :: a -> String

class D a where
  bar1 :: C b => a -> b -> String
  bar2 :: a -> Bool

instance C Char where
  met x = "Foo"

instance C Int where
  met x = "Bar"

instance D Int where
  bar1 x y = "Boo"
  bar2 x = False

f = bar2 10
