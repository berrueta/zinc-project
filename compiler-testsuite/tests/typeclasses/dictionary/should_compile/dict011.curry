class MyClass a where
  myMethod :: a -> a -> [Char]

-- if
f x y = if (myMethod x x) == (myMethod x y)
          then myMethod x x
          else myMethod x y
