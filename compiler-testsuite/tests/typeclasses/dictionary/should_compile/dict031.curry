class F a where
--  method1 :: a -> (a,a)
  method2 :: a -> a

instance F Int where
--  method1 x = undefined
  method2 x = q
            where (q,_) = method1 x

method1 :: F a => a -> (a,a)
method1 x = undefined

--f x = q
--    where (q,_) = method1 x

main = method2 10
