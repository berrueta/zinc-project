chooseOne :: Int -> IO Int
chooseOne max =
	do
	 n <- getLine
	 let valueN  = 0 in
	  if valueN<0 || valueN>max then chooseOne max
        	                     else return valueN 
