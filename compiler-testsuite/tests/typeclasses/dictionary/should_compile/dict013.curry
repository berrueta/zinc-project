module dict013 where

class C a where
  metC :: a -> a -> a

class C a => D a where
  metD :: a -> a -> a

f x = x `metD` (y [])
    where y _ = x `metC` x
