module ShouldCompile where

class C a where
  met :: a -> String

class D a where
  bar :: C b => a -> b -> String

instance C Char where
  met x = "Foo"

instance C Int where
  met x = "Bar"

instance D Int where
  bar x y = "Boo"

f = bar 10 'a'
