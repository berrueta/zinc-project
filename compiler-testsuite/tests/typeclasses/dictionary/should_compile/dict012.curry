class MyClass a where
  myMethod :: a -> a -> [Char]

-- case
f x y = case (myMethod x x) of
          "Haskell" -> myMethod x y
          "Curry"   -> myMethod x x