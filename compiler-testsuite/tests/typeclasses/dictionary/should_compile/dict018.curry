module ShouldCompile where

-- method where the first variable is not the class variable
class C a where
  met :: b -> a -> Bool

instance C Char where
  met x 'a' = undefined

f = met undefined 'b'
