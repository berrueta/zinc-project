module ShouldCompile where

class C a where
  add :: a -> a -> a

instance C Int where
  -- testing evaluation annotations
  add eval rigid
  add x y = x + y

