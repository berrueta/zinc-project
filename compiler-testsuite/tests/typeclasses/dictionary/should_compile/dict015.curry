module ShouldCompile where

class C a where
  met :: a -> a -> Bool

instance C Char where
  met x y = False

f x1 x2 = (y,z)
        where y = met x1 x1
              z = met x2 'a'