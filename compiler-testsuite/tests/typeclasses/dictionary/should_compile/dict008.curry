class MyClass a where
  myMethod :: a -> a -> [Char]

instance MyClass Char where
  myMethod x y = x:y:[]

instance MyClass a => MyClass [a] where
  myMethod xs ys = concat (zipWith myMethod xs ys)

-- let
f x y = let aux i = myMethod x
        in map aux [y,y,y]
