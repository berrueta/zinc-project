module main where

class B a where
  l :: a -> b -> Bool

class C a where
  m :: B b => a -> b -> Bool
  m' :: a -> c -> Char

class D a where
  n :: C b => a -> b -> String
  n' :: C c => a -> c -> Char

class D a => E a where
  o :: a -> String

instance B Int where
  l x y = (x>15)

instance C Int where
  m x y = (x>10) && l x y
  m' x y = undefined

instance D Char where
  n x y = if (m y (14::Int)) then (x : "Foo") else (x : "Bar")
  n' x y = undefined

instance E Char where
  o x = (x:x:x:[])

-- calling an overloaded method (n)

f :: (C w, E z) => z -> w -> String
f y1 y2 = n y1 y2 ++ o y1

g x = f x (10::Int)

main = putStrLn (show (g 'a'))