class MyClass a where
  myMethod :: a -> a -> String

instance MyClass Char where
  myMethod x y = x:y:[]

f xs ys = myMethod (head xs) (head ys)

main = putStrLn (show (f "Curry" "Haskell"))
