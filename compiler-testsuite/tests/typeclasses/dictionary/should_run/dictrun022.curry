-- testing traversers

class A a where
  metA :: a -> Int

class A a => B a where
  metB :: a -> Int

class A a => C a where
  metC :: a -> Int

class (B a, C a) => D a where
  metD :: a -> Int

instance A Int where metA x = x
instance B Int where metB x = (x+1)
instance C Int where metC x = (x+2)
instance D Int where metD x = (x+3)

f x = metA x + metB x + metC x + metD x

main = f 10