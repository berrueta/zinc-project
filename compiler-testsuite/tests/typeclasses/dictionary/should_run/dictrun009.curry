class MyClass a where
  myMethod :: a -> a -> [Char]

instance MyClass Char where
  myMethod x y = x:y:[]

instance MyClass a => MyClass [a] where
  myMethod xs ys = concat (zipWith myMethod xs ys)

f x y = myMethod x y

g x = f x (reverse x)

main = putStrLn (show (g "Curry"))
