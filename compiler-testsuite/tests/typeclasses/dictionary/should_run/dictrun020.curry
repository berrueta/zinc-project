module main where

class A a where
  met :: (A b) => a -> b -> Int

instance A Int where
  met x y = if (x>100) then x else (met (x*2) y)

f :: (A a) => a -> Int
f x = met x x

main = f (2::Int)
