class C a where
  m1 :: [a] -> String
  m2 :: [a] -> String

instance C Int where
  m1 []     = ""
  m1 (x:xs) = (show x) ++ " " ++ (m2 xs)
  m2 []     = ""
  m2 (x:xs) = (show (-x)) ++ " " ++ (m1 xs)

main = m1 [1..10]
