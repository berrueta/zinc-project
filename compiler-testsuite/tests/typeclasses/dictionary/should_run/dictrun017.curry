module main where

class C a where
  toString :: a -> String

instance C Int where
  toString x = "#"

instance C a => C [a] where
  toString [] = ""
  toString (x:xs) = toString x ++ "," ++ toString xs

instance C a => C (Maybe a) where
  toString Nothing  = "Nothing"
  toString (Just x) = "Just " ++ toString x

parens :: C a => a -> String
parens x = "(" ++ toString x ++ ")"

main = parens (Just [1..5])