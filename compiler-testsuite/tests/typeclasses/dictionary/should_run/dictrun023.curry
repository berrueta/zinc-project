restOfList:: [String] -> String -> String
restOfList []     c = c
restOfList (x:xs) c = x ++ comma ++ (restOfList xs c)
  where comma :: String
        comma =  if (xs == []) then "" else ", "

main = restOfList ["Zinc","Is","Not","Curry"] " (ZINC)"
		   	