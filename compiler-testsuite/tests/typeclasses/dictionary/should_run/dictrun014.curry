--mySearch :: Eq a => a -> [a] -> Bool
mySearch x (y:ys) = (x == y) || (mySearch x ys)
mySearch x []     = False

--myGreaterOrEqual :: Ord a => a -> a -> Bool
myGreaterOrEqual x y = (x > y) || (x == y)

f :: Bool
f = mySearch i [1,2,3,6] where i::Int
                               i = 10

g :: [Bool]
g = zipWith myGreaterOrEqual xs ys
    where xs,ys :: [Int]
          xs = [1..5]
          ys = [4,3,1,5,2]

main = putStrLn $ show f ++ " " ++ show g