class MyClass a where
  myMethod :: a -> a -> Bool

instance MyClass Char where
  myMethod x y = x == y

f x y = myMethod x y

main = putStrLn (show (f 'a' 'a'))
