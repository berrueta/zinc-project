class MyClass a where
  myMethod :: a -> a -> Bool

instance MyClass Char where
  myMethod x y = x == y

main = putStrLn (show (myMethod 'a' 'a'))
