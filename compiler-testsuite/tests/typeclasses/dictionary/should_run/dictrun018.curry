module main where

class C a where
  m :: a -> Bool
  m' :: a -> b -> Char

class D a where
  n :: C b => a -> b -> String
  n' :: C c => a -> c -> Char

class D a => E a where
  o :: a -> String

instance C Int where
  m x = (x>10)
  m' x y = undefined

instance D Char where
  n x y = if (m y) then (x : "Foo") else (x : "Bar")
  n' x y = undefined

instance E Char where
  o x = (x:x:x:[])

-- calling an overloaded method (n)

f :: (C w, E z) => z -> w -> String
f y1 y2 = n y1 y2 ++ o y1

g x = f x (10::Int)

main = putStrLn (show (g 'a'))
