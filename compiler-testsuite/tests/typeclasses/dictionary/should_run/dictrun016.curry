module main where

-- testing functors

class MyFunctor f where
  myMap :: (a -> b) -> f a -> f b

instance MyFunctor Maybe where
  myMap f Nothing  = Nothing
  myMap f (Just x) = Just (f x)

main = myMap (+1) (Just 10)
