module Intffail003 where

import qualified Intffail001

-- Intffail001.Mobable doesn't exist

class Intffail001.Mobable a => SomeClass a where
  yetAnotherMethod :: a -> a -> (a,a)

