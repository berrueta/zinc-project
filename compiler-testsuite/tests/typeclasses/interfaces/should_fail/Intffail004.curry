module Intffail004 where

import Intffail001

-- Intffail001.Point2D exists, but it is not a type class

class Point2D a => SomeClass a where
  yetAnotherMethod :: a -> a -> (a,a)

