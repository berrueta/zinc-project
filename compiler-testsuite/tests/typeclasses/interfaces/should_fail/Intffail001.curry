module Intffail001 where

data Point2D = Point Float Float

class Movable a where
  moveTo :: Point2D -> a -> a

data Figure = Circle Point2D Float

data Canvas a = Canvas Point2D a
