module Intffail002 where

import Intffail001

-- same name

class Movable a where
  otherMethod :: a -> a

-- ambiguous

class Movable a => SomeClass a where
  yetAnotherMethod :: a -> a -> (a,a)

