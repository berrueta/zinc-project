module Intffail005 where

import Intffail001

-- Intffail001.Canvas exists, but it is not a type class and 
-- has the wrong arity

class Canvas a => SomeClass a where
  yetAnotherMethod :: a -> a -> (a,a)

