module Intffail010 where

import Intffail001

instance Movable Int where
  moveTo x = id
  -- error: cannot define new functions
  auxFunction = undefined