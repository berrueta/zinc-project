module Intf017 where

class C1 a where
  method1 :: a -> Bool

instance C1 Int where
  method1 x = False

f1 x = method1 x

f1' x = Intf017.method1 x
