module Intf023 where

class MyFunctor f where
  myFmap :: (a -> b) -> f a -> f b

data MyIdentity a

--instance MyFunctor MyIdentity where
--  myFmap f (I x) = I (f x) 