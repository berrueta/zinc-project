module Intf001 where

data Point2D = Point Float Float

class Movable a where
  moveTo :: Point2D -> a -> a

data Figure = Circle Point2D Float

instance Movable Figure where
  moveTo p (Circle _ r) = (Circle p r)

class Movable a => MovableChild a where
  getPos :: a -> Point2D
