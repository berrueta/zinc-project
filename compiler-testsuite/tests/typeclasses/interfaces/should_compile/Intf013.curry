-- some declarations used in other tests

module Intf013 where

class C3 a where
  bar1 :: a -> Bool
  bar2 :: a -> b -> Bool

f3 :: C3 a => a -> b -> Bool
f3 x y = bar1 x {- && bar2 x y -}

