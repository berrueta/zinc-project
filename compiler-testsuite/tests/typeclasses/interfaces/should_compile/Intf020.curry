module Intf20 where

import Intf017 as Intf

instance Intf.C1 Float where method1 = undefined

f1 x = Intf.f1 x
f1' x = Intf.method1 x
