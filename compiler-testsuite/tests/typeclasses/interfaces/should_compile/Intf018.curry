module Intf018(module Intf017,f2,class C2(..)) where

import Intf017

class C1 b => C2 b where
  method2 :: b -> Bool

f2 x = method1 x && method2 x