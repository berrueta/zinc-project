module Intf021 where

-- kind of StateT: * -> (* -> *) -> * -> *
newtype StateT s m a = ST (s -> m (a,s))