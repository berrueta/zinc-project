module Intf004 where

import Intf001

-- classes should not collide

class Movable a where
  otherMethod :: a -> (a,a)
