module Intf011 where

type Al a = Maybe a

class C b where
  -- method with alias type
  met :: Al b -> b

  bar :: d -> b -> b