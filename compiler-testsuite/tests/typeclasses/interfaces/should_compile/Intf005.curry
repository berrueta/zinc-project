module Intf005 where

import qualified Intf001

-- classes should not collide

class Movable a where
  otherMethod :: a -> (a,a)

class Movable a => OtherClass a where
  someMethod :: a -> a

class Intf001.Movable a => YetAnotherClass a where
  yetAnotherMethod :: a -> b

