module Intf002 where

import Intf001

class Movable a => OtherClass a  where
  otherMethod :: a -> (a,a)

-- for other tests

data PickableObject a = AtPoint Point2D a

instance Movable (PickableObject a) where
  moveTo p (AtPoint _ x) = (AtPoint p x)

instance MovableChild (PickableObject a) where
  getPos (AtPoint p _) = p