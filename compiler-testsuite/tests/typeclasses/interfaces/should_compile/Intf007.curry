-- some declarations used in other tests

module Intf007 where

class C1 a where
  met1 :: a -> Bool

class C1 a => C2 a where
  met2 :: a -> Char

f1 :: C1 a => a -> Bool
f1 x = met1 x

f2 :: C2 a => a -> (Bool,Char)
f2 x = (met1 x, met2 x)

class C3 a where
  bar1 :: a -> Bool
  bar2 :: a -> b -> Bool

f3 :: C3 a => a -> b -> Bool
f3 x y = bar1 x && bar2 x y

class C4 a where
  test1 :: a -> _ -> a
  test2 :: a -> _ -> a
  test3 :: a -> b -> a
  test4 :: a -> b -> a

