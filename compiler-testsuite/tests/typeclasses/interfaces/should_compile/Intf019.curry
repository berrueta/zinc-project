module Intf019 where

import Intf018

class C1 c => C3 c where
  method3 :: c -> Bool

class C2 d => C4 d where
  method4 :: d -> Bool

f3 x = method1 x && method2 x && method3 x && method4 x &&
       f1 x && f2 x
