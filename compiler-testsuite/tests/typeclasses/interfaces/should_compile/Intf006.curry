module Intf006 where

import Intf001

-- same name

class Movable a where
  otherMethod :: a -> a

-- without qualification, the following is ambiguous

class Intf001.Movable a => SomeClass a where
  yetAnotherMethod :: a -> a -> (a,a)

