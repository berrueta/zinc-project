module TecFail003 where

class Eq a where
  myEq :: a -> a -> Bool

-- unbound type vars

instance Eq a => Eq b where
  myEq = undefined


