module TecFail009 where

class MyEq a where
  (===) :: a -> a -> Bool

-- variable expected
instance MyEq [Int] where
  (===) = (==)
