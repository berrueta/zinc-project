module TecFail012 where

data MyEither a b = L a | R b

class MyEq a where
  myEq :: a -> a -> Bool

-- invalid type expr
instance MyEq (Either Int Float) where
  myEq = undefined
