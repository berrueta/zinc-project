module ShouldFail where

class MyEq a where
  myEq :: a -> a -> Bool

-- anonymous type variable not allowed

instance MyEq _ where
  myEq x y = False
