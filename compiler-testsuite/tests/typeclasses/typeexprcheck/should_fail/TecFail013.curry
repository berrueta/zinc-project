module TecFail013 where

data MyEither a b = L a | R b

class MyEq a where
  myEq :: a -> a -> Bool

-- invalid type expr
instance MyEq (a b) where
  myEq = undefined
