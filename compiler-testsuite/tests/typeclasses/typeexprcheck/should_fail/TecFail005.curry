module ShouldFail where

class MyEq a where
  myEq :: a -> a -> Bool

instance MyEq Int where
  myEq x y = False

-- anonymous type variable not allowed

instance MyEq a => MyEq [_] where
  myEq x y = False