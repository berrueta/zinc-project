module TecFail008 where

class MyEq a where
  (===) :: a -> a -> Bool

-- constructor expected
instance MyEq a => MyEq a where
  (===) = (==)
