module TecFail002 where

class Eq a where
  myEq :: a -> a -> Bool

-- anon type vars in context

instance Eq _ => Eq [a] where
  myEq = undefined
