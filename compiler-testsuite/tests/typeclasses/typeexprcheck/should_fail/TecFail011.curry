module TecFail011 where

data MyEither a b = L a | R b

class MyEq a where
  myEq :: a -> a -> Bool

-- invalid type expr
instance MyEq (Either (Maybe a) b) where
  myEq = undefined
