module TecFail014 where

data MyEither a b = L a | R b

class MyEq a where
  myEq :: a -> a -> Bool

-- invalid type expr
instance (MyEq (MyEither a a)) => MyEq [a] where
  myEq = undefined
