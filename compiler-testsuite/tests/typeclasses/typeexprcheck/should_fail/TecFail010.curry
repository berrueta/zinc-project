module TecFail010 where

data MyEither a b = L a | R b

class MyEq a where
  myEq :: a -> a -> Bool

-- repeated type var
instance MyEq (Either a a) where
  myEq = undefined
