-- !!! Ambiguity in local declarations

module ShouldSucceed where

class MyEq a where
  (===) :: a -> a -> Bool

instance (MyEq a, MyEq b) => MyEq (a,b) where
  (x1,y1) === (x2,y2) = (x1 === x2) && (y1 === y2)

instance MyEq a => MyEq [a] where
  (x:xs) === (y:ys) = (x === y) && (xs === ys)
  []     === []     = True
  _      === _      = False

type Cp a =  a -> a -> Ordering

m :: MyEq a => Cp a -> [a] -> a
m            _       [x,y,z] =  if x===y then x else z  
     
cpPairs :: Cp [j] -> (a,[j]) -> (a,[j]) -> Ordering
cpPairs    cp        (_,p)      (_,q)   =  cp p q

mp :: (MyEq i,MyEq j) => Cp [j] -> [(i,[j])] -> (i,[j])
mp                       cp        dD          =  
   let  minInRow = m (cpPairs cp)
   in   minInRow dD

{- GHC 3.02 reported

    T.hs:24:
	Ambiguous type variable(s)
	`j' in the constraint `Eq (aYD, [j])'
	    arising from use of `m' at T.hs:24
	In an equation for function `mp':
	    mp cp dD = let minInRow = m (cpPairs cp) in minInRow dD

This was because the ambiguity test in tcSimplify didn't
take account of the type variables free in the environment.

It should compile fine.
-}
