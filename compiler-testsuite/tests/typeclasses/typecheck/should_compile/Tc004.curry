module ShouldSucceed where

-- !!! tests the deduction of contexts.

class MyEq a where
  (===) :: a -> a -> Bool

f :: (MyEq a) => a -> [a]

f x = g x
      where
      g y = if (y === x) then [] else [y]
