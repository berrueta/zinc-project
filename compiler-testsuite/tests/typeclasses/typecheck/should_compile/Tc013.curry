module Tc013 where

class MyEq a where
  (===) :: a -> a -> Bool

-- h :: MyEq a => a -> Bool
h x = g undefined
    where -- g :: (MyEq a) => b -> Bool
          g _ = x === x