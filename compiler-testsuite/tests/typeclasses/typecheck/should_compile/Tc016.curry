module Tc016 where

class MyEq a where
  myEq :: a -> a -> Bool

class MyFunctor f where
  myMap :: (a -> b) -> f a -> f b

comp :: (MyEq (f b), MyFunctor f) => (a -> b) -> f a -> f a -> Bool
comp tf x y = myEq (myMap tf x) (myMap tf y)
