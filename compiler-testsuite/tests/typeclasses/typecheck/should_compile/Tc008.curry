module ShouldSucceed where

data T a = D (B a) | C
data B b = X | Y b

class MyEq a where
  (===) :: a -> a -> Bool
  (/==) :: a -> a -> Bool

instance (MyEq a) => MyEq (T a) where
 (D x) === (D y) = x === y
 C === C = True
 a === b = False

 a /== b = not (a === b)

instance (MyEq b) => MyEq (B b) where
 X === X = True
 (Y a) === (Y b) = a === b
 a === b = False

 a /== b = not (a === b)
