module Tc014 where

class C a where
   met :: a -> b -> Bool

f,g :: C a => a -> Bool
f x = met x 'a'
g x = met x True

-- test if both f and g share the same dictionary instance
h :: C a => a -> Bool
h x = (f x) && (g x)