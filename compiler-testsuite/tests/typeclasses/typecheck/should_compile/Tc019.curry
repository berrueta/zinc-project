module ShouldCompile where

class B a where
  l :: a -> b -> Bool

class C a where
  m :: B b => a -> b -> Bool
  m' :: a -> c -> Char

class D a where
  n :: C b => a -> b -> String
  n' :: C c => a -> c -> Char

class D a => E a where
  o :: a -> String
