-- This program test whether the Curry implementation can
-- deal with recursive types provided that they are given
-- by the user (cf. Section 4.2 of the Curry Report):
--
-- This program is well typed but traditional Hindley/Milner
-- type inferencer cannot derive the types if the type definition
-- of 'g' is omitted:

-- (Adapted from the test rectype)

module Tc001 where
      
class C a where
  met :: [a] -> Bool

f :: C a => [a] -> [a]
f x = if (met x) then fst (g x x) else x

g :: (C a, C b) => [a] -> [b] -> ([a],[b])
g x y = (f x , f y)
