module Tc015 where

class C a where
  met :: a -> a

class D a where
  bar :: a -> a

-- irrelevant context (C a)
f :: (C a, D a) => a -> a
f x = bar x
