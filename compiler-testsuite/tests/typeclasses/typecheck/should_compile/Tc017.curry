-- context reduction should permit this

module ShouldCompile where

class Foo a where
  bar :: Foo a => a -> a -> a
