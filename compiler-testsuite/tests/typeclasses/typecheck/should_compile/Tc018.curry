module TcFail004 where

class C a where
  met :: a -> Bool

-- signature is less general than inferred type
f :: (C a) => a -> a
f x = x
