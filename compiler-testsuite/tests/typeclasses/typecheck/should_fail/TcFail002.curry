class MyClass a where
  myMethod :: a -> [Char]

-- which instance?
f x = ((myMethod x) == (myMethod Y)) =:= True
      where Y free


