module TcFail005 where

class C a where
  met :: a -> a

-- no instance of C for []
f (x:xs) = (met x:met xs)