module TcFail003 where

class C a where
  met :: a -> a -> Bool

f :: a -> b
f x = undefined

-- ambiguous type
-- g :: (C b) => a -> Bool
g x = met (f x) (f x)
