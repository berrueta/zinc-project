module TcFail006 where

class C a where
  m :: a -> Bool

instance C Int where
  m x = (x>10)

instance (C a, C b) => C (Either a b) where
   m (Left x)  = m x
   m (Right y) = m y

f :: C z => z -> Bool
f y = m y

-- unresolved
g  = f (Left 10)
