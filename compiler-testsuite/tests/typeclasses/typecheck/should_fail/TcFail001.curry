class MyClass a where
  myMethod :: a -> [Char]

-- which instance?
f x = let Y free
      in ((myMethod x) == (myMethod Y)) =:= True

