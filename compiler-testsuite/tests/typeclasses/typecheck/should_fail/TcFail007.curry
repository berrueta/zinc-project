module TcFail007 where

class Foo a where
  foo :: a -> Bool

instance Foo Int where
  foo x = True

instance Foo Float where
  foo y = False

goal = let X free in foo X =:= True
