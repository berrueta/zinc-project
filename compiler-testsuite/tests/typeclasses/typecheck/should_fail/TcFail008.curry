-- module TcFail008 where

class Foo a where
  foo :: a -> Bool

instance Foo Int where
  foo x = True

instance Foo Float where
  foo y = False

-- solut :: Foo a => [a -> Success]
solut = solveAll (\x -> foo x =:= True)

main = solut