-- !! check if tc type substitutions really do
-- !! clone (or if not, work around it by cloning
-- !! all binders in first pass of the simplifier).
module ShouldCompile where

class MyEq a

f,g :: MyEq a => (a,b) -> Int
f = g
g = f
