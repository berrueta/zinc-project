module ShouldFail where

-- !!! Using a type constructor as a class name

data Foo a

f :: Foo i => i -> Int
f x = 0
