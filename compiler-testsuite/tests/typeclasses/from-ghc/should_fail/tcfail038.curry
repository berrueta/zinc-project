-- !!! duplicate class-method declarations

module ShouldFail where

class MyEq a where
  (===) :: a -> a -> Bool
  (/==) :: a -> a -> Bool

data NUM = ONE | TWO
instance MyEq NUM where
	a === b = True
	a /== b = False
	a === b = False
	a /== b = True

