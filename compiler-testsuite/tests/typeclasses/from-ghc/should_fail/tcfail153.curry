-- No -fglasgow-exts, so (v::a) means (v:: forall a.a)

module ShouldCompile where

class MyEq a where
  (===) :: a -> a -> Bool

data T a = T a

instance MyEq (T a) where
  (===) x y = let v :: a
                  v = undefined
	      in  v

