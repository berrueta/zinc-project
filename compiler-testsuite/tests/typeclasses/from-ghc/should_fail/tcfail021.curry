-- !!! Illegally giving methods in a pattern binding (for no v good reason...)

module ShouldFail where

data Foo = MkFoo Int

class MyEq a where
   (===) :: a -> a -> Bool
   (/==) :: a -> a -> Bool

instance MyEq Foo where
    ((===), (/==)) = (\x -> \y -> True, \x -> \y -> False)
