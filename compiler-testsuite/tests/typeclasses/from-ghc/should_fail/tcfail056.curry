module ShouldFail where

data Foo = MkFoo Bool

class MyEq a where
  (===) :: a -> a -> Bool

class MyOrd a where
  (<==) :: a -> a -> Bool

instance MyEq Foo where
    (MkFoo x) === (MkFoo y) = x == y

instance MyEq Foo where
    -- forgot to type "Ord" above
    (MkFoo x) <== (MkFoo y) = x <= y

