module ShouldCompile where

--- execute IO actions before and after an IO action
--- <CODE>bracket before after m</CODE> executes the actions in the
--- order before, m, and after. The IO action after is executed even
--- if an exception occurs in m. The result of before is passed to
--- m and after.
bracket' :: IO a -> (a -> IO b) -> (a -> IO c) -> IO c
bracket' before after m =
  do
    x <- before
    r <- catch (m x) (\ioe -> after x >> ioError ioe)
    after x
    return r

