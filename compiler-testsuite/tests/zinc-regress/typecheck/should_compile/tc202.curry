module main where

data Foo a = FooD a

-- f :: main.Foo a -> main.Foo a -> Bool
f (FooD x) (FooD y) = (x == y)

