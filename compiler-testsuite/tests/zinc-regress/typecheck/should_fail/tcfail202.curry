-- Test case for a name-clashing bug discovered by Jose E. Labra

--concat :: [a] -> [a] -> [a]
concat [] xs = xs
