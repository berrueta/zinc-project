module ShouldFail where

-- Explicit signature is more general than inferred type

f :: _ -> _
f x = x
