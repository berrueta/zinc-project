module ShouldFail where

-- Explicit signature is more general than inferred type

g = f
  where f :: _ -> _
        f x = x
