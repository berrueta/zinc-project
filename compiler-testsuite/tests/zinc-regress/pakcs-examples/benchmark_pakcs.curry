-- Benchmarks for PAKCS

import System

-- Standard Prolog benchmark: naive reverse:

append []     ys = ys
append (x:xs) ys = x : append xs ys

rev []     = []
rev (x:xs) = append (rev xs) [x]

-- start naive reverse benchmark with a list of n elements:
nrev n = let l,r free in
         l =:= [1..n] &> evalSpace (rev l) =:= r
-- LIPS = (n+1)*(n+2)/2/exec.time

-- Result on a Sun Ultra-10 (chevalblanc, with Sicstus/Fast code):
-- 2.5 MLIPS for (nrev 1000)

-- Result on a Linux-PC PIII/650Mhz (with Sicstus/Emulated code):
-- 0.94 MLIPS for (nrev 1000)

-- Result on a Linux-PC AMD Athlon/900Mhz (with Sicstus/Emulated code):
-- 1.12 MLIPS for (nrev 1000)

-- Result on a Linux-PC AMD Athlon/1.300Mhz (with Sicstus/Emulated code):
-- 1.43 MLIPS for (nrev 1000)

-- Result on a Linux-PC AMD Athlon XP 2600+/2.000Mhz (petrus, with Sicstus/Emulated code):
-- 2.95 MLIPS for (nrev 1000)


-- as nrev but double evaluation
nrev2 n = let l,r1,r2 free in
          l =:= [1..n] &> evalSpace (rev l, rev l) =:= (r1,r2)

-- as nrev2 but with test equality instead of unification:
nrev3 n = let l free in
          l =:= [1..n] &> evalSpace (rev l == rev l) =:= True
