-- implementation of a fair search strategy to enumerate all solutions
-- of a constraint (combines encapsulated search and committed choice)

all_parallel :: (a -> Success) -> [a -> Success]
all_parallel g = eval1 (try g)
  where
    eval1 [] = []
    eval1 [s] = [s]
    eval1 (g1:g2:gs) = foldr merge [] (map all_parallel (g1:g2:gs))


-------------------------------
-- indeterministic merge:

merge :: [a] -> [a] -> [a]
merge eval choice
merge [] l2    = l2
merge l1 []    = l1
merge (e:r) l2 = e : merge r l2
merge l1 (e:r) = e : merge l1 r


-------------------------------
-- "test" has exactly two solutions which cannot be computed with
-- strict left-to-right or right-to-left search strategy since
-- the first and last alternative results in infinite non-branching
-- derivations. The two solutions are found by the strategy
-- "all_parallel"

test = f
test = 1
test = 2
test = 1+f

f = 1+f


-------------------------------
-- Examples:

goal1 = map unpack (take 2 (all_parallel (\x -> x=:=test))) -- yields [1,2]
goal2 = map unpack (take 2 (solveAll (\x -> x=:=test)))     -- does not terminate
goal3 = map unpack (take 2 (bfs (\x -> x=:=test)))          -- does not terminate
goal4 = map unpack (take 2 (all_id 1 (\x -> x=:=test)))     -- does not terminate
goal5 = map unpack (one (\x -> x=:=test))                   -- yields [1] or [2]




-------------------------------


-- breadth-first search:
bfs g = trygoals [g]
  where
    trygoals [] = [] 
    trygoals (g:gs) = splitgoals (map try (g:gs)) []

    splitgoals []               ugs = trygoals ugs
    splitgoals ([]:gs)          ugs = splitgoals gs ugs
    splitgoals ([g]:gs)         ugs = g:(splitgoals gs ugs)
    splitgoals ((g1:g2:g3s):gs) ugs = splitgoals gs (ugs++g1:g2:g3s)


-- iterative deepening search:
all_id n g  = depthLoop n n (toDepthN n n g) False
  where
    depthLoop n m [] True          = depthLoop (n+m) m
                                               (toDepthN (n+m) m g) False
    depthLoop _ _ [] False         = []
    depthLoop n m ([]:gs) recomp   = depthLoop n m gs recomp
    depthLoop n m ([g1]:gs) recomp = g1:(depthLoop n m gs recomp)
    depthLoop n m ((_:_:_):gs) _   = depthLoop n m gs True

    toDepthN n m g  = collect (try g)
      where
        collect []   = [[]]
        collect [g]  = if n>m then [[]]
                              else [[g]]
        collect(g1:g2:gs) = if n==1
                            then [g1:g2:gs]
                            else concat (map (toDepthN (n-1) m) (g1:g2:gs))


-- depth-first search with depth bound:
allbounded n g = if n>0 then evalall (try g) else []
      where
        evalall []         = []
        evalall [g]        = [g]
        evalall (g1:g2:gs) = concat (map (allbounded (n-1)) (g1:g2:gs))


