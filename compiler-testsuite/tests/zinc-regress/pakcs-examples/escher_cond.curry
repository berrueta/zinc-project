-- Module "conditional" from the Escher report:

data Alpha = A | B | C | D
           deriving (Eq,Show)

membercheck :: (Alpha , [Alpha]) -> Bool
membercheck eval rigid

membercheck(_,[])   = False
membercheck(x,y:z) = if x==y then True else membercheck(x,z)


-- goals:
goal1 = membercheck(B,[A,B])   -->  true
goal2 = membercheck(C,[A,B])   -->  false
goal3 = membercheck(x,[A,B]) where x free
  -->  suspend

main = do putStrLn (show goal1)
          putStrLn (show goal2)
          putStrLn (show goal3)