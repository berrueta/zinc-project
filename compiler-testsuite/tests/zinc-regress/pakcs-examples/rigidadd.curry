-- First example of POPL'97 paper: evaluate addition by residuation:

data Nat = O | S Nat
         deriving (Show)

add :: Nat -> Nat -> Nat
-- default: add eval rigid

add O     n = n
add (S m) n = S(add m n )


isNat :: Nat -> Success

isNat O     = success
isNat (S n) = isNat n 



goal = let x free in add x O =:= S O & isNat x

-- in Zinc, we must add the signature to prevent the
-- unresolved overloading (Show) error
main = browseList (solveAll (\_ -> goal) :: [Nat -> Success])