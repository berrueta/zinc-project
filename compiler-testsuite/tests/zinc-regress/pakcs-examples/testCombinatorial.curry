-- Some tests for module Combinatorial
--
-- To run all tests automatically by the currytest tool, use the command:
-- "currytest testCombinatorial"

import Combinatorial
import Integer
import List
import SetRBT
import Assertion -- to assert test cases

------------------------------------------------------------------

testPermute = AssertTrue "permute" (testEmpty && testSingle && testTriple)
  where allPermutations x = findall (\y->permute x =:= y)
        testEmpty = allPermutations [] == [[]]
        testSingle = allPermutations [1] == [[1]]
        testTriple =  equalSets given computed &&
                      all (\x -> length x == 3) computed &&
                      length computed == factorial 3                 
          where given = [[1,2,3],[2,1,3],[2,3,1],[1,3,2],[3,1,2],[3,2,1]]
                computed = allPermutations [1,2,3]

-- Efficiency is not a goal of this test harness
equalSets x y = isSubsetOf x y && isSubsetOf y x
isSubsetOf [] _ = True
isSubsetOf (x:xs) y = elem x y && isSubsetOf xs y

------------------------------------------------------------------

testSubset = AssertTrue "subset" (length output == pow 2 (length input) && 
                                  all (`elem` output) [[],[2],[1,3],input])
    where input = [0,1,2,3]
          output = allSubsets input

------------------------------------------------------------------

testSizedSubset = AssertEqual "sizedSubset"
                              (length all4) (binomial (length base) size)
    where base = [0,1,2,3,4,5,6,7]
          size = 4
          all4 = findall (\x -> sizedSubset size base =:= x)


------------------------------------------------------------------

testSplitSet = AssertTrue "splitSet"
                          (checkLength && checkDistinct && checkMembers)
    where input = [1,2,3,4] 
          output = findall (\x -> splitSet input =:= x)
          checkLength = length output == pow 2 (length input)
          checkDistinct = nub output == output
          checkMembers = elem ([2,3],[1,4]) output &&
                         elem ([1,2,3],[4]) output &&
                         elem ([],[1,2,3,4]) output

------------------------------------------------------------------

testPartition = AssertTrue "partition"
                    (checkDistinct && checkMemberLength && checkMemberContent)
    where input = [1,2,3,4] 
          output = findall (\x -> partition input =:= x)
          checkDistinct = nub output == output
          checkMemberLength = all (\x -> length x == length input)
                                  (map concat output)
          checkMemberContent = all (\x -> sortRBT (<) x == input)
                                   (map concat output)

------------------------------------------------------------------

