-- test the fair search strategy "one" of the Curry Report:

-- test function, result only computable with a fair strategy:
test = f
test = 1
test = 2
test = 1+f

f = 1+f

goal y = (head (one (\x->test=:=x))) y
