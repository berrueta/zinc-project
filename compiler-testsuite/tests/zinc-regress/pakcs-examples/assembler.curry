-- a one-pass assembler:
-- translate arbitrary jump-instructions into machine code
-- use logical variables to resolve forward jump addresses

module main where


-- we consider only two assembler instructions: jumps and labels
data Instruction = Jump LabelId | Label LabelId

-- we consider only a few number of label identifiers (could also be strings):
data LabelId = L0 | L1 | L2 | L3 | L4 | L5 | L6 | L7 | L8 | L9

-- new addition in Zinc

instance Eq LabelId where
  L0 == L0 = True
  L1 == L1 = True
  L2 == L2 = True
  L3 == L3 = True
  L4 == L4 = True
  L5 == L5 = True
  L6 == L6 = True
  L7 == L7 = True
  L8 == L8 = True
  L9 == L9 = True
  L0 == L1 = False
  L0 == L2 = False
  L0 == L3 = False
  L0 == L4 = False
  L0 == L5 = False
  L0 == L6 = False
  L0 == L7 = False
  L0 == L8 = False
  L0 == L9 = False
  L1 == L0 = False
  L1 == L2 = False
  L1 == L3 = False
  L1 == L4 = False
  L1 == L5 = False
  L1 == L6 = False
  L1 == L7 = False
  L1 == L8 = False
  L1 == L9 = False
  L2 == L0 = False
  L2 == L1 = False
  L2 == L3 = False
  L2 == L4 = False
  L2 == L5 = False
  L2 == L6 = False
  L2 == L7 = False
  L2 == L8 = False
  L2 == L9 = False
  L3 == L0 = False
  L3 == L1 = False
  L3 == L2 = False
  L3 == L4 = False
  L3 == L5 = False
  L3 == L6 = False
  L3 == L7 = False
  L3 == L8 = False
  L3 == L9 = False
  L4 == L0 = False
  L4 == L1 = False
  L4 == L2 = False
  L4 == L3 = False
  L4 == L5 = False
  L4 == L6 = False
  L4 == L7 = False
  L4 == L8 = False
  L4 == L9 = False
  L5 == L0 = False
  L5 == L1 = False
  L5 == L2 = False
  L5 == L3 = False
  L5 == L4 = False
  L5 == L6 = False
  L5 == L7 = False
  L5 == L8 = False
  L5 == L9 = False
  L6 == L0 = False
  L6 == L1 = False
  L6 == L2 = False
  L6 == L3 = False
  L6 == L4 = False
  L6 == L5 = False
  L6 == L7 = False
  L6 == L8 = False
  L6 == L9 = False
  L7 == L0 = False
  L7 == L1 = False
  L7 == L2 = False
  L7 == L3 = False
  L7 == L4 = False
  L7 == L5 = False
  L7 == L6 = False
  L7 == L8 = False
  L7 == L9 = False
  L8 == L0 = False
  L8 == L1 = False
  L8 == L2 = False
  L8 == L3 = False
  L8 == L4 = False
  L8 == L5 = False
  L8 == L6 = False
  L8 == L7 = False
  L8 == L9 = False
  L9 == L0 = False
  L9 == L1 = False
  L9 == L2 = False
  L9 == L3 = False
  L9 == L4 = False
  L9 == L5 = False
  L9 == L6 = False
  L9 == L7 = False
  L9 == L8 = False

-- implementation of the symbol table:
-- list of pairs consisting of labelids and codeaddresses
type SymTab = [(LabelId,Int)]

assembler :: [Instruction] -> SymTab -> Int -> [Int]

assembler [] _ _ = []
assembler (Jump l : ins) st a 
  | lookupST l st label st1  = 9:label:assembler ins st1 (a+2)
  where label,st1 free
assembler (Label l : ins) st a 
  | st1 =:= insertST l a st   = assembler ins st1 a
  where st1 free

-- insert an address of a labelid in a symboltable:
insertST l a []  = [(l,a)]
insertST l a ((l1,a1):st) | (l==l1)=:=True & a=:=a1 = (l1,a1):st
insertST l a ((l1,a1):st) | (l==l1)=:=False         = (l1,a1):(insertST l a st)

-- lookup an address of a labelid in a symboltable:
lookupST l [] a st1  = st1=:=[(l,a)]
lookupST l ((l1,a1):st) a st1 =
  if l==l1 then a=:=a1 & st1=:=(l1,a1):st
           else let st2 free in lookupST l st a st2 & st1=:=(l1,a1):st2


-- Goal:

goal = assembler [Label L0, Jump L1, Jump L0, Label L1] [] 0

-----> Result: [9,4,9,0]

main = browseList (solveAll (\x -> x=:=goal))