-- Examples for the application of Curry's choice construct:

-- non-deterministic lazy merge:

merge :: [a] -> [a] -> [a]
merge eval choice
merge [] l2    = l2
merge l1 []    = l1
merge (e:r) l2 = e : merge r l2
merge l1 (e:r) = e : merge l1 r


goal1 l = let x,xs,y,ys free in
          merge x y =:= l & x=:=1:xs & y=:=2:ys & ys=:=[3] & xs=:=[4]

goal2 l = let x,xs,y,ys free in
          merge x y =:= l & y=:=2:ys & x=:=1:xs & xs=:=[4] & ys=:=[3]


ones = 1:ones
twos = 2:twos

m12 = merge ones twos

goal = take 20 m12
