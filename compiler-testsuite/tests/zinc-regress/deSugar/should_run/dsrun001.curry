{-	Check that list comprehensions can be written
	in do-notation. This actually broke 2.02, with
	a pattern match failure in dsListComp!
-}

-- Cyc specific: s/Main/main
module main where

main = putStrLn (show theList)
theList = do x <- [1..3]
             y <- [1..3]
             return (x,y)

