{- Tests let-expressions in do-statments -}

-- Cyc specific: s/Main/main
module main( main ) where

foo = do
  	putStr "a"
	let x = "b" in putStr x
	putStr "c"

main = do
	 putStr "a"
	 foo
	 let x = "b" in putStrLn x

