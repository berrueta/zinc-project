module ShouldFail where

data MT a = M a

data OT a b = O (b a)

type PT b = OT Int b
type QT a = OT a MT

g' :: PT Int
g' = error "a"
