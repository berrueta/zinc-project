module ShouldFail where

data MT a = M a

data NT a b = N (a b)

f :: NT Char IO
f = error "a"
