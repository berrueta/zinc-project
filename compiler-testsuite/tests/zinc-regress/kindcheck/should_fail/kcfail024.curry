module ShouldFail where

f = g
    where g = let h :: Int Int
                  h = undefined
              in h