module ShouldFail where

data MT a = M a

data OT a b = O (b a)

g :: OT MT Char
g = error "a"

