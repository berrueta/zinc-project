module ShouldFail where

data MT a = M a

data OT a b = O (b a)

type PT b = OT b Int
