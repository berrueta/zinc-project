data Tree a = Leaf a | Branch (Tree a) (Tree a)

count :: Tree -> Int
count (Leaf _) = 1
count (Branch b1 b2) = (count b1) + (count b2)