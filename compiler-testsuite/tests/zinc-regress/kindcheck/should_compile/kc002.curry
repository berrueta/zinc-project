data Subst a b = Subst a b

type StringToInt = Subst String Int

type Map a b = [(a,b)]

type ConcreteMap b = Map String b
