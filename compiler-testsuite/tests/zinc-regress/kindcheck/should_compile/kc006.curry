-- this one was failing in Zinc 0.1.1pre

module ShouldCompile where

data Expr e = One e | Many [e]
newtype PExpr a = InPE (Expr (PExpr a), Int)
