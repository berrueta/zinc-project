module ShouldCompile where

data MT a = M a

data OT a b = O (b a)

type QT a = OT a MT
