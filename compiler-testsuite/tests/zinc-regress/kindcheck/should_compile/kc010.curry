module ShouldCompile where

data MT a = M a

data NT a b = N (a b)

f' :: NT MT (MT Int)
f' = N $ M $ M 1
