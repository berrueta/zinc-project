module ShouldCompile where

data NT a b = N (a b)

f :: NT IO Char
f = N $ return 'a'
