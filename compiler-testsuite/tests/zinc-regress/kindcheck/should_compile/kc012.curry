module ShouldCompile where

data MT a = M a

data OT a b = O (b a)

g :: OT Char MT
g = O $ M 'a'
