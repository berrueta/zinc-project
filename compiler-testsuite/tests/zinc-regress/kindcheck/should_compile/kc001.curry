data Tree a = Leaf a | Branch (Tree a) (Tree a)

type IntTree = Tree Int

f :: Tree a -> [a]
f (Leaf x)       = [x]
f (Branch b1 b2) = (f b1) ++ (f b2)

max :: IntTree -> Int
max (Leaf x) = x
max (Branch _ b2) = max b2

readTree :: IO (Tree Char)
readTree = return (Leaf 'a')

readTree' = readTree