module ShouldCompile where

data OT a b = O (b a)

type PT b = OT Int b

