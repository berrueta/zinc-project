-- !! signature bugs exposed by Sigbjorn Finne
--
module ShouldFail where

--More fun can be had if we change the signature slightly

type Bob a = a

type Flarp2 a = Bob (b,b)
