module ShouldCompile where

-- repeated type var on the rhs

data T a b = C a a | D a b
