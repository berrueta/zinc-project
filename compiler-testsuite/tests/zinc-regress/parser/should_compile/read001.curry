-- !!! import qualified Prelude should leave (), [] etc in scope

module ShouldCompile where

-- Curry specific: s/Prelude/prelude
import qualified prelude

f :: prelude.IO ()
f = prelude.return ()
