-- !!! Special Ids and ops

-- The special ids 'as', 'qualified' and 'hiding' should be 
-- OK in both qualified and unqualified form.
-- Ditto special ops

module ShouldCompile where
-- Curry specific: s/Prelude/prelude
import prelude hiding ( (-) )

as	  = ShouldCompile.as
hiding	  = ShouldCompile.hiding
qualified = ShouldCompile.qualified
x!y	  = x ShouldCompile.! y
x-y	  = x ShouldCompile.- y
