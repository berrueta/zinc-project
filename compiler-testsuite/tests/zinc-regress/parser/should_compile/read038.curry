module Read038 where

data IntAndFloat = Int /\ Float

getInt (x /\ _) = Left x
getFloat (_ /\ y) = Right y
