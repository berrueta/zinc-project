-- !!! Checking that empty declarations are permitted.
module ShouldCompile where

-- Curry specific: does not support (yet) type classes
--class Foo a where

--class Foz a

x = 2 where 
y = 3

--instance Foo Int where

-- Cyc specific: does not support this layout
--f = f where g = g where
type T = Int
