module Read038 where

data BinTree a = Leaf
               | a /\ Bool

depth Leaf = 0
depth (x /\ y) = undefined
