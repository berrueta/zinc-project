newtype T = C Bool
          deriving (Show, Bounded)

main = putStrLn (show x) >> putStrLn (show y)
     where x,y :: T
           x = minBound
           y = maxBound
