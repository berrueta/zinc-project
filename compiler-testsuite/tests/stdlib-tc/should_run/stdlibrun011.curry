data Demo1 = One | Two | Three | Four
           deriving (Show, Bounded)

data Demo2 = I | II | III | IV
           deriving (Show, Bounded)

data CompDemo = DC Demo1 Demo2
              deriving (Show, Bounded)

main = putStrLn (show a ++ "\n" ++ show b)
     where a,b :: CompDemo
           a = minBound
           b = maxBound
