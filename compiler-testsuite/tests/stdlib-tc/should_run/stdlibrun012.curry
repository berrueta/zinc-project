data Demo = One | Two | Tree | Four
          deriving (Show, Enum)

main = putStrLn (show (pred Four)) >>
       putStrLn (show (succ One)) >>
       putStrLn (show (fromEnum Two)) >>
       putStrLn (show x)
     where x :: Demo
           x = toEnum 1
