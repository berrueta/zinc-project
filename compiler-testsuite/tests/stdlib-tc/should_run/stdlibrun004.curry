data BinTree a = Leaf a
               | Branch (BinTree a) (BinTree a)
               deriving (Eq)

main = show (Leaf 'B' == Leaf 'B') ++
       show (Branch (Leaf 10) (Leaf 11) == Branch (Leaf 10) (Leaf 20))
