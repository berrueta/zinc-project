data BinTree a = Leaf a
               | Branch (BinTree a) (BinTree a)
               deriving (Ord,Eq)

main = show (Leaf 'B' < Leaf 'D') ++
       show (Leaf 'B' < Leaf 'A') ++
       show (Branch (Leaf 10) (Leaf 11) < Branch (Leaf 10) (Leaf 20))
