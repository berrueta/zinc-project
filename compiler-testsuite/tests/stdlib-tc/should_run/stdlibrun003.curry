data Color = Red | Green | Blue
           deriving (Eq)

main = show (Red == Red) ++ show (Red == Green)
