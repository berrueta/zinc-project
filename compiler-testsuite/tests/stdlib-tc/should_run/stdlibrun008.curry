data Foo a = NothingFoo
           | JustFoo a
           deriving (Show)

main = show (JustFoo "abba")
