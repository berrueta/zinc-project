data BinTree a = Leaf a
               | Branch (BinTree a) (BinTree a)
               deriving (Show)


tree1 = Branch (Leaf 10) (Leaf 20)

tree2 = Branch (Branch (Leaf 'a') (Leaf 'b')) (Leaf 'c')

main = putStrLn (show (tree1) ++ "\n" ++ show (tree2))