data BinTree a = Leaf
               | Branch a (BinTree a) (BinTree a)
               deriving (Show)

inorder :: BinTree a -> [a]
inorder (Leaf) = []
inorder (Branch x sl sr) = inorder sl ++ (x : inorder sr)

preorder :: BinTree a -> [a]
preorder (Leaf) = []
preorder (Branch x sl sr) = (x : preorder sl) ++ (preorder sr)

main = browseList (solveAll (\X -> preorder X =:= [1..3]))
