data Demo = One | Two | Three | Four
          deriving (Show, Bounded)

main = show a ++ show b
     where a,b :: Demo
           a = minBound
           b = maxBound
