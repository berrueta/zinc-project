%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{KindErrors.lhs}
\codesection{Kind errors}
This module containts the error messages related to the kind inference
process or the kind checking.
\begin{lstlisting}

> module KindErrors where
> import TypeExpr
> import Kind
> import Ident
> import Pretty
> import CurryPP

> kindMismatch :: TypeExpr -> Kind -> Kind -> String
> kindMismatch tyexpr expectedKind inferredKind
>   | expectedKO /= inferredKO = show $
>       what <+> ppTypeExpr 0 tyexpr <+>
>       text "expects" <+> (text $ arguments inferredKO) <+>
>       text "but is applied to" <+> (text $ show expectedKO)
>   | otherwise = genericKindError "type constructor" tyexpr leftmost
>   where inferredKO = kindOrder inferredKind
>         expectedKO = kindOrder expectedKind
>         leftmost = leftmostTypeExpr tyexpr
>         what = if isTypeExprConstructor tyexpr then text "Type constructor"
>                                               else text "Type expression"
>         genericKindError what tyexpr subTyexpr = show $ vcat
>           [text "Kind error of" <+> text what <+> ppTypeExpr 0 subTyexpr,
>            text "In type expression" <+> ppTypeExpr 0 tyexpr,
>            text "Inferred kind:" <+> ppKind inferredKind,
>            text "Expected kind:" <+> ppKind expectedKind]


> wrongArity :: QualIdent -> Int -> Int -> TypeExpr -> String
> wrongArity tc arity argc tyexpr =
>   "Type constructor " ++ qualName tc ++ " expects " ++ arguments arity ++
>   " but it has " ++ show argc ++ " in type expression " ++ show doc
>   where doc = ppTypeExpr 0 tyexpr

> invalidKind :: Ident -> Kind -> String
> invalidKind id k = "Kind of " ++ name id ++
>                    " is invalid: " ++ (show $ ppKind k)

> unknownKind :: Ident -> String
> unknownKind id = "Unknown kind of " ++ name id

> finalType :: TypeExpr -> String
> finalType tyexpr = "Type expression " ++ show (ppTypeExpr 0 tyexpr) ++
>	             " can not have any arguments"

\end{lstlisting}
Auxiliary functions.
\begin{lstlisting}

> arguments :: Int -> String
> arguments 0 = "no arguments"
> arguments 1 = "1 argument"
> arguments n = show n ++ " arguments"


\end{lstlisting}
