% $Id: Error.lhs 1300 2004-09-03 18:08:47Z berrueta $
%
% Copyright (c) 2003, Wolfgang Lux
% Copyright (c) 2003, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Error.lhs}
\codesection{Errors}\label{sec:error}
The \texttt{Error} type is used for describing the result of a
computation that can fail. In contrast to the standard \texttt{Maybe}
type, its \texttt{Error} case provides for an error message that
describes the failure.
\begin{lstlisting}

> module Error where
> import Monad
> import Position

> data Error a = Ok a | Error String deriving (Eq,Ord,Show)

> instance Functor Error where
>   fmap f (Ok x) = Ok (f x)
>   fmap f (Error e) = Error e

> instance Monad Error where
>   return x = Ok x
>   fail s = Error s
>   Ok x >>= f = f x
>   Error e >>= _ = Error e

> ok :: Error a -> a
> ok (Ok x) = x
> ok (Error e) = error e

> okM :: Monad m => Error a -> m a
> okM (Ok x) = return x
> okM (Error e) = fail e

> emap :: (String -> String) -> Error a -> Error a
> emap _ (Ok x) = Ok x
> emap f (Error e) = Error (f e)

\end{lstlisting}
\codesubsection{Error handling}
\begin{lstlisting}

> errorAt :: Position -> String -> a
> errorAt p msg = error ("\n" ++ show p ++ ": " ++ msg)

> internalError :: String -> a
> internalError what = error ("internal error: " ++ what)

\end{lstlisting}
