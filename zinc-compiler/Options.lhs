% $Id: Options.lhs 1495 2004-12-06 18:09:55Z berrueta $
%
% Copyright (c) 2001-2003, Wolfgang Lux
% Copyright (c) 2003-2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Options.lhs}
\codesection{Compiler options}
\begin{lstlisting}

> module Options where
> import GetOpt

\end{lstlisting}
A record of type \texttt{Options} is used to gather the settings of
all compiler options.
\begin{lstlisting}

> data Options =
>   Options {
>     importPath :: [FilePath],         -- directories for searching imports
>     output :: Maybe FilePath,         -- name of output file
>     goal :: Maybe (Maybe String),     -- goal to be evaluated
>     typeIt :: Maybe String,           -- goal to be typed
>     noInterface :: Bool,              -- do not create an interface file
>     splitCode :: Bool,                -- split object code
>     debug :: Bool,                    -- add debugging transformation
>     trusted :: Bool,                  -- trusted module for debugging
>     dump :: [Dump]                    -- dumps
>   }
>   deriving Show

> defaultOptions =
>   Options{
>     importPath = [],
>     output = Nothing,
>     goal = Nothing,
>     typeIt = Nothing,
>     noInterface = False,
>     splitCode = False,
>     debug = False,
>     trusted = False,
>     dump = []
>   }

> data Dump =
>     DumpRenamed                       -- dump source after renaming
>   | DumpTypes                         -- dump types after typechecking
>   | DumpDesugared                     -- dump source after desugaring
>   | DumpSimplified                    -- dump source after simplification
>   | DumpLifted                        -- dump source after lambda-lifting
>   | DumpIL                            -- dump IL code after translation
>   | DumpTransformed                   -- dump transformed code
>   | DumpNormalized                    -- dump IL code after normalization
>   | DumpCam                           -- dump abstract machine code
>   | DumpPEnv                          -- dump priorities environment
>   | DumpTypeConstructorsEnv           -- dump type constructors environment
>   | DumpKindEnv                       -- dump kind environment
>   | DumpTypeClassesEnv                -- dump type classes environment
>   | DumpInstancesEnv                  -- dump instances environment
>   | DumpModule1                       -- dump module (phase 1)
>   | DumpModule2                       -- dump module (phase 2)
>   | DumpModule3                       -- dump module (phase 3)
>   | DumpModule4                       -- dump module (phase 4)
>   | DumpModule5                       -- dump module (phase 5)
>   | DumpModule6                       -- dump module (phase 6)
>   | DumpModule7                       -- dump module (phase 7)
>   | DumpGenerated                     -- dump generated code
>   deriving (Eq,Bounded,Enum,Show)

\end{lstlisting}
The \texttt{Option} type maps every command line switch on a data
constructor. This is needed in order to use the \texttt{GetOpt}
library.
\begin{lstlisting}

> data Option =
>     Help | Version
>   | ImportPath FilePath | Output FilePath
>   | Eval (Maybe String) | Type String
>   | SplitCode | NoInterface | Debug | Trusted | Dump [Dump]
>   deriving (Eq,Show)

\end{lstlisting}
The global variable \texttt{options} defines all options which are
recognized by the compiler.
\begin{lstlisting}

> options = [
>     Option "i" ["import-dir"] (ReqArg ImportPath "DIR")
>            "search for imports in DIR",
>     Option "e" ["eval"] (OptArg Eval "GOAL")
>            "generate code to evaluate GOAL",
>     Option "t" ["type"] (ReqArg Type "GOAL")
>            "print type of GOAL",
>     Option "o" ["output"] (ReqArg Output "FILE")
>            "write code to FILE",
>     Option "" ["no-icurry"] (NoArg NoInterface)
>            "do not create an interface file",
>     Option "" ["split-code"] (NoArg SplitCode)
>            "emit one C file for each function",
>     Option "g" ["debug"] (NoArg Debug)
>            "transform code for debugging",
>     Option "" ["trusted"] (NoArg Trusted)
>            "trust this module (if compiled with -g/--debug)",
>     Option "" ["dump-all"] (NoArg (Dump [minBound..maxBound]))
>            "dump everything",
>     Option "" ["dump-renamed"] (NoArg (Dump [DumpRenamed]))
>            "dump source code after renaming",
>     Option "" ["dump-types"] (NoArg (Dump [DumpTypes]))
>            "dump types after type-checking",
>     Option "" ["dump-desugared"] (NoArg (Dump [DumpDesugared]))
>            "dump source code after desugaring",
>     Option "" ["dump-simplified"] (NoArg (Dump [DumpSimplified]))
>            "dump source code after simplification",
>     Option "" ["dump-lifted"] (NoArg (Dump [DumpLifted]))
>            "dump source code after lambda-lifting",
>     Option "" ["dump-il"] (NoArg (Dump [DumpIL]))
>            "dump intermediate language before lifting",
>     Option "" ["dump-transformed"] (NoArg (Dump [DumpTransformed]))
>            "dump IL code after debugging transformation",
>     Option "" ["dump-normalized"] (NoArg (Dump [DumpNormalized]))
>            "dump IL code after normalization",
>     Option "" ["dump-cam"] (NoArg (Dump [DumpCam]))
>            "dump abstract machine code",
>     Option "" ["dump-penv"] (NoArg (Dump [DumpPEnv]))
>            "dump priorities environment",
>     Option "" ["dump-type-constructors"] (NoArg (Dump [DumpTypeConstructorsEnv]))
>            "dump type constructors environment",
>     Option "" ["dump-kind"] (NoArg (Dump [DumpKindEnv]))
>            "dump kind environment",
>     Option "" ["dump-type-classes"] (NoArg (Dump [DumpTypeClassesEnv]))
>            "dump type classes environment",
>     Option "" ["dump-instances"] (NoArg (Dump [DumpInstancesEnv]))
>            "dump instances environment",
>     Option "" ["dump-module-1"] (NoArg (Dump [DumpModule1]))
>            "dump module (phase 1)",
>     Option "" ["dump-module-2"] (NoArg (Dump [DumpModule2]))
>            "dump module (phase 2)",
>     Option "" ["dump-module-3"] (NoArg (Dump [DumpModule3]))
>            "dump module (phase 3)",
>     Option "" ["dump-module-4"] (NoArg (Dump [DumpModule4]))
>            "dump module (phase 4)",
>     Option "" ["dump-module-5"] (NoArg (Dump [DumpModule5]))
>            "dump module (phase 5)",
>     Option "" ["dump-module-6"] (NoArg (Dump [DumpModule6]))
>            "dump module (phase 6)",
>     Option "" ["dump-module-7"] (NoArg (Dump [DumpModule7]))
>            "dump module (phase 7)",
>     Option "" ["dump-generated"] (NoArg (Dump [DumpGenerated]))
>            "dump generated code",
>     Option "?h" ["help"] (NoArg Help)
>            "display this help and exit",
>     Option "" ["version"] (NoArg Version)
>            "display version info and exit"
>   ]

\end{lstlisting}
The function \texttt{selectOption} applies an \texttt{Option} to an
\texttt{Options} record. Note that there is no case for
\texttt{Help}. If the user asks for help, the compiler will simply
print its usage message and terminate.
\begin{lstlisting}

> selectOption :: Option -> Options -> Options
> selectOption (ImportPath dir) opts =
>   opts{ importPath = dir : importPath opts }
> selectOption (Output file) opts = opts{ output = Just file }
> selectOption (Eval goal) opts = opts{ goal = Just goal }
> selectOption (Type goal) opts = opts{ typeIt = Just goal }
> selectOption NoInterface opts = opts{ noInterface = True }
> selectOption SplitCode opts = opts{ splitCode = True }
> selectOption Debug opts = opts{ debug = True }
> selectOption Trusted opts = opts{ trusted = True }
> selectOption (Dump ds) opts = opts{ dump = ds ++ dump opts }

\end{lstlisting}
