% $Id: RenameEnv.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2003-2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{RenameEnv.lhs}
\codesection{Rename environment}

A nested environment is used for recording information about the data
constructors and variables in the module. For every data constructor
its arity is saved. This is used for checking that all constructor
applications in patterns are saturated. For local variables the
environment records the new name of the variable after renaming. No
further information is needed for global variables.
\begin{lstlisting}

> module RenameEnv where
> import NestEnv
> import ValueEnv
> import Base
> import Ident
> import Types
> import Utils

> type RenameEnv = NestEnv RenameInfo
> data RenameInfo = Constr Int       -- data constructor (with arity)
>                 | GlobalVar        -- global variable
>                 | LocalVar Ident   -- local variable
>                 deriving (Eq,Show)

> renameInfo :: ValueInfo -> RenameInfo
> renameInfo (DataConstructor _ (ForAllExist _ _ ty)) = Constr (arrowArity ty)
> renameInfo (NewtypeConstructor _ _) = Constr 1
> renameInfo _ = GlobalVar

> bindFunc :: ModuleIdent -> PIdent -> RenameEnv -> RenameEnv
> bindFunc m (PIdent p f) = bindGlobal m f GlobalVar

> bindVar :: PIdent -> RenameEnv -> RenameEnv
> bindVar (PIdent p v) env
>   | v' == anonId = env
>   | otherwise    = bindLocal v' (LocalVar v) env
>   where v' = unRenameIdent v

> bindGlobal :: ModuleIdent -> Ident -> RenameInfo -> RenameEnv -> RenameEnv
> bindGlobal m c r = bindNestEnv c r . qualBindNestEnv (qualifyWith m c) r

> bindLocal :: Ident -> RenameInfo -> RenameEnv -> RenameEnv
> bindLocal f r = bindNestEnv f r

> lookupVar :: Ident -> RenameEnv -> [RenameInfo]
> lookupVar v env = lookupNestEnv v env ++! lookupTupleConstr v

> qualLookupVar :: QualIdent -> RenameEnv -> [RenameInfo]
> qualLookupVar v env =
>   qualLookupNestEnv v env ++! lookupTupleConstr (unqualify v)

> lookupTupleConstr :: Ident -> [RenameInfo]
> lookupTupleConstr v
>   | isTupleId v = [Constr (tupleArity v)]
>   | otherwise = []

\end{lstlisting}
