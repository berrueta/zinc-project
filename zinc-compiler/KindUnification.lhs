% $Id: KindUnification.lhs 970 2004-08-08 12:05:32Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{KindUnification.lhs}
\codesection{Kind unification}
The unification uses Robinson's algorithm (cf., e.g., Chap.~9
of~\cite{PeytonJones87:Book}).

This module is very similar to TypeUnification.
\begin{lstlisting}

> module KindUnification(unify,unifyKinds) where
> import Position
> import Ident
> import Kind
> import KindInferenceMonad
> import KindSubst
> import Combined
> import Error(errorAt)
> import Monad
> import TypeExpr
> import KindErrors

\end{lstlisting}
General kind unification.
\begin{lstlisting}

> unify :: Position -> TypeExpr -> Kind -> Kind -> KiState ()
> unify p ty k1 k2 =
>   liftSt $
>   do theta <- fetchSt
>      let k1' = subst theta k1
>      let k2' = subst theta k2
>      let ur  = unifyKinds k1' k2'
>      maybe (errorAt p (kindMismatch ty k1' k2'))
>            (updateSt_ . compose) ur

\end{lstlisting}
Unification of Kinds:
\begin{lstlisting}

> unifyKinds :: Kind -> Kind -> Maybe KindSubst
> unifyKinds Star Star = Just idSubst
> unifyKinds (KFun k1 k2) (KFun k1' k2') =
>   unifyKinds k2 k2' >>= unifyKindsTheta k1 k1'
>   where unifyKindsTheta :: Kind -> Kind -> KindSubst -> Maybe KindSubst
>         unifyKindsTheta k1 k2 theta =
>           fmap (compose theta) $ unifyKinds (subst theta k1) (subst theta k2)
> unifyKinds (KVar i) k2 = kindVarBind i k2
> unifyKinds k1 (KVar i) = kindVarBind i k1
> unifyKinds k1 k2 = Nothing

> kindVarBind :: Ident -> Kind -> Maybe KindSubst
> kindVarBind i k2
>   | k2 == (KVar i)         = Just idSubst
>   | i `elem` (kindVars k2) = Nothing
>   | otherwise              = Just (bindSubst i k2 idSubst)

\end{lstlisting}
Error messages.
