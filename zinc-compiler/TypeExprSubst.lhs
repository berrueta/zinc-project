% $Id: TypeExprSubst.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeExprSubst.lhs}
\codesection{Substitution of type expressions}
This module implements substitutions on type expressions. It is similar
to the module TypeSubst.
\begin{lstlisting}

> module TypeExprSubst(module TypeExprSubst,
>                      idSubst,bindSubst,compose,substToList,restrictSubstTo) where
> import Subst
> import TypeExpr
> import CurrySyntax
> import Ident
> import Error(internalError)
> import CurrySyntaxUtils(tvUsedBy)

\end{lstlisting}
An specialized (Haskell) type for type expression substitutions. It
is a map of type expr variables into another type expressions.
\begin{lstlisting}

> type TypeExprSubst = Subst Ident TypeExpr

\end{lstlisting}
An specialized (Haskell) type class for type expression substitutions:
It substitutes type expression variables, not type constructors.
\begin{lstlisting}

> class SubstTypeExpr a where
>   substTypeExpr :: TypeExprSubst -> a -> a

\end{lstlisting}
Variable management in type expressions:
\begin{lstlisting}

> bindTypeExprVar :: Ident -> TypeExpr -> TypeExprSubst -> TypeExprSubst
> bindTypeExprVar id tyexpr
>   | id `elem` (tvUsedBy tyexpr) =
>       internalError $ "bindTypeExprVar: occurs check: " ++
>          show id ++ " in " ++ show tyexpr
>   | otherwise = compose (bindSubst id tyexpr idSubst)

> substTypeExprVar :: TypeExprSubst -> Ident -> TypeExpr
> substTypeExprVar = substVar' TypeExprVariable substTypeExpr

\end{lstlisting}
Instantiation of SubstTypeExpr for type expressions.
\begin{lstlisting}

> instance SubstTypeExpr TypeExpr where
>   substTypeExpr sigma ty@(TypeExprConstructor qi) = ty
>   substTypeExpr sigma (TypeExprVariable i) =
>      substTypeExprVar sigma i
>   substTypeExpr sigma (TypeExprApplication ty1 ty2) =
>      TypeExprApplication (substTypeExpr sigma ty1) (substTypeExpr sigma ty2)

> instance (SubstTypeExpr t) => SubstTypeExpr [t] where
>   substTypeExpr sigma = map (substTypeExpr sigma)

\end{lstlisting}
Types expressions with context:
\begin{lstlisting}

> instance SubstTypeExpr TypeExprContext where
>   substTypeExpr sigma (TypeExprContext classConstr) =
>      (TypeExprContext (map (substTypeExpr sigma) classConstr))

> instance SubstTypeExpr TypeExprClassConstraint where
>   substTypeExpr sigma (TypeExprClassConstraint tc tv) =
>      (TypeExprClassConstraint tc (substTypeExpr sigma tv))

> instance SubstTypeExpr TypeExprWithContext where
>   substTypeExpr sigma (TypeExprWithContext ctx tyexpr) =
>      (TypeExprWithContext (substTypeExpr sigma ctx) (substTypeExpr sigma tyexpr))

\end{lstlisting}
Declarations.
\begin{lstlisting}

> instance SubstTypeExpr Decl where
>   substTypeExpr sigma (TypeClassDecl p ctx tc tv decls) =
>      (TypeClassDecl p (substTypeExpr sigma ctx) tc
>                       (substTypeExprVariable sigma tv)
>                       (substTypeExpr sigma decls))
>   substTypeExpr sigma (InstanceDecl p ctx tc tyexpr decls) =
>      (InstanceDecl p (substTypeExpr sigma ctx) tc
>                    (substTypeExpr sigma tyexpr)
>                    (substTypeExpr sigma decls))
>   substTypeExpr sigma (DataDecl p tyconId varIds constrs derivedTC) =
>      (DataDecl p tyconId (substTypeExprVariables sigma varIds)
>                          (substTypeExpr sigma constrs) derivedTC)
>   substTypeExpr sigma (NewtypeDecl p tyconId varIds constrs derivedTC) =
>      (NewtypeDecl p tyconId (substTypeExprVariables sigma varIds)
>                             (substTypeExpr sigma constrs) derivedTC)
>   substTypeExpr sigma (TypeDecl p tyconId varIds tyexpr) =
>      (TypeDecl p tyconId (substTypeExprVariables sigma varIds)
>                          (substTypeExpr sigma tyexpr))
>   substTypeExpr sigma (TypeSig p ids qtyexpr) =
>      (TypeSig p ids (substTypeExpr sigma qtyexpr))
>   substTypeExpr sigma (FunctionDecl p id eqs) =
>      (FunctionDecl p id (substTypeExpr sigma eqs))
>   substTypeExpr sigma (ExternalDecl p cc s id tyexpr) =
>      (ExternalDecl p cc s id (substTypeExpr sigma tyexpr))
>   substTypeExpr sigma (PatternDecl p constr rhs) =
>      (PatternDecl p constr (substTypeExpr sigma rhs))
>   substTypeExpr sigma decl@(ImportDecl _ _ _ _ _) = decl
>   substTypeExpr sigma decl@(InfixDecl _ _ _ _)    = decl
>   substTypeExpr sigma decl@(EvalAnnot _ _ _)      = decl
>   substTypeExpr sigma decl@(ExtraVariables _ _)   = decl

> instance SubstTypeExpr ConstrDecl where
>   substTypeExpr sigma (ConstrDecl p ids i tyexprs) =
>      ConstrDecl p ids i (substTypeExpr sigma tyexprs)
>   substTypeExpr sigma (ConOpDecl p i tyexpr1 op tyexpr2) =
>      ConOpDecl p i (substTypeExpr sigma tyexpr1) op
>                    (substTypeExpr sigma tyexpr2)

> instance SubstTypeExpr NewConstrDecl where
>   substTypeExpr sigma (NewConstrDecl p ids i tyexpr) =
>     NewConstrDecl p ids i (substTypeExpr sigma tyexpr)

> instance SubstTypeExpr Equation where
>   substTypeExpr sigma (Equation p constr rhs) =
>      (Equation p constr (substTypeExpr sigma rhs))

> instance SubstTypeExpr Rhs where
>   substTypeExpr sigma (SimpleRhs p expr decls) =
>      (SimpleRhs p (substTypeExpr sigma expr) (substTypeExpr sigma decls))
>   substTypeExpr sigma (GuardedRhs condExprs decls) =
>      (GuardedRhs (substTypeExpr sigma condExprs)
>                  (substTypeExpr sigma decls))

> instance SubstTypeExpr CondExpr where
>   substTypeExpr sigma (CondExpr p expr1 expr2) =
>      (CondExpr p (substTypeExpr sigma expr1) (substTypeExpr sigma expr2))

> instance SubstTypeExpr Expression where
>   substTypeExpr sigma e@(Literal _) = e
>   substTypeExpr sigma e@(Variable _) = e
>   substTypeExpr sigma e@(Constructor _) = e
>   substTypeExpr sigma e@(Paren expr) =
>      (Paren (substTypeExpr sigma expr))
>   substTypeExpr sigma e@(Typed expr tyexpr) =
>      (Typed (substTypeExpr sigma expr) (substTypeExpr sigma tyexpr))
>   substTypeExpr sigma e@(Tuple exprs) =
>      (Tuple (substTypeExpr sigma exprs))
>   substTypeExpr sigma e@(List exprs) =
>      (List (substTypeExpr sigma exprs))
>   substTypeExpr sigma e@(ListCompr expr stats) =
>      (ListCompr (substTypeExpr sigma expr) (substTypeExpr sigma stats))
>   substTypeExpr sigma e@(EnumFrom expr) =
>      (EnumFrom (substTypeExpr sigma expr))
>   substTypeExpr sigma e@(EnumFromThen expr1 expr2) =
>      (EnumFromThen (substTypeExpr sigma expr1) (substTypeExpr sigma expr2))
>   substTypeExpr sigma e@(EnumFromTo expr1 expr2) =
>      (EnumFromTo (substTypeExpr sigma expr1) (substTypeExpr sigma expr2))
>   substTypeExpr sigma e@(EnumFromThenTo expr1 expr2 expr3) =
>      (EnumFromThenTo (substTypeExpr sigma expr1)
>                      (substTypeExpr sigma expr2)
>                      (substTypeExpr sigma expr3))
>   substTypeExpr sigma e@(UnaryMinus id expr) =
>      (UnaryMinus id (substTypeExpr sigma expr))
>   substTypeExpr sigma e@(Apply expr1 expr2) =
>      (Apply (substTypeExpr sigma expr1) (substTypeExpr sigma expr2))
>   substTypeExpr sigma e@(InfixApply expr1 op expr2) =
>      (InfixApply (substTypeExpr sigma expr1) op (substTypeExpr sigma expr2))
>   substTypeExpr sigma e@(LeftSection expr op) =
>      (LeftSection (substTypeExpr sigma expr) op)
>   substTypeExpr sigma e@(RightSection op expr) =
>      (RightSection op (substTypeExpr sigma expr))
>   substTypeExpr sigma e@(Lambda constrs expr) =
>      (Lambda constrs (substTypeExpr sigma expr))
>   substTypeExpr sigma e@(Let decls expr) =
>      (Let (substTypeExpr sigma decls) (substTypeExpr sigma expr))
>   substTypeExpr sigma e@(Do stats expr) =
>      (Do (substTypeExpr sigma stats) (substTypeExpr sigma expr))
>   substTypeExpr sigma e@(IfThenElse expr1 expr2 expr3) =
>      (IfThenElse (substTypeExpr sigma expr1)
>                  (substTypeExpr sigma expr2)
>                  (substTypeExpr sigma expr3))
>   substTypeExpr sigma e@(Case expr alts) =
>      (Case (substTypeExpr sigma expr) (substTypeExpr sigma alts))

> instance SubstTypeExpr Statement where
>   substTypeExpr sigma (StmtExpr expr) =
>      (StmtExpr (substTypeExpr sigma expr))
>   substTypeExpr sigma (StmtDecl decls) =
>      (StmtDecl (substTypeExpr sigma decls))
>   substTypeExpr sigma (StmtBind constr expr) =
>      (StmtBind constr (substTypeExpr sigma expr))

> instance SubstTypeExpr Alt where
>   substTypeExpr sigma (Alt p constr rhs) =
>      (Alt p constr (substTypeExpr sigma rhs))

> instance SubstTypeExpr Goal where
>   substTypeExpr sigma (Goal p expr decls) =
>      (Goal p (substTypeExpr sigma expr) (substTypeExpr sigma decls))

\end{lstlisting}
Auxiliary functions.
\begin{lstlisting}

> substTypeExprVariable :: TypeExprSubst -> Ident -> Ident
> substTypeExprVariable sigma id =
>   case substTypeExprVar sigma id of
>     (TypeExprVariable id') -> id'
>     x -> internalError ("substTypeExprVariable: " ++ show id)

> substTypeExprVariables :: TypeExprSubst -> [Ident] -> [Ident]
> substTypeExprVariables sigma = map (substTypeExprVariable sigma)

\end{lstlisting}
