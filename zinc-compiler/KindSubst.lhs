% $Id: KindSubst.lhs 970 2004-08-08 12:05:32Z berrueta $
%
% Copyright (c) 2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{KindSubst.lhs}
\codesection{Kind substitutions}
This module implements substitutions on kinds.
\begin{lstlisting}

> module KindSubst(module KindSubst, idSubst,bindSubst,compose) where
> import Kind
> import KindEnv
> import Ident
> import TopEnv
> import Maybe
> import List
> import Subst

> type KindSubst = Subst Ident Kind

> class SubstKind a where
>   subst :: KindSubst -> a -> a

> bindVar :: Ident -> Kind -> KindSubst -> KindSubst
> bindVar kv kind = compose (bindSubst kv kind idSubst)

> substVar :: KindSubst -> Ident -> Kind
> substVar = substVar' KVar subst

> instance SubstKind Kind where
>   subst sigma Star = Star
>   subst sigma (KFun k1 k2) = KFun (subst sigma k1) (subst sigma k2)
>   subst sigma (KVar kv) = substVar sigma kv

> instance SubstKind a => SubstKind [a] where
>   subst sigma = map (subst sigma)

> instance SubstKind KindInfo where
>   subst theta (KindInfo tc k) = KindInfo tc (subst theta k)

> instance SubstKind a => SubstKind (TopEnv a) where
>   subst = fmap . subst

\end{lstlisting}
