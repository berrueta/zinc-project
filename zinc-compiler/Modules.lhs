% $Id: Modules.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2004, Wolfgang Lux
% Copyright (c) 2003-2005, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Modules.lhs}
\codesection{Modules}
This module controls the compilation of modules.
\begin{lstlisting}

> module Modules(compileModule,compileGoal,typeGoal) where
> import CurrySyntax
> import ModuleEnv
> import ValueEnv
> import PEnv
> import TypeConstructorEnv
> import Ident
> import Position
> import TypeTrans(ppType,ppTypeWithContext)
> import PredefTypes
> import Types
> import TypeTrans
> import Unlit(unlit)
> import CurryParser(parseSource,parseInterface,parseGoal)
> import SyntaxCheck(syntaxCheck,syntaxCheckGoal)
> import PrecCheck(precCheck,precCheckGoal)
> import TypeCheck(typeCheck,typeCheckGoal)
> import IntfCheck(intfCheck,intfEquiv)
> import Imports(importInterface,importInterfaceIntf,importUnifyData)
> import Exports(exportInterface)
> import IntfExpansion(expandInterface)
> import Eval(evalEnv,evalEnvGoal)
> import Qual(qual,qualGoal)
> import Desugar(desugar,desugarGoal)
> import Simplify(simplify)
> import Lift(lift)
> import qualified IL
> import ILTrans(ilTrans,ilTransIntf)
> import ILLift(liftProg)
> import DTransform(dTransform,dAddMain)
> import ILCompile(camCompile,camCompileData,fun)
> import qualified CamPP(ppModule)
> import CGen(genMain,genEntry,genModule,genSplitModule)
> import CCode(CFile,mergeCFile)
> import CPretty(ppCFile)
> import CurryPP(ppModule,ppInterface,ppIDecl,ppGoal,ppBlock)
> import qualified ILPP(ppModule)
> import Options(Options(..),Dump(..))
> import PathUtils
> import List
> import IO
> import Maybe
> import Monad
> import Pretty
> import Error
> import Env
> import TopEnv
> import DualTopEnv
> import Typing
> import TypeExprRenaming(typeExprRenaming)
> import KindInference(kindInference,kindInferenceGoal)
> import KindEnv
> import TypeClassEnv
> import TypeExprDisambiguate(disambiguateDecls,disambiguateIDecls,disambiguateGoal)
> import TypeClassBinding(bindTypeClasses)
> import InstanceBinding(bindInstances)
> import TypeBinding(bindTypes)
> import DataConstrBinding(bindLocalDataConstrs)
> import TypeExprCheck(checkTypeExprDecls,checkTypeExprGoal)
> import InstanceEnv
> import DictGeneration(genDict)
> import DictBinding(bindDict)
> import DictTransEnv(dictTransValueEnv)
> import DictTransDecls(dictTransDecls,dictTransGoal)
> import MethodBinding(bindMethods)
> import TypeExpr
> import TypeInstGen(gen)
> import Set
> import DerivedGen(genDerived)

\end{lstlisting}
The \texttt{compile} function handles the compilation of a
module. First, the function will load the prelude (except when the
prelude itself is compiled). Then, the module is parsed and all
imported modules are loaded as well. After loading the modules, the
environments will be setup from the imports and within these
environments syntax checking and type checking is done. Finally,
C code is generated for the module and the module's interface is
updated if necessary.
\begin{lstlisting}

> compileModule :: Options -> FilePath -> IO ()
> compileModule opts fn =
>   do
>     -- compile the module
>     (intf,ccode,dumps,tcEnv) <- doCompileModule (splitCode opts) (debug opts)
>                                                 (trusted opts) (importPath opts) fn
>     -- update the module interface
>     unless (noInterface opts) (updateInterface fn tcEnv intf)
>     -- generate the dumps
>     mapM_ (doDump opts) dumps
>     -- write the output code
>     writeCode (output opts) fn ccode

> doCompileModule :: Bool -> Bool -> Bool -> [FilePath] -> FilePath
>                 -> IO (Interface,Either CFile [CFile],[(Dump,Doc)],TCEnv)
> doCompileModule split debug trusted paths fn =
>   do
>     m <- liftM (parseModule fn) (readFile fn)
>     mEnv <- loadInterfaces paths m
>     let (tyEnv,m',intf,checkDumps,tcEnv) = checkModule mEnv m
>         (ccode,transDumps) = transModule split debug trusted mEnv tyEnv m'
>         ccode' = compileDefaultGoal debug mEnv intf
>     return (intf,merge ccode ccode',checkDumps ++ transDumps,tcEnv)
>   where merge ccode = maybe ccode (merge' ccode)
>         merge' (Left cf1) = Left . mergeCFile cf1
>         merge' (Right cfs) = Right . (cfs ++) . return

> parseModule :: FilePath -> String -> Module
> parseModule fn = importPrelude fn . ok . parseSource fn . unlitLiterate fn

> loadInterfaces :: [FilePath] -> Module -> IO ModuleEnv
> loadInterfaces paths (Module m _ ds) =
>   foldM (loadInterface paths [m]) emptyEnv
>         [(p,m) | ImportDecl p m _ _ _ <- ds]

\end{lstlisting}
The function \texttt{checkModule} does almost all the tasks
of the front-end. It checks and transforms the declarations.
\begin{lstlisting}

> checkModule :: ModuleEnv -> Module -> (ValueEnv,Module,Interface,[(Dump,Doc)],TCEnv)
> checkModule mEnv (Module m es ds) =
>   (finalTyEnv,modul,intf,dumps,finalTcEnv)
>   where -- partition over declarations
>         (impDs,topDs) = partition isImportDecl ds
>         -- import modules
>         (pEnv,tcEnv,typeClassEnv,instEnv,kEnv,tyEnv) = importModules mEnv impDs
>         -- generate derived instances and disambiguate type expressions
>         disTopDs = disambiguateDecls m tcEnv
>                  $ genDerived topDs
>         -- check type expressions and rename the type variables
>         disTopDs' = typeExprRenaming
>                   $ checkTypeExprDecls m tcEnv typeClassEnv disTopDs
>         -- kind inference
>         (topDs',kEnv') = kindInference m disTopDs' kEnv
>         -- bind type classes
>         typeClassEnv' = bindTypeClasses m topDs' typeClassEnv
>         -- bind instances
>         instEnv' = bindInstances m topDs' typeClassEnv' instEnv
>         -- bind imported dictionaries
>         (tcEnvF,tyEnvF) = bindDict typeClassEnv' instEnv' (tcEnv,tyEnv)
>         -- bind local types
>         tcEnvF'  = bindTypes m topDs' tcEnvF
>         -- local dictionary generation
>         genDs    = genDict m typeClassEnv' instEnv' tcEnvF'
>         -- bind dictionary types
>         tcEnv'   = bindTypes m genDs tcEnvF'
>         -- bind data constructors and methods
>         tyEnv'   = bindMethods          typeClassEnv' tcEnv'
>                  $ bindLocalDataConstrs m tcEnv' tyEnvF
>         -- syntax check
>         (topDsM,key) = syntaxCheck m tyEnv' (topDs' ++ genDs)
>         -- prec check
>         (pEnv' ,topDsM') = precCheck m pEnv topDsM
>         -- first type check
>         tyEnvI   = typeCheck m typeClassEnv' instEnv' tcEnv' tyEnv' topDsM'
>         -- dictionary transformation of declarations and expressions
>         topDsT   = dictTransDecls typeClassEnv' instEnv' tcEnv' tyEnvI key topDsM'
>         -- dictionary transformation of the type environment
>         tyEnvT   = dictTransValueEnv typeClassEnv' tyEnv'
>         -- second type check
>         tyEnvII  = typeCheck m noTypeClassEnv noInstanceEnv tcEnv' tyEnvT topDsT
>         -- qualification of data expressions
>         qualTopDs = qual tyEnvII topDsT
>         -- concat module declarations
>         ds'      = impDs ++ qualTopDs
>         -- expand interface
>         es'      = expandInterface m es ds' typeClassEnv' tcEnv' tyEnvI
>         -- recompute the environments
>         (exportEnvs,backendEnvs) =
>           recomputeEnvs mEnv (pEnv',tcEnv',typeClassEnv',
>                               instEnv',kEnv',tyEnvI,tyEnvII)
>         (finalPEnv,finalTcEnv,finalTypeClassEnv,
>          finalInstEnv,finalKEnv,finalTyEnv) =
>            backendEnvs
>         -- export the interface
>         intf     = exportInterface m es' exportEnvs
>         -- the final module
>         modul    = Module m es' ds'
>         -- dumps
>         dumps = [
>             (DumpModule1,   ppModule (Module m es (impDs ++ topDs))),
>             (DumpModule2,   ppModule (Module m es (impDs ++ disTopDs))),
>             (DumpModule3,   ppModule (Module m es (impDs ++ disTopDs'))),
>             (DumpModule4,   ppModule (Module m es (impDs ++ topDsM))),
>             (DumpModule5,   ppModule (Module m es (impDs ++ topDsM'))),
>             (DumpModule6,   ppModule (Module m es (impDs ++ topDsT))),
>             (DumpModule7,   ppModule (Module m es ds')),
>             (DumpGenerated, ppBlock  genDs),
>             (DumpPEnv,               ppTopEnv     finalPEnv        ),
>             (DumpTypeConstructorsEnv,ppTopEnv     finalTcEnv       ),
>             (DumpKindEnv,            ppTopEnv     finalKEnv        ),
>             (DumpTypeClassesEnv,     ppTopEnv     finalTypeClassEnv),
>             (DumpInstancesEnv,       ppDualTopEnv finalInstEnv     )
>           ]

> transModule :: Bool -> Bool -> Bool -> ModuleEnv -> ValueEnv -> Module
>             -> (Either CFile [CFile],[(Dump,Doc)])
> transModule split debug trusted mEnv tyEnv (Module m es ds) = (ccode,dumps)
>   where topDs = filter (not . isImportDecl) ds
>         -- bind evaluation annotations
>         evEnv = evalEnv topDs
>         -- desugar syntax tree
>         (desugared,tyEnv') = desugar tyEnv (Module m es topDs)
>         -- simplify syntax tree
>         (simplified,tyEnv'') = simplify tyEnv' evEnv desugared
>         -- lift expressions
>         (lifted,tyEnv''',evEnv') = lift tyEnv'' evEnv simplified
>         -- transform into IL
>         il = ilTrans tyEnv''' evEnv' lifted
>         -- transform IL for debugging
>         ilDbg = if debug then dTransform trusted il else il
>         -- lift complex code into global functions
>         ilNormal = liftProg ilDbg
>         -- transform into abstract machine language
>         cam = camCompile ilNormal
>         imports = camCompileData (ilImports mEnv il)
>         -- transform into C
>         ccode
>           | split = Right (genSplitModule imports cam)
>           | otherwise = Left (genModule imports cam)
>         dumps = [
>             (DumpRenamed,    ppModule (Module m es ds)),
>             (DumpTypes,      ppTypes m (localBindings tyEnv)),
>             (DumpDesugared,  ppModule desugared),
>             (DumpSimplified, ppModule simplified),
>             (DumpLifted,     ppModule lifted),
>             (DumpIL,         ILPP.ppModule il),
>             (DumpTransformed,ILPP.ppModule ilDbg),
>             (DumpNormalized, ILPP.ppModule ilNormal),
>             (DumpCam,        CamPP.ppModule cam)
>           ]

> ilImports :: ModuleEnv -> IL.Module -> [IL.Decl]
> ilImports mEnv (IL.Module _ is _) =
>   concat [ilTransIntf (Interface m ds) | (m,ds) <- envToList mEnv,
>                                          m `elem` is]

> writeCode :: Maybe FilePath -> FilePath -> Either CFile [CFile] -> IO ()
> writeCode tfn sfn (Left cfile) = writeCCode ofn cfile
>   where ofn = fromMaybe (rootname sfn ++ cExt) tfn
> writeCode tfn sfn (Right cfiles) = zipWithM_ (writeCCode . mkFn) [1..] cfiles
>   where prefix = fromMaybe (rootname sfn) tfn
>         mkFn i = prefix ++ show i ++ cExt

> writeCCode :: FilePath -> CFile -> IO ()
> writeCCode fn = writeFile fn . showln . ppCFile

> showln :: Show a => a -> String
> showln x = shows x "\n"

\end{lstlisting}
A goal is compiled with respect to a given module. If no module is
specified the Curry prelude is used. The source module has to be
parsed and type checked before the goal can be compiled.  Otherwise
compilation of a goal is similar to that of a module.
\begin{lstlisting}

> compileGoal :: Options -> Maybe String -> Maybe FilePath -> IO ()
> compileGoal opts g fn =
>   do
>     (ccode,dumps) <- maybe (return startupCode) goalCode g
>     mapM_ (doDump opts) dumps
>     writeCCode ofn ccode
>   where ofn = fromMaybe (internalError "No filename for startup code")
>                         (output opts)
>         startupCode = (genMain "curry_run",[])
>         goalCode = doCompileGoal (debug opts) (importPath opts) fn

> doCompileGoal :: Bool -> [FilePath] -> Maybe FilePath -> String
>               -> IO (CFile,[(Dump,Doc)])
> doCompileGoal debug paths fn g =
>   do
>     (mEnv,_,ds) <- loadGoalModule paths fn
>     let (tyEnv,g',tyWC) = checkGoal mEnv ds (ok (parseGoal g))
>         (ccode,dumps) =
>           transGoal debug runGoal mEnv tyEnv (mkIdent "goal") g'
>         ccode' = genMain runGoal
>     return (mergeCFile ccode ccode',dumps)
>   where runGoal = "curry_runGoal"

> typeGoal :: Options -> String -> Maybe FilePath -> IO ()
> typeGoal opts g fn =
>   do
>     (mEnv,m,ds) <- loadGoalModule (importPath opts) fn
>     let (_,_,tyWC) =
>           checkGoal mEnv ds (ok (parseGoal g))
>     print (ppTypeScheme m (gen zeroSet tyWC))

> loadGoalModule :: [FilePath] -> Maybe FilePath
>                -> IO (ModuleEnv,ModuleIdent,[Decl])
> loadGoalModule paths fn =
>   do
>     Module m _ ds <- maybe (return emptyModule) parseGoalModule fn
>     mEnv <- loadInterfaces paths (Module m Nothing ds)
>     let (_,_,intf,_,_) = checkModule mEnv (Module m Nothing ds)
>     return (bindModule intf mEnv,m,filter isImportDecl ds ++ [importMain m])
>   where emptyModule = importPrelude "" (Module emptyMIdent Nothing [])
>         parseGoalModule fn = liftM (parseModule fn) (readFile fn)
>         importMain m = ImportDecl (first "") m False Nothing Nothing

> checkGoal :: ModuleEnv -> [Decl] -> Goal ->
>              (ValueEnv,Goal,TypeWithContext)
> checkGoal mEnv impDs goal = (finalTyEnv,qGoalT,tyWC)
>   where -- import modules
>         (pEnv,tcEnv,typeClassEnv,instEnv,kEnv,tyEnv) = importModules mEnv impDs
>         -- bind imported dictionaries
>         (tcEnv',tyEnv') = bindDict typeClassEnv instEnv (tcEnv,tyEnv)
>         -- bind methods
>         tyEnv'' = bindMethods typeClassEnv tcEnv' tyEnv'
>         -- beware of the order
>         (goal',key) = syntaxCheckGoal tyEnv''    -- syntax check
>                     $ kindInferenceGoal kEnv     -- kind inference
>                     $ checkTypeExprGoal tcEnv' typeClassEnv -- check type exprs
>                     $ disambiguateGoal tcEnv'    -- disambiguate type expressions
>                     $ goal
>         goal''      = precCheckGoal pEnv goal'   -- prec check
>         -- first type check
>         (tyEnvI,tyWC) = typeCheckGoal typeClassEnv instEnv tcEnv' tyEnv'' goal''
>         -- dictionary transformation of declarations and expressions
>         goalT   = dictTransGoal typeClassEnv instEnv tcEnv' tyEnvI key goal''
>         -- dictionary transformation of the type environment
>         tyEnvT  = dictTransValueEnv typeClassEnv tyEnv''
>         -- second type check
>         (tyEnvII,_) = typeCheckGoal noTypeClassEnv noInstanceEnv tcEnv' tyEnvT goalT
>         -- qualification of data expressions
>         qGoal  = qualGoal tyEnvI  goal''
>         qGoalT = qualGoal tyEnvII goalT
>         -- recompute the environments
>         (_,backendEnvs) =
>           recomputeEnvs mEnv (pEnv,tcEnv',typeClassEnv,
>                               instEnv,kEnv,tyEnvI,tyEnvII)
>         -- re-bind methods
>         (_,_,_,_,_,finalTyEnv) = backendEnvs

> transGoal :: Bool -> String -> ModuleEnv -> ValueEnv -> Ident -> Goal
>           -> (CFile,[(Dump,Doc)])
> transGoal debug run mEnv tyEnv goalId g = (ccode,dumps)
>   where qGoalId = qualifyWith emptyMIdent goalId
>         evEnv = evalEnvGoal g
>         (vs,desugared,tyEnv') = desugarGoal debug tyEnv emptyMIdent goalId g
>         (simplified,tyEnv'') = simplify tyEnv' evEnv desugared
>         (lifted,tyEnv''',evEnv') = lift tyEnv'' evEnv simplified
>         il = ilTrans tyEnv''' evEnv' lifted
>         ilDbg = if debug then dAddMain goalId (dTransform False il) else il
>         ilNormal = liftProg ilDbg
>         cam = camCompile ilNormal
>         imports = camCompileData (ilImports mEnv il)
>         ccode =
>           genModule imports cam ++
>           genEntry run (fun qGoalId) (fmap (map name) vs)
>         dumps = [
>             (DumpRenamed,    ppGoal g),
>             (DumpTypes,      ppTypes emptyMIdent (localBindings tyEnv)),
>             (DumpDesugared,  ppModule desugared),
>             (DumpSimplified, ppModule simplified),
>             (DumpLifted,     ppModule lifted),
>             (DumpIL,         ILPP.ppModule il),
>             (DumpTransformed,ILPP.ppModule ilDbg),
>             (DumpNormalized, ILPP.ppModule ilNormal),
>             (DumpCam,        CamPP.ppModule cam)
>           ]

\end{lstlisting}
The compiler adds a startup function for the default goal
\texttt{main.main} to the \texttt{main} module. Thus, there is no need
to determine the type of the goal when linking the program.
\begin{lstlisting}

> compileDefaultGoal :: Bool -> ModuleEnv -> Interface -> Maybe CFile
> compileDefaultGoal debug mEnv (Interface m ds)
>   | m == mainMIdent && any (qMainId ==) [f | IFunctionDecl _ f _ <- ds] =
>       Just ccode
>   | otherwise = Nothing
>   where qMainId = qualify mainId
>         mEnv' = bindModule (Interface m ds) mEnv
>         (tyEnv,g,_) =
>           checkGoal mEnv' [ImportDecl (first "") m False Nothing Nothing]
>                     (Goal (first "") (Variable qMainId) [])
>         (ccode,_) = transGoal debug "curry_run" mEnv' tyEnv mainId g

\end{lstlisting}
The function \texttt{importModules} brings the declarations of all
imported modules into scope for the current module.
\begin{lstlisting}

> importModules :: ModuleEnv -> [Decl] ->
>                  (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv)
> importModules mEnv ds = (pEnv,tcEnv',typeClassEnv,instEnv,kEnv,tyEnv)
>   where (pEnv,tcEnv,typeClassEnv,instEnv,kEnv,tyEnv) =
>           foldl (importModule' mEnv) initEnvs ds
>         tcEnv' = importUnifyData tcEnv

> importModule' :: ModuleEnv ->
>                  (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv) ->
>                  Decl ->
>                  (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv)
> importModule' mEnv envs (ImportDecl p m q asM is) =
>   case lookupModule m mEnv of
>     Just ds -> importInterface p (fromMaybe m asM) q is (Interface m ds) envs
>     Nothing -> internalError "importModule"
> importModule' _ envs _ = envs

\end{lstlisting}
After type checking, the environments must be recomputed in order
to bind every entity with its original name (they may be binded with
another name if they have been requalified in the import). All the
unqualified entities are also removed.
\begin{lstlisting}

> reimportModules :: ModuleEnv ->
>                    (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv)
> reimportModules mEnv = foldl importInterface' initEnvs (envToList mEnv)
>   where importInterface' envs (m,ds) =
>           importInterfaceIntf (Interface m ds) envs

> rebindLocalEntities :: (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv) ->
>                        (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv) ->
>                        (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv)
> rebindLocalEntities (origPEnv,origTcEnv,origTypeClassEnv,
>                      origInstEnv,origKindEnv,origTyEnv)
>                     (freshPEnv,freshTcEnv,freshTypeClassEnv,
>                      freshInstEnv,freshKindEnv,freshTyEnv) =
>   (pEnv,tcEnv,typeClassEnv,instEnv,kEnv,tyEnv)
>   where pEnv          = foldr bindLocalQual freshPEnv
>                               (localBindings origPEnv)
>         tcEnv         = foldr bindLocalQual freshTcEnv
>                               (localBindings origTcEnv)
>         typeClassEnv  = foldr bindLocalQual freshTypeClassEnv
>                               (localBindings origTypeClassEnv)
>         instEnv       = foldr bindLocalQualDualTopEnv freshInstEnv
>                               (localBindingsDualTopEnv origInstEnv)
>         kEnv          = foldr bindLocalQual freshKindEnv
>                               (localBindings origKindEnv)
>         tyEnv         = foldr bindLocalGlobal freshTyEnv
>                               (localBindings origTyEnv)
>         bindLocalQual     (_,y) = qualBindLocalTopEnv     (origName y) y
>         bindLocalGlobal (x,y)
>           | uniqueId x == 0 = bindLocalQual (undefined,y)
>           | otherwise       = bindLocalTopEnv x y
>         bindLocalQualDualTopEnv (_,_,z) = (uncurry bindLocalDualTopEnv)
>                                             (dualEntityOrigName z) z

> type Envs = (PEnv,TCEnv,TypeClassEnv,
>              InstanceEnv,KindEnv,ValueEnv)

> recomputeEnvs :: ModuleEnv -> (PEnv,TCEnv,TypeClassEnv,InstanceEnv,
>                                KindEnv,ValueEnv,ValueEnv) ->
>                  (Envs,Envs)
> recomputeEnvs mEnv (localPEnv,localTcEnv,localTypeClassEnv,
>                     localInstEnv,localKEnv,
>                     localTyEnvI,localTyEnvII) =
>   (exportEnvs,backendEnvs)
>   where -- the starting point
>         freshEnvs = reimportModules mEnv
>         -- first branch: export envs
>         (exportPEnv,exportTcEnv,exportTypeClassEnv,
>          exportInstEnv,exportKEnv,exportTyEnv) =
>            rebindLocalEntities (localPEnv,localTcEnv,localTypeClassEnv,
>                                 localInstEnv,localKEnv,localTyEnvI) freshEnvs
>         exportTyEnv' = bindMethods exportTypeClassEnv exportTcEnv exportTyEnv
>         exportEnvs = (exportPEnv,exportTcEnv,exportTypeClassEnv,
>                       exportInstEnv,exportKEnv,exportTyEnv')
>         -- second branch: back-end envs
>         backendPEnv = exportPEnv
>         backendTcEnv = exportTcEnv
>         backendTypeClassEnv = exportTypeClassEnv
>         backendInstEnv = exportInstEnv
>         backendKEnv = exportKEnv
>         (_,_,_,_,_,backendTyEnv) =
>            rebindLocalEntities (undefined,undefined,undefined,
>                                 undefined,undefined,localTyEnvII) freshEnvs
>         (backendTcEnv',backendTyEnv') = bindDict backendTypeClassEnv backendInstEnv
>                                                  (backendTcEnv,backendTyEnv)
>         backendTyEnv'' = dictTransValueEnv backendTypeClassEnv backendTyEnv'
>         backendEnvs = (backendPEnv,backendTcEnv',backendTypeClassEnv,
>                        backendInstEnv,backendKEnv,backendTyEnv'')

\end{lstlisting}
Initial set of environments.
\begin{lstlisting}

> initEnvs :: (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv)
> initEnvs = (initPEnv,initTCEnv,initTypeClassEnv,initInstanceEnv,
>             initKindEnv,initDCEnv)

\end{lstlisting}
An implicit import of the prelude is added to the declarations of
every module, except for the prelude itself. If no explicit import for
the prelude is present, the prelude is imported unqualified, otherwise
only a qualified import is added.
\begin{lstlisting}

> importPrelude :: FilePath -> Module -> Module
> importPrelude fn (Module m es ds) =
>   Module m es (if m == preludeMIdent then ds else ds')
>   where ids = filter isImportDecl ds
>         ds' = ImportDecl (first fn) preludeMIdent
>                          (preludeMIdent `elem` map importedModule ids)
>                          Nothing Nothing : ds
>         importedModule (ImportDecl _ m q asM is) = fromMaybe m asM

\end{lstlisting}
If an import declaration for a module is found, the compiler first
checks whether an import for the module is already pending. In this
case the module imports are cyclic which is not allowed in Curry. The
compilation will therefore be aborted. Next, the compiler checks
whether the module has been imported already. If so, nothing needs to
be done, otherwise the interface will be searched in the import paths
and compiled.
\begin{lstlisting}

> loadInterface :: [FilePath] -> [ModuleIdent] -> ModuleEnv ->
>     (Position,ModuleIdent) -> IO ModuleEnv
> loadInterface paths ctxt mEnv (p,m)
>   | m `elem` ctxt = errorAt p (cyclicImport m (takeWhile (/= m) ctxt))
>   | isLoaded m mEnv = return mEnv
>   | otherwise =
>       lookupInterface paths m >>=
>       maybe (errorAt p (interfaceNotFound m))
>             (compileInterface paths ctxt mEnv m)
>   where isLoaded m mEnv = maybe False (const True) (lookupModule m mEnv)

\end{lstlisting}
After parsing an interface, all imported interfaces are recursively
loaded and entered into the interface's environment.
\begin{lstlisting}

> compileInterface :: [FilePath] -> [ModuleIdent] -> ModuleEnv -> ModuleIdent
>                  -> FilePath -> IO ModuleEnv
> compileInterface paths ctxt mEnv m fn =
>   do
>     intf@(Interface m' _) <- liftM (ok . parseInterface fn) (readFile fn)
>     unless (m == m') (errorAt (first fn) (wrongInterface m m'))
>     mEnv' <- loadIntfInterfaces paths ctxt mEnv intf
>     return (bindModule (checkInterface mEnv' intf) mEnv')

> loadIntfInterfaces :: [FilePath] -> [ModuleIdent] -> ModuleEnv -> Interface
>                    -> IO ModuleEnv
> loadIntfInterfaces paths ctxt mEnv (Interface m ds) =
>   foldM (loadInterface paths (m:ctxt)) mEnv [(p,m) | IImportDecl p m <- ds]

> checkInterface :: ModuleEnv -> Interface -> Interface
> checkInterface mEnv (Interface m ds) =
>   intfCheck pEnv tcEnv typeClassEnv instEnv kEnv tyEnv (Interface m ds')
>   where (pEnv,tcEnv,typeClassEnv,instEnv,kEnv,tyEnv) =
>           foldl (importInterface' mEnv) initEnvs ds
>         ds' = disambiguateIDecls m tcEnv ds

> importInterface' :: ModuleEnv ->
>                     (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv) ->
>                     IDecl -> 
>                     (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv)
> importInterface' mEnv envs (IImportDecl p m) =
>   case lookupModule m mEnv of
>     Just ds -> importInterfaceIntf (Interface m ds) envs
>     Nothing -> internalError "importInterface"
> importInterface' _ envs _ = envs

\end{lstlisting}
After checking the module successfully, the compiler may need to
update the module's interface file. The file will be updated only if
the interface has been changed or the file did not exist before.

The code is a little bit tricky because we must make sure that the
interface file is closed before rewriting the interface, even if it
has not been read completely. On the other hand, we must not apply
\texttt{hClose} too early. Note that there is no need to close the
interface explicitly if the interface check succeeds because the whole
file must have been read in this case. In addition, we do not update
the interface file in this case and therefore it doesn't matter when
the file is closed.
\begin{lstlisting}

> updateInterface :: FilePath -> TCEnv -> Interface -> IO ()
> updateInterface sfn tcEnv i =
>   do
>     eq <- catch (matchInterface ifn tcEnv i) (const (return False))
>     unless eq (writeInterface ifn i)
>   where ifn = rootname sfn ++ intfExt

> matchInterface :: FilePath -> TCEnv -> Interface -> IO Bool
> matchInterface ifn tcEnv i =
>   do
>     h <- openFile ifn ReadMode
>     s <- hGetContents h
>     case parseInterface ifn s of
>       Ok (Interface mid iDecls)
>         | i `intfEquiv` (Interface mid (disambiguateIDecls mid tcEnv iDecls)) ->
>             return True
>       _ -> hClose h >> return False

> writeInterface :: FilePath -> Interface -> IO ()
> writeInterface ifn = writeFile ifn . showln . ppInterface

\end{lstlisting}
The compiler searches for interface files in the import search path
using the extension \texttt{".icurry"}. Note that the current
directory is always searched first.
\begin{lstlisting}

> lookupInterface :: [FilePath] -> ModuleIdent -> IO (Maybe FilePath)
> lookupInterface paths m = lookupFile (ifn : [catPath p ifn | p <- paths])
>   where ifn = foldr1 catPath (moduleQualifiers m) ++ intfExt

\end{lstlisting}
Literate source files use the extension \texttt{".lcurry"}.
\begin{lstlisting}

> unlitLiterate :: FilePath -> String -> String
> unlitLiterate fn s
>   | not (isLiterateSource fn) = s
>   | null es = s'
>   | otherwise = error es
>   where (es,s') = unlit fn s

> isLiterateSource :: FilePath -> Bool
> isLiterateSource fn = litExt `isSuffixOf` fn

\end{lstlisting}
The \texttt{doDump} function writes the selected information to the
standard output.
\begin{lstlisting}

> doDump :: Options -> (Dump,Doc) -> IO ()
> doDump opts (d,x) =
>   when (d `elem` dump opts)
>        (print (text hd $$ text (replicate (length hd) '=') $$ x))
>   where hd = dumpHeader d

> dumpHeader :: Dump -> String
> dumpHeader DumpRenamed = "Module after renaming"
> dumpHeader DumpTypes = "Types"
> dumpHeader DumpDesugared = "Source code after desugaring"
> dumpHeader DumpSimplified = "Source code after simplification"
> dumpHeader DumpLifted = "Source code after lifting"
> dumpHeader DumpIL = "Intermediate code"
> dumpHeader DumpTransformed = "Transformed code" 
> dumpHeader DumpNormalized = "Intermediate code after normalization"
> dumpHeader DumpCam = "Abstract machine code"
> dumpHeader DumpPEnv = "Priorities environment"
> dumpHeader DumpTypeConstructorsEnv = "Type constructors environment"
> dumpHeader DumpKindEnv = "Kind environment"
> dumpHeader DumpTypeClassesEnv = "Type classes environment"
> dumpHeader DumpInstancesEnv = "Instances environment"
> dumpHeader DumpModule1 = "Module (1)"
> dumpHeader DumpModule2 = "Module (2)"
> dumpHeader DumpModule3 = "Module (3)"
> dumpHeader DumpModule4 = "Module (4)"
> dumpHeader DumpModule5 = "Module (5)"
> dumpHeader DumpModule6 = "Module (6)"
> dumpHeader DumpModule7 = "Module (7)"
> dumpHeader DumpGenerated = "Generated declarations"

\end{lstlisting}
The function \texttt{ppTypes} is used for pretty-printing the types
from the type environment.
\begin{lstlisting}

> ppTypes :: ModuleIdent -> [(Ident,ValueInfo)] -> Doc
> ppTypes m = vcat . map (ppIDecl . mkDecl) . filter (isValue . snd)
>   where mkDecl (v,Value _ (ForAll _ tyWC)) =
>           IFunctionDecl undefined (qualify v)
>                         (fromTypeWithContext $ unqualifyIdentsIn m tyWC)
>         isValue (DataConstructor    _ _) = False
>         isValue (NewtypeConstructor _ _) = False
>         isValue (Value              _ _) = True

\end{lstlisting}
Various filename extensions
\begin{lstlisting}

> cExt    = ".c"
> intfExt = ".icurry"
> litExt  = ".lcurry"

\end{lstlisting}
Error functions.
\begin{lstlisting}

> interfaceNotFound :: ModuleIdent -> String
> interfaceNotFound m = "Interface for module " ++ moduleName m ++ " not found"

> cyclicImport :: ModuleIdent -> [ModuleIdent] -> String
> cyclicImport m [] = "Recursive import for module " ++ moduleName m
> cyclicImport m ms =
>   "Cyclic import dependency between modules " ++ moduleName m ++
>     modules "" ms
>   where modules comma [m] = comma ++ " and " ++ moduleName m
>         modules _ (m:ms) = ", " ++ moduleName m ++ modules "," ms

> wrongInterface :: ModuleIdent -> ModuleIdent -> String
> wrongInterface m m' =
>   "Expected interface for " ++ show m ++ " but found " ++ show m'

\end{lstlisting}
