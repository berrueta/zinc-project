% $Id: Imports.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2000-2003, Wolfgang Lux
% Copyright (c) 2003-2005, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Imports.lhs}
\codesection{Importing interfaces}
This module provides a few functions which can be used to import
interfaces into the current module.
\begin{lstlisting}

> module Imports(importInterface,importInterfaceIntf,importUnifyData) where
> import Ident
> import TypeConstructorEnv
> import PEnv
> import Position
> import CurrySyntax
> import Types
> import TypeTrans
> import TypeClassEnv
> import ValueEnv
> import Env
> import TopEnv
> import Maybe
> import Map
> import Set
> import Error(errorAt,internalError)
> import TypeExpr
> import Kind
> import KindEnv hiding (bindKind)
> import InstanceEnv
> import DualTopEnv
> import CurrySyntaxUtils
> import TypeClassBinding
> import TypeExprRenaming
> import TypeExprSubst
> import DictUtils(dictTypeVariablePrefix)

\end{lstlisting}
These environments are computed from the interface.
Note that the original names of all
entities defined in the imported module are qualified appropriately.
The same is true for type expressions.
\begin{lstlisting}

> type ExpPEnv         = Env Ident PrecInfo
> type ExpTCEnv        = Env Ident TypeInfo
> type ExpValueEnv     = Env Ident ValueInfo
> type ExpKindEnv      = Env Ident KindInfo
> type ExpTypeClassEnv = Env Ident TypeClassInfo
> type ExpInstanceEnv  = Env (QualIdent,QualIdent) InstanceInfo

\end{lstlisting}
When an interface is imported, the compiler first transforms the
interface into these environments. If an import specification is
present, the environments are restricted to only those entities which
are included in the specification or not hidden by it, respectively.
The resulting environments are then imported into the current module
using either a qualified import or both a qualified and an unqualified
import (boolean flag \texttt{q}).
\begin{lstlisting}

> importInterface :: Position -> ModuleIdent -> Bool -> Maybe ImportSpec
>                 -> Interface
>                 -> (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv)
>                 -> (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv)
> importInterface p m q is i (pEnv,tcEnv,typeClassEnv,instEnv,kEnv,tyEnv) =
>   (pEnv',tcEnv',typeClassEnv',instEnv',kEnv',tyEnv')
>   where -- bind the entities to the temporary environments
>         mPEnv         = intfEnv bindPrec   i
>         mTCEnv        = intfEnv bindTC     i
>         mTyEnv        = intfEnv bindTy     i
>         mKEnv         = intfEnv bindKind   i
>         mTypeClassEnv = intfEnv bindTClass i
>         mInstEnv      = intfEnv bindInst   i
>         -- expand import specs
>         is' :: [Import]
>         is' = maybe [] (expandSpecs m mTCEnv mTyEnv) is
>         ts,vs :: Ident -> Bool
>         ts = isVisible is (fromListSet (foldr addType  [] is'))
>         vs = isVisible is (fromListSet (foldr addValue [] is'))
>         -- binds the entities to the final environments
>         pEnv'         = importEntities m q vs id mPEnv pEnv
>         tcEnv'        = importEntities m q ts (importData vs) mTCEnv tcEnv
>         typeClassEnv' = importEntities m q ts id mTypeClassEnv typeClassEnv
>         instEnv'      = importInstances m mInstEnv instEnv
>         kEnv'         = importEntities m q ts id mKEnv kEnv
>         tyEnv'        = importEntities m q vs id mTyEnv tyEnv

> isVisible :: Maybe ImportSpec -> Set Ident -> Ident -> Bool
> isVisible (Just (Importing _ _)) xs = (`elemSet`    xs)
> isVisible (Just (Hiding    _ _)) xs = (`notElemSet` xs)
> isVisible _ _ = const True

\end{lstlisting}
The \texttt{importEntities} bind the entities from the temporary environment
to their final environment. If the boolean flag \texttt{q} is up, only a
qualified binding is performed. The \texttt{isVisible} parameter is a
predicate to filter the entities which should imported. And the \texttt{f}
function performs a rewriting of the entity (see the function
\texttt{importData} below).
\begin{lstlisting}

> importEntities :: Entity a => ModuleIdent -> Bool -> (Ident -> Bool)
>                -> (a -> a) -> Env Ident a -> TopEnv a -> TopEnv a
> importEntities m q isVisible f mEnv env =
>   foldr (uncurry (if q then qualBindImportTopEnv m else importUnqual m)) env
>         [(x,f y) | (x,y) <- envToList mEnv, isVisible x]
>   where importUnqual m x y = bindImportTopEnv m x y . qualBindImportTopEnv m x y

\end{lstlisting}
Importing type constructors is the most complex task, because some of
the data constructors may be hidden. First, all the data constructors are
imported, but then they are filtered by the \texttt{importData} function
to remove the hidden data constructors.
\begin{lstlisting}

> importData :: (Ident -> Bool) -> TypeInfo -> TypeInfo
> importData isVisible (DataType tc n cs) =
>   DataType tc n (map (>>= importConstr isVisible) cs)
> importData isVisible (RenamingType tc n nc) =
>   maybe (DataType tc n []) (RenamingType tc n) (importConstr isVisible nc)
> importData isVisible (AliasType tc  n ty) = AliasType tc n ty

> importConstr :: (Ident -> Bool) -> Data a -> Maybe (Data a)
> importConstr isVisible (Data c n tys)
>   | isVisible c = Just (Data c n tys)
>   | otherwise = Nothing

\end{lstlisting}
Importing instances is an special case.
\begin{lstlisting}

> importInstances :: ModuleIdent -> Env (QualIdent,QualIdent) InstanceInfo ->
>                    InstanceEnv -> InstanceEnv
> importInstances m mInstEnv instEnv =
>   foldr (uncurry $ uncurry (bindImportDualTopEnv m)) instEnv instList
>   where instList :: [((QualIdent,QualIdent),InstanceInfo)]
>         instList = envToList mInstEnv

\end{lstlisting}
Importing an interface into another interface is somewhat simpler
because all entities are imported into the environment. In addition,
only a qualified import is necessary. Note that the hidden data types
are imported as well because they may be used in type expressions in
an interface.
\begin{lstlisting}

> importInterfaceIntf :: Interface ->
>                        (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv) ->
>                        (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv)
> importInterfaceIntf i (pEnv,tcEnv,typeClassEnv,instEnv,kEnv,tyEnv) =
>   (pEnv',tcEnv',typeClassEnv',instEnv',kEnv',tyEnv')
>   where  Interface m _ = i
>          -- bind the entities to the temporary environments
>          mPEnv         = intfEnv bindPrec     i
>          mTCHiddenEnv  = intfEnv bindTCHidden i
>          mTyEnv        = intfEnv bindTy       i
>          mKEnv         = intfEnv bindKind     i
>          mTypeClassEnv = intfEnv bindTClass   i
>          mInstEnv      = intfEnv bindInst     i
>          -- binds the entities to the final environments
>          pEnv'         = importEntities m True (const True) id mPEnv pEnv
>          tcEnv'        = importEntities m True (const True) id mTCHiddenEnv tcEnv
>          typeClassEnv' = importEntities m True (const True) id mTypeClassEnv
>                                                                typeClassEnv
>          instEnv'      = importInstances m mInstEnv instEnv
>          kEnv'         = importEntities m True (const True) id mKEnv kEnv
>          tyEnv'        = importEntities m True (const True) id mTyEnv tyEnv

\end{lstlisting}
In a first step, the export environments are initialized from
the interface's declarations. This step also qualifies the names of
all entities defined in (but not imported into) the interface with its
module name.  
\begin{lstlisting}

> intfEnv :: Ord a => (ModuleIdent -> IDecl -> Env a b -> Env a b) ->
>                     Interface -> Env a b
> intfEnv bind (Interface m ds) = foldr (bind m) emptyEnv ds

> bindPrec :: ModuleIdent -> IDecl -> ExpPEnv -> ExpPEnv
> bindPrec m (IInfixDecl _ fix p op) =
>   bindEnv (unqualify op) (PrecInfo (qualQualify m op) (OpPrec fix p))
> bindPrec _ _ = id

> bindTClass :: ModuleIdent -> IDecl -> ExpTypeClassEnv -> ExpTypeClassEnv
> bindTClass m decl@(ITypeClassDecl _ ctx tc tv decls) =
>   bindEnv (unqualify tc) (TypeClassInfo qualTc tv' super methodEnv)
>   where qualTc = qualQualify m tc
>         super  = map (qualQualify m) (parentTypeClassesIDecl decl)
>         tv'    = dictTypeVariablePrefix qualTc tv
>         sigma  = bindTypeExprVar tv (TypeExprVariable tv') idSubst
>         decls' = typeExprRenamingShared sigma decls
>         methodEnv = foldr (uncurry bindEnv) emptyEnv (methodSigsIDecls decls')
> bindTClass _ _ = id

> bindInst :: ModuleIdent -> IDecl -> ExpInstanceEnv -> ExpInstanceEnv
> bindInst m decl@(IInstanceDecl p ctx typeClass tyexpr) =
>   bindEnv (qualTypeClass,qualTypeConstr) instInfo
>   where typeConstr =
>           case leftmostTypeExpr tyexpr of
>             (TypeExprConstructor tc') -> tc'
>             _                         -> internalError "bindInst"
>         instInfo = importedInstance m qualTypeClass qualTypeConstr instCtx
>         instCtx = typeExprContextToInstanceContext (qualifyIdentsIn m ctx)
>                                                    (qualifyIdentsIn m tyexpr)
>         qualTypeClass  = qualQualify m typeClass
>         qualTypeConstr = qualQualify m typeConstr
> bindInst _ _ = id

> bindKind :: ModuleIdent -> IDecl -> ExpKindEnv -> ExpKindEnv
> bindKind m (IKindDecl _ tc k) = 
>   bindEnv (unqualify tc) (KindInfo (qualQualify m tc) k)
> bindKind _ _ = id

> bindTC :: ModuleIdent -> IDecl -> ExpTCEnv -> ExpTCEnv
> bindTC m (IDataDecl _ tc tvs cs) =
>   bindType DataType m tc tvs (map (fmap mkData) cs)
>   where mkData (ConstrDecl _ evs c tys) =
>           Data c (length evs) (map (qualifyIdentsIn m) (toTypes tvs tys))
>         mkData (ConOpDecl _ evs ty1 c ty2) =
>           Data c (length evs) (map (qualifyIdentsIn m) (toTypes tvs [ty1,ty2]))
> bindTC m (INewtypeDecl _ tc tvs (NewConstrDecl _ evs c ty)) =
>   bindType RenamingType m tc tvs (Data c (length evs)
>                                        (qualifyIdentsIn m $ toType tvs ty))
> bindTC m (ITypeDecl _ tc tvs ty) =
>   bindType AliasType m tc tvs (qualifyIdentsIn m $ toType tvs ty)
> bindTC m _ = id

> bindTCHidden :: ModuleIdent -> IDecl -> ExpTCEnv -> ExpTCEnv
> bindTCHidden m (HidingDataDecl _ tc tvs) =
>   bindType DataType m (qualify tc) tvs []
> bindTCHidden m d = bindTC m d

> bindType :: (QualIdent -> Int -> a -> TypeInfo) -> ModuleIdent -> QualIdent
>          -> [Ident] -> a -> ExpTCEnv -> ExpTCEnv
> bindType f m tc tvs =
>   bindEnv (unqualify tc) . f (qualQualify m tc) (length tvs) 

> bindTy :: ModuleIdent -> IDecl -> ExpValueEnv -> ExpValueEnv
> bindTy m (IDataDecl _ tc tvs cs) =
>   flip (foldr (bindConstr m tc' tvs (constrType tc' tvs))) (catMaybes cs)
>   where tc' = qualQualify m tc
> bindTy m (INewtypeDecl _ tc tvs nc) =
>   bindNewConstr m tc' tvs (constrType tc' tvs) nc
>   where tc' = qualQualify m tc
> bindTy m (IFunctionDecl _ f tyWC) =
>   bindEnv (unqualify f)
>           (Value (qualQualify m f) (polyTypeWithContext $
>                                     qualifyIdentsIn m $
>                                     toTypeWithContext [] tyWC))
> bindTy m _ = id

> bindConstr :: ModuleIdent -> QualIdent -> [Ident] -> TypeExpr -> ConstrDecl
>            -> ExpValueEnv -> ExpValueEnv
> bindConstr m tc tvs ty0 (ConstrDecl _ evs c tys) =
>   bindValue DataConstructor m tc tvs c evs
>             (foldr typeExprApplyArrowConstructor ty0 tys)
> bindConstr m tc tvs ty0 (ConOpDecl _ evs ty1 op ty2) =
>   bindValue DataConstructor m tc tvs op evs
>             (typeExprApplyArrowConstructor ty1 (typeExprApplyArrowConstructor ty2 ty0))

> bindNewConstr :: ModuleIdent -> QualIdent -> [Ident] -> TypeExpr
>               -> NewConstrDecl -> ExpValueEnv -> ExpValueEnv
> bindNewConstr m tc tvs ty0 (NewConstrDecl _ evs c ty1) =
>   bindValue NewtypeConstructor m tc tvs c evs
>             (typeExprApplyArrowConstructor ty1 ty0)

> bindValue :: (QualIdent -> ExistTypeScheme -> ValueInfo) -> ModuleIdent
>           -> QualIdent -> [Ident] -> Ident -> [Ident] -> TypeExpr
>           -> ExpValueEnv -> ExpValueEnv
> bindValue f m tc tvs c evs ty = bindEnv c (f (qualifyLike tc c) sigma)
>   where sigma = ForAllExist (length tvs) (length evs)
>                             (qualifyIdentsIn m $ toType tvs ty)

\end{lstlisting}
After the environments have been initialized, the optional import
specifications can be checked. There are two kinds of import
specifications, a ``normal'' one, which names the entities that shall
be imported, and a hiding specification, which lists those entities
that shall not be imported.

There is a subtle difference between both kinds of
specifications. While it is not allowed to list a data constructor
outside of its type in a ``normal'' specification, it is allowed to
hide a data constructor explicitly. E.g., if module \texttt{A} exports
the data type \texttt{T} with constructor \texttt{C}, the data
constructor can be imported with one of the two specifications
\begin{lstlisting}
import A(T(C))
import A(T(..))
\end{lstlisting}
but can be hidden in three different ways:
\begin{lstlisting}
import A hiding(C)
import A hiding(T(C))
import A hiding(T(..))
\end{lstlisting}

The functions \texttt{expandImport} and \texttt{expandHiding} check
that all entities in an import specification are actually exported
from the module. In addition, all imports of type constructors are
changed into a \texttt{T()} specification and explicit imports for the
data constructors are added.
\begin{lstlisting}

> expandSpecs :: ModuleIdent -> ExpTCEnv -> ExpValueEnv -> ImportSpec
>             -> [Import]
> expandSpecs m tcEnv tyEnv (Importing p is) =
>   concat (map (expandImport p m tcEnv tyEnv) is)
> expandSpecs m tcEnv tyEnv (Hiding p is) =
>   concat (map (expandHiding p m tcEnv tyEnv) is)

> expandImport :: Position -> ModuleIdent -> ExpTCEnv -> ExpValueEnv -> Import
>              -> [Import]
> expandImport p m tcEnv tyEnv (Import x) = expandThing p m tcEnv tyEnv x
> expandImport p m tcEnv tyEnv (ImportTypeWith tc cs) =
>   [expandTypeWith p m tcEnv tc cs]
> expandImport p m tcEnv tyEnv (ImportTypeAll tc) =
>   [expandTypeAll p m tcEnv tc]

> expandHiding :: Position -> ModuleIdent -> ExpTCEnv -> ExpValueEnv -> Import
>              -> [Import]
> expandHiding p m tcEnv tyEnv (Import x) = expandHide p m tcEnv tyEnv x
> expandHiding p m tcEnv tyEnv (ImportTypeWith tc cs) =
>   [expandTypeWith p m tcEnv tc cs]
> expandHiding p m tcEnv tyEnv (ImportTypeAll tc) =
>   [expandTypeAll p m tcEnv tc]

> expandThing :: Position -> ModuleIdent -> ExpTCEnv -> ExpValueEnv -> Ident
>             -> [Import]
> expandThing p m tcEnv tyEnv tc =
>   case lookupEnv tc tcEnv of
>     Just _ -> expandThing' p m tyEnv tc (Just [ImportTypeWith tc []])
>     Nothing -> expandThing' p m tyEnv tc Nothing

> expandThing' :: Position -> ModuleIdent -> ExpValueEnv -> Ident
>              -> Maybe [Import] -> [Import]
> expandThing' p m tyEnv f tcImport =
>   case lookupEnv f tyEnv of
>     Just v
>       | isConstr v -> maybe (errorAt p (importDataConstr m f)) id tcImport
>       | otherwise -> Import f : maybe [] id tcImport
>     Nothing -> maybe (errorAt p (undefinedEntity m f)) id tcImport
>   where isConstr (DataConstructor _ _) = True
>         isConstr (NewtypeConstructor _ _) = True
>         isConstr (Value _ _) = False

> expandHide :: Position -> ModuleIdent -> ExpTCEnv -> ExpValueEnv -> Ident
>            -> [Import]
> expandHide p m tcEnv tyEnv tc =
>   case lookupEnv tc tcEnv of
>     Just _ -> expandHide' p m tyEnv tc (Just [ImportTypeWith tc []])
>     Nothing -> expandHide' p m tyEnv tc Nothing

> expandHide' :: Position -> ModuleIdent -> ExpValueEnv -> Ident
>             -> Maybe [Import] -> [Import]
> expandHide' p m tyEnv f tcImport =
>   case lookupEnv f tyEnv of
>     Just _ -> Import f : maybe [] id tcImport
>     Nothing -> maybe (errorAt p (undefinedEntity m f)) id tcImport

> expandTypeWith :: Position -> ModuleIdent -> ExpTCEnv -> Ident -> [Ident]
>                -> Import
> expandTypeWith p m tcEnv tc cs =
>   case lookupEnv tc tcEnv of
>     Just (DataType _ _ cs') ->
>       ImportTypeWith tc (map (checkConstr [c | Just (Data c _ _) <- cs']) cs)
>     Just (RenamingType _ _ (Data c _ _)) ->
>       ImportTypeWith tc (map (checkConstr [c]) cs)
>     Just _ -> errorAt p (nonDataType m tc)
>     Nothing -> errorAt p (undefinedEntity m tc)
>   where checkConstr cs c
>           | c `elem` cs = c
>           | otherwise = errorAt p (undefinedDataConstr m tc c)

> expandTypeAll :: Position -> ModuleIdent -> ExpTCEnv -> Ident -> Import
> expandTypeAll p m tcEnv tc =
>   case lookupEnv tc tcEnv of
>     Just (DataType _ _ cs) -> ImportTypeWith tc [c | Just (Data c _ _) <- cs]
>     Just (RenamingType _ _ (Data c _ _)) -> ImportTypeWith tc [c]
>     Just _ -> errorAt p (nonDataType m tc)
>     Nothing -> errorAt p (undefinedEntity m tc)

\end{lstlisting}
After all modules have been imported, the compiler has to ensure that
all references to a data type use the same list of constructors.
\begin{lstlisting}

> importUnifyData :: TCEnv -> TCEnv
> importUnifyData tcEnv =
>   fmap (setInfo (foldr (mergeData . snd) zeroFM (allImports tcEnv))) tcEnv
>   where setInfo :: FM QualIdent TypeInfo -> TypeInfo -> TypeInfo
>         setInfo tcs t = fromJust (lookupFM (origName t) tcs)
>         mergeData :: TypeInfo -> FM QualIdent TypeInfo -> FM QualIdent TypeInfo
>         mergeData t tcs =
>           addToFM tc (maybe t (fromJust . merge t) (lookupFM tc tcs)) tcs
>           where tc = origName t

\end{lstlisting}
Auxiliary functions:
\begin{lstlisting}

> addType :: Import -> [Ident] -> [Ident]
> addType (Import _) tcs = tcs
> addType (ImportTypeWith tc _) tcs = tc : tcs
> addType (ImportTypeAll _) _ = internalError "types"

> addValue :: Import -> [Ident] -> [Ident]
> addValue (Import f) fs = f : fs
> addValue (ImportTypeWith _ cs) fs = cs ++ fs
> addValue (ImportTypeAll _) _ = internalError "values"

> constrType :: QualIdent -> [Ident] -> TypeExpr
> constrType tc tvs = typeExprApplyConstructor tc (map TypeExprVariable tvs)

\end{lstlisting}
Error messages:
\begin{lstlisting}

> undefinedEntity :: ModuleIdent -> Ident -> String
> undefinedEntity m x =
>   "Module " ++ moduleName m ++ " does not export " ++ name x

> undefinedType :: ModuleIdent -> Ident -> String
> undefinedType m tc =
>   "Module " ++ moduleName m ++ " does not export a type " ++ name tc

> undefinedDataConstr :: ModuleIdent -> Ident -> Ident -> String
> undefinedDataConstr m tc c =
>   name c ++ " is not a data constructor of type " ++ name tc

> nonDataType :: ModuleIdent -> Ident -> String
> nonDataType m tc = name tc ++ " is not a data type"

> importDataConstr :: ModuleIdent -> Ident -> String
> importDataConstr m c = "Explicit import for data constructor " ++ name c

\end{lstlisting}
