% $Id: TypeExprDisambiguate.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeExprDisambiguate.lhs}
\codesection{Disambiguating type expressions}
The parser cannot distinguish between unqualified type constructor
identifiers and type variables. So it reads all qualified identifiers
as type constructors and all unqualified identifiers as type
variables. Some of the latter will be type constructors. The objective
of this module is to recognise them, removing the ambiguity of the
parser.
\begin{lstlisting}

> module TypeExprDisambiguate(disambiguateDecls,
>                             disambiguateIDecls,
>                             disambiguateGoal) where
> import TypeExpr
> import CurrySyntax
> import Ident
> import TypeConstructorEnv
> import CurrySyntaxUtils
> import Error(internalError)

\end{lstlisting}
Blocks of declarations.
\begin{lstlisting}

> disambiguateDecls :: ModuleIdent -> TCEnv -> [Decl] -> [Decl]
> disambiguateDecls mid = disambiguateDecls' mid [] []

> disambiguateIDecls :: ModuleIdent -> TCEnv -> [IDecl] -> [IDecl]
> disambiguateIDecls mid = disambiguateDecls' mid [] []

> disambiguateDecls' :: (DefinesTypes a, Ambiguous a) =>
>   ModuleIdent -> [Ident] -> [Ident] -> TCEnv -> [a] -> [a]
> disambiguateDecls' mid tvs tcs tcEnv decls =
>   map (disambiguate mid tvs tcs' tcEnv) decls
>   where tcs' = tcs ++ concat (map tcDefinedBy decls)

\end{lstlisting}
Goals.
\begin{lstlisting}

> disambiguateGoal :: TCEnv -> Goal -> Goal
> disambiguateGoal tcEnv goal = disambiguate noMIdent [] [] tcEnv goal
>   where noMIdent = internalError "TypeExprDisambiguate.noMIdent"

\end{lstlisting}
A type class describing ambiguous constructions.
\begin{lstlisting}

> class Ambiguous a where
>   disambiguate :: ModuleIdent ->
>                   [Ident] ->  -- type vars in scope
>                   [Ident] ->  -- type constructors in scope
>                   TCEnv ->    -- type constructors environment
>                   a -> a

\end{lstlisting}
Generic instantiations.
\begin{lstlisting}

> instance Ambiguous a => Ambiguous [a] where
>   disambiguate mid tvs tcs tcEnv = map (disambiguate mid tvs tcs tcEnv)

> instance Ambiguous a => Ambiguous (Maybe a) where
>   disambiguate mid tvs tcs tcEnv (Nothing) = Nothing
>   disambiguate mid tvs tcs tcEnv (Just x ) =
>     (Just (disambiguate mid tvs tcs tcEnv x))

\end{lstlisting}
Type expressions. Here is where the real disambiguation takes place.
\begin{lstlisting}

> instance Ambiguous TypeExpr where
>   disambiguate mid tvs tcs tcEnv (TypeExprConstructor qid)
>     | isQualified qid = (TypeExprConstructor qid)
>     | otherwise       = disambiguate mid tvs tcs tcEnv (TypeExprVariable id)
>     where id  = unqualify qid
>   disambiguate mid tvs tcs tcEnv (TypeExprVariable id)
>     | id `elem` tvs    = (TypeExprVariable id)
>     | id `elem` tcs    = (TypeExprConstructor (qualify id))
>     | (not $ null tis) = (TypeExprConstructor (qualify id))
>     | otherwise        = (TypeExprVariable id)
>     where tis = lookupTC id tcEnv
>   disambiguate mid tvs tcs tcEnv (TypeExprApplication ty1 ty2) =
>     (TypeExprApplication ty1' ty2')
>     where ty1' = disambiguate mid tvs tcs tcEnv ty1
>           ty2' = disambiguate mid tvs tcs tcEnv ty2

\end{lstlisting}
Type contexts:
\begin{lstlisting}

> instance Ambiguous TypeExprWithContext where
>   disambiguate mid tvs tcs tcEnv (TypeExprWithContext ctx ty) =
>     (TypeExprWithContext (disambiguate mid tvs tcs tcEnv ctx)
>                          (disambiguate mid tvs tcs tcEnv ty))

> instance Ambiguous TypeExprContext where
>   disambiguate mid tvs tcs tcEnv (TypeExprContext classPreds) =
>     (TypeExprContext $ disambiguate mid tvs tcs tcEnv classPreds)

> instance Ambiguous TypeExprClassConstraint where
>   disambiguate mid tvs tcs tcEnv (TypeExprClassConstraint qualTc tv) =
>     (TypeExprClassConstraint qualTc tv)

\end{lstlisting}
Declarations.
\begin{lstlisting}

> instance Ambiguous Decl where
>   disambiguate mid tvs tcs tcEnv (TypeClassDecl p ctx tc tv decls) =
>     (TypeClassDecl p (disambiguate mid (tvs ++ [tv]) tcs tcEnv ctx)
>                    tc tv (disambiguate mid (tvs ++ [tv]) tcs tcEnv decls))
>   disambiguate mid tvs tcs tcEnv (InstanceDecl p ctx tc tyexpr decls) =
>     (InstanceDecl p (disambiguate mid tvs tcs tcEnv ctx)
>                   tc (disambiguate mid tvs tcs tcEnv tyexpr)
>                   (disambiguate mid tvs tcs tcEnv decls))
>   disambiguate mid tvs tcs tcEnv (DataDecl    p tc varIds constrs derivedTC) =
>     (DataDecl p tc varIds (disambiguate mid (tvs ++ varIds)
>                            tcs tcEnv constrs) derivedTC)
>   disambiguate mid tvs tcs tcEnv (NewtypeDecl p tc varIds constrs derivedTC) =
>     (NewtypeDecl p tc varIds (disambiguate mid (tvs ++ varIds) tcs tcEnv constrs) derivedTC)
>   disambiguate mid tvs tcs tcEnv (TypeDecl    p tc varIds ty     ) =
>     (TypeDecl p tc varIds (disambiguate mid (tvs ++ varIds) tcs tcEnv ty))
>   disambiguate mid tvs tcs tcEnv (TypeSig     p funIds ty        ) =
>     (TypeSig p funIds (disambiguate mid tvs tcs tcEnv ty))
>   disambiguate mid tvs tcs tcEnv (FunctionDecl p i eqs) =
>     (FunctionDecl p i (disambiguate mid tvs tcs tcEnv eqs))
>   disambiguate mid tvs tcs tcEnv (ExternalDecl p cc s i tyexpr) =
>     (ExternalDecl p cc s i (disambiguate mid tvs tcs tcEnv tyexpr))
>   disambiguate mid tvs tcs tcEnv (PatternDecl p constr rhs) =
>     (PatternDecl p constr (disambiguate mid tvs tcs tcEnv rhs))
>   disambiguate _ _ _ _ decl@(ImportDecl     _ _ _ _ _) = decl
>   disambiguate _ _ _ _ decl@(InfixDecl      _ _ _ _  ) = decl
>   disambiguate _ _ _ _ decl@(EvalAnnot      _ _ _    ) = decl
>   disambiguate _ _ _ _ decl@(ExtraVariables _ _      ) = decl

> instance Ambiguous IDecl where
>   disambiguate mid tvs tcs tcEnv (ITypeClassDecl p ctx tc tv decls) =
>     (ITypeClassDecl p (disambiguate mid (tvs ++ [tv]) tcs tcEnv ctx)
>                     tc tv (disambiguate mid (tvs ++ [tv]) tcs tcEnv decls))
>   disambiguate mid tvs tcs tcEnv (IInstanceDecl p ctx tc tyexpr) =
>     (IInstanceDecl p  (disambiguate mid tvs tcs tcEnv ctx)
>                    tc (disambiguate mid tvs tcs tcEnv tyexpr))
>   disambiguate mid tvs tcs tcEnv (IDataDecl     p tc varIds constrs) =
>     (IDataDecl p tc varIds (disambiguate mid (tvs ++ varIds) tcs tcEnv constrs))
>   disambiguate mid tvs tcs tcEnv (INewtypeDecl  p tc varIds constr ) =
>     (INewtypeDecl p tc varIds (disambiguate mid (tvs ++ varIds) tcs tcEnv constr))
>   disambiguate mid tvs tcs tcEnv (ITypeDecl     p tc varIds ty     ) =
>     (ITypeDecl p tc varIds (disambiguate mid (tvs ++ varIds) tcs tcEnv ty))
>   disambiguate mid tvs tcs tcEnv (IFunctionDecl p funId ty         ) =
>     (IFunctionDecl p funId (disambiguate mid tvs tcs tcEnv ty))
>   disambiguate _ _ _ _ idecl@(IImportDecl    _ _    ) = idecl
>   disambiguate _ _ _ _ idecl@(IInfixDecl     _ _ _ _) = idecl
>   disambiguate _ _ _ _ idecl@(HidingDataDecl _ _ _  ) = idecl
>   disambiguate _ _ _ _ idecl@(IKindDecl      _ _ _  ) = idecl

> instance Ambiguous ConstrDecl where
>   disambiguate mid tvs tcs tcEnv (ConstrDecl p tv dc tyexprs) =
>     ConstrDecl p tv dc (disambiguate mid tvs tcs tcEnv tyexprs)
>   disambiguate mid tvs tcs tcEnv (ConOpDecl p tv tyexpr1 op tyexpr2) =
>     ConOpDecl p tv (disambiguate mid tvs tcs tcEnv tyexpr1) op
>                    (disambiguate mid tvs tcs tcEnv tyexpr2)

> instance Ambiguous NewConstrDecl where
>   disambiguate mid tvs tcs tcEnv (NewConstrDecl p tv dc tyexpr) =
>     NewConstrDecl p tv dc (disambiguate mid tvs tcs tcEnv tyexpr)

> instance Ambiguous Equation where
>   disambiguate mid tvs tcs tcEnv (Equation p cts rhs) =
>     (Equation p cts (disambiguate mid tvs tcs tcEnv rhs))

> instance Ambiguous Rhs where
>   disambiguate mid tvs tcs tcEnv (SimpleRhs p expr decls) =
>     (SimpleRhs p (disambiguate mid tvs tcs tcEnv expr)
>                  (disambiguate mid tvs tcs tcEnv decls))
>   disambiguate mid tvs tcs tcEnv (GuardedRhs cond decls) =
>     (GuardedRhs (disambiguate mid tvs tcs tcEnv cond)
>                 (disambiguate mid tvs tcs tcEnv decls))

> instance Ambiguous CondExpr where
>   disambiguate mid tvs tcs tcEnv (CondExpr p expr1 expr2) =
>     (CondExpr p (disambiguate mid tvs tcs tcEnv expr1)
>                 (disambiguate mid tvs tcs tcEnv expr2))

> instance Ambiguous Expression where
>   disambiguate mid tvs tcs tcEnv (Literal l) = (Literal l)
>   disambiguate mid tvs tcs tcEnv (Variable v) = (Variable v)
>   disambiguate mid tvs tcs tcEnv (Constructor c) = (Constructor c)
>   disambiguate mid tvs tcs tcEnv (Paren expr) =
>     (Paren (disambiguate mid tvs tcs tcEnv expr))
>   disambiguate mid tvs tcs tcEnv (Typed expr tyexpr) =
>     (Typed (disambiguate mid tvs tcs tcEnv expr)
>            (disambiguate mid tvs tcs tcEnv tyexpr))
>   disambiguate mid tvs tcs tcEnv (Tuple exprs) =
>     (Tuple (disambiguate mid tvs tcs tcEnv exprs))
>   disambiguate mid tvs tcs tcEnv (List exprs) =
>     (List (disambiguate mid tvs tcs tcEnv exprs))
>   disambiguate mid tvs tcs tcEnv (ListCompr expr stats) =
>     (ListCompr (disambiguate mid tvs tcs tcEnv expr)
>                (disambiguate mid tvs tcs tcEnv stats))
>   disambiguate mid tvs tcs tcEnv (EnumFrom expr) =
>     (EnumFrom (disambiguate mid tvs tcs tcEnv expr))
>   disambiguate mid tvs tcs tcEnv (EnumFromThen expr1 expr2) =
>     (EnumFromThen (disambiguate mid tvs tcs tcEnv expr1)
>                   (disambiguate mid tvs tcs tcEnv expr2))
>   disambiguate mid tvs tcs tcEnv (EnumFromTo expr1 expr2) =
>     (EnumFromTo (disambiguate mid tvs tcs tcEnv expr1)
>                 (disambiguate mid tvs tcs tcEnv expr2))
>   disambiguate mid tvs tcs tcEnv (EnumFromThenTo expr1 expr2 expr3) =
>     (EnumFromThenTo (disambiguate mid tvs tcs tcEnv expr1)
>                     (disambiguate mid tvs tcs tcEnv expr2)
>                     (disambiguate mid tvs tcs tcEnv expr3))
>   disambiguate mid tvs tcs tcEnv (UnaryMinus id expr) =
>     (UnaryMinus id (disambiguate mid tvs tcs tcEnv expr))
>   disambiguate mid tvs tcs tcEnv (Apply expr1 expr2) =
>     (Apply (disambiguate mid tvs tcs tcEnv expr1)
>            (disambiguate mid tvs tcs tcEnv expr2))
>   disambiguate mid tvs tcs tcEnv (InfixApply expr1 op expr2) =
>     (InfixApply (disambiguate mid tvs tcs tcEnv expr1) op
>                 (disambiguate mid tvs tcs tcEnv expr2))
>   disambiguate mid tvs tcs tcEnv (LeftSection expr op) =
>     (LeftSection (disambiguate mid tvs tcs tcEnv expr) op)
>   disambiguate mid tvs tcs tcEnv (RightSection op expr) =
>     (RightSection op (disambiguate mid tvs tcs tcEnv expr))
>   disambiguate mid tvs tcs tcEnv (Lambda constrs expr) =
>     (Lambda constrs (disambiguate mid tvs tcs tcEnv expr))
>   disambiguate mid tvs tcs tcEnv (Let decls expr) =
>     (Let (disambiguate mid tvs tcs tcEnv decls)
>          (disambiguate mid tvs tcs tcEnv expr))
>   disambiguate mid tvs tcs tcEnv (Do stats expr) =
>     (Do (disambiguate mid tvs tcs tcEnv stats)
>         (disambiguate mid tvs tcs tcEnv expr))
>   disambiguate mid tvs tcs tcEnv (IfThenElse expr1 expr2 expr3) =
>     (IfThenElse (disambiguate mid tvs tcs tcEnv expr1)
>                 (disambiguate mid tvs tcs tcEnv expr2)
>                 (disambiguate mid tvs tcs tcEnv expr3))
>   disambiguate mid tvs tcs tcEnv (Case expr alts) =
>     (Case (disambiguate mid tvs tcs tcEnv expr)
>           (disambiguate mid tvs tcs tcEnv alts))

> instance Ambiguous Statement where
>   disambiguate mid tvs tcs tcEnv (StmtExpr expr) =
>     (StmtExpr (disambiguate mid tvs tcs tcEnv expr))
>   disambiguate mid tvs tcs tcEnv (StmtDecl decls) =
>     (StmtDecl (disambiguate mid tvs tcs tcEnv decls))
>   disambiguate mid tvs tcs tcEnv (StmtBind constr expr) =
>     (StmtBind constr (disambiguate mid tvs tcs tcEnv expr))

> instance Ambiguous Alt where
>   disambiguate mid tvs tcs tcEnv (Alt p constr rhs) =
>     (Alt p constr (disambiguate mid tvs tcs tcEnv rhs))

> instance Ambiguous Goal where
>   disambiguate mid tvs tcs tcEnv (Goal p expr decls) =
>     (Goal p expr' decls')
>     where expr'  = disambiguate mid tvs tcs tcEnv expr
>           decls' = disambiguate mid tvs tcs tcEnv decls

\end{lstlisting}
