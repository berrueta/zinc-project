% $Id: TypeInferenceMonad.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeInferenceMonad.lhs}
\codesection{Type inference monad}
The type checker makes use of nested state monads in order to
maintain the type environment, the current substitution, and a counter
which is used for generating fresh type variables.
\begin{lstlisting}

> module TypeInferenceMonad where
> import ValueEnv
> import TypeSubst
> import Combined
> import Types

> type TcState a = StateT ValueEnv (StateT TypeSubst (StateT Int Id)) a

> run :: TcState a -> ValueEnv -> a
> run m tyEnv = runSt (callSt (callSt m tyEnv) idSubst) 0

\end{lstlisting}
