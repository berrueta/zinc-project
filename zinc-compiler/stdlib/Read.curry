module Read(ReadS, read, readDec, readHex, readOct, readSigned,
	    ParseS, parse, parseDec, parseHex, parseOct, parseSigned) where

import Char
import Success

type ReadS a = String -> [(a,String)]
type ParseS a = a -> String -> String

convertToInt :: Int -> Int -> String -> Int
convertToInt b n [] = n
convertToInt b n (c:cs) = convertToInt b (b * n + digitToInt c) cs

{- numeric conversions, Haskell style -}
readDec :: ReadS Int
readDec cs = if null cs' then [] else [(convertToInt 10 0 cs',cs'')]
  where (cs',cs'') = span isDigit cs

readHex :: ReadS Int
readHex cs = if null cs' then [] else [(convertToInt 16 0 cs',cs'')]
  where (cs',cs'') = span isHexDigit cs

readOct :: ReadS Int
readOct cs = if null cs' then [] else [(convertToInt 8 0 cs',cs'')]
  where (cs',cs'') = span isOctDigit cs

readSigned :: ReadS Int
readSigned [] = []
readSigned (c:cs) =
  case c of
    '+' -> readDec cs
    '-' -> [(-n,cs') | (n,cs') <- readDec cs]
    _ -> readDec (c:cs)

read :: ReadS a -> String -> a
read reads cs = choose [x | (x,cs') <- reads cs, null (dropWhile isSpace cs')]

{- numeric conversions, functional-logic style -}
parseDec :: ParseS Int
parseDec n cs | null cs' =:= False &> n =:= convertToInt 10 0 cs' = cs''
  where (cs',cs'') = span isDigit cs

parseHex :: ParseS Int
parseHex n cs | null cs' =:= False &> n =:= convertToInt 16 0 cs' = cs''
  where (cs',cs'') = span isHexDigit cs

parseOct :: ParseS Int
parseOct n cs | null cs' =:= False &> n =:= convertToInt 8 0 cs' = cs''
  where (cs',cs'') = span isOctDigit cs

parseSigned :: ParseS Int
parseSigned n (c:cs) =
  case c of
    '+' -> parseDec n cs
    '-' -> (cs' `seq` n =:= -n') ==> cs'
      where cs' = parseSigned n' cs
            n' free
    _ -> parseDec n (c:cs)

parse :: ParseS a -> String -> a
parse parser cs | null (dropWhile isSpace (parser x cs)) = x
  where x free
