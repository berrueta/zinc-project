module System(ExitCode(..),
              getArgs, getProgName, getEnv,
	      system, exitWith, exitFailure) where

data ExitCode = ExitSuccess | ExitFailure Int

external primitive getArgs     :: IO [String]
external primitive getProgName :: IO String
external primitive getEnv      :: String -> IO String

system :: String -> IO ExitCode
system cmd =
  do
    n <- system cmd
    return (if n == 0 then ExitSuccess else ExitFailure n)
  where external primitive system :: String -> IO Int

external primitive curryExit :: Int -> IO a

exitWith :: ExitCode -> IO a
exitWith ExitSuccess = curryExit 0
exitWith (ExitFailure n) = curryExit (if n == 0 then 1 else n)

exitFailure :: IO a
exitFailure = exitWith (ExitFailure 1)
