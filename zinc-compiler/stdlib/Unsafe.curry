module Unsafe where

external primitive unsafePerformIO :: IO a -> a

unsafeInterleaveIO :: IO a -> IO a
unsafeInterleaveIO m = return (unsafePerformIO m)
