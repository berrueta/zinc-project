module Numeric(showSigned, showIntAtBase, showInt, showOct, showHex,
               readSigned, readInt, readDec, readOct, readHex,
	       showEFloat, showFFloat, showGFloat, showFloat,
	       lexDigits) where
import Char

{- Missing Haskell Prelude definitions -}
type ShowS = String -> String
type ReadS a = String -> [(a,String)]
max x y = if x < y then y else x
{- end of Haskell Prelude definitions -}

showSigned :: (Int -> ShowS) -> Int -> Int -> ShowS
showSigned showPos p x
  | x < 0     = showParen (p > 6) (showChar '-' . showPos (-x))
  | otherwise = showPos x
  where showParen True shows = showChar '(' . shows . showChar ')'
        showParen False shows = shows
	showChar c = (c :)

showIntAtBase :: Int -> (Int -> Char) -> Int -> ShowS
showIntAtBase base intToDig n rest
  | n < 0 = error "Numeric.showIntAtBase: can't show negative numbers"
  | n' == 0 = rest'
  | otherwise = showIntAtBase base intToDig n' rest'
  where n' = n `div` base
        d  = n `mod` base
	rest' = intToDig d : rest

showInt :: Int -> ShowS
showInt = showIntAtBase 10 intToDigit

showOct :: Int -> ShowS
showOct = showIntAtBase 8 intToDigit

showHex :: Int -> ShowS
showHex = showIntAtBase 16 intToDigit

-- NB: In contrast to the definition in the Haskell library, this
--     implementation does not parse parenthesized numbers.
readSigned :: ReadS Int -> ReadS Int
readSigned readPos r =
  case dropWhile isSpace r of
    [] -> []
    (c:cs)
      | c == '-' -> [(-n,s) | (n,s) <- readPos cs]
      | otherwise -> readPos (c:cs)

-- NB: In constrast to the definition in the Haskell library, this
--     implementation can handle leading blanks
readInt :: Int -> (Char -> Bool) -> (Char -> Int) -> ReadS Int
readInt base isDig digToInt s =
  [(foldl1 (\n d -> n * base + d) (map digToInt ds),r)
  | (ds,r) <- nonnull isDig (dropWhile isSpace s)]

readDec :: ReadS Int
readDec = readInt 10 isDigit digitToInt

readOct :: ReadS Int
readOct = readInt 8 isOctDigit digitToInt

readHex :: ReadS Int
readHex = readInt 16 isHexDigit digitToInt


showEFloat :: Maybe Int -> Float -> ShowS
showEFloat d f = showEFloat (maybe (-1) (max 0) d) f
  where external primitive showEFloat :: Int -> Float -> ShowS

showFFloat :: Maybe Int -> Float -> ShowS
showFFloat d f = showFFloat (maybe (-1) (max 0) d) f
  where external primitive showFFloat :: Int -> Float -> ShowS

showGFloat :: Maybe Int -> Float -> ShowS
showGFloat d f
  | f' >= 0.1 && f' < 1.0e7 = showFFloat d f
  | otherwise = showEFloat d f
  where f' = if f < 0.0 then -. f else f
        
showFloat :: Float -> ShowS
showFloat = showGFloat Nothing

floatFormat :: Char -> Maybe Int -> String
floatFormat fmt decs =
  '%' : maybe "" (\d -> '.' : show (max 0 d)) decs ++ [fmt]
  where max x y = if x < y then y else x

lexDigits :: ReadS String
lexDigits = nonnull isDigit

nonnull p s = [(cs,t) | (cs@(_:_),t) <- [span p s]]
