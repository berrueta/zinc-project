module IO(Handle, HandlePosn, IOMode(..), BufferMode(..), SeekMode(..),
          stdin, stdout, stderr,
	  openFile, hClose, 
	  hIsOpen, hIsClosed, hIsReadable, hIsWritable, hIsSeekable,
	  isEOF, hIsEOF, hFileSize, 
	  hGetChar, hLookAhead, hGetLine, hGetContents,
	  hPutChar, hPutStr, hPutStrLn, hPrint,
	  hGetBuffering, hSetBuffering, hFlush,
	  hGetPosn, hSetPosn, hSeek,
	  tryIO, bracket, bracket_,
          {- entities re-exported from the prelude -}
	  IO, FilePath, IOError, ioError, catch, interact,
	  putChar, putStr, putStrLn, print, getChar, getLine, getContents,
	  readFile, writeFile, appendFile) where

data Handle
data HandlePosn	= HandlePosn Handle Int

data IOMode = ReadMode | WriteMode | AppendMode | ReadWriteMode
data BufferMode = NoBuffering | LineBuffering | BlockBuffering (Maybe Int)
data SeekMode = AbsoluteSeek | RelativeSeek | SeekFromEnd

--- Predefined handles for standard input, output, and error
external primitive stdin  :: Handle
external primitive stdout :: Handle
external primitive stderr :: Handle

--- Action to open a file. Returns a handle for the file if successful
--- and raises an IOError otherwise.
external primitive openFile :: FilePath -> IOMode -> IO Handle

--- Action to close a handle. A handle may safely be closed more than once.
external primitive hClose :: Handle -> IO ()

--- Action to check whether a handle is open. A handle is open until closed
--- explicitly with hClose or implicity when hGetContents was applied to
--- it and the file has been read.
external primitive hIsOpen     :: Handle -> IO Bool

--- Action to check whether a handle is closed. A handle is closed
--- explicitly when hClose or hGetContents is applied to it
external primitive hIsClosed   :: Handle -> IO Bool

--- Actions to check whether a handle is readable.
external primitive hIsReadable :: Handle -> IO Bool

--- Action to check whether a handle is writable. 
external primitive hIsWritable :: Handle -> IO Bool

--- Action to check whether a handle is seekable. 
external primitive hIsSeekable :: Handle -> IO Bool

--- Actions that check whether a (readable) handle is at the
--- end-of-file.
external primitive isEOF  :: IO Bool
external primitive hIsEOF :: Handle -> IO Bool

--- Action that returns the size of file.
external primitive hFileSize :: Handle -> IO Int

--- Action to read a single character from an open handle.
external primitive hGetChar :: Handle -> IO Char

--- Action that returns the next character from an open handle
--- but does not remove it from the file buffer
external primitive hLookAhead :: Handle -> IO Char

--- Action to read a single line from an open handle.
external primitive hGetLine :: Handle -> IO String

--- Action that (lazily) reads and closes the handle.
external primitive hGetContents :: Handle -> IO String

--- Action to write a character to an open handle.
external primitive hPutChar :: Handle -> Char -> IO ()

--- Action to write a string to an open handle.
external primitive hPutStr :: Handle -> String -> IO ()

--- Action to write a string with a newline to an open handle.
hPutStrLn :: Handle -> String -> IO ()
hPutStrLn h cs = hPutStr h cs >> hPutChar h '\n'

--- Action that converts a term into a strings and writes it to an open handle.
hPrint :: Show a => Handle -> a -> IO ()
hPrint h x = hPutStrLn h (show x)

--- Action to determine the current buffer mode of a handle.
external primitive hGetBuffering :: Handle -> IO BufferMode

--- Action to change the current buffer mode of a handle.
external primitive hSetBuffering :: Handle -> BufferMode -> IO ()

--- Action to flush all buffers associated with the handle.
external primitive hFlush :: Handle -> IO ()

--- Action to save the current I/O position of a handle.
hGetPosn :: Handle -> IO HandlePosn
hGetPosn h =
  do
    p <- hTell h
    return (HandlePosn h p)
  where external primitive hTell :: Handle -> IO Int

--- Action to restore the current I/O position of a handle.
hSetPosn :: HandlePosn -> IO ()
hSetPosn (HandlePosn h p) = hSeek h AbsoluteSeek p

--- Action to change the current I/O position of a handle.
external primitive hSeek :: Handle -> SeekMode -> Int -> IO ()

--- tryIO executes an IO action and either returns the exception value
--- or the result of the action.
tryIO :: IO a -> IO (Either IOError a)
tryIO m = catch (m >>= return . Right) (return . Left)

--- execute IO actions before and after an IO action
--- <CODE>bracket before after m</CODE> executes the actions in the
--- order before, m, and after. The IO action after is executed even
--- if an exception occurs in m. The result of before is passed to
--- m and after.
bracket :: IO a -> (a -> IO b) -> (a -> IO c) -> IO c
bracket before after m =
  do
    x <- before
    r <- catch (m x) (\ioe -> after x >> ioError ioe)
    after x
    return r

--- execute IO actions before and after an IO action
--- <CODE>bracket before after m</CODE> executes the actions in the
--- order before, m, and after. The IO action after is executed even
--- if an exception occurs in m. The result of before is passed to after.
bracket_ :: IO a -> (a -> IO b) -> IO c -> IO c
bracket_ before after m =
  do
    x <- before
    r <- catch m (\ioe -> after x >> ioError ioe)
    after x
    return r
