module Random where

data StdGen

external primitive genRange	     :: StdGen -> (Int,Int)
external primitive "nextStdGen" next :: StdGen -> (Int,StdGen)
external primitive mkStdGen	     :: Int -> StdGen

split :: StdGen -> (StdGen,StdGen)
split rng = (mkStdGen x,mkStdGen y)
  where (x,rng') = next rng
  	(y,_) = next rng'

random :: StdGen -> (Int,StdGen)
random = next

randomR :: (Int,Int) -> StdGen -> (Int,StdGen)
randomR (lo,hi) = nextRStdGen lo hi
  where external primitive nextRStdGen :: Int -> Int -> StdGen -> (Int,StdGen)

randoms :: StdGen -> [Int]
randoms rng = x : randoms rng'
  where (x,rng') = random rng

randomRs :: (Int,Int) -> StdGen -> [Int]
randomRs range rng = x : randomRs range rng'
  where (x,rng') = randomR range rng

randomIO :: IO Int
randomIO = getStdRandom random

randomRIO :: (Int,Int) -> IO Int
randomRIO range = getStdRandom (randomR range)

external primitive getStdGen :: IO StdGen
external primitive setStdGen :: StdGen -> IO ()

newStdGen :: IO StdGen
newStdGen =
  do
    rng <- getStdGen
    let (rng1,rng2) = split rng
    setStdGen rng1
    return rng2

getStdRandom :: (StdGen -> (a,StdGen)) -> IO a
getStdRandom random =
  do
    rng <- getStdGen
    let (x,rng') = random rng
    setStdGen rng'
    return x
