module Directory({-Permissions(..),-}
                 createDirectory, removeDirectory, removeFile,
		 renameDirectory, renameFile, getDirectoryContents,
		 getCurrentDirectory, setCurrentDirectory,
		 doesFileExist, doesDirectoryExist,
		 {-getPermissions, setPermissions,-}
		 getModificationTime) where

import Time(ClockTime)

-- data Permissions = Permissions {
--   readable, writable, executable, searchable :: Bool
-- }

external primitive createDirectory :: FilePath -> IO ()
external primitive removeDirectory :: FilePath -> IO ()
external primitive removeFile	   :: FilePath -> IO ()
external primitive renameDirectory :: FilePath -> FilePath -> IO ()
external primitive renameFile	   :: FilePath -> FilePath -> IO ()

external primitive getDirectoryContents :: FilePath -> IO [FilePath]
external primitive getCurrentDirectory :: IO FilePath
external primitive setCurrentDirectory :: FilePath -> IO ()

external primitive doesFileExist      :: FilePath -> IO Bool
external primitive doesDirectoryExist :: FilePath -> IO Bool

-- external primitive getPermissions :: FilePath -> IO Permissions
-- external primitive setPermissions :: FilePath -> Permissions -> IO ()

external primitive getModificationTime :: FilePath -> IO ClockTime
