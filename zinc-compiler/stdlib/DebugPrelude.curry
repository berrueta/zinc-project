
-- Prelude to be included  (automatically) in all the programs
-- transformed for debugging purposes
-- Rafa 03-07-2001

module DebugPrelude where

external primitive dval :: a -> String

-- data type representing computation trees		   	
data CTree = CTreeNode String [String] String String [CTree] | 
             EmptyCTreeNode [CTree] | CTreeVoid


-- This function removes all the trees corresponding to non-demanded values
-- It not only saves time and space but it is neccessary for avoiding 
-- non terminating computations
clean :: [(String,CTree)] -> [CTree]
clean []         = []
clean ((p,x):xs) = if p=="_" 
		   then clean xs
		   else case x of
                        CTreeVoid  -> rest
		   	EmptyCTreeNode trees -> trees ++ rest
                        CTreeNode _ _ _ _ _ -> x : rest
		    where 
			 rest = clean xs
		

-- 08-05-02 Try defined for debugging
try' :: (a -> (Success,CTree)) -> ([a -> (Success,CTree)], CTree)
try' g = (map wrap (try (unwrap g)), CTreeVoid)

unwrap :: (a -> (Success,b)) -> (a,b) -> Success
unwrap   g (x,t) | r = t =:= t' where (r,t') = g x 
wrap :: ((a,b) -> Success) -> a -> (Success,b)
wrap     g x | g (x,t) = (success,t) where t free


startDebugging :: ((a,CTree) -> Success) -> IO ()
startDebugging  = navigate . snd . head . findall

-- rhs=debugging for navigating, rhs=prettyTree for pretty printing
navigate :: CTree -> IO ()
navigate =   debugging

debugging :: CTree -> IO ()
debugging (CTreeNode name args result rule trees) = 
	do
	     putStrLn "" 
             putStrLn "Entering debugger..." 
             bugFound <- buggyChildren trees
             putStrLn "" 
             if bugFound then putStrLn "Buggy node found"
                else putStrLn "No error has been found"
             putStrLn "Debugger exiting" 

buggyTree :: CTree -> IO Bool             
buggyTree CTreeVoid = return False
buggyTree (EmptyCTreeNode trees) = buggyChildren trees
buggyTree (CTreeNode name args result rule trees) = 
      buggyChildren trees >>= \b -> if b then return True
    		                     else putStrLn "" >> putStrLn (isBuggy n) >>
    		                          return True
  where  n = CTreeNode name args result rule trees	                     

basicArrow :: CTree -> String
basicArrow (CTreeNode name args result rule trees) = 
    name++"("++restOfList args ")"++"  -> "++result

isBuggy :: CTree -> String
isBuggy (CTreeNode name args result rule trees) = 
	" ** Function "++name++" is incorrect ( " ++
	basicArrow (CTreeNode name args result rule trees) ++") **"

buggyChildren :: [CTree] -> IO Bool
buggyChildren [] = return False
buggyChildren (x:xs) = 
	do
	putStrLn " "
	putStrLn "Considering the following basic facts: "
	mapM putStrLn listArrows
	putStrLn "Are all of them valid? (y/n)"
    	yes <- answerYes
	if yes then return False
 	 else chooseOne l >>= \n -> putStrLn "" >> buggyTree ((x:xs)!!(n-1))
 	 
 where 
	l = length (x:xs)
	listN = zip [1..l]  (x:xs)
	listArrows = map (\(x,y) -> (show x)++". "++basicArrow y) listN

answerYes :: IO Bool	   
answerYes = 
  getLine >>= \l -> if l=="y" || l=="Y" then return True
                     else  if l=="n" || l=="N" then return False
                           else putStrLn "(y/n)" >> answerYes

chooseOne :: Int -> IO Int
chooseOne max =
      if max==1 then return 1
      else
 	do
 	 putStrLn "Write the number of an erroneous basic fact in the list "
	 n <- getLine
	 let valueN  = foldl (\x y->x*10+(ord(y)-ord('0'))) 0 n in
	  if valueN<0 || valueN>max then chooseOne max
        	                     else return valueN 

----------------------------------------------------------------------------

--
-- Pretty (not really) Printing of the computation trees
--

prettyTree:: CTree -> IO ()
prettyTree = ppCTs 0
 
ppCTs :: Int -> CTree -> IO ()
ppCTs i CTreeVoid =	putStrLn "DebugPrelude.CTreeVoid"
	
ppCTs i (EmptyCTreeNode trees) = 
	do 
	   ident i
	   ppTChildren i trees
	   putStrLn ""

ppCTs i (CTreeNode name args result rule trees) = 
	do 
	   ident i
	   putStr "CTreeNode " 
	   putStr (name++" ")
	   putStr ("["++(concat (map (" "++) args))++"] ")
	   putStr  (result++" ")
	   putStr rule
	   ppTChildren i trees
	   putStrLn ""

ident :: Int -> IO ()
ident n = putStr (take n (repeat ' '))

ppTChildren :: Int -> [CTree] -> IO ()
ppTChildren i []  = putStr "[]"
ppTChildren i (x:xs)  = 
	do
	   putStrLn ""
	   ident (i+2)
	   putStrLn "["
	   mapM (ppCTs (i+3)) (x:xs)
   	   ident (i+2)
   	   putStrLn "]"

mapM :: (a -> IO b ) -> [a] -> IO [b]
mapM f [] = return []
mapM f (x:xs) = f x >>= \a -> mapM f xs >>= \al -> return (a:al)

dEval:: a -> String
dEval = dval

restOfList:: [String] -> String -> String
restOfList []     c = c
restOfList (x:xs) c = x ++ comma ++ (restOfList xs c)
	where comma =  if (null xs) then "" else ", "
		   	
