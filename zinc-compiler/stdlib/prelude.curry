module prelude where

-- Lines beginning with "--++" are part of the prelude, but are already
-- predefined by the compiler (or cannot be defined at all)

-- Infix operator declarations:

infixl 9 !!
infixr 9 .
infixl 7 *, `div`, `mod`, *., /.
infixl 6 +, -, +., -.
--++ infixr 5 :
infixr 5 ++
infix  4 =:=, =/=, ==, /=, <, >, <=, >=
infix  4 `elem`, `notElem`
infixr 3 &&
infixr 2 ||
infixl 1 >>, >>=
infixr 0 $, $!, `seq`, &, &>

-- Some standard combinators:

--- Function composition.
(.)   :: (b -> c) -> (a -> b) -> (a -> c)
(f . g) x = f (g x)

--- Identity function.
id              :: a -> a
id x            = x

--- Constant function.
const           :: a -> b -> a
const x _       = x

--- Converts an uncurried function to a curried function.
curry           :: ((a,b) -> c) -> a -> b -> c
curry f a b     =  f (a,b)

--- Converts an curried function to a function on pairs.
uncurry         :: (a -> b -> c) -> (a,b) -> c
uncurry f (a,b) = f a b

--- (flip f) is identical to f but with the order of arguments reversed.
flip            :: (a -> b -> c) -> b -> a -> c
flip  f x y     = f y x

--- Right-associative application.
($)             :: (a -> b) -> a -> b
f $ x           = f x

--- Evaluate the first argument to head  normal form and return the
--- second argument. Suspend if the first argument evaluats to a
--- free variable.
external primitive seq :: a -> b -> b

--- Right-associative application with strict evaluation of its argument.
($!)		:: (a -> b) -> a -> b
f $! x		= x `seq` f x

--- Repeat application of a function until a predicate holds.
until          :: (a -> Bool) -> (a -> a) -> a -> a
until p f x     = if p x then x else until p f (f x)

--- Abort the execution with an error message.
error :: String -> a
error msg = unsafePerformIO (abort ("Error: " ++ msg ++ "\n"))
  where abort msg = hPutStr stderr msg >> curryExit 1
	external primitive stderr :: Handle
	external primitive hPutStr :: Handle -> String -> IO ()
	external primitive unsafePerformIO :: IO a -> a
 	external primitive curryExit :: Int -> IO a

--- The totally undefined function.
undefined :: a
undefined = error "prelude.undefined"

--- failed is a non-reducible polymorphic function.
--- It is useful to express a failure in a search branch of the execution.
external primitive failed :: a


-- Boolean values
-- already defined as builtin, since it is required for if-then-else
data Bool = False | True
          deriving (Eq, Ord, Enum, Show, Bounded)

--- Sequential conjunction on Booleans.
(&&)            :: Bool -> Bool -> Bool
True  && x      = x
False && _      = False

--- Sequential disjunction on Booleans.
(||)            :: Bool -> Bool -> Bool
True  || _      = True
False || x      = x

--- Negation.
not             :: Bool -> Bool
not True        = False
not False       = True

--- Conditional.
if_then_else	:: Bool -> a -> a -> a
if_then_else eval rigid
if_then_else True  t _ = t
if_then_else False _ f = f

--- Useful name for the last condition in a sequence of conditional equations.
otherwise       :: Bool
otherwise       = True

-- Ordering
data Ordering = LT | EQ | GT
              deriving (Eq, Ord, Enum, Show, Bounded)

--- Polymorphic comparisons
external primitive primCompare :: a -> a -> Ordering


-- Type class Eq

class Eq a where
  (==) :: a -> a -> Bool

--- Disequality (should be a method of Ord)
(/=) :: Eq a => a -> a -> Bool
(/=) x y = not (x == y)

-- Instances of Eq

instance Eq Int where
  x == y = case primCompare x y of EQ -> True; _ -> False

instance Eq Float where
  x == y = case primCompare x y of EQ -> True; _ -> False

instance Eq Char where
  x == y = case primCompare x y of EQ -> True; _ -> False

instance Eq a => Eq [a] where
  (==) eval rigid
  []     == []     = True
  (x:xs) == (y:ys) = (x == y) && (xs == ys)
  []     == (_:_)  = False
  (_:_)  == []     = False

instance (Eq a1, Eq a2) => Eq (a1,a2) where
  (==) eval rigid
  (x1,x2) == (y1,y2) = (x1 == y1) && (x2 == y2)

instance (Eq a1, Eq a2, Eq a3) => Eq (a1,a2,a3) where
  (==) eval rigid
  (x1,x2,x3) == (y1,y2,y3) = (x1 == y1) && (x2 == y2) && (x3 == y3)

-- Type class Ord

class Eq a => Ord a where
  compare :: a -> a -> Ordering

-- Instances of Ord

instance Ord Int where
  compare = primCompare

instance Ord Float where
  compare = primCompare

instance Ord Char where
  compare = primCompare

instance Ord a => Ord [a] where
  compare eval rigid
  []     `compare` []     = EQ
  []     `compare` (_:_)  = LT
  (_:_)  `compare` []     = GT
  (x:xs) `compare` (y:ys) =
    case x `compare` y of
      LT -> LT
      EQ -> compare xs ys
      GT -> GT

instance (Ord a, Ord b) => Ord (a,b) where
  compare eval rigid
  (x1,y1) `compare` (x2,y2) =
    case x1 `compare` x2 of
      LT -> LT
      EQ -> y1 `compare` y2
      GT -> GT

instance (Ord a, Ord b, Ord c) => Ord (a,b,c) where
  compare eval rigid
  (x1,y1,z1) `compare` (x2,y2,z2) =
    case x1 `compare` x2 of
      LT -> LT
      EQ -> case y1 `compare` y2 of
      	      LT -> LT
	      EQ -> z1 `compare` z2
	      GT -> GT
      GT -> GT

--- Polymorphic less-than (should be a method of Ord)
(<) :: Ord a => a -> a -> Bool
x < y = case compare x y of LT -> True; _ -> False

--- Polymorphic greater-than (should be a method of Ord)
(>) :: Ord a => a -> a -> Bool
x > y = case compare x y of GT -> True; _ -> False

--- Polymorphic less-or-equal (should be a method of Ord)
(<=) :: Ord a => a -> a -> Bool
x <= y = not (x > y)

--- Polymorphic greater-or-equal (should be a method of Ord)
(>=) :: Ord a => a -> a -> Bool
x >= y = not (x < y)

-- Pairs

--++ data (a,b) = (a,b)

--- Selects the first component of a pair.
fst             :: (a,b) -> a
fst (x,_)       = x

--- Selects the second component of a pair.
snd             :: (a,b) -> b
snd (_,y)       = y


-- Unit type
--++ data () = ()


-- Lists

--++ data [a] = [] | a : [a]

--- Computes the first element of a list.
head            :: [a] -> a
head (x:_)      = x

--- Computes the remaining elements of a list.
tail            :: [a] -> [a]
tail (_:xs)     = xs

--- Is a list empty?
null            :: [a] -> Bool
null []         = True
null (_:_)      = False

--- Concatenates two lists.
(++)            :: [a] -> [a] -> [a]
[]     ++ ys    = ys
(x:xs) ++ ys    = x : xs++ys

--- Computes the length of a list.
length          :: [a] -> Int
length		= count 0
  where count n []    		 = n
  	count n (_:xs) | n'=:=n' = count n' xs where n' = n + 1
  	-- NB: Do not replace n'=:=n' = ... by n' `seq` ... because this
        --     can lead to a stack overflow in the current implementation

--- List index (subscript) operator, head has index 0
(!!)              :: [a] -> Int -> a
(x:xs) !! n
  | n == 0        = x
  | n > 0         = xs !! (n-1)

--- Map a function on all elements of a list.
map             :: (a -> b) -> [a] -> [b]
map _ []        = []
map f (x:xs)    = f x : map f xs

--- Accumulates all list elements by applying a binary operator from
--- left to right. Thus,
--- <CODE>foldl f z [x1,x2,...,xn] = (...((z `f` x1) `f` x2) ...) `f` xn</CODE>
foldl            :: (a -> b -> a) -> a -> [b] -> a
foldl _ z []     = z
foldl f z (x:xs) = foldl f (f z x) xs

--- Accumulates a non-empty list from left to right.
foldl1           :: (a -> a -> a) -> [a] -> a
foldl1 f (x:xs)  = foldl f x xs

--- Accumulates all list elements by applying a binary operator from
--- right to left. Thus,
--- <CODE>foldr f z [x1,x2,...,xn] = (x1 `f` (x2 `f` ... (xn `f` z)...))</CODE>
foldr            :: (a -> b -> b) -> b -> [a] -> b
foldr _ z []     = z
foldr f z (x:xs) = f x (foldr f z xs)

--- Accumulates a non-empty list from right to left:
foldr1              :: (a -> a -> a) -> [a] -> a
foldr1 _ [x]        = x
foldr1 f (x1:x2:xs) = f x1 (foldr1 f (x2:xs))

--- Filters all elements satisfying a given predicate in a list.
filter            :: (a -> Bool) -> [a] -> [a]
filter _ []       = []
filter p (x:xs)   = if p x then x : filter p xs
                           else filter p xs

--- Joins two lists into one list of pairs. If one input list is shorter than
--- the other, the additional elements of the longer list are discarded.
zip               :: [a] -> [b] -> [(a,b)]
zip		  = zipWith (,)

--- Joins three lists into one list of triples. If one input list is shorter
--- than the other, the additional elements of the longer lists are discarded.
zip3              :: [a] -> [b] -> [c] -> [(a,b,c)]
zip3		  = zipWith3 (,,)

--- Joins two lists into one list by applying a combination function to
--- corresponding pairs of elements. Thus <CODE>zip = zipWith (,)</CODE>
zipWith                 :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith _ []     _      = []
zipWith _ (_:_)  []     = []
zipWith f (x:xs) (y:ys) = f x y : zipWith f xs ys

--- Joins three lists into one list by applying a combination function to
--- corresponding triples of elements. Thus <CODE>zip3 = zipWith3 (,,)</CODE>
zipWith3	  :: (a -> b -> c -> d) -> [a] -> [b] -> [c] -> [d]
zipWith3 _ []     _      _      = []
zipWith3 _ (_:_)  []     _      = []
zipWith3 _ (_:_)  (_:_)  []     = []
zipWith3 f (x:xs) (y:ys) (z:zs) = f x y z : zipWith3 f xs ys zs

--- Transforms a list of pairs into a pair of lists.
unzip               :: [(a,b)] -> ([a],[b])
unzip []            = ([],[])
unzip ((x,y):ps)    = (x:xs,y:ys) where (xs,ys) = unzip ps

--- Transforms a list of triples into a triple of lists.
unzip3              :: [(a,b,c)] -> ([a],[b],[c])
unzip3 []           = ([],[],[])
unzip3 ((x,y,z):ts) = (x:xs,y:ys,z:zs) where (xs,ys,zs) = unzip3 ts

--- Concatenates a list of lists into one list.
concat            :: [[a]] -> [a]
concat l          = foldr (++) [] l

--- Maps a function from elements to lists and merges the result into one list.
concatMap         :: (a -> [b]) -> [a] -> [b]
concatMap f xs    = concat (map f xs)

--- Infinite list of repeated applications of a function f to an element x.
--- Thus, <CODE>iterate f x = [x, f x, f (f x),...]</CODE>
iterate           :: (a -> a) -> a -> [a]
iterate f x       = x : iterate f (f x)

--- Infinite list where all elements have the same value.
--- Thus, <CODE>repeat x = [x, x, x,...]</CODE>
repeat            :: a -> [a]
repeat x          = xs where xs = x : xs

--- List of length n where all elements have the same value.
replicate         :: Int -> a -> [a]
replicate n x     = take n (repeat x)

--- Returns prefix of length n.
take              :: Int -> [a] -> [a]
take n l          = if n<=0 then [] else takep n l
   where takep _ []     = []
         takep n (x:xs) = x : take (n-1) xs

--- Returns suffix without first n elements.
drop              :: Int -> [a] -> [a]
drop n l          = if n<=0 then l else dropp n l
   where dropp _ []     = []
         dropp n (_:xs) = drop (n-1) xs

--- (splitAt n xs) is equivalent to (take n xs, drop n xs)
splitAt		  :: Int -> [a] -> ([a],[a])
splitAt n l	  = if n <= 0 then ([],l) else splitAtp n l
  where splitAtp _ []     = ([],[])
	splitAtp n (x:xs) = let (ys,zs) = splitAt (n-1) xs in (x:ys,zs)

--- Returns longest prefix with elements satisfying a predicate.
takeWhile          :: (a -> Bool) -> [a] -> [a]
takeWhile _ []     = []
takeWhile p (x:xs) = if p x then x : takeWhile p xs else []

--- Returns suffix without takeWhile prefix.
dropWhile          :: (a -> Bool) -> [a] -> [a]
dropWhile _ []     = []
dropWhile p (x:xs) = if p x then dropWhile p xs else x:xs

--- (span p xs) is equivalent to (takeWhile p xs, dropWhile p xs)
span               :: (a -> Bool) -> [a] -> ([a],[a])
span _ []          = ([],[])
span p (x:xs)
  | p x	      = let (ys,zs) = span p xs in (x:ys,zs)
  | otherwise = ([],x:xs)

--- (break p xs) is equivalent to (takeWhile (not.p) xs, dropWhile (not.p) xs).
--- Thus, it breaks a list at the first occurrence of an element satisfying p.
break              :: (a -> Bool) -> [a] -> ([a],[a])
break p            = span (not . p)

--- Reverses the order of all elements in a list.
reverse    :: [a] -> [a]
reverse    = foldl (flip (:)) []

--- Computes the conjunction of a Boolean list.
and        :: [Bool] -> Bool
and []     = True
and (x:xs) = if x then and xs else False

--- Computes the disjunction of a Boolean list.
or         :: [Bool] -> Bool
or []      = False
or (x:xs)  = if x then True else or xs

--- Is there an element in a list satisfying a given predicate?
any        :: (a -> Bool) -> [a] -> Bool
any p xs   = or (map p xs)

--- Is a given predicate satisfied by all elements in a list?
all        :: (a -> Bool) -> [a] -> Bool
all p xs   = and (map p xs)

--- Element of a list?
elem       :: Eq a => a -> [a] -> Bool
elem x     = any (x==)

--- Not element of a list?
notElem    :: Eq a => a -> [a] -> Bool
notElem x  = all (x/=)

--- Looks up a key in an association list
lookup		     :: Eq a => a -> [(a,b)] -> Maybe b
lookup x []	     = Nothing
lookup x ((y,z):yzs) = if x == y then Just z else lookup x yzs



-- character type

-- Characters
data Char

--- Converts a characters into its ASCII value.
external primitive ord :: Char -> Int

--- Converts an ASCII value into a character.
external primitive chr :: Int -> Char


-- Strings

type String = [Char]

--- Breaks a string into a list of lines where a line is terminated at a
--- newline character. The resulting lines do not contain newline characters.
lines        :: String -> [String]
lines []     = []
lines (c:cs) = l : lines' s'
  where (l,s') = break ('\n' ==) (c:cs)
	lines' []    = []
	lines' (_:s'') = lines s''

--- Concatenates a list of strings with terminating newlines.
unlines    :: [String] -> String
unlines ls = concatMap (++ "\n") ls

--- Breaks a string into a list of words where the words are delimited by
--- white spaces.
words      :: String -> [String]
words cs   =
  case dropWhile isSpace cs of
    "" -> []
    cs' -> let (w,cs'') = break isSpace cs' in w : words cs''
  where isSpace c = c `elem` " \t\n\r\f\v\xA0"

--- Concatenates a list of strings with a blank between two strings.
unwords    	:: [String] -> String
unwords []	 = []
unwords (w:ws) = w ++ foldr (\w cs -> (' ' : w) ++ cs) "" ws

--- Converts an arbitrary term into an external string representation.
external primitive primShow :: a -> String

class Show a where
  show :: a -> String

instance Show Int where
  show x = primShow x

instance Show Float where
  show x = primShow x

instance Show Char where
  show x = primShow x

instance Show Success where
  show x = primShow x

instance Show a => Show [a] where
  show x = primShow x  -- needed to proper showing of Strings

instance (Show a1, Show a2) => Show (a1,a2) where
  show eval rigid
  show (x1,x2) = primShow (x1,x2)

instance (Show a1, Show a2, Show a3) => Show (a1,a2,a3) where
  show eval rigid
  show (x1,x2,x3) = primShow (x1,x2,x3)

instance (Show a1, Show a2, Show a3, Show a4) => Show (a1,a2,a3,a4) where
  show eval rigid
  show (x1,x2,x3,x4) = primShow (x1,x2,x3,x4)

instance (Show a1, Show a2, Show a3, Show a4, Show a5) =>
         Show (a1,a2,a3,a4,a5) where
  show eval rigid
  show (x1,x2,x3,x4,x5) = primShow (x1,x2,x3,x4,x5)

--show eval rigid
--show :: a -> String
--show x = primShow x

-- Type class Num

class Num a where
  (+), (-), (*) :: a -> a -> a

-- Types of primitive arithmetic functions and predicates

data Int
external primitive primAddInt :: Int -> Int -> Int
external primitive primSubInt :: Int -> Int -> Int
external primitive primMulInt :: Int -> Int -> Int
external primitive div :: Int -> Int -> Int
external primitive mod :: Int -> Int -> Int

instance Num Int where
  (+) = primAddInt
  (-) = primSubInt
  (*) = primMulInt

negate :: Int -> Int
negate n = 0 - n

data Float
external primitive primAddFloat :: Float -> Float -> Float
external primitive primSubFloat :: Float -> Float -> Float
external primitive primMulFloat :: Float -> Float -> Float
external primitive primDivFloat :: Float -> Float -> Float

instance Num Float where
  (+) = primAddFloat
  (-) = primSubFloat
  (*) = primMulFloat

-- back-compatibility
(+.), (-.), (*.) :: Float -> Float -> Float
(+.) = primAddFloat
(-.) = primSubFloat
(*.) = primMulFloat

(/.) :: Float -> Float -> Float
(/.) = primDivFloat

negateFloat :: Float -> Float
negateFloat f = 0.0 -. f

-- conversions
external primitive floatFromInt  :: Int -> Float
external primitive truncateFloat :: Float -> Int
external primitive roundFloat    :: Float -> Int

-- Bounded class
class Bounded a where
  minBound, maxBound :: a

instance Bounded () where
  minBound = ()
  maxBound = ()

instance Bounded Char where
  minBound = '\0'
  maxBound = primMaxChar where external primitive primMaxChar :: Char

instance Bounded Int where
  minBound = primMinInt where external primitive primMinInt :: Int
  maxBound = primMaxInt where external primitive primMaxInt :: Int

instance (Bounded a,Bounded b) => Bounded (a,b) where
  minBound = (minBound,minBound)
  maxBound = (maxBound,maxBound)

instance (Bounded a,Bounded b,Bounded c) => Bounded (a,b,c) where
  minBound = (minBound,minBound,minBound)
  maxBound = (maxBound,maxBound,maxBound)

-- Enum class
class Enum a where
  succ, pred :: a -> a
  toEnum     :: Int -> a
  fromEnum   :: a -> Int

instance Enum () where
  succ () = failed
  pred () = failed
  toEnum 0 = ()
  fromEnum () = 0

instance Enum Char where
  succ c | c < maxBound = chr (ord c + 1)
  pred c | c > minBound = chr (ord c - 1)
  toEnum = chr
  fromEnum = ord

instance Enum Int where
  succ x = x + 1
  pred x = x - 1
  toEnum x = x
  fromEnum x = x

-- The following functions should be generalized to Enum instances
-- At present, this does not work due to a problem in the compiler.

--- Generates an infinite sequence of ascending integers.
enumFrom               :: Int -> [Int]                   -- [n..]
enumFrom n             = iterate (1 +) n

--- Generates an infinite sequence of integers with a particular in/decrement.
enumFromThen           :: Int -> Int -> [Int]            -- [n1,n2..]
enumFromThen n1 n2     = iterate ((n2 - n1) +) n1

--- Generates a sequence of ascending integers.
enumFromTo             :: Int -> Int -> [Int]            -- [n..m]
enumFromTo n m         = takeWhile (m >=) (enumFrom n)

--- Generates a sequence of integers with a particular in/decrement.
enumFromThenTo         :: Int -> Int -> Int -> [Int]     -- [n1,n2..m]
enumFromThenTo n1 n2 m =
  takeWhile (if n1 <= n2 then (m >=) else (m <=)) (enumFromThen n1 n2)


-- Constraints
data Success

--- Equational constraint
external primitive (=:=) :: a -> a -> Success

--- Disequality constraint
external primitive (=/=) :: a -> a -> Success

--- Always satisfiable constraint
external primitive success :: Success

--- Concurrent conjunction of constraints
external primitive (&) :: Success -> Success -> Success

--- Sequential conjunction of constraints
(&>) :: Success -> Success -> Success
c1 &> c2 | c1 = c2


-- Maybe type

data Maybe a = Nothing | Just a
             deriving (Eq, Ord, Show)

maybe		   :: b -> (a -> b) -> Maybe a -> b
maybe z _ Nothing  = z
maybe _ f (Just x) = f x


-- Either type

data Either a b = Left a | Right b
                deriving (Eq, Ord, Show)

either		     :: (a -> c) -> (b -> c) -> Either a b -> c
either f _ (Left x)  = f x
either _ g (Right x) = g x


-- Monadic IO

data IO a  -- conceptually: World -> (a,World)

type FilePath = String

external primitive done   :: IO ()
external primitive return :: a -> IO a
external primitive (>>)   :: IO a -> IO b        -> IO b
external primitive (>>=)  :: IO a -> (a -> IO b) -> IO b

--- Action that (lazily) reads file and returns its contents.
external primitive readFile   :: FilePath -> IO String

--- Actions that writes a file.
external primitive writeFile  :: FilePath -> String -> IO ()

--- Actions that appends a string to a file.
external primitive appendFile :: FilePath -> String -> IO ()

--- Action to read a single character from stanard input.
external primitive getChar     :: IO Char

--- Action to read a single line from standard input.
external primitive getLine     :: IO String

--- Action that (lazily) reads all of standard input.
external primitive getContents :: IO String

--- Action to print a character on stdout.
external primitive putChar     :: Char -> IO ()

--- Action to print a string on stdout.
external primitive putStr :: String -> IO ()
 
--- Action to print a string with a newline on stdout.
putStrLn          :: String -> IO ()
putStrLn cs       = putStr cs >> putChar '\n'

--- Converts a term into a string and prints it.
print             :: Show a => a -> IO ()
print t           = putStrLn (show t)

--- Convert a simple stream filter into an I/O program
--- Note: interact closes the standard input
interact     	  :: (String -> String) -> IO ()
interact f = getContents >>= putStr . f

--- Solves a constraint as an I/O action.
--- Note: the constraint should be always solvable in a deterministic way
doSolve :: Success -> IO ()
doSolve constraint | constraint = done


-- IO monad auxiliary functions

--- Execute a sequence of I/O actions and collect all results in a list
sequenceIO :: [IO a] -> IO [a]
sequenceIO = foldr mcons (return [])
  where mcons m ms = do x <- m; xs <- ms; return (x:xs)

--- Execute a sequence of I/O actions and ignore the result
sequenceIO_ :: [IO a] -> IO ()
sequenceIO_ = foldr (>>) done

--- Map an I/O action function on a list of elements.
--- The results are of all I/O actions are collected in a list.
mapIO :: (a -> IO b) -> [a] -> IO [b]
mapIO f ms = sequenceIO (map f ms)

--- Map an I/O action function on a list of elements.
--- The results are of all I/O actions are ignored.
mapIO_ :: (a -> IO b) -> [a] -> IO ()
mapIO_ f ms = sequenceIO_ (map f ms)


-- IO Exceptions
-- At present exception values are strings, this will change in the future
type IOError = String

--- Raise an I/O exception
external primitive ioError :: IOError -> IO a

--- Establish an exception handler for the execution of an action
external primitive catch :: IO a -> (IOError -> IO a) -> IO a


----------------------------------------------------------------
-- Encapsulated search:

--- Basic search control operator.
external primitive try :: (a -> Success) -> [a -> Success]

--- Inject operator which adds the application of the unary
--- procedure p to the search variable to the search goal
--- taken from Oz. p x comes before g x to enable a test+generate
--- form in a sequential implementation.
inject	   :: (a -> Success) -> (a -> Success) -> (a -> Success) 
inject g p = \x -> p x & g x

--- Compute all solutions via a left-to-right strategy.
solveAll   :: (a -> Success) -> [a -> Success]
solveAll g = all (try g) []
  where all []           gs  = all2 gs
        all [g]          gs  = g : all2 gs 
        all (g:gs@(_:_)) gs' = all (try g) (gs ++ gs')

        all2 []     = []
        all2 (g:gs) = all (try g) gs


--- Get the first solution via left-to-right strategy.
once :: (a -> Success) -> a -> Success
once g = head (solveAll g)

{-
--- Try to find a solution to a search goal via a fair search.
--- Fail if there is no solution.
tryone         :: (a -> Success) -> a
tryone         eval choice
tryone g | g x = x  where x free

--- Search for one solution via a fair strategy ("tryone").
--- To control failure and to return the solution in a lambda abstraction,
--- encapsulate the search with "solveAll".
one     :: (a -> Success) -> [a -> Success]
one g   = solveAll (\x -> x =:= tryone g)

--- Search the first solution that meets a specified condition.
--- "cond" must be a unary Boolean function.
condSearch        :: (a -> Success) -> (a -> Bool) -> [a -> Success]
condSearch g cond = one (\x -> g x & cond x =:= True)
 -}

--- Get the best solution via left-to-right strategy according to
--- a specified operator that can always take a decision which
--- of two solutions is better.
--- In general, the comparison operation should be rigid in its arguments!
best    :: (a -> Success) -> (a -> a -> Bool) -> [a -> Success]
best g compare = bestHelp [] (try g) []
 where
   bestHelp [] []     curbest = curbest
   bestHelp [] (y:ys) curbest = evalB (try (constrain y curbest)) ys curbest
   bestHelp (x:xs) ys curbest = evalA (try x) xs ys curbest
   
   evalB []         ys curbest = bestHelp [] ys curbest
   evalB [newbest]  ys _       = bestHelp [] ys [newbest]
   evalB (x1:x2:xs) ys curbest = bestHelp (x1:x2:xs) ys curbest
   
   evalA []         xs ys curbest = bestHelp xs ys curbest
   evalA [newbest]  xs ys _       = bestHelp [] (xs++ys) [newbest]
   evalA (u1:u2:us) xs ys curbest = bestHelp ((u1:u2:us)++xs) ys curbest
   
   constrain b []        = b
   constrain b [curbest] =
       inject b (\x -> let y free in curbest y & compare x y =:= True)


--- Get all solutions via left-to-right strategy and unpack
--- the values from the lambda-abstractions.
--- Similar to Prolog's findall.
findall :: (a -> Success) -> [a]
findall g = map unpack (solveAll g)


--- Get the first solution via left-to-right strategy
--- and unpack the values from the search goals.
findfirst :: (a -> Success) -> a
findfirst g = head (findall g)

--- Show the solution of a solved constraint.
browse  :: Show a => (a -> Success) -> IO ()
browse g = putStr (show (unpack g))

--- Unpack solutions from a list of lambda abstractions and write 
--- them to the screen.
browseList :: Show a => [a -> Success] -> IO ()
browseList [] = done
browseList (g:gs) = browse g >> putChar '\n' >> browseList gs


--- Unpack a solution's value from a (solved) search goal.
unpack  :: (a -> Success) -> a
unpack g | g x  = x  where x free

