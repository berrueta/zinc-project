module Trace where

import Unsafe(unsafePerformIO)

trace :: String -> a -> a
trace msg x = unsafePerformIO (putStr msg) `seq` x
