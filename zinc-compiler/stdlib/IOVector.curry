module IOVector where

-- Primitive vectors

data IOVector a

external primitive newIOVector    :: Int -> a -> IO (IOVector a)
external primitive copyIOVector   :: IOVector a -> IO (IOVector a)
external primitive readIOVector   :: IOVector a -> Int -> IO a
external primitive writeIOVector  :: IOVector a -> Int -> a -> IO ()
external primitive lengthIOVector :: IOVector a -> Int
