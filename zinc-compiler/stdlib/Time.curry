module Time where

data ClockTime

external primitive getClockTime :: IO ClockTime
