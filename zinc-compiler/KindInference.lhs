% $Id: KindInference.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{KindInference.lhs}
\codesection{Kind inference}
\begin{lstlisting}

> module KindInference where
> import Kind
> import KindSubst
> import KindUnification
> import KindErrors
> import Base
> import Set
> import Position
> import Ident
> import TypeExpr
> import CurrySyntax
> import SCC
> import CurrySyntaxUtils
> import Monad
> import KindEnv
> import Maybe(fromJust)
> import Error(errorAt,internalError)
> import Utils((++!))
> import Maybe
> import Pretty
> import List(nub)
> import CurryPP(ppKind,ppTypeExpr)
> import TypeExprDisambiguate
> import KindInferenceMonad
> import Combined

\end{lstlisting}
Perform kind inference over all declarations. First, it splits declarations
into groups of related declarations. Then makes kind inference on each 
group.
\begin{lstlisting}

> kindInference :: ModuleIdent -> [Decl] -> KindEnv -> ([Decl],KindEnv)
> kindInference mid topDs kEnv =
>   foldl (kindInfGroup mid) ([],kEnv) groupedDecls
>   where groupedDecls = groupDecls mid topDs

> kindInferenceGoal :: KindEnv -> Goal -> Goal
> kindInferenceGoal = kindInfGoal

\end{lstlisting}
Makes a partition of declarations using the function in the module SCC.
\begin{lstlisting}

> groupDecls :: ModuleIdent -> [Decl] -> [[Decl]]
> groupDecls mid = scc definedBy usedBy
>   where definedBy = (\xs -> map qualify xs ++ map (qualifyWith mid) xs) .
>                     tcDefinedBy
>         usedBy = tcUsedBy

\end{lstlisting}
Assumption of kind of type variables.
\begin{lstlisting}

> assumptTypeVarsDecl :: ModuleIdent -> Decl -> KiState ()
> assumptTypeVarsDecl m decl =
>   mapM_ (\id -> freshKindVar >>= (updateSt . bindKind m id)) tvs
>   where tvs = nub (tvUsedBy decl ++ tvDefinedBy decl)

\end{lstlisting}
Assumption of kind of type constructors and type classes.
\begin{lstlisting}

> assumptTypeConstrDecl :: ModuleIdent -> Decl -> KiState ()
> assumptTypeConstrDecl m (DataDecl      _ tc _ _ _) =
>   freshKindVar >>= updateSt_ . bindKind m tc
> assumptTypeConstrDecl m (NewtypeDecl   _ tc _ _ _) =
>   freshKindVar >>= updateSt_ . bindKind m tc
> assumptTypeConstrDecl m (TypeDecl      _ tc _ _  ) =
>   freshKindVar >>= updateSt_ . bindKind m tc
> assumptTypeConstrDecl m (TypeClassDecl _ _ tc _ _) =
>   freshKindVar >>= updateSt_ . bindKind m tc
> assumptTypeConstrDecl _ _ = return ()

\end{lstlisting}
The initial set of assumptions is composed of the assumptions of the
type constructors and type classes, and the assumptions of the type
variables.
\begin{lstlisting}

> initAssumpts :: ModuleIdent -> [Decl] -> KiState ()
> initAssumpts m decls =
>   mapM_ (assumptTypeVarsDecl m) decls >> mapM_ (assumptTypeConstrDecl m) decls

\end{lstlisting}
Search the kind of a type expression.
\begin{lstlisting}

> kindOfTypeExprConstructor :: Position -> KindEnv -> QualIdent -> Kind
> kindOfTypeExprConstructor p kindEnv qid =
>   case (qualLookupKind qid kindEnv) of
>     [KindInfo _ k] -> k
>     [] -> errorAt p (undefinedType qid)
>     _ -> internalError "kindOfTypeExprConstructor"

> kindOfTypeExprVariable :: Position -> KindEnv -> Ident -> Kind
> kindOfTypeExprVariable p kindEnv id =
>   case lookupKind id kindEnv of
>      [KindInfo _ k] -> k
>      []  -> errorAt p (unknownKind id)
>      _ -> internalError "kindOfTypeExprVariable"

\end{lstlisting}
Search the kind of a type class constructor (very similar to
kindOfTypeExprConstructor above).
\begin{lstlisting}

> kindOfTypeClass :: Position -> KindEnv -> QualIdent -> Kind
> kindOfTypeClass p kindEnv qid =
>   case (qualLookupKind qid kindEnv) of
>     [KindInfo _ k] -> k
>     [] -> errorAt p (undefinedTypeClass qid)
>     _ -> internalError "kindOfTypeClass"

> kiTypeApplication :: Position -> TypeExpr -> Kind -> KiState (Kind,Kind)
> kiTypeApplication p tyexpr kind =
>   do theta <- liftSt fetchSt
>      let kind' = subst theta kind
>      typeApplication kind'
>   where typeApplication :: Kind -> KiState (Kind,Kind)
>         typeApplication (KFun k1 k2) = return (k1,k2)
>         typeApplication (KVar kv) =
>           do alpha <- freshKindVar
>              beta  <- freshKindVar
>              liftSt (updateSt_ (bindSubst kv (KFun alpha beta)))
>              return (alpha,beta)
>         typeApplication Star =
>           errorAt p (finalType tyexpr)

> kindOfTypeExpr :: Position -> TypeExpr -> KiState Kind
> kindOfTypeExpr p (TypeExprConstructor tc) =
>   do kindEnv <- fetchSt
>      return (kindOfTypeExprConstructor p kindEnv tc)
> kindOfTypeExpr p (TypeExprVariable tv) =
>   do kindEnv <- fetchSt
>      return (kindOfTypeExprVariable p kindEnv tv)
> kindOfTypeExpr p tyexpr@(TypeExprApplication _ _) =
>   do kindLeftmost <- kindOfTypeExpr p leftmost
>      (_,kind) <- foldM kindApplication (leftmost,kindLeftmost) args
>      return kind
>   where leftmost = leftmostTypeExpr tyexpr
>         args = typeExprArguments tyexpr
>         kindApplication :: (TypeExpr,Kind) -> TypeExpr -> KiState (TypeExpr,Kind)
>         kindApplication (tyexpr,kind) arg =
>           do (alpha,beta) <- kiTypeApplication p tyexpr kind
>              argKind <- kindOfTypeExpr p arg
>              unify p arg alpha argKind
>              return (typeExprApply tyexpr [arg],beta)

\end{lstlisting}
Kind inference on declarations. See rules on pages 20-21
of \cite{faxen02static}.
\begin{lstlisting}

> class KindInferenceClass a where
>   kindInf :: ModuleIdent -> Position -> a -> KiState ()

> instance KindInferenceClass a => KindInferenceClass [a] where
>   kindInf mid p = mapM_ (kindInf mid p)

> instance KindInferenceClass Decl where
>
>   kindInf mid _ (TypeClassDecl p ctx tc tv decls) =
>     do kindInf mid p ctx
>        kindInf mid p (TypeExprContext
>                        [TypeExprClassConstraint
>                          (qualifyWith mid tc)
>                          (TypeExprVariable tv)])
>        kindInf mid p decls
>
>   kindInf mid _ (InstanceDecl p ctx typeClass tyexpr decls) =
>     do kindInf mid p ctx
>        kindInf mid p (TypeExprContext
>                        [TypeExprClassConstraint typeClass tyexpr])
>        kindInf mid p decls
>
>   kindInf mid _ (DataDecl p tc tvs constrs _) =
>     do kindLhs <- kindOfDeclLhs mid p tc tvs
>        unify p (declLhsAsTypeExpr mid tc tvs) Star kindLhs
>        mapM_ kindInfConstrDecl constrs
>
>   kindInf mid _ (NewtypeDecl p tc tvs (NewConstrDecl p' _ _ tyexpr) _) =
>     do kindLhs <- kindOfDeclLhs mid p tc tvs
>        kindRhs <- kindOfTypeExpr p' tyexpr
>        unify p tyexpr kindLhs kindRhs
>
>   kindInf mid _ (TypeDecl p tc tvs tyexpr) =
>     do kindLhs <- kindOfDeclLhs mid p tc tvs
>        kindRhs <- kindOfTypeExpr p tyexpr
>        unify p tyexpr kindLhs kindRhs
>
>   kindInf mid _ (TypeSig p funIds (TypeExprWithContext ctx tyexpr)) =
>     do kindInf mid p ctx
>        kind <- kindOfTypeExpr p tyexpr
>        unify p tyexpr Star kind
>
>   kindInf mid _ (FunctionDecl p id eqs) =
>     kindInf mid p eqs
>
>   kindInf mid _ (ExternalDecl p cc s i tyexpr) =
>     do kind <- kindOfTypeExpr p tyexpr
>        unify p tyexpr Star kind
>
>   kindInf mid _ (PatternDecl p constr rhs) =
>     kindInf mid p rhs
>
>   kindInf _ _ (ImportDecl _ _ _ _ _) = return ()
>   kindInf _ _ (InfixDecl _ _ _ _)    = return ()
>   kindInf _ _ (EvalAnnot _ _ _)      = return ()
>   kindInf _ _ (ExtraVariables _ _)   = return ()

> instance KindInferenceClass Equation where
>   kindInf mid _ (Equation p constrs rhs) =
>     kindInf mid p rhs

> instance KindInferenceClass Rhs where
>   kindInf mid _ (SimpleRhs p expr decls) =
>     do kindInf mid p expr
>        kindInf mid p decls
>   kindInf mid p (GuardedRhs condExprs decls) =
>     do kindInf mid p condExprs
>        kindInf mid p decls

> instance KindInferenceClass CondExpr where
>   kindInf mid _ (CondExpr p expr1 expr2) =
>     do kindInf mid p expr1
>        kindInf mid p expr2

> instance KindInferenceClass Expression where
>   kindInf mid p (Literal li) = return ()
>   kindInf mid p (Variable v) = return ()
>   kindInf mid p (Constructor c) = return ()
>   kindInf mid p (Paren expr) = kindInf mid p expr
>   kindInf mid p (Typed expr (TypeExprWithContext ctx tyexpr)) =
>     do kindInf mid p expr
>        kindInf mid p ctx
>        tyexprKind <- kindOfTypeExpr p tyexpr
>        unify p tyexpr Star tyexprKind
>   kindInf mid p (Tuple exprs) = kindInf mid p exprs
>   kindInf mid p (List exprs) = kindInf mid p exprs
>   kindInf mid p (ListCompr expr stats) =
>     kindInf mid p expr >> kindInf mid p stats
>   kindInf mid p (EnumFrom expr) = kindInf mid p expr
>   kindInf mid p (EnumFromThen expr1 expr2) =
>     kindInf mid p expr1 >> kindInf mid p expr2
>   kindInf mid p (EnumFromTo expr1 expr2) =
>     kindInf mid p expr1 >> kindInf mid p expr2
>   kindInf mid p (EnumFromThenTo expr1 expr2 expr3) =
>     kindInf mid p expr1 >> kindInf mid p expr2 >> kindInf mid p expr3
>   kindInf mid p (UnaryMinus id expr) =
>     kindInf mid p expr
>   kindInf mid p (Apply expr1 expr2) =
>     kindInf mid p expr1 >> kindInf mid p expr2
>   kindInf mid p (InfixApply expr1 op expr2) =
>     kindInf mid p expr1 >> kindInf mid p expr2
>   kindInf mid p (LeftSection expr op) =
>     kindInf mid p expr
>   kindInf mid p (RightSection op expr) =
>     kindInf mid p expr
>   kindInf mid p (Lambda constrs expr) =
>     kindInf mid p expr
>   kindInf mid p (Let decls expr) =
>     kindInf mid p decls >> kindInf mid p expr
>   kindInf mid p (Do stats expr) =
>     kindInf mid p stats >> kindInf mid p expr
>   kindInf mid p (IfThenElse expr1 expr2 expr3) =
>     kindInf mid p expr1 >> kindInf mid p expr2 >> kindInf mid p expr3
>   kindInf mid p (Case expr alts) =
>     kindInf mid p expr >> kindInf mid p alts

> instance KindInferenceClass Statement where
>   kindInf mid p (StmtExpr expr) = kindInf mid p expr
>   kindInf mid p (StmtDecl decls) = kindInf mid p decls
>   kindInf mid p (StmtBind constr expr) = kindInf mid p expr

> instance KindInferenceClass Alt where
>   kindInf mid _ (Alt p constr rhs) = kindInf mid p rhs

> instance KindInferenceClass Goal where
>   kindInf mid _ (Goal p expr decls) =
>     kindInf mid p expr >> kindInf mid p decls

> kindInfConstrDecl :: ConstrDecl -> KiState ()
> kindInfConstrDecl (ConstrDecl p _ _ tyexprs) =
>   mapM_ (\tyexpr -> (kindOfTypeExpr p tyexpr >>= unify p tyexpr Star)) tyexprs
> kindInfConstrDecl (ConOpDecl p _ tyexpr1 op tyexpr2) =
>   do kindOfTypeExpr p tyexpr1 >>= unify p tyexpr1 Star
>      kindOfTypeExpr p tyexpr2 >>= unify p tyexpr2 Star

> kindOfDeclLhs :: ModuleIdent -> Position -> Ident -> [Ident] -> KiState Kind
> kindOfDeclLhs mid p tc tvs =
>   kindOfTypeExpr p (declLhsAsTypeExpr mid tc tvs)

> declLhsAsTypeExpr :: ModuleIdent -> Ident -> [Ident] -> TypeExpr
> declLhsAsTypeExpr mid tc tvs = typeExprApplyConstructor (qualifyWith mid tc)
>                                                         (map TypeExprVariable tvs)

\end{lstlisting}
Kind inference on type contexts:
\begin{lstlisting}

> instance KindInferenceClass TypeExprContext where
>   kindInf mid p (TypeExprContext classPreds) = kindInf mid p classPreds

> instance KindInferenceClass TypeExprClassConstraint where
>   kindInf mid p (TypeExprClassConstraint classQId tyexpr) =
>     do kindEnv <- fetchSt
>        tyexprKind <- kindOfTypeExpr p tyexpr
>        unify p tyexpr
>          (kindOfTypeClass p kindEnv classQId) tyexprKind

\end{lstlisting}
Kind unification of type expressions, as seen in `ktype' inference
rule\cite{faxen02static}.
The TypeExprConstructor and
TypeExprVariable cases are right straighforward: just look up the kind
of the constructor/variable and perform kind unification. The
TypeExprApplication case as two antecedents: if ty1 has kind k1->k2 and
ty2 has kind k1, then the type application ty1 ty2 has kind k2. The
rule is applied backwards, introducing a new kind variable (that plays
the role of k1 before). A new assumption is inserted for this kind
variable, so it is updated by the first call to unifyKindTypeExpr, and
then the updated value is recovered to use it in the second call.

The order of the calls to unifyKindTypeExpr affects to the qualify of
the error messages provided by the compiler. These messages are most
complete and useful when unifying first the ty2 subexpression.

Another trick to improve the quality of error messages is to pass the
root type expression to every call of the function. This is the reason
to pass two type expressions as arguments.
\begin{lstlisting}

\end{lstlisting}
Setting of default kinds.

\ToDo{Review this.}
\begin{lstlisting}

> kindDefaulting :: KindEnv -> KindSubst
> kindDefaulting kindEnv = defaultingSubst (toListSet (fvEnv kindEnv))
>   where defaultingSubst :: [Ident] -> KindSubst
>         defaultingSubst = foldr (\kv -> bindSubst kv Star) idSubst

\end{lstlisting}
Perform kind inference over a group of declarations.
\begin{lstlisting}

> kindInfGroup :: ModuleIdent -> ([Decl],KindEnv) -> [Decl] -> ([Decl],KindEnv)
> kindInfGroup mid (xs,kindEnv) decls =
>   (xs++decls',intermKindEnv')
>   where (decls',intermKindEnv) =
>                         runKiState kindEnv
>                           (do initAssumpts mid decls
>                               kindInf mid noPos decls
>                               kindEnv <- fetchSt
>                               sigma   <- liftSt $ fetchSt
>                               return (decls,subst sigma kindEnv))
>         defaultingSubst = kindDefaulting intermKindEnv
>         intermKindEnv'  = subst defaultingSubst intermKindEnv

\end{lstlisting}
Perform kind inference over a goal. A goal hasn't any type constructor
or type class declaration, so we only have to check type signatures.
The kind environment is unaffected. So, in order to ensure that Haskell
performs the kind inference, we must avoid the lazy evaluation.
\begin{lstlisting}

> kindInfGoal :: KindEnv -> Goal -> Goal
> kindInfGoal kindEnv goal =
>   runKiState kindEnv (kindInf noModule noPos goal) `seq` goal
>   where noModule = undefined

\end{lstlisting}
Auxiliary functions.
\begin{lstlisting}

> noPos :: Position
> noPos = undefined

\end{lstlisting}
Error messages:
\begin{lstlisting}

> undefinedType :: QualIdent -> String
> undefinedType tc = "Undefined type constructor " ++ qualName tc

> undefinedTypeClass :: QualIdent -> String
> undefinedTypeClass tc = "Undefined type class " ++ qualName tc

\end{lstlisting}
