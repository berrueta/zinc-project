% $Id: ValueEnv.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2003-2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{ValueEnv.lhs}
\codesection{Function and constructor types}
\begin{lstlisting}

> module ValueEnv where
> import Ident
> import TopEnv
> import Types
> import Utils
> import TypeConstructorEnv
> import Set

\end{lstlisting}
In order to test the type correctness of a module, the compiler needs
to determine the type of every data constructor, function, and
variable in the module. For the purpose of type checking there is no
need for distinguishing between variables and functions. For all objects
their original names and their types are saved. On import two values
are considered equal if their original names match.
\begin{lstlisting}

> data ValueInfo = DataConstructor QualIdent ExistTypeScheme
>                | NewtypeConstructor QualIdent ExistTypeScheme
>                | Value QualIdent TypeScheme
>                deriving (Eq,Show)

> instance Entity ValueInfo where
>   origName (DataConstructor origName _) = origName
>   origName (NewtypeConstructor origName _) = origName
>   origName (Value origName _) = origName

\end{lstlisting}
Even though value declarations may be nested, the compiler uses only
flat environments for saving type information. This is possible
because all identifiers are renamed by the compiler. Again, we need
special cases for handling tuple constructors.
\begin{lstlisting}

> type ValueEnv = TopEnv ValueInfo

> bindLocalGlobalInfo :: (QualIdent -> a -> ValueInfo) ->
>                        ModuleIdent -> Ident -> a -> ValueEnv -> ValueEnv
> bindLocalGlobalInfo f m c ty = bindLocalTopEnv c v . qualBindLocalTopEnv c' v
>   where c' = qualifyWith m c
>         v  = f c' ty

> bindFun :: ModuleIdent -> Ident -> TypeScheme -> ValueEnv -> ValueEnv
> bindFun m f ty
>   | uniqueId f == 0 = bindLocalTopEnv f v . qualBindLocalTopEnv f' v
>   | otherwise       = bindLocalTopEnv f v
>   where f' = qualifyWith m f
>         v = Value f' ty

> rebindFun :: ModuleIdent -> Ident -> TypeScheme -> ValueEnv -> ValueEnv
> rebindFun m f ty
>   | uniqueId f == 0 = rebindLocalTopEnv f v . qualRebindLocalTopEnv f' v
>   | otherwise       = rebindLocalTopEnv f v
>   where f' = qualifyWith m f
>         v = Value f' ty

> lookupValue :: Ident -> ValueEnv -> [ValueInfo]
> lookupValue x tyEnv = lookupTopEnv x tyEnv ++! lookupTuple x

> qualLookupValue :: QualIdent -> ValueEnv -> [ValueInfo]
> qualLookupValue x tyEnv =
>   qualLookupTopEnv x tyEnv ++! lookupTuple (unqualify x)

> lookupTuple :: Ident -> [ValueInfo]
> lookupTuple c
>   | isTupleId c = [tupleDCs !! (tupleArity c - 2)]
>   | otherwise = []

> tupleDCs :: [ValueInfo]
> tupleDCs = map dataInfo tupleTCs
>   where dataInfo (DataType tc tvs [Just (Data c _ tys)]) =
>           DataConstructor (qualUnqualify preludeMIdent tc)
>                           (ForAllExist (length tys) 0
>                                        (foldr typeArrow (tupleType tys) tys))

\end{lstlisting}
The functions \texttt{fvEnv} and \texttt{fsEnv} compute the set of
free type variables and free skolems of a type environment,
respectively. We ignore the types of data constructors here because we
know that they are closed.
\begin{lstlisting}

> fvEnv :: ValueEnv -> Set Int
> fvEnv tyEnv =
>   fromListSet [tv | ty <- localTypes tyEnv, tv <- typeVars ty, tv < 0]

> fsEnv :: ValueEnv -> Set Int
> fsEnv tyEnv = unionSets (map (fromListSet . typeSkolems) (localTypes tyEnv))

\end{lstlisting}
Auxiliary function. Note that we ignore the type contexts.
\begin{lstlisting}

> localTypes :: ValueEnv -> [Type]
> localTypes tyEnv = [ty | (_,Value _ (ForAll _ (TypeWithContext _ ty)))
>                            <- localBindings tyEnv]

\end{lstlisting}
