% $Id: DualTopEnv.lhs 970 2004-08-08 12:05:32Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{DualTopEnv.lhs}
\codesection{Top-Lovel Environments with dual key}
The module \texttt{DualTopEnv} implements an environment similar
to the \texttt{TopEnv}, but with a dual key.
\begin{lstlisting}

> module DualTopEnv(DualTopEnv,DualEntity(..),
>                   emptyDualTopEnv,ppDualTopEnv,
>                   bindLocalDualTopEnv,bindImportDualTopEnv,
>                   lookupDualTopEnv,
>                   allImportsDualTopEnv,localBindingsDualTopEnv) where
> import Ident
> import Env
> import Pretty
> import CurryPP(ppQIdent)

\end{lstlisting}
First we define a new environment, similar to the TopEnv but with
a key composed of two idents.
\begin{lstlisting}

> data Source = Local | Import [ModuleIdent] deriving (Eq,Show)

> class DualEntity a where
>  dualEntityOrigName :: a -> (QualIdent,QualIdent)
>  dualEntityMerge    :: a -> a -> Maybe a
>  dualEntityMerge x y
>    | dualEntityOrigName x == dualEntityOrigName y = Just x
>    | otherwise = Nothing

> newtype DualTopEnv a = DualTopEnv
>                          (Env (QualIdent,QualIdent) [(Source,a)])
>                      deriving (Show,Eq)

> emptyDualTopEnv :: DualTopEnv a
> emptyDualTopEnv = DualTopEnv emptyEnv

\end{lstlisting}
Binding imported entities.
\begin{lstlisting}

> bindImportDualTopEnv :: DualEntity a => ModuleIdent -> QualIdent -> QualIdent ->
>                         a -> DualTopEnv a -> DualTopEnv a
> bindImportDualTopEnv m id1 id2 i (DualTopEnv env) =
>   DualTopEnv (bindEnv (id1,id2) (mergeImport m i (entities (id1,id2) env)) env)

\end{lstlisting}
Binding local entities.
\begin{lstlisting}

> bindLocalDualTopEnv :: QualIdent -> QualIdent -> a ->
>                        DualTopEnv a -> DualTopEnv a
> bindLocalDualTopEnv id1 id2 i (DualTopEnv env) =
>   DualTopEnv (bindEnv (id1,id2) (bindLocal i (entities (id1,id2) env)) env)
>   where bindLocal :: a -> [(Source,a)] -> [(Source,a)]
>         bindLocal x xs
>           | null [x' | (Local,x') <- xs] = (Local,x) : xs
>           | otherwise = error "internal error: bindDualTopEnv"

\end{lstlisting}
Looking up the environment.
\begin{lstlisting}

> lookupDualTopEnv :: QualIdent -> QualIdent -> DualTopEnv a -> [a]
> lookupDualTopEnv id1 id2 (DualTopEnv env) = map snd (entities (id1,id2) env)

\end{lstlisting}
Environment-to-list functions.
\begin{lstlisting}

> allImportsDualTopEnv :: DualTopEnv a -> [(QualIdent,QualIdent,a)]
> allImportsDualTopEnv (DualTopEnv env) =
>   [(x,y,z) | ((x,y),zs) <- envToList env, (Import _,z) <- zs]

> localBindingsDualTopEnv :: DualTopEnv a -> [(QualIdent,QualIdent,a)]
> localBindingsDualTopEnv (DualTopEnv env) =
>   [(x,y,z) | ((x,y),zs) <- envToList env, (Local,z) <- zs]

\end{lstlisting}
Pretty-print the environment.
\begin{lstlisting}

> ppDualTopEnv :: Show a => DualTopEnv a -> Doc
> ppDualTopEnv = vcat . map (uncurry ppContent) . dualTopEnvToList
>   where dualTopEnvToList :: DualTopEnv a -> [((QualIdent,QualIdent),[(Source,a)])]
>         dualTopEnvToList (DualTopEnv env) = envToList env
>         ppContent :: (Show a) => (QualIdent,QualIdent) -> [(Source,a)] -> Doc
>         ppContent (qid1,qid2) dt = hsep [ppQIdent qid1,
>                                          ppQIdent qid2,
>                                          vcat $ map (text . show) dt]

\end{lstlisting}
Auxiliary function.
\begin{lstlisting}

> mergeImport :: DualEntity a => ModuleIdent -> a -> [(Source,a)] -> [(Source,a)]
> mergeImport m x [] = [(Import [m],x)]
> mergeImport m x ((Local,x') : xs) = (Local,x') : mergeImport m x xs
> mergeImport m x ((Import ms,x') : xs) =
>   case dualEntityMerge x x' of
>     Just x'' -> (Import (m:ms),x'') : xs
>     Nothing -> (Import ms,x') : mergeImport m x xs

\end{lstlisting}
