%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Kind.lhs}
\codesection{Kind}
This module describes kinds and implements them as a term system.
\begin{lstlisting}

> module Kind where
> import List(union,nub)
> import Ident

\end{lstlisting}
Kind variables are not found in the language that describes the kind of
type constructors and type classes, but are required to perform kind
inference. At the end of the inference process, all kind variables had
been removed.
\begin{lstlisting}

> data Kind  = Star
>            | KFun Kind Kind
>            | KVar Ident
>            deriving (Eq, Show, Ord)

\end{lstlisting}
Searches for kind variables in a kind expression.
\begin{lstlisting}

> kindVars :: Kind -> [Ident]
> kindVars Star = []
> kindVars (KFun k1 k2) = kindVars k1 ++ kindVars k2
> kindVars (KVar kv) = [kv]

> containsKindVars :: Kind -> Bool
> containsKindVars = not . null . kindVars

\end{lstlisting}
The function \texttt{kindOrder} is used to provide useful error messages to
the user. Kind variables are assumed to be Star.
\begin{lstlisting}

> kindOrder :: Kind -> Int
> kindOrder Star = 0
> kindOrder (KFun _ k) = 1 + (kindOrder k)
> kindOrder (KVar _) = kindOrder Star

\end{lstlisting}
