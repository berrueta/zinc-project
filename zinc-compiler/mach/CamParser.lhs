% -*- LaTeX -*-
% $Id: CamParser.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2004, Wolfgang Lux
% See LICENSE for the full license.
%
\subsection{Parsing Abstract Machine Code}
In this section a lexer and parser for the abstract machine code are
presented, which are based on the LL(1) parsing combinators described
in appendix~\ref{sec:ll-parsecomb}.
\begin{verbatim}

> module CamParser(parseModule) where
> import Cam
> import Position
> import LexComb
> import LLParseComb
> import Map
> import Char
> import Maybe
> import Error

\end{verbatim}
\paragraph{Lexer}
\begin{verbatim}

> data Token = Token Category Attributes

> instance Eq Token where
>   Token t1 _ == Token t2 _ = t1 == t2
> instance Ord Token where
>   Token t1 _ `compare` Token t2 _ = t1 `compare` t2

> data Category =
>   -- identifiers
>     Ident | Keyword Keyword
>   -- numbers
>   | NatNum | IntNum | FloatNum
>   -- symbols
>   | Slash | Equals | Colon | Comma | Semicolon | Bar | LeftArrow
>   | LeftParen | RightParen | LeftBrace | RightBrace
>   -- end-of-file
>   | EOF
>   deriving (Eq,Ord)

> data Attributes =
>     NoAttributes
>   | NameAttributes{ sval :: String }
>   | IntAttributes{ ival :: Int }
>   | FloatAttributes{ fval :: Double }

> tok :: Category -> Token
> tok c = Token c NoAttributes

> nameTok :: Category -> String -> Token
> nameTok c cs = Token c NameAttributes{ sval = cs }

> intTok :: Category -> String -> Token
> intTok c cs = Token c IntAttributes{ ival = convertSignedIntegral 10 cs }

> floatTok :: String -> String -> String -> Token
> floatTok mant frac exp =
>   Token FloatNum FloatAttributes{ fval = convertSignedFloating mant frac e }
>   where e = convertSignedIntegral 10 exp

> data Keyword =
>     KW_char
>   | KW_choices
>   | KW_data
>   | KW_default
>   | KW_enter
>   | KW_exec
>   | KW_flex
>   | KW_float
>   | KW_free
>   | KW_function
>   | KW_import
>   | KW_int
>   | KW_lazy
>   | KW_let
>   | KW_lock
>   | KW_return
>   | KW_rigid
>   | KW_switch
>   | KW_update
>   deriving (Eq,Ord)

> instance Show Attributes where
>   showsPrec _ NoAttributes = showChar '_'
>   showsPrec _ (NameAttributes sval) =
>     showChar '`' . showString sval . showChar '\''
>   showsPrec _ (IntAttributes ival) = shows ival
>   showsPrec _ (FloatAttributes fval) = shows fval

> instance Show Token where
>   showsPrec _ (Token Ident a) = shows a
>   showsPrec _ (Token (Keyword k) _) = showChar '`' . shows k . showChar '\''
>   showsPrec _ (Token NatNum a) = showString "natural " . shows a
>   showsPrec _ (Token IntNum a) = showString "integer " . shows a
>   showsPrec _ (Token FloatNum a) = showString "float " . shows a
>   showsPrec _ (Token Slash _) = showString "`/'"
>   showsPrec _ (Token Equals _) = showString "="
>   showsPrec _ (Token Colon _) = showString "`:'"
>   showsPrec _ (Token Comma _) = showString "`,'"
>   showsPrec _ (Token Semicolon _) = showString "`;'"
>   showsPrec _ (Token Bar _) = showString "`|'"
>   showsPrec _ (Token LeftArrow _) = showString "`<-'"
>   showsPrec _ (Token LeftParen _) = showString "`('"
>   showsPrec _ (Token RightParen _) = showString "`)'"
>   showsPrec _ (Token LeftBrace _) = showString "`{'"
>   showsPrec _ (Token RightBrace _) = showString "`}'"
>   showsPrec _ (Token EOF _) = showString "end-of-file"

> instance Show Keyword where
>   showsPrec _ KW_char = showKeyword "char"
>   showsPrec _ KW_choices = showKeyword "choices"
>   showsPrec _ KW_data = showKeyword "data"
>   showsPrec _ KW_default = showKeyword "default"
>   showsPrec _ KW_enter = showKeyword "enter"
>   showsPrec _ KW_exec = showKeyword "exec"
>   showsPrec _ KW_flex = showKeyword "flex"
>   showsPrec _ KW_float = showKeyword "float"
>   showsPrec _ KW_free = showKeyword "free"
>   showsPrec _ KW_function = showKeyword "function"
>   showsPrec _ KW_import = showKeyword "import"
>   showsPrec _ KW_int = showKeyword "int"
>   showsPrec _ KW_lazy = showKeyword "lazy"
>   showsPrec _ KW_let = showKeyword "let"
>   showsPrec _ KW_lock = showKeyword "lock"
>   showsPrec _ KW_return = showKeyword "return"
>   showsPrec _ KW_rigid = showKeyword "rigid"
>   showsPrec _ KW_switch = showKeyword "switch"
>   showsPrec _ KW_update = showKeyword "update"

> showKeyword :: String -> ShowS
> showKeyword kw = showChar '.' . showString kw

> instance Symbol Token where
>   isEOF (Token t _) = t == EOF

> isIdent :: Char -> Bool
> isIdent c = isAlphaNum c || c == '_'

> keywords :: FM String Keyword
> keywords = fromListFM [
>     ("char",     KW_char),
>     ("choices",  KW_choices),
>     ("data",     KW_data),
>     ("default",  KW_default),
>     ("enter",    KW_enter),
>     ("exec",     KW_exec),
>     ("flex",     KW_flex),
>     ("float",    KW_float),
>     ("free",     KW_free),
>     ("function", KW_function),
>     ("import",   KW_import),
>     ("int",      KW_int),
>     ("lazy",     KW_lazy),
>     ("let",      KW_let),
>     ("lock",     KW_lock),
>     ("return",   KW_return),
>     ("rigid",    KW_rigid),
>     ("switch",   KW_switch),
>     ("update",   KW_update)
>   ]

> type SuccessP a = Position -> Token -> P a
> type FailP a = Position -> String -> P a

> lexFile :: P [(Position,Token)]
> lexFile = lexer tokens failP
>   where tokens p t
>           | isEOF t = returnP [(p,t)]
>           | otherwise = lexFile `thenP` returnP . ((p,t):)

> lexer :: SuccessP a -> FailP a -> P a
> lexer success fail p [] = success p (tok EOF) p []
> lexer success fail p (c:cs)
>   | isSpace c = lexer success fail (advance c p) cs
>   | c == '-' =
>       case cs of
>         ('-':cs') -> lexer success fail p (dropWhile (/= '\n') cs')
>         _ -> illegalChar
>   | c == '/' = token Slash
>   | c == '=' = token Equals
>   | c == ':' = token Colon
>   | c == ',' = token Comma
>   | c == ';' = token Semicolon
>   | c == '|' = token Bar
>   | c == '<' =
>       case cs of
>         ('-':cs') -> token2 LeftArrow cs'
>         _ -> illegalChar
>   | c == '(' = token LeftParen
>   | c == ')' = token RightParen
>   | c == '{' = token LeftBrace
>   | c == '}' = token RightBrace
>   | c == '.' = lexKeyword (success p) fail p cs
>   | isAlpha c || c == '_' = lexIdent (success p) c p cs
>   | isDigit c || c `elem` "+-" = lexNumber (success p) fail c p cs
>   | otherwise = illegalChar
>   where token t = success p (tok t) (next p) cs
>         token2 t = success p (tok t) (incr p 2)
>         advance '\n' = nl
>         advance '\t' = tab
>         advance _ = next
>         illegalChar = fail p ("Illegal character " ++ show c) p (c:cs)

> lexIdent :: (Token -> P a) -> Char -> P a
> lexIdent success c p cs =
>   success (nameTok Ident (c : cs')) (incr p (1 + length cs')) cs''
>   where (cs',cs'') = span isIdent cs

> lexKeyword :: (Token -> P a) -> FailP a -> P a
> lexKeyword success fail p cs
>   | null cs' = fail p "Keyword expected after '.'" (next p) cs
>   | otherwise =
>       maybe (fail p ("Unknown keyword " ++ show ('.':cs')))
>             (success . tok . Keyword)
>             (lookupFM cs' keywords) (incr p (1 + length cs')) cs''
>   where (cs',cs'') = span isIdent cs

> lexNumber :: (Token -> P a) -> FailP a -> Char -> P a
> lexNumber success fail c p cs
>   | null ds && not (isDigit c) =
>      fail p ("Digit expected after " ++ show c) (next p) cs
>   | otherwise =
>       case cs' of
>         ('.':cs'') -> lexFraction success fail (c:ds) (next p') cs''
>         ('e':cs'') -> lexExponent success fail (c:ds) "" (next p') cs''
>         ('E':cs'') -> lexExponent success fail (c:ds) "" (next p') cs''
>         _ -> success (intTok (cat c) (c:ds)) p' cs'
>   where (ds,cs' ) = span isDigit cs
>         p' = incr p (1 + length ds)
>         cat c = if c `elem` "+-" then IntNum else NatNum

> lexFraction :: (Token -> P a) -> FailP a -> String -> P a
> lexFraction success fail ds p cs =
>   case cs' of
>     ('e':cs'') -> lexExponent success fail ds ds' (next p') cs''
>     ('E':cs'') -> lexExponent success fail ds ds' (next p') cs''
>     _ -> success (floatTok ds ds' "") p' cs'
>   where (ds',cs') = span isDigit cs
>         p' = incr p (length ds')

> lexExponent :: (Token -> P a) -> FailP a -> String -> String -> P a
> lexExponent success fail ds ds' p (c:cs)
>   | isDigit c || c `elem` "+-" =
>       case span isDigit cs of
>         (es,cs')
>           | not (isDigit c) && null es -> fail p "Empty exponent" p cs
>           | otherwise ->
>               success (floatTok ds ds' (c:es)) (incr p (1 + length es)) cs'
> lexExponent success fail _ _ p cs = fail p "Exponent expected" p cs

\end{verbatim}
\paragraph{Parser}
\begin{verbatim}

> parseModule :: FilePath -> String -> Error Module
> parseModule fn s = applyParser camModule lexer fn s

> camModule :: Parser Token Module a
> camModule = (:) <$> importDecl <*> (semi <-*> camModule `opt` [])
>         <|> camDecl `sepBy` semi

> camDecl :: Parser Token Decl a
> camDecl = dataDecl <|> funcDecl

> importDecl :: Parser Token Decl a
> importDecl = ImportDecl <$-> keyword KW_import <*> checkName

> dataDecl :: Parser Token Decl a
> dataDecl = DataDecl <$-> keyword KW_data <*> checkName
>                     <*> (equals <-*> (constrDecl `sepBy1` bar) `opt` [])

> constrDecl :: Parser Token ConstrDecl a
> constrDecl = ConstrDecl <$> name <*-> checkSlash <*> checkNat

> funcDecl :: Parser Token Decl a
> funcDecl =
>   FunctionDecl <$-> keyword KW_function <*> checkName <*> nameList <*> block

> block :: Parser Token Stmt a
> block = braces stmt

> stmt :: Parser Token Stmt a
> stmt = Return <$-> keyword KW_return <*> checkName
>    <|> Enter <$-> keyword KW_enter <*> checkName
>    <|> Exec <$-> keyword KW_exec <*> checkName <*> nameList
>    <|> stmtSeq (Lock <$-> keyword KW_lock <*> checkName)
>    <|> stmtSeq (Update <$-> keyword KW_update <*> checkName <*> checkName)
>    <|> stmtSeq (Seq <$> name <*-> checkLeftArrow <*> stmt)
>    <|> stmtSeq (Let <$-> keyword KW_let <*> braces (binding `sepBy1` semi))
>    <|> flip Switch <$-> keyword KW_switch <*> checkName <*> rf
>                    <*> braces cases
>    <|> Choices <$-> keyword KW_choices <*> braces (block `sepBy1` bar)
>    <|> block
>    where rf = Rigid <$-> keyword KW_rigid <|> Flex <$-> keyword KW_flex

> stmtSeq :: Parser Token (Stmt -> Stmt) a -> Parser Token Stmt a
> stmtSeq p = p <*-> checkSemi <*> stmt

> binding :: Parser Token Bind a
> binding = Bind <$> name <*-> checkEquals <*> node

> node :: Parser Token Expr a
> node = Lit <$> literal
>    <|> Constr <$-> keyword KW_data <*> checkName <*> nameList
>    <|> Closure <$-> keyword KW_function <*> checkName <*> nameList
>    <|> Lazy <$-> keyword KW_lazy <*> checkName <*> nameList
>    <|> Free <$-> keyword KW_free
>    <|> Ref <$> name

> cases :: Parser Token [Case] a
> cases = return <$> switchCase defaultTag
>     <|> (:) <$> switchCase caseTag <*> (bar <-*> cases `opt` [])
>   where switchCase tag = Case <$> tag <*-> checkColon <*> block

> literal :: Parser Token Literal a
> literal = Char . toEnum <$-> keyword KW_char <*> checkInt
>       <|> Int <$-> keyword KW_int <*> checkInt
>       <|> Float <$-> keyword KW_float <*> checkFloat

> name, checkName :: Parser Token Name a
> name = Name . sval <$> token Ident
> checkName = name <?> "name expected"

> nameList :: Parser Token [Name] a
> nameList = parens (name `sepBy` comma)

> keyword :: Keyword -> Parser Token Attributes a
> keyword k = token (Keyword k)

> nat,checkNat :: Parser Token Int a
> nat = ival <$> token NatNum
> checkNat = nat <?> "unsigned number expected"

> int,checkInt :: Parser Token Int a
> int = ival <$> (token NatNum <|> token IntNum)
> checkInt = int <?> "integer number expected"

> float,checkFloat :: Parser Token Double a
> float = fval <$> token FloatNum
> checkFloat = float <?> "floating point number expected"

> caseTag :: Parser Token Tag a
> caseTag = LitCase <$> literal
>       <|> ConstrCase <$-> keyword KW_data <*> checkName <*> nameList

> defaultTag :: Parser Token Tag a
> defaultTag = DefaultCase <$-> keyword KW_default

> token :: Category -> Parser Token Attributes a
> token c = attr <$> symbol (Token c NoAttributes)
>   where attr (Token _ a) = a

> slash, checkSlash :: Parser Token Attributes a
> slash = token Slash
> checkSlash = slash <?> "/ expected"

> equals, checkEquals :: Parser Token Attributes a
> equals = token Equals
> checkEquals = equals <?> "= expected"

> comma :: Parser Token Attributes a
> comma = token Comma

> colon, checkColon :: Parser Token Attributes a
> colon = token Colon
> checkColon = colon <?> ": expected"

> semi, checkSemi :: Parser Token Attributes a
> semi = token Semicolon
> checkSemi = semi <?> "; expected"

> bar :: Parser Token Attributes a
> bar = token Bar

> leftArrow, checkLeftArrow :: Parser Token Attributes a
> leftArrow = token LeftArrow
> checkLeftArrow = leftArrow <?> "<- expected"

> leftParen, rightParen :: Parser Token Attributes a
> leftParen = token LeftParen
> rightParen = token RightParen

> leftBrace, rightBrace :: Parser Token Attributes a
> leftBrace = token LeftBrace
> rightBrace = token RightBrace

> parens, braces :: Parser Token a b -> Parser Token a b
> parens p = leftParen <-*> p <*-> rightParen
>        <?> "( expected"
> braces p = leftBrace <-*> p <*-> rightBrace
>        <?> "{ expected"

\end{verbatim}
