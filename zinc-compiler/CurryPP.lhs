% $Id: CurryPP.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2004, Wolfgang Lux
% Copyright (c) 2003-2005, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{CurryPP.lhs}
\codesection{A pretty printer for Zinc}\label{sec:CurryPP}
This module implements a pretty printer for Curry expressions. It was
derived from the Haskell pretty printer provided in Simon Marlow's
Haskell parser.
\begin{lstlisting}

> module CurryPP(module CurryPP, Doc) where
> import Ident
> import CurrySyntax
> import Pretty
> import TypeExpr
> import Kind

\end{lstlisting}
Pretty print a module
\begin{lstlisting}

> ppModule :: Module -> Doc
> ppModule (Module m es ds) = ppModuleHeader m es $$ ppBlock ds

\end{lstlisting}
Module header
\begin{lstlisting}

> ppModuleHeader :: ModuleIdent -> Maybe ExportSpec -> Doc
> ppModuleHeader m es =
>   text "module" <+> ppMIdent m <+> maybePP ppExportSpec es <+> text "where"

> ppExportSpec :: ExportSpec -> Doc
> ppExportSpec (Exporting _ es) = parenList (map ppExport es)

> ppExport :: Export -> Doc
> ppExport (Export x) = ppQIdent x
> ppExport (ExportTypeWith tc cs) = ppQIdent tc <> parenList (map ppIdent cs)
> ppExport (ExportTypeAll tc) = ppQIdent tc <> text "(..)"
> ppExport (ExportModule m) = text "module" <+> ppMIdent m
> ppExport (ExportTypeClassWith tc mets) =
>   text "class" <+> ppQIdent tc <> parenList (map ppIdent mets)

\end{lstlisting}
Declarations
\begin{lstlisting}

> ppBlock :: [Decl] -> Doc
> ppBlock = vcat . map ppDecl

> ppDecl :: Decl -> Doc
> ppDecl (ImportDecl _ m q asM is) =
>   text "import" <+> ppQualified q <+> ppMIdent m <+> maybePP ppAs asM
>                 <+> maybePP ppImportSpec is
>   where ppQualified q = if q then text "qualified" else empty
>         ppAs m = text "as" <+> ppMIdent m
> ppDecl (InfixDecl _ fix p ops) = ppPrec fix p <+> list (map ppInfixOp ops)
> ppDecl (TypeClassDecl _ ctx tc tv decls) =
>   sep (text "class" <+> ppTypeExprContextPrefix ctx
>        <+> ppIdent tc <+> ppIdent tv <+> text "where"
>       : map (indent . ppDecl) decls)
> ppDecl (InstanceDecl _ ctx tc tyexpr decls) =
>   sep (text "instance" <+> ppTypeExprContextPrefix ctx <+> ppQIdent tc
>        <+> ppTypeExpr 2 tyexpr <+> text "where"
>       : map (indent . ppDecl) decls)
> ppDecl (DataDecl _ tc tvs cs derivedTC) =
>   sep (ppTypeDeclLhs "data" tc tvs :
>        map indent (zipWith (<+>) (equals : repeat vbar) (map ppConstr cs) ++
>                    [ppDeriving derivedTC]))
> ppDecl (NewtypeDecl _ tc tvs nc derivedTC) =
>   sep [ppTypeDeclLhs "newtype" tc tvs <+> equals,
>        indent (ppNewConstr nc),
>        indent (ppDeriving derivedTC)]
> ppDecl (TypeDecl _ tc tvs ty) =
>   sep [ppTypeDeclLhs "type" tc tvs <+> equals, indent (ppTypeExpr 0 ty)]
> ppDecl (TypeSig _ fs tyWC) =
>   list (map ppIdent fs) <+> text "::" <+> ppTypeExprWithContext tyWC
> ppDecl (EvalAnnot _ fs ev) =
>   list (map ppIdent fs) <+> text "eval" <+> ppEval ev
>   where ppEval EvalRigid = text "rigid"
>         ppEval EvalChoice = text "choice"
> ppDecl (FunctionDecl _ _ eqs) = vcat (map ppEquation eqs)
> ppDecl (ExternalDecl p cc impent f ty) =
>   sep [text "external" <+> ppCallConv cc <+> maybePP (text . show) impent,
>        indent
>          (ppDecl (TypeSig p [f]
>            (TypeExprWithContext emptyTypeExprContext ty)))]
>   where ppCallConv CallConvPrimitive = text "primitive"
>         ppCallConv CallConvCCall = text "ccall"
> ppDecl (PatternDecl _ t rhs) = ppRule (ppConstrTerm 0 t) equals rhs
> ppDecl (ExtraVariables _ vs) = list (map ppIdent vs) <+> text "free"

> ppDeriving :: [Ident] -> Doc
> ppDeriving [] = empty
> ppDeriving tcs = text "deriving" <+> parenList (map ppIdent tcs)

> ppImportSpec :: ImportSpec -> Doc
> ppImportSpec (Importing _ is) = parenList (map ppImport is)
> ppImportSpec (Hiding _ is) = text "hiding" <+> parenList (map ppImport is)

> ppImport :: Import -> Doc
> ppImport (Import x) = ppIdent x
> ppImport (ImportTypeWith tc cs) = ppIdent tc <> parenList (map ppIdent cs)
> ppImport (ImportTypeAll tc) = ppIdent tc <> text "(..)"

> ppPrec :: Infix -> Int -> Doc
> ppPrec fix p = ppAssoc fix <+> ppPrio p
>   where ppAssoc InfixL = text "infixl"
>         ppAssoc InfixR = text "infixr"
>         ppAssoc Infix = text "infix"
>         ppPrio p = if p < 0 then empty else int p

> ppTypeDeclLhs :: String -> Ident -> [Ident] -> Doc
> ppTypeDeclLhs kw tc tvs = text kw <+> ppIdent tc <+> hsep (map ppIdent tvs)

> ppConstr :: ConstrDecl -> Doc
> ppConstr (ConstrDecl _ tvs c tys) =
>   sep [ppExistVars tvs,ppIdent c <+> fsep (map (ppTypeExpr 2) tys)]
> ppConstr (ConOpDecl _ tvs ty1 op ty2) =
>   sep [ppExistVars tvs,ppTypeExpr 1 ty1,ppInfixOp op <+> ppTypeExpr 1 ty2]

> ppNewConstr :: NewConstrDecl -> Doc
> ppNewConstr (NewConstrDecl _ tvs c ty) =
>   sep [ppExistVars tvs,ppIdent c <+> ppTypeExpr 2 ty]

> ppExistVars :: [Ident] -> Doc
> ppExistVars tvs
>   | null tvs = empty
>   | otherwise = text "forall" <+> hsep (map ppIdent tvs) <+> char '.'

> ppEquation :: Equation -> Doc
> ppEquation (Equation _ lhs rhs) = ppRule (ppLhs lhs) equals rhs

> ppLhs :: Lhs -> Doc
> ppLhs (FunLhs f ts) = ppIdent f <+> fsep (map (ppConstrTerm 2) ts)
> ppLhs (OpLhs t1 f t2) =
>   ppConstrTerm 1 t1 <+> ppInfixOp f <+> ppConstrTerm 1 t2
> ppLhs (ApLhs lhs ts) = parens (ppLhs lhs) <+> fsep (map (ppConstrTerm 2) ts)

> ppRule :: Doc -> Doc -> Rhs -> Doc
> ppRule lhs eq (SimpleRhs _ e ds) =
>   sep [lhs <+> eq,indent (ppExpr 0 e)] $$ ppLocalDefs ds
> ppRule lhs eq (GuardedRhs es ds) =
>   sep [lhs,indent (vcat (map (ppCondExpr eq) es))] $$ ppLocalDefs ds

> ppLocalDefs :: [Decl] -> Doc
> ppLocalDefs ds
>   | null ds = empty
>   | otherwise = indent (text "where" <+> ppBlock ds)

\end{lstlisting}
Interfaces
\begin{lstlisting}

> ppInterface :: Interface -> Doc
> ppInterface (Interface m ds) =
>   text "interface" <+> ppMIdent m <+> text "where" <+> lbrace
>     $$ vcat (punctuate semi (map ppIDecl ds)) $$ rbrace

> ppIDecl :: IDecl -> Doc
> ppIDecl (IImportDecl _ m) = text "import" <+> ppMIdent m
> ppIDecl (IInfixDecl _ fix p op) = ppPrec fix p <+> ppQInfixOp op
> ppIDecl (ITypeClassDecl _ ctx tc tv ds) =
>   text "class" <+> ppTypeExprContextPrefix ctx <+>
>   ppQIdent tc <+> ppIdent tv <+> text "where" <+> lbrace $$
>   (vcat $ map indent $ punctuate semi $ map ppIDecl ds) $$
>   rbrace
> ppIDecl (IInstanceDecl _ ctx tc tyexpr) =
>   text "instance" <+> ppTypeExprContextPrefix ctx <+> ppQIdent tc
>                   <+> ppTypeExpr 2 tyexpr
> ppIDecl (HidingDataDecl _ tc tvs) =
>   text "hiding" <+> ppITypeDeclLhs "data" (qualify tc) tvs
> ppIDecl (IDataDecl _ tc tvs cs) =
>   sep (ppITypeDeclLhs "data" tc tvs :
>        map indent (zipWith (<+>) (equals : repeat vbar) (map ppIConstr cs)))
>   where ppIConstr = maybe (char '_') ppConstr
> ppIDecl (INewtypeDecl _ tc tvs nc) =
>   sep [ppITypeDeclLhs "newtype" tc tvs <+> equals, indent (ppNewConstr nc)]
> ppIDecl (ITypeDecl _ tc tvs ty) =
>   sep [ppITypeDeclLhs "type" tc tvs <+> equals, indent (ppTypeExpr 0 ty)]
> ppIDecl (IFunctionDecl _ f tyWC) =
>   ppQIdent f <+> text "::" <+> ppTypeExprWithContext tyWC
> ppIDecl (IKindDecl _ tc k) = text "kind" <+> ppQIdent tc <+> ppKind k

> ppITypeDeclLhs :: String -> QualIdent -> [Ident] -> Doc
> ppITypeDeclLhs kw tc tvs = text kw <+> ppQIdent tc <+> hsep (map ppIdent tvs)

\end{lstlisting}
Types
\begin{lstlisting}

> ppTypeExpr :: Int -> TypeExpr -> Doc
> ppTypeExpr _ ty@(TypeExprConstructor tc) = ppQIdent tc
> ppTypeExpr _ ty@(TypeExprVariable tv) = ppIdent tv
> ppTypeExpr p ty@(TypeExprApplication ty1 ty2)
>   | isTupleTypeExpr ty = ppTupleType tys
>   | isListTypeExpr ty  = ppListType (head tys)
>   | isArrowTypeExpr ty = parenExp (p > 0) (fsep (ppArrowType ty))
>   | otherwise = parenExp (p > 1 && not (null tys))
>                   (ppTypeExpr 0 leftmost <+> fsep (map (ppTypeExpr 2) tys))
>   where leftmost = leftmostTypeExpr ty
>         tys = typeExprArguments ty

> ppTupleType :: [TypeExpr] -> Doc
> ppTupleType tys = parenList (map (ppTypeExpr 0) tys)

> ppListType :: TypeExpr -> Doc
> ppListType ty = brackets (ppTypeExpr 0 ty)

> ppArrowType :: TypeExpr -> [Doc]
> ppArrowType ty
>   | isArrowTypeExpr ty = (ppTypeExpr 1 arg1 <+> rarrow) : (ppArrowType arg2)
>   | otherwise = [ppTypeExpr 0 ty]
>   where args = typeExprArguments ty
>         [arg1,arg2] = args

\end{lstlisting}
Type classes contexts.
\begin{lstlisting}

> ppTypeExprWithContext :: TypeExprWithContext -> Doc
> ppTypeExprWithContext (TypeExprWithContext ctx tyexpr) =
>   ppTypeExprContextPrefix ctx <+> ppTypeExpr 0 tyexpr

> ppTypeExprContextPrefix :: TypeExprContext -> Doc
> ppTypeExprContextPrefix (TypeExprContext []) = empty
> ppTypeExprContextPrefix ctx = ppTypeExprContext ctx <+> text "=>"

> ppTypeExprContext :: TypeExprContext -> Doc
> ppTypeExprContext (TypeExprContext []) = empty
> ppTypeExprContext (TypeExprContext [tcc]) = ppTypeExprClassConstraint tcc
> ppTypeExprContext (TypeExprContext tccs) =
>   parenList (map ppTypeExprClassConstraint tccs)

> ppTypeExprClassConstraint :: TypeExprClassConstraint -> Doc
> ppTypeExprClassConstraint (TypeExprClassConstraint tc ty) =
>   ppQIdent tc <+> ppTypeExpr 0 ty

\end{lstlisting}
Kinds. Stars are represented as ''@'' because ''*'' is a valid token in
operator names.
\begin{lstlisting}

> ppKind :: Kind -> Doc
> ppKind (Star) = text "@"
> ppKind (KFun k1@(Star)   k2) = (ppKind k1) <+> text "->" <+> (ppKind k2)
> ppKind (KFun k1@(KVar _) k2) = (ppKind k1) <+> text "->" <+> (ppKind k2)
> ppKind (KFun k1          k2) = parens (ppKind k1) <+> text "->" <+> (ppKind k2)
> ppKind (KVar kv) = ppIdent kv

\end{lstlisting}
Literals
\begin{lstlisting}

> ppLiteral :: Literal -> Doc
> ppLiteral (Char c) = text (show c)
> ppLiteral (Int _ i) = int i
> ppLiteral (Float f) = double f
> ppLiteral (String s) = text (show s)

\end{lstlisting}
Patterns
\begin{lstlisting}

> ppConstrTerm :: Int -> ConstrTerm -> Doc
> ppConstrTerm p (LiteralPattern l) =
>   parenExp (p > 1 && isNegative l) (ppLiteral l)
>   where isNegative (Char _) = False
>         isNegative (Int _ i) = i < 0
>         isNegative (Float f) = f < 0.0
>         isNegative (String _ ) = False
> ppConstrTerm p (NegativePattern op l) =
>   parenExp (p > 1) (ppInfixOp op <> ppLiteral l)
> ppConstrTerm _ (VariablePattern v) = ppIdent v
> ppConstrTerm p (ConstructorPattern c ts) =
>   parenExp (p > 1 && not (null ts))
>            (ppQIdent c <+> fsep (map (ppConstrTerm 2) ts))
> ppConstrTerm p (InfixPattern t1 c t2) =
>   parenExp (p > 0)
>            (sep [ppConstrTerm 1 t1 <+> ppQInfixOp c,
>                  indent (ppConstrTerm 0 t2)])
> ppConstrTerm _ (ParenPattern t) = parens (ppConstrTerm 0 t)
> ppConstrTerm _ (TuplePattern ts) = parenList (map (ppConstrTerm 0) ts)
> ppConstrTerm _ (ListPattern ts) = bracketList (map (ppConstrTerm 0) ts)
> ppConstrTerm _ (AsPattern v t) = ppIdent v <> char '@' <> ppConstrTerm 2 t
> ppConstrTerm _ (LazyPattern t) = char '~' <> ppConstrTerm 2 t

\end{lstlisting}
Expressions
\begin{lstlisting}

> ppCondExpr :: Doc -> CondExpr -> Doc
> ppCondExpr eq (CondExpr _ g e) =
>   vbar <+> sep [ppExpr 0 g <+> eq,indent (ppExpr 0 e)]

> ppExpr :: Int -> Expression -> Doc
> ppExpr _ (Literal l) = ppLiteral l
> ppExpr _ (Variable v) = ppQIdent v
> ppExpr _ (Constructor c) = ppQIdent c
> ppExpr _ (Paren e) = parens (ppExpr 0 e)
> ppExpr p (Typed e tyWC) =
>   parenExp (p > 0) (ppExpr 0 e <+> text "::" <+> ppTypeExprWithContext tyWC)
> ppExpr _ (Tuple es) = parenList (map (ppExpr 0) es)
> ppExpr _ (List es) = bracketList (map (ppExpr 0) es)
> ppExpr _ (ListCompr e qs) =
>   brackets (ppExpr 0 e <+> vbar <+> list (map ppStmt qs))
> ppExpr _ (EnumFrom e) = brackets (ppExpr 0 e <+> text "..")
> ppExpr _ (EnumFromThen e1 e2) =
>   brackets (ppExpr 0 e1 <> comma <+> ppExpr 0 e2 <+> text "..")
> ppExpr _ (EnumFromTo e1 e2) =
>   brackets (ppExpr 0 e1 <+> text ".." <+> ppExpr 0 e2)
> ppExpr _ (EnumFromThenTo e1 e2 e3) =
>   brackets (ppExpr 0 e1 <> comma <+> ppExpr 0 e2
>               <+> text ".." <+> ppExpr 0 e3)
> ppExpr p (UnaryMinus op e) = parenExp (p > 1) (ppInfixOp op <> ppExpr 1 e)
> ppExpr p (Apply e1 e2) =
>   parenExp (p > 1) (sep [ppExpr 1 e1,indent (ppExpr 2 e2)])
> ppExpr p (InfixApply e1 op e2) =
>   parenExp (p > 0) (sep [ppExpr 1 e1 <+> ppQInfixOp (opName op),
>                          indent (ppExpr 1 e2)])
> ppExpr _ (LeftSection e op) = parens (ppExpr 1 e <+> ppQInfixOp (opName op))
> ppExpr _ (RightSection op e) = parens (ppQInfixOp (opName op) <+> ppExpr 1 e)
> ppExpr p (Lambda t e) =
>   parenExp (p > 0)
>            (sep [backsl <> fsep (map (ppConstrTerm 2) t) <+> rarrow,
>                  indent (ppExpr 0 e)])
> ppExpr p (Let ds e) =
>   parenExp (p > 0)
>            (sep [text "let" <+> ppBlock ds <+> text "in",ppExpr 0 e])
> ppExpr p (Do sts e) =
>   parenExp (p > 0) (text "do" <+> vcat (map ppStmt sts) $$ ppExpr 0 e)
> ppExpr p (IfThenElse e1 e2 e3) =
>   parenExp (p > 0)
>            (text "if" <+>
>             sep [ppExpr 0 e1,
>                  text "then" <+> ppExpr 0 e2,
>                  text "else" <+> ppExpr 0 e3])
> ppExpr p (Case e alts) =
>   parenExp (p > 0)
>            (text "case" <+> ppExpr 0 e <+> text "of" $$
>             indent (vcat (map ppAlt alts)))
> ppExpr p (OverloadedVariablePH tc ty) =
>   parens (text "OVPH" <+> ppQIdent tc <+> text (show ty))

> ppStmt :: Statement -> Doc
> ppStmt (StmtExpr e) = ppExpr 0 e
> ppStmt (StmtBind t e) = sep [ppConstrTerm 0 t <+> larrow,indent (ppExpr 0 e)]
> ppStmt (StmtDecl ds) = text "let" <+> ppBlock ds

> ppAlt :: Alt -> Doc
> ppAlt (Alt _ t rhs) = ppRule (ppConstrTerm 0 t) rarrow rhs

> ppOp :: InfixOp -> Doc
> ppOp (InfixOp op) = ppQInfixOp op
> ppOp (InfixConstr op) = ppQInfixOp op

\end{lstlisting}
Goals
\begin{lstlisting}

> ppGoal :: Goal -> Doc
> ppGoal (Goal _ e ds) = sep [ppExpr 0 e,indent (ppLocalDefs ds)]

\end{lstlisting}
Names
\begin{lstlisting}

> ppIdent :: Ident -> Doc
> ppIdent x = parenExp (isInfixOp x) (text (name x))

> ppQIdent :: QualIdent -> Doc
> ppQIdent x = parenExp (isQInfixOp x) (text $ qualName x')
>   where x' = qualUnqualify internalTypeMIdent x

> ppInfixOp :: Ident -> Doc
> ppInfixOp x = backQuoteExp (not (isInfixOp x)) (text (name x))

> ppQInfixOp :: QualIdent -> Doc
> ppQInfixOp x = backQuoteExp (not (isQInfixOp x)) (text (qualName x'))
>   where x' = qualUnqualify internalTypeMIdent x

> ppMIdent :: ModuleIdent -> Doc
> ppMIdent m = text (moduleName m)

\end{lstlisting}
Print printing utilities
\begin{lstlisting}

> indent :: Doc -> Doc
> indent = nest 2

> maybePP :: (a -> Doc) -> Maybe a -> Doc
> maybePP pp = maybe empty pp

> parenExp :: Bool -> Doc -> Doc
> parenExp b doc = if b then parens doc else doc

> backQuoteExp :: Bool -> Doc -> Doc
> backQuoteExp b doc = if b then backQuote <> doc <> backQuote else doc

> list, parenList, bracketList, braceList :: [Doc] -> Doc
> list = fsep . punctuate comma
> parenList = parens . list
> bracketList = brackets . list
> braceList = braces . list

> backQuote,backsl,vbar,rarrow,larrow :: Doc
> backQuote = char '`'
> backsl = char '\\'
> vbar = char '|'
> rarrow = text "->"
> larrow = text "<-"

\end{lstlisting}
