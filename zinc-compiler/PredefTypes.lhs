% $Id: PredefTypes.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2003, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{PredefTypes.lhs}
\codesection{Predefined types}
\begin{lstlisting}

> module PredefTypes(initPEnv,initTCEnv,initDCEnv,initKindEnv,
>                    initTypeClassEnv,initInstanceEnv) where
> import TopEnv
> import DualTopEnv(emptyDualTopEnv)
> import TypeConstructorEnv
> import ValueEnv
> import PEnv
> import KindEnv
> import TypeClassEnv
> import InstanceEnv
> import Ident
> import Types
> import CurrySyntax
> import Kind

\end{lstlisting}
The list and unit data types must be predefined because their
definitions
\begin{lstlisting}
data () = ()
data [] a = [] | a : [a]
\end{lstlisting}
are not allowed by Curry's syntax. The corresponding types
are available in the environments \texttt{initTCEnv} and
\texttt{initDCEnv}. In addition, the precedence of the (infix) list
constructor is available in the environment \texttt{initPEnv}.
\begin{lstlisting}

> initPEnv :: PEnv
> initPEnv = predefPEnv emptyTopEnv
>   where predefPEnv = bindPredefTopEnv qConsId consPrecInfo .
>                      bindPredefTopEnv (qualify consId) consPrecInfo
>         consPrecInfo = (PrecInfo qConsId (OpPrec InfixR 5))

> initTCEnv :: TCEnv
> initTCEnv = foldr (uncurry predefTC) emptyTopEnv predefTypes
>   where predefTC ty cs = case leftmostType ty of
>           (TypeConstructor tc) ->
>              let arity = length $ typeArguments ty
>                  dt = DataType tc arity (map Just cs)
>              in bindPredefTopEnv tc dt .
>                 bindPredefTopEnv (qualify $ unqualify tc) dt

> initDCEnv :: ValueEnv
> initDCEnv =
>   foldr (uncurry predefDC) emptyTopEnv
>         [(c,constrType (polyType ty) n' tys)
>         | (ty,cs) <- predefTypes, Data c n' tys <- cs]
>   where primTypes = map snd (moduleImports preludeMIdent initTCEnv)
>         predefDC :: Ident -> ExistTypeScheme -> ValueEnv -> ValueEnv
>         predefDC c ty =
>           bindPredefTopEnv c'  (DataConstructor c' ty) .
>           bindPredefTopEnv c'' (DataConstructor c' ty)
>           where c'  = qualifyWith internalTypeMIdent c
>                 c'' = qualify c
>         constrType (ForAll n (TypeWithContext (TypeContext []) ty)) n' =
>           ForAllExist n n' . foldr typeArrow ty

> predefTypes :: [(Type,[Data [Type]])]
> predefTypes =
>   let a = typeVar 0
>       b = typeVar 1
>   in [
>     (unitType,      [Data noDataId 0 []]),
>     (listType a,    [Data nilId 0 [],Data consId 0 [a,listType a]]),
>     (typeArrow a b, [Data arrowId 0 []])
>   ]

> initKindEnv :: KindEnv
> initKindEnv = foldr (uncurry predefKind) -- TypeConstructorKind (mkMIdent []))
>                     emptyTopEnv kindInfoList
>   where predefKind :: QualIdent -> KindInfo -> KindEnv -> KindEnv
>         predefKind qid kindInfo =
>            bindPredefTopEnv qid kindInfo .
>            bindPredefTopEnv (qualify $ unqualify qid) kindInfo
>         kindInfoList :: [(QualIdent,KindInfo)]
>         kindInfoList = map (\(qid,k) -> (qid,KindInfo qid k))
>                            predefTypeConstructorKinds

> predefTypeConstructorKinds :: [(QualIdent,Kind)]
> predefTypeConstructorKinds =
>   [ (qUnitId,Star),
>     (qListId,KFun Star Star),
>     (qArrowId,KFun Star $ KFun Star Star)
>   ]

> initTypeClassEnv :: TypeClassEnv
> initTypeClassEnv = emptyTopEnv

> initInstanceEnv :: InstanceEnv
> initInstanceEnv = emptyDualTopEnv

\end{lstlisting}
