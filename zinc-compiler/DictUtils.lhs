% $Id: DictUtils.lhs 1341 2004-09-07 15:50:08Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{DictUtils.lhs}
\codesection{Dictionary utility functions}
Some common functions shared by the modules related to the dictionary
transformation.
\begin{lstlisting}

> module DictUtils where
> import Ident
> import Env
> import TypeExpr
> import TypeClassEnv
> import Maybe
> import List
> import TypeExprSubst
> import Types
> import TypeTrans(fromType,toTypeWithContext)
> import TypeExpansion(expandAliasType)
> import CurrySyntaxUtils(tvUsedBy)
> import CurrySyntax

\end{lstlisting}
Generation of identifiers.
\begin{lstlisting}

> dictTypeClassDataIdent :: Ident -> Ident
> dictTypeClassDataIdent typeClass =
>   mkIdent $ "_dictData_" ++ (name typeClass)

> dictTypeClassDataQIdent :: QualIdent -> QualIdent
> dictTypeClassDataQIdent typeClass =
>   qualifyLike typeClass $ dictTypeClassDataIdent $ unqualify typeClass

> dictTypeClassDataConstrIdent :: Ident -> Ident
> dictTypeClassDataConstrIdent typeClass =
>   mkIdent $ "_DictData_" ++ (name typeClass)

> dictTypeClassDataConstrQIdent :: QualIdent -> QualIdent
> dictTypeClassDataConstrQIdent typeClass =
>   qualifyLike typeClass $ dictTypeClassDataConstrIdent $ unqualify typeClass

> dictSelectorIdent :: Ident -> Ident
> dictSelectorIdent method = mkIdent $ "_dictSel_" ++ name method

> dictSelectorQIdent :: QualIdent -> QualIdent
> dictSelectorQIdent method = qualifyWith m $ dictSelectorIdent method'
>   where (Just m,method') = splitQualIdent method

> dictTraverserIdent :: QualIdent -> QualIdent -> Ident
> dictTraverserIdent typeClass superTypeClass =
>   mkIdent $ "_dictTrav_" ++ encodeQualIdent superTypeClass ++ "_from_" ++
>             encodeQualIdent typeClass

> dictTraverserQIdent :: QualIdent -> QualIdent -> QualIdent
> dictTraverserQIdent typeClass superTypeClass =
>   qualifyLike typeClass $ dictTraverserIdent typeClass superTypeClass

> dictLiftedMethodIdent :: QualIdent -> QualIdent -> Ident -> Ident
> dictLiftedMethodIdent typeClass typeConstructor method =
>   mkIdent $ "_dictMethod_" ++ encodeQualIdent typeClass ++ "_" ++
>             encodeQualIdent typeConstructor ++ "_" ++ name method

> dictInstanceIdent :: QualIdent -> QualIdent -> Ident
> dictInstanceIdent typeClass typeConstructor =
>   mkIdent $ "_dictInst_" ++ encodeQualIdent typeClass ++ "_" ++
>             encodeQualIdent typeConstructor

> dictTypeVariablePrefix :: QualIdent -> Ident -> Ident
> dictTypeVariablePrefix typeClass =
>   prefixIdent (encodeQualIdent typeClass ++ "_")

> dictGenerateTypeVariables :: [Ident] -> [Ident]
> dictGenerateTypeVariables skip = 
>   [mkIdent ("_tv" ++ show i) | i <- [1..]] \\ (nub skip)

\end{lstlisting}
Auxiliary functions.

\ToDo{Proper encoding of special characters.}
\begin{lstlisting}

> zipExtL :: [[a]] -> [b] -> [(a,b)]
> zipExtL xs ys = foldr (\(xs',y) l -> zip xs' (repeat y) ++ l) [] (zip xs ys)

> zipExtLWith :: (a -> b -> c) -> [[a]] -> [b] -> [c]
> zipExtLWith f xs ys = map (uncurry f) $ zipExtL xs ys

> encodeQualIdent :: QualIdent -> String
> encodeQualIdent = map (\x -> if x=='.' then '_' else x) . qualName

> sortMethods :: Env Ident TypeExprWithContext -> [(Ident,TypeExprWithContext)]
> sortMethods sigEnv = sortBy (\x y -> compare (fst x) (fst y)) (envToList sigEnv)

> nullDictionary :: Expression
> nullDictionary = Apply (Variable $ qualifyWith preludeMIdent $ mkIdent "error")
>                        (Literal $ String "null dictionary")

\end{lstlisting}
Obtaining the type expression of a dictionary.

\ToDo{Sort methods.}
\begin{lstlisting}

> dictTypeClassDataRhs :: TypeClassEnv -> QualIdent -> (TypeExprWithContext,Ident)
> dictTypeClassDataRhs typeClassEnv typeClass =
>   (TypeExprWithContext ctx
>      (typeExprApplyConstructor
>        (dictTypeClassDataConstrQIdent typeClass)
>        ((map removeTypeExprContext superDicts) ++
>         (map removeTypeExprContext methodSigs))),
>    tv)
>   where [TypeClassInfo _ tv superIds sigEnv] =
>           qualLookupTypeClass typeClass typeClassEnv
>         superDicts, methodSigs :: [TypeExprWithContext]
>         superDicts = map rewriteSuper superIds
>         methodSigs = map snd $ envToList sigEnv
>         ctx = foldr composeTypeExprContext emptyTypeExprContext
>                     (map typeExprContext (superDicts ++ methodSigs))
>         rewriteSuper superId =
>           let tyWC  = dictTypeClassDataLhs typeClassEnv superId
>               tvs   = tvUsedBy (removeTypeExprContext tyWC)
>               subst = foldr (\id -> bindTypeExprVar id (TypeExprVariable (dictTypeVariablePrefix superId id)))
>                             idSubst
>                             (tail tvs)
>               subst' = bindTypeExprVar (head tvs) (TypeExprVariable tv) subst
>           in  substTypeExpr subst' tyWC

> dictTypeClassDataLhs :: TypeClassEnv -> QualIdent -> TypeExprWithContext
> dictTypeClassDataLhs typeClassEnv typeClass =
>   TypeExprWithContext
>     (typeExprContext rhs)
>     (typeExprApplyConstructor
>       (dictTypeClassDataQIdent typeClass)
>       (map TypeExprVariable tvs))
>   where (rhs,tv) = dictTypeClassDataRhs typeClassEnv typeClass
>         tvs = (tv : ((nub $ tvUsedBy $ removeTypeExprContext rhs) \\ [tv]))

\end{lstlisting}
Obtaining the type expression of some dictionary constructions.
\begin{lstlisting}

> liftedMethodTypeExpr :: QualIdent -> Ident -> [[QualIdent]] ->
>                         TypeExprWithContext -> TypeExprWithContext
> liftedMethodTypeExpr typeConstr tv ctx tyexprWC = substTypeExpr sigma tyexprWC'
>   where tyexprWC'  = mapTypeExprClassConstraint ((++) classPreds) tyexprWC
>         classPreds = zipExtLWith TypeExprClassConstraint ctx tvs
>         tvs = take (length ctx)
>             $ map TypeExprVariable
>             $ dictGenerateTypeVariables []
>         sigma = bindTypeExprVar tv
>                   (typeExprApplyConstructor typeConstr tvs)
>                   idSubst

> instanceValueTypeExpr :: TypeClassEnv -> QualIdent -> QualIdent -> [[QualIdent]] ->
>                          TypeExprWithContext
> instanceValueTypeExpr typeClassEnv typeClass typeConstr instCtx =
>   mapTypeExprClassConstraint ((++) instCtx') tyexprWC'
>   where tyexprWC   = dictTypeClassDataLhs typeClassEnv typeClass
>         tyexprWC'  = substTypeExpr sigma tyexprWC
>         dataTvs    = tvUsedBy $ removeTypeExprContext tyexprWC
>         classTv    = head dataTvs
>         sigma      = bindTypeExprVar classTv instTyexpr idSubst
>         instTvs    = take (length instCtx) $
>                        map TypeExprVariable $
>                        dictGenerateTypeVariables dataTvs
>         instTyexpr = foldl TypeExprApplication
>                            (TypeExprConstructor typeConstr) instTvs
>         instCtx'   = zipExtLWith TypeExprClassConstraint instCtx instTvs

> selectorFunctionTypeExpr :: TypeClassEnv -> QualIdent -> Ident ->
>                             TypeExprWithContext -> TypeExpr
> selectorFunctionTypeExpr typeClassEnv typeClass method methodTypeExprWC =
>   typeExprApplyArrowConstructor headTypeExpr
>     (removeTypeExprContext methodTypeExprWC)
>   where headTypeExpr = removeTypeExprContext
>                          (dictTypeClassDataLhs typeClassEnv typeClass)

> traverserFunctionTypeExpr :: TypeClassEnv -> QualIdent -> QualIdent ->
>                              TypeExpr
> traverserFunctionTypeExpr typeClassEnv typeClass superTypeClass =
>   fromType (typeArrow leftType rightType)
>   where left   = dictTypeClassDataLhs typeClassEnv typeClass
>         right  = dictTypeClassDataLhs typeClassEnv superTypeClass
>         right' = substTypeExpr sigma right
>         origTvs = tvUsedBy $ removeTypeExprContext right
>         renamedTvs = map (dictTypeVariablePrefix superTypeClass) origTvs
>         sigma  =  foldr (uncurry bindTypeExprVar) idSubst
>                         (zip origTvs (map TypeExprVariable renamedTvs))
>         leftType  = dictTransType typeClassEnv
>                       (toTypeWithContext
>                          (head (tvUsedBy $removeTypeExprContext left) :
>                           tail renamedTvs)
>                          left)
>         rightType = dictTransType typeClassEnv
>                       (toTypeWithContext renamedTvs right')

\end{lstlisting}
Type transformation.
\begin{lstlisting}

> dictTransType :: TypeClassEnv -> TypeWithContext -> Type
> dictTransType typeClassEnv (TypeWithContext (TypeContext classPreds) ty) =
>   foldr (\tcc ty' -> typeArrow (dictTransTCC tcc ty') ty') ty classPreds
>   where dictTransTCC (TypeClassConstraint tc tv) ty =
>           expandAliasType (tv:[TypeVariable i | i <- [1..],
>                                                 i `notElem` (typeVars ty)])
>             (dictTransType typeClassEnv
>               (toTypeWithContext []
>                 (dictTypeClassDataLhs typeClassEnv tc)))

\end{lstlisting}
