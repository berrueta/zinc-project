% $Id: Typing.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2003-2004, Wolfgang Lux
% Copyright (c) 2003-2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Typing.lhs}
\codesection{Computing the Type of Curry Expressions}
\begin{lstlisting}

> module Typing where
> import ValueEnv
> import Types
> import Ident
> import Combined
> import Monad
> import CurrySyntax
> import TypeSubst
> import Utils
> import TypeExpansion
> import Error(internalError)
> import TypingMonad
> import TopEnv
> import ContextReduction
> import TypeClassEnv
> import InstanceEnv
> import Position

\end{lstlisting}
During the transformation of the Curry source code into the
intermediate language, the compiler has to recompute the type of
expressions. This is simpler than type checking because the types of
all variables are known. Still the compiler must handle polymorphic
types of variables and their instantiation. Fortunately, all
monomorphic types entered into the type environment by the type
inferencer have negative offsets. As all types computed by
\texttt{typeOf} are also monomorphic, we can use positive offsets for
the instantiation of type schemes here without risk of name conflicts.

\ToDo{When computing the type of an expression with a type signature
make use of the annotation instead of recomputing its type. In order
to do this we must either ensure that the type is properly qualified
or we need access to the type constructor environment.}
\begin{lstlisting}

> class Typeable a where
>   simpleTypeOf :: ValueEnv -> a -> Type
>   simpleTypeOf valueEnv x = case typeOf noTypeClassEnv noInstanceEnv valueEnv x of
>     TypeWithContext (TypeContext []) ty -> ty
>     _ -> internalError "simpleTypeOf"
>   typeOf :: TypeClassEnv -> InstanceEnv -> ValueEnv -> a -> TypeWithContext

> instance Typeable Ident where
>   typeOf = computeType identType

> instance Typeable ConstrTerm where
>   typeOf = computeType argType

> instance Typeable Expression where
>   typeOf = computeType exprType

> instance Typeable Rhs where
>   typeOf = computeType rhsType

> computeType :: (ValueEnv -> a -> TyState Type) ->
>                TypeClassEnv -> InstanceEnv -> ValueEnv ->
>                a -> TypeWithContext
> computeType f typeClassEnv instEnv tyEnv x =
>  normalizeTypeWithContext (runTyState doComputeType tyEnv)
>  where doComputeType =
>          do
>            ty      <- f tyEnv x
>            theta   <- fetchSt
>            let ty'  = subst theta ty
>            ctx     <- liftSt $ liftSt $ fetchSt
>            let ctx' = contextReduction noPos typeClassEnv instEnv $ subst theta ctx
>            return $ contextFilter $ TypeWithContext ctx' ty'

> identType :: ValueEnv -> Ident -> TyState Type
> identType tyEnv x = instUniv (varType x tyEnv)

> litType :: ValueEnv -> Literal -> TyState Type
> litType _ (Char _) = return charType
> -- FIXME: overloaded numeric literals
> --litType tyEnv (Int v _) = identType tyEnv v
> litType _ (Int _ _) = return intType
> litType _ (Float _) = return floatType
> litType _ (String _) = return stringType

> argType :: ValueEnv -> ConstrTerm -> TyState Type
> argType tyEnv (LiteralPattern l) = litType tyEnv l
> argType tyEnv (NegativePattern _ l) = litType tyEnv l
> argType tyEnv (VariablePattern v) = identType tyEnv v
> argType tyEnv (ConstructorPattern c ts) =
>   do
>     ty <- instUnivExist (constrType c tyEnv)
>     tys <- mapM (argType tyEnv) ts
>     unifyList (init (flatten ty)) tys
>     return (last (flatten ty))
>   where flatten ty
>           | isArrowType ty = let [tyAlpha,tyBeta] = typeArguments ty
>                              in tyAlpha : flatten tyBeta
>           | otherwise = [ty]
> argType tyEnv (InfixPattern t1 op t2) =
>   argType tyEnv (ConstructorPattern op [t1,t2])
> argType tyEnv (ParenPattern t) = argType tyEnv t
> argType tyEnv (TuplePattern ts)
>   | null ts = return unitType
>   | otherwise = liftM tupleType $ mapM (argType tyEnv) ts                -- $
> argType tyEnv (ListPattern ts) = freshTypeVar >>= flip elemType ts
>   where elemType ty [] = return (listType ty)
>         elemType ty (t:ts) =
>           argType tyEnv t >>= unify ty >> elemType ty ts
> argType tyEnv (AsPattern v _) = argType tyEnv (VariablePattern v)
> argType tyEnv (LazyPattern t) = argType tyEnv t

> exprType :: ValueEnv -> Expression -> TyState Type
> exprType tyEnv (Literal l) = litType tyEnv l
> exprType tyEnv (Variable v) = instUniv (funType v tyEnv)
> exprType tyEnv (Constructor c) = instUnivExist (constrType c tyEnv)
> exprType tyEnv (Typed e _) = exprType tyEnv e
> exprType tyEnv (Paren e) = exprType tyEnv e
> exprType tyEnv (Tuple es)
>   | null es = return unitType
>   | otherwise = liftM tupleType $ mapM (exprType tyEnv) es
> exprType tyEnv (List es) = freshTypeVar >>= flip elemType es
>   where elemType ty [] = return (listType ty)
>         elemType ty (e:es) =
>           exprType tyEnv e >>= unify ty >> elemType ty es
> exprType tyEnv (ListCompr e _) = liftM listType $ exprType tyEnv e
> exprType tyEnv (EnumFrom _) = return (listType intType)
> exprType tyEnv (EnumFromThen _ _) = return (listType intType)
> exprType tyEnv (EnumFromTo _ _) = return (listType intType)
> exprType tyEnv (EnumFromThenTo _ _ _) = return (listType intType)
> exprType tyEnv (UnaryMinus _ e) = exprType tyEnv e
> exprType tyEnv (Apply e1 e2) =
>   do
>     (ty1,ty2) <- exprType tyEnv e1 >>= unifyArrow
>     exprType tyEnv e2 >>= unify ty1
>     return ty2
> exprType tyEnv (InfixApply e1 op e2) =
>   do
>     (ty1,ty2,ty3) <- exprType tyEnv (infixOp op) >>= unifyArrow2
>     exprType tyEnv e1 >>= unify ty1
>     exprType tyEnv e2 >>= unify ty2
>     return ty3
> exprType tyEnv (LeftSection e op) =
>   do
>     (ty1,ty2,ty3) <- exprType tyEnv (infixOp op) >>= unifyArrow2
>     exprType tyEnv e >>= unify ty1
>     return (typeArrow ty2 ty3)
> exprType tyEnv (RightSection op e) =
>   do
>     (ty1,ty2,ty3) <- exprType tyEnv (infixOp op) >>= unifyArrow2
>     exprType tyEnv e >>= unify ty2
>     return (typeArrow ty1 ty3)
> exprType tyEnv (Lambda args e) =
>   do
>     tys <- mapM (argType tyEnv) args
>     ty <- exprType tyEnv e
>     return (foldr typeArrow ty tys)
> exprType tyEnv (Let _ e) = exprType tyEnv e
> exprType tyEnv (Do _ e) = exprType tyEnv e
>   where stmtType tyEnv (StmtExpr e) = exprType tyEnv e
>         stmtType tyEnv _ = internalError "exprType do ..."
> exprType tyEnv (IfThenElse e1 e2 e3) =
>   do
>     exprType tyEnv e1 >>= unify boolType
>     ty2 <- exprType tyEnv e2
>     ty3 <- exprType tyEnv e3
>     unify ty2 ty3
>     return ty3
> exprType tyEnv (Case _ alts) = freshTypeVar >>= flip altType alts
>   where altType ty [] = return ty
>         altType ty (Alt _ _ rhs:alts) =
>           rhsType tyEnv rhs >>= unify ty >> altType ty alts

> rhsType :: ValueEnv -> Rhs -> TyState Type
> rhsType tyEnv (SimpleRhs _ e _) = exprType tyEnv e
> rhsType tyEnv (GuardedRhs es _) = freshTypeVar >>= flip condExprType es
>   where condExprType ty [] = return ty
>         condExprType ty (CondExpr _ _ e:es) =
>           exprType tyEnv e >>= unify ty >> condExprType ty es

\end{lstlisting}
In order to avoid name conflicts with non-generalized type variables
in a type we instantiate quantified type variables using positive
offsets here.
\begin{lstlisting}

> freshTypeVar :: TyState Type
> freshTypeVar = liftM TypeVariable $ liftSt $ updateSt (1 +)

> instType :: Int -> Type -> TyState Type
> instType n ty =
>   do
>     tys <- sequence (replicate n freshTypeVar)
>     return (expandAliasType tys ty)

> instTypeWithContext :: Int -> TypeWithContext -> TyState TypeWithContext
> instTypeWithContext n tyWC =
>   do
>     tys <- sequence (replicate n freshTypeVar)
>     return (expandAliasTypeWithContext tys tyWC)

> instUniv :: TypeScheme -> TyState Type
> instUniv ts =
>   liftM removeTypeContext (instUnivTypeWithContext ts)

> instUnivTypeWithContext :: TypeScheme -> TyState TypeWithContext
> instUnivTypeWithContext (ForAll n tyWC) =
>   do tyWC' <- instTypeWithContext n tyWC
>      liftSt $ liftSt $ updateSt (composeTypeContext $ typeContext tyWC')
>      return tyWC'

> instUnivExist :: ExistTypeScheme -> TyState Type
> instUnivExist (ForAllExist n n' ty) = instType (n + n') ty

\end{lstlisting}
When unifying two types, the non-generalized variables, i.e.,
variables with negative offsets, must not be substituted. Otherwise,
the unification algorithm is identical to the one used by the type
checker.
\begin{lstlisting}

> unify :: Type -> Type -> TyState ()
> unify ty1 ty2 =
>   updateSt_ (\theta -> unifyTypes (subst theta ty1) (subst theta ty2) theta)

> unifyList :: [Type] -> [Type] -> TyState ()
> unifyList tys1 tys2 = sequence_ (zipWith unify tys1 tys2)

> unifyArrow :: Type -> TyState (Type,Type)
> unifyArrow ty =
>   do
>     theta <- fetchSt
>     case subst theta ty of
>       TypeVariable tv
>         | tv >= 0 ->
>             do
>               ty1 <- freshTypeVar
>               ty2 <- freshTypeVar
>               updateSt_ (bindVar tv (typeArrow ty1 ty2))
>               return (ty1,ty2)
>       ty
>         | isArrowType ty -> let [tyAlpha,tyBeta] = typeArguments ty
>                             in return (tyAlpha,tyBeta)
>         | otherwise -> internalError ("unifyArrow (" ++ show ty ++ ")")

> unifyArrow2 :: Type -> TyState (Type,Type,Type)
> unifyArrow2 ty =
>   do
>     (ty1,ty2) <- unifyArrow ty
>     (ty21,ty22) <- unifyArrow ty2
>     return (ty1,ty21,ty22)

> unifyTypes :: Type -> Type -> TypeSubst -> TypeSubst
> unifyTypes (TypeVariable tv1) (TypeVariable tv2) theta
>   | tv1 == tv2 = theta
> unifyTypes (TypeVariable tv) ty theta
>   | tv >= 0 = bindVar tv ty theta
> unifyTypes ty (TypeVariable tv) theta
>   | tv >= 0 = bindVar tv ty theta
> unifyTypes (TypeConstructor tc1) (TypeConstructor tc2) theta
>   | tc1 == tc2 = theta
> unifyTypes (TypeConstrained tys1 tv1) (TypeConstrained tys2 tv2) theta
>   | tv1 == tv2 = theta
> unifyTypes (TypeSkolem k1) (TypeSkolem k2) theta
>   | k1 == k2 = theta
> unifyTypes (TypeApplication ty1 ty2) (TypeApplication ty1' ty2') theta =
>   unifyTypes ty1 ty1' (unifyTypes ty2 ty2' theta)
> unifyTypes ty1 ty2 _ =
>   internalError ("Typing.unifyTypes:\n  (" ++ show ty1 ++
>                  ")\n  (" ++ show ty2 ++ ")")

> unifyTypeWithContext :: TypeWithContext -> TypeWithContext -> TyState ()
> unifyTypeWithContext tyWC1 tyWC2 =
>   unify (removeTypeContext tyWC1) (removeTypeContext tyWC2)

\end{lstlisting}
The functions \texttt{constrType}, \texttt{varType}, and
\texttt{funType} are used to compute the type of constructors,
pattern variables, and variables.

\ToDo{These functions should be shared with the type checker.}
\begin{lstlisting}

> constrType :: QualIdent -> ValueEnv -> ExistTypeScheme
> constrType c tyEnv =
>   case qualLookupValue c tyEnv of
>     [DataConstructor    _ ets] -> ets
>     [NewtypeConstructor _ ets] -> ets
>     _ -> internalError ("Typing.constrType " ++ show c)

> varType :: Ident -> ValueEnv -> TypeScheme
> varType v tyEnv =
>   case lookupValue v tyEnv of
>     [Value _ ts] -> ts
>     _ -> internalError ("Typing.varType " ++ show v)

> funType :: QualIdent -> ValueEnv -> TypeScheme
> funType f tyEnv =
>   case qualLookupValue f tyEnv of
>     [Value _ ts] -> ts
>     _ -> internalError ("Typing.funType " ++ show f)

\end{lstlisting}
Auxiliary functions.
\begin{lstlisting}

> noPos :: Position
> noPos = internalError "Typing.noPos"

\end{lstlisting}
