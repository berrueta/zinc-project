% $Id: DictTransEnv.lhs 1287 2004-09-03 12:58:53Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{DictTransEnv.lhs}
\codesection{Dictionary transformation of value environment}
\begin{lstlisting}

> module DictTransEnv(dictTransValueEnv,dictTransTypeScheme) where
> import ValueEnv
> import Types
> import DictUtils
> import TypeClassEnv
> import TypeExpansion(normalize)
> import List

\end{lstlisting}
After the dictionary transformation of expressions has taken place,
the type environment must be also transformed to remove all the
references to class constraints and add the dictionary arguments.

\ToDo{Transform existentially qualified type schemes.}
\begin{lstlisting}

> dictTransValueEnv :: TypeClassEnv -> ValueEnv -> ValueEnv
> dictTransValueEnv typeClassEnv = fmap (dictTransValueInfo typeClassEnv)

> dictTransValueInfo :: TypeClassEnv -> ValueInfo -> ValueInfo
> dictTransValueInfo typeClassEnv (DataConstructor    qi ets) =
>   (DataConstructor qi (dictTransExistTypeScheme ets))
> dictTransValueInfo typeClassEnv (NewtypeConstructor qi ets) =
>   (NewtypeConstructor qi (dictTransExistTypeScheme ets))
> dictTransValueInfo typeClassEnv (Value              qi  ts) =
>   (Value qi (dictTransTypeScheme typeClassEnv ts))

> dictTransTypeScheme :: TypeClassEnv -> TypeScheme -> TypeScheme
> dictTransTypeScheme typeClassEnv (ForAll n typeWithContext) =
>   (ForAll n' (TypeWithContext emptyTypeContext ty))
>   where ty = normalize (dictTransType typeClassEnv typeWithContext)
>         n' = length (nub (typeVars ty))

> dictTransExistTypeScheme :: ExistTypeScheme -> ExistTypeScheme
> dictTransExistTypeScheme (ForAllExist n m ty) = (ForAllExist n m ty)

\end{lstlisting}
