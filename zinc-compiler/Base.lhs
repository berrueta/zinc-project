% $Id: Base.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2004, Wolfgang Lux
% Copyright (c) 2003-2005, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Base.lhs}
\codesection{Common Definitions for the Compiler}
The module \texttt{Base} provides common definitions for the various 
phases of the compiler.
\begin{lstlisting}

> module Base where
> import Ident
> import Position

\end{lstlisting}
The function \texttt{linear} checks whether a list of entities is
linear, i.e., if every entity in the list occurs only once. If it is
non-linear, the first offending object is returned.
\begin{lstlisting}

> data Linear a = Linear | NonLinear a

> linear :: Eq a => [a] -> Linear a
> linear (x:xs)
>   | x `elem` xs = NonLinear x
>   | otherwise = linear xs
> linear [] = Linear

\end{lstlisting}
In order to give precise error messages on duplicate definitions of
identifiers, the compiler pairs identifiers with their position in the
source file when passing them to the function above. However, the
position must be ignored when comparing two such pairs.
\begin{lstlisting}

> data PIdent = PIdent Position Ident
>             deriving (Show)

> instance Eq PIdent where
>   PIdent _ x == PIdent _ y = x == y

\end{lstlisting}
