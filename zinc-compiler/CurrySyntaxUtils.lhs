% $Id: CurrySyntaxUtils.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{CurrySyntaxUtils.lhs}
\codesection{Utility functions with Curry syntax}
This module contains two type classes used to extract information
from the syntactical tree.
\begin{lstlisting}

> module CurrySyntaxUtils where
> import TypeExpr
> import CurrySyntax
> import Ident
> import List(nub)

\end{lstlisting}
The \lstinline{DefinesTypes} type class groups the syntactical elements
for which is possible to compute the set of type constructors and
type variables (parameters) defined by.
\begin{lstlisting}

> class DefinesTypes a where
>   tcDefinedBy :: a -> [Ident]
>   tvDefinedBy :: a -> [Ident]

> instance DefinesTypes Decl where
>   tcDefinedBy (DataDecl      _   id _ _ _) = [id]
>   tcDefinedBy (NewtypeDecl   _   id _ _ _) = [id]
>   tcDefinedBy (TypeDecl      _   id _ _  ) = [id]
>   tcDefinedBy (TypeClassDecl _ _ id _ _  ) = [id]
>   tcDefinedBy _                            = []
>
>   tvDefinedBy (DataDecl      _ _   varIds _ _) = varIds
>   tvDefinedBy (NewtypeDecl   _ _   varIds _ _) = varIds
>   tvDefinedBy (TypeDecl      _ _   varIds _  ) = varIds
>   tvDefinedBy (TypeClassDecl _ _ _ varId  _  ) = [varId]
>   tvDefinedBy _                                = []

> instance DefinesTypes IDecl where
>   tcDefinedBy (IDataDecl      _   id _ _) = [unqualify id]
>   tcDefinedBy (INewtypeDecl   _   id _ _) = [unqualify id]
>   tcDefinedBy (ITypeDecl      _   id _ _) = [unqualify id]
>   tcDefinedBy (ITypeClassDecl _ _ id _ _) = [unqualify id]
>   tcDefinedBy _                           = []
>
>   tvDefinedBy (IDataDecl      _ _   varIds _) = varIds
>   tvDefinedBy (INewtypeDecl   _ _   varIds _) = varIds
>   tvDefinedBy (ITypeDecl      _ _   varIds _) = varIds
>   tvDefinedBy (ITypeClassDecl _ _ _ varId _ ) = [varId]
>   tvDefinedBy _                              = []

\end{lstlisting}
The \lstinline{DefinesTypes} type class groups the syntactical elements
for which is possible to compute the set of type constructors and
type variables (parameters) used by.

Notice that for declarations, type variables in LHS are not included.
\begin{lstlisting}

> class UsesTypes a where
>   tcUsedBy :: a -> [QualIdent]
>   tvUsedBy :: a -> [Ident]

> instance UsesTypes a => UsesTypes [a] where
>   tcUsedBy = nub . concat . map tcUsedBy
>   tvUsedBy = nub . concat . map tvUsedBy

> instance UsesTypes a => UsesTypes (Maybe a) where
>   tcUsedBy (Nothing) = []
>   tcUsedBy (Just x ) = tcUsedBy x
>   tvUsedBy (Nothing) = []
>   tvUsedBy (Just x ) = tvUsedBy x

> instance UsesTypes Decl where
>   tcUsedBy (DataDecl _ _ _ constrDecls _)  = tcUsedBy constrDecls
>   tcUsedBy (NewtypeDecl _ _ _ constr _)    = tcUsedBy constr
>   tcUsedBy (TypeDecl _ _ _ tyexpr)         = tcUsedBy tyexpr
>   tcUsedBy (TypeSig _ _ qtyexpr)           = tcUsedBy qtyexpr
>   tcUsedBy (TypeClassDecl _ ctx _ _ decls) =
>     nub (tcUsedBy ctx ++ tcUsedBy decls)
>   tcUsedBy (InstanceDecl _ ctx tc tyexpr decls) =
>     nub (tcUsedBy ctx ++ [tc] ++ tcUsedBy tyexpr ++ tcUsedBy decls)
>   tcUsedBy (FunctionDecl _ _ eqs)          = tcUsedBy eqs
>   tcUsedBy (ExternalDecl _ _ _ _ tyexpr)   = tcUsedBy tyexpr
>   tcUsedBy (PatternDecl _ _ rhs)           = tcUsedBy rhs
>   tcUsedBy (ImportDecl _ _ _ _ _)          = []
>   tcUsedBy (InfixDecl _ _ _ _)             = []
>   tcUsedBy (EvalAnnot _ _ _)               = []
>   tcUsedBy (ExtraVariables _ _)            = []
>
>   tvUsedBy (DataDecl _ _ _ constrDecls _)  = tvUsedBy constrDecls
>   tvUsedBy (NewtypeDecl _ _ _ constr _)    = tvUsedBy constr
>   tvUsedBy (TypeDecl _ _ _ tyexpr)         = tvUsedBy tyexpr
>   tvUsedBy (TypeSig _ _ tyexpr)            = tvUsedBy tyexpr
>   tvUsedBy (TypeClassDecl _ ctx _ _ decls) = tvUsedBy decls
>   tvUsedBy (InstanceDecl _ ctx _ tyexpr decls) =
>     nub (tvUsedBy tyexpr ++ tvUsedBy decls)
>   tvUsedBy (FunctionDecl _ _ eqs)          = tvUsedBy eqs
>   tvUsedBy (ExternalDecl _ _ _ _ tyexpr)   = tvUsedBy tyexpr
>   tvUsedBy (PatternDecl _ _ rhs)           = tvUsedBy rhs
>   tvUsedBy (ImportDecl _ _ _ _ _)          = []
>   tvUsedBy (InfixDecl _ _ _ _)             = []
>   tvUsedBy (EvalAnnot _ _ _)               = []
>   tvUsedBy (ExtraVariables _ _)            = []

> instance UsesTypes IDecl where
>   tcUsedBy (IDataDecl _ _ _ constrDecls)    = tcUsedBy constrDecls
>   tcUsedBy (INewtypeDecl _ _ tyvars constr) = tcUsedBy constr
>   tcUsedBy (ITypeDecl _ _ _ tyexpr)         = tcUsedBy tyexpr
>   tcUsedBy (IFunctionDecl _ _ qtyexpr)      = tcUsedBy qtyexpr
>   tcUsedBy (ITypeClassDecl _ ctx _ _ decls) =
>     nub (tcUsedBy ctx ++ tcUsedBy decls)
>   tcUsedBy (IInstanceDecl _ ctx tc tyexpr)  =
>     nub (tcUsedBy ctx ++ [tc] ++ tcUsedBy tyexpr)
>   tcUsedBy (IImportDecl _ _)                = []
>   tcUsedBy (IInfixDecl _ _ _ _)             = []
>   tcUsedBy (HidingDataDecl _ _ _)           = []
>   tcUsedBy (IKindDecl _ tc _)               = [tc]
>
>   tvUsedBy (IDataDecl _ _ _ constrDecls)    = tvUsedBy constrDecls
>   tvUsedBy (INewtypeDecl _ _ _ constr)      = tvUsedBy constr
>   tvUsedBy (ITypeDecl _ _ _ tyexpr)         = tvUsedBy tyexpr
>   tvUsedBy (IFunctionDecl _ _ tyexpr)       = tvUsedBy tyexpr
>   tvUsedBy (ITypeClassDecl _ ctx _ _ decls) = tvUsedBy decls
>   tvUsedBy (IInstanceDecl _ ctx _ tyexpr)   =
>     nub (tvUsedBy ctx ++ tvUsedBy tyexpr)
>   tvUsedBy (IImportDecl _ _)                = []
>   tvUsedBy (IInfixDecl _ _ _ _)             = []
>   tvUsedBy (HidingDataDecl _ _ _)           = []
>   tvUsedBy (IKindDecl _ _ _)                = []

> instance UsesTypes ConstrDecl where
>   tcUsedBy (ConstrDecl _ _ _ tyexprs) = tcUsedBy tyexprs
>   tcUsedBy (ConOpDecl  _ _ tyexpr1 _ tyexpr2) =
>     nub (tcUsedBy tyexpr1 ++ tcUsedBy tyexpr2)
>   tvUsedBy (ConstrDecl _ _ _ tyexprs) = tvUsedBy tyexprs
>   tvUsedBy (ConOpDecl  _ _ tyexpr1 _ tyexpr2) =
>     nub (tvUsedBy tyexpr1 ++ tvUsedBy tyexpr2)

> instance UsesTypes NewConstrDecl where
>   tcUsedBy (NewConstrDecl _ _ _ tyexpr) = tcUsedBy tyexpr
>   tvUsedBy (NewConstrDecl _ _ _ tyexpr) = tvUsedBy tyexpr

> instance UsesTypes Equation where
>   tcUsedBy (Equation p cts rhs) = tcUsedBy rhs
>   tvUsedBy (Equation p cts rhs) = tvUsedBy rhs

> instance UsesTypes Rhs where
>   tcUsedBy (SimpleRhs _ expr decls) =
>     nub (tcUsedBy expr ++ tcUsedBy decls)
>   tcUsedBy (GuardedRhs cond decls)  =
>     nub (tcUsedBy cond ++ tcUsedBy decls)
>   tvUsedBy (SimpleRhs _ expr decls) =
>     nub (tvUsedBy expr ++ tvUsedBy decls)
>   tvUsedBy (GuardedRhs cond decls)  =
>     nub (tvUsedBy cond ++ tvUsedBy decls)

> instance UsesTypes CondExpr where
>   tcUsedBy (CondExpr _ expr1 expr2) =
>     nub (tcUsedBy expr1 ++ tcUsedBy expr2)
>   tvUsedBy (CondExpr _ expr1 expr2) =
>     nub (tvUsedBy expr1 ++ tvUsedBy expr2)

> instance UsesTypes Expression where
>   tcUsedBy (Literal _)     = []
>   tcUsedBy (Variable _)    = []
>   tcUsedBy (Constructor _) = []
>   tcUsedBy (Paren expr) = tcUsedBy expr
>   tcUsedBy (Typed expr tyexpr) =
>     nub (tcUsedBy expr ++ tcUsedBy tyexpr)
>   tcUsedBy (Tuple exprs) = tcUsedBy exprs
>   tcUsedBy (List exprs) = tcUsedBy exprs
>   tcUsedBy (ListCompr expr stats) =
>     nub (tcUsedBy expr ++ tcUsedBy stats)
>   tcUsedBy (EnumFrom expr) = tcUsedBy expr
>   tcUsedBy (EnumFromThen expr1 expr2) =
>     nub (tcUsedBy expr1 ++ tcUsedBy expr2)
>   tcUsedBy (EnumFromTo expr1 expr2) =
>     nub (tcUsedBy expr1 ++ tcUsedBy expr2)
>   tcUsedBy (EnumFromThenTo expr1 expr2 expr3) =
>     nub (tcUsedBy expr1 ++ tcUsedBy expr2 ++ tcUsedBy expr3)
>   tcUsedBy (UnaryMinus _ expr) = tcUsedBy expr
>   tcUsedBy (Apply expr1 expr2) =
>     nub (tcUsedBy expr1 ++ tcUsedBy expr2)
>   tcUsedBy (InfixApply expr1 _ expr2) =
>     nub (tcUsedBy expr1 ++ tcUsedBy expr2)
>   tcUsedBy (LeftSection expr _) = tcUsedBy expr
>   tcUsedBy (RightSection _ expr) = tcUsedBy expr
>   tcUsedBy (Lambda _ expr) = tcUsedBy expr
>   tcUsedBy (Let decls expr) =
>     nub (tcUsedBy decls ++ tcUsedBy decls)
>   tcUsedBy (Do stats expr) = nub (tcUsedBy stats ++ tcUsedBy expr)
>   tcUsedBy (IfThenElse expr1 expr2 expr3) =
>     nub (tcUsedBy expr1 ++ tcUsedBy expr2 ++ tcUsedBy expr3)
>   tcUsedBy (Case expr alt) =
>     nub (tcUsedBy expr ++ tcUsedBy alt)
>     
>   tvUsedBy (Literal _)     = []
>   tvUsedBy (Variable _)    = []
>   tvUsedBy (Constructor _) = []
>   tvUsedBy (Paren expr) = tvUsedBy expr
>   tvUsedBy (Typed expr tyexpr) =
>     nub (tvUsedBy expr ++ tvUsedBy tyexpr)
>   tvUsedBy (Tuple exprs) = tvUsedBy exprs
>   tvUsedBy (List exprs) = tvUsedBy exprs
>   tvUsedBy (ListCompr expr stats) =
>     nub (tvUsedBy expr ++ tvUsedBy stats)
>   tvUsedBy (EnumFrom expr) = tvUsedBy expr
>   tvUsedBy (EnumFromThen expr1 expr2) =
>     nub (tvUsedBy expr1 ++ tvUsedBy expr2)
>   tvUsedBy (EnumFromTo expr1 expr2) =
>     nub (tvUsedBy expr1 ++ tvUsedBy expr2)
>   tvUsedBy (EnumFromThenTo expr1 expr2 expr3) =
>     nub (tvUsedBy expr1 ++ tvUsedBy expr2 ++ tvUsedBy expr3)
>   tvUsedBy (UnaryMinus _ expr) = tvUsedBy expr
>   tvUsedBy (Apply expr1 expr2) =
>     nub (tvUsedBy expr1 ++ tvUsedBy expr2)
>   tvUsedBy (InfixApply expr1 _ expr2) =
>     nub (tvUsedBy expr1 ++ tvUsedBy expr2)
>   tvUsedBy (LeftSection expr _) = tvUsedBy expr
>   tvUsedBy (RightSection _ expr) = tvUsedBy expr
>   tvUsedBy (Lambda _ expr) = tvUsedBy expr
>   tvUsedBy (Let decls expr) =
>     nub (tvUsedBy decls ++ tvUsedBy decls)
>   tvUsedBy (Do stats expr) = nub (tvUsedBy stats ++ tvUsedBy expr)
>   tvUsedBy (IfThenElse expr1 expr2 expr3) =
>     nub (tvUsedBy expr1 ++ tvUsedBy expr2 ++ tvUsedBy expr3)
>   tvUsedBy (Case expr alt) =
>     nub (tvUsedBy expr ++ tvUsedBy alt)

> instance UsesTypes Alt where
>   tcUsedBy (Alt _ _ rhs) = tcUsedBy rhs
>   tvUsedBy (Alt _ _ rhs) = tvUsedBy rhs

> instance UsesTypes Statement where
>   tcUsedBy (StmtExpr expr)   = tcUsedBy expr
>   tcUsedBy (StmtDecl decls)  = tcUsedBy decls
>   tcUsedBy (StmtBind _ expr) = tcUsedBy expr
>   tvUsedBy (StmtExpr expr)   = tvUsedBy expr
>   tvUsedBy (StmtDecl decls)  = tvUsedBy decls
>   tvUsedBy (StmtBind _ expr) = tvUsedBy expr

> instance UsesTypes TypeExpr where
>   tcUsedBy (TypeExprConstructor id)      = [id]
>   tcUsedBy (TypeExprVariable id)         = []
>   tcUsedBy (TypeExprApplication ty1 ty2) =
>     (tcUsedBy ty1) ++ (tcUsedBy ty2)
>
>   tvUsedBy (TypeExprConstructor _)       = []
>   tvUsedBy (TypeExprVariable id)         = [id]
>   tvUsedBy (TypeExprApplication ty1 ty2) =
>     (tvUsedBy ty1) ++ (tvUsedBy ty2)

> instance UsesTypes TypeExprWithContext where
>   tcUsedBy (TypeExprWithContext ctx tyexpr) = tcUsedBy ctx ++ tcUsedBy tyexpr
>   tvUsedBy (TypeExprWithContext ctx tyexpr) = tvUsedBy ctx ++ tvUsedBy tyexpr

> instance UsesTypes TypeExprContext where
>   tcUsedBy (TypeExprContext classConstrs) = tcUsedBy classConstrs
>   tvUsedBy (TypeExprContext classConstrs) = tvUsedBy classConstrs

> instance UsesTypes TypeExprClassConstraint where
>   tcUsedBy (TypeExprClassConstraint typeClass ty) = (typeClass : tcUsedBy ty)
>   tvUsedBy (TypeExprClassConstraint _         ty) = tvUsedBy ty

\end{lstlisting}
