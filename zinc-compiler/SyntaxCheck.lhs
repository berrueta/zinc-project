% $Id: SyntaxCheck.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2004, Wolfgang Lux
% Copyright (c) 2003-2005, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{SyntaxCheck.lhs}
\codesection{Syntax Checks}
After the type declarations have been checked, the compiler performs a
syntax check on the remaining declarations. This check disambiguates
nullary data constructors and variables which -- in contrast to
Haskell -- is not possible on purely syntactic criteria. In addition,
this pass checks for undefined as well as ambiguous variables and
constructors. In order to allow lifting of local definitions in
later phases, all local variables are renamed by adding a unique
key.\footnote{Actually, all variables defined in the same scope share
the same key.} Finally, all (adjacent) equations of a function are
merged into a single definition.
\begin{lstlisting}

> module SyntaxCheck(syntaxCheck,syntaxCheckGoal) where
> import Base
> import Ident
> import ValueEnv
> import Position
> import Types
> import CurrySyntax
> import Env
> import NestEnv
> import RenameEnv
> import List
> import Maybe
> import Monad
> import Utils
> import RenameState
> import Error(errorAt,internalError)
> import Expr
> import TypeClassBinding(declsOfTypeClassDecl)

\end{lstlisting}
The syntax checking proceeds as follows. First, the compiler extracts
information about all imported values and data constructors from the
imported (type) environments. Next, the data constructors defined in
the current module are entered into this environment. Finally, all
declarations are checked within the resulting environment. In
addition, this process will also rename the local variables.
\begin{lstlisting}

> syntaxCheck :: ModuleIdent -> ValueEnv -> [Decl] -> ([Decl],Int)
> syntaxCheck m tyEnv ds =
>   case linear (concatMap constrs tds) of
>     Linear -> (tds ++ vds',key)
>     NonLinear (PIdent p c) -> errorAt p (duplicateData c)
>   where (tds,vds)  = partition (\d -> isTypeDecl d || isTypeClassDecl d) ds
>         renameEnv  = globalEnv (fmap renameInfo tyEnv)
>         (vds',key) = runRenameState (checkModule m renameEnv methods vds)
>                                     globalKey
>         methods    = concat $ map vars $ filter isTypeSig $
>                        concat $ map declsOfTypeClassDecl $
>                          filter isTypeClassDecl tds

> syntaxCheckGoal :: ValueEnv -> Goal -> (Goal,Int)
> syntaxCheckGoal tyEnv g =
>   runRenameState (checkGoal renameEnv g) globalKey
>   where renameEnv = globalEnv (fmap renameInfo tyEnv)

\end{lstlisting}
When a module is checked, the global declaration group is checked. The
resulting renaming environment can be discarded. The same is true for
a goal. Note that all declarations in the goal must be considered as
local declarations.
\begin{lstlisting}

> checkModule :: ModuleIdent -> RenameEnv -> [PIdent] -> [Decl] ->
>                RenameState [Decl]
> checkModule m env methods ds = liftM snd (checkTopDecls m env methods ds)

> checkTopDecls :: ModuleIdent -> RenameEnv -> [PIdent] -> [Decl] ->
>                  RenameState (RenameEnv,[Decl])
> checkTopDecls m env methods ds =
>   checkDeclGroup (bindFunc m) globalKey env methods ds

> checkGoal :: RenameEnv -> Goal -> RenameState Goal
> checkGoal env (Goal p e ds) =
>   do
>     (env',ds') <- checkLocalDecls env ds
>     e' <- checkExpr p env' e
>     return (Goal p e' ds')

\end{lstlisting}
Each declaration group opens a new scope and uses a distinct key
for renaming the variables in this scope. In a declaration group,
first the left hand sides of all declarations are checked, next the
compiler checks that there is a definition for every type signature
and evaluation annotation in this group. Finally, the right hand sides
are checked and adjacent equations for the same function are merged
into a single definition.

The function \texttt{checkDeclLhs} also handles the case where a
pattern declaration is recognized as a function declaration by the
parser. This happens, e.g., for the declaration \verb|where Just x = y|
because the parser cannot distinguish nullary constructors and
functions. Note that pattern declarations are not allowed on the
top-level.
\begin{lstlisting}

> checkLocalDecls :: RenameEnv -> [Decl] -> RenameState (RenameEnv,[Decl])
> checkLocalDecls env ds =
>   newId >>= \k -> checkDeclGroup bindVar k (nestEnv env) [] ds

> checkDeclGroup :: (PIdent -> RenameEnv -> RenameEnv) -> Int -> RenameEnv ->
>                   [PIdent] -> [Decl] -> RenameState (RenameEnv,[Decl])
> checkDeclGroup bindVar k env methods ds =
>   mapM (checkDeclLhs k env) ds >>=
>   checkDecls bindVar env methods . joinEquations

> checkDeclLhs :: Int -> RenameEnv -> Decl -> RenameState Decl
> checkDeclLhs k _ (InfixDecl p fix pr ops) =
>   return (InfixDecl p fix pr (map (flip renameIdent k) ops))
> checkDeclLhs k env (TypeSig p vs ty) =
>   return (TypeSig p (map (checkVar "type signature" k p env) vs) ty)
> checkDeclLhs k env (EvalAnnot p fs ev) =
>   return (EvalAnnot p (map (checkVar "evaluation annotation" k p env) fs) ev)
> checkDeclLhs k env (FunctionDecl p _ eqs) = checkEquationLhs k env p eqs
> checkDeclLhs k env (ExternalDecl p cc ie f ty) =
>   return (ExternalDecl p cc ie (checkVar "external declaration" k p env f) ty)
> checkDeclLhs k env (PatternDecl p t rhs) =
>   do
>     t' <- checkConstrTerm k p env t
>     return (PatternDecl p t' rhs)
> checkDeclLhs k env (ExtraVariables p vs) =
>   return (ExtraVariables p
>             (map (checkVar "free variables declaration" k p env) vs))
> checkDeclLhs _ _ d = return d

> checkEquationLhs :: Int -> RenameEnv -> Position -> [Equation]
>                  -> RenameState Decl
> checkEquationLhs k env p [Equation p' lhs rhs] =
>   either (return . funDecl) (checkDeclLhs k env . patDecl)
>          (checkEqLhs k env p' lhs)
>   where funDecl (f,lhs) = FunctionDecl p f [Equation p' lhs rhs]
>         patDecl t
>           | k == globalKey = errorAt p noToplevelPattern
>           | otherwise = PatternDecl p' t rhs
> checkEquationLhs _ _ _ x =
>   internalError ("SyntaxCheck.checkEquationLhs: " ++ show x)

> checkEqLhs :: Int -> RenameEnv -> Position -> Lhs
>            -> Either (Ident,Lhs) ConstrTerm
> checkEqLhs k env _ (FunLhs f ts)
>   | isDataConstr f env = Right (ConstructorPattern (qualify f) ts)
>   | otherwise = Left (f',FunLhs f' ts)
>   where f' = renameIdent f k
> checkEqLhs k env _ (OpLhs t1 op t2)
>   | isDataConstr op env = checkOpLhs k env (infixPattern t1 (qualify op)) t2
>   | otherwise = Left (op',OpLhs t1 op' t2)
>   where op' = renameIdent op k
>         infixPattern (InfixPattern t1 op1 t2) op2 t3 =
>           InfixPattern t1 op1 (infixPattern t2 op2 t3)
>         infixPattern t1 op t2 = InfixPattern t1 op t2
> checkEqLhs k env p (ApLhs lhs ts) =
>   case checkEqLhs k env p lhs of
>     Left (f',lhs') -> Left (f',ApLhs lhs' ts)
>     Right _ -> errorAt p $ nonVariable "curried definition" f
>   where (f,_) = flatLhs lhs

> checkOpLhs :: Int -> RenameEnv -> (ConstrTerm -> ConstrTerm) -> ConstrTerm
>            -> Either (Ident,Lhs) ConstrTerm
> checkOpLhs k env f (InfixPattern t1 op t2)
>   | isJust m || isDataConstr op' env =
>       checkOpLhs k env (f . InfixPattern t1 op) t2
>   | otherwise = Left (op'',OpLhs (f t1) op'' t2)
>   where (m,op') = splitQualIdent op
>         op'' = renameIdent op' k
> checkOpLhs _ _ f t = Right (f t)

> checkVar :: String -> Int -> Position -> RenameEnv -> Ident -> Ident
> checkVar what k p env v
>   | isDataConstr v env = errorAt p (nonVariable what v)
>   | otherwise = renameIdent v k

> checkDecls :: (PIdent -> RenameEnv -> RenameEnv) -> RenameEnv -> [PIdent] ->
>               [Decl] -> RenameState (RenameEnv,[Decl])
> checkDecls bindVar env methods ds
>   | checkLinearValues && checkLinearSigs && checkLinearEvalAnnot =
>       liftM ((,) env') (mapM (checkDeclRhs bvs env') ds)
>   where env'  = foldr bindVar env bvs
>         bvs   = concat (map vars (filter isValueDecl ds))
>         sigs  = concat (map vars (filter isTypeSig   ds))
>         evals = concat (map vars (filter isEvalAnnot ds))
>         checkLinearValues =
>            case linear (methods ++ bvs) of
>              Linear -> True
>              NonLinear (PIdent p v) -> errorAt p (duplicateDefinition v)
>         checkLinearSigs =
>            case linear sigs of
>              Linear -> True
>              NonLinear (PIdent p v) -> errorAt p (duplicateTypeSig v)
>         checkLinearEvalAnnot =
>            case linear evals of
>              Linear -> True
>              NonLinear (PIdent p v) -> errorAt p (duplicateEvalAnnot v)


> checkDeclRhs :: [PIdent] -> RenameEnv -> Decl -> RenameState Decl
> checkDeclRhs bvs _ (TypeSig p vs ty) =
>   return (TypeSig p (map (checkLocalVar bvs p) vs) ty)
> checkDeclRhs bvs _ (EvalAnnot p vs ev) =
>   return (EvalAnnot p (map (checkLocalVar bvs p) vs) ev)
> checkDeclRhs _ env (FunctionDecl p f eqs) =
>   liftM (FunctionDecl p f) (mapM (checkEquation env) eqs)
> checkDeclRhs _ env (PatternDecl p t rhs) =
>   liftM (PatternDecl p t) (checkRhs env rhs)
> checkDeclRhs _ _ d = return d

> checkLocalVar :: [PIdent] -> Position -> Ident -> Ident
> checkLocalVar bvs p v
>   | PIdent p v `elem` bvs = v
>   | otherwise = errorAt p (noBody v)

> joinEquations :: [Decl] -> [Decl]
> joinEquations [] = []
> joinEquations (FunctionDecl p f eqs : FunctionDecl p' f' [eq] : ds)
>   | f == f' =
>       if arity (head eqs) == arity eq then
>         joinEquations (FunctionDecl p f (eqs ++ [eq]) : ds)
>       else
>         errorAt p' (differentArity f)
>   where arity (Equation _ lhs _) = length $ snd $ flatLhs lhs
> joinEquations (d : ds) = d : joinEquations ds

> checkEquation :: RenameEnv -> Equation -> RenameState Equation
> checkEquation env (Equation p lhs rhs) =
>   do
>     (env',lhs') <- checkLhs p env lhs
>     rhs' <- checkRhs env' rhs
>     return (Equation p lhs' rhs')

> checkLhs :: Position -> RenameEnv -> Lhs -> RenameState (RenameEnv,Lhs)
> checkLhs p env lhs =
>   newId >>= \k ->
>   checkLhsTerm k p env lhs >>=
>   return . checkConstrTerms p (nestEnv env)

> checkLhsTerm :: Int -> Position -> RenameEnv -> Lhs -> RenameState Lhs
> checkLhsTerm k p env (FunLhs f ts) =
>   do
>     ts' <- mapM (checkConstrTerm k p env) ts
>     return (FunLhs f ts')
> checkLhsTerm k p env (OpLhs t1 op t2) =
>   do
>     t1' <- checkConstrTerm k p env t1
>     t2' <- checkConstrTerm k p env t2
>     return (OpLhs t1' op t2')
> checkLhsTerm k p env (ApLhs lhs ts) =
>   do
>     lhs' <- checkLhsTerm k p env lhs
>     ts' <- mapM (checkConstrTerm k p env) ts
>     return (ApLhs lhs' ts')

> checkArgs :: Position -> RenameEnv -> [ConstrTerm]
>           -> RenameState (RenameEnv,[ConstrTerm])
> checkArgs p env ts =
>   newId >>= \k ->
>   mapM (checkConstrTerm k p env) ts >>=
>   return . checkConstrTerms p (nestEnv env)

> checkConstrTerms :: QuantExpr t => Position -> RenameEnv -> t
>                  -> (RenameEnv,t)
> checkConstrTerms p env ts =
>   case linear bvs of
>     Linear -> (foldr (bindVar . PIdent p) env bvs,ts)
>     NonLinear v -> errorAt p (duplicateVariable v)
>   where bvs = bv ts

> checkConstrTerm :: Int -> Position -> RenameEnv -> ConstrTerm
>                 -> RenameState ConstrTerm
> checkConstrTerm _ _ _ (LiteralPattern l) =
>   liftM LiteralPattern (renameLiteral l)
> checkConstrTerm _ _ _ (NegativePattern op l) =
>   liftM (NegativePattern op) (renameLiteral l)
> checkConstrTerm k p env (VariablePattern v)
>   | v == anonId = liftM (VariablePattern . renameIdent anonId) newId
>   | otherwise = checkConstrTerm k p env (ConstructorPattern (qualify v) [])
> checkConstrTerm k p env (ConstructorPattern c ts) =
>   case qualLookupVar c env of
>     [Constr n]
>       | n == n' ->
>           liftM (ConstructorPattern c) (mapM (checkConstrTerm k p env) ts)
>       | otherwise -> errorAt p (wrongArity c n n')
>       where n' = length ts
>     rs
>       | any isConstr rs -> errorAt p (ambiguousData c)
>       | not (isQualified c) && null ts ->
>           return (VariablePattern (renameIdent (unqualify c) k))
>       | otherwise -> errorAt p (undefinedData c)
> checkConstrTerm k p env (InfixPattern t1 op t2) =
>   case qualLookupVar op env of
>     [Constr n]
>       | n == 2 ->
>           do
>             t1' <- checkConstrTerm k p env t1
>             t2' <- checkConstrTerm k p env t2
>             return (InfixPattern t1' op t2')
>       | otherwise -> errorAt p (wrongArity op n 2)
>     rs
>       | any isConstr rs -> errorAt p (ambiguousData op)
>       | otherwise -> errorAt p (undefinedData op)
> checkConstrTerm k p env (ParenPattern t) =
>   liftM ParenPattern (checkConstrTerm k p env t)
> checkConstrTerm k p env (TuplePattern ts) =
>   liftM TuplePattern (mapM (checkConstrTerm k p env) ts)
> checkConstrTerm k p env (ListPattern ts) =
>   liftM ListPattern (mapM (checkConstrTerm k p env) ts)
> checkConstrTerm k p env (AsPattern v t) =
>   liftM (AsPattern (checkVar "@ pattern" k p env v))
>         (checkConstrTerm k p env t)
> checkConstrTerm k p env (LazyPattern t) =
>   liftM LazyPattern (checkConstrTerm k p env t)

> checkRhs :: RenameEnv -> Rhs -> RenameState Rhs
> checkRhs env (SimpleRhs p e ds) =
>   do
>     (env',ds') <- checkLocalDecls env ds
>     e' <- checkExpr p env' e
>     return (SimpleRhs p e' ds')
> checkRhs env (GuardedRhs es ds) =
>   do
>     (env',ds') <- checkLocalDecls env ds
>     es' <- mapM (checkCondExpr env') es
>     return (GuardedRhs es' ds')

> checkCondExpr :: RenameEnv -> CondExpr -> RenameState CondExpr
> checkCondExpr env (CondExpr p g e) =
>   do
>     g' <- checkExpr p env g
>     e' <- checkExpr p env e
>     return (CondExpr p g' e')

> checkExpr :: Position -> RenameEnv -> Expression -> RenameState Expression
> checkExpr _ _ (Literal l) = liftM Literal (renameLiteral l)
> checkExpr p env (Variable v) =
>   case qualLookupVar v env of
>     [] -> errorAt p (undefinedVariable v)
>     [Constr _] -> return (Constructor v)
>     [GlobalVar] -> return (Variable v)
>     [LocalVar v'] -> return (Variable (qualify v'))
>     rs -> errorAt p (ambiguousIdent rs v)
> checkExpr p env (Constructor c) = checkExpr p env (Variable c)
> checkExpr p env (Paren e) = liftM Paren (checkExpr p env e)
> checkExpr p env (Typed e ty) = liftM (flip Typed ty) (checkExpr p env e)
> checkExpr p env (Tuple es) = liftM Tuple (mapM (checkExpr p env) es)
> checkExpr p env (List es) = liftM List (mapM (checkExpr p env) es)
> checkExpr p env (ListCompr e qs) =
>   do
>     (env',qs') <- mapAccumM (checkStatement p) env qs
>     e' <- checkExpr p env' e
>     return (ListCompr e' qs')
> checkExpr p env (EnumFrom e) = liftM EnumFrom (checkExpr p env e)
> checkExpr p env (EnumFromThen e1 e2) =
>   do
>     e1' <- checkExpr p env e1
>     e2' <- checkExpr p env e2
>     return (EnumFromThen e1' e2')
> checkExpr p env (EnumFromTo e1 e2) =
>   do
>     e1' <- checkExpr p env e1
>     e2' <- checkExpr p env e2
>     return (EnumFromTo e1' e2')
> checkExpr p env (EnumFromThenTo e1 e2 e3) =
>   do
>     e1' <- checkExpr p env e1
>     e2' <- checkExpr p env e2
>     e3' <- checkExpr p env e3
>     return (EnumFromThenTo e1' e2' e3')
> checkExpr p env (UnaryMinus op e) = liftM (UnaryMinus op) (checkExpr p env e)
> checkExpr p env (Apply e1 e2) =
>   do
>     e1' <- checkExpr p env e1
>     e2' <- checkExpr p env e2
>     return (Apply e1' e2')
> checkExpr p env (InfixApply e1 op e2) =
>   do
>     e1' <- checkExpr p env e1
>     e2' <- checkExpr p env e2
>     return (InfixApply e1' (checkOp p env op) e2')
> checkExpr p env (LeftSection e op) =
>   liftM (flip LeftSection (checkOp p env op)) (checkExpr p env e)
> checkExpr p env (RightSection op e) =
>   liftM (RightSection (checkOp p env op)) (checkExpr p env e)
> checkExpr p env (Lambda ts e) =
>   do
>     (env',ts') <- checkArgs p env ts
>     e' <- checkExpr p env' e
>     return (Lambda ts' e')
> checkExpr p env (Let ds e) =
>   do
>     (env',ds') <- checkLocalDecls env ds
>     e' <- checkExpr p env' e
>     return (Let ds' e')
> checkExpr p env (Do sts e) =
>   do
>     (env',sts') <- mapAccumM (checkStatement p) env sts
>     e' <- checkExpr p env' e
>     return (Do sts' e')
> checkExpr p env (IfThenElse e1 e2 e3) =
>   do
>     e1' <- checkExpr p env e1
>     e2' <- checkExpr p env e2
>     e3' <- checkExpr p env e3
>     return (IfThenElse e1' e2' e3')
> checkExpr p env (Case e alts) =
>   do
>     e' <- checkExpr p env e
>     alts' <- mapM (checkAlt env) alts
>     return (Case e' alts')

> checkStatement :: Position -> RenameEnv -> Statement
>                -> RenameState (RenameEnv,Statement)
> checkStatement p env (StmtExpr e) =
>   do
>     e' <- checkExpr p env e
>     return (env,StmtExpr e')
> checkStatement p env (StmtBind t e) =
>   do
>     e' <- checkExpr p env e
>     (env',[t']) <- checkArgs p env [t]
>     return (env',StmtBind t' e')
> checkStatement p env (StmtDecl ds) =
>   do
>     (env',ds') <- checkLocalDecls env ds
>     return (env',StmtDecl ds')

> checkAlt :: RenameEnv -> Alt -> RenameState Alt
> checkAlt env (Alt p t rhs) =
>   do
>     (env',[t']) <- checkArgs p env [t]
>     rhs' <- checkRhs env' rhs
>     return (Alt p t' rhs')

> checkOp :: Position -> RenameEnv -> InfixOp -> InfixOp
> checkOp p env op =
>   case qualLookupVar v env of
>     [] -> errorAt p (undefinedVariable v)
>     [Constr _] -> InfixConstr v
>     [GlobalVar] -> InfixOp v
>     [LocalVar v'] -> InfixOp (qualify v')
>     rs -> errorAt p (ambiguousIdent rs v)
>   where v = opName op

\end{lstlisting}
Auxiliary definitions.
\begin{lstlisting}

> constrs :: Decl -> [PIdent]
> constrs (DataDecl _ _ _ cs _) = map constr cs
>   where constr (ConstrDecl p _ c _) = PIdent p c
>         constr (ConOpDecl p _ _ op _) = PIdent p op
> constrs (NewtypeDecl _ _ _ (NewConstrDecl p _ c _) _) = [PIdent p c]
> constrs _ = []

> vars :: Decl -> [PIdent]
> vars (TypeSig        p fs _   ) = map (PIdent p) fs
> vars (EvalAnnot      p fs _   ) = map (PIdent p) fs
> vars (FunctionDecl   p f _    ) = [PIdent p f]
> vars (ExternalDecl   p _ _ f _) = [PIdent p f]
> vars (PatternDecl    p t _    ) = map (PIdent p) (bv t)
> vars (ExtraVariables p vs     ) = map (PIdent p) vs
> vars _ = []

> renameLiteral :: Literal -> RenameState Literal
> renameLiteral (Int v i) = liftM (flip Int i . renameIdent v) newId
> renameLiteral l = return l

\end{lstlisting}
Due to the lack of a capitalization convention in Curry, it is
possible that an identifier may ambiguously refer to a data
constructor and a function provided that both are imported from some
other module. When checking whether an identifier denotes a
constructor there are two options with regard to ambiguous
identifiers:
\begin{enumerate}
\item Handle the identifier as a data constructor if at least one of
  the imported names is a data constructor.
\item Handle the identifier as a data constructor only if all imported
  entities are data constructors.
\end{enumerate}
We choose the first possibility here because in the second case a
redefinition of a constructor can magically become possible if a
function with the same name is imported. It seems better to warn
the user about the fact that the identifier is ambiguous.
\begin{lstlisting}

> isDataConstr :: Ident -> RenameEnv -> Bool
> isDataConstr v = any isConstr . lookupVar v . globalEnv . toplevelEnv

> isConstr :: RenameInfo -> Bool
> isConstr (Constr _) = True
> isConstr GlobalVar = False
> isConstr (LocalVar _) = False

\end{lstlisting}
Error messages.
\begin{lstlisting}

> undefinedVariable :: QualIdent -> String
> undefinedVariable v = qualName v ++ " is undefined"

> undefinedData :: QualIdent -> String
> undefinedData c = "Undefined data constructor " ++ qualName c

> ambiguousIdent :: [RenameInfo] -> QualIdent -> String
> ambiguousIdent rs
>   | any isConstr rs = ambiguousData
>   | otherwise = ambiguousVariable

> ambiguousVariable :: QualIdent -> String
> ambiguousVariable v = "Ambiguous variable " ++ qualName v

> ambiguousData :: QualIdent -> String
> ambiguousData c = "Ambiguous data constructor " ++ qualName c

> duplicateDefinition :: Ident -> String
> duplicateDefinition v = "More than one definition for " ++ name v

> duplicateVariable :: Ident -> String
> duplicateVariable v = name v ++ " occurs more than once in pattern"

> duplicateData :: Ident -> String
> duplicateData c = "More than one definition for data constructor " ++ name c

> duplicateTypeSig :: Ident -> String
> duplicateTypeSig v = "More than one type signature for " ++ name v

> duplicateEvalAnnot :: Ident -> String
> duplicateEvalAnnot v = "More than one eval annotation for " ++ name v

> nonVariable :: String -> Ident -> String
> nonVariable what c =
>   "Data constructor " ++ name c ++ " in left hand side of " ++ what

> noBody :: Ident -> String
> noBody v = "No body for " ++ name v

> noToplevelPattern :: String
> noToplevelPattern = "Pattern declaration not allowed at top-level"

> differentArity :: Ident -> String
> differentArity f = "Equations for " ++ name f ++ " have different arities"

> wrongArity :: QualIdent -> Int -> Int -> String
> wrongArity c arity argc =
>   "Data constructor " ++ qualName c ++ " expects " ++ arguments arity ++
>   " but is applied to " ++ show argc
>   where arguments 0 = "no arguments"
>         arguments 1 = "1 argument"
>         arguments n = show n ++ " arguments"

> noExpressionStatement :: String
> noExpressionStatement =
>   "Last statement in a do expression must be an expression"

\end{lstlisting}
