% $Id: TypeInstGen.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeInstGen.lhs}
\codesection{Instantiation and Generalization}
This module contains the functions for instantiation and generalization
of type expressions from and to type schemes. Both processes are
described by two inference rules in the Hindley/Milner type system.
\begin{lstlisting}

> module TypeInstGen(freshTypeVar,freshTypeVarWithContext,
>                    freshConstrainedWithContext,
>                    inst,instExist,
>                    skol,
>                    gen) where
> import TypeInferenceMonad
> import Types
> import Combined
> import Set
> import Monad
> import TypeSubst
> import List(nub)
> import Utils(foldr2)
> import TypeExpansion(expandAliasType,expandAliasTypeWithContext)

\end{lstlisting}
Generation of fresh type variables. We use negative offsets for them.
\begin{lstlisting}

> fresh :: (Int -> a) -> TcState a
> fresh f = liftM f (liftSt (liftSt (updateSt (1 +))))

> freshVar :: (Int -> a) -> TcState a
> freshVar f = fresh (\n -> f (- n - 1))

> freshTypeVar :: TcState Type
> freshTypeVar = freshVar TypeVariable

> freshTypeVarWithContext :: TcState TypeWithContext
> freshTypeVarWithContext =
>   liftM (TypeWithContext emptyTypeContext) freshTypeVar

> freshConstrainedWithContext :: [Type] -> TcState TypeWithContext
> freshConstrainedWithContext tys =
>   freshVar (TypeWithContext emptyTypeContext . TypeConstrained tys)

> freshSkolem :: TcState Type
> freshSkolem = fresh TypeSkolem

\end{lstlisting}
\codesubsection{Instantiation of type schemes}
Instantiation substitutes generic type variables by
fresh new type variables.

\ToDo{The functions \lstinline|instExist| and \lstinline|skol| may
change if contexts are allowed in existentially qualified type schemes.}
\begin{lstlisting}

> inst :: TypeScheme -> TcState TypeWithContext
> inst (ForAll n tyWC) =
>   do
>     tys <- replicateM n freshTypeVar
>     let tyWC' = expandAliasTypeWithContext tys tyWC
>     return tyWC'

> instExist :: ExistTypeScheme -> TcState TypeWithContext
> instExist (ForAllExist n n' ty) =
>   do
>     tys <- replicateM (n + n') freshTypeVar
>     return (TypeWithContext emptyTypeContext (expandAliasType tys ty))

> skol :: ExistTypeScheme -> TcState TypeWithContext
> skol (ForAllExist n n' ty) =
>   do
>     tys  <- replicateM n  freshTypeVar
>     tys' <- replicateM n' freshSkolem
>     return (TypeWithContext emptyTypeContext (expandAliasType (tys ++ tys') ty))

\end{lstlisting}
Auxiliary function.
\begin{lstlisting}

> replicateM :: Monad m => Int -> m a -> m [a]
> replicateM n = sequence . replicate n

\end{lstlisting}
\codesubsection{Generalization}
Generalization of types to type schemes. Type variables in the set passed
as the first argument are not generalised.
\begin{lstlisting}

> gen :: Set Int -> TypeWithContext -> TypeScheme
> gen gvs tyWC =
>   ForAll (length tvs) (subst (foldr2 bindSubst idSubst tvs tvs') tyWC)
>   where tvs  = [tv | tv <- nub (typeVars tyWC), tv `notElemSet` gvs]
>         tvs' = map TypeVariable [0..]

\end{lstlisting}
