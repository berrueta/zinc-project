% $Id: ModuleEnv.lhs 970 2004-08-08 12:05:32Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2003, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{ModuleEnv.lhs}
\codesection{Interfaces}
The compiler maintains a global environment holding all (directly or
indirectly) imported interfaces.
\begin{lstlisting}

> module ModuleEnv(ModuleEnv,bindModule,lookupModule) where
> import Env
> import Ident
> import CurrySyntax

> type ModuleEnv = Env ModuleIdent [IDecl]

> bindModule :: Interface -> ModuleEnv -> ModuleEnv
> bindModule (Interface m ds) = bindEnv m ds

> lookupModule :: ModuleIdent -> ModuleEnv -> Maybe [IDecl]
> lookupModule = lookupEnv

\end{lstlisting}
