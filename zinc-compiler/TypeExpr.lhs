% $Id: TypeExpr.lhs 1300 2004-09-03 18:08:47Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeExpr.lhs}
\codesection{Sintactical representation of types}
Arrow type expression recursivity is build to the right.
\begin{lstlisting}

> module TypeExpr where
> import Ident

> data TypeExpr = TypeExprConstructor QualIdent
>               | TypeExprVariable    Ident
>               | TypeExprApplication TypeExpr TypeExpr
>               deriving (Eq,Show)

\end{lstlisting}
Type expression primitive constructors:
\begin{lstlisting}

> typeExprTupleConstructor :: Int -> TypeExpr
> typeExprTupleConstructor n = typeExprPrimitiveConstructor (tupleId n)

> typeExprListConstructor :: TypeExpr
> typeExprListConstructor = typeExprPrimitiveConstructor listId

> typeExprUnitConstructor :: TypeExpr
> typeExprUnitConstructor = typeExprPrimitiveConstructor unitId

> typeExprArrowConstructor :: TypeExpr
> typeExprArrowConstructor = typeExprPrimitiveConstructor arrowId

\end{lstlisting}
Auxiliary function to build primitive type expression constructors:
\begin{lstlisting}

> typeExprPrimitiveConstructor :: Ident -> TypeExpr
> typeExprPrimitiveConstructor id =
>   TypeExprConstructor (qualifyWith internalTypeMIdent id)

\end{lstlisting}
Helper functions for primitive constructors:
\begin{lstlisting}

> typeExprApplyTupleConstructor :: [TypeExpr] -> TypeExpr
> typeExprApplyTupleConstructor tys
>   | n == 0    = typeExprUnitConstructor
>   | n == 1    = (head tys)
>   | otherwise = typeExprApply (typeExprTupleConstructor n) tys
>   where n = length tys

> typeExprApplyListConstructor :: TypeExpr -> TypeExpr
> typeExprApplyListConstructor ty = typeExprApply typeExprListConstructor [ty]

> typeExprApplyArrowConstructor :: TypeExpr -> TypeExpr -> TypeExpr
> typeExprApplyArrowConstructor ty1 ty2 =
>   typeExprApply typeExprArrowConstructor [ty1,ty2]

\end{lstlisting}
Helper funtion for user-defined constructors:
\begin{lstlisting}

> typeExprApplyConstructor :: QualIdent -> [TypeExpr] -> TypeExpr
> typeExprApplyConstructor qid = typeExprApply (TypeExprConstructor qid)
                                                                                
\end{lstlisting}
Application of type expressions:
\begin{lstlisting}

> typeExprApply :: TypeExpr -> [TypeExpr] -> TypeExpr
> typeExprApply = foldl TypeExprApplication

> leftmostTypeExpr :: TypeExpr -> TypeExpr
> leftmostTypeExpr t@(TypeExprConstructor _    ) = t
> leftmostTypeExpr t@(TypeExprVariable    _    ) = t
> leftmostTypeExpr   (TypeExprApplication ty1 _) = leftmostTypeExpr ty1

> typeExprArguments :: TypeExpr -> [TypeExpr]
> typeExprArguments (TypeExprConstructor _      ) = []
> typeExprArguments (TypeExprVariable    _      ) = []
> typeExprArguments (TypeExprApplication ty1 ty2) =
>   (typeExprArguments ty1) ++ [ty2]

\end{lstlisting}
Some predicates checking type expresions
\begin{lstlisting}

> isTupleTypeExpr,isUnitTypeExpr :: TypeExpr -> Bool
> isListTypeExpr,isArrowTypeExpr :: TypeExpr -> Bool
> isTupleTypeExpr tyexpr = case leftmostTypeExpr tyexpr of
>   (TypeExprConstructor qid) -> isQTupleId qid
>   otherwise                 -> False
> isUnitTypeExpr tyexpr = case leftmostTypeExpr tyexpr of
>   (TypeExprConstructor qid) -> isQUnitId qid
>   otherwise                 -> False
> isListTypeExpr tyexpr = case leftmostTypeExpr tyexpr of
>   (TypeExprConstructor qid) -> isQListId qid
>   otherwise                 -> False
> isArrowTypeExpr tyexpr = case leftmostTypeExpr tyexpr of
>   (TypeExprConstructor qid) -> isQArrowId qid &&
>                                (length (typeExprArguments tyexpr) == 2)
>   otherwise                 -> False

> isTypeExprVariable,isTypeExprConstructor :: TypeExpr -> Bool
> isTypeExprApplication :: TypeExpr -> Bool
> isTypeExprVariable (TypeExprVariable _) = True
> isTypeExprVariable _                    = False
> isTypeExprConstructor (TypeExprConstructor _) = True
> isTypeExprConstructor _                       = False
> isTypeExprApplication (TypeExprApplication ty1 ty2) = True
> isTypeExprApplication _                             = False

> isInternalTypeExprConstructor :: TypeExpr -> Bool
> isInternalTypeExprConstructor tyexpr =
>   isTupleTypeExpr tyexpr || isUnitTypeExpr tyexpr ||
>   isListTypeExpr tyexpr || isArrowTypeExpr tyexpr

\end{lstlisting}
A type context is a set of class constraints. A class constraints
establishes a relation between a type class and a type variable.
\begin{lstlisting}

> data TypeExprContext = TypeExprContext [TypeExprClassConstraint]
>                      deriving (Eq,Show)

> data TypeExprClassConstraint = TypeExprClassConstraint QualIdent TypeExpr
>                              deriving(Eq,Show)

> emptyTypeExprContext :: TypeExprContext
> emptyTypeExprContext = TypeExprContext []

> composeTypeExprContext :: TypeExprContext -> TypeExprContext -> TypeExprContext
> composeTypeExprContext (TypeExprContext classPreds1) (TypeExprContext classPreds2) =
>   TypeExprContext (classPreds1 ++ classPreds2)

> appendTypeExprClassConstraint :: QualIdent -> TypeExpr ->
>                                  TypeExprContext -> TypeExprContext
> appendTypeExprClassConstraint tc tyexpr (TypeExprContext tccs) =
>   TypeExprContext (tccs ++ [TypeExprClassConstraint tc tyexpr])

> typeExprClassConstraints :: TypeExprContext -> [TypeExprClassConstraint]
> typeExprClassConstraints (TypeExprContext classConstrs) = classConstrs

\end{lstlisting}
Qualified type expressions (a type expression with a context) are
typed as TypeExprWithContext in order to avoid ambiguities with
QualTypeExpr.
\begin{lstlisting}

> data TypeExprWithContext = TypeExprWithContext TypeExprContext TypeExpr
>    deriving (Eq,Show)

> removeTypeExprContext :: TypeExprWithContext -> TypeExpr
> removeTypeExprContext (TypeExprWithContext _ tyexpr) = tyexpr

> typeExprContext :: TypeExprWithContext -> TypeExprContext
> typeExprContext (TypeExprWithContext ctx _) = ctx

> mapTypeExpr :: (TypeExpr -> TypeExpr) -> 
>                TypeExprWithContext -> TypeExprWithContext
> mapTypeExpr f (TypeExprWithContext ctx ty) =
>   TypeExprWithContext ctx (f ty)

> mapTypeExprClassConstraint :: ([TypeExprClassConstraint] ->
>                                [TypeExprClassConstraint]) ->
>                               TypeExprWithContext -> TypeExprWithContext
> mapTypeExprClassConstraint f (TypeExprWithContext (TypeExprContext classPreds) ty) =
>   TypeExprWithContext (TypeExprContext (f classPreds)) ty

> mapTypeExprContext :: (TypeExprContext -> TypeExprContext) ->
>                       TypeExprWithContext -> TypeExprWithContext
> mapTypeExprContext f (TypeExprWithContext ctx ty) =
>   TypeExprWithContext (f ctx) ty

\end{lstlisting}
