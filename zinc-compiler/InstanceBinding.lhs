% $Id: InstanceBinding.lhs 970 2004-08-08 12:05:32Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{InstanceBinding.lhs}
\codesection{Instance Binding}
This module enters the instances into their environment.
\begin{lstlisting}

> module InstanceBinding(bindInstances) where
> import CurrySyntax
> import TypeExpr
> import TypeClassEnv
> import InstanceEnv
> import Ident
> import Position
> import CurryPP
> import Env
> import Error(errorAt,internalError)
> import Types
> import ContextReduction
> import TypeTrans
> import Base
> import List
> import Expr(bv)
> import qualified TypeExprCheck

\end{lstlisting}
This function is the entry point of the module. It filters the
instance declarations, and the binds them. Ordering isn't needed.
\begin{lstlisting}

> bindInstances :: ModuleIdent -> [Decl] ->
>                  TypeClassEnv -> InstanceEnv -> InstanceEnv
> bindInstances m ds typeClassEnv instEnv
>   | all (checkInstanceDecl typeClassEnv instEnv') instDecls = instEnv'
>   where instDecls = filter isInstanceDecl ds
>         instEnv'  = foldl (flip $ bindDecl m typeClassEnv) instEnv instDecls

\end{lstlisting}
It is not required to check that the type constructor exists,
because it has been checked before.
But we must check there isn't any repeated instance declaration
for the same pair (type class, type constructor).
\begin{lstlisting}

> bindDecl :: ModuleIdent -> TypeClassEnv -> Decl -> InstanceEnv -> InstanceEnv
> bindDecl m typeClassEnv (InstanceDecl p ctx typeClass tyexpr decls) instEnv
>   | checkRepeatedInstance p typeClass typeConstructor instEnv &&
>     checkLinearEvalAnnots otherDecls &&
>     checkEvalAnnots typeClass definedMethodsIds otherDecls &&
>     all (checkIsTypeClassMethod typeClass typeClassMethodIds) valueDecls &&
>     all (checkMethodIsDefined p valueDecls) typeClassMethodIds =
>   bindInstance typeClass typeConstructor m instCtx evalEnv valueDecls instEnv
>   where typeConstructor =
>           case leftmostTypeExpr tyexpr of
>             (TypeExprConstructor tc') -> tc'
>             _                         -> errorAt p (typeConstructorExpected tyexpr)
>         instCtx = let TypeExprWithContext ctx' tyexpr' =
>                         fromTypeWithContext
>                         $ mapTypeContext (contextReduction p typeClassEnv instEnv)
>                         $ toTypeWithContext [] (TypeExprWithContext ctx tyexpr)
>                   in  typeExprContextToInstanceContext ctx' tyexpr'
>         (valueDecls,otherDecls) = partition isValueDecl decls
>         definedMethodsIds = nub [m | d <- valueDecls, m <- bv d]
>         evalEnv = methodEvalAnnotDecls otherDecls
>         (TypeClassInfo _ _ _ sigEnv) =
>           case (qualLookupTypeClass typeClass typeClassEnv) of
>             [] -> errorAt p (undefinedTypeClass typeClass)
>             [tci] -> tci
>         typeClassMethodIds = map fst $ envToList sigEnv

\end{lstlisting}
Bind the evaluation annotations.
\begin{lstlisting}

> methodEvalAnnotDecls :: [Decl] -> [(Ident,EvalAnnotation)]
> methodEvalAnnotDecls =
>   concatMap methodEvalAnnotDecl . filter isEvalAnnot
>   where methodEvalAnnotDecl (EvalAnnot _ ids annot) = zip ids (repeat annot)

\end{lstlisting}
Basic instance declaration checking.
\begin{lstlisting}

> checkRepeatedInstance :: Position -> QualIdent -> QualIdent -> InstanceEnv -> Bool
> checkRepeatedInstance p typeClass typeConstructor instEnv =
>   case lookupInstance typeClass typeConstructor instEnv of
>     [] -> True
>     _  -> errorAt p (repeatedInstance typeClass typeConstructor)

> checkIsTypeClassMethod :: QualIdent -> [Ident] -> Decl -> Bool
> checkIsTypeClassMethod typeClass typeClassMethodIds (FunctionDecl p id _)
>   | id `elem` typeClassMethodIds = True
>   | otherwise                    = errorAt p (notMethod id typeClass)

> checkMethodIsDefined :: Position -> [Decl] -> Ident -> Bool
> checkMethodIsDefined p decls method
>   | any (\d -> case d of (FunctionDecl _ id _) -> id == method) decls = True
>   | otherwise = errorAt p (undefinedMethod method)

> checkEvalAnnots :: QualIdent -> [Ident] -> [Decl] -> Bool
> checkEvalAnnots typeClass methodIds = all checkEvalAnnot
>   where checkEvalAnnot (EvalAnnot p fs _)
>           | length undeclaredMethods > 0 =
>               errorAt p (notMethod (head undeclaredMethods) typeClass)
>           | otherwise = True
>           where undeclaredMethods = fs \\ methodIds 
>         checkEvalAnnot (TypeSig p _ _) =
>           errorAt p typeSigNotAllowed


> checkLinearEvalAnnots :: [Decl] -> Bool
> checkLinearEvalAnnots methodDecls =
>   case linear (concat (map vars (filter isEvalAnnot methodDecls))) of
>     Linear -> True
>     NonLinear (PIdent p v) -> errorAt p (repeatedEvalAnnot v)
>   where vars (EvalAnnot p fs _) = map (PIdent p) fs

\end{lstlisting}
After binding all the instances, a test is run for each instance declaration
to check if the type constructor has a instance of all the super classes.
\begin{lstlisting}

> checkInstanceDecl :: TypeClassEnv -> InstanceEnv -> Decl -> Bool
> checkInstanceDecl typeClassEnv instEnv (InstanceDecl p _ typeClass tyexpr decls) =
>   all checkSuperTypeClass superTypeClasses
>   where (TypeExprConstructor typeConstructor) = leftmostTypeExpr tyexpr
>         (TypeClassInfo _ _ superTypeClasses _) =
>           case (qualLookupTypeClass typeClass typeClassEnv) of
>             [] -> errorAt p (undefinedTypeClass typeClass)
>             [tci] -> tci
>         checkSuperTypeClass superTC
>           | hasInstance superTC typeConstructor instEnv = True
>           | otherwise = errorAt p
>                           (requiredInstance superTC typeClass typeConstructor)

\end{lstlisting}
Error messages.
\begin{lstlisting}

> undefinedTypeClass :: QualIdent -> String
> undefinedTypeClass tc = "Undefined type class " ++ qualName tc

> typeConstructorExpected :: TypeExpr -> String
> typeConstructorExpected ty =
>   "A type constructor was expected in type expression " ++ (show $ ppTypeExpr 0 ty)

> repeatedInstance :: QualIdent -> QualIdent -> String
> repeatedInstance typeClass typeConstructor =
>   "Repeated instance declaration of type class " ++ qualName typeClass ++
>   " for type constructor " ++ qualName typeConstructor

> requiredInstance :: QualIdent -> QualIdent -> QualIdent -> String
> requiredInstance superTypeClass typeClass typeConstructor =
>   "Instance of type class " ++ qualName superTypeClass ++
>   " required for instantiation of type class " ++ qualName typeClass ++
>   " for type constructor " ++ qualName typeConstructor

> repeatedEvalAnnot :: Ident -> String
> repeatedEvalAnnot v = "More than one eval annotation for " ++ name v

> notMethod :: Ident -> QualIdent -> String
> notMethod id typeClass =
>   "Function " ++ name id ++ " is not a method of type class " ++
>   qualName typeClass

> typeSigNotAllowed :: String
> typeSigNotAllowed = "Type signatures are not allowed in instance declarations"

> undefinedMethod :: Ident -> String
> undefinedMethod method = "Instance lacks definition for method " ++ name method

\end{lstlisting}
