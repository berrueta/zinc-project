% $Id: DictParEnv.lhs 1286 2004-09-03 12:45:32Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{DictParEnv.lhs}
\codesection{Dictionary parameter environment}

The parameter environment maps class constraints with their dictionary.
\begin{lstlisting}

> module DictParEnv where
> import Env
> import Types
> import CurrySyntax
> import Error(internalError)
> import Ident
> import TypeClassEnv
> import DictUtils
> import TypeSubst

> type ParEnv = Env TypeClassConstraint Expression

\end{lstlisting}
When a search fails, this function returns a null dictionary. This dictionary
will produce a run-time error, but it doesn't matter, as it shouldn't be
evaluated.
\begin{lstlisting}

> lookupParEnv :: TypeClassConstraint -> Int -> ParEnv -> Expression
> lookupParEnv tcc eqNum parEnv = case lookupEnv tcc parEnv of
>   Just dictExpr -> addEqNumExpr dictExpr
>   Nothing       -> nullDictionary
>   where addEqNumExpr :: Expression -> Expression
>         addEqNumExpr (Variable qi)
>           | isDictParIdent qi = (Variable $ addEqNumIdent qi eqNum)
>           | otherwise = Variable qi
>         addEqNumExpr (Apply e1 e2) =
>           (Apply (addEqNumExpr e1) (addEqNumExpr e2))

\end{lstlisting}
The function \lstinline|dictParConstrTerms| returns the list of
constructor terms for the new parameters inserted into the
overloaded function.

\ToDo{Sort parameters.}
\begin{lstlisting}

> dictParConstrTerms :: ParEnv -> Int -> [ConstrTerm]
> dictParConstrTerms parEnv eqNum =
>   map (varToConstrTerm . snd) (envToList parEnv)
>   where varToConstrTerm (Variable qi) =
>           VariablePattern (unqualify $ addEqNumIdent qi eqNum)

\end{lstlisting}
Identifiers of the dictionary parameters.
\begin{lstlisting}

> addEqNumIdent :: QualIdent -> Int -> QualIdent
> addEqNumIdent qid eqNum =
>   qualify $ postfixIdent ("#" ++ show eqNum) (unqualify qid)

> dictParIdent :: Int -> Int -> QualIdent
> dictParIdent n key = qualify $ renameIdent (mkIdent $ dictParPrefix ++ show n) key

> isDictParIdent :: QualIdent -> Bool
> isDictParIdent qid
>   | isQualified qid = False
>   | otherwise = (take (length dictParPrefix) (qualName qid)) == dictParPrefix

> dictParPrefix :: String
> dictParPrefix = "_dictPar_"

\end{lstlisting}
Building the parameter environment from the type context.
\begin{lstlisting}

> buildParEnv :: TypeContext -> Int -> ParEnv
> buildParEnv (TypeContext classPreds) key =
>   foldr (\(classPred,v) -> bindEnv classPred v) emptyEnv $ zip classPreds dictVars
>   where dictVars = [Variable (dictParIdent i key) | i <- [1..]] 

\end{lstlisting}
Transformations of parameter environment.
\begin{lstlisting}

> substParEnv :: TypeSubst -> ParEnv -> ParEnv
> substParEnv sigma parEnv =
>   foldr (uncurry bindEnv) emptyEnv
>     (map (\(tcc,e) -> (subst sigma tcc,e)) (envToList parEnv))

> composeParEnvs :: ParEnv -> ParEnv -> ParEnv
> composeParEnvs parEnv1 parEnv2 = foldr (uncurry bindEnv) parEnv2
>                                        (envToList parEnv1)

\end{lstlisting}
Expansion of the parameter environment. This function adds the
superclasses to the environment.

When two or more type classes share a common superclass, there are many
expressions for the same dictionary. In this case, this function shadows
some of these expressions.
\begin{lstlisting}

> expandParEnv :: TypeClassEnv -> ParEnv -> ParEnv
> expandParEnv typeClassEnv parEnv =
>   foldr (\(tcc,dictVar) -> expandParEnv' typeClassEnv tcc dictVar)
>         parEnv (envToList parEnv)

> expandParEnv' :: TypeClassEnv -> TypeClassConstraint -> Expression ->
>                  ParEnv -> ParEnv
> expandParEnv' typeClassEnv (TypeClassConstraint typeClass ty) dictVar
>               parEnv =
>   foldr bind parEnv superQIds
>   where [TypeClassInfo _ _ superQIds _] =
>           qualLookupTypeClass typeClass typeClassEnv
>         bind :: QualIdent -> ParEnv -> ParEnv
>         bind superTC = expandParEnv' typeClassEnv newTCC newDictVar .
>                        bindEnv newTCC newDictVar
>           where newTCC = TypeClassConstraint superTC ty
>                 newDictVar = Apply (Variable trav) dictVar
>                 trav = dictTraverserQIdent typeClass superTC

\end{lstlisting}
