% $Id: KindInferenceMonad.lhs 1289 2004-09-03 13:56:57Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{KindInferenceMonad.lhs}
\codesection{Kind inference monad}
\begin{lstlisting}

> module KindInferenceMonad where
> import KindEnv
> import Kind
> import KindSubst
> import Combined
> import TypeExpr
> import Ident

\end{lstlisting}
A monad to perform kind inference.
\begin{lstlisting}

> type KiState a = StateT KindEnv (StateT KindSubst (StateT Int Id)) a

> runKiState :: KindEnv -> KiState a -> a
> runKiState kindEnv m =
>   runSt (callSt (callSt m kindEnv) idSubst) 0

> freshKindVar :: KiState Kind
> freshKindVar = do i <- liftSt (liftSt (updateSt (1 +)))
>                   return (KVar (mkIdent ("_ki" ++ show i)))

\end{lstlisting}
