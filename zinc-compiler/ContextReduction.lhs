% $Id: ContextReduction.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{ContextReduction.lhs}
\codesection{Context reduction}
Context reduction is an algorithm to simplify a type contexts. It
does not depend on the type expression being constrained, only on
the type context.
\begin{lstlisting}

> module ContextReduction(contextReduction,contextFilter,
>                         relevantTypeContext) where
> import Ident
> import Types
> import TypeClassEnv
> import InstanceEnv
> import List
> import Error(errorAt,internalError)
> import DictUtils(zipExtLWith)
> import Position

\end{lstlisting}
Context reduction consists of three steps: instance reduction, duplicate
reduction and superclasses reduction.
\begin{lstlisting}

> contextReduction :: Position -> TypeClassEnv -> InstanceEnv ->
>                     TypeContext -> TypeContext
> contextReduction p typeClassEnv instEnv (TypeContext tccs0) =
>   TypeContext tccs3
>   where tccs1 = instanceReduction p instEnv tccs0 
>         tccs2 = nub tccs1
>         tccs3 = superclassesReduction typeClassEnv tccs2

\end{lstlisting}
Instance reduction.
\begin{lstlisting}

> instanceReduction :: Position -> InstanceEnv ->
>                      [TypeClassConstraint] -> [TypeClassConstraint]
> instanceReduction p instEnv = concat . map (instanceReductionTCC p instEnv)

> instanceReductionTCC :: Position -> InstanceEnv -> TypeClassConstraint ->
>                         [TypeClassConstraint]
> instanceReductionTCC p _   tcc@(TypeClassConstraint _ (TypeVariable _)) =
>   [tcc]
> instanceReductionTCC p instEnv (TypeClassConstraint typeClass ty) =
>   expandTCC p instEnv typeClass (leftmostType ty) (typeArguments ty)

> expandTCC :: Position -> InstanceEnv -> QualIdent ->
>              Type -> [Type] -> [TypeClassConstraint]
> expandTCC p instEnv typeClass (TypeConstructor tc) args =
>   instanceReduction p instEnv tcc
>   where instCtx :: [[QualIdent]]
>         instCtx = case lookupInstance typeClass tc instEnv of
>                     [] -> errorAt p (noInstanceFound typeClass tc)
>                     [InstanceInfo _ _ _ ic _ _] -> ic
>         tcc :: [TypeClassConstraint]
>         tcc = zipExtLWith TypeClassConstraint instCtx args
> expandTCC p instEnv typeClass (TypeConstrained tys tv) args =
>   concatMap (\tc -> expandTCC p instEnv typeClass tc args) tys

\end{lstlisting}
Superclasses reduction.
\begin{lstlisting}

> superclassesReduction :: TypeClassEnv ->
>                          [TypeClassConstraint] -> [TypeClassConstraint]
> superclassesReduction typeClassEnv tccs =
>   tccs \\ implied
>   where implied = concatMap (impliedTccs typeClassEnv) tccs

> impliedTccs :: TypeClassEnv -> TypeClassConstraint -> [TypeClassConstraint]
> impliedTccs typeClassEnv (TypeClassConstraint tc tv) =
>   superTccs ++ (concatMap (impliedTccs typeClassEnv) superTccs)
>   where [TypeClassInfo _ _ superQIds _] = qualLookupTypeClass tc typeClassEnv
>         superTccs = map ((flip TypeClassConstraint) tv) superQIds

\end{lstlisting}
Filtering the contexts to include only relevant type variables.
\begin{lstlisting}

> contextFilter :: TypeWithContext -> TypeWithContext
> contextFilter (TypeWithContext (TypeContext classPreds) ty) =
>   TypeWithContext (TypeContext classPreds') ty
>   where classPreds' = filter isRelevant classPreds
>         isRelevant (TypeClassConstraint _ (TypeVariable tv)) = tv `elem` tvs
>         tvs = typeVars ty

\end{lstlisting}
Obtaining the list of relevant type class constraints.
\begin{lstlisting}

> relevantTypeContext :: TypeWithContext -> (TypeWithContext,TypeContext)
> relevantTypeContext (TypeWithContext (TypeContext classPreds) ty) =
>   (TypeWithContext (TypeContext (filter isRelevant classPreds)) ty,
>    TypeContext (filter isNotRelevant classPreds))
>   where isRelevant    = any (`elem`    tvs) . typeVars
>         isNotRelevant = not . isRelevant
>         tvs = typeVars ty

\end{lstlisting}
Error messages:
\begin{lstlisting}

> noInstanceFound :: QualIdent -> QualIdent -> String
> noInstanceFound typeClass typeConstructor =
>   "No instance of type class " ++ qualName typeClass ++
>   " for type constructor " ++ qualName typeConstructor ++ " found"

\end{lstlisting}
