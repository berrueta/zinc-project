% $Id: TypeClassEnv.lhs 1323 2004-09-05 17:28:16Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeClassEnv.lhs}
\codesection{Type class environment}
This module defines the type class environment. It stores static information
about type classes, needed to perform type checking.

For more information about this environment, refer to section 4 of
Implementing Type Classes; pages 10 and 15 of Type Classes in Haskell
\begin{lstlisting}

> module TypeClassEnv where
> import Ident
> import TopEnv
> import TypeExpr
> import Env
> import CurrySyntax
> import Error(internalError)

> data TypeClassInfo =
>    TypeClassInfo
>       QualIdent                         -- full qualified identifier
>       Ident                             -- type variable
>       [QualIdent]                       -- superclasses
>       (Env Ident TypeExprWithContext)   -- method signatures
>    deriving (Eq,Show)

> instance Entity TypeClassInfo where
>   origName (TypeClassInfo qid _ _ _) = qid

> type TypeClassEnv = TopEnv TypeClassInfo

\end{lstlisting}
Basic operations on type class environments.
\begin{lstlisting}

> bindTypeClass :: ModuleIdent -> Ident -> Ident ->  [QualIdent] ->
>                  [(Ident,TypeExprWithContext)] ->
>                  TypeClassEnv -> TypeClassEnv
> bindTypeClass mid tc tv super methodSigs =
>   bindLocalTopEnv tc tci . qualBindLocalTopEnv tc' tci
>   where tci = TypeClassInfo tc' tv super methodSigEnv
>         tc' = qualifyWith mid tc
>         methodSigEnv :: Env Ident TypeExprWithContext
>         methodSigEnv = foldr (uncurry bindEnv) emptyEnv methodSigs

> lookupTypeClass :: Ident -> TypeClassEnv -> [TypeClassInfo]
> lookupTypeClass tc typeClassEnv = lookupTopEnv tc typeClassEnv

> qualLookupTypeClass :: QualIdent -> TypeClassEnv -> [TypeClassInfo]
> qualLookupTypeClass tc typeClassEnv = qualLookupTopEnv tc typeClassEnv

> noTypeClassEnv :: TypeClassEnv
> noTypeClassEnv = internalError "noTypeClassEnv"

> typeClassMethods :: TypeClassInfo -> [Ident]
> typeClassMethods (TypeClassInfo _ _ _ sigEnv) = map fst (envToList sigEnv)

> allMethods :: TypeClassEnv -> [QualIdent]
> allMethods = concatMap (\(tc,TypeClassInfo _ _ _ mets) ->
>                         map (qualifyLike tc . fst) (envToList mets)) .
>              allBindings

\end{lstlisting}
