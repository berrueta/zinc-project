% $Id: TypeErrors.lhs 970 2004-08-08 12:05:32Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeErrors.lhs}
\codesection{Type errors}
Error functions.
\begin{lstlisting}

> module TypeErrors where
> import Ident
> import Pretty
> import TypeExpr
> import Types
> import CurryPP(ppTypeExpr,ppTypeExprWithContext)
> import TypeTrans(ppTypeScheme,ppType,ppTypeWithContext,unqualifyIdentsIn)

> recursiveTypes :: [Ident] -> String
> recursiveTypes [tc] = "Recursive synonym type " ++ name tc
> recursiveTypes (tc:tcs) =
>   "Recursive synonym types " ++ name tc ++ types "" tcs
>   where types comma [tc] = comma ++ " and " ++ name tc
>         types _ (tc:tcs) = ", " ++ name tc ++ types "," tcs

> polymorphicFreeVar :: Ident -> String
> polymorphicFreeVar v =
>   "Free variable " ++ name v ++ " has a polymorphic type"

> typeSigTooGeneral :: ModuleIdent -> Doc -> TypeExprWithContext -> TypeScheme ->
>                      String
> typeSigTooGeneral m what tyWC sigma = show $
>   vcat [text "Type signature too general", what,
>         text "Inferred type:" <+> ppTypeScheme m sigma,
>         text "Type signature:" <+> ppTypeExprWithContext (unqualifyIdentsIn m tyWC)]

> nonFunctionType :: String -> Doc -> ModuleIdent -> Type -> String
> nonFunctionType what doc m ty = show $
>   vcat [text "Type error in" <+> text what, doc,
>         text "Type:" <+> ppType m ty,
>         text "Cannot be applied"]

> nonBinaryOp :: String -> Doc -> ModuleIdent -> Type -> String
> nonBinaryOp what doc m ty = show $
>   vcat [text "Type error in" <+> text what, doc,
>         text "Type:" <+> ppType m ty,
>         text "Cannot be used as binary operator"]

> typeMismatch :: String -> Doc -> ModuleIdent -> Type -> Type -> String
> typeMismatch what doc m ty1 ty2 = show $
>   vcat [text "Type error in" <+> text what, doc,
>         text "Inferred type:" <+> ppType m ty2,
>         text "Expected type:" <+> ppType m ty1]

> skolemEscapingScope :: ModuleIdent -> Doc -> TypeWithContext -> String
> skolemEscapingScope m what tyWC = show $
>   vcat [text "Existential type escapes out of its scope", what,
>         text "Type:" <+> ppTypeWithContext m tyWC]

> unresolvedOverloading :: Doc -> [TypeClassConstraint] -> String
> unresolvedOverloading what tcc =
>  show what ++ "\nUnresolved overloading of type variable " ++ show tv ++
>  " on type class " ++ show tc
>  where (TypeClassConstraint tc (TypeVariable tv)) = head tcc

\end{lstlisting}
