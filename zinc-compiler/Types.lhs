% $Id: Types.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2002, Wolfgang Lux
% See LICENSE for the full license.
%
\nwfilename{Types.lhs}
\codesection{Types}
This module modules provides the definitions for the internal 
representation of types in the compiler.
\begin{lstlisting}

> module Types where
> import Ident
> import List

\end{lstlisting}
A type is either a type variable, a type constructor, or an application
of a type to a list of types (arguments).

There are two special cases of type variables. The \texttt{TypeConstrained}
case is used to represent type variables that are restricted to a
particular set of types. At present, this is used for typing guard
expressions, which are restricted to be either of type \texttt{Bool}
or of type \texttt{Success}, and integer literals, which are
restricted to types \texttt{Int} and \texttt{Float}. If the type is
not restricted it defaults to the first type from the constraint list.
The case \texttt{TypeSkolem} is used for handling skolem types, which
result from the use of existentially quantified data constructors.

Type variables are represented with deBruijn style indices. Universally
quantified type variables are assigned indices in the order of their
occurrence in the type from left to right. This leads to a canonical
representation of types where $\alpha$-equivalence of two types
coincides with equality of the representation.

Note that even though \texttt{TypeConstrained} variables use indices
as well, these variables must never be quantified.
\begin{lstlisting}

> data Type =
>     TypeConstructor QualIdent
>   | TypeVariable Int
>   | TypeConstrained [Type] Int
>   | TypeSkolem Int
>   | TypeApplication Type Type
>   deriving (Eq,Show)

\end{lstlisting}
Application of type expressions:
\begin{lstlisting}

> typeApply :: Type -> [Type] -> Type
> typeApply = foldl TypeApplication

> leftmostType :: Type -> Type
> leftmostType t@(TypeConstructor _    ) = t
> leftmostType t@(TypeVariable    _    ) = t
> leftmostType t@(TypeConstrained _   _) = t
> leftmostType t@(TypeSkolem      _    ) = t
> leftmostType   (TypeApplication ty1 _) = leftmostType ty1

> typeArguments :: Type -> [Type]
> typeArguments (TypeConstructor _      ) = []
> typeArguments (TypeVariable    _      ) = []
> typeArguments (TypeConstrained _    _ ) = []
> typeArguments (TypeSkolem      _      ) = []
> typeArguments (TypeApplication ty1 ty2) =
>   (typeArguments ty1) ++ [ty2]

\end{lstlisting}
The function \texttt{isArrowType} checks whether a type is a function
type $t_1 \rightarrow t_2 \rightarrow \dots \rightarrow t_n$ . The
function \texttt{arrowArity} computes the arity $n$ of a function type
and \texttt{arrowBase} returns the type $t_n$.

\ToDo{As exposed by unit tests, the function \lstinline|arrowArity|
returns $n_1$, not $n$. Is this a bug in the documentation? Is this
a feature?}

The function \lstinline|arrowArgs| returns the list $[t_1,\ldots,t_{n-1}]$.
\begin{lstlisting}

> typeArrow :: Type -> Type -> Type
> typeArrow ty1 ty2 = typeApply (primInternalType arrowId) [ty1,ty2]

> typeArrowWithContext :: TypeWithContext -> TypeWithContext -> TypeWithContext
> typeArrowWithContext (TypeWithContext ctx1 ty1) (TypeWithContext ctx2 ty2) =
>   TypeWithContext (composeTypeContext ctx1 ctx2) (typeArrow ty1 ty2)

> isArrowType :: Type -> Bool
> isArrowType ty = case leftmostType ty of
>   (TypeConstructor id) -> isQArrowId id
>   _                    -> False

> arrowArity :: Type -> Int
> arrowArity ty
>   | isArrowType ty = length $ arrowArgs ty
>   | otherwise      = 0

> arrowArgs :: Type -> [Type]
> arrowArgs ty
>   | isArrowType ty = ty1 : (arrowArgs ty2)
>   | otherwise      = []
>   where [ty1,ty2] = typeArguments ty

> arrowBase :: Type -> Type
> arrowBase ty
>   | isArrowType ty = arrowBase ty2
>   | otherwise      = ty
>   where [ty1,ty2] = typeArguments ty

\end{lstlisting}
The functions \texttt{typeVars}, \texttt{typeConstrs},
\texttt{typeSkolems} return a list of all type variables, type
constructors, or skolems occurring in a type $t$, respectively. Note
that \texttt{TypeConstrained} variables are not included in the set of
type variables because they cannot be generalized.
\begin{lstlisting}

> class TypeVarsClass a where
>   typeVars    :: a -> [Int]
>   typeSkolems :: a -> [Int]

> instance TypeVarsClass Type where
>   typeVars ty = vars ty []
>     where vars (TypeConstructor _      ) tvs = tvs
>           vars (TypeVariable    tv     ) tvs = tv : tvs
>           vars (TypeConstrained _   _  ) tvs = tvs
>           vars (TypeSkolem      _      ) tvs = tvs
>           vars (TypeApplication ty1 ty2) tvs = vars ty1 $ vars ty2 tvs
>   typeSkolems ty = skolems ty []
>     where skolems (TypeConstructor _      ) sks = sks
>           skolems (TypeVariable    _      ) sks = sks
>           skolems (TypeConstrained _   _  ) sks = sks
>           skolems (TypeSkolem      k      ) sks = k : sks
>           skolems (TypeApplication ty1 ty2) sks = skolems ty1 $ skolems ty2 sks

> instance TypeVarsClass TypeClassConstraint where
>   typeVars    (TypeClassConstraint tc ty) = typeVars ty
>   typeSkolems (TypeClassConstraint tc ty) = typeSkolems ty

> instance TypeVarsClass TypeWithContext where
>   typeVars    (TypeWithContext _ ty) = typeVars ty
>   typeSkolems (TypeWithContext _ ty) = typeSkolems ty

\end{lstlisting}
We support two kinds of quantifications of types here, universally
quantified type schemes $\forall\overline{\alpha} \theta .
\tau(\overline{\alpha})$ and universally and existentially quantified
type schemes $\forall\overline{\alpha} \exists\overline{\eta} .
\tau(\overline{\alpha},\overline{\eta})$.  In both, quantified type
variables are assigned ascending indices starting from 0. Therefore it
is sufficient to record the numbers of quantified type variables in
the \texttt{ForAll} and \texttt{ForAllExist} constructors. In case of
the latter, the first of the two numbers is the number of universally
quantified variables and the second the number of existentially
quantified variables.
\begin{lstlisting}

> data TypeScheme = ForAll Int TypeWithContext deriving (Eq,Show)
> data ExistTypeScheme = ForAllExist Int Int Type deriving (Eq,Show)

> mapTypeScheme :: (TypeWithContext -> TypeWithContext) ->
>                  TypeScheme -> TypeScheme
> mapTypeScheme f (ForAll n tyWC) = ForAll n (f tyWC)

\end{lstlisting}
The functions \texttt{monoType} and \texttt{polyType} translate a type
$\tau$ into a monomorphic type scheme $\forall.\tau$ and a polymorphic
type scheme $\forall\overline{\alpha}.\tau$ where $\overline{\alpha} =
\textrm{fv}(\tau)$, respectively. \texttt{polyType} assumes that all
universally quantified variables in the type are assigned indices
starting with 0 and does not renumber the variables.
\begin{lstlisting}

> monoType, polyType :: Type -> TypeScheme
> monoType ty = monoTypeWithContext (TypeWithContext emptyTypeContext ty)
> polyType ty = polyTypeWithContext (TypeWithContext emptyTypeContext ty)

> monoTypeWithContext,polyTypeWithContext :: TypeWithContext -> TypeScheme
> monoTypeWithContext tyWC = ForAll 0 tyWC
> polyTypeWithContext tyWC = ForAll (maximum (-1 : typeVars tyWC) + 1) tyWC

\end{lstlisting}
There are a few predefined types:
\begin{lstlisting}

> unitType,boolType,charType,intType,floatType,stringType,successType :: Type
> unitType    = primInternalType unitId
> boolType    = primType boolId
> charType    = primType charId
> intType     = primType intId
> floatType   = primType floatId
> stringType  = listType charType
> successType = primType successId

> unitTypeWithContext,boolTypeWithContext,charTypeWithContext :: TypeWithContext
> intTypeWithContext,floatTypeWithContext,stringTypeWithContext :: TypeWithContext
> successTypeWithContext :: TypeWithContext
> unitTypeWithContext    = TypeWithContext emptyTypeContext unitType
> boolTypeWithContext    = TypeWithContext emptyTypeContext boolType
> charTypeWithContext    = TypeWithContext emptyTypeContext charType
> intTypeWithContext     = TypeWithContext emptyTypeContext intType
> floatTypeWithContext   = TypeWithContext emptyTypeContext floatType
> stringTypeWithContext  = TypeWithContext emptyTypeContext stringType
> successTypeWithContext = TypeWithContext emptyTypeContext successType

> listType,ioType :: Type -> Type
> listType ty = typeApply (primInternalType listId) [ty]
> ioType ty   = typeApply (primType ioId) [ty]

> tupleType :: [Type] -> Type
> tupleType tys = typeApply (primInternalType $ tupleId $ length tys) tys

> listTypeWithContext,ioTypeWithContext :: TypeWithContext -> TypeWithContext
> listTypeWithContext (TypeWithContext ctx ty) =
>   TypeWithContext ctx (listType ty)
> ioTypeWithContext (TypeWithContext ctx ty) =
>   TypeWithContext ctx (ioType ty)

> tupleTypeWithContext :: [TypeWithContext] -> TypeWithContext
> tupleTypeWithContext tyWCs =
>   TypeWithContext ctx (tupleType (map removeTypeContext tyWCs))
>   where ctx = foldr composeTypeContext emptyTypeContext (map typeContext tyWCs)

> primType,primInternalType :: Ident -> Type
> primType = TypeConstructor . qualifyWith preludeMIdent
> primInternalType = TypeConstructor . qualifyWith internalTypeMIdent

> typeVar :: Int -> Type
> typeVar = TypeVariable

\end{lstlisting}
A type context is a set of class constraints. A class constraint
establishes a binding between a type class and a type expression
(usually, a type variable, but it can also be a type constructor).
\begin{lstlisting}

> data TypeContext = TypeContext [TypeClassConstraint]
>                  deriving (Eq,Show)

> data TypeClassConstraint = TypeClassConstraint QualIdent Type
>                          deriving (Eq,Show)

> emptyTypeContext :: TypeContext
> emptyTypeContext = TypeContext []

> composeTypeContext :: TypeContext -> TypeContext -> TypeContext
> composeTypeContext (TypeContext classPreds1) (TypeContext classPreds2) =
>   TypeContext (classPreds1 ++ classPreds2)

> typeClassConstraints :: TypeContext -> [TypeClassConstraint]
> typeClassConstraints (TypeContext classPreds) = classPreds

\end{lstlisting}
Two class constraints can only be compared if they constraint a
variable.
\begin{lstlisting}

> instance Ord TypeClassConstraint where
>   compare (TypeClassConstraint tc1 (TypeVariable tv1))
>           (TypeClassConstraint tc2 (TypeVariable tv2)) =
>     case compare tv1 tv2 of
>       LT -> LT
>       GT -> GT
>       EQ -> compare tc1 tc2

\end{lstlisting}
Qualified types (a type with a context) are written as TypeWithContext
in order to avoid ambiguities with QualType.
\begin{lstlisting}

> data TypeWithContext = TypeWithContext TypeContext Type
>                      deriving (Eq,Show)

> removeTypeContext :: TypeWithContext -> Type
> removeTypeContext (TypeWithContext _ ty) = ty

> typeContext :: TypeWithContext -> TypeContext
> typeContext (TypeWithContext ctx _) = ctx

> appendTypeContext :: TypeWithContext -> TypeContext -> TypeWithContext
> appendTypeContext (TypeWithContext ctx ty) ctx' =
>   TypeWithContext (composeTypeContext ctx ctx') ty

> mapType :: (Type -> Type) -> TypeWithContext -> TypeWithContext
> mapType f (TypeWithContext ctx ty) = TypeWithContext ctx (f ty)

> mapTypeContext :: (TypeContext -> TypeContext) ->
>                   TypeWithContext -> TypeWithContext
> mapTypeContext f (TypeWithContext ctx ty) = TypeWithContext (f ctx) ty

> removeExistTypeClassConstraints :: TypeContext -> TypeContext
> removeExistTypeClassConstraints
>   (TypeContext classPreds ) =
>   (TypeContext classPreds')
>   where classPreds' = filter (not . any (<0) . typeVars) classPreds

\end{lstlisting}
