% $Id: Ident.lhs 1398 2004-09-09 21:45:26Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2003, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Ident.lhs}
\codesection{Identifiers}
This module provides the implementation of identifiers and, in 
addition, some utility functions for identifiers which are used at 
various places in the compiler.

Identifiers comprise the name of the denoted entity and a unique 
\emph{id} that can be used to distinguish different entities with the 
same name, e.g., in different scopes. We take the convention that an 
\emph{id} of $0$ is ignored if the name is not empty.

\ToDo{Probably we should use \texttt{Integer} for the \emph{id}s.}

Qualified identifiers may optionally be prefixed by a module
name. \textbf{The order of the cases \texttt{UnqualIdent} and
\texttt{QualIdent} is important. Some parts of the compiler rely on
the fact that all qualified identifiers are greater than any
unqualified identifier.}
\begin{lstlisting}

> module Ident(Ident,QualIdent,ModuleIdent,
>              mkIdent,name,qualName,uniqueId,renameIdent,unRenameIdent,
>              prefixIdent,postfixIdent,
>              mkMIdent,moduleName,moduleQualifiers,isInfixOp,isQInfixOp,
>              qualify,qualifyWith,qualifyLike,qualifyLikeOr,
>              qualQualify,isQualified,
>              unqualify,qualUnqualify,qualRequalify,localIdent,splitQualIdent,
>              emptyMIdent,mainMIdent,preludeMIdent,internalTypeMIdent,
>              anonId,isAnonId,
>              -- tuple functions
>              tupleArity,qTupleArity,
>              -- unqualified type class idents
>              eqId,ordId,enumId,boundedId,showId,
>              -- unqualified type constructor idents
>              unitId,boolId,charId,intId,floatId,listId,arrowId,ioId,
>              successId,tupleId,selectorId,
>              -- qualified type constructor idents
>              qUnitId,qBoolId,qCharId,qIntId,qFloatId,qListId,qArrowId,
>              qIOId,qSuccessId,qTupleId,
>              -- unqualified data constructor idents
>              trueId,falseId,nilId,consId,noDataId,
>              -- qualified data constructor idents
>              qNilId,qConsId,qTrueId,qFalseId,qNoDataId,
>              -- predicates
>              isListId,isArrowId,isUnitId,isTupleId,isSelectorId,
>              isQUnitId,isQListId,isQArrowId,isQTupleId,isQSelectorId,
>              isInternalId,isQInternalId,
>              -- others
>              mainId,minusId,fminusId,
>              nameSupply) where
> import Char
> import List
> import Maybe

> data Ident = Ident String Int deriving (Eq,Ord)
> data QualIdent = UnqualIdent Ident | QualIdent ModuleIdent Ident
>                  deriving (Eq,Ord)
> newtype ModuleIdent = ModuleIdent [String] deriving (Eq,Ord)

> instance Show Ident where
>   showsPrec _ (Ident x n)
>     | n == 0 = showString x
>     | otherwise = showString x . showChar '.' . shows n
> instance Show QualIdent where
>   showsPrec _ (UnqualIdent x) = shows x
>   showsPrec _ (QualIdent m x) = shows m . showChar '.' . shows x
> instance Show ModuleIdent where
>   showsPrec _ m = showString (moduleName m)

> mkIdent :: String -> Ident
> mkIdent x = Ident x 0

> name :: Ident -> String
> name (Ident x _) = x

> qualName :: QualIdent -> String
> qualName (UnqualIdent x) = name x
> qualName (QualIdent m x) = moduleName m ++ "." ++ name x

> uniqueId :: Ident -> Int
> uniqueId (Ident _ n) = n

> renameIdent :: Ident -> Int -> Ident
> renameIdent (Ident x _) n = Ident x n

> unRenameIdent :: Ident -> Ident
> unRenameIdent (Ident x _) = Ident x 0

> prefixIdent :: String -> Ident -> Ident
> prefixIdent prefix (Ident x n) = Ident (prefix ++ x) n

> postfixIdent :: String -> Ident -> Ident
> postfixIdent postfix (Ident x n) = Ident (x ++ postfix) n

> mkMIdent :: [String] -> ModuleIdent
> mkMIdent = ModuleIdent

> moduleName :: ModuleIdent -> String
> moduleName (ModuleIdent xs) = concat (intersperse "." xs)

> moduleQualifiers :: ModuleIdent -> [String]
> moduleQualifiers (ModuleIdent xs) = xs

> isInfixOp :: Ident -> Bool
> isInfixOp (Ident ('<':c:cs) _)=
>   last (c:cs) /= '>' || not (isAlphaNum c) && c `notElem` "_(["
> isInfixOp (Ident (c:_) _) = not (isAlphaNum c) && c `notElem` "_(["
> isInfixOp (Ident _ _) = False -- error "Zero-length identifier"

> isQInfixOp :: QualIdent -> Bool
> isQInfixOp (UnqualIdent x) = isInfixOp x
> isQInfixOp (QualIdent _ x) = isInfixOp x

\end{lstlisting}
The functions \texttt{qualify} and \texttt{qualifyWith} convert an
unqualified identifier into a qualified identifier (without or with a
given module prefix).
\begin{lstlisting}

> qualify :: Ident -> QualIdent
> qualify = UnqualIdent

> qualifyWith :: ModuleIdent -> Ident -> QualIdent
> qualifyWith = QualIdent

> qualifyLike :: QualIdent -> Ident -> QualIdent
> qualifyLike = maybe qualify qualifyWith . fst . splitQualIdent

> qualifyLikeOr :: QualIdent -> ModuleIdent -> Ident -> QualIdent
> qualifyLikeOr x m = qualifyWith (fromMaybe m $ fst $ splitQualIdent x)

> qualQualify :: ModuleIdent -> QualIdent -> QualIdent
> qualQualify m (UnqualIdent x) = QualIdent m x
> qualQualify _ x = x

> isQualified :: QualIdent -> Bool
> isQualified (UnqualIdent _) = False
> isQualified (QualIdent _ _) = True

> unqualify :: QualIdent -> Ident
> unqualify (UnqualIdent x) = x
> unqualify (QualIdent _ x) = x

> qualUnqualify :: ModuleIdent -> QualIdent -> QualIdent
> qualUnqualify m (UnqualIdent x) = UnqualIdent x
> qualUnqualify m (QualIdent m' x)
>   | m == m' = UnqualIdent x
>   | otherwise = QualIdent m' x

> qualRequalify :: ModuleIdent -> ModuleIdent -> QualIdent -> QualIdent
> qualRequalify m1 m2 (UnqualIdent x) = (UnqualIdent x)
> qualRequalify m1 m2 (QualIdent m' x)
>   | m1 == m'  = (QualIdent m2 x)
>   | otherwise = (QualIdent m' x)

> localIdent :: ModuleIdent -> QualIdent -> Maybe Ident
> localIdent _ (UnqualIdent x) = Just x
> localIdent m (QualIdent m' x)
>   | m == m' = Just x
>   | otherwise = Nothing

> splitQualIdent :: QualIdent -> (Maybe ModuleIdent,Ident)
> splitQualIdent (UnqualIdent x) = (Nothing,x)
> splitQualIdent (QualIdent m x) = (Just m,x)

\end{lstlisting}
Predefined module identifiers:
\begin{lstlisting}

> emptyMIdent, mainMIdent, preludeMIdent, internalTypeMIdent :: ModuleIdent
> emptyMIdent   = ModuleIdent []
> mainMIdent    = ModuleIdent ["main"]
> preludeMIdent = ModuleIdent ["prelude"]
> internalTypeMIdent = ModuleIdent ["_internalType"]

\end{lstlisting}
Anonymous identifier:
\begin{lstlisting}

> anonId :: Ident
> anonId = Ident "_" 0

> isAnonId :: Ident -> Bool
> isAnonId id = (id == anonId)

\end{lstlisting}
Predefined type class identifiers:
\begin{lstlisting}

> eqId, ordId, enumId, boundedId, showId :: Ident
> eqId	    = Ident "Eq" 0
> ordId	    = Ident "Ord" 0
> enumId    = Ident "Enum" 0
> boundedId = Ident "Bounded" 0
> showId    = Ident "Show" 0

\end{lstlisting}
Predefined type identifiers:
\begin{lstlisting}

> unitId, boolId, charId, intId, floatId, listId, arrowId, ioId, successId :: Ident
> unitId    = Ident "()" 0
> boolId    = Ident "Bool" 0
> charId    = Ident "Char" 0
> intId     = Ident "Int" 0
> floatId   = Ident "Float" 0
> listId    = Ident "[]" 0
> arrowId   = Ident "->" 0
> ioId      = Ident "IO" 0
> successId = Ident "Success" 0

> tupleId :: Int -> Ident
> tupleId n
>   | n >= 2 = Ident ("(" ++ replicate (n - 1) ',' ++ ")") 0
>   | otherwise = error "internal error: tupleId"

> selectorId :: Int -> Ident
> selectorId n = Ident ("_#sel" ++ show n) 0

> qUnitId, qBoolId, qCharId, qIntId, qFloatId, qListId, qArrowId :: QualIdent
> qIOId, qSuccessId :: QualIdent
> qUnitId    = qualifyWith internalTypeMIdent unitId
> qBoolId    = qualifyWith internalTypeMIdent boolId
> qCharId    = qualifyWith internalTypeMIdent charId
> qIntId     = qualifyWith internalTypeMIdent intId
> qFloatId   = qualifyWith internalTypeMIdent floatId
> qListId    = qualifyWith internalTypeMIdent listId
> qArrowId   = qualifyWith internalTypeMIdent arrowId
> qIOId      = qualifyWith preludeMIdent ioId
> qSuccessId = qualifyWith preludeMIdent successId

> qTupleId :: Int -> QualIdent
> qTupleId = qualifyWith internalTypeMIdent . tupleId

\end{lstlisting}
Computes de arity of a tuple identifier:
\begin{lstlisting}

> tupleArity :: Ident -> Int
> tupleArity x
>   | n > 1 && x == tupleId n = n
>   | otherwise = error "internal error: tupleArity"
>   where n = length (name x) - 1

> qTupleArity :: QualIdent -> Int
> qTupleArity = tupleArity . unqualify

\end{lstlisting}
Predefined data constructors:
\begin{lstlisting}

> trueId, falseId, nilId, consId, noDataId :: Ident
> trueId  = Ident "True" 0
> falseId = Ident "False" 0
> nilId   = Ident "[]" 0
> consId  = Ident ":" 0
> noDataId = Ident "()" 0

> qNilId, qConsId, qNoDataId :: QualIdent
> qNilId  = qualifyWith internalTypeMIdent nilId
> qConsId = qualifyWith internalTypeMIdent consId
> qNoDataId = qualifyWith internalTypeMIdent noDataId

> qTrueId, qFalseId :: QualIdent
> qTrueId = qualifyWith preludeMIdent trueId
> qFalseId = qualifyWith preludeMIdent falseId

\end{lstlisting}
Predicates on idents:
\begin{lstlisting}

> isListId :: Ident -> Bool
> isListId id = (id == listId)

> isArrowId :: Ident -> Bool
> isArrowId id = (id == arrowId)

> isQListId :: QualIdent -> Bool
> isQListId = isListId . unqualify

> isQArrowId :: QualIdent -> Bool
> isQArrowId = isArrowId . unqualify

> isTupleId :: Ident -> Bool
> isTupleId x = n > 1 && x == tupleId n
>   where n = length (name x) - 1

> isUnitId :: Ident -> Bool
> isUnitId id = (id == unitId)

> isSelectorId :: Ident -> Bool
> isSelectorId x = any ("_#sel" `isPrefixOf`) (tails (name x))

> isQTupleId :: QualIdent -> Bool
> isQTupleId = isTupleId . unqualify

> isQUnitId :: QualIdent -> Bool
> isQUnitId = isUnitId . unqualify

> isQSelectorId :: QualIdent -> Bool
> isQSelectorId = isSelectorId . unqualify

> isInternalId :: Ident -> Bool
> isInternalId (Ident s _) = length s > 1 && head s == '_'

> isQInternalId :: QualIdent -> Bool
> isQInternalId = isInternalId . unqualify

\end{lstlisting}
Other identifiers:
\begin{lstlisting}

> mainId, minusId, fminusId :: Ident
> mainId = Ident "main" 0
> minusId = Ident "-" 0
> fminusId = Ident "-." 0

\end{lstlisting}
Name supply for the generation of (type) variable names.
\begin{lstlisting}

> nameSupply :: [Ident]
> nameSupply = map mkIdent [c:showNum i | i <- [0..], c <- ['a'..'z']]
>   where showNum 0 = ""
>         showNum n = show n

\end{lstlisting}
\ToDo{The \texttt{nameSupply} should respect the current case mode, 
i.e., use upper case for variables in Prolog mode.}
