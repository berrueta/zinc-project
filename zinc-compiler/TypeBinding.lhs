% $Id: TypeBinding.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeBinding.lhs}
\codesection{Defining Types}
\begin{lstlisting}

> module TypeBinding where
> import Base
> import Ident
> import CurrySyntax
> import TypeConstructorEnv
> import ValueEnv
> import TypeExpansion
> import SCC
> import Error
> import TypeErrors
> import Types
> import TopEnv
> import TypeExpr
> import AliasCheck

\end{lstlisting}
Before type checking starts, the types defined in the local module
have to be entered into the type constructor environment. All type
synonyms occurring in the definitions are fully expanded and all type
constructors are qualified with the name of the module in which they
are defined. This is possible because Curry does not allow (mutually)
recursive type synonyms. In order to simplify the expansion of type
synonyms, the compiler first performs a dependency analysis on the
type definitions. This also makes it easy to identify (mutually)
recursive synonyms.

Note that \texttt{bindTC} is passed the \emph{final} type constructor
environment in order to handle the expansion of type synonyms. This
does not lead to a termination problem because \texttt{sortTypeDecls}
already has checked that there are no recursive type synonyms.

We have to be careful with existentially quantified type variables for
data constructors. An existentially quantified type variable may
shadow a universally quantified variable from the left hand side of
the type declaration. In order to avoid wrong indices being assigned
to these variables, we replace all shadowed variables in the left hand
side by \texttt{anonId} before passing them to \texttt{expandMonoType}
and \texttt{expandMonoTypes}, respectively.
\begin{lstlisting}

> bindTypes :: ModuleIdent -> [Decl] -> TCEnv -> TCEnv
> bindTypes m ds tcEnv = tcEnv'
>   where tds    = filter isTypeDecl ds
>         tds'   = sortTypeDecls m tds
>         tcEnv' = foldr (bindTC m tcEnv') tcEnv tds'

> bindTC :: ModuleIdent -> TCEnv -> Decl -> TCEnv -> TCEnv
> bindTC m tcEnv (DataDecl _ tc tvs cs _) =
>   bindLocalTypeInfo DataType m tc tvs (map (Just . mkData) cs)
>   where mkData (ConstrDecl p evs c tys) = Data c (length evs) tys'
>           where tys' = expandMonoTypes tcEnv (cleanTVars tvs evs)
>                      $ map (aliasCheckTypeExpr p tcEnv) tys
>         mkData (ConOpDecl p evs ty1 op ty2) = Data op (length evs) tys'
>           where tys' = expandMonoTypes tcEnv (cleanTVars tvs evs)
>                      $ map (aliasCheckTypeExpr p tcEnv) [ty1,ty2]
> bindTC m tcEnv (NewtypeDecl _ tc tvs (NewConstrDecl p evs c ty) _) =
>   bindLocalTypeInfo RenamingType m tc tvs (Data c (length evs) ty')
>   where ty' = expandMonoType tcEnv (cleanTVars tvs evs)
>             $ aliasCheckTypeExpr p tcEnv ty
> bindTC m tcEnv (TypeDecl p tc tvs ty) =
>   bindLocalTypeInfo AliasType m tc tvs $
>     expandMonoType tcEnv tvs $
>       aliasCheckTypeExpr p tcEnv ty
> bindTC _ _ d = internalError ("TypeBinding.bindTC: " ++ show d)

> cleanTVars :: [Ident] -> [Ident] -> [Ident]
> cleanTVars tvs evs = [if tv `elem` evs then anonId else tv | tv <- tvs]

> sortTypeDecls :: ModuleIdent -> [Decl] -> [Decl]
> sortTypeDecls m = map (typeDecl m) . scc bound free
>   where bound (DataDecl _ tc _ _ _) = [tc]
>         bound (NewtypeDecl _ tc _ _ _) = [tc]
>         bound (TypeDecl _ tc _ _) = [tc]
>         free (DataDecl _ _ _ _ _) = []
>         free (NewtypeDecl _ _ _ _ _) = []
>         free (TypeDecl _ _ _ ty) = ft m ty []

> typeDecl :: ModuleIdent -> [Decl] -> Decl
> typeDecl _ [] = internalError "typeDecl"
> typeDecl _ [d@(DataDecl _ _ _ _ _)] = d
> typeDecl _ [d@(NewtypeDecl _ _ _ _ _)] = d
> typeDecl m [d@(TypeDecl p tc _ ty)]
>   | tc `elem` ft m ty [] = errorAt p (recursiveTypes [tc])
>   | otherwise = d
> typeDecl _ (TypeDecl p tc _ _ : ds) =
>   errorAt p (recursiveTypes (tc : [tc' | TypeDecl _ tc' _ _ <- ds]))

> ft :: ModuleIdent -> TypeExpr -> [Ident] -> [Ident]
> ft m ty@(TypeExprConstructor tc  ) tcs =
>   maybe id (:) (localIdent m tc) tcs
> ft _ (TypeExprVariable    _      ) tcs = tcs
> ft m (TypeExprApplication ty1 ty2) tcs = ft m ty1 (ft m ty2 tcs)

\end{lstlisting}
