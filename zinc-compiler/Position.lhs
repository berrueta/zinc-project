% $Id: Position.lhs 970 2004-08-08 12:05:32Z berrueta $
%
% Copyright (c) 2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Position.lhs}
\codesection{Positions}
A source file position consists of a filename, a line number, and a
column number. A tab stop is assumed at every eighth column.
\begin{lstlisting}

> module Position where

> data Position =
>   Position{ file :: FilePath, line :: Int, column :: Int }
>   deriving (Eq, Ord)

> instance Show Position where
>   showsPrec _ (Position fn l c) =
>     (if null fn then id else shows fn . showString ", ") .
>     showString "line " . shows l .
>     (if c > 0 then showChar '.' . shows c else id)

> tabWidth :: Int
> tabWidth = 8

> first :: FilePath -> Position
> first fn = Position fn 1 1

> incr :: Position -> Int -> Position
> incr (Position fn l c) n = Position fn l (c + n)

> next :: Position -> Position
> next = flip incr 1

> tab :: Position -> Position
> tab (Position fn l c) = Position fn l (c + tabWidth - (c - 1) `mod` tabWidth)

> nl :: Position -> Position
> nl (Position fn l c) = Position fn (l + 1) 1

\end{lstlisting}
Position of generated code.
\begin{lstlisting}

> genPos :: Position
> genPos = Position { file = "*GENERATED CODE*", line=1, column=1 }

\end{lstlisting}
