% $Id: CurrySyntax.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2004, Wolfgang Lux
% See LICENSE for the full license.
%
\nwfilename{CurrySyntax.lhs}
\codesection{The Parse Tree}
This module provides the necessary data structures to maintain the
parsed representation of a Curry program.
\begin{lstlisting}

> module CurrySyntax where
> import Ident
> import Position
> import TypeExpr
> import Kind
> import Types

\end{lstlisting}
\codeparagraph{Modules}
\begin{lstlisting}

> data Module = Module ModuleIdent (Maybe ExportSpec) [Decl] deriving (Eq,Show)

> data ExportSpec = Exporting Position [Export] deriving (Eq,Show)
> data Export =
>     Export              QualIdent                  -- f/T
>   | ExportTypeAll       QualIdent                  -- T(..)
>   | ExportTypeWith      QualIdent [Ident]          -- T(C1,...,Cn)
>   | ExportTypeClassAll  QualIdent
>   | ExportTypeClassWith QualIdent [Ident]
>   | ExportModule        ModuleIdent
>   deriving (Eq,Show)

\end{lstlisting}
\codeparagraph{Module declarations}
\begin{lstlisting}

> data ImportSpec =
>     Importing Position [Import]
>   | Hiding Position [Import]
>   deriving (Eq,Show)
> data Import =
>     Import         Ident            -- f/T
>   | ImportTypeAll  Ident            -- T(..)
>   | ImportTypeWith Ident [Ident]    -- T(C1,...,Cn)
>   deriving (Eq,Show)

\end{lstlisting}
\codeparagraph{Declarations}
Note that external functions cannot have type contexts.
\begin{lstlisting}

> data Decl =
>     ImportDecl     Position ModuleIdent Qualified (Maybe ModuleIdent)
>                    (Maybe ImportSpec)
>   | InfixDecl      Position Infix Int [Ident]
>   | TypeClassDecl  Position TypeExprContext Ident Ident [Decl]
>   | InstanceDecl   Position TypeExprContext QualIdent TypeExpr [Decl]
>   | DataDecl       Position Ident [Ident] [ConstrDecl] [Ident]
>   | NewtypeDecl    Position Ident [Ident] NewConstrDecl [Ident]
>   | TypeDecl       Position Ident [Ident] TypeExpr
>   | TypeSig        Position [Ident] TypeExprWithContext
>   | EvalAnnot      Position [Ident] EvalAnnotation
>   | FunctionDecl   Position Ident [Equation]
>   | PatternDecl    Position ConstrTerm Rhs
>   | ExternalDecl   Position CallConv (Maybe String) Ident TypeExpr
>   | ExtraVariables Position [Ident]
>   deriving (Eq,Show)

> data ConstrDecl =
>     ConstrDecl Position [Ident] Ident [TypeExpr]
>   | ConOpDecl Position [Ident] TypeExpr Ident TypeExpr
>   deriving (Eq,Show)
> data NewConstrDecl =
>   NewConstrDecl Position [Ident] Ident TypeExpr
>   deriving (Eq,Show)

> type Qualified = Bool
> data Infix = InfixL | InfixR | Infix deriving (Eq,Show)
> data EvalAnnotation = EvalRigid | EvalChoice deriving (Eq,Show)
> data CallConv = CallConvPrimitive | CallConvCCall deriving (Eq,Show)

\end{lstlisting}
\codeparagraph{Module interfaces}
Interface declarations are restricted to type declarations and signatures.
\begin{lstlisting}

> data Interface = Interface ModuleIdent [IDecl] deriving (Eq,Show)

> data IDecl =
>     IImportDecl    Position ModuleIdent
>   | IInfixDecl     Position Infix Int QualIdent
>   | ITypeClassDecl Position TypeExprContext QualIdent Ident [IDecl]
>   | IInstanceDecl  Position TypeExprContext QualIdent TypeExpr
>   | HidingDataDecl Position Ident [Ident] 
>   | IDataDecl      Position QualIdent [Ident] [Maybe ConstrDecl]
>   | INewtypeDecl   Position QualIdent [Ident] NewConstrDecl
>   | ITypeDecl      Position QualIdent [Ident] TypeExpr
>   | IKindDecl      Position QualIdent Kind
>   | IFunctionDecl  Position QualIdent TypeExprWithContext
>   deriving (Eq,Show)

\end{lstlisting}
\codeparagraph{Functions}
\begin{lstlisting}

> data Equation = Equation Position Lhs Rhs deriving (Eq,Show)
> data Lhs =
>     FunLhs Ident [ConstrTerm]
>   | OpLhs ConstrTerm Ident ConstrTerm
>   | ApLhs Lhs [ConstrTerm]
>   deriving (Eq,Show)
> data Rhs =
>     SimpleRhs Position Expression [Decl]
>   | GuardedRhs [CondExpr] [Decl]
>   deriving (Eq,Show)
> data CondExpr = CondExpr Position Expression Expression deriving (Eq,Show)

> flatLhs :: Lhs -> (Ident,[ConstrTerm])
> flatLhs lhs = flat lhs []
>   where flat (FunLhs f ts) ts' = (f,ts ++ ts')
>         flat (OpLhs t1 op t2) ts = (op,t1:t2:ts)
>         flat (ApLhs lhs ts) ts' = flat lhs (ts ++ ts')

\end{lstlisting}
\codeparagraph{Literals} The \texttt{Ident} argument of an \texttt{Int}
literal is used for supporting ad-hoc polymorphism on integer
numbers. An integer literal can be used either as an integer number or
as a floating-point number depending on its context. The compiler uses
the identifier of the \texttt{Int} literal for maintaining its type.
\begin{lstlisting}

> data Literal =
>     Char Char                         -- should be Int to handle Unicode
>   | Int Ident Int
>   | Float Double
>   | String String                     -- should be [Int] to handle Unicode
>   deriving (Eq,Show)

\end{lstlisting}
\codeparagraph{Patterns}
\begin{lstlisting}

> data ConstrTerm =
>     LiteralPattern Literal
>   | NegativePattern Ident Literal
>   | VariablePattern Ident
>   | ConstructorPattern QualIdent [ConstrTerm]
>   | InfixPattern ConstrTerm QualIdent ConstrTerm
>   | ParenPattern ConstrTerm
>   | TuplePattern [ConstrTerm]
>   | ListPattern [ConstrTerm]
>   | AsPattern Ident ConstrTerm
>   | LazyPattern ConstrTerm
>   deriving (Eq,Show)

\end{lstlisting}
\codeparagraph{Expressions}
\begin{lstlisting}

> data Expression =
>     Literal Literal
>   | Variable QualIdent
>   | Constructor QualIdent
>   | Paren Expression
>   | Typed Expression TypeExprWithContext
>   | Tuple [Expression]
>   | List [Expression]
>   | ListCompr Expression [Statement]
>   | EnumFrom Expression
>   | EnumFromThen Expression Expression
>   | EnumFromTo Expression Expression
>   | EnumFromThenTo Expression Expression Expression
>   | UnaryMinus Ident Expression
>   | Apply Expression Expression
>   | InfixApply Expression InfixOp Expression
>   | LeftSection Expression InfixOp
>   | RightSection InfixOp Expression
>   | Lambda [ConstrTerm] Expression
>   | Let [Decl] Expression
>   | Do [Statement] Expression
>   | IfThenElse Expression Expression Expression
>   | Case Expression [Alt]
>   | OverloadedVariablePH QualIdent Type   -- internal use
>   deriving (Eq,Show)

> data InfixOp = InfixOp QualIdent | InfixConstr QualIdent deriving (Eq,Show)

> data Statement =
>     StmtExpr Expression
>   | StmtDecl [Decl]
>   | StmtBind ConstrTerm Expression
>   deriving (Eq,Show)

> data Alt = Alt Position ConstrTerm Rhs deriving (Eq,Show)

> opName :: InfixOp -> QualIdent
> opName (InfixOp op) = op
> opName (InfixConstr c) = c

\end{lstlisting}
\codeparagraph{Goals}
A goal is equivalent to an unconditional right hand side of an equation.
\begin{lstlisting}

> data Goal = Goal Position Expression [Decl] deriving (Eq,Show)

\end{lstlisting}
Here is a list of predicates identifying various kinds of
declarations.
\begin{lstlisting}

> isImportDecl, isInfixDecl, isTypeDecl :: Decl -> Bool
> isTypeSig, isEvalAnnot, isExtraVariables, isValueDecl :: Decl -> Bool
> isTypeClassDecl :: Decl -> Bool
> isImportDecl (ImportDecl _ _ _ _ _) = True
> isImportDecl _ = False
> isInfixDecl (InfixDecl _ _ _ _) = True
> isInfixDecl _ = False
> isTypeDecl (DataDecl _ _ _ _ _) = True
> isTypeDecl (NewtypeDecl _ _ _ _ _) = True
> isTypeDecl (TypeDecl _ _ _ _) = True
> isTypeDecl _ = False
> isTypeSig (TypeSig _ _ _) = True
> isTypeSig (ExternalDecl _ _ _ _ _) = True
> isTypeSig _ = False
> isEvalAnnot (EvalAnnot _ _ _) = True
> isEvalAnnot _ = False
> isExtraVariables (ExtraVariables _ _) = True
> isExtraVariables _ = False
> isValueDecl (FunctionDecl _ _ _) = True
> isValueDecl (ExternalDecl _ _ _ _ _) = True
> isValueDecl (PatternDecl _ _ _) = True
> isValueDecl (ExtraVariables _ _) = True
> isValueDecl _ = False
> isTypeClassDecl (TypeClassDecl _ _ _ _ _) = True
> isTypeClassDecl _ = False
> isInstanceDecl (InstanceDecl _ _ _ _ _) = True
> isInstanceDecl _ = False

> isIImportDecl :: IDecl -> Bool
> isIImportDecl (IImportDecl _ _) = True
> isIImportDecl _ = False

\end{lstlisting}
The function \texttt{infixOp} converts an infix operator into an
expression.
\begin{lstlisting}

> infixOp :: InfixOp -> Expression
> infixOp (InfixOp op) = Variable op
> infixOp (InfixConstr op) = Constructor op

\end{lstlisting}
The function \texttt{declPosition} extracts the position of
a declaration.
\begin{lstlisting}

> declPosition :: Decl -> Position
> declPosition (ImportDecl p _ _ _ _) = p
> declPosition (InfixDecl p _ _ _ ) = p
> declPosition (TypeClassDecl p _ _ _ _) = p
> declPosition (InstanceDecl p _ _ _ _) = p
> declPosition (DataDecl p _ _ _ _) = p
> declPosition (NewtypeDecl p _ _ _ _) = p
> declPosition (TypeDecl p _ _ _) = p
> declPosition (TypeSig p _ _) = p
> declPosition (EvalAnnot p _ _) = p
> declPosition (FunctionDecl p _ _) = p
> declPosition (ExternalDecl p _ _ _ _) = p
> declPosition (PatternDecl p _ _) = p
> declPosition (ExtraVariables p _) = p

\end{lstlisting}
