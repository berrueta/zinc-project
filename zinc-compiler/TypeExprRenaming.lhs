% $Id: TypeExprRenaming.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeExprRenaming.lhs}
\codesection{Type expression variables renaming}
Before performing kind inference, type variables should be renamed
when they share an identifier but have different scopes.
\begin{lstlisting}

> module TypeExprRenaming where
> import TypeExpr
> import Ident
> import TypeExprSubst
> import CurrySyntax
> import List
> import Monad
> import CurrySyntaxUtils
> import Error(internalError)
> import RenameState

\end{lstlisting}
This function is the main entry point to the module. First, substitutes
the user-provided idents of all type expression variables by
compiler-generated idents. Then, it removes all
the anonymous type variables.
\begin{lstlisting}

> typeExprRenaming :: HasTypeExprVariables a => a -> a
> typeExprRenaming = typeExprRenamingShared idSubst

> typeExprRenamingShared :: HasTypeExprVariables a => TypeExprSubst -> a -> a
> typeExprRenamingShared sigma x = fst (runRenameState (renameTypeVars sigma x) 0)

\end{lstlisting}
Generation of new type expression variables.
\begin{lstlisting}

> newTypeExprVariable :: RenameState TypeExpr
> newTypeExprVariable =
>   newId >>= return . TypeExprVariable . enumIdent

> enumIdent :: Int -> Ident
> enumIdent n = renameIdent (mkIdent ("_anon" ++ (show n))) n

\end{lstlisting}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\codesubsection{Renaming type expression variables}

The \texttt{newRenameSubst}
function builds a new substitution for the variables passed as the
first argument.
\begin{lstlisting}

> newRenameSubst :: [Ident] -> Int -> TypeExprSubst
> newRenameSubst tvs n =
>   foldr (\tv -> if isAnonId tv
>                    then id
>                    else bindTypeExprVar tv (TypeExprVariable (renameIdent tv n)))
>         idSubst tvs

\end{lstlisting}
The objetive of the function \texttt{renameTypeVars} is to
rewrite a syntactic element, substituting all the type variable
identifiers with brand new internal identifiers (except those
variables whose identifiers are passed in the first argument; this is
used to prevent renaming of shared type variables in the different
methods of a type class declaration).

It returns a tuple containing the renamed element and a type
expression substitution which is the inverse of the one that was
used to perform the substitution, i.e., applying the returned
substitution on the returned element must restore the
original element.
\begin{lstlisting}

> class HasTypeExprVariables a where
>   renameTypeVars :: TypeExprSubst -> a -> RenameState a

> instance HasTypeExprVariables IDecl where
>   renameTypeVars sigma (IFunctionDecl p f tyexprWC) =
>     do n <- newId
>        let sigma' = sigma `compose` (newRenameSubst (tvs \\ sharedTvs) n)
>        tyexprWC' <- renameTypeVars sigma' tyexprWC
>        return (IFunctionDecl p f tyexprWC')
>     where tvs = nub (tvUsedBy tyexprWC)
>           sharedTvs = map fst (substToList sigma)
>
>   renameTypeVars _ (IInfixDecl p fix n f) =
>     return (IInfixDecl p fix n f)

> instance HasTypeExprVariables Decl where
>   renameTypeVars _ (TypeClassDecl p ctx tc tv decls) =
>     do n <- newId
>        let sigma = newRenameSubst [tv] n
>        ctx'   <- renameTypeVars sigma ctx
>        tv'    <- renameTypeVars sigma tv
>        decls' <- renameTypeVars sigma decls
>        return (TypeClassDecl p ctx' tc tv' decls')
>
>   renameTypeVars _ (InstanceDecl p ctx tc tyexpr decls) =
>     do n <- newId
>        let sigma = newRenameSubst tvs n
>        ctx'    <- renameTypeVars sigma ctx
>        tyexpr' <- renameTypeVars sigma tyexpr
>        decls'  <- renameTypeVars idSubst decls
>        return (InstanceDecl p ctx' tc tyexpr' decls')
>     where tvs = nub (tvUsedBy tyexpr)
>
>   renameTypeVars _ (DataDecl p tc tvs constrs derivedTC) =
>     do n <- newId
>        let sigma = newRenameSubst tvs n
>        tvs'     <- renameTypeVars sigma tvs
>        constrs' <- renameTypeVars sigma constrs
>        return (DataDecl p tc tvs' constrs' derivedTC)
>
>   renameTypeVars _ (NewtypeDecl p tc tvs constr derivedTC) =
>     do n <- newId
>        let sigma = newRenameSubst tvs n
>        tvs'    <- renameTypeVars sigma tvs
>        constr' <- renameTypeVars sigma constr
>        return (NewtypeDecl p tc tvs' constr' derivedTC)
>
>   renameTypeVars _ (TypeDecl p tc tvs tyexpr) =
>     do n <- newId
>        let sigma = newRenameSubst tvs n
>        tvs'    <- renameTypeVars sigma tvs
>        tyexpr' <- renameTypeVars sigma tyexpr
>        return (TypeDecl p tc tvs' tyexpr')
>
>   renameTypeVars sigma (TypeSig p ids tyexpr) =
>     do n <- newId
>        let sigma' = sigma `compose` (newRenameSubst (tvs \\ sharedTvs) n)
>        tyexpr' <- renameTypeVars sigma' tyexpr
>        return (TypeSig p ids tyexpr')
>     where tvs = nub (tvUsedBy tyexpr)
>           sharedTvs = map fst (substToList sigma)
>
>   renameTypeVars _ (FunctionDecl p i eqs) =
>     do eqs' <- renameTypeVars idSubst eqs
>        return (FunctionDecl p i eqs')
>
>   renameTypeVars _ (ExternalDecl p cc s id tyexpr) =
>     do n <- newId
>        let sigma = newRenameSubst tvs n
>        tyexpr' <- renameTypeVars sigma tyexpr
>        return (ExternalDecl p cc s id tyexpr')
>     where tvs = nub (tvUsedBy tyexpr)
>
>   renameTypeVars _ decl@(PatternDecl p constr rhs) =
>     do rhs' <- renameTypeVars idSubst rhs
>        return (PatternDecl p constr rhs')
>
>   renameTypeVars _ decl@(ImportDecl _ _ _ _ _) =
>     return decl
>   renameTypeVars _ decl@(InfixDecl _ _ _ _) =
>     return decl
>   renameTypeVars _ decl@(EvalAnnot _ _ _) =
>     return decl
>   renameTypeVars _ decl@(ExtraVariables _ _) =
>     return decl

> instance HasTypeExprVariables ConstrDecl where
>   renameTypeVars sigma (ConstrDecl p ids c tyexprs) =
>     do ids'     <- renameTypeVars sigma ids
>        tyexprs' <- renameTypeVars sigma tyexprs
>        return (ConstrDecl p ids' c tyexprs')
>   renameTypeVars sigma (ConOpDecl p ids tyexpr1 op tyexpr2) =
>     do ids'     <- renameTypeVars sigma ids
>        tyexpr1' <- renameTypeVars sigma tyexpr1
>        tyexpr2' <- renameTypeVars sigma tyexpr2
>        return (ConOpDecl p ids' tyexpr1' op tyexpr2)

> instance HasTypeExprVariables NewConstrDecl where
>   renameTypeVars sigma (NewConstrDecl p ids c tyexpr) =
>     do ids'    <- renameTypeVars sigma ids
>        tyexpr' <- renameTypeVars sigma tyexpr
>        return (NewConstrDecl p ids' c tyexpr')

> instance HasTypeExprVariables Equation where
>   renameTypeVars _ (Equation p constrs rhs) =
>     do rhs' <- renameTypeVars idSubst rhs
>        return (Equation p constrs rhs')

> instance HasTypeExprVariables Rhs where
>   renameTypeVars _ (SimpleRhs p expr decls) =
>     do expr'  <- renameTypeVars idSubst expr
>        decls' <- renameTypeVars idSubst decls
>        return (SimpleRhs p expr' decls')
>   renameTypeVars _ (GuardedRhs conds decls) =
>     do conds' <- renameTypeVars idSubst conds
>        decls' <- renameTypeVars idSubst decls
>        return (GuardedRhs conds' decls')

> instance HasTypeExprVariables CondExpr where
>   renameTypeVars _ (CondExpr p expr1 expr2) =
>     do expr1' <- renameTypeVars idSubst expr1
>        expr2' <- renameTypeVars idSubst expr2
>        return (CondExpr p expr1' expr2')

> instance HasTypeExprVariables Expression where
>   renameTypeVars _ (Literal li) =
>     return (Literal li)
>   renameTypeVars _ (Variable qid) =
>     return (Variable qid)
>   renameTypeVars _ (Constructor qid) =
>     return (Constructor qid)
>   renameTypeVars _ (Paren expr) =
>     do expr' <- renameTypeVars idSubst expr
>        return (Paren expr')
>   renameTypeVars _ (Typed expr tyexpr) =
>     do expr'  <- renameTypeVars idSubst expr
>        n      <- newId
>        let sigma  = newRenameSubst (nub (tvUsedBy tyexpr)) n
>        tyexpr' <- renameTypeVars sigma tyexpr
>        return (Typed expr' tyexpr')
>   renameTypeVars _ (Tuple exprs) =
>     do exprs' <- renameTypeVars idSubst exprs
>        return (Tuple exprs')
>   renameTypeVars _ (List exprs) =
>     do exprs' <- renameTypeVars idSubst exprs
>        return (List exprs')
>   renameTypeVars _ (ListCompr expr stats) =
>     do expr'  <- renameTypeVars idSubst expr
>        stats' <- renameTypeVars idSubst stats
>        return (ListCompr expr' stats')
>   renameTypeVars _ (EnumFrom expr) =
>     do expr' <- renameTypeVars idSubst expr
>        return (EnumFrom expr')
>   renameTypeVars _ (EnumFromThen expr1 expr2) =
>     do expr1' <- renameTypeVars idSubst expr1
>        expr2' <- renameTypeVars idSubst expr2
>        return (EnumFromThen expr1' expr2')
>   renameTypeVars _ (EnumFromTo expr1 expr2) =
>     do expr1' <- renameTypeVars idSubst expr1
>        expr2' <- renameTypeVars idSubst expr2
>        return (EnumFromTo expr1' expr2')
>   renameTypeVars _ (EnumFromThenTo expr1 expr2 expr3) =
>     do expr1' <- renameTypeVars idSubst expr1
>        expr2' <- renameTypeVars idSubst expr2
>        expr3' <- renameTypeVars idSubst expr3
>        return (EnumFromThenTo expr1' expr2' expr3')
>   renameTypeVars _ (UnaryMinus id expr) =
>     do expr' <- renameTypeVars idSubst expr
>        return (UnaryMinus id expr')
>   renameTypeVars sharedTvs (Apply expr1 expr2) =
>     do expr1' <- renameTypeVars idSubst expr1
>        expr2' <- renameTypeVars idSubst expr2
>        return (Apply expr1' expr2')
>   renameTypeVars _ (InfixApply expr1 op expr2) =
>     do expr1' <- renameTypeVars idSubst expr1
>        expr2' <- renameTypeVars idSubst expr2
>        return (InfixApply expr1' op expr2')
>   renameTypeVars _ (LeftSection expr op) =
>     do expr' <- renameTypeVars idSubst expr
>        return (LeftSection expr' op)
>   renameTypeVars _ (RightSection op expr) =
>     do expr' <- renameTypeVars idSubst expr
>        return (RightSection op expr')
>   renameTypeVars _ (Lambda constrs expr) =
>     do expr' <- renameTypeVars idSubst expr
>        return (Lambda constrs expr')
>   renameTypeVars _ (Let decls expr) =
>     do decls' <- renameTypeVars idSubst decls
>        expr'  <- renameTypeVars idSubst expr
>        return (Let decls' expr')
>   renameTypeVars _ (Do stats expr) =
>     do stats' <- renameTypeVars idSubst stats
>        expr'  <- renameTypeVars idSubst expr
>        return (Do stats' expr')
>   renameTypeVars _ (IfThenElse expr1 expr2 expr3) =
>     do expr1' <- renameTypeVars idSubst expr1
>        expr2' <- renameTypeVars idSubst expr2
>        expr3' <- renameTypeVars idSubst expr3
>        return (IfThenElse expr1' expr2' expr3')
>   renameTypeVars _ (Case expr alts) =
>     do expr' <- renameTypeVars idSubst expr
>        alts' <- renameTypeVars idSubst alts
>        return (Case expr' alts')

> instance HasTypeExprVariables Statement where
>   renameTypeVars _ (StmtExpr expr) =
>     do expr' <- renameTypeVars idSubst expr
>        return (StmtExpr expr')
>   renameTypeVars _ (StmtDecl decls) =
>     do decls' <- renameTypeVars idSubst decls
>        return (StmtDecl decls)
>   renameTypeVars _ (StmtBind constr expr) =
>     do expr' <- renameTypeVars idSubst expr
>        return (StmtBind constr expr')

> instance HasTypeExprVariables Alt where
>   renameTypeVars _ (Alt p constr rhs) =
>     do rhs' <- renameTypeVars idSubst rhs
>        return (Alt p constr rhs')

> instance HasTypeExprVariables Goal where
>   renameTypeVars _ (Goal p expr decls) =
>     do expr'  <- renameTypeVars idSubst expr
>        decls' <- renameTypeVars idSubst decls
>        return (Goal p expr' decls')

> instance HasTypeExprVariables a => HasTypeExprVariables [a] where
>   renameTypeVars sigma xs = mapM (renameTypeVars sigma) xs

> instance HasTypeExprVariables TypeExpr where
>   renameTypeVars sigma tyexpr@(TypeExprVariable tv)
>     | isAnonId tv = newTypeExprVariable
>     | otherwise = return (substTypeExpr sigma tyexpr)
>   renameTypeVars sigma tyexpr@(TypeExprConstructor _) = return tyexpr
>   renameTypeVars sigma tyexpr@(TypeExprApplication tyexpr1 tyexpr2) =
>     do tyexpr1' <- renameTypeVars sigma tyexpr1
>        tyexpr2' <- renameTypeVars sigma tyexpr2
>        return (TypeExprApplication tyexpr1' tyexpr2')

> instance HasTypeExprVariables TypeExprContext where
>   renameTypeVars sigma tyexprCtx = return (substTypeExpr sigma tyexprCtx)

> instance HasTypeExprVariables TypeExprWithContext where
>   renameTypeVars sigma (TypeExprWithContext ctx tyexpr) =
>     do ctx'    <- renameTypeVars sigma ctx
>        tyexpr' <- renameTypeVars sigma tyexpr
>        return (TypeExprWithContext ctx' tyexpr')

> instance HasTypeExprVariables Ident where
>   renameTypeVars sigma tv =
>     do let tyexpr = TypeExprVariable tv
>        tyexpr' <- renameTypeVars sigma tyexpr
>        return (head (tvUsedBy tyexpr'))

\end{lstlisting}
