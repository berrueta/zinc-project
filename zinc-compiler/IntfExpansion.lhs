% $Id: IntfExpansion.lhs 1328 2004-09-05 17:56:35Z berrueta $
%
% Copyright (c) 2000-2003, Wolfgang Lux
% Copyright (c) 2003-2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{IntfExpansion.lhs}
\codesection{Expanding the interface}
\begin{lstlisting}

> module IntfExpansion where
> import Base
> import CurrySyntax
> import TypeConstructorEnv
> import TypeClassEnv
> import ValueEnv
> import Set
> import Ident
> import Position
> import Map
> import Maybe
> import Error(errorAt)
> import TopEnv
> import List

\end{lstlisting}
The function
\texttt{expandInterface} checks the export specifications of the
module and expands them into a list containing all exported types and
functions, combining multiple exports for the same entity. The
expanded export specifications refer to the original names of all
entities.
\begin{lstlisting}

> expandInterface :: ModuleIdent -> Maybe ExportSpec -> [Decl] ->
>                    TypeClassEnv -> TCEnv -> ValueEnv ->
>                    Maybe ExportSpec
> expandInterface m es ds typeClassEnv tcEnv tyEnv =
>   Just $ Exporting genPos $ checkExportSpecification es'
>   where ms = fromListSet [fromMaybe m asM | ImportDecl _ m _ asM _ <- ds]
>         es' = joinExports $                                              -- $
>               maybe (expandLocalModule typeClassEnv tcEnv tyEnv)
>                     (expandSpecs ms m typeClassEnv tcEnv tyEnv)
>                     es

> checkExportSpecification :: [Export] -> [Export]
> checkExportSpecification es =
>   case linear [unqualify tc | ExportTypeWith tc _ <- es] of
>     Linear ->
>       case linear ([c | ExportTypeWith _ cs <- es, c <- cs] ++
>                    [unqualify f | Export f <- es]) of
>         Linear -> es
>         NonLinear v -> error (ambiguousExportValue v)
>     NonLinear tc -> error (ambiguousExportType tc)


\end{lstlisting}
While checking all export specifications, the compiler expands
specifications of the form \verb|T(..)| into
\texttt{T($C_1,\dots,C_n$)}, where $C_1,\dots,C_n$ are the data
constructors of type \texttt{T}, and replaces an export specification
\verb|module M| by specifications for all entities which are defined
in module \texttt{M} and imported into the current module with their
unqualified name. In order to distinguish exported type constructors
from exported functions, the former are translated into the equivalent
form \verb|T()|. Note that the export specification \texttt{x} may
export a type constructor \texttt{x} \emph{and} a global function
\texttt{x} at the same time.
\begin{lstlisting}

> expandSpecs :: Set ModuleIdent -> ModuleIdent ->
>                TypeClassEnv -> TCEnv -> ValueEnv ->
>                ExportSpec -> [Export]
> expandSpecs ms m typeClassEnv tcEnv tyEnv (Exporting p es) =
>   concatMap (expandExport p ms m methods typeClassEnv tcEnv tyEnv) es
>   where methods = allMethods typeClassEnv

> expandExport :: Position -> Set ModuleIdent -> ModuleIdent ->
>                 [QualIdent] -> TypeClassEnv -> TCEnv -> ValueEnv ->
>                 Export -> [Export]
> expandExport p _ _ methods _ tcEnv tyEnv (Export x) =
>   expandThing p methods tcEnv tyEnv x
> expandExport p _ _ _ _ tcEnv _ (ExportTypeWith tc cs) =
>   [expandTypeWith p tcEnv tc cs]
> expandExport p _ _ _ _ tcEnv _ (ExportTypeAll tc) =
>   [expandTypeAll p tcEnv tc]
> expandExport p ms m _ typeClassEnv tcEnv tyEnv (ExportModule m')
>   | m == m' = (if m `elemSet` ms
>                   then expandModule typeClassEnv tcEnv tyEnv m
>                   else [])
>               ++ expandLocalModule typeClassEnv tcEnv tyEnv
>   | m' `elemSet` ms = expandModule typeClassEnv tcEnv tyEnv m'
>   | otherwise = errorAt p (moduleNotImported m')
> expandExport p _ _ _ typeClassEnv _ _ (ExportTypeClassWith tc mets) =
>   [expandTypeClassWith p typeClassEnv tc mets]
> expandExport p _ _ _ typeClassEnv _ _ (ExportTypeClassAll tc) =
>   [expandTypeClassAll p typeClassEnv tc]

> expandThing :: Position -> [QualIdent] -> TCEnv -> ValueEnv ->
>                QualIdent -> [Export]
> expandThing p methods tcEnv tyEnv tc =
>   case qualLookupTC tc tcEnv of
>     [] -> expandThing' p methods tyEnv tc Nothing
>     [t] -> expandThing' p methods tyEnv tc
>                         (Just [ExportTypeWith (origName t) []])
>     _ -> errorAt p (ambiguousType tc)

> expandThing' :: Position -> [QualIdent] -> ValueEnv -> QualIdent ->
>                 Maybe [Export] -> [Export]
> expandThing' p methods tyEnv f tcExport =
>   case qualLookupValue f tyEnv of
>     [] -> fromMaybe (errorAt p (undefinedEntity f)) tcExport
>     [Value f' _]
>       | f' `notElem` methods -> Export f' : fromMaybe [] tcExport
>       | otherwise            -> errorAt p (cannotExportMethodAsFunction f)
>     [_] -> fromMaybe (errorAt p (exportDataConstr f)) tcExport
>     _ -> errorAt p (ambiguousName f)

> expandTypeWith :: Position -> TCEnv -> QualIdent -> [Ident] -> Export
> expandTypeWith p tcEnv tc cs =
>   case qualLookupTC tc tcEnv of
>     [] -> errorAt p (undefinedType tc)
>     [t]
>       | isDataType t -> ExportTypeWith (origName t)
>                           (map (checkConstr (constrs t)) (nub cs))
>       | otherwise -> errorAt p (nonDataType tc)
>     _ -> errorAt p (ambiguousType tc)
>   where checkConstr cs c
>           | c `elem` cs = c
>           | otherwise = errorAt p (undefinedDataConstr tc c)

> expandTypeAll :: Position -> TCEnv -> QualIdent -> Export
> expandTypeAll p tcEnv tc =
>   case qualLookupTC tc tcEnv of
>     [] -> errorAt p (undefinedType tc)
>     [t]
>       | isDataType t -> exportType t 
>       | otherwise -> errorAt p (nonDataType tc)
>     _ -> errorAt p (ambiguousType tc)

> expandTypeClassWith :: Position -> TypeClassEnv -> QualIdent -> [Ident] -> Export
> expandTypeClassWith p typeClassEnv tc mets =
>   case qualLookupTypeClass tc typeClassEnv of
>     []    -> errorAt p (undefinedTypeClass tc)
>     [tci] -> ExportTypeClassWith (origName tci)
>                (map (checkMethod (typeClassMethods tci)) (nub mets))
>   where checkMethod ms m
>           | m `elem` ms = m
>           | otherwise = errorAt p (undefinedMethod tc m)

> expandTypeClassAll :: Position -> TypeClassEnv -> QualIdent -> Export
> expandTypeClassAll p typeClassEnv tc =
>   case qualLookupTypeClass tc typeClassEnv of
>     []    -> errorAt p (undefinedTypeClass tc)
>     [tci] -> exportTypeClass tci

> expandLocalModule :: TypeClassEnv -> TCEnv -> ValueEnv -> [Export]
> expandLocalModule typeClassEnv tcEnv tyEnv =
>   [exportTypeClass tci | (_,tci) <- localBindings typeClassEnv] ++
>   [exportType t        | (i,t)   <- localBindings tcEnv, not (isInternalId i)] ++
>   [Export f'           | (f,Value f' _) <- localBindings tyEnv,
>                          f == unRenameIdent f,
>                          not (isInternalId f),
>                          f' `notElem` methods]
>   where methods = allMethods typeClassEnv

> expandModule :: TypeClassEnv -> TCEnv -> ValueEnv -> ModuleIdent -> [Export]
> expandModule typeClassEnv tcEnv tyEnv m =
>   [exportTypeClass tci | (_,tci) <- moduleImports m typeClassEnv] ++
>   [exportType t | (_,t) <- moduleImports m tcEnv] ++
>   [Export f | (_,Value f _) <- moduleImports m tyEnv,
>                                f `notElem` methods]
>   where methods = allMethods typeClassEnv

> exportTypeClass :: TypeClassInfo -> Export
> exportTypeClass tci = ExportTypeClassWith (origName tci) (typeClassMethods tci)

> exportType :: TypeInfo -> Export
> exportType t = ExportTypeWith (origName t) (constrs t)

\end{lstlisting}
The expanded list of exported entities may contain duplicates. These
are removed by the function \texttt{joinExports}.
\begin{lstlisting}

> joinExports :: [Export] -> [Export]
> joinExports es =
>   [ExportTypeWith tc cs | (tc,cs) <- toListFM  (foldr joinType zeroFM  es)] ++
>   [Export f | f <- toListSet (foldr joinFun zeroSet es)] ++
>   [ExportTypeClassWith tc ms | (tc,ms) <- toListFM (foldr joinTypeClass zeroFM es)]

> joinType :: Export -> FM QualIdent [Ident] -> FM QualIdent [Ident]
> joinType (Export              _    ) tcs = tcs
> joinType (ExportTypeWith      tc cs) tcs =
>   addToFM tc (cs `union` fromMaybe [] (lookupFM tc tcs)) tcs
> joinType (ExportTypeClassWith _ _  ) tcs = tcs

> joinFun :: Export -> Set QualIdent -> Set QualIdent
> joinFun (Export              f  ) fs  = f `addToSet` fs
> joinFun (ExportTypeWith      _ _) fs  = fs
> joinFun (ExportTypeClassWith _ _) tcs = tcs

> joinTypeClass :: Export -> FM QualIdent [Ident] -> FM QualIdent [Ident]
> joinTypeClass (Export              _  ) tcs = tcs
> joinTypeClass (ExportTypeWith      _ _) tcs = tcs
> joinTypeClass (ExportTypeClassWith tc ms) tcs =
>   addToFM tc (ms `union` fromMaybe [] (lookupFM tc tcs)) tcs

\end{lstlisting}
Auxiliary definitions
\begin{lstlisting}

> isDataType :: TypeInfo -> Bool
> isDataType (DataType _ _ _) = True
> isDataType (RenamingType _ _ _) = True
> isDataType (AliasType _ _ _) = False

> constrs :: TypeInfo -> [Ident]
> constrs (DataType _ _ cs) = [c | Just (Data c _ _) <- cs]
> constrs (RenamingType _ _ (Data c _ _)) = [c]
> constrs (AliasType _ _ _) = []

\end{lstlisting}
Error messages
\begin{lstlisting}

> undefinedEntity :: QualIdent -> String
> undefinedEntity x =
>   "Entity " ++ qualName x ++ " in export list is not defined"

> undefinedType :: QualIdent -> String
> undefinedType tc = "Type " ++ qualName tc ++ " in export list is not defined"

> undefinedTypeClass :: QualIdent -> String
> undefinedTypeClass tc = "Type class " ++ qualName tc ++
>                         " in export list is not defined"

> moduleNotImported :: ModuleIdent -> String
> moduleNotImported m = "Module " ++ moduleName m ++ " not imported"

> ambiguousExportType :: Ident -> String
> ambiguousExportType x = "Ambiguous export of type " ++ name x

> ambiguousExportValue :: Ident -> String
> ambiguousExportValue x = "Ambiguous export of " ++ name x

> ambiguousType :: QualIdent -> String
> ambiguousType tc = "Ambiguous type " ++ qualName tc

> ambiguousName :: QualIdent -> String
> ambiguousName x = "Ambiguous name " ++ qualName x

> exportDataConstr :: QualIdent -> String
> exportDataConstr c = "Data constructor " ++ qualName c ++ " in export list"

> nonDataType :: QualIdent -> String
> nonDataType tc = qualName tc ++ " is not a data type"

> undefinedDataConstr :: QualIdent -> Ident -> String
> undefinedDataConstr tc c =
>   name c ++ " is not a data constructor of type " ++ qualName tc

> undefinedMethod :: QualIdent -> Ident -> String
> undefinedMethod tc m =
>   name m ++ " is not a method of type class " ++ qualName tc

> cannotExportMethodAsFunction :: QualIdent -> String
> cannotExportMethodAsFunction m =
>   "Export specification for method " ++ qualName m ++
>   " should be inside of its type class specification"

\end{lstlisting}
