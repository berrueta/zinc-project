% $Id: TypeCheck.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2004, Wolfgang Lux
% Copyright (c) 2003-2005, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeCheck.lhs}
\codesection{Type Checking Curry Programs}
This module implements the type checker of the Curry compiler. The
type checker is invoked after the syntactic correctness of the program
has been verified. Local variables have been renamed already. Thus the
compiler can maintain a flat type environment (which is necessary in
order to pass the type information to later phases of the compiler).
The type checker now checks the correct typing of all expressions and
also verifies that the type signatures given by the user match the
inferred types. The type checker uses algorithm
W~\cite{DamasMilner82:Principal} for inferring the types of
unannotated declarations, but allows for polymorphic recursion when a
type annotation is present.
\begin{lstlisting}

> module TypeCheck where
> import Ident
> import CurrySyntax
> import TypeExpr
> import ValueEnv
> import TypeConstructorEnv
> import Position
> import Types
> import TypeTrans
> import Pretty
> import CurryPP
> import Env
> import TopEnv
> import Set
> import Combined
> import SCC
> import TypeSubst
> import Utils
> import List
> import Monad
> import Maybe
> import Error(errorAt,internalError)
> import Expr
> import TypeInferenceMonad
> import TypeErrors
> import TypeUnification
> import TypeInstGen
> import SigEnv
> import TypeExpansion
> import TypeClassEnv
> import InstanceEnv
> import DeclGen
> import TypeCheckUtils
> import ContextReduction(contextReduction)
> import AliasCheck

> infixl 5 $-$

> ($-$) :: Doc -> Doc -> Doc
> x $-$ y = x $$ space $$ y

\end{lstlisting}
Prior to type checking, the type constructor environment has been
initialized by adding all types defined in the current
module, and the types of all data constructors have been entered into
the type environment. Then a type inference for all function and value
definitions is performed. The type checker returns the resulting type
constructor and type environments.
\begin{lstlisting}

> typeCheck :: ModuleIdent -> TypeClassEnv -> InstanceEnv -> TCEnv -> ValueEnv ->
>              [Decl] -> ValueEnv
> typeCheck m typeClassEnv instEnv tcEnv tyEnv ds =
>   run (do tcDecls True m typeClassEnv instEnv tcEnv emptyEnv ds
>           theta  <- liftSt fetchSt
>           tyEnv' <- fetchSt
>           return (subst theta tyEnv'))
>       tyEnv

\end{lstlisting}
Type checking of a goal expression is simpler because the type
constructor environment is fixed already and there are no
type declarations in a goal.
\begin{lstlisting}

> typeCheckGoal :: TypeClassEnv -> InstanceEnv -> TCEnv -> ValueEnv -> Goal ->
>                  (ValueEnv,TypeWithContext)
> typeCheckGoal typeClassEnv instEnv tcEnv tyEnv (Goal p e ds) =
>    run (do tyWC <- tcRhs m0 typeClassEnv instEnv tcEnv tyEnv emptyEnv
>                          (SimpleRhs p e ds)
>            theta  <- liftSt fetchSt
>            tyEnv' <- fetchSt
>            let tyWC'   = mapTypeContext (contextReduction p typeClassEnv instEnv)
>                                         tyWC
>                tccs    = typeClassConstraints (typeContext tyWC')
>                tyEnv'' = if (tccs == [])
>                             then subst theta tyEnv'
>                             else errorAt p (unresolvedOverloading what tccs)
>            return (tyEnv'',tyWC'))
>        tyEnv
>   where m0 = mkMIdent []
>         what = text "Goal"

\end{lstlisting}
\codeparagraph{Type Inference}
Before type checking a group of declarations, a dependency analysis is
performed and the declaration group is eventually transformed into
nested declaration groups which are checked separately. Within each
declaration group, first the left hand sides of all declarations are
typed. Next, the right hand sides of the declarations are typed in the
extended type environment. Finally, the types for the left and right
hand sides are unified and the types of all defined functions are
generalized. The generalization step will also check that the type
signatures given by the user match the inferred types.
\begin{lstlisting}

> tcDecls :: Bool -> ModuleIdent ->
>            TypeClassEnv -> InstanceEnv -> TCEnv -> SigEnv ->
>            [Decl] -> TcState TypeContext
> tcDecls topLevel m typeClassEnv instEnv tcEnv sigs ds =
>   do ctxs <- mapM (tcDeclGroup topLevel m typeClassEnv instEnv tcEnv sigEnv)
>                   groupedDs
>      return (foldl composeTypeContext emptyTypeContext ctxs)
>   where (vds,ods) = partition isValueDecl ds
>         groupedDs = scc bv (qfv m) vds
>         sigEnv    = foldr (bindTypeSigs tcEnv) sigs ods

> tcDeclGroup :: Bool -> ModuleIdent ->
>                TypeClassEnv -> InstanceEnv -> TCEnv -> SigEnv ->
>                [Decl] -> TcState TypeContext
> tcDeclGroup _ m _ _ tcEnv _ [ExternalDecl p _ _ f ty] =
>   do tcExternalFunct m tcEnv p f ty
>      return emptyTypeContext
> tcDeclGroup _ m _ _ tcEnv sigs [ExtraVariables p vs] =
>   do mapM_ (tcExtraVar m tcEnv sigs p) vs
>      return emptyTypeContext
> tcDeclGroup topLevel m typeClassEnv instEnv tcEnv sigs ds =
>   do
>     tyEnv0 <- fetchSt
>     tysLhs <- mapM (tcDeclLhs m tcEnv        sigs) ds
>     tysRhs <- mapM (tcDeclRhs m typeClassEnv instEnv tcEnv tyEnv0 sigs) ds
>     ctxs   <- sequence (zipWith3 (unifyDecl m) ds tysLhs tysRhs)
>     theta  <- liftSt fetchSt
>     let ctxs' = subst theta ctxs
>     ctxs'' <- sequence (zipWith (genDecl topLevel m typeClassEnv instEnv tcEnv sigs
>                                      (fvEnv (subst theta tyEnv0)) theta)
>                                 ctxs' ds)
>     return (foldl composeTypeContext emptyTypeContext ctxs'')

> tcDeclLhs :: ModuleIdent -> TCEnv -> SigEnv -> Decl -> TcState TypeWithContext
> tcDeclLhs m tcEnv sigs (FunctionDecl p f _) =
>   tcConstrTerm m tcEnv sigs p (VariablePattern f)
> tcDeclLhs m tcEnv sigs (PatternDecl p t _) =
>   tcConstrTerm m tcEnv sigs p t

> tcDeclRhs :: ModuleIdent -> TypeClassEnv -> InstanceEnv -> TCEnv ->
>              ValueEnv -> SigEnv -> Decl -> TcState TypeWithContext
> tcDeclRhs m typeClassEnv instEnv tcEnv tyEnv0 sigs (FunctionDecl _ f (eq:eqs)) =
>   tcEquation m typeClassEnv instEnv tcEnv tyEnv0 sigs eq >>= flip tcEqns eqs
>   where tcEqns :: TypeWithContext -> [Equation] -> TcState TypeWithContext
>         tcEqns tyWC [] = return tyWC
>         tcEqns tyWC (eq@(Equation p _ _):eqs) =
>           do tyWC' <- tcEquation m typeClassEnv instEnv tcEnv tyEnv0 sigs eq
>              ctx   <- unifyTypeWithContext p "equation"
>                                            (ppDecl (FunctionDecl p f [eq]))
>                                            m tyWC tyWC'
>              tcEqns (appendTypeContext tyWC ctx) eqs
> tcDeclRhs m typeClassEnv instEnv tcEnv tyEnv0 sigs (PatternDecl _ _ rhs) =
>   tcRhs m typeClassEnv instEnv tcEnv tyEnv0 sigs rhs

> unifyDecl :: ModuleIdent -> Decl -> TypeWithContext -> TypeWithContext ->
>              TcState TypeContext
> unifyDecl m (FunctionDecl p f _) =
>   unifyTypeWithContext p "function binding" (text "Function:" <+> ppIdent f) m
> unifyDecl m (PatternDecl p t _) =
>   unifyTypeWithContext p "pattern binding" (ppConstrTerm 0 t) m

\end{lstlisting}
Note that external functions cannot be overloaded.
\begin{lstlisting}

> tcExternalFunct :: ModuleIdent -> TCEnv -> Position -> Ident -> TypeExpr
>                 -> TcState ()
> tcExternalFunct m tcEnv p f ty = updateSt_
>                                $ bindFun m f
>                                $ expandPolyType tcEnv
>                                $ aliasCheckTypeExprWithContext p tcEnv tyWC
>   where tyWC = TypeExprWithContext emptyTypeExprContext ty

> tcExtraVar :: ModuleIdent -> TCEnv -> SigEnv -> Position -> Ident
>            -> TcState ()
> tcExtraVar m tcEnv sigs p v =
>   typeOf v tcEnv sigs >>= updateSt_ . bindFun m v . monoType
>   where typeOf :: Ident -> TCEnv -> SigEnv -> TcState Type
>         typeOf v tcEnv sigs =
>           case lookupTypeSig v sigs of
>             Just tyWC
>               | n == 0    -> return (removeTypeContext ty')
>               | otherwise -> errorAt p (polymorphicFreeVar v)
>               where ForAll n ty' = expandPolyType tcEnv tyWC
>             Nothing -> freshTypeVar

\end{lstlisting}
Type checking of the different components of the syntax tree.
\begin{lstlisting}

> tcEquation :: ModuleIdent -> TypeClassEnv -> InstanceEnv -> TCEnv ->
>               ValueEnv -> SigEnv -> Equation -> TcState TypeWithContext
> tcEquation m typeClassEnv instEnv tcEnv tyEnv0 sigs eq@(Equation p lhs rhs) =
>   do
>     tys  <- mapM (tcConstrTerm m tcEnv sigs p) ts
>     tyWC <- tcRhs m typeClassEnv instEnv tcEnv tyEnv0 sigs rhs
>     let tyWC' = foldr typeArrowWithContext tyWC tys
>     checkSkolems p m (text "Function: " <+> ppIdent f) tyEnv0 tyWC'
>   where (f,ts) = flatLhs lhs

> tcLiteral :: ModuleIdent -> Literal -> TcState TypeWithContext
> tcLiteral _ (Char _) = return charTypeWithContext
> tcLiteral m (Int v _) = return intTypeWithContext
> -- FIXME: overloaded numeric literals
> --  do
> --    ty <- freshConstrainedWithContext [intType,floatType]
> --    updateSt_ (bindFun m v (monoTypeWithContext ty))
> --    return ty
> tcLiteral _ (Float _) = return floatTypeWithContext
> tcLiteral _ (String _) = return stringTypeWithContext
  
> tcConstrTerm :: ModuleIdent -> TCEnv -> SigEnv -> Position -> ConstrTerm
>              -> TcState TypeWithContext
> tcConstrTerm m tcEnv sigs p (LiteralPattern l) = tcLiteral m l
> tcConstrTerm m tcEnv sigs p (NegativePattern _ l) = tcLiteral m l
> tcConstrTerm m tcEnv sigs p (VariablePattern v) =
>   do
>     tyWC <-
>       case lookupTypeSig v sigs of
>         Just tyWC' -> inst (expandPolyType tcEnv tyWC')
>         Nothing    -> freshTypeVarWithContext
>     updateSt_ (bindFun m v (monoTypeWithContext tyWC))
>     return tyWC
> tcConstrTerm m tcEnv sigs p t@(ConstructorPattern c ts) =
>   do
>     tyEnv <- fetchSt
>     tyWC  <- skol (constrType c tyEnv)
>     unifyArgs (ppConstrTerm 0 t) ts tyWC
>   where unifyArgs :: Doc -> [ConstrTerm] -> TypeWithContext ->
>                      TcState TypeWithContext
>         unifyArgs _   []     tyWC = return tyWC
>         unifyArgs doc (t:ts) tyWC
>           | isArrowType ty =
>               let [tyAlpha,tyBeta] = typeArguments ty
>               in  do tyWC' <- tcConstrTerm m tcEnv sigs p t
>                      ctx'  <- unifyTypeWithContext p "pattern"
>                                 (doc $-$ text "Term:" <+> ppConstrTerm 0 t) m
>                                 (TypeWithContext ctx tyAlpha) tyWC'
>                      unifyArgs doc ts (TypeWithContext ctx' tyBeta)
>           | otherwise = internalError "tcConstrTerm"
>           where ty  = removeTypeContext tyWC
>                 ctx = typeContext tyWC
> tcConstrTerm m tcEnv sigs p t@(InfixPattern t1 op t2) =
>   do
>     tyEnv <- fetchSt
>     tyWC  <- skol (constrType op tyEnv)
>     unifyArgs (ppConstrTerm 0 t) [t1,t2] tyWC
>   where unifyArgs :: Doc -> [ConstrTerm] -> TypeWithContext ->
>                      TcState TypeWithContext
>         unifyArgs _   []     tyWC = return tyWC
>         unifyArgs doc (t:ts) tyWC
>           | isArrowType ty =
>               let [tyAlpha,tyBeta] = typeArguments ty
>               in  do tyWC' <- tcConstrTerm m tcEnv sigs p t
>                      ctx'  <- unifyTypeWithContext p "pattern"
>                                 (doc $-$ text "Term:" <+> ppConstrTerm 0 t) m
>                                 (TypeWithContext ctx tyAlpha) tyWC'
>                      unifyArgs doc ts (TypeWithContext ctx' tyBeta)
>           | otherwise = internalError "tcConstrTerm"
>           where ty  = removeTypeContext tyWC
>                 ctx = typeContext tyWC
> tcConstrTerm m tcEnv sigs p (ParenPattern t) = tcConstrTerm m tcEnv sigs p t
> tcConstrTerm m tcEnv sigs p (TuplePattern ts)
>  | null ts   = return unitTypeWithContext
>  | otherwise = liftM tupleTypeWithContext $ mapM (tcConstrTerm m tcEnv sigs p) ts
> tcConstrTerm m tcEnv sigs p t@(ListPattern ts) =
>   freshTypeVarWithContext >>= flip (tcElems (ppConstrTerm 0 t)) ts
>   where tcElems :: Doc -> TypeWithContext -> [ConstrTerm] ->
>                    TcState TypeWithContext
>         tcElems _ tyWC [] = return (listTypeWithContext tyWC)
>         tcElems doc tyWC (t:ts) =
>           do tyWC' <- tcConstrTerm m tcEnv sigs p t
>              ctx   <- unifyTypeWithContext p "pattern"
>                         (doc $-$ text "Term:" <+> ppConstrTerm 0 t) m
>                         tyWC tyWC'
>              tcElems doc (appendTypeContext tyWC ctx) ts
> tcConstrTerm m tcEnv sigs p t@(AsPattern v t') =
>   do
>     tyWC1 <- tcConstrTerm m tcEnv sigs p (VariablePattern v)
>     tyWC2 <- tcConstrTerm m tcEnv sigs p t'
>     ctx   <- unifyTypeWithContext p "pattern" (ppConstrTerm 0 t) m tyWC1 tyWC2
>     return (appendTypeContext tyWC1 ctx)
> tcConstrTerm m tcEnv sigs p (LazyPattern t) = 
>   tcConstrTerm m tcEnv sigs p t

> tcRhs :: ModuleIdent -> TypeClassEnv -> InstanceEnv -> TCEnv ->
>          ValueEnv -> SigEnv -> Rhs -> TcState TypeWithContext
> tcRhs m typeClassEnv instEnv tcEnv tyEnv0 sigs (SimpleRhs p e ds) =
>   do
>     ctx  <- tcDecls False m typeClassEnv instEnv tcEnv sigs ds
>     tyWC <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>     let tyWC' = appendTypeContext tyWC ctx
>     checkSkolems p m (text "Expression:" <+> ppExpr 0 e) tyEnv0 tyWC'
> tcRhs m typeClassEnv instEnv tcEnv tyEnv0 sigs (GuardedRhs es ds) =
>   do
>     ctx  <- tcDecls False m typeClassEnv instEnv tcEnv sigs ds
>     tyWC <- tcCondExprs m typeClassEnv instEnv tcEnv tyEnv0 sigs es
>     return (appendTypeContext tyWC ctx)

> tcCondExprs :: ModuleIdent -> TypeClassEnv -> InstanceEnv -> TCEnv ->
>                ValueEnv -> SigEnv -> [CondExpr] -> TcState TypeWithContext
> tcCondExprs m typeClassEnv instEnv tcEnv tyEnv0 sigs es =
>   do
>     gtyWC <- if length es > 1 then return boolTypeWithContext
>                               else freshConstrainedWithContext [successType,boolType]
>     tyWC  <- freshTypeVarWithContext
>     tcCondExprs' gtyWC tyWC es
>   where tcCondExprs' :: TypeWithContext -> TypeWithContext -> [CondExpr] ->
>                         TcState TypeWithContext
>         tcCondExprs' gtyWC tyWC [] = return tyWC
>         tcCondExprs' gtyWC tyWC (e:es) =
>           do ctx <- tcCondExpr gtyWC tyWC e
>              tcCondExprs' gtyWC (appendTypeContext tyWC ctx) es
>         tcCondExpr :: TypeWithContext -> TypeWithContext -> CondExpr ->
>                       TcState TypeContext
>         tcCondExpr gtyWC tyWC (CondExpr p g e) =
>           do gtyWC' <- tcExpr m typeClassEnv instEnv tcEnv sigs p g
>              ctx1   <- unifyTypeWithContext p "guard" (ppExpr 0 g) m gtyWC gtyWC'
>              tyWC'  <- tcExpr m typeClassEnv instEnv tcEnv sigs p e >>=
>                        checkSkolems p m (text "Expression:" <+> ppExpr 0 e) tyEnv0
>              ctx2   <- unifyTypeWithContext p "guarded expression" (ppExpr 0 e) m
>                                             tyWC tyWC'
>              return (composeTypeContext ctx1 ctx2)

> tcExpr :: ModuleIdent -> TypeClassEnv -> InstanceEnv -> TCEnv -> SigEnv ->
>           Position -> Expression -> TcState TypeWithContext
> tcExpr m typeClassEnv instEnv _     _    _ (Literal l) =
>   tcLiteral m l
> tcExpr m typeClassEnv instEnv tcEnv sigs p (Variable v) =
>   case qualLookupTypeSig m v sigs of
>     Just tyWC -> inst (expandPolyType tcEnv tyWC)
>     Nothing   -> fetchSt >>= inst . funType v
> tcExpr m typeClassEnv instEnv tcEnv sigs p (Constructor c) =
>   fetchSt >>= instExist . constrType c
> tcExpr m typeClassEnv instEnv tcEnv sigs p (Typed e sig) =
>   do
>     tyEnv0 <- fetchSt
>     tyWC   <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>     inst declSigma >>=
>       flip (unifyTypeWithContext p "explicitly typed expression" (ppExpr 0 e) m) tyWC
>     theta  <- liftSt fetchSt
>     let tyWC' = mapTypeContext (contextReduction p typeClassEnv instEnv)
>                                (subst theta tyWC)
>     let infSigma = gen (fvEnv (subst theta tyEnv0)) tyWC'
>     unless (infSigma == declSigma)
>       (errorAt p (typeSigTooGeneral m (text "Expression:" <+> ppExpr 0 e)
>                  sig' infSigma))
>     return tyWC'
>   where sig'      = nameSigTypeWithContext sig
>         declSigma = expandPolyType tcEnv sig'
> tcExpr m typeClassEnv instEnv tcEnv sigs p (Paren e) =
>   tcExpr m typeClassEnv instEnv tcEnv sigs p e
> tcExpr m typeClassEnv instEnv tcEnv sigs p (Tuple es)
>   | null es   = return unitTypeWithContext
>   | otherwise = liftM tupleTypeWithContext
>                         (mapM (tcExpr m typeClassEnv instEnv tcEnv sigs p) es)
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(List es) =
>   freshTypeVarWithContext >>= tcElems (ppExpr 0 e) es
>   where tcElems :: Doc -> [Expression] -> TypeWithContext ->
>                    TcState TypeWithContext
>         tcElems _ [] tyWC = return (listTypeWithContext tyWC)
>         tcElems doc (e:es) tyWC =
>           do tyWC' <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>              ctx   <- unifyTypeWithContext p "expression"
>                         (doc $-$ text "Term:" <+> ppExpr 0 e) m
>                         tyWC tyWC'
>              tcElems doc es (appendTypeContext tyWC ctx)
> tcExpr m typeClassEnv instEnv tcEnv sigs p (ListCompr e qs) =
>   do
>     tyEnv0 <- fetchSt
>     ctxs   <- mapM (tcQual m typeClassEnv instEnv tcEnv sigs p) qs
>     tyWC   <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>     tyWC'  <- checkSkolems p m (text "Expression:" <+> ppExpr 0 e) tyEnv0
>                 (listTypeWithContext tyWC)
>     return (foldl appendTypeContext tyWC' ctxs)
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(EnumFrom e1) =
>   do
>     tyWC1 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e1
>     ctx1  <- unifyTypeWithContext p "arithmetic sequence"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e1) m
>                intTypeWithContext tyWC1
>     return (listTypeWithContext (appendTypeContext intTypeWithContext ctx1))
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(EnumFromThen e1 e2) =
>   do
>     tyWC1 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e1
>     tyWC2 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e2
>     ctx1  <- unifyTypeWithContext p "arithmetic sequence"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e1) m
>                intTypeWithContext tyWC1
>     ctx2  <- unifyTypeWithContext p "arithmetic sequence"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e2) m
>                intTypeWithContext tyWC2
>     return (listTypeWithContext
>               (foldl appendTypeContext intTypeWithContext [ctx1,ctx2]))
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(EnumFromTo e1 e2) =
>   do
>     tyWC1 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e1
>     tyWC2 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e2
>     ctx1  <- unifyTypeWithContext p "arithmetic sequence"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e1) m
>                intTypeWithContext tyWC1
>     ctx2  <- unifyTypeWithContext p "arithmetic sequence"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e2) m
>                intTypeWithContext tyWC2
>     return (listTypeWithContext
>               (foldl appendTypeContext intTypeWithContext [ctx1,ctx2]))
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(EnumFromThenTo e1 e2 e3) =
>   do
>     tyWC1 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e1
>     tyWC2 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e2
>     tyWC3 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e3
>     ctx1  <- unifyTypeWithContext p "arithmetic sequence"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e1) m
>                intTypeWithContext tyWC1
>     ctx2  <- unifyTypeWithContext p "arithmetic sequence"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e2) m
>                intTypeWithContext tyWC2
>     ctx3  <- unifyTypeWithContext p "arithmetic sequence"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e3) m
>                intTypeWithContext tyWC3
>     return (listTypeWithContext
>               (foldl appendTypeContext intTypeWithContext [ctx1,ctx2,ctx3]))
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(UnaryMinus op e1) =
>   do
>     tyWC1 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e1
>     opTy  <- opType op
>     ctx   <- unifyTypeWithContext p "unary negation"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e1) m
>                opTy tyWC1
>     return (appendTypeContext tyWC1 ctx)
>   where opType :: Ident -> TcState TypeWithContext
>         opType op
> -- FIXME: overloaded numeric literals
> --          | op == minusId  = freshConstrainedWithContext [intType,floatType]
>           | op == minusId  = return intTypeWithContext
>           | op == fminusId = return floatTypeWithContext
>           | otherwise = internalError ("tcExpr unary " ++ name op)
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(Apply e1 e2) =
>   do
>     tyWC1 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e1
>     tyWC2 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e2
>     (alpha,beta) <-
>       tcArrow p "application" (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e1)
>               m tyWC1
>     ctx <- unifyTypeWithContext p "application"
>              (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e2) m
>              alpha tyWC2
>     return (appendTypeContext beta ctx)
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(InfixApply e1 op e2) =
>   do
>     opTyWC <- tcExpr m typeClassEnv instEnv tcEnv sigs p (infixOp op)
>     tyWC1  <- tcExpr m typeClassEnv instEnv tcEnv sigs p e1
>     tyWC2  <- tcExpr m typeClassEnv instEnv tcEnv sigs p e2
>     (alpha,beta,gamma) <-
>       tcBinary p "infix application"
>                (ppExpr 0 e $-$ text "Operator:" <+> ppOp op) m opTyWC
>     ctx1 <- unifyTypeWithContext p "infix application"
>               (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e1) m
>               alpha tyWC1
>     ctx2 <- unifyTypeWithContext p "infix application"
>               (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e2) m
>               beta tyWC2
>     return (foldl appendTypeContext gamma [ctx1,ctx2])
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(LeftSection e1 op) =
>   do
>     opTyWC <- tcExpr m typeClassEnv instEnv tcEnv sigs p (infixOp op)
>     tyWC1  <- tcExpr m typeClassEnv instEnv tcEnv sigs p e1
>     (alpha,beta) <-
>       tcArrow p "left section" (ppExpr 0 e $-$ text "Operator:" <+> ppOp op)
>               m opTyWC
>     ctx    <- unifyTypeWithContext p "left section"
>                 (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e1) m
>                 alpha tyWC1
>     return (appendTypeContext beta ctx)
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(RightSection op e1) =
>   do
>     opTyWC <- tcExpr m typeClassEnv instEnv tcEnv sigs p (infixOp op)
>     tyWC1  <- tcExpr m typeClassEnv instEnv tcEnv sigs p e1
>     (alpha,beta,gamma) <-
>       tcBinary p "right section"
>                (ppExpr 0 e $-$ text "Operator:" <+> ppOp op) m opTyWC
>     ctx    <- unifyTypeWithContext p "right section"
>                 (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e1) m
>                 beta tyWC1
>     return (appendTypeContext (typeArrowWithContext alpha gamma) ctx)
> tcExpr m typeClassEnv instEnv tcEnv sigs p (Lambda ts e) =
>   do
>     tyEnv0 <- fetchSt
>     tyWCs  <- mapM (tcConstrTerm m tcEnv sigs p) ts
>     tyWC   <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>     checkSkolems p m (text "Expression:" <+> ppExpr 0 (Lambda ts e)) tyEnv0
>                  (foldr typeArrowWithContext tyWC tyWCs)
> tcExpr m typeClassEnv instEnv tcEnv sigs p (Let ds e) =
>   do
>     tyEnv0 <- fetchSt
>     theta  <- liftSt fetchSt
>     ctx1   <- tcDecls False m typeClassEnv instEnv tcEnv sigs ds
>     tyWC   <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>     ctx2   <- if (any isExtraVariables ds)
>                 then (unifyTypeWithContext p "existentially quantified expression"
>                        (ppExpr 0 e) m
>                        successTypeWithContext tyWC)
>                 else return (emptyTypeContext)
>     let tyWC' = appendTypeContext (appendTypeContext tyWC ctx1) ctx2
>     checkSkolems p m (text "Expression:" <+> ppExpr 0 e) tyEnv0 tyWC'
> tcExpr m typeClassEnv instEnv tcEnv sigs p (Do sts e) =
>   do
>     tyEnv0   <- fetchSt
>     ctxs     <- mapM (tcStmt m typeClassEnv instEnv tcEnv sigs p) sts
>     tyWCExpr <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>     let tyWC = foldl appendTypeContext tyWCExpr ctxs
>     checkSkolems p m (text "Expression:" <+> ppExpr 0 e) tyEnv0 tyWC
> tcExpr m typeClassEnv instEnv tcEnv sigs p e@(IfThenElse e1 e2 e3) =
>   do
>     tyWC1 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e1
>     ctx1  <- unifyTypeWithContext p "expression"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e1) m
>                boolTypeWithContext tyWC1
>     tyWC2 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e2
>     tyWC3 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e3
>     ctx2  <- unifyTypeWithContext p "expression"
>                (ppExpr 0 e $-$ text "Term:" <+> ppExpr 0 e3) m
>                tyWC2 tyWC3
>     return (foldl appendTypeContext tyWC3 [ctx1,ctx2])
> tcExpr m typeClassEnv instEnv tcEnv sigs p (Case e alts) =
>   do
>     tyEnv0 <- fetchSt
>     tyWC   <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>     alpha  <- freshTypeVarWithContext
>     tcAlts tyEnv0 tyWC alpha alts
>   where tcAlts :: ValueEnv -> TypeWithContext -> TypeWithContext -> [Alt] ->
>                   TcState TypeWithContext
>         tcAlts tyEnv0 _     tyWC2 [] = return tyWC2
>         tcAlts tyEnv0 tyWC1 tyWC2 (alt:alts) =
>           do ctx <- tcAlt (ppAlt alt) tyEnv0 tyWC1 tyWC2 alt
>              tcAlts tyEnv0 tyWC1 (appendTypeContext tyWC2 ctx) alts
>         tcAlt :: Doc -> ValueEnv -> TypeWithContext -> TypeWithContext -> Alt ->
>                  TcState TypeContext
>         tcAlt doc tyEnv0 tyWC1 tyWC2 (Alt p t rhs) =
>           do tyWC1' <- tcConstrTerm m tcEnv sigs p t
>              ctx1   <- unifyTypeWithContext p "case pattern"
>                          (doc $-$ text "Term:" <+> ppConstrTerm 0 t) m
>                          tyWC1 tyWC1'
>              tyWC2' <- tcRhs m typeClassEnv instEnv tcEnv tyEnv0 sigs rhs
>              ctx2   <- unifyTypeWithContext p "case branch" doc m tyWC2 tyWC2'
>              return (composeTypeContext ctx1 ctx2)

> tcQual :: ModuleIdent -> TypeClassEnv -> InstanceEnv -> TCEnv ->
>           SigEnv -> Position -> Statement -> TcState TypeContext
> tcQual m typeClassEnv instEnv tcEnv sigs p (StmtExpr e) =
>   do tyWC <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>      unifyTypeWithContext p "guard" (ppExpr 0 e) m boolTypeWithContext tyWC
> tcQual m typeClassEnv instEnv tcEnv sigs p q@(StmtBind t e) =
>   do
>     tyWC1 <- tcConstrTerm m tcEnv sigs p t
>     tyWC2 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>     unifyTypeWithContext p "generator"
>          (ppStmt q $-$ text "Term:" <+> ppExpr 0 e) m
>          (listTypeWithContext tyWC1) tyWC2
> tcQual m typeClassEnv instEnv tcEnv sigs p (StmtDecl ds) =
>   tcDecls False m typeClassEnv instEnv tcEnv sigs ds

> tcStmt :: ModuleIdent -> TypeClassEnv -> InstanceEnv -> TCEnv -> SigEnv ->
>           Position -> Statement -> TcState TypeContext
> tcStmt m typeClassEnv instEnv tcEnv sigs p (StmtExpr e) =
>   do
>     alpha <- freshTypeVarWithContext
>     tyWC  <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>     ctx   <- unifyTypeWithContext p "statement" (ppExpr 0 e) m
>                (ioTypeWithContext alpha) tyWC
>     return (composeTypeContext (typeContext tyWC) ctx)
> tcStmt m typeClassEnv instEnv tcEnv sigs p st@(StmtBind t e) =
>   do
>     tyWC1 <- tcConstrTerm m tcEnv sigs p t
>     tyWC2 <- tcExpr m typeClassEnv instEnv tcEnv sigs p e
>     ctx   <- unifyTypeWithContext p "statement"
>                (ppStmt st $-$ text "Term:" <+> ppExpr 0 e) m
>                (ioTypeWithContext tyWC1) tyWC2
>     return ctx
> tcStmt m typeClassEnv instEnv tcEnv sigs p (StmtDecl ds) =
>   tcDecls False m typeClassEnv instEnv tcEnv sigs ds

\end{lstlisting}
The function \texttt{tcArrow} checks that its argument can be used as
an arrow type $\alpha\rightarrow\beta$ and returns the pair
$(\alpha,\beta)$. Similarly, the function \texttt{tcBinary} checks
that its argument can be used as an arrow type
$\alpha\rightarrow\beta\rightarrow\gamma$ and returns the triple
$(\alpha,\beta,\gamma)$.
\begin{lstlisting}

> tcArrow :: Position -> String -> Doc -> ModuleIdent -> TypeWithContext
>         -> TcState (TypeWithContext,TypeWithContext)
> tcArrow p what doc m tyWC =
>   do
>     theta        <- liftSt fetchSt
>     let tyWC'    =  subst theta tyWC
>     (alpha,beta) <- unaryArrow (removeTypeContext tyWC')
>     let ctx      =  typeContext tyWC'
>     return (TypeWithContext ctx alpha,TypeWithContext ctx beta)
>   where unaryArrow :: Type -> TcState (Type,Type)
>         unaryArrow (TypeVariable tv) =
>           do
>             alpha <- freshTypeVar
>             beta  <- freshTypeVar
>             let arrow = typeArrow alpha beta
>             liftSt (updateSt_ (bindVar tv arrow))
>             return (alpha,beta)
>         unaryArrow ty
>           | isArrowType ty =
>               let [tyAlpha,tyBeta] = typeArguments ty
>               in  return (tyAlpha,tyBeta)
>           | otherwise = errorAt p (nonFunctionType what doc m ty)

> tcBinary :: Position -> String -> Doc -> ModuleIdent -> TypeWithContext
>          -> TcState (TypeWithContext,TypeWithContext,TypeWithContext)
> tcBinary p what doc m tyWC =
>   do let ctx = typeContext tyWC
>      (alphaWC,tyWC')    <- tcArrow p what doc m tyWC
>      (alpha,beta,gamma) <- binaryArrow (removeTypeContext alphaWC)
>                                        (removeTypeContext tyWC')
>      return (TypeWithContext ctx alpha,TypeWithContext ctx beta,
>              TypeWithContext ctx gamma)
>   where binaryArrow :: Type -> Type -> TcState (Type,Type,Type)
>         binaryArrow ty1 (TypeVariable tv) =
>           do
>             beta  <- freshTypeVar
>             gamma <- freshTypeVar
>             let arrow = typeArrow beta gamma
>             liftSt (updateSt_ (bindVar tv arrow))
>             return (ty1,beta,gamma)
>         binaryArrow ty1 ty
>           | isArrowType ty =
>               let [tyBeta,tyGamma] = typeArguments ty
>               in  return (ty1,tyBeta,tyGamma)
>           | otherwise =
>               errorAt p (nonBinaryOp what doc m (typeArrow ty1 ty))

\end{lstlisting}
For each declaration group, the type checker has to ensure that no
skolem type escapes its scope.
\begin{lstlisting}

> checkSkolems :: Position -> ModuleIdent -> Doc -> ValueEnv -> TypeWithContext
>              -> TcState TypeWithContext
> checkSkolems p m what tyEnv tyWC =
>   do
>     theta <- liftSt fetchSt
>     let tyWC' = subst theta tyWC
>         fs    = fsEnv (subst theta tyEnv)
>     unless (all (`elemSet` fs) (typeSkolems tyWC'))
>            (errorAt p (skolemEscapingScope m what tyWC'))
>     return tyWC'

\end{lstlisting}
