% $Id: TypeExpansion.lhs 1526 2005-02-10 17:01:19Z berrueta $
%
% Copyright (c) 2000-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeExpansion.lhs}
\codesection{Expanding types}
\begin{lstlisting}
                                                                                
> module TypeExpansion where
> import TypeConstructorEnv
> import Ident
> import TypeTrans
> import Types
> import Error(internalError)
> import List(nub,sort)
> import Maybe(fromJust)
> import TypeExpr
> import List

\end{lstlisting}
The function \texttt{expandType} expands all type synonyms in a type
and also qualifies all type constructors with the name of the module
in which the type was defined.
\begin{lstlisting}

> expandMonoType :: TCEnv -> [Ident] -> TypeExpr -> Type
> expandMonoType tcEnv tvs ty = expandType tcEnv (toType tvs ty)

> expandMonoTypes :: TCEnv -> [Ident] -> [TypeExpr] -> [Type]
> expandMonoTypes tcEnv tvs tys = map (expandType tcEnv) (toTypes tvs tys)

> expandPolyType :: TCEnv -> TypeExprWithContext -> TypeScheme
> expandPolyType tcEnv tyexprWC =
>   polyTypeWithContext $ normalizeTypeWithContext (TypeWithContext ctx ty)
>   where (TypeWithContext ctx _) = toTypeWithContext [] tyexprWC
>         ty = expandMonoType tcEnv [] (removeTypeExprContext tyexprWC)

> expandType :: TCEnv -> Type -> Type
> expandType tcEnv (TypeConstructor tc) =
>   case qualLookupTC tc tcEnv of
>     [DataType     tc' _ _] -> TypeConstructor tc'
>     [RenamingType tc' _ _] -> TypeConstructor tc'
>     [AliasType    _ _ ty'] -> expandAliasType [] ty'
>     _ -> internalError ("expandType " ++ show tc)
> expandType _ (TypeVariable tv) = TypeVariable tv
> expandType _ (TypeConstrained tys tv) = TypeConstrained tys tv
> expandType tcEnv (TypeSkolem k) = TypeSkolem k
> expandType tcEnv ty@(TypeApplication _ _) =
>   case leftmost of
>     (TypeConstructor tc) -> case qualLookupTC tc tcEnv of
>        [DataType     tc' _ _] -> typeApply (TypeConstructor tc') args'
>        [RenamingType tc' _ _] -> typeApply (TypeConstructor tc') args'
>        [AliasType    _ _ ty'] -> expandAliasType args' ty'
>        _ -> internalError ("expandType " ++ show tc)
>     (TypeVariable tv)    -> typeApply leftmost args'
>   where leftmost = leftmostType ty 
>         args     = typeArguments ty
>         args'    = map (expandType tcEnv) args

\end{lstlisting}
The function \texttt{expandAliasType} expands all occurrences of a
type synonym in a type. After the expansion we have to reassign the
type indices for all type variables. Otherwise, expanding a type
synonym like \verb|type Pair' a b = (b,a)| could break the invariant
that the universally quantified type variables are assigned indices in
the order of their occurrence. This is handled by the function
\texttt{normalize}.
\begin{lstlisting}

> expandAliasType :: [Type] -> Type -> Type
> expandAliasType tys (TypeConstructor tc) =
>   TypeConstructor tc
> expandAliasType tys (TypeVariable n)
>   | n >= 0 = if n < length (take (n+1) tys)
>                 then tys !! n
>                 else internalError ("expandAliasType: " ++ show n ++
>                                     " of " ++ show tys)
>   | otherwise = TypeVariable n
> expandAliasType _   (TypeConstrained tys n)   = TypeConstrained tys n
> expandAliasType _   (TypeSkolem k)            = TypeSkolem k
> expandAliasType tys (TypeApplication ty1 ty2) =
>   TypeApplication (expandAliasType tys ty1) (expandAliasType tys ty2)

> expandAliasTypeContext :: [Type] -> TypeContext -> TypeContext
> expandAliasTypeContext tys (TypeContext classPreds) =
>   TypeContext (map (expandAliasTCC tys) classPreds)
>   where expandAliasTCC :: [Type] -> TypeClassConstraint ->
>                                     TypeClassConstraint
>         expandAliasTCC tys (TypeClassConstraint tc ty) =
>           TypeClassConstraint tc (expandAliasType tys ty)

> expandAliasTypeWithContext :: [Type] -> TypeWithContext -> TypeWithContext
> expandAliasTypeWithContext tys (TypeWithContext ctx ty) =
>   sortTypeContext $ TypeWithContext (expandAliasTypeContext tys ctx)
>                                     (expandAliasType        tys ty )

> normalize :: Type -> Type
> normalize ty = expandAliasType tvs' ty
>   where tvs  = zip (nub (filter (>= 0) (typeVars ty))) [0..]
>         tvs' = [TypeVariable (occur tv) | tv <- [0..]]
>         occur tv = maybe (internalError "TypeExpansion.normalize") id
>                          (lookup tv tvs)

> normalizeTypeWithContext :: TypeWithContext -> TypeWithContext
> normalizeTypeWithContext tyWC = expandAliasTypeWithContext tvs' tyWC
>   where tvs  = zip (nub (filter (>= 0) (typeVars tyWC))) [0..]
>         tvs' = [TypeVariable (occur tv) | tv <- [0..]]
>         occur tv = maybe (internalError "TypeExpansion.normalizeTWC") id
>                          (lookup tv tvs)

> sortTypeContext :: TypeWithContext -> TypeWithContext
> sortTypeContext (TypeWithContext (TypeContext classPreds) ty) =
>   TypeWithContext (TypeContext classPreds') ty
>   where classPreds' = sortBy compareClassPreds classPreds
>         compareClassPreds :: TypeClassConstraint -> TypeClassConstraint -> Ordering
>         compareClassPreds (TypeClassConstraint tc1 ty1)
>                           (TypeClassConstraint tc2 ty2) =
>           case compare (posVars ty1) (posVars ty2) of
>             EQ -> compare tc1 tc2
>             x  -> x
>         posVars :: Type -> [Int]
>         posVars ty' = map (fromJust . flip elemIndex tyVars) (typeVars ty')
>         tyVars = typeVars ty

\end{lstlisting}
