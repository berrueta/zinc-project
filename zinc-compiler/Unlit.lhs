% -*- LaTeX -*-
% $Id: Unlit.lhs 970 2004-08-08 12:05:32Z berrueta $
%
% $Log$
% Revision 1.3  2004/08/08 12:05:32  berrueta
% Changed section, subsection and paragraph by codesection, codesubsection and codeparagraph
%
% Revision 1.2  2004/08/08 11:58:54  berrueta
% Substituted verbatim by lstlisting
%
% Revision 1.1.1.1  2003/12/01 19:54:55  berrueta
% Initial check-in. Sources from Muenster Curry Compiler, without the doc subdirectory
%
% Revision 1.2  2002/10/01 06:55:50  lux
% unlit returns an error message to the caller instead of calling error.
%
% Revision 1.1  2000/02/07 14:05:55  lux
% The compiler now supports literate source files. Literate source files
% must end with the suffix ".lcurry".
%
%
\nwfilename{Unlit.lhs}
\codesection{Literate comments}
Since version 0.7 of the language report, Curry accepts literate
source programs. In a literate source all program lines must begin
with a greater sign in the first column. All other lines are assumed
to be documentation. In order to avoid some common errors with
literate programs, Curry requires at least one program line to be
present in the file. In addition, every block of program code must be
preceded by a blank line and followed by a blank line.

The module \texttt{Unlit} acts as a preprocessor which converts
literate source programs into the ``un-literate'' format accepted by
the lexer. The implementation, together with the comments below, was
derived from appendix D in the Haskell 1.2 report.
\begin{lstlisting}

> module Unlit(unlit) where
> import Char
> import Position

\end{lstlisting}
Each of the lines in a literate script is a program line, a blank
line, or a comment line. In the first case the text is kept with the
line.
\begin{lstlisting}

> data Classified = Program String | Blank | Comment

\end{lstlisting}
In a literate program, program lines begin with a \verb|>| character,
blank lines contain only whitespace, and all other lines are comment
lines.
\begin{lstlisting}

> classify :: String -> Classified
> classify ""            = Blank
> classify (c:cs)
>   | c == '>'           = Program cs
>   | all isSpace (c:cs) = Blank
>   | otherwise          = Comment

\end{lstlisting}
In the corresponding program, program lines have the leading \verb|>|
replaced by a leading space, to preserve tab alignments.
\begin{lstlisting}

> unclassify :: Classified -> String
> unclassify (Program cs) = ' ' : cs
> unclassify Blank        = ""
> unclassify Comment      = ""

\end{lstlisting}
Process a literate program into error messages (if any) and the
corresponding non-literate program.
\begin{lstlisting}

> unlit :: FilePath -> String -> (String,String)
> unlit fn lcy = (es,cy)
>   where cs = map classify (lines lcy)
>         es = unlines (errors fn cs)
>         cy = unlines (map unclassify cs)

\end{lstlisting}
Check that each program line is not adjacent to a comment line and
there is at least one program line.
\begin{lstlisting}

> errors :: FilePath -> [Classified] -> [String]
> errors fn cs =
>   concat (zipWith3 adjacent (iterate nl (first fn)) cs (tail cs)) ++
>   empty fn (filter isProgram cs)

\end{lstlisting}
Given a line number and a pair of adjacent lines, generate a list of
error messages, which will contain either one entry or none.
\begin{lstlisting}

> adjacent :: Position -> Classified -> Classified -> [String]
> adjacent p (Program _) Comment     = [message (nl p) "after"]
> adjacent p Comment     (Program _) = [message p "before"]
> adjacent p _           _           = []

> message p w = show p ++ ": comment line " ++ w ++ " program line."

\end{lstlisting}
Given the list of program lines generate an error if this list is
empty.
\begin{lstlisting}

> empty :: FilePath -> [Classified] -> [String]
> empty fn [] = [show (first fn) ++ ": no code in literate script"]
> empty fn _ = []

> isProgram :: Classified -> Bool
> isProgram (Program _) = True
> isProgram _ = False

\end{lstlisting}
