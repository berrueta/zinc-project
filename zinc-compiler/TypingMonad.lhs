% $Id: TypingMonad.lhs 970 2004-08-08 12:05:32Z berrueta $
%
% Copyright (c) 2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypingMonad.lhs}
\codesection{Monad to compute the type of Curry expressions}
\begin{lstlisting}

> module TypingMonad where
> import Combined
> import Monad
> import TypeSubst
> import ValueEnv
> import Types

> type TyState a = StateT TypeSubst (StateT Int (StateT TypeContext Id)) a

> runTyState :: TyState a -> ValueEnv -> a
> runTyState m tyEnv = runTyStateStartIn m tyEnv 0

> runTyStateStartIn :: TyState a -> ValueEnv -> Int -> a
> runTyStateStartIn m tyEnv initKey = 
>   runSt (callSt (callSt m idSubst) initKey) emptyTypeContext

\end{lstlisting}
