% $Id: TypeCheckUtils.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeCheckUtils.lhs}
\codesection{Type check utilities}
This module contains some auxiliary functions related to the
type inference process.
\begin{lstlisting}

> module TypeCheckUtils where
> import CurrySyntax
> import Types
> import Ident
> import ValueEnv
> import Error(internalError)

\begin{lstlisting}
The functions \texttt{constrType}, \texttt{varType}, and
\texttt{funType} are used to retrieve the type of constructors,
pattern variables, and variables in expressions, respectively, from
the type environment. Because the syntactical correctness has already
been verified by the syntax checker, none of these functions should
fail.

Note that \texttt{varType} can handle ambiguous identifiers and
returns the first available type. This function is used for looking up
the type of an identifier on the left hand side of a rule where it
unambiguously refers to the local definition.
\begin{lstlisting}

> constrType :: QualIdent -> ValueEnv -> ExistTypeScheme
> constrType c tyEnv =
>   case qualLookupValue c tyEnv of
>     [DataConstructor    _ ets] -> ets
>     [NewtypeConstructor _ ets] -> ets
>     _ -> internalError ("TypeCheck.constrType " ++ show c)

> varType :: Ident -> ValueEnv -> TypeScheme
> varType v tyEnv =
>   case lookupValue v tyEnv of
>     Value _ ts : _ -> ts
>     _ -> internalError ("TypeCheck.varType " ++ show v)

> funType :: QualIdent -> ValueEnv -> TypeScheme
> funType f tyEnv =
>   case qualLookupValue f tyEnv of
>     [Value _ ts] -> ts
>     _ -> internalError ("funType " ++ show f)

\end{lstlisting}
