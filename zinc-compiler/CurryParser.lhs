% $Id: CurryParser.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2004, Wolfgang Lux
% Copyright (c) 2003-2005, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{CurryParser.lhs}
\codesection{A Parser for Zinc}
The Curry parser is implemented using the (mostly) LL(1) parsing
combinators described in section~\ref{sec:ll-parsecomb}.
\begin{lstlisting}

> module CurryParser where
> import Ident
> import Position
> import Error
> import LexComb
> import LLParseComb
> import CurrySyntax hiding (infixOp)
> import CurryLexer
> import TypeExpr hiding (typeExprContext)
> import Kind

> instance Symbol Token where
>   isEOF (Token c _) = c == EOF

\end{lstlisting}
\codesubsection{Modules}
\begin{lstlisting}

> parseSource :: FilePath -> String -> Error Module
> parseSource = applyParser parseModule lexer

> parseHeader :: FilePath -> String -> Error Module
> parseHeader = prefixParser (moduleHeader <*->
>                             (leftBrace `opt` undefined) <*>
>                             many (importDecl <*-> many semicolon))
>                            lexer

> parseModule :: Parser Token Module a
> parseModule = moduleHeader <*> decls

> moduleHeader :: Parser Token ([Decl] -> Module) a
> moduleHeader = Module <$-> token KW_module
>                       <*> (mIdent <?> "module name expected")
>                       <*> (Just <$> exportSpec `opt` Nothing)
>                       <*-> (token KW_where <?> "where expected")
>          `opt` Module mainMIdent Nothing

> exportSpec :: Parser Token ExportSpec a
> exportSpec = Exporting <$> position <*> parens (export `sepBy` comma)

> export :: Parser Token Export a
> export = qtycon <**> (parens spec `opt` Export)
>      <|> Export <$> qfun <\> qtycon
>      <|> ExportModule <$-> token KW_module <*> mIdent
>      <|> (token KW_class <-*> qIdent) <**> (parens typeClassSpec)
>   where spec = ExportTypeAll <$-> token DotDot
>            <|> flip ExportTypeWith <$> con `sepBy` comma
>         typeClassSpec = ExportTypeClassAll <$-> token DotDot
>            <|> flip ExportTypeClassWith <$> fun `sepBy` comma

\end{lstlisting}
\codesubsection{Interfaces}
\begin{lstlisting}

> parseInterface :: FilePath -> String -> Error Interface
> parseInterface fn s = applyParser parseIface lexer fn s

> parseIface :: Parser Token Interface a
> parseIface = Interface <$-> token Id_interface
>                        <*> (mIdent <?> "module name expected")
>                        <*-> (token KW_where <?> "where expected")
>                        <*> braces intfDecls

\end{lstlisting}
\codesubsection{Goals}
\begin{lstlisting}

> parseGoal :: String -> Error Goal
> parseGoal s = applyParser goal lexer "" s

> goal :: Parser Token Goal a
> goal = Goal <$> position <*> expr <*> localDefs

\end{lstlisting}
\codesubsection{Declarations}
\begin{lstlisting}

> decls :: Parser Token [Decl] a
> decls = layout globalDecls

> globalDecls :: Parser Token [Decl] a
> globalDecls = (:) <$> importDecl <*> (semicolon <-*> globalDecls `opt` [])
>           <|> topDecl `sepBy` semicolon

> topDecl :: Parser Token Decl a
> topDecl = infixDecl
>       <|> typeClassDecl <|> instanceDecl
>       <|> dataDecl <|> newtypeDecl <|> typeDecl
>       <|> functionDecl <|> externalDecl

> localDefs :: Parser Token [Decl] a
> localDefs = token KW_where <-*> layout valueDecls
>       `opt` []

> valueDecls :: Parser Token [Decl] a
> valueDecls = (infixDecl <|> valueDecl <|> externalDecl) `sepBy` semicolon

> importDecl :: Parser Token Decl a
> importDecl =
>   flip . ImportDecl <$> position <*-> token KW_import 
>                     <*> (True <$-> token Id_qualified `opt` False)
>                     <*> mIdent
>                     <*> (Just <$-> token Id_as <*> mIdent `opt` Nothing)
>                     <*> (Just <$> importSpec `opt` Nothing)

> importSpec :: Parser Token ImportSpec a
> importSpec = position <**> (Hiding <$-> token Id_hiding `opt` Importing)
>                       <*> parens (spec `sepBy` comma)
>   where spec = tycon <**> (parens constrs `opt` Import)
>            <|> Import <$> fun <\> tycon
>         constrs = ImportTypeAll <$-> token DotDot
>               <|> flip ImportTypeWith <$> con `sepBy` comma

> infixDecl :: Parser Token Decl a
> infixDecl = infixDeclLhs InfixDecl <*> funop `sepBy1` comma

> infixDeclLhs :: (Position -> Infix -> Int -> a) -> Parser Token a b
> infixDeclLhs f = f <$> position <*> tokenOps infixKW <*> int
>   where infixKW = [(KW_infix,Infix),(KW_infixl,InfixL),(KW_infixr,InfixR)]

\end{lstlisting}
Parsing of type class declarations and instance declarations is
very similar, so we introduce a new combinator.
\begin{lstlisting}

> classInstHead :: (Position -> TypeExprContext -> a -> b -> c) -> 
>                  Category ->
>                  Parser Token a d -> Parser Token b d ->
>                  Parser Token c d
> classInstHead f kw classIdent argument =
>   mkDecl <$> position <*-> token kw <*> declHead
>   where mkDecl p (ctxt,tc,x) = f p ctxt tc x
>         declHead = (,,) <$> typeExprContext <*-> token ContextRightArrow
>                         <*> classIdent <*> argument
>                  <|?>
>                    (,,) emptyTypeExprContext
>                         <$> classIdent <*> argument

> typeClassDecl :: Parser Token Decl a
> typeClassDecl =
>   classInstHead TypeClassDecl KW_class ident ident
>     <*> (token KW_where <-*> layout methodDecl `opt` [])
>   where methodDecl :: Parser Token [Decl] a
>         methodDecl = functionDecl `sepBy` semicolon

> instanceDecl :: Parser Token Decl a
> instanceDecl =
>   classInstHead InstanceDecl KW_instance qIdent type2
>     <*> (token KW_where <-*> layout methodDef `opt` [])
>   where methodDef :: Parser Token [Decl] a
>         methodDef = functionDecl `sepBy` semicolon

\end{lstlisting}
Parsing of type declarations are also very similar. A new combinator
is introduced.
\begin{lstlisting}

> dataDecl :: Parser Token Decl a
> dataDecl = typeDeclLhs DataDecl KW_data <*> constrs <*> optDeriving
>   where constrs = equals <-*> constrDecl `sepBy1` bar
>             `opt` []

> optDeriving :: Parser Token [Ident] a
> optDeriving = token KW_deriving <-*> classes `opt` []
>   where classes = return <$> ident
>               <|> parens (ident `sepBy1` comma)

> newtypeDecl :: Parser Token Decl a
> newtypeDecl = typeDeclLhs NewtypeDecl KW_newtype <*-> equals <*> newConstrDecl
>               <*> optDeriving

> typeDecl :: Parser Token Decl a
> typeDecl = typeDeclLhs TypeDecl KW_type <*-> equals <*> type0

> typeDeclLhs :: (Position -> Ident -> [Ident] -> a) -> Category
>             -> Parser Token a b
> typeDeclLhs f kw = f <$> position <*-> token kw <*> tycon <*> many typeVar
>   where typeVar = tyvar <|> anonId <$-> token Underscore

> constrDecl :: Parser Token ConstrDecl a
> constrDecl = position <**> (existVars <**> constr)
>   where constr = conId <**> identDecl
>              <|> leftParen <-*> parenDecl
>              <|> type1 <\> conId <\> leftParen <**> opDecl
>         identDecl = many type2 <**> (conType <$> opDecl `opt` conDecl)
>         parenDecl = flip conDecl <$> conSym <*-> rightParen <*> many type2
>                 <|> tupleType <*-> rightParen <**> opDecl
>         opDecl = conOpDecl <$> conop <*> type1
>         conType f tys c = f (typeExprApplyConstructor (qualify c) tys)
>         conDecl tys c tvs p = ConstrDecl p tvs c tys
>         conOpDecl op ty2 ty1 tvs p = ConOpDecl p tvs ty1 op ty2

> newConstrDecl :: Parser Token NewConstrDecl a
> newConstrDecl = NewConstrDecl <$> position <*> existVars <*> con <*> type2

> existVars :: Parser Token [Ident] a
> {- existVars = token Id_forall <-*> many1 tyvar <*-> dot `opt` [] -}
> existVars = succeed []

> functionDecl :: Parser Token Decl a
> functionDecl = position <**> decl
>   where decl = fun `sepBy1` comma <**> funListDecl
>           <|?> funDecl <$> lhs <*> declRhs
>         lhs = (\f -> (f,FunLhs f [])) <$> fun
>          <|?> funLhs

> valueDecl :: Parser Token Decl a
> valueDecl = position <**> decl
>   where decl = var `sepBy1` comma <**> valListDecl
>           <|?> valDecl <$> constrTerm0 <*> declRhs
>           <|?> funDecl <$> curriedLhs <*> declRhs
>         valDecl t@(ConstructorPattern c ts)
>           | not (isConstrId c) = funDecl (f,FunLhs f ts)
>           where f = unqualify c
>         valDecl t = opDecl id t
>         opDecl f (InfixPattern t1 op t2)
>           | isConstrId op = opDecl (f . InfixPattern t1 op) t2
>           | otherwise = funDecl (op',OpLhs (f t1) op' t2)
>           where op' = unqualify op
>         opDecl f t = patDecl (f t)
>         isConstrId c = c == qConsId || isQualified c || isQTupleId c

> funDecl :: (Ident,Lhs) -> Rhs -> Position -> Decl
> funDecl (f,lhs) rhs p = FunctionDecl p f [Equation p lhs rhs]

> patDecl :: ConstrTerm -> Rhs -> Position -> Decl
> patDecl t rhs p = PatternDecl p t rhs

> funListDecl :: Parser Token ([Ident] -> Position -> Decl) a
> funListDecl = typeSig <$-> token DoubleColon <*> typeExprWithContext
>           <|> evalAnnot <$-> token KW_eval <*> tokenOps evalKW
>   where typeSig tyWC vs p = TypeSig p vs tyWC
>         evalAnnot ev vs p = EvalAnnot p vs ev
>         evalKW = [(KW_rigid,EvalRigid),(KW_choice,EvalChoice)]

> valListDecl :: Parser Token ([Ident] -> Position -> Decl) a
> valListDecl = funListDecl <|> extraVars <$-> token KW_free
>   where extraVars vs p = ExtraVariables p vs

> funLhs :: Parser Token (Ident,Lhs) a
> funLhs = funLhs <$> fun <*> many1 constrTerm2
>     <|?> flip ($ id) <$> constrTerm1 <*> opLhs'
>     <|?> curriedLhs
>   where opLhs' = opLhs <$> funSym <*> constrTerm0
>              <|> infixPat <$> gConSym <\> funSym <*> constrTerm1 <*> opLhs'
>              <|> backquote <-*> opIdLhs
>         opIdLhs = opLhs <$> funId <*-> checkBackquote <*> constrTerm0
>               <|> infixPat <$> qConId <\> funId <*-> backquote <*> constrTerm1
>                            <*> opLhs'
>         funLhs f ts = (f,FunLhs f ts)
>         opLhs op t2 f t1 = (op,OpLhs (f t1) op t2)
>         infixPat op t2 f g t1 = f (g . InfixPattern t1 op) t2

> curriedLhs :: Parser Token (Ident,Lhs) a
> curriedLhs = apLhs <$> parens funLhs <*> many1 constrTerm2
>   where apLhs (f,lhs) ts = (f,ApLhs lhs ts)
  
> declRhs :: Parser Token Rhs a
> declRhs = rhs equals

> rhs :: Parser Token a b -> Parser Token Rhs b
> rhs eq = rhsExpr <*> localDefs
>   where rhsExpr = SimpleRhs <$-> eq <*> position <*> expr
>               <|> GuardedRhs <$> many1 (condExpr eq)

> externalDecl :: Parser Token Decl a
> externalDecl =
>   ExternalDecl <$> position <*-> token KW_external
>                <*> callConv <*> (Just <$> string `opt` Nothing)
>                <*> fun <*-> token DoubleColon <*> type0
>   where callConv = CallConvPrimitive <$-> token Id_primitive
>                <|> CallConvCCall <$-> token Id_ccall
>                <?> "Unsupported calling convention"

\end{lstlisting}
\codesubsection{Interface declarations}
\begin{lstlisting}

> intfDecls :: Parser Token [IDecl] a
> intfDecls = (:) <$> iImportDecl <*> (semicolon <-*> intfDecls `opt` [])
>         <|> intfDecl `sepBy` semicolon

> intfDecl :: Parser Token IDecl a
> intfDecl = iInfixDecl
>       <|> iTypeClassDecl <|> iInstanceDecl
>       <|> iHidingDecl <|> iDataDecl <|> iNewtypeDecl <|> iTypeDecl
>       <|> iKindDecl
>       <|> iFunctionDecl <\> token Id_hiding <\> token Id_kind

> iImportDecl :: Parser Token IDecl a
> iImportDecl = IImportDecl <$> position <*-> token KW_import <*> mIdent

> iInfixDecl :: Parser Token IDecl a
> iInfixDecl = infixDeclLhs IInfixDecl <*> qfunop

> iTypeClassDecl :: Parser Token IDecl a
> iTypeClassDecl =
>   classInstHead ITypeClassDecl KW_class qIdent ident
>     <*> (token KW_where <-*> layout methodDecl `opt` [])
>   where methodDecl :: Parser Token [IDecl] a
>         methodDecl = iFunctionDecl `sepBy` semicolon

> iInstanceDecl :: Parser Token IDecl a
> iInstanceDecl =
>   classInstHead IInstanceDecl KW_instance qIdent type2

> iHidingDecl :: Parser Token IDecl a
> iHidingDecl = position <*-> token Id_hiding <**> (dataDecl <|> funcDecl)
>   where dataDecl = hiddenData <$-> token KW_data <*> tycon <*> many tyvar
>         funcDecl = hidingFunc <$-> token DoubleColon <*> typeExprWithContext
>         hiddenData tc tvs p = HidingDataDecl p tc tvs
>         hidingFunc tyWC p = IFunctionDecl p hidingId tyWC
>         hidingId = qualify (mkIdent "hiding")

> iDataDecl :: Parser Token IDecl a
> iDataDecl = iTypeDeclLhs IDataDecl KW_data <*> constrs
>   where constrs = equals <-*> iConstrDecl `sepBy1` bar `opt` []
>         iConstrDecl = Just <$> constrDecl <\> token Underscore
>                   <|> Nothing <$-> token Underscore

> iNewtypeDecl :: Parser Token IDecl a
> iNewtypeDecl =
>   iTypeDeclLhs INewtypeDecl KW_newtype <*-> equals <*> newConstrDecl

> iTypeDecl :: Parser Token IDecl a
> iTypeDecl = iTypeDeclLhs ITypeDecl KW_type <*-> equals <*> type0

> iTypeDeclLhs :: (Position -> QualIdent -> [Ident] -> a) -> Category
>              -> Parser Token a b
> iTypeDeclLhs f kw = f <$> position <*-> token kw <*> qtycon <*> many tyvar

> iFunctionDecl :: Parser Token IDecl a
> iFunctionDecl = IFunctionDecl <$> position <*> qfun <*-> token DoubleColon
>                               <*> typeExprWithContext

> iKindDecl :: Parser Token IDecl a
> iKindDecl = position <*-> token Id_kind <**> (kindDecl <|> funcDecl)
>   where kindDecl = intfKind <$> qIdent <*> kind
>         funcDecl = kindFunc <$-> token DoubleColon <*> typeExprWithContext
>         intfKind f k p = IKindDecl p f k
>         kindFunc tyWC p = IFunctionDecl p kindId tyWC
>         kindId = qualify (mkIdent "kind")

\end{lstlisting}
\codesubsection{Types}
\begin{lstlisting}

> type0 :: Parser Token TypeExpr a
> type0 = type1 `chainr1` (typeExprApplyArrowConstructor <$-> token RightArrow)

> type1 :: Parser Token TypeExpr a
> type1 = apply <$> qtycon <*> many type2
>     <|> type2 <\> qtycon
>   where apply id tys
>           | isQualified id = typeExprApplyConstructor id tys
>           | otherwise = typeExprApply (TypeExprVariable $ unqualify id) tys

> type2 :: Parser Token TypeExpr a
> type2 = anonType <|> identType <|> parenType <|> listType

> anonType :: Parser Token TypeExpr a
> anonType = TypeExprVariable anonId <$-> token Underscore

> identType :: Parser Token TypeExpr a
> identType = TypeExprVariable <$> tyvar
>         <|> TypeExprConstructor <$> qtycon <\> tyvar

> parenType :: Parser Token TypeExpr a
> parenType = parens tupleType

> tupleType :: Parser Token TypeExpr a
> tupleType = type0 <??> (tuple <$> many1 (comma <-*> type0))
>       `opt` typeExprUnitConstructor
>   where tuple tys ty = typeExprApplyTupleConstructor (ty:tys)

> listType :: Parser Token TypeExpr a
> listType = brackets (typeExprApplyListConstructor <$> type0 `opt`
>                      typeExprListConstructor)

\end{lstlisting}
\codesubsection{Type expressions with contexts}
\begin{lstlisting}

> typeExprWithContext :: Parser Token TypeExprWithContext a
> typeExprWithContext =
>    TypeExprWithContext <$> typeExprContext <*-> token ContextRightArrow <*> type0
>    <|?> TypeExprWithContext emptyTypeExprContext <$> type0

> typeExprContext :: Parser Token TypeExprContext a
> typeExprContext =   TypeExprContext <$> parens (typeExprClassConstraint `sepBy` comma)
>                 <|> (\x -> TypeExprContext [x]) <$> typeExprClassConstraint

> typeExprClassConstraint :: Parser Token TypeExprClassConstraint a
> typeExprClassConstraint = TypeExprClassConstraint <$> qIdent <*>
>                             (TypeExprVariable <$> ident)

\end{lstlisting}
\codesubsection{Literals}
\begin{lstlisting}

> literal :: Parser Token Literal a
> literal = Char <$> char
>       <|> Int anonId <$> int
>       <|> Float <$> float
>       <|> String <$> string

\end{lstlisting}
\codesubsection{Kinds}
The kind expression language contains parentesis.
\begin{lstlisting}

> kind :: Parser Token Kind a
> kind = (Star <$-> token At <|> parens kind) `chainr1`
>        (KFun <$-> token RightArrow)

\end{lstlisting}
\codesubsection{Patterns}
\begin{lstlisting}

> constrTerm0 :: Parser Token ConstrTerm a
> constrTerm0 = constrTerm1 `chainr1` (flip InfixPattern <$> gconop)

> constrTerm1 :: Parser Token ConstrTerm a
> constrTerm1 = varId <**> identPattern
>           <|> ConstructorPattern <$> qConId <\> varId <*> many constrTerm2
>           <|> minus <**> negNum
>           <|> fminus <**> negFloat
>           <|> leftParen <-*> parenPattern
>           <|> constrTerm2 <\> qConId <\> leftParen
>   where identPattern = optAsPattern
>                    <|> conPattern <$> many1 constrTerm2
>         parenPattern = minus <**> minusPattern negNum
>                    <|> fminus <**> minusPattern negFloat
>                    <|> gconPattern
>                    <|> funSym <\> minus <\> fminus <*-> rightParen
>                                                    <**> identPattern
>                    <|> parenTuplePattern <\> minus <\> fminus <*-> rightParen
>         minusPattern p = rightParen <-*> identPattern
>                      <|> parenMinusPattern p <*-> rightParen
>         gconPattern = ConstructorPattern <$> gconId <*-> rightParen
>                                          <*> many constrTerm2
>         conPattern ts = flip ConstructorPattern ts . qualify

> constrTerm2 :: Parser Token ConstrTerm a
> constrTerm2 = literalPattern <|> anonPattern <|> identPattern
>           <|> parenPattern <|> listPattern <|> lazyPattern

> literalPattern :: Parser Token ConstrTerm a
> literalPattern = LiteralPattern <$> literal

> anonPattern :: Parser Token ConstrTerm a
> anonPattern = VariablePattern anonId <$-> token Underscore

> identPattern :: Parser Token ConstrTerm a
> identPattern = varId <**> optAsPattern
>            <|> flip ConstructorPattern [] <$> qConId <\> varId

> parenPattern :: Parser Token ConstrTerm a
> parenPattern = leftParen <-*> parenPattern
>   where parenPattern = minus <**> minusPattern negNum
>                    <|> fminus <**> minusPattern negFloat
>                    <|> flip ConstructorPattern [] <$> gconId <*-> rightParen
>                    <|> funSym <\> minus <\> fminus <*-> rightParen
>                                                    <**> optAsPattern
>                    <|> parenTuplePattern <\> minus <\> fminus <*-> rightParen
>         minusPattern p = rightParen <-*> optAsPattern
>                      <|> parenMinusPattern p <*-> rightParen

> listPattern :: Parser Token ConstrTerm a
> listPattern = ListPattern <$> brackets (constrTerm0 `sepBy` comma)

> lazyPattern :: Parser Token ConstrTerm a
> lazyPattern = LazyPattern <$-> token Tilde <*> constrTerm2

\end{lstlisting}
Partial patterns used in the combinators above, but also for parsing
the left-hand side of a declaration.
\begin{lstlisting}

> gconId :: Parser Token QualIdent a
> gconId = colon <|> tupleCommas

> negNum,negFloat :: Parser Token (Ident -> ConstrTerm) a
> negNum = flip NegativePattern <$> (Int anonId <$> int <|> Float <$> float)
> negFloat = flip NegativePattern . Float <$> (fromIntegral <$> int <|> float)

> optAsPattern :: Parser Token (Ident -> ConstrTerm) a
> optAsPattern = flip AsPattern <$-> token At <*> constrTerm2
>          `opt` VariablePattern

> optInfixPattern :: Parser Token (ConstrTerm -> ConstrTerm) a
> optInfixPattern = infixPat <$> gconop <*> constrTerm0
>             `opt` id
>   where infixPat op t2 t1 = InfixPattern t1 op t2

> optTuplePattern :: Parser Token (ConstrTerm -> ConstrTerm) a
> optTuplePattern = tuple <$> many1 (comma <-*> constrTerm0)
>             `opt` ParenPattern
>   where tuple ts t = TuplePattern (t:ts)

> parenMinusPattern :: Parser Token (Ident -> ConstrTerm) a
>                   -> Parser Token (Ident -> ConstrTerm) a
> parenMinusPattern p = p <.> optInfixPattern <.> optTuplePattern

> parenTuplePattern :: Parser Token ConstrTerm a
> parenTuplePattern = constrTerm0 <**> optTuplePattern
>               `opt` TuplePattern []

\end{lstlisting}
\codesubsection{Expressions}
\begin{lstlisting}

> condExpr :: Parser Token a b -> Parser Token CondExpr b
> condExpr eq = CondExpr <$> position <*-> bar <*> expr0 <*-> eq <*> expr

> expr :: Parser Token Expression a
> expr = expr0 <??> (flip Typed <$-> token DoubleColon <*> typeExprWithContext)

> expr0 :: Parser Token Expression a
> expr0 = expr1 `chainr1` (flip InfixApply <$> infixOp)

> expr1 :: Parser Token Expression a
> expr1 = UnaryMinus <$> (minus <|> fminus) <*> expr2
>     <|> expr2

> expr2 :: Parser Token Expression a
> expr2 = lambdaExpr <|> letExpr <|> doExpr <|> ifExpr <|> caseExpr
>     <|> foldl1 Apply <$> many1 expr3

> expr3 :: Parser Token Expression a
> expr3 = constant <|> variable <|> parenExpr <|> listExpr

> constant :: Parser Token Expression a
> constant = Literal <$> literal

> variable :: Parser Token Expression a
> variable = Variable <$> qFunId

> parenExpr :: Parser Token Expression a
> parenExpr = parens pExpr
>   where pExpr = (minus <|> fminus) <**> minusOrTuple
>             <|> Constructor <$> tupleCommas
>             <|> leftSectionOrTuple <\> minus <\> fminus
>             <|> opOrRightSection <\> minus <\> fminus
>           `opt` Tuple []
>         minusOrTuple = flip UnaryMinus <$> expr1 <.> infixOrTuple
>                  `opt` Variable . qualify
>         leftSectionOrTuple = expr1 <**> infixOrTuple
>         infixOrTuple = ($ id) <$> infixOrTuple'
>         infixOrTuple' = infixOp <**> leftSectionOrExp
>                     <|> (.) <$> (optType <.> tupleExpr)
>         leftSectionOrExp = expr1 <**> (infixApp <$> infixOrTuple')
>                      `opt` leftSection
>         optType = flip Typed <$-> token DoubleColon <*> typeExprWithContext
>             `opt` id
>         tupleExpr = tuple <$> many1 (comma <-*> expr)
>               `opt` Paren
>         opOrRightSection = qFunSym <**> optRightSection
>                        <|> colon <**> optCRightSection
>                        <|> infixOp <\> colon <\> qFunSym <**> rightSection
>         optRightSection = (. InfixOp) <$> rightSection `opt` Variable
>         optCRightSection = (. InfixConstr) <$> rightSection `opt` Constructor
>         rightSection = flip RightSection <$> expr0
>         infixApp f e2 op g e1 = f (g . InfixApply e1 op) e2
>         leftSection op f e = LeftSection (f e) op
>         tuple es e = Tuple (e:es)

> infixOp :: Parser Token InfixOp a
> infixOp = InfixOp <$> qfunop
>       <|> InfixConstr <$> colon  

> listExpr :: Parser Token Expression a
> listExpr = brackets (elements `opt` List [])
>   where elements = expr <**> rest
>         rest = comprehension
>            <|> enumeration (flip EnumFromTo) EnumFrom
>            <|> comma <-*> expr <**>
>                (enumeration (flip3 EnumFromThenTo) (flip EnumFromThen)
>                <|> (\es e2 e1 -> List (e1:e2:es)) <$> many (comma <-*> expr))
>          `opt` (\e -> List [e])
>         comprehension = flip ListCompr <$-> bar <*> quals
>         enumeration enumTo enum =
>           token DotDot <-*> (enumTo <$> expr `opt` enum)
>         flip3 f x y z = f z y x

> lambdaExpr :: Parser Token Expression a
> lambdaExpr = Lambda <$-> token Backslash <*> many1 constrTerm2
>                     <*-> (token RightArrow <?> "-> expected") <*> expr

> letExpr :: Parser Token Expression a
> letExpr = Let <$-> token KW_let <*> layout valueDecls
>               <*-> (token KW_in <?> "in expected") <*> expr

> doExpr :: Parser Token Expression a
> doExpr = uncurry Do <$-> token KW_do <*> layout stmts

> ifExpr :: Parser Token Expression a
> ifExpr = IfThenElse <$-> token KW_if <*> expr
>                     <*-> (token KW_then <?> "then expected") <*> expr
>                     <*-> (token KW_else <?> "else expected") <*> expr

> caseExpr :: Parser Token Expression a
> caseExpr = Case <$-> token KW_case <*> expr
>                 <*-> (token KW_of <?> "of expected") <*> layout alts

> alts :: Parser Token [Alt] a
> alts = alt `sepBy1` semicolon

> alt :: Parser Token Alt a
> alt = Alt <$> position <*> constrTerm0
>           <*> rhs (token RightArrow <?> "-> expected")

\end{lstlisting}
\codesubsection{Statements in list comprehensions and \texttt{do} expressions}
Parsing statements is a bit difficult because the syntax of patterns
and expressions largely overlaps. The parser will first try to
recognize the prefix \emph{Pattern}~\texttt{<-} of a binding statement
and if this fails fall back into parsing an expression statement. In
addition, we have to be prepared that the sequence
\texttt{let}~\emph{LocalDefs} can be either a let-statement or the
prefix of a let expression.
\begin{lstlisting}

> stmts :: Parser Token ([Statement],Expression) a
> stmts = stmt reqStmts optStmts

> reqStmts :: Parser Token (Statement -> ([Statement],Expression)) a
> reqStmts = (\(sts,e) st -> (st : sts,e)) <$-> semicolon <*> stmts

> optStmts :: Parser Token (Expression -> ([Statement],Expression)) a
> optStmts = succeed StmtExpr <.> reqStmts
>      `opt` (,) []

> quals :: Parser Token [Statement] a
> quals = stmt (succeed id) (succeed StmtExpr) `sepBy1` comma

> stmt :: Parser Token (Statement -> a) b -> Parser Token (Expression -> a) b
>      -> Parser Token a b
> stmt stmtCont exprCont = letStmt stmtCont exprCont
>                      <|> exprOrBindStmt stmtCont exprCont

> letStmt :: Parser Token (Statement -> a) b -> Parser Token (Expression -> a) b
>         -> Parser Token a b
> letStmt stmtCont exprCont = token KW_let <-*> layout valueDecls <**> optExpr
>   where optExpr = flip Let <$-> token KW_in <*> expr <.> exprCont
>               <|> succeed StmtDecl <.> stmtCont

> exprOrBindStmt :: Parser Token (Statement -> a) b
>                -> Parser Token (Expression -> a) b
>                -> Parser Token a b
> exprOrBindStmt stmtCont exprCont =
>        StmtBind <$> constrTerm0 <*-> leftArrow <*> expr <**> stmtCont
>   <|?> expr <\> token KW_let <**> exprCont

\end{lstlisting}
\codesubsection{Literals, identifiers, and (infix) operators}
\begin{lstlisting}

> char :: Parser Token Char a
> char = cval <$> token CharTok

> int, checkInt :: Parser Token Int a
> int = ival <$> token IntTok
> checkInt = int <?> "integer number expected"

> float, checkFloat :: Parser Token Double a
> float = fval <$> token FloatTok
> checkFloat = float <?> "floating point number expected"

> string :: Parser Token String a
> string = sval <$> token StringTok

> tycon, tyvar :: Parser Token Ident a
> tycon = conId
> tyvar = varId

> qtycon :: Parser Token QualIdent a
> qtycon = qConId

> varId, funId, conId :: Parser Token Ident a
> varId = ident
> funId = ident
> conId = ident

> funSym, conSym :: Parser Token Ident a
> funSym = sym
> conSym = sym

> var, fun, con :: Parser Token Ident a
> var = varId <|> parens (funSym <?> "operator symbol expected")
> fun = funId <|> parens (funSym <?> "operator symbol expected")
> con = conId <|> parens (conSym <?> "operator symbol expected")

> funop, conop :: Parser Token Ident a
> funop = funSym <|> backquotes (funId <?> "operator name expected")
> conop = conSym <|> backquotes (conId <?> "operator name expected")

> qFunId, qConId :: Parser Token QualIdent a
> qFunId = qIdent
> qConId = qIdent

> qFunSym, qConSym :: Parser Token QualIdent a
> qFunSym = qSym
> qConSym = qSym
> gConSym = qConSym <|> colon

> qfun, qcon :: Parser Token QualIdent a
> qfun = qFunId <|> parens (qFunSym <?> "operator symbol expected")
> qcon = qConId <|> parens (qConSym <?> "operator symbol expected")

> qfunop, qconop, gconop :: Parser Token QualIdent a
> qfunop = qFunSym <|> backquotes (qFunId <?> "operator name expected")
> qconop = qConSym <|> backquotes (qConId <?> "operator name expected")
> gconop = gConSym <|> backquotes (qConId <?> "operator name expected")
  
> ident :: Parser Token Ident a
> ident = mkIdent . sval <$> tokens [Id,Id_as,Id_ccall,Id_forall,Id_hiding,
>                                    Id_interface,Id_kind,Id_primitive,
>                                    Id_qualified]

> qIdent :: Parser Token QualIdent a
> qIdent = qualify <$> ident <|> mkQIdent <$> token QId
>   where mkQIdent a = qualifyWith (mkMIdent (modul a)) (mkIdent (sval a))

> mIdent :: Parser Token ModuleIdent a
> mIdent = mIdent <$> tokens [Id,QId,Id_as,Id_ccall,Id_forall,Id_hiding,
>                             Id_interface,Id_kind,Id_primitive,Id_qualified]
>   where mIdent a = mkMIdent (modul a ++ [sval a])

> sym :: Parser Token Ident a
> sym = mkIdent . sval <$> tokens [Sym,Sym_Dot,Sym_Minus,Sym_MinusDot]

> qSym :: Parser Token QualIdent a
> qSym = qualify <$> sym <|> mkQIdent <$> token QSym
>   where mkQIdent a = qualifyWith (mkMIdent (modul a)) (mkIdent (sval a))
  
> colon :: Parser Token QualIdent a
> colon = qConsId <$-> token Colon

> minus :: Parser Token Ident a
> minus = minusId <$-> token Sym_Minus

> fminus :: Parser Token Ident a
> fminus = fminusId <$-> token Sym_MinusDot

> tupleCommas :: Parser Token QualIdent a
> tupleCommas = qTupleId . (1 + ) . length <$> many1 comma

\end{lstlisting}
\codesubsection{Layout}
\begin{lstlisting}

> layout :: Parser Token a b -> Parser Token a b
> layout p = layoutOff <-*> braces p
>        <|> layoutOn <-*> p <*-> (token VRightBrace <|> layoutEnd)

\end{lstlisting}
\codesubsection{More combinators}
\begin{lstlisting}

> braces, brackets, parens, backquotes :: Parser Token a b -> Parser Token a b
> braces p = bracket leftBrace p rightBrace
> brackets p = bracket leftBracket p rightBracket
> parens p = bracket leftParen p rightParen
> backquotes p = bracket backquote p checkBackquote

\end{lstlisting}
\codesubsection{Simple token parsers}
\begin{lstlisting}

> token :: Category -> Parser Token Attributes a
> token c = attr <$> symbol (Token c NoAttributes)
>   where attr (Token _ a) = a

> tokens :: [Category] -> Parser Token Attributes a
> tokens cs = foldr1 (<|>) (map token cs)

> tokenOps :: [(Category,a)] -> Parser Token a b
> tokenOps cs = ops [(Token c NoAttributes,x) | (c,x) <- cs]

> dot, comma, semicolon, bar, equals :: Parser Token Attributes a
> dot = token Sym_Dot
> comma = token Comma
> semicolon = token Semicolon <|> token VSemicolon
> bar = token Bar
> equals = token Equals

> backquote, checkBackquote :: Parser Token Attributes a
> backquote = token Backquote
> checkBackquote = backquote <?> "backquote (`) expected"

> leftParen, rightParen :: Parser Token Attributes a
> leftParen = token LeftParen
> rightParen = token RightParen

> leftBracket, rightBracket :: Parser Token Attributes a
> leftBracket = token LeftBracket
> rightBracket = token RightBracket

> leftBrace, rightBrace :: Parser Token Attributes a
> leftBrace = token LeftBrace
> rightBrace = token RightBrace

> leftArrow :: Parser Token Attributes a
> leftArrow = token LeftArrow

\end{lstlisting}
