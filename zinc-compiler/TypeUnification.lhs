% $Id: TypeUnification.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeUnification.lhs}
\codesection{Type unification}
The unification uses Robinson's algorithm (cf., e.g., Chap.~9
of~\cite{PeytonJones87:Book}).
\begin{lstlisting}

> module TypeUnification where
> import Position
> import Ident
> import Types
> import TypeInferenceMonad
> import TypeSubst
> import Pretty
> import Combined
> import TypeErrors(typeMismatch)
> import Error(errorAt)
> import Monad

> unify :: Position -> String -> Doc -> ModuleIdent -> Type -> Type
>       -> TcState ()
> unify p what doc m ty1 ty2 =
>   liftSt $ {-$-}
>   do
>     theta <- fetchSt
>     let ty1' = subst theta ty1
>     let ty2' = subst theta ty2
>     maybe (errorAt p (typeMismatch what doc m ty1' ty2'))
>           (updateSt_ . compose)
>           (unifyTypes ty1' ty2')

> unifyTypeWithContext :: Position -> String -> Doc -> ModuleIdent ->
>                         TypeWithContext -> TypeWithContext ->
>                         TcState TypeContext
> unifyTypeWithContext p what doc m tyWC1 tyWC2 =
>   do unify p what doc m (removeTypeContext tyWC1) (removeTypeContext tyWC2)
>      theta <- liftSt $ fetchSt
>      return (subst theta ctx)
>   where ctx = composeTypeContext (typeContext tyWC1) (typeContext tyWC2)

> class SubstType a => TypeUnification a where
>   unifyTypes :: a -> a -> Maybe TypeSubst

> instance TypeUnification Type where
>   unifyTypes (TypeVariable tv1) (TypeVariable tv2)
>     | tv1 == tv2 = Just idSubst
>     | otherwise = Just (bindSubst tv1 (TypeVariable tv2) idSubst)
>   unifyTypes (TypeVariable tv) ty
>     | tv `elem` typeVars ty = Nothing
>     | otherwise = Just (bindSubst tv ty idSubst)
>   unifyTypes ty (TypeVariable tv)
>     | tv `elem` typeVars ty = Nothing
>     | otherwise = Just (bindSubst tv ty idSubst)
>   unifyTypes (TypeConstrained tys1 tv1) (TypeConstrained tys2 tv2)
>     | tv1 == tv2 = Just idSubst
>     | tys1 == tys2 = Just (bindSubst tv1 (TypeConstrained tys2 tv2) idSubst)
>   unifyTypes (TypeConstrained tys tv) ty =
>     liftM (bindSubst tv ty) (foldr mplus Nothing (map (unifyTypes ty) tys))
>   unifyTypes ty (TypeConstrained tys tv) =
>     liftM (bindSubst tv ty) (foldr mplus Nothing (map (unifyTypes ty) tys))
>   unifyTypes (TypeConstructor tc1) (TypeConstructor tc2)
>     | tc1 == tc2 = Just idSubst
>   unifyTypes (TypeSkolem k1) (TypeSkolem k2)
>     | k1 == k2 = Just idSubst
>   unifyTypes (TypeApplication ty1 ty2) (TypeApplication ty1' ty2') =
>     unifyTypes ty2 ty2' >>= unifyTypesTheta ty1 ty1'
>     where unifyTypesTheta :: Type -> Type -> TypeSubst -> Maybe TypeSubst
>           unifyTypesTheta ty1 ty2 theta =
>             fmap (compose theta) $ unifyTypes (subst theta ty1) (subst theta ty2)
>   unifyTypes _ _ = Nothing

> instance TypeUnification TypeWithContext where
>   unifyTypes (TypeWithContext ctx1 ty1) (TypeWithContext ctx2 ty2) =
>     unifyTypes ty1 ty2 >>= unifyTypesTheta ctx1 ctx2
>     where unifyTypesTheta :: TypeContext -> TypeContext -> TypeSubst ->
>                              Maybe TypeSubst
>           unifyTypesTheta ctx1 ctx2 theta =
>             fmap (compose theta) $ unifyTypes (subst theta ctx1) (subst theta ctx2)

> instance TypeUnification TypeContext where
>   unifyTypes (TypeContext classConstr1) (TypeContext classConstr2) =
>     unifyTypes classConstr1 classConstr2

> instance TypeUnification TypeClassConstraint where
>   unifyTypes (TypeClassConstraint tc1 ty1) (TypeClassConstraint tc2 ty2)
>     | tc1 == tc2 = unifyTypes ty1 ty2

> instance TypeUnification a => TypeUnification [a] where
>   unifyTypes [] []         = Just idSubst
>   unifyTypes (x:xs) (y:ys) = 
>     unifyTypes x y >>= unifyTypesTheta xs ys
>     where unifyTypesTheta :: TypeUnification b => [b] -> [b] -> TypeSubst ->
>                              Maybe TypeSubst
>           unifyTypesTheta xs ys theta =
>             fmap (compose theta) $ unifyTypes (subst theta xs) (subst theta ys)

\end{lstlisting}
