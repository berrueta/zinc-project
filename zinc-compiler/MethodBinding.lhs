% $Id: MethodBinding.lhs 1320 2004-09-05 16:51:25Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Method binding.lhs}
\codesection{Bind method types into the type environment}
\begin{lstlisting}

> module MethodBinding where
> import TypeExpr
> import TypeClassEnv
> import ValueEnv
> import Ident
> import TopEnv
> import TypeInstGen
> import Set
> import Env
> import TypeTrans
> import Maybe
> import TypeConstructorEnv
> import TypeExpansion

\end{lstlisting}
This module binds the method types into the value environment. They
are computed from the information in the type class environment, in
a similar fashion as data constructors are computed from the
information in the type constructor environment.

All the type classes (and methods) are processed by this module,
both local and imported.
\begin{lstlisting}

> bindMethods :: TypeClassEnv -> TCEnv -> ValueEnv -> ValueEnv
> bindMethods typeClassEnv tcEnv tyEnv =
>   foldr (bindTypeClassMethods tcEnv) tyEnv (allBindings typeClassEnv)

> bindTypeClassMethods :: TCEnv -> (QualIdent,TypeClassInfo) -> ValueEnv -> ValueEnv
> bindTypeClassMethods tcEnv (qid,TypeClassInfo tc tv _ sigEnv) tyEnv =
>   foldr (bindTypeClassMethod mmid tcEnv tc tv) tyEnv (envToList sigEnv)
>   where mmid = fst (splitQualIdent qid)

\end{lstlisting}
Two transformations are performed to the type signature of the method
provided by the programmer. One of them is alias expansion. The second
one is to add a new type class constraint to the context of the type
expression. \textbf{It is added at the beginning}. Do not change this,
because later phases of the compiler depend on this (they assume they can
determine the type class of a method by peeking the first type class
constraint of its type).
\begin{lstlisting}

> bindTypeClassMethod :: Maybe ModuleIdent -> TCEnv -> QualIdent -> Ident ->
>                        (Ident,TypeExprWithContext) -> ValueEnv -> ValueEnv
> bindTypeClassMethod mmid tcEnv typeClass tv (id,tyexprWC) valueEnv =
>   bind id vi valueEnv
>   where vi :: ValueInfo
>         vi = Value qualFun ts
>         bind = case mmid of
>                   (Nothing) -> bindImportTopEnv m
>                   (Just m') -> qualBindImportTopEnv m'
>         qualFun = qualifyWith m id
>         m = fromJust $ fst $ splitQualIdent typeClass
>         tyexprWC' = mapTypeExprContext
>                       (appendTypeExprClassConstraint
>                          typeClass (TypeExprVariable tv)) tyexprWC
>         ts = expandPolyType tcEnv tyexprWC'

\end{lstlisting}
