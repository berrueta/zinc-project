% $Id: AliasCheck.lhs 1413 2004-09-11 17:50:21Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2003-2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{AliasCheck.lhs}
\codesection{Checking alias types}
This module inherits from the deprecated KindCheck module. It
checks type expressions searching for unsatured type alias
applications, which are invalid.
\begin{lstlisting}

> module AliasCheck(aliasCheckTypeExpr,aliasCheckTypeExprWithContext) where
> import Base
> import Ident
> import TypeConstructorEnv
> import CurrySyntax
> import TypeExpr
> import Position
> import Error(errorAt,internalError)
> import TypeExpr
> import KindErrors

> aliasCheckTypeExprWithContext :: Position -> TCEnv ->
>                             TypeExprWithContext -> TypeExprWithContext
> aliasCheckTypeExprWithContext p tcEnv (TypeExprWithContext ctx tyexpr) =
>   TypeExprWithContext ctx (aliasCheckTypeExpr p tcEnv tyexpr)

> aliasCheckTypeExpr :: Position -> TCEnv -> TypeExpr -> TypeExpr
> aliasCheckTypeExpr p tcEnv ty@(TypeExprConstructor tc) =
>   aliasCheckTypeExpr' p tcEnv 0 ty
> aliasCheckTypeExpr p tcEnv ty@(TypeExprVariable tv) =
>   aliasCheckTypeExpr' p tcEnv 0 ty
> aliasCheckTypeExpr p tcEnv ty@(TypeExprApplication ty1 ty2) =
>   typeExprApply (aliasCheckTypeExpr' p tcEnv (length args) leftmost)
>                 (map (aliasCheckTypeExpr p tcEnv) args)
>   where leftmost = leftmostTypeExpr ty
>         args = typeExprArguments ty

> aliasCheckTypeExpr' :: Position -> TCEnv -> Int -> TypeExpr -> TypeExpr
> aliasCheckTypeExpr' p tcEnv actualArity ty@(TypeExprConstructor tc)
>   | not (isAliasType) = ty
>   | expectedArity == actualArity = ty
>   | otherwise = errorAt p (wrongArity tc expectedArity actualArity ty)
>   where tci = qualLookupTC tc tcEnv
>         isAliasType = case tci of
>           [AliasType _ _ _] -> True
>           [_]               -> False
>           _                 -> internalError "AliasCheck.aliasCheckTypeExpr'"
>         expectedArity = tcArity $ head tci
> aliasCheckTypeExpr' p tcEnv actualArity ty@(TypeExprVariable tv) = ty

\end{lstlisting}
