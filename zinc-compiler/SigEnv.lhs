% $Id: SigEnv.lhs 1413 2004-09-11 17:50:21Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{SigEnv.lhs}
\codesection{Type Signatures}
\begin{lstlisting}

> module SigEnv where
> import Env
> import TypeExpr
> import CurrySyntax
> import Ident
> import TypeConstructorEnv
> import TypeTrans(nameSigTypeWithContext)
> import AliasCheck

\end{lstlisting}
The type checker collects type signatures in a flat environment. All
anonymous variables occurring in a signature are replaced by fresh
names. However, the type is not expanded so that the signature is
available for use in the error message that is printed when the
inferred type is less general than the signature.
\begin{lstlisting}

> type SigEnv = Env Ident TypeExprWithContext

> bindTypeSig :: Ident -> TypeExprWithContext -> SigEnv -> SigEnv
> bindTypeSig = bindEnv

> bindTypeSigs :: TCEnv -> Decl -> SigEnv -> SigEnv
> bindTypeSigs tcEnv (TypeSig p vs tyWC) env =
>   foldr (flip bindTypeSig $
>          nameSigTypeWithContext $
>          aliasCheckTypeExprWithContext p tcEnv tyWC) env vs 
> bindTypeSigs _ _ env = env

> lookupTypeSig :: Ident -> SigEnv -> Maybe TypeExprWithContext
> lookupTypeSig = lookupEnv

> qualLookupTypeSig :: ModuleIdent -> QualIdent -> SigEnv ->
>                      Maybe TypeExprWithContext
> qualLookupTypeSig m f sigs = localIdent m f >>= flip lookupTypeSig sigs

\end{lstlisting}
