% $Id: RenameState.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{RenameState.lhs}
\codesection{Rename state monad}
\begin{lstlisting}

> module RenameState where
> import Combined
> import Ident

\end{lstlisting}
A global state transformer is used for generating fresh integer keys
by which the variables get renamed.
\begin{lstlisting}

> type RenameState a = StateT Int Id a

> runRenameState :: RenameState a -> Int -> (a,Int)
> runRenameState m initKey = runSt m' (initKey + 1)
>   where m' = do v <- m
>                 finalKey <- newId
>                 return (v,finalKey)

> newId :: RenameState Int
> newId = updateSt (1 +)

> globalKey :: Int
> globalKey = uniqueId (mkIdent "")

\end{lstlisting}
\ToDo{Probably the state transformer should use an \texttt{Integer} 
counter.}
