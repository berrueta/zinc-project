{-
  $Id: TypeConstructorEnvTest.hs 611 2004-05-10 12:06:19Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeConstructorEnvTest where

import HUnit
import TypeConstructorEnv
import Types
import Ident
import TopEnv(emptyTopEnv)

mid :: ModuleIdent
mid = mkMIdent ["demoModule"]

id1,varId1 :: Ident
id1 = mkIdent "id1"
varId1 = mkIdent "a"

qId1 :: QualIdent
qId1 = qualifyWith mid id1

tuple2DataInfo,tuple3DataInfo :: TypeInfo
tuple2DataInfo = DataType (qTupleId 2) 2 [Just (Data (tupleId 2) 0 [TypeVariable 0,TypeVariable 1])]
tuple3DataInfo = DataType (qTupleId 3) 3 [Just (Data (tupleId 3) 0 [TypeVariable 0,TypeVariable 1,TypeVariable 2])]

-- bindTypeInfo/lookupTC/qualLookupTC

testBindLookup = TestLabel "bindTypeInfo/lookupTC/qualLookupTC" (TestList [
  TestCase (assertEqual "empty"         [] 
              (lookupTC id1 emptyTopEnv)), 
  TestCase (assertEqual "empty (qual)"  [] 
              (qualLookupTC qId1 emptyTopEnv)),
  TestCase (assertEqual "2-tuple"       [tuple2DataInfo]
              (lookupTC (tupleId 2) emptyTopEnv)),
  TestCase (assertEqual "2-tuple (qual)" [tuple2DataInfo]
              (qualLookupTC (qTupleId 2) emptyTopEnv)),
  TestCase (assertEqual "3-tuple"       [tuple3DataInfo]
              (lookupTC (tupleId 3) emptyTopEnv)),
  TestCase (assertEqual "3-tuple (qual)" [tuple3DataInfo]
              (qualLookupTC (qTupleId 3) emptyTopEnv)),
  TestCase (assertEqual "id1"       [type1]
              (lookupTC id1 topEnv)),
  TestCase (assertEqual "id1 (qual)" [type1]
              (qualLookupTC qId1 topEnv))
 ])
  where topEnv = bindTestTypes emptyTopEnv
        bindTestTypes = bindLocalTypeInfo DataType mid id1 [varId1] [Nothing]
        type1 = DataType qId1 1 [Nothing]

-- Collects all tests

tests = TestList [
  testBindLookup
  ]

main = runTestTT tests
