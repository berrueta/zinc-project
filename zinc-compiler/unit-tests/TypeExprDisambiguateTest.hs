{-
  $Id: TypeExprDisambiguateTest.hs 1523 2005-02-09 18:40:22Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeExprDisambiguateTest where
import HUnit
import TypeExprDisambiguate
import CurryParser
import Ident
import TypeExpr
import CurrySyntax
import TypeConstructorEnv
import CurryLexer
import LLParseComb
import Error
import PredefTypes
import Position
import qualified CurryParserTest

mid :: ModuleIdent
mid = CurryParserTest.mid

tcEnv :: TCEnv
tcEnv = bindLocalTypeInfo DataType preludeMIdent intId [] [] $
        bindLocalTypeInfo DataType preludeMIdent boolId [] [] $
        bindLocalTypeInfo DataType mid (mkIdent "Position") [] [] $
        initTCEnv

-- declarations used in other test modules

someDecls :: [Decl]
someDecls = disambiguateDecls mid tcEnv
  [CurryParserTest.dataDeclTree1 10,
   CurryParserTest.dataDeclTree2 20,
   CurryParserTest.dataDeclTree3 30,
   CurryParserTest.newtypeDeclTree1 60,
   CurryParserTest.typeDeclTree1 70,
   CurryParserTest.typeDeclTree2 80,
   CurryParserTest.typeClassDeclTree1 40,
   CurryParserTest.typeClassDeclTree2 50,
   CurryParserTest.typeClassDeclTree3 90,
   CurryParserTest.instanceDeclTree1 100,
   CurryParserTest.instanceDeclTree2 110,
   CurryParserTest.instanceDeclTree3 120]


-- disambiguateDecls

demoParseDecls :: String -> [Decl]
demoParseDecls = ok . applyParser decls lexer CurryParserTest.filePath 

code1,code2,code3,code4,code5,code6,code7 :: String
code1 = "data Tree a = Leaf a | Branch (Tree a) (Tree a)"
code2 = "newtype IntTree = IntTree (Tree Int)"
code3 = "type IntTuple = (Int,Int)"
code4 = "data Comb a b = C (a b)"
code5 = "f x = g x where g :: a -> Tree a"
code6 = "class Eq a"
code7 = "class Eq a => Ord a"

tree1orig,tree2orig,tree3orig,tree4orig,tree5orig,tree6orig,tree7orig :: [Decl]
tree1orig = demoParseDecls code1
tree2orig = demoParseDecls code2
tree3orig = demoParseDecls code3
tree4orig = demoParseDecls code4
tree5orig = demoParseDecls code5
tree6orig = demoParseDecls code6
tree7orig = demoParseDecls code7

tree1expected,tree2expected,tree3expected,tree4expected :: Int -> [Decl]
tree5expected,tree6expected,tree7expected :: Int -> [Decl]
tree1expected l =
  [DataDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (mkIdent "Tree")
    [(mkIdent "a")]
    [ConstrDecl
      (Position {file=CurryParserTest.filePath,line=l,column=15})
      []
      (mkIdent "Leaf")
      [TypeExprVariable (mkIdent "a")],
     ConstrDecl
      (Position {file=CurryParserTest.filePath,line=l,column=24})
      []
      (mkIdent "Branch")
      [TypeExprApplication
        (TypeExprConstructor (qualify $ mkIdent "Tree"))
        (TypeExprVariable (mkIdent "a")),
       TypeExprApplication
        (TypeExprConstructor (qualify $ mkIdent "Tree"))
        (TypeExprVariable (mkIdent "a"))
      ]
    ]
    []
  ]
tree2expected l =
  [NewtypeDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (mkIdent "IntTree")
    []
    (NewConstrDecl
      (Position {file=CurryParserTest.filePath,line=l,column=19})
      []
      (mkIdent "IntTree")
      (TypeExprApplication
        (TypeExprConstructor $ qualify $ mkIdent "Tree")
        (TypeExprConstructor $ qualify $ mkIdent "Int")
      )
    )
    []
  ]
tree3expected l =
  [TypeDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (mkIdent "IntTuple")
    []
    (TypeExprApplication
      (TypeExprApplication
        (TypeExprConstructor $ qTupleId 2)
        (TypeExprConstructor $ qualify $ mkIdent "Int")
      )
      (TypeExprConstructor $ qualify $ mkIdent "Int")
    )
  ]
tree4expected l =
  [DataDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (mkIdent "Comb")
    [mkIdent "a",mkIdent "b"]
    [ConstrDecl
      (Position {file=CurryParserTest.filePath,line=l,column=17})
      []
      (mkIdent "C")
      [TypeExprApplication
        (TypeExprVariable $ mkIdent "a")
        (TypeExprVariable $ mkIdent "b")]
    ]
    []
  ]
tree5expected l =
  [FunctionDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (mkIdent "f")
    [Equation
      (Position {file=CurryParserTest.filePath,line=l,column=1})
      (FunLhs (mkIdent "f") [VariablePattern $ mkIdent "x"])
      (SimpleRhs
        (Position {file=CurryParserTest.filePath,line=l,column=7})
        (Apply
          (Variable $ qualify $ mkIdent "g")
          (Variable $ qualify $ mkIdent "x"))
        [TypeSig
          (Position {file=CurryParserTest.filePath,line=l,column=17})
          [mkIdent "g"]
          (TypeExprWithContext emptyTypeExprContext
            (typeExprApplyArrowConstructor
              (TypeExprVariable $ mkIdent "a")
              (TypeExprApplication
                (TypeExprConstructor $ qualify $ mkIdent "Tree")
                (TypeExprVariable $ mkIdent "a"))))])]]
tree6expected l = tree6orig
tree7expected l =
  [TypeClassDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (TypeExprContext
      [TypeExprClassConstraint (qualify $ mkIdent "Eq") (TypeExprVariable $ mkIdent "a")])
    (mkIdent "Ord")
    (mkIdent "a")
    []
   ]

testDisambiguateDecls = TestLabel "disambiguateDecls" (TestList [
  TestCase (assertEqual "code1"
    (tree1expected 1)
    (disambiguateDecls mid tcEnv $ tree1orig)),
  TestCase (assertEqual "code3"
    (tree3expected 1)
    (disambiguateDecls mid tcEnv $ tree3orig)),
  TestCase (assertEqual "code4"
    (tree4expected 1)
    (disambiguateDecls mid tcEnv $ tree4orig)),
  TestCase (assertEqual "code1+code2+code3+code4+code5+code6+code7"
    (tree1expected 1 ++ tree2expected 1 ++ tree3expected 1 ++
     tree4expected 1 ++ tree5expected 1 ++ tree6expected 1 ++
     tree7expected 1)
    (disambiguateDecls mid tcEnv $
       tree1orig ++ tree2orig ++ tree3orig ++ tree4orig ++ tree5orig ++
       tree6orig ++ tree7orig))
  ])

-- collects all tests

tests = TestList [
  testDisambiguateDecls
  ]

main = runTestTT tests
