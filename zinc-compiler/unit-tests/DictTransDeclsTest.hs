{-
  $Id: DictTransDeclsTest.hs 1163 2004-08-28 18:48:03Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module DictTransDeclsTest where

import HUnit
import DictTransDecls
import TopEnv
import ValueEnv
import Env
import Types
import CurrySyntax
import Ident
import TypeClassEnv
import TypeExpr
import PredefTypes
import TypeInstGen
import Set
import TypeTrans
import InstanceEnv
import DualTopEnv
import TypeConstructorEnv
import DictBinding
import DictUtils
import RenameState
import DictParEnv

mid :: ModuleIdent
mid = mkMIdent ["mid"]

qid1,qid2 :: QualIdent
qid1 = qualifyWith mid $ mkIdent "MyEq"
qid2 = qualifyWith mid $ mkIdent "MyOrd"
qid3 = qualifyWith mid $ mkIdent "f"
qid4 = qualifyWith mid $ id1
qid5 = qualifyWith mid $ id2

id1,id2 :: Ident
id1 = mkIdent "eq"
id2 = mkIdent "comp"
id3 = unqualify qid3
id4 = mkIdent "g"

ts1 :: TypeScheme
ts1 = gen zeroSet (TypeWithContext emptyTypeContext ty1)

ty1,ty2,ty3,ty4 :: Type
ty1 = toType [] $
        typeExprApplyArrowConstructor
          (TypeExprVariable $ mkIdent "a")
          (TypeExprConstructor qBoolId)
ty2 = toType [] $
        typeExprApplyArrowConstructor
          (typeExprApplyListConstructor $ TypeExprVariable $ mkIdent "a")
          (TypeExprConstructor qBoolId)        
ty3= toType [] $
        typeExprApplyArrowConstructor
          (TypeExprConstructor qIntId)
          (TypeExprConstructor qBoolId)        
ty4= toType [] $
        typeExprApplyArrowConstructor
          (typeExprApplyListConstructor $ TypeExprConstructor qIntId)
          (TypeExprConstructor qBoolId)        

v1 :: Type
v1 = TypeVariable 0

tcc1,tcc2 :: TypeClassConstraint
tcc1 = TypeClassConstraint qid1 v1
tcc2 = TypeClassConstraint qid2 v1

twc1,twc2 :: TypeWithContext
twc1 = TypeWithContext (TypeContext [tcc1]) ty1
twc2 = TypeWithContext (TypeContext [tcc1,tcc2]) ty1
twc3 = TypeWithContext (TypeContext [tcc2]) ty1
twc4 = TypeWithContext (TypeContext [tcc1]) ty2
twc5 = TypeWithContext (emptyTypeContext) ty3

parEnv :: ParEnv
parEnv = emptyEnv

typeClassEnv :: TypeClassEnv
typeClassEnv = qualBindImportTopEnv mid (unqualify qid1) tci1 $
               qualBindImportTopEnv mid (unqualify qid2) tci2 $
               emptyTopEnv
  where tci1 = TypeClassInfo qid1 (mkIdent "a") []
                 (foldr (uncurry bindEnv) emptyEnv
                    [(id1,TypeExprWithContext
                            emptyTypeExprContext
                            (typeExprApplyArrowConstructor
                              (TypeExprVariable $ mkIdent "a")
                              (TypeExprConstructor qBoolId)))])
        tci2 = TypeClassInfo qid1 (mkIdent "b") [qid1]
                 (foldr (uncurry bindEnv) emptyEnv
                    [(id2,TypeExprWithContext
                            emptyTypeExprContext
                            (typeExprApplyArrowConstructor
                              (TypeExprVariable $ mkIdent "b")
                              (TypeExprConstructor qBoolId)))])

tcEnv :: TCEnv
tcEnv = initTCEnv

tyEnv :: ValueEnv
tyEnv = bindFun mid id3 ts1 $
        initDCEnv

instEnv :: InstanceEnv
instEnv = bindImportDualTopEnv mid qid1 qIntId ii1 $ 
          bindImportDualTopEnv mid qid1 qListId ii2 $ 
          emptyDualTopEnv
  where ii1 = importedInstance mid qid1 qIntId []
        ii2 = importedInstance mid qid1 qListId [[qid1]]

tyEnv' = snd $ bindDict typeClassEnv instEnv (tcEnv,tyEnv)
-- demos

--envs' = envs { getParEnv = expandParEnv typeClassEnv $ fst $ runRenameState (buildParEnv twc3) globalKey }

--demoResolvePH x = resolvePH instEnv parEnv' x
--  where parEnv' = expandParEnv typeClassEnv $ fst $ run (buildParEnv twc3) globalKey

-- collects all tests

tests = TestList [
  ]

main = runTestTT tests
