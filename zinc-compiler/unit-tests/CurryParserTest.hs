{-
  $Id: CurryParserTest.hs 1523 2005-02-09 18:40:22Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module CurryParserTest where

import HUnit
import CurryParser
import CurryLexer
import CurrySyntax
import Ident
import Error
import LLParseComb
import Position
import TypeExpr
import Kind
import CurryPP

filePath :: FilePath
filePath = ""

mid :: ModuleIdent
mid = mkMIdent ["demo"]

emptyModuleIdent :: ModuleIdent
emptyModuleIdent = mkMIdent []

-- some programs and subprograms

moduleCode1,moduleCode2 :: String
moduleCode1 = ""
moduleCode2 = "module mid where\n" ++ declCode1

declCode1 :: String
declCode1 = "id1 = 5\n"

moduleTree1,moduleTree2 :: Module
moduleTree1 = Module mainMIdent Nothing []
moduleTree2 = Module (mkMIdent ["mid"]) Nothing [declTree1 2]

declTree1 :: Int -> Decl
declTree1 line' =
  FunctionDecl (Position {file=filePath,line=line',column=1}) (mkIdent "id1")
    [Equation (Position {file=filePath,line=line',column=1})
      (FunLhs (mkIdent "id1") [])
      (SimpleRhs (Position {file=filePath,line=line',column=7})
                 (Literal (Int anonId 5)) [])
    ]

-- parseSource

testParseSource = TestLabel "parseSource" (TestList [
  TestCase (assertEqual "moduleCode1" moduleTree1
             (ok $ parseSource filePath moduleCode1)),
  TestCase (assertEqual "moduleCode2" moduleTree2
             (ok $ parseSource filePath moduleCode2))
  ])

-- parseModule: TODO: incomplete test

testParseModule = TestLabel "parseModule" (TestList [
  TestCase (assertEqual "moduleCode1" moduleTree1
    (ok $ applyParser parseModule lexer filePath moduleCode1)),
  TestCase (assertEqual "moduleCode2" moduleTree2
    (ok $ applyParser parseModule lexer filePath moduleCode2))
  ])

-- moduleHeader, exportSpec, export
-- parseInterface, parseIface
-- parseGoal, goal
-- decls, localDefs, globalDecl, importDecl, importSpec, infixDecl
-- infixDeclLhs

-- typeClassDecl

typeClassDeclCode1,typeClassDeclCode2,typeClassDeclCode3 :: String
typeClassDeclCode1 = "class Eq a where (==) :: a -> a -> Bool"
typeClassDeclCode2 = "class Eq a => Ord a where (>) :: a -> a -> Bool"
typeClassDeclCode3 = "class (Pickable m, Eq m) => Movable m where\n  moveTo :: Position -> m -> m\n  getPosition :: m -> Position"

typeClassDeclTree1,typeClassDeclTree2,typeClassDeclTree3 :: Int -> Decl
typeClassDeclTree1 line' =
  TypeClassDecl
    (Position {file=filePath,line=line',column=1})
    emptyTypeExprContext
    (mkIdent "Eq")
    (mkIdent "a")
    [TypeSig
       (Position {file=filePath,line=line',column=18})
       [mkIdent "=="]
       (TypeExprWithContext emptyTypeExprContext
         (typeExprApplyArrowConstructor
           (TypeExprVariable $ mkIdent "a")
           (typeExprApplyArrowConstructor
             (TypeExprVariable $ mkIdent "a")
             (TypeExprVariable $ mkIdent "Bool"))))]
typeClassDeclTree2 line' =
  TypeClassDecl
    (Position {file=filePath,line=line',column=1})
    (TypeExprContext
       [TypeExprClassConstraint (qualify $ mkIdent "Eq") (TypeExprVariable $ mkIdent "a")])
    (mkIdent "Ord")
    (mkIdent "a")
    [TypeSig
       (Position {file=filePath,line=line',column=27})
       [mkIdent ">"]
       (TypeExprWithContext emptyTypeExprContext
         (typeExprApplyArrowConstructor
           (TypeExprVariable $ mkIdent "a")
           (typeExprApplyArrowConstructor
             (TypeExprVariable $ mkIdent "a")
             (TypeExprVariable $ mkIdent "Bool"))))]
typeClassDeclTree3 line' =
  TypeClassDecl
    (Position {file=filePath,line=line',column=1})
    (TypeExprContext
       [TypeExprClassConstraint (qualify $ mkIdent "Pickable") (TypeExprVariable $ mkIdent "m"),
        TypeExprClassConstraint (qualify $ mkIdent "Eq") (TypeExprVariable $ mkIdent "m")])
    (mkIdent "Movable")
    (mkIdent "m")
    [TypeSig
       (Position {file=filePath,line=line'+1,column=3})
       [mkIdent "moveTo"]
       (TypeExprWithContext emptyTypeExprContext
         (typeExprApplyArrowConstructor
           (TypeExprVariable $ mkIdent "Position")
           (typeExprApplyArrowConstructor
             (TypeExprVariable $ mkIdent "m")
             (TypeExprVariable $ mkIdent "m")))),
     TypeSig
       (Position {file=filePath,line=line'+2,column=3})
       [mkIdent "getPosition"]
       (TypeExprWithContext emptyTypeExprContext
         (typeExprApplyArrowConstructor
           (TypeExprVariable $ mkIdent "m")
           (TypeExprVariable $ mkIdent "Position")))]


testTypeClassDecl = TestLabel "typeClassDecl" (TestList [
  TestCase (assertEqual "typeClassDeclCode1" (typeClassDeclTree1 1)
    (ok $ applyParser typeClassDecl lexer filePath typeClassDeclCode1)),
  TestCase (assertEqual "typeClassDeclCode2" (typeClassDeclTree2 1)
    (ok $ applyParser typeClassDecl lexer filePath typeClassDeclCode2)),
  TestCase (assertEqual "typeClassDeclCode3" (typeClassDeclTree3 1)
    (ok $ applyParser typeClassDecl lexer filePath typeClassDeclCode3))
  ])

-- instanceDecl

instanceDeclCode1,instanceDeclCode2,instanceDeclCode3 :: String
instanceDeclCode1 = "instance Eq Int where x == y = True"
instanceDeclCode2 = "instance Eq a => Eq (BinTree a) where\n  Leaf == Leaf = True\n  Leaf == Branch _ _ = False\n  Branch _ _ == Leaf = False\n  Branch x1 x2 == Branch y1 y2 = (x1 == y1) && (x2 == y2)"
instanceDeclCode3 = "instance Eq a => Eq [a] where\n  [] == [] = True\n  (x : xs) == (y : ys) = (x == y) && (xs == ys)\n  _ == _ = False"
   
instanceDeclTree1,instanceDeclTree2,instanceDeclTree3 :: Int -> Decl
instanceDeclTree1 line' =
  InstanceDecl
    (Position {file=filePath,line=line',column=1})
    (TypeExprContext [])
    (qualify $ mkIdent "Eq")
    (TypeExprVariable $ mkIdent "Int")
    [FunctionDecl
      (Position {file=filePath,line=line',column=23})
      (mkIdent "==")
      [Equation
        (Position {file=filePath,line=line',column=23})
        (OpLhs (VariablePattern (mkIdent "x"))
               (mkIdent "==")
               (VariablePattern (mkIdent "y")))
        (SimpleRhs
          (Position {file=filePath,line=line',column=32})
          (Variable $ qualify $ mkIdent "True")
          [])
      ]
    ]
instanceDeclTree2 line' =
  InstanceDecl
    (Position {file=filePath,line=line',column=1})
    (TypeExprContext [TypeExprClassConstraint (qualify $ mkIdent "Eq") (TypeExprVariable $ mkIdent "a")])
    (qualify $ mkIdent "Eq")
    (TypeExprApplication
      (TypeExprVariable $ mkIdent "BinTree")
      (TypeExprVariable $ mkIdent "a"))
    [FunctionDecl
      (Position {file=filePath,line=line'+1,column=3})
      (mkIdent "==")
      [Equation
        (Position {file=filePath,line=line'+1,column=3})
        (OpLhs (VariablePattern (mkIdent "Leaf"))
               (mkIdent "==")
               (VariablePattern (mkIdent "Leaf")))
        (SimpleRhs
          (Position {file=filePath,line=line'+1,column=18})
          (Variable $ qualify $ mkIdent "True")
          [])],
     FunctionDecl
       (Position {file=filePath,line=line'+2,column=3})
       (mkIdent "==") 
       [Equation
         (Position {file=filePath,line=line'+2,column=3})
         (OpLhs (VariablePattern (mkIdent "Leaf"))
                (mkIdent "==")
                (ConstructorPattern (qualify $ mkIdent "Branch")
                                    [VariablePattern (mkIdent "_"),
                                     VariablePattern (mkIdent "_")]))
         (SimpleRhs
           (Position {file=filePath,line=line'+2,column=24})
           (Variable $ qualify $ mkIdent "False")
           [])],
     FunctionDecl
       (Position {file=filePath,line=line'+3,column=3})
       (mkIdent "==")
       [Equation
         (Position {file=filePath,line=line'+3,column=3})
         (OpLhs (ConstructorPattern (qualify $ mkIdent "Branch")
                                    [VariablePattern (mkIdent "_"),
                                     VariablePattern (mkIdent "_")])
                (mkIdent "==")
                (VariablePattern (mkIdent "Leaf")))
         (SimpleRhs
           (Position {file=filePath,line=line'+3,column=24})
           (Variable $ qualify $ mkIdent "False")
           [])],
     FunctionDecl
       (Position {file=filePath,line=line'+4,column=3})
       (mkIdent "==")
       [Equation
         (Position {file=filePath,line=line'+4,column=3})
         (OpLhs (ConstructorPattern (qualify $ mkIdent "Branch")
                                    [VariablePattern (mkIdent "x1"),
                                     VariablePattern (mkIdent "x2")])
                (mkIdent "==")
                (ConstructorPattern (qualify $ mkIdent "Branch")
                                    [VariablePattern (mkIdent "y1"),
                                     VariablePattern (mkIdent "y2")]))
         (SimpleRhs 
           (Position {file=filePath,line=line'+4,column=34})
           (InfixApply
             (Paren
               (InfixApply
                 (Variable $ qualify $ mkIdent "x1")
                 (InfixOp $ qualify $ mkIdent "==")
                 (Variable $ qualify $ mkIdent "y1")))
             (InfixOp $ qualify $ mkIdent "&&")
             (Paren
               (InfixApply
                 (Variable $ qualify $ mkIdent "x2")
                 (InfixOp $ qualify $ mkIdent "==")
                 (Variable $ qualify $ mkIdent "y2"))))
           [])]]
instanceDeclTree3 line' =
  InstanceDecl
    (Position {file=filePath,line=line',column=1})
    (TypeExprContext [TypeExprClassConstraint (qualify $ mkIdent "Eq")
                                              (TypeExprVariable $ mkIdent "a")])
    (qualify $ mkIdent "Eq")
    (typeExprApplyListConstructor $ TypeExprVariable $ mkIdent "a")
    [FunctionDecl
      (Position {file=filePath,line=line'+1,column=3})
      (mkIdent "==")
      [Equation
        (Position {file=filePath,line=line'+1,column=3})
        (OpLhs (ListPattern []) (mkIdent "==") (ListPattern []))
        (SimpleRhs
          (Position {file=filePath,line=line'+1,column=14})
          (Variable $ qualify $ mkIdent "True")
         [])],
     FunctionDecl
       (Position {file=filePath,line=line'+2,column=3})
       (mkIdent "==")
       [Equation
         (Position {file=filePath,line=line'+2,column=3})
         (OpLhs (ParenPattern
                    (InfixPattern
                       (VariablePattern $ mkIdent "x")
                       qConsId
                       (VariablePattern $ mkIdent "xs")))
                (mkIdent "==")
                (ParenPattern
                    (InfixPattern
                      (VariablePattern $ mkIdent "y")
                      qConsId
                      (VariablePattern $ mkIdent "ys"))))
         (SimpleRhs
           (Position {file=filePath,line=line'+2,column=26})
           (InfixApply 
             (Paren
               (InfixApply
                 (Variable $ qualify $ mkIdent "x")
                 (InfixOp $ qualify $ mkIdent "==")
                 (Variable $ qualify $ mkIdent "y")))
             (InfixOp $ qualify $ mkIdent "&&")
             (Paren
               (InfixApply
                 (Variable $ qualify $ mkIdent "xs")
                 (InfixOp $ qualify $ mkIdent "==")
                 (Variable $ qualify $ mkIdent "ys"))))
           [])],
     FunctionDecl
       (Position {file=filePath,line=line'+3,column=3})
       (mkIdent "==")
       [Equation
         (Position {file=filePath,line=line'+3,column=3})
         (OpLhs (VariablePattern $ mkIdent "_")
                (mkIdent "==")
                (VariablePattern $ mkIdent "_"))
         (SimpleRhs
           (Position {file=filePath,line=line'+3,column=12})
           (Variable $ qualify $ mkIdent "False")
           [])]]

testInstanceDecl = TestLabel "instanceDecl" (TestList [
  TestCase (assertEqual "instanceDeclCode1" (instanceDeclTree1 1)
    (ok $ applyParser instanceDecl lexer filePath instanceDeclCode1)),
  TestCase (assertEqual "instanceDeclCode2" (instanceDeclTree2 1)
    (ok $ applyParser instanceDecl lexer filePath instanceDeclCode2)),
  TestCase (assertEqual "instanceDeclCode3" (instanceDeclTree3 1)
    (ok $ applyParser instanceDecl lexer filePath instanceDeclCode3))
  ])

-- dataDecl

dataDeclCode1, dataDeclCode2,dataDeclCode3 :: String
dataDeclCode1 =
  "data TC1 a = Sum (TC1 a) (TC1 a)\n" ++
  "           | Simple (TC2 a)\n"
dataDeclCode2 =
  "data TC2 a = Product (TC2 a) (TC2 a)\n" ++
  "           | Constant Int\n" ++
  "           | Parens (TC1 a)\n"
dataDeclCode3 =
  "data BinTree a = Leaf\n" ++
  "               | Branch a a\n" ++
  "               deriving (Eq,Show)"

dataDeclTree1,dataDeclTree2,dataDeclTree3 :: Int -> Decl
dataDeclTree1 line' =
  DataDecl (Position {file=filePath,line=line',column=1})
           (mkIdent "TC1")
           [(mkIdent "a")]
           [ConstrDecl
             (Position {file=filePath,line=line',column=14})
             []
             (mkIdent "Sum")
             [typeExprApply (TypeExprVariable $ mkIdent "TC1")
                            [TypeExprVariable (mkIdent "a")],
              typeExprApply (TypeExprVariable $ mkIdent "TC1")
                            [TypeExprVariable (mkIdent "a")]],
            ConstrDecl
              (Position {file=filePath,line=line'+1,column=14})
              []
              (mkIdent "Simple")
              [typeExprApply (TypeExprVariable $ mkIdent "TC2")
                             [TypeExprVariable (mkIdent "a")]]
           ]
           []
dataDeclTree2 line' =
  DataDecl
    (Position {file=filePath,line=line',column=1})
    (mkIdent "TC2")
    [(mkIdent "a")]
    [ConstrDecl
       (Position {file=filePath,line=line',column=14})
       []
       (mkIdent "Product")
       [typeExprApply (TypeExprVariable $ mkIdent "TC2")
                      [TypeExprVariable (mkIdent "a")],
        typeExprApply (TypeExprVariable $ mkIdent "TC2")
                      [TypeExprVariable (mkIdent "a")]],
     ConstrDecl
       (Position {file=filePath,line=line'+1,column=14})
       []
       (mkIdent "Constant")
       [TypeExprVariable (mkIdent "Int")],
     ConstrDecl
       (Position {file=filePath,line=line'+2,column=14})
       []
       (mkIdent "Parens")
       [typeExprApply (TypeExprVariable $ mkIdent "TC1")
                      [TypeExprVariable (mkIdent "a")]]
    ]
    []
dataDeclTree3 line' =
  DataDecl
    (Position {file=filePath,line=line',column=1})
    (mkIdent "BinTree")
    [(mkIdent "a")]
    [ConstrDecl
       (Position {file=filePath,line=line',column=18})
       []
       (mkIdent "Leaf")
       [],
     ConstrDecl
       (Position {file=filePath,line=line'+1,column=18})
       []
       (mkIdent "Branch")
       [TypeExprVariable (mkIdent "a"),TypeExprVariable (mkIdent "a")]
    ]
    [mkIdent "Eq",mkIdent "Show"]

testDataDecl = TestLabel "dataDecl" (TestList [
  TestCase (assertEqual "dataDeclCode1" (dataDeclTree1 1)
    (ok $ applyParser dataDecl lexer filePath dataDeclCode1)),
  TestCase (assertEqual "dataDeclCode2" (dataDeclTree2 1)
    (ok $ applyParser dataDecl lexer filePath dataDeclCode2)),
  TestCase (assertEqual "dataDeclCode3" (dataDeclTree3 1)
    (ok $ applyParser dataDecl lexer filePath dataDeclCode3))
  ])

-- newtypeDecl

newtypeDeclCode1 :: String
newtypeDeclCode1 =
  "newtype TupleTree t1 t2 = TupleTree (BinTree (t1,t2))\n"

newtypeDeclTree1 :: Int -> Decl
newtypeDeclTree1 line' =
  NewtypeDecl
    (Position {file=filePath,line=line',column=1})
    (mkIdent "TupleTree")
    [mkIdent "t1",mkIdent "t2"]
    (NewConstrDecl
      (Position {file=filePath,line=line',column=27})
      []
      (mkIdent "TupleTree")
      (typeExprApply
        (TypeExprVariable $ mkIdent "BinTree")
        [typeExprApplyTupleConstructor
           [TypeExprVariable (mkIdent "t1"),
            TypeExprVariable (mkIdent "t2")]]))
    []

testNewtypeDecl = TestLabel "newtypeDecl" (TestList [
  TestCase (assertEqual "newtypeDeclCode1" (newtypeDeclTree1 1)
    (ok $ applyParser newtypeDecl lexer filePath newtypeDeclCode1))
  ])

-- typeDecl

typeDeclCode1,typeDeclCode2 :: String
typeDeclCode1 =
  "type IntTree = BinTree Int\n"
typeDeclCode2 =
  "type TS a = TC1 [a]\n"

typeDeclTree1,typeDeclTree2 :: Int -> Decl
typeDeclTree1 line' =
  TypeDecl
     (Position {file=filePath,line=line',column=1})
     (mkIdent "IntTree")
     []
     (typeExprApply
        (TypeExprVariable $ mkIdent "BinTree")
        [TypeExprVariable $ mkIdent "Int"])
typeDeclTree2 line' =
  TypeDecl
     (Position {file=filePath,line=line',column=1})
     (mkIdent "TS")
     [mkIdent "a"]
     (typeExprApply
        (TypeExprVariable $ mkIdent "TC1")
        [typeExprApplyListConstructor
           (TypeExprVariable (mkIdent "a"))])

testTypeDecl = TestLabel "typeDecl" (TestList [
  TestCase (assertEqual "typeDeclCode1" (typeDeclTree1 1)
    (ok $ applyParser typeDecl lexer filePath typeDeclCode1)),
  TestCase (assertEqual "typeDeclCode2" (typeDeclTree2 1)
    (ok $ applyParser typeDecl lexer filePath typeDeclCode2))
  ])

-- typeDeclLhs
-- constrDecl, nconstrDecl, constr, functionDecl, valueDecl
-- funListDecl, valListDecl, declRhs, rhs, externalDecl

-- intfDecl

testIntfDecl = TestLabel "Decl" (TestList [
  TestCase (assertEqual "iKindDeclCode1" (iKindDeclTree1 1)
    (ok $ applyParser intfDecl lexer filePath iKindDeclCode1)),
  TestCase (assertEqual "iKindDeclCode2" (iKindDeclTree2 1)
    (ok $ applyParser intfDecl lexer filePath iKindDeclCode2))
  ])

-- iImportDecl, iInfixDecl
-- TODO(dbm): write tests

-- iTypeClassDecl

iTypeClassDeclCode1,iTypeClassDeclCode2,iTypeClassDeclCode3 :: String
iTypeClassDeclCode1 = "class Eq a where {\n  (==) :: a -> a -> Bool\n}"
iTypeClassDeclCode2 = "class Eq a => Ord a where {\n  (>) :: a -> a -> Bool\n}"
iTypeClassDeclCode3 = "class (Pickable m, Eq m) => Movable m where {\n  moveTo :: Position -> m -> m;\n  getPosition :: m -> Position\n}"

iTypeClassDeclTree1,iTypeClassDeclTree2,iTypeClassDeclTree3 :: Int -> IDecl
iTypeClassDeclTree1 line' =
  ITypeClassDecl
    (Position {file=filePath,line=line',column=1})
    emptyTypeExprContext
    (qualify $ mkIdent "Eq")
    (mkIdent "a")
    [IFunctionDecl
       (Position {file=filePath,line=line'+1,column=3})
       (qualify $ mkIdent "==")
       (TypeExprWithContext emptyTypeExprContext
         (typeExprApplyArrowConstructor
           (TypeExprVariable $ mkIdent "a")
           (typeExprApplyArrowConstructor
             (TypeExprVariable $ mkIdent "a")
             (TypeExprVariable $ mkIdent "Bool"))))]
iTypeClassDeclTree2 line' =
  ITypeClassDecl
    (Position {file=filePath,line=line',column=1})
    (TypeExprContext
       [TypeExprClassConstraint (qualify $ mkIdent "Eq") (TypeExprVariable $ mkIdent "a")])
    (qualify $ mkIdent "Ord")
    (mkIdent "a")
    [IFunctionDecl
       (Position {file=filePath,line=line'+1,column=3})
       (qualify $ mkIdent ">")
       (TypeExprWithContext emptyTypeExprContext
         (typeExprApplyArrowConstructor
           (TypeExprVariable $ mkIdent "a")
           (typeExprApplyArrowConstructor
             (TypeExprVariable $ mkIdent "a")
             (TypeExprVariable $ mkIdent "Bool"))))]
iTypeClassDeclTree3 line' =
  ITypeClassDecl
    (Position {file=filePath,line=line',column=1})
    (TypeExprContext
       [TypeExprClassConstraint (qualify $ mkIdent "Pickable") (TypeExprVariable $ mkIdent "m"),
        TypeExprClassConstraint (qualify $ mkIdent "Eq") (TypeExprVariable $ mkIdent "m")])
    (qualify $ mkIdent "Movable")
    (mkIdent "m")
    [IFunctionDecl
       (Position {file=filePath,line=line'+1,column=3})
       (qualify $ mkIdent "moveTo")
       (TypeExprWithContext emptyTypeExprContext
         (typeExprApplyArrowConstructor
           (TypeExprVariable $ mkIdent "Position")
           (typeExprApplyArrowConstructor
             (TypeExprVariable $ mkIdent "m")
             (TypeExprVariable $ mkIdent "m")))),
     IFunctionDecl
       (Position {file=filePath,line=line'+2,column=3})
       (qualify $ mkIdent "getPosition")
       (TypeExprWithContext emptyTypeExprContext
         (typeExprApplyArrowConstructor
           (TypeExprVariable $ mkIdent "m")
           (TypeExprVariable $ mkIdent "Position")))]

testITypeClassDecl = TestLabel "iTypeClassDecl" (TestList [
  TestCase (assertEqual "iTypeClassDeclCode1" (iTypeClassDeclTree1 1)
    (ok $ applyParser iTypeClassDecl lexer filePath iTypeClassDeclCode1)),
  TestCase (assertEqual "iTypeClassDeclCode2" (iTypeClassDeclTree2 1)
    (ok $ applyParser iTypeClassDecl lexer filePath iTypeClassDeclCode2)),
  TestCase (assertEqual "iTypeClassDeclCode3" (iTypeClassDeclTree3 1)
    (ok $ applyParser iTypeClassDecl lexer filePath iTypeClassDeclCode3))
  ])

-- iHiddingDecl, iDataDecl
-- iNewtypeDecl, iTypeDecl, iTypeDeclLhs, iFunctionDecl
-- TODO(dbm): write tests

-- iKindDecl

iKindDeclCode1,iKindDeclCode2 :: String
iKindDeclCode1 = "kind Int " ++ kindCode1
iKindDeclCode2 = "kind Maybe " ++ kindCode2

iKindDeclTree1 :: Int -> IDecl
iKindDeclTree1 line' =
  IKindDecl
    (Position {file=filePath,line=line',column=1})
    (qualify $ mkIdent "Int")
    kind1
iKindDeclTree2 line' =
  IKindDecl
    (Position {file=filePath,line=line',column=1})
    (qualify $ mkIdent "Maybe")
    kind2

testIKindDecl = TestLabel "iKindDecl" (TestList [
  TestCase (assertEqual "iTypeClassDeclCode1" (iKindDeclTree1 1)
    (ok $ applyParser iKindDecl lexer filePath iKindDeclCode1)),
  TestCase (assertEqual "iTypeClassDeclCode2" (iKindDeclTree2 1)
    (ok $ applyParser iKindDecl lexer filePath iKindDeclCode2))
  ])

-- TYPES

-- type0, type1, type2, anonType, identType, parenType, listType
-- literal

typeCode1,typeCode2,typeCode3,typeCode4,typeCode5 :: String
typeCode6,typeCode7,typeCode8,typeCode9,typeCode10:: String
typeCode11,typeCode12,typeCode13 :: String
typeCode1 = "a"
typeCode2 = "Int"
typeCode3 = "String"
typeCode4 = "[String]"
typeCode5 = "(a, Int)"
typeCode6 = "a -> a"
typeCode7 = "a -> a -> Int"
typeCode8 = "IO a"
typeCode9 = "(" ++ typeCode6 ++ ") -> String -> IO a"
typeCode10= "String -> (" ++ typeCode6 ++ ") -> IO a"
typeCode11= "()"
typeCode12= "(a)"
typeCode13= "[]"

typeExpr1,typeExpr2,typeExpr3,typeExpr4,typeExpr5 :: TypeExpr
typeExpr6,typeExpr7,typeExpr8,typeExpr9,typeExpr10:: TypeExpr
typeExpr11,typeExpr12,typeExpr13 :: TypeExpr
typeExpr1 = TypeExprVariable (mkIdent "a")
typeExpr2 = TypeExprVariable (mkIdent "Int")
typeExpr3 = TypeExprVariable (mkIdent "String")
typeExpr4 = typeExprApplyListConstructor (typeExpr3)
typeExpr5 = typeExprApplyTupleConstructor [typeExpr1,typeExpr2]
typeExpr6 = typeExprApplyArrowConstructor typeExpr1 typeExpr1
typeExpr7 = typeExprApplyArrowConstructor typeExpr1 (typeExprApplyArrowConstructor typeExpr1 typeExpr2)
typeExpr8 = typeExprApply (TypeExprVariable $ mkIdent "IO") [TypeExprVariable (mkIdent "a")]
typeExpr9 = typeExprApplyArrowConstructor typeExpr6 (typeExprApplyArrowConstructor typeExpr3 typeExpr8)
typeExpr10= typeExprApplyArrowConstructor typeExpr3 (typeExprApplyArrowConstructor typeExpr6 typeExpr8)
typeExpr11= typeExprUnitConstructor
typeExpr12= typeExpr1
typeExpr13 = typeExprListConstructor


testType0 = TestLabel "type0" (TestList [
  TestCase (assertEqual "typeCode1" typeExpr1
    (ok $ applyParser type0 lexer filePath typeCode1)),
  TestCase (assertEqual "typeCode2" typeExpr2
    (ok $ applyParser type0 lexer filePath typeCode2)),
  TestCase (assertEqual "typeCode3" typeExpr3
    (ok $ applyParser type0 lexer filePath typeCode3)),
  TestCase (assertEqual "typeCode4" typeExpr4
    (ok $ applyParser type0 lexer filePath typeCode4)),
  TestCase (assertEqual "typeCode5" typeExpr5
    (ok $ applyParser type0 lexer filePath typeCode5)),
  TestCase (assertEqual "typeCode6" typeExpr6
    (ok $ applyParser type0 lexer filePath typeCode6)),
  TestCase (assertEqual "typeCode7" typeExpr7
    (ok $ applyParser type0 lexer filePath typeCode7)),
  TestCase (assertEqual "typeCode8" typeExpr8
    (ok $ applyParser type0 lexer filePath typeCode8)),
  TestCase (assertEqual "typeCode9" typeExpr9
    (ok $ applyParser type0 lexer filePath typeCode9)),
  TestCase (assertEqual "typeCode10" typeExpr10
    (ok $ applyParser type0 lexer filePath typeCode10)),
  TestCase (assertEqual "typeCode11" typeExpr11
    (ok $ applyParser type0 lexer filePath typeCode11)),
  TestCase (assertEqual "typeCode12" typeExpr12
    (ok $ applyParser type0 lexer filePath typeCode12)),
  TestCase (assertEqual "typeCode13" typeExpr13
    (ok $ applyParser type0 lexer filePath typeCode13))
  ])

testIdentType = TestLabel "identType" (TestList [
  TestCase (assertEqual "TV" (TypeExprVariable (mkIdent "a"))
    (ok $ applyParser identType lexer filePath "a")),
  TestCase (assertEqual "TC" (TypeExprVariable (mkIdent "Int"))
    (ok $ applyParser identType lexer filePath "Int")),
  TestCase (assertEqual "TC" (TypeExprConstructor (qualifyWith mainMIdent $ mkIdent "Int"))
    (ok $ applyParser identType lexer filePath "main.Int"))
  ])

-- constrTerm0, constrTerm1, constrTerm2, literalPattern
-- anonPattern, varPattern, parenPattern, listPattern, lazyPattern
-- condExpr, expr, expr0, expr1, expr2, expr3, constant
-- variable, parentExpr, listExpr, lambaExpr, letExpr, doExpr
-- ifExpr, caseExpr, alt
-- statement, letStmt, exprOrBindStmt
-- char, int, checkInt, float, checkFloat, string
-- infixOpId, qInfixOpId, infixOp, functionName, qFunctionName
-- ident, opId, mIdent, qIdent, qOpId, minus, fminus
-- layout, layoutBraces
-- braces, brackets, parens, backquotes
-- TODO: write tests

-- kind

kind1,kind2,kind3 :: Kind
kind1 = Star
kind2 = KFun Star Star
kind3 = KFun Star $ KFun Star Star

kindCode1,kindCode2,kindCode3 :: String
kindCode1 = "@"
kindCode2 = "@ -> @"
kindCode3 = "@ -> @ -> @"

testKind = TestLabel "kind" (TestList [
  TestCase (assertEqual "kind1" kind1
    (ok $ applyParser kind lexer filePath kindCode1)),
  TestCase (assertEqual "kind2" kind2
    (ok $ applyParser kind lexer filePath kindCode2)),
  TestCase (assertEqual "kind3" kind3
    (ok $ applyParser kind lexer filePath kindCode3))
  ])

-- token, tokens, tokenOps, dot, color, comma, semicolon, bar, equals: not tested
-- backquote, checkBackquote: not tested
-- leftParen, rightParen, leftBracket, rightBracket: not tested
-- leftBrace, rightBrace, leftArrow: not tested

-- collects all tests

tests = TestList [
  testParseSource,
  testParseModule,
  testTypeClassDecl,
  testInstanceDecl,
  testDataDecl,
  testNewtypeDecl,
  testIntfDecl,
  testITypeClassDecl,
  testIKindDecl,
  testTypeDecl,
  testType0,
  testIdentType,
  testKind
  ]

main = runTestTT tests
