{-
  $Id: TypeClassBindingTest.hs 827 2004-06-21 16:14:35Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeClassBindingTest where
import HUnit
import TypeExpr
import TypeClassEnv
import TypeClassBinding
import Ident
import CurrySyntax
import PredefTypes
import CurrySyntaxUtils
import qualified TypeExprCheckTest

mid :: ModuleIdent
mid = TypeExprCheckTest.mid

decl1,decl2,decl3 :: Decl
decl1 = TypeExprCheckTest.someDecls !! 6
decl2 = TypeExprCheckTest.someDecls !! 7
decl3 = TypeExprCheckTest.someDecls !! 8

-- bindTypeClasses: TODO: write test

-- bindDecl

testBindDecl = TestLabel "bindDecl" (TestList [
  TestCase (assertEqual "decl2" typeClassEnv2
              (bindDecl mid decl2 typeClassEnv2orig))
  ])
  where typeClassEnv2 =
          bindTypeClass mid (mkIdent "Ord") (mkIdent "a")
                        [qualifyWith mid $ mkIdent "Eq"]
                        [(mkIdent ">",TypeExprWithContext emptyTypeExprContext ltTypeExpr)]
                        typeClassEnv2orig
        typeClassEnv2orig =
          bindTypeClass mid (mkIdent "Eq") (mkIdent "a") []
                        [(mkIdent "==",TypeExprWithContext emptyTypeExprContext (TypeExprVariable $ mkIdent "a"))]
                        initTypeClassEnv
        ltTypeExpr = typeExprApplyArrowConstructor
                       (TypeExprVariable $ mkIdent "a")
                       (typeExprApplyArrowConstructor
                         (TypeExprVariable $ mkIdent "a")
                         (TypeExprConstructor $ qualifyWith preludeMIdent $ mkIdent "Bool"))

-- methodIdents: TODO: write test

-- sortTypeClassDecls

testSortTypeClassDecls = TestLabel "sortTypeClassDecls" (TestList [
  TestCase (assertEqual "1" [decl1,decl3,decl2]
              (sortTypeClassDecls mid [decl3,decl2,decl1]))
  ])

-- parentTypeClassesDecl

testParentTypeClassesDecl = TestLabel "parentTypeClassesDecl" (TestList [
  TestCase (assertEqual "decl1" []
              (parentTypeClassesDecl decl1)),
  TestCase (assertEqual "decl2" [qualifyWith mid $ mkIdent "Eq"]
              (parentTypeClassesDecl decl2)),
  TestCase (assertEqual "decl3" [qualifyWith mid $ mkIdent "Eq",
                                 qualifyWith mid $ mkIdent "Pickable"]
              (parentTypeClassesDecl decl3))
  ])

-- collects all tests

tests = TestList [
  testBindDecl,
  testSortTypeClassDecls,
  testParentTypeClassesDecl
  ]

main = runTestTT tests