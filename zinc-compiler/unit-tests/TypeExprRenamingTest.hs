{-
  $Id: TypeExprRenamingTest.hs 1289 2004-09-03 13:56:57Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeExprRenamingTest where
import HUnit
import TypeExprRenaming
import TypeExpr
import CurryParser
import LLParseComb
import Error
import CurrySyntax
import CurryLexer
import PredefTypes
import TypeConstructorEnv
import TypeExprDisambiguate
import RenameState
import Ident
import CurryPP
import Subst(idSubst)
import TypeExprSubst
import qualified CurryParserTest

mid :: ModuleIdent
mid = CurryParserTest.mid

filePath :: FilePath
filePath = CurryParserTest.filePath

tcEnv :: TCEnv
tcEnv = initTCEnv

declCode1,declCode2,declCode3,declCode4,declCode5,declCode6 :: String
declCode7,declCode8 :: String
declCode1 = "data Tree a = Leaf | Branch (Tree a) (Tree a)"
declCode2 = "f :: Tree ()"
declCode3 = "g :: a -> _ -> b -> a"
declCode4 = "data Subst a b = Subst a b"
declCode5 = "h :: _"
declCode6 = "class Eq a where myEq :: a -> a -> Bool"
declCode7 = "instance Eq a => Eq [a] where\n  myEq = myMap\n  myMap :: [a] -> Bool"
declCode8 = "f = let g' :: a -> b\n        g' x = error\n    in g'"

allCode :: String
allCode = unlines [declCode1,declCode2,declCode3,declCode4,declCode5,
                   declCode6,declCode7,declCode8]

allDecls :: [Decl]
allDecls = disambiguateDecls mid tcEnv $ parseDecls allCode

declTree1,declTree2,declTree3,declTree4,declTree5,declTree6 :: Decl
declTree7,declTree8 :: Decl
[declTree1,declTree2,declTree3,declTree4,declTree5,
 declTree6,declTree7,declTree8] = allDecls

-- auxiliary function

parseDecls :: String -> [Decl]
parseDecls = ok . applyParser decls lexer filePath

-- newTypeExprVariable

testNewTypeExprVariable = TestLabel "newTypeExprVariable" (TestList [
  TestCase (assertEqual "" (map (TypeExprVariable . enumIdent) [1,2,3])
                           (fst (runRenameState
                                  (do v0 <- newTypeExprVariable
                                      v1 <- newTypeExprVariable
                                      v2 <- newTypeExprVariable
                                      return [v0,v1,v2]) 0)))
  ])

-- enumIdent: not tested

id1,id1',id2,id2' :: Ident
id1  = mkIdent "a"
id1' = mkIdent "_29"
id2  = mkIdent "c"
id2' = mkIdent "_328"

tyexpr1,tyexpr2 :: TypeExpr
tyexpr1 = TypeExprVariable id1'
tyexpr2 = TypeExprVariable id2'

subst1 :: TypeExprSubst
subst1 = bindTypeExprVar id1 tyexpr1 $
         bindTypeExprVar id2 tyexpr2 $
         idSubst

substInv1 :: TypeExprSubst
substInv1 = bindTypeExprVar id1' (TypeExprVariable id1) $
            bindTypeExprVar id2' (TypeExprVariable id2) $
            idSubst

-- collects all tests

tests = TestList [
  testNewTypeExprVariable
  ]

main = runTestTT tests

