{-
  $Id: TypeExpansionTest.hs 697 2004-05-29 09:48:20Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeExpansionTest where

import HUnit
import TypeExpansion
import Types
import Ident
import TypeConstructorEnv
import PredefTypes
import TypeTrans(qualifyIdentsIn)
import CurrySyntax
import TypeExpr

mid :: ModuleIdent
mid = mkMIdent ["mid"]

expansionTy1 :: Type
-- expansionTy1 = "ty1 b a"
expansionTy1 = typeApply (TypeConstructor $ qualifyWith mid $ mkIdent "ty1")
                 [TypeVariable 1,TypeVariable 0]

expectedTy1 :: Type
-- expectedTy1 = "ty1 () d"
expectedTy1 = typeApply (TypeConstructor $ qualifyWith mid $ mkIdent "ty1")
                 [TypeConstructor qUnitId,TypeVariable 3]

aliasArgsTys1 :: [Type]
aliasArgsTys1 = [TypeVariable 3, TypeConstructor qUnitId]

originalTy1 :: Type
-- originalTy1 = "tcAlias d ()"
originalTy1 = typeApply (TypeConstructor $ qualifyWith mid $ mkIdent "tcAlias")
                        aliasArgsTys1

tcEnv :: TCEnv
tcEnv = bindLocalTypeInfo AliasType mid (mkIdent "tcAlias") [mkIdent "a",mkIdent "b"] expansionTy1 $ initTCEnv

tyexpr1,tyexpr2,tyexpr3,tyexpr4 :: TypeExpr
tyexpr1 = TypeExprVariable $ mkIdent "a"
tyexpr2 = TypeExprVariable $ mkIdent "b"
tyexpr3 = typeExprUnitConstructor
tyexpr4 = typeExprApplyConstructor (qualify $ mkIdent "tcAlias") [TypeExprVariable (mkIdent "d"),typeExprUnitConstructor]

ts1,ts3,ts4 :: TypeScheme
ts1 = ForAll 1 (TypeWithContext emptyTypeContext (TypeVariable 0))
ts3 = ForAll 0 (TypeWithContext emptyTypeContext unitType)
ts4 = ForAll 1 (TypeWithContext emptyTypeContext
                 (typeApply (TypeConstructor $ qualifyWith mid $ mkIdent "ty1")
                   [unitType,TypeVariable 0]))

ty1e,ty2e,ty3e,ty4e :: Type
ty1e = TypeVariable 1
ty2e = TypeVariable 0
ty3e = unitType
ty4e = typeApply (TypeConstructor $ qualifyWith mid $ mkIdent "ty1") [ty3e,ty2e]

-- expandMonoType

testExpandMonoType = TestLabel "expandMonoType" (TestList [
  TestCase (assertEqual "tyexpr1" ty1e (expandMonoType tcEnv [mkIdent "b"] tyexpr1)),
  TestCase (assertEqual "tyexpr2" ty2e (expandMonoType tcEnv [mkIdent "b"] tyexpr2)),
  TestCase (assertEqual "tyexpr3" ty3e (expandMonoType tcEnv [] tyexpr3)),
  TestCase (assertEqual "tyexpr4" ty4e (expandMonoType tcEnv [mkIdent "d"] tyexpr4))
  ])

-- expandMonoTypes: not tested

-- expandPolyType

tyexprWC1,tyexprWC2,tyexprWC3,tyexprWC4 :: TypeExprWithContext
tyexprWC1 = TypeExprWithContext emptyTypeExprContext tyexpr1
tyexprWC2 = TypeExprWithContext emptyTypeExprContext tyexpr2
tyexprWC3 = TypeExprWithContext emptyTypeExprContext tyexpr3
tyexprWC4 = TypeExprWithContext emptyTypeExprContext tyexpr4

testExpandPolyType = TestLabel "expandPolyType" (TestList [
  TestCase (assertEqual "tyexpr1" ts1 (expandPolyType tcEnv tyexprWC1)),
  TestCase (assertEqual "tyexpr2" ts1 (expandPolyType tcEnv tyexprWC2)),
  TestCase (assertEqual "tyexpr3" ts3 (expandPolyType tcEnv tyexprWC3)),
  TestCase (assertEqual "tyexpr4" ts4 (expandPolyType tcEnv tyexprWC4))
  ])

-- expandType

testExpandType = TestLabel "expandType" (TestList [
  TestCase (assertEqual "type1n" (qualifyIdentsIn preludeMIdent expectedTy1) (expandType tcEnv originalTy1))
  ])

-- expandAliasType

testExpandAliasType = TestLabel "expandAliasType" (TestList [
  TestCase (assertEqual "1" expectedTy1 (expandAliasType aliasArgsTys1 expansionTy1))
  ])

-- normalize

type1n,type2n,type3n,type4n :: Type
type1n = TypeVariable 0
type2n = TypeVariable 1
type3n = typeArrow type1n type2n
type4n = typeArrow type2n type1n

testNormalize = TestLabel "normalize" (TestList [
  TestCase (assertEqual "type1n" type1n (normalize type1n)),
  TestCase (assertEqual "type2n" type1n (normalize type2n)),
  TestCase (assertEqual "type3n" type3n (normalize type3n)),
  TestCase (assertEqual "type4n" type3n (normalize type4n))
  ])


-- Collects all tests

tests = TestList [
  testExpandMonoType,
  testExpandPolyType,
  testExpandType,
  testExpandAliasType,
  testNormalize
  ]

main = runTestTT tests
