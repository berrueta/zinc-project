{-
  $Id: SubstTest.hs 1289 2004-09-03 13:56:57Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module SubstTest where

import HUnit
import Subst

-- some substitutions

subst1, subst2, subst3 :: Subst String String
subst1 = idSubst
subst2 = bindSubst "FOO" "foo" $ bindSubst "VAR" "var" idSubst
subst3 = bindSubst "CURRY" "curry" $ bindSubst "foo" "fuu" idSubst

-- substToList

testSubstToList = TestLabel "substToList" (TestList [
  TestCase (assertEqual "subst1" [] (substToList subst1)),
  TestCase (assertEqual "subst2" [("FOO","foo"),("VAR","var")]
                                 (substToList subst2))
  ])

-- unbindSubst

testUnbindSubst = TestLabel "unbindSubst" (TestList [
  TestCase (assertEqual "subst2" [("VAR","var")]
                                 (substToList $ unbindSubst "FOO" subst2))
  ])

-- compose
-- (note[DBM]: the name of the "compose" function seems confusing to me, as
-- it only performs an union)

testCompose = TestLabel "compose" (TestList [
  TestCase (assertEqual "subst2.subst3" [("CURRY","curry"),
                                         ("FOO","foo"),
                                         ("VAR","var"),
                                         ("foo","fuu")]
                                        (substToList $ compose subst2 subst3))
  ])

-- restrictSubstTo

testRestrictSubstTo = TestLabel "restrictSubstTo" (TestList [
  TestCase (assertEqual "subst2" [("VAR","var")] 
                        (substToList $ restrictSubstTo ["VAR"] subst2))
  ])

-- Collects all tests

tests = TestList [
  testSubstToList,
  testUnbindSubst,
  testCompose,
  testRestrictSubstTo
  ]

main = runTestTT tests
