{-
  $Id: IdentTest.hs 1318 2004-09-05 07:41:45Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module IdentTest where

import HUnit
import Ident
import Maybe

-- some identifiers

id1 :: Ident
id1 = mkIdent "id1"

qid1,qid2,qid3,qid4,qid5 :: QualIdent
qid1 = qualify id1
qid2 = qualifyWith mid1 id1
qid3 = qualifyWith mid2 id1
qid4 = qualQualify mid1 qid1
qid5 = qualQualify mid1 qid2

mid1 :: ModuleIdent
mid1 = mkMIdent ["mid1"]
mid2 = mkMIdent ["mid2a","mid2b","mid2c"]

-- name

testName = TestLabel "name" (TestList [
  TestCase (assertEqual "id1" "id1" (name id1))
  ])

-- qualName

testQualName = TestLabel "qualName" (TestList [
  TestCase (assertEqual "id1" "id1" (qualName qid1)),
  TestCase (assertEqual "id2" "mid1.id1" (qualName qid2))
  ])

-- uniqueId/renameIdent

testUniqueId = TestLabel "uniqueId/renameIdent" (TestList [
  TestCase (assertEqual "id1" 5 (uniqueId (renameIdent id1 5)))
  ])

-- moduleName

testModuleName = TestLabel "moduleName" (TestList [
  TestCase (assertEqual "mid1" "mid1" (moduleName mid1)),
  TestCase (assertEqual "mid2" "mid2a.mid2b.mid2c" (moduleName mid2))
  ])

-- moduleQualifiers

testModuleQualifiers = TestLabel "moduleQualifiers" (TestList [
  TestCase (assertEqual "mid1" ["mid1"] (moduleQualifiers mid1)),
  TestCase (assertEqual "mid2" ["mid2a","mid2b","mid2c"] (moduleQualifiers mid2))
  ])

-- isInfixOp, isQInfixOp: TODO: write tests

-- qualify, qualifyWith, qualQualify: not (directly) tested

-- qualifyLike: TODO: write tests

-- isQualified

testIsQualified = TestLabel "isQualified" (TestList [
  TestCase (assertEqual "qid1" False (isQualified qid1)),
  TestCase (assertEqual "qid2" True  (isQualified qid2)),
  TestCase (assertEqual "qid3" True  (isQualified qid3)),
  TestCase (assertEqual "qid4" True  (isQualified qid4)),
  TestCase (assertEqual "qid5" True  (isQualified qid5))
  ])

-- unqualify

testUnqualify = TestLabel "unqualify" (TestList [
  TestCase (assertEqual "qid1" id1 (unqualify qid1)),
  TestCase (assertEqual "qid2" id1 (unqualify qid2)),
  TestCase (assertEqual "qid3" id1 (unqualify qid3)),
  TestCase (assertEqual "qid4" id1 (unqualify qid4)),
  TestCase (assertEqual "qid5" id1 (unqualify qid5))
  ])

-- qualUnqualify

testQualUnqualify = TestLabel "qualUnqualify" (TestList [
  TestCase (assertEqual "mid1,qid1" qid1 (qualUnqualify mid1 qid1)),
  TestCase (assertEqual "mid2,qid1" qid1 (qualUnqualify mid2 qid1)),
  TestCase (assertEqual "mid1,qid2" qid1 (qualUnqualify mid1 qid2)),
  TestCase (assertEqual "mid2,qid2" qid2 (qualUnqualify mid2 qid2)),
  TestCase (assertEqual "mid1,qid3" qid3 (qualUnqualify mid1 qid3)),
  TestCase (assertEqual "mid2,qid3" qid1 (qualUnqualify mid2 qid3))
  ])

-- localIdent

testLocalIdent = TestLabel "localIdent" (TestList [
  TestCase (assertEqual "mid1,qid1" (Just id1) (localIdent mid1 qid1)),
  TestCase (assertEqual "mid2,qid1" (Just id1) (localIdent mid2 qid1)),
  TestCase (assertEqual "mid1,qid2" (Just id1) (localIdent mid1 qid2)),
  TestCase (assertEqual "mid2,qid2" (Nothing)  (localIdent mid2 qid2)),
  TestCase (assertEqual "mid1,qid3" (Nothing)  (localIdent mid1 qid3)),
  TestCase (assertEqual "mid2,qid3" (Just id1) (localIdent mid2 qid3))
  ])

-- splitQualIdent

testSplitQualIdent = TestLabel "splitQualIdent" (TestList [
  TestCase (assertEqual "qid1" (Nothing,id1) (splitQualIdent qid1)),
  TestCase (assertEqual "qid2" (Just mid1,id1) (splitQualIdent qid2)),
  TestCase (assertEqual "qid3" (Just mid2,id1) (splitQualIdent qid3))
  ])

-- tupleId/isTupleId/tupleArity

testTupleId = TestLabel "tupleId/isTupleId/tupleArity" (TestList [
  TestCase (assertEqual "isTupleId" False (isTupleId id1)),
  TestCase (assertEqual "isTupleId" True (isTupleId tid1)),
  TestCase (assertEqual "tupleArity" 3 (tupleArity tid1))
  ])
  where tid1 = tupleId 3

-- selectorId/isSelectorId

testSelectorId = TestLabel "selectorId/isSelectorId" (TestList [
  TestCase (assertEqual "isSelectorId" False (isSelectorId id1)),
  TestCase (assertEqual "isSelectorId" True (isSelectorId sid1))
  ])
  where sid1 = selectorId 3

-- Collects all tests

tests = TestList [
  testName,
  testQualName,
  testUniqueId,
  testModuleName,
  testModuleQualifiers,
  testIsQualified,
  testUnqualify,
  testQualUnqualify,
  testLocalIdent,
  testSplitQualIdent,
  testTupleId,
  testSelectorId
  ]

main = runTestTT tests
