{-
  $Id: SCCTest.hs 13 2003-12-02 09:38:58Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module SCCTest where

import HUnit
import SCC

-- some declarations

decl1,decl2,decl3,decl4,decl5 :: (String,String)
decl1 = ("","abc")
decl2 = ("a","bc")
decl3 = ("b","ab")
decl4 = ("cd","")
decl5 = ("e","a")

decls :: [(String,String)]
decls = [decl1,decl2,decl3,decl4,decl5]

-- scc

testScc = TestLabel "scc" (TestList [
  TestCase (assertEqual "decls" [[decl4],[decl2,decl3],[decl1],[decl5]] (scc fst snd decls))
  ])

-- Collects all tests

tests = TestList [
  testScc
  ]

main = runTestTT tests
