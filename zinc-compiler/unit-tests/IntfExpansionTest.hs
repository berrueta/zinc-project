{-
  $Id: IntfExpansionTest.hs 697 2004-05-29 09:48:20Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module IntfExpansionTest where

import HUnit
import IntfExpansion
import CurrySyntax
import Ident
import TypeConstructorEnv
import TypeClassEnv
import ValueEnv
import PredefTypes
import Position
import CurryPP
import TypeBinding
import TypeClassBinding
import TopEnv
import qualified CurryParserTest
import qualified TypeExprCheckTest

pos :: Position
pos = Position {file=CurryParserTest.filePath,line=1,column=1}

mid :: ModuleIdent
mid = CurryParserTest.mid

ds1,ds2 :: [Decl]
ds1 = []
ds2 = TypeExprCheckTest.someDecls

es1,es2 :: Maybe ExportSpec
es1 = Nothing
es2 = Just $ Exporting pos []

module1,module2,module3,module4 :: Module
module1 = Module mid es1 ds1
module2 = Module mid es1 ds2 -- the right way
module3 = Module mid es2 ds1
module4 = Module mid es2 ds2

tcEnv1 :: TCEnv
tcEnv1 = bindLocalTypeInfo DataType preludeMIdent intId [] [] initTCEnv
tcEnv2 :: [Decl] -> TCEnv
tcEnv2 ds = bindTypes mid ds tcEnv1

valueEnv1 :: ValueEnv
valueEnv1 = initDCEnv

typeClassEnv1 :: TypeClassEnv
typeClassEnv1 = initTypeClassEnv
typeClassEnv2 :: [Decl] -> TypeClassEnv
typeClassEnv2 ds = bindTypeClasses mid (filter isTypeClassDecl ds) typeClassEnv1

demoExpandInterface1 :: Maybe ExportSpec
demoExpandInterface1 = expandInterface mid es1 ds2 (typeClassEnv2 ds2) (tcEnv2 ds2) valueEnv1

-- expandInterface: TODO: write test

-- Collects all tests

tests = TestList [
  ]

main = runTestTT tests
