{-
  $Id: TypingTest.hs 827 2004-06-21 16:14:35Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypingTest where
import HUnit
import Types
import Ident
import Typing
import ValueEnv
import TopEnv(emptyTopEnv)
import DualTopEnv(emptyDualTopEnv)
import TypeConstructorEnv
import PredefTypes
import CurrySyntax
import Subst
import TypeSubst
import TypingMonad
import TypeClassEnv
import InstanceEnv
import qualified CurryParserTest

mid :: ModuleIdent
mid = CurryParserTest.mid

initValueEnv :: ValueEnv
initValueEnv = initDCEnv

-- typeOf

id1,id2,id3 :: Ident
id1 = mkIdent "x"
id2 = mkIdent "y"
id3 = mkIdent "f"

qid1,qid2,qid3 :: QualIdent
qid1 = qualifyWith mainMIdent id1
qid2 = qualifyWith mainMIdent id2
qid3 = qualifyWith mainMIdent id3

tcid1 :: Ident
tcid1 = mkIdent "MyEq"

tcqid1 :: QualIdent
tcqid1 = qualifyWith mainMIdent tcid1

expr1,expr2,expr3,expr4,expr5 :: Expression
expr1 = Variable qid1
expr2 = Variable qid2
expr3 = Variable qid3
expr4 = Apply expr3 expr2
expr5 = Apply expr3 expr1

tyWC1,tyWC2,tyWC3 :: TypeWithContext
tyWC1 = intTypeWithContext
tyWC2 = TypeWithContext emptyTypeContext (TypeVariable $ -1)
tyWC3 = TypeWithContext
          (TypeContext [TypeClassConstraint tcqid1 (TypeVariable 0)])
          (typeArrow (TypeVariable 0) boolType)

testTypeOf = TestLabel "typeOf" (TestList [
  TestCase (assertEqual "1"
                        (intTypeWithContext)
                        (typeOf typeClassEnv instEnv valueEnv expr1)),
  TestCase (assertEqual "2"
                        (tyWC2)
                        (typeOf typeClassEnv instEnv valueEnv expr2)),
  TestCase (assertEqual "3"
                        (tyWC3)
                        (typeOf typeClassEnv instEnv valueEnv expr3)),
  TestCase (assertEqual "4"
                        (boolTypeWithContext)
                        (typeOf typeClassEnv instEnv valueEnv expr4)),
  TestCase (assertEqual "5"
                        (boolTypeWithContext)
                        (typeOf typeClassEnv instEnv valueEnv expr5))
  ])
  where valueEnv = bindFun mainMIdent id1 (ForAll 1 tyWC1)
                 $ bindFun mainMIdent id2 (ForAll 1 tyWC2)
                 $ bindFun mainMIdent id3 (ForAll 1 tyWC3)
                 $ initValueEnv
        typeClassEnv = bindTypeClass mainMIdent tcid1 (mkIdent "a") [] []
                     $ emptyTopEnv
        instEnv = bindInstance tcqid1 (qualifyWith preludeMIdent intId) mainMIdent [] [] []
                $ emptyDualTopEnv


-- identType: TODO: write test

-- argType

constrTerm1, constrTerm2 :: ConstrTerm
constrTerm1 = ConstructorPattern qNoDataId []
constrTerm2 = TuplePattern [constrTerm1,
                            constrTerm1]

constrTermType1, constrTermType2 :: Type
constrTermType1 = unitType
constrTermType2 = tupleType [unitType,unitType]

testArgType = TestLabel "argType" (TestList [
  TestCase (assertEqual "constrTerm1"
                        constrTermType1
                        (argType' constrTerm1)),
  TestCase (assertEqual "constrTerm2"
                        constrTermType2
                        (argType' constrTerm2))
  ])
  where argType' ct = runTyState (argType valueEnv ct) initValueEnv
        valueEnv = initDCEnv

-- exprType, rhsType: TODO: write test
-- freshTypeVar, instType, instUniv, instUnivExist: TODO: write test
-- unify, unifyList: TODO: write test

-- unifyArrow

unifyArrowType1,unifyArrowType2,unifyArrowType3,unifyArrowType4 :: Type
unifyArrowType5,unifyArrowType6 :: Type
unifyArrowType1 = TypeVariable 0
unifyArrowType2 = TypeVariable 1
unifyArrowType3 = typeArrow unifyArrowType1 unifyArrowType2
unifyArrowType4 = typeArrow unifyArrowType2 unifyArrowType1
unifyArrowType5 = typeArrow unifyArrowType3 unifyArrowType1
unifyArrowType6 = typeArrow unifyArrowType1 unifyArrowType3
-- unifyArrowType7 = TypeConstructor (qualifyWith mid $ mkIdent "foo") []

testUnifyArrow = TestLabel "unifyArrow" (TestList [
  TestCase (assertEqual "unifyArrowType1"
                        (unifyArrowType1,unifyArrowType2)
                        (unifyArrow' unifyArrowType1)),
  TestCase (assertEqual "unifyArrowType3"
                        (unifyArrowType1,unifyArrowType2)
                        (unifyArrow' unifyArrowType3)),
  TestCase (assertEqual "unifyArrowType4"
                        (unifyArrowType2,unifyArrowType1)
                        (unifyArrow' unifyArrowType4)),
  TestCase (assertEqual "unifyArrowType5"
                        (unifyArrowType3,unifyArrowType1)
                        (unifyArrow' unifyArrowType5)),
  TestCase (assertEqual "unifyArrowType6"
                        (unifyArrowType1,unifyArrowType3)
                        (unifyArrow' unifyArrowType6))
--TestCase (assertEqual "unifyArrowType7"
--                      (unifyArrowType1',unifyArrowType2')
--                      (unifyArrow' unifyArrowType7))
  ])
  where unifyArrow' ty = runTyState (unifyArrow ty) initValueEnv

-- unifyArrow2: TODO: write test

-- unifyTypes

unifyTypesType1,unifyTypesType2,unifyTypesType3 :: Type
unifyTypesType4,unifyTypesType5,unifyTypesType6 :: Type
unifyTypesType7 :: Type
unifyTypesType1 = TypeVariable 1
unifyTypesType2 = TypeVariable 2
unifyTypesType3 = TypeConstructor qUnitId
unifyTypesType4 = typeArrow unifyTypesType1 unifyTypesType1
unifyTypesType5 = typeArrow unifyTypesType1 unifyTypesType2
unifyTypesType6 = listType unifyTypesType1
unifyTypesType7 = listType unifyTypesType2

testUnifyTypes = TestLabel "unifyTypes" (TestList [
  TestCase (assertEqual "unifyTypesType1-1"
                        (idSubst)
                        (unifyTypes unifyTypesType1 unifyTypesType1 idSubst)),
  TestCase (assertEqual "unifyTypesType1-2"
                        (bindVar 1 unifyTypesType2 idSubst)
                        (unifyTypes unifyTypesType1 unifyTypesType2 idSubst)),
  TestCase (assertEqual "unifyTypesType2-1"
                        (bindVar 2 unifyTypesType1 idSubst)
                        (unifyTypes unifyTypesType2 unifyTypesType1 idSubst)),
  TestCase (assertEqual "unifyTypesType1-3"
                        (bindVar 1 unifyTypesType3 idSubst)
                        (unifyTypes unifyTypesType1 unifyTypesType3 idSubst)),
  TestCase (assertEqual "unifyTypesType3-1"
                        (bindVar 1 unifyTypesType3 idSubst)
                        (unifyTypes unifyTypesType3 unifyTypesType1 idSubst)),
  TestCase (assertEqual "unifyTypesType3-3"
                        (idSubst)
                        (unifyTypes unifyTypesType3 unifyTypesType3 idSubst)),
  -- there is no ocurrs check
  TestCase (assertEqual "unifyTypesType1-4"
                        (bindVar 1 unifyTypesType4 idSubst)
                        (unifyTypes unifyTypesType1 unifyTypesType4 idSubst)),
  TestCase (assertEqual "unifyTypesType2-5"
                        (bindVar 2 unifyTypesType5 idSubst)
                        (unifyTypes unifyTypesType2 unifyTypesType5 idSubst)),
  TestCase (assertEqual "unifyTypesType4-5"
                        (bindVar 1 unifyTypesType2 idSubst)
                        (unifyTypes unifyTypesType4 unifyTypesType5 idSubst)),
  TestCase (assertEqual "unifyTypesType6-7"
                        (bindVar 1 unifyTypesType2 idSubst)
                        (unifyTypes unifyTypesType6 unifyTypesType7 idSubst))
  ])

-- constrType, varType, funType: TODO: write test

-- collects all tests

tests = TestList [
  testTypeOf,
  testArgType,
  testUnifyArrow,
  testUnifyTypes
  ]

main = runTestTT tests
