{-
  $Id: KindUnificationTest.hs 839 2004-06-24 16:29:25Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module KindUnificationTest where

import HUnit
import Kind
import KindUnification
import KindSubst
import Ident
import Subst(substToList)

-- unifyKinds

testUnifyKind = TestLabel "unifyKinds" (TestList [
  TestCase (assertEqual "unifyKinds1"
     (Just [])
     (f Star Star)),
  TestCase (assertEqual "unifyKinds2"
     (Just [])
     (f (KFun Star Star) (KFun Star Star))),
  TestCase (assertEqual "unifyKinds3"
     (Nothing)
     (f (KFun Star Star) Star)),
  TestCase (assertEqual "unifyKinds4"
     (Nothing)
     (f Star (KFun Star Star))),
  TestCase (assertEqual "unifyKinds5"
     (Just [(id1,Star)])
     (f Star kVarA)),
  TestCase (assertEqual "unifyKinds6"
     (Just [(id1,Star)])
     (f kVarA Star)),
  TestCase (assertEqual "unifyKinds7"
     (Just [])
     (f kVarA kVarA)),
  TestCase (assertEqual "unifyKinds8"
     (Just [(id2,kVarA)])
     (f kVarB kVarA)),
  TestCase (assertEqual "unifyKinds9"
     (Just [(id1,kVarB)])
     (f kVarA kVarB)),
  TestCase (assertEqual "unifyKinds10"
     Nothing
     (f kVarA (KFun Star kVarA))),
  TestCase (assertEqual "unifyKinds10"
     (Just [(id1,KFun Star kVarB)])
     (f kVarA (KFun Star kVarB)))
  ])
  where id1 = mkIdent "a"
        id2 = mkIdent "b"
        kVarA = KVar id1
        kVarB = KVar id2
        f k1 k2 = fmap substToList (unifyKinds k1 k2)

-- Collects all tests

tests = TestList [
  testUnifyKind
  ]

main = runTestTT tests

