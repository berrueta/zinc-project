{-
  $Id: TypeBindingTest.hs 1289 2004-09-03 13:56:57Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeBindingTest where

import HUnit
import TypeBinding
import qualified TypeTransTest
import Ident

mid :: ModuleIdent
mid = mkMIdent ["mid"]

-- bindTypes: TODO: write test
-- bindTC: TODO: write test
-- clearTVars: TODO: write test
-- sortTypeDecls: TODO: write test
-- typeDecl: TODO: write test

-- ft

testFt = TestLabel "ft" (TestList [
  TestCase (assertEqual "tyexpr1"  [] (ft mid TypeTransTest.tyExpr1  [])),
  TestCase (assertEqual "tyexpr2"  [] (ft mid TypeTransTest.tyExpr2  [])),
  TestCase (assertEqual "tyexpr3"  [] (ft mid TypeTransTest.tyExpr3  [])),
  TestCase (assertEqual "tyexpr4"  [mkIdent "Bool"] (ft mid TypeTransTest.tyExpr4  [])),
  TestCase (assertEqual "tyexpr5"  [mkIdent "Char"] (ft mid TypeTransTest.tyExpr5  [])),
  TestCase (assertEqual "tyexpr6"  [mkIdent "Bool"] (ft mid TypeTransTest.tyExpr6  [])),
  TestCase (assertEqual "tyexpr7"  [mkIdent "Success"] (ft mid TypeTransTest.tyExpr7  [])),
  TestCase (assertEqual "tyexpr8"  [mkIdent "IO"] (ft mid TypeTransTest.tyExpr8  [])),
  TestCase (assertEqual "tyexpr9"  [mkIdent "Bool"] (ft mid TypeTransTest.tyExpr9  [])),
  TestCase (assertEqual "tyexpr10" [] (ft mid TypeTransTest.tyExpr10 [])),
  TestCase (assertEqual "tyexpr11" [mkIdent "Char"] (ft mid TypeTransTest.tyExpr11 [])),
  TestCase (assertEqual "tyexpr12" [mkIdent "Char"] (ft mid TypeTransTest.tyExpr12 [])),
  TestCase (assertEqual "tyexpr13" [mkIdent "Char"] (ft mid TypeTransTest.tyExpr13 [])),
  TestCase (assertEqual "tyexpr15" [] (ft mid TypeTransTest.tyExpr15 [])),
  TestCase (assertEqual "tyexpr16" [] (ft mid TypeTransTest.tyExpr16 [])),
  TestCase (assertEqual "tyexpr17" [mkIdent "IO",mkIdent "Char"] (ft mid TypeTransTest.tyExpr17 []))
  ])

-- bindConstrs: TODO: write test

-- Collects all tests

tests = TestList [
  testFt
  ]

main = runTestTT tests
