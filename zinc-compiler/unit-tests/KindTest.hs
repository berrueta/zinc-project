{-
  $Id: KindTest.hs 839 2004-06-24 16:29:25Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module KindTest where

import HUnit
import Kind
import Ident

-- containsKindVars

testContainsKindVars = TestLabel "containsKindVars" (TestList [
  TestCase (assertEqual "1" False (containsKindVars Star)),
  TestCase (assertEqual "2" False (containsKindVars $ KFun Star Star)),
  TestCase (assertEqual "3" False (containsKindVars $ KFun Star $ KFun Star Star)),
  TestCase (assertEqual "4" True  (containsKindVars $ KVar $ mkIdent "a")),
  TestCase (assertEqual "5" True  (containsKindVars $ KFun Star $ KVar $ mkIdent "a")),
  TestCase (assertEqual "6" False (containsKindVars $ KFun (KFun Star Star) Star))
  ])

-- kindOrder

testKindOrder = TestLabel "kindOrder" (TestList [
  TestCase (assertEqual "*"       0 (kindOrder Star)),
  TestCase (assertEqual "*->*"    1 (kindOrder $ KFun Star Star)),
  TestCase (assertEqual "*->*->*" 2 (kindOrder $ KFun Star $ KFun Star Star))
  ])

-- Collects all tests

tests = TestList [
  testContainsKindVars,
  testKindOrder
  ]

main = runTestTT tests

