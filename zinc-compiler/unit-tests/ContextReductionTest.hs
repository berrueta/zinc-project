{-
  $Id: ContextReductionTest.hs 827 2004-06-21 16:14:35Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module ContextReductionTest where

import HUnit
import ContextReduction
import Types
import Ident
import InstanceEnv
import TypeClassEnv
import TypeExpr
import Env
import TopEnv
import DualTopEnv
import Position

p :: Position
p = error "noPos"

mid :: ModuleIdent
mid = mkMIdent ["mid"]

qid1,qid2 :: QualIdent
qid1 = qualifyWith mid $ mkIdent "A"
qid2 = qualifyWith mid $ mkIdent "B"

typeClassEnv :: TypeClassEnv
typeClassEnv = qualBindImportTopEnv mid (unqualify qid1) tci1 $
               qualBindImportTopEnv mid (unqualify qid2) tci2 $
               emptyTopEnv
  where tci1 = TypeClassInfo qid1 (mkIdent "a") [] emptyEnv
        tci2 = TypeClassInfo qid1 (mkIdent "b") [qid1] emptyEnv

instEnv :: InstanceEnv
instEnv = bindImportDualTopEnv mid qid1 (qualifyWith preludeMIdent intId) ii1 $ 
          bindImportDualTopEnv mid qid1 qListId ii2 $ 
          bindImportDualTopEnv mid qid1 (qTupleId 2) ii3 $ 
          emptyDualTopEnv
  where ii1 = importedInstance mid qid1 qIntId []
        ii2 = importedInstance mid qid1 qListId [[qid1]]
        ii3 = importedInstance mid qid1 (qTupleId 2) [[qid1],[qid2]]

tv1,tv2 :: Type
tv1 = TypeVariable 1
tv2 = TypeVariable 2

tcc1,tcc2,tcc3,tcc4,tcc5,tcc6,tcc7,tcc8 :: TypeClassConstraint
tcc1 = TypeClassConstraint qid1 tv1
tcc2 = TypeClassConstraint qid2 tv1
tcc3 = TypeClassConstraint qid1 tv2
tcc4 = TypeClassConstraint qid1 intType
tcc5 = TypeClassConstraint qid1 (listType tv1)
tcc6 = TypeClassConstraint qid1 (listType intType)
tcc7 = TypeClassConstraint qid1 (tupleType [tv1,tv1])
tcc8 = TypeClassConstraint qid1 (tupleType [tv2,tv1])


testContextReduction = TestLabel "contextReduction" (TestList [
  TestCase (assertEqual "1" emptyTypeContext
              (contextReduction p typeClassEnv instEnv emptyTypeContext)),
  TestCase (assertEqual "2" (TypeContext [tcc1])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc1]))),
  TestCase (assertEqual "3" (TypeContext [tcc1])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc1,tcc1]))),
  TestCase (assertEqual "4" (TypeContext [tcc1,tcc3])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc1,tcc3]))),
  TestCase (assertEqual "5" (TypeContext [tcc3,tcc1])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc3,tcc1]))),
  TestCase (assertEqual "6" (TypeContext [tcc2,tcc3])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc1,tcc2,tcc3]))),
  TestCase (assertEqual "7" (TypeContext [tcc3,tcc2])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc3,tcc2,tcc1]))),
  TestCase (assertEqual "8" (TypeContext [])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc4]))),
  TestCase (assertEqual "8" (TypeContext [tcc1])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc5]))),
  TestCase (assertEqual "9" (TypeContext [])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc6]))),
  TestCase (assertEqual "10" (TypeContext [tcc2])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc7]))),
  TestCase (assertEqual "11" (TypeContext [tcc3,tcc2])
              (contextReduction p typeClassEnv instEnv (TypeContext [tcc8])))
  ])

-- Collects all tests

tests = TestList [
  testContextReduction
  ]

main = runTestTT tests
