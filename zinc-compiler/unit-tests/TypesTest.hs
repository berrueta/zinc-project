{-
  $Id: TypesTest.hs 1289 2004-09-03 13:56:57Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module TypesTest where

import HUnit
import Ident
import Types

-- module identifier
mid :: ModuleIdent
mid = mkMIdent ["mid"]

-- some types

type1,type2,type3,type4,type5,type6,type7,type8,type9,type10 :: Type
type1 = TypeConstructor (qualify $ mkIdent "a"     )     -- a
type2 = intType                                          -- Int
type3 = stringType                                       -- String
type4 = listType type3                                   -- [String]
type5 = tupleType [type1,type2]                          -- (a,Int)
type6 = typeArrow type1 type1                            -- a->a
type7 = typeArrow type1 (typeArrow type1 type2)          -- a->a->Int
type8 = typeApply (TypeConstructor $ qualifyWith preludeMIdent ioId) -- IO a
          [TypeVariable 0]
type9 = typeArrow type6 (typeArrow type3 type8)          -- (a->a)->String->IO b
type10= typeArrow type3 (typeArrow type6 type8)          -- String->(a->a)->IO b

type1',type2',type3',type4',type5',type6',type7',type8',type9',type10' :: Type
type1' = TypeVariable 0                                   -- a
type2' = intType                                          -- Int
type3' = stringType                                       -- String
type4' = listType type3'                                  -- [String]
type5' = tupleType [type1',type2']                        -- (a,Int)
type6' = typeArrow type1' type1'                          -- a->a
type7' = typeArrow type1' (typeArrow type1' type2')       -- a->a->Int
type8' = typeApply (TypeConstructor $ qualifyWith preludeMIdent ioId) [type1'] -- IO a
type9' = typeArrow type6' (typeArrow type3' type8')       -- (a->a)->String->IO b
type10'= typeArrow type3' (typeArrow type6' type8')       -- String->(a->a)->IO b

-- isArrowType

testIsArrowType = TestLabel "isArrowType" (TestList [
  TestCase (assertEqual "type1"  False (isArrowType type1)),
  TestCase (assertEqual "type2"  False (isArrowType type2)),
  TestCase (assertEqual "type3"  False (isArrowType type3)),
  TestCase (assertEqual "type4"  False (isArrowType type4)),
  TestCase (assertEqual "type5"  False (isArrowType type5)),
  TestCase (assertEqual "type6"  True  (isArrowType type6)),
  TestCase (assertEqual "type7"  True  (isArrowType type7)),
  TestCase (assertEqual "type8"  False (isArrowType type8)),
  TestCase (assertEqual "type9"  True  (isArrowType type9)),
  TestCase (assertEqual "type10" True  (isArrowType type10))
  ])

-- arrowArity

testArrowArity = TestLabel "arrowArity" (TestList [
  TestCase (assertEqual "type6"  1 (arrowArity type6)),
  TestCase (assertEqual "type7"  2 (arrowArity type7)),
  TestCase (assertEqual "type9"  2 (arrowArity type9)),
  TestCase (assertEqual "type10" 2 (arrowArity type10))
  ])

-- arrowArgs

testArrowArgs = TestLabel "arrowArgs" (TestList [
  TestCase (assertEqual "type6"  [type1] (arrowArgs type6)),
  TestCase (assertEqual "type7"  [type1,type1] (arrowArgs type7)),
  TestCase (assertEqual "type9"  [type6,type3] (arrowArgs type9)),
  TestCase (assertEqual "type10" [type3,type6] (arrowArgs type10))
  ])

-- arrowBase

testArrowBase = TestLabel "arrowBase" (TestList [
  TestCase (assertEqual "type6"  type1 (arrowBase type6)),
  TestCase (assertEqual "type7"  type2 (arrowBase type7)),
  TestCase (assertEqual "type9"  type8 (arrowBase type9)),
  TestCase (assertEqual "type10" type8 (arrowBase type10))
  ])

-- typeVars

testTypeVars = TestLabel "typeVars" (TestList [
  TestCase (assertEqual "type1"  [0]     (typeVars type1')),
  TestCase (assertEqual "type2"  []      (typeVars type2')),
  TestCase (assertEqual "type3"  []      (typeVars type3')),
  TestCase (assertEqual "type4"  []      (typeVars type4')),
  TestCase (assertEqual "type5"  [0]     (typeVars type5')),
  TestCase (assertEqual "type6"  [0,0]   (typeVars type6')),
  TestCase (assertEqual "type7"  [0,0]   (typeVars type7')),
  TestCase (assertEqual "type8"  [0]     (typeVars type8')),
  TestCase (assertEqual "type9"  [0,0,0] (typeVars type9')),
  TestCase (assertEqual "type10" [0,0,0] (typeVars type10'))
  ])

-- typeSkolems: TODO: write tests

-- monoType

testMonoType = TestLabel "monoType" (TestList [
  TestCase (assertEqual "type1"  (ForAll 0 (TypeWithContext emptyTypeContext type1'))  (monoType type1')),
  TestCase (assertEqual "type2"  (ForAll 0 (TypeWithContext emptyTypeContext type2'))  (monoType type2')),
  TestCase (assertEqual "type3"  (ForAll 0 (TypeWithContext emptyTypeContext type3'))  (monoType type3')),
  TestCase (assertEqual "type4"  (ForAll 0 (TypeWithContext emptyTypeContext type4'))  (monoType type4')),
  TestCase (assertEqual "type5"  (ForAll 0 (TypeWithContext emptyTypeContext type5'))  (monoType type5')),
  TestCase (assertEqual "type6"  (ForAll 0 (TypeWithContext emptyTypeContext type6'))  (monoType type6')),
  TestCase (assertEqual "type7"  (ForAll 0 (TypeWithContext emptyTypeContext type7'))  (monoType type7')),
  TestCase (assertEqual "type8"  (ForAll 0 (TypeWithContext emptyTypeContext type8'))  (monoType type8')),
  TestCase (assertEqual "type9"  (ForAll 0 (TypeWithContext emptyTypeContext type9'))  (monoType type9')),
  TestCase (assertEqual "type10" (ForAll 0 (TypeWithContext emptyTypeContext type10')) (monoType type10'))
  ])

-- polyType

testPolyType = TestLabel "polyType" (TestList [
  TestCase (assertEqual "type1"  (ForAll 1 (TypeWithContext emptyTypeContext type1'))  (polyType type1')),
  TestCase (assertEqual "type2"  (ForAll 0 (TypeWithContext emptyTypeContext type2'))  (polyType type2')),
  TestCase (assertEqual "type3"  (ForAll 0 (TypeWithContext emptyTypeContext type3'))  (polyType type3')),
  TestCase (assertEqual "type4"  (ForAll 0 (TypeWithContext emptyTypeContext type4'))  (polyType type4')),
  TestCase (assertEqual "type5"  (ForAll 1 (TypeWithContext emptyTypeContext type5'))  (polyType type5')),
  TestCase (assertEqual "type6"  (ForAll 1 (TypeWithContext emptyTypeContext type6'))  (polyType type6')),
  TestCase (assertEqual "type7"  (ForAll 1 (TypeWithContext emptyTypeContext type7'))  (polyType type7')),
  TestCase (assertEqual "type8"  (ForAll 1 (TypeWithContext emptyTypeContext type8'))  (polyType type8')),
  TestCase (assertEqual "type9"  (ForAll 1 (TypeWithContext emptyTypeContext type9'))  (polyType type9')),
  TestCase (assertEqual "type10" (ForAll 1 (TypeWithContext emptyTypeContext type10')) (polyType type10'))
  ])

-- listType

testListType = TestLabel "listType" (TestList [
  TestCase (assertEqual "[Int]" (typeApply (TypeConstructor listConstr) [intType]) (listType intType)),
  TestCase (assertEqual "[[Char]]" (typeApply (TypeConstructor listConstr) [typeApply (TypeConstructor listConstr) [charType]]) (listType stringType))
  ])
  where listConstr = qualifyWith internalTypeMIdent listId

-- applyIoType: TODO: write tests

-- tupleType

testTupleType = TestLabel "tupleType" (TestList [
  TestCase (assertEqual "(Int,Char)" (typeApply (TypeConstructor tupleConstr) [intType,charType]) (tupleType [intType,charType]))
  ])
  where tupleConstr = qualifyWith internalTypeMIdent (mkIdent "(,)")

-- primType: not tested
-- typeVar: not tested

-- Collects all tests

tests = TestList [
  testIsArrowType,
  testArrowArity,
  testArrowArgs,
  testArrowBase,
  testTypeVars,
-- unused function
--  testTypeConstrs,
  testMonoType,
  testPolyType,
  testTupleType,
  testListType,
  testTupleType
  ]

main = runTestTT tests