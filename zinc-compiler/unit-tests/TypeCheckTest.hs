{-
  $Id: TypeCheckTest.hs 1289 2004-09-03 13:56:57Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeCheckTest where
import HUnit
import Types
import Ident
import TypeCheck
import TypeInferenceMonad
import Position
import Pretty
import ValueEnv
import TopEnv(emptyTopEnv)
import Env(emptyEnv)
import SigEnv
import TypeConstructorEnv
import PredefTypes
import CurrySyntax
import qualified CurryParserTest

mid :: ModuleIdent
mid = CurryParserTest.mid

p :: Position
p = Position {file=CurryParserTest.filePath,line=99,column=99}

what :: String
what = "WHAT"

doc :: Doc
doc = text "DOC"

initValueEnv :: ValueEnv
initValueEnv = initDCEnv

initSigEnv :: SigEnv
initSigEnv = emptyEnv

-- typeCheck: TODO: write test
-- typeCheckGoal: TODO: write test
-- tcDecls: TODO: write test
-- tcDeclGroup: TODO: write test
-- tcExternalFunct: TODO: write test
-- tcExtraVar: TODO: write test
-- tcDeclLhs: TODO: write test
-- tcDeclRhs: TODO: write test
-- unifyDecl: TODO: write test

-- genDecl: TODO: write test
-- genVar: TODO: write test

-- tcEquation: TODO: write test

-- tcConstrTerm

constrTerm1, constrTerm2 :: ConstrTerm
constrTerm1 = ConstructorPattern qNoDataId []
constrTerm2 = TuplePattern [VariablePattern (mkIdent "1"),
                            VariablePattern (mkIdent "2")]
constrTerm3 = InfixPattern (VariablePattern $ mkIdent "1") qConsId (VariablePattern $ mkIdent "2")

constrTermType1, constrTermType2 :: TypeWithContext
constrTermType1 = unitTypeWithContext
constrTermType2 = TypeWithContext emptyTypeContext $ tupleType [TypeVariable (-1),
                                                                TypeVariable (-2)]
constrTermType3 = TypeWithContext emptyTypeContext $ listType (TypeVariable (-1))

testTcConstrTerm = TestLabel "tcConstrTerm" (TestList [
  TestCase (assertEqual "constrTerm1"
                        constrTermType1
                        (tcConstrTerm' constrTerm1)),
  TestCase (assertEqual "constrTerm2"
                        constrTermType2
                        (tcConstrTerm' constrTerm2)),
  TestCase (assertEqual "constrTerm3"
                        constrTermType3
                        (tcConstrTerm' constrTerm3))
  ])
  where tcConstrTerm' ct = run (tcConstrTerm mid tcEnv sigEnv p ct) initValueEnv
        sigEnv = initSigEnv
        tcEnv = initTCEnv

-- tcRhs: TODO: write test
-- tcCondExprs: TODO: write test

-- tcExpr: TODO: write test

-- tcQual: TODO: write test
-- tcStmt: TODO: write test

-- tcArrow

tcArrowType1',tcArrowType2' :: TypeWithContext
tcArrowType1' = TypeWithContext emptyTypeContext $ TypeVariable (-1)
tcArrowType2' = TypeWithContext emptyTypeContext $ TypeVariable (-2)

tcArrowType1,tcArrowType2,tcArrowType3,tcArrowType4,tcArrowType5 :: TypeWithContext
tcArrowType6 :: TypeWithContext
tcArrowType1 = TypeWithContext emptyTypeContext $ TypeVariable 0
tcArrowType2 = TypeWithContext emptyTypeContext $ TypeVariable 1
tcArrowType3 = typeArrowWithContext tcArrowType1 tcArrowType2
tcArrowType4 = typeArrowWithContext tcArrowType2 tcArrowType1
tcArrowType5 = typeArrowWithContext tcArrowType3 tcArrowType1
tcArrowType6 = typeArrowWithContext tcArrowType1 tcArrowType3
-- tcArrowType7 = TypeConstructor (qualifyWith mid $ mkIdent "foo") []

testTcArrow = TestLabel "tcArrow" (TestList [
  TestCase (assertEqual "tcArrowType1"
                        (tcArrowType1',tcArrowType2')
                        (tcArrow' tcArrowType1)),
  TestCase (assertEqual "tcArrowType3"
                        (tcArrowType1,tcArrowType2)
                        (tcArrow' tcArrowType3)),
  TestCase (assertEqual "tcArrowType4"
                        (tcArrowType2,tcArrowType1)
                        (tcArrow' tcArrowType4)),
  TestCase (assertEqual "tcArrowType5"
                        (tcArrowType3,tcArrowType1)
                        (tcArrow' tcArrowType5)),
  TestCase (assertEqual "tcArrowType6"
                        (tcArrowType1,tcArrowType3)
                        (tcArrow' tcArrowType6))
--TestCase (assertEqual "tcArrowType7"
--                      (tcArrowType1',tcArrowType2')
--                      (tcArrow' tcArrowType7))
  ])
  where tcArrow' ty = run (tcArrow p what doc mid ty) initValueEnv

-- tcBinary: TODO: write test

-- checkSkolems: TODO: write test

-- constrType: TODO: write test
-- varType: TODO: write test
-- funType: TODO: write test

-- collects all tests

tests = TestList [
  testTcConstrTerm,
  testTcArrow
  ]

main = runTestTT tests
