{-
  $Id: CurrySyntaxUtilsTest.hs 591 2004-05-08 15:20:52Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module CurrySyntaxUtilsTest where
import HUnit
import CurrySyntaxUtils
import Ident
import CurrySyntax
import qualified TypeExprCheckTest

mid :: ModuleIdent
mid = TypeExprCheckTest.mid

dataDeclTree1,dataDeclTree2,dataDeclTree3 :: Decl
newtypeDeclTree1 :: Decl
typeDeclTree1, typeDeclTree2 :: Decl
typeClassDeclTree1, typeClassDeclTree2, typeClassDeclTree3 :: Decl
instanceDeclTree1, instanceDeclTree2, instanceDeclTree3 :: Decl
[dataDeclTree1,
 dataDeclTree2,
 dataDeclTree3,
 newtypeDeclTree1,
 typeDeclTree1,
 typeDeclTree2,
 typeClassDeclTree1,
 typeClassDeclTree2,
 typeClassDeclTree3,
 instanceDeclTree1,
 instanceDeclTree2,
 instanceDeclTree3] = TypeExprCheckTest.someDecls

-- tcDefinedBy

testTcDefinedBy = TestLabel "tcDefinedBy" (TestList [
  TestCase (assertEqual "dataDeclTree1" [mkIdent "TC1"]
                        (tcDefinedBy $ dataDeclTree1)),
  TestCase (assertEqual "dataDeclTree2" [mkIdent "TC2"]
                        (tcDefinedBy $ dataDeclTree2)),
  TestCase (assertEqual "dataDeclTree3" [mkIdent "BinTree"]
                        (tcDefinedBy $ dataDeclTree3)),
  TestCase (assertEqual "newtypeDeclTree1" [mkIdent "TupleTree"]
                        (tcDefinedBy $ newtypeDeclTree1)),
  TestCase (assertEqual "typeDeclTree1" [mkIdent "IntTree"]
                        (tcDefinedBy $ typeDeclTree1)),
  TestCase (assertEqual "typeDeclTree2" [mkIdent "TS"]
                        (tcDefinedBy $ typeDeclTree2)),
  TestCase (assertEqual "classDeclTree1" [mkIdent "Eq"]
                        (tcDefinedBy $ typeClassDeclTree1)),
  TestCase (assertEqual "classDeclTree2" [mkIdent "Ord"]
                        (tcDefinedBy $ typeClassDeclTree2)),
  TestCase (assertEqual "classDeclTree3" [mkIdent "Movable"]
                        (tcDefinedBy $ typeClassDeclTree3)),
  TestCase (assertEqual "instanceDeclTree1" []
                        (tcDefinedBy $ instanceDeclTree1))
  ])

-- tvDefinedBy

testTvDefinedBy = TestLabel "tvDefinedBy" (TestList [
  TestCase (assertEqual "dataDeclTree1" [mkIdent "a"]
                        (tvDefinedBy $ dataDeclTree1)),
  TestCase (assertEqual "dataDeclTree2" [mkIdent "a"]
                        (tvDefinedBy $ dataDeclTree2)),
  TestCase (assertEqual "dataDeclTree3" [mkIdent "a"]
                        (tvDefinedBy $ dataDeclTree3)),
  TestCase (assertEqual "newtypeDeclTree1" [mkIdent "t1",mkIdent "t2"]
                        (tvDefinedBy $ newtypeDeclTree1)),
  TestCase (assertEqual "typeDeclTree1" []
                        (tvDefinedBy $ typeDeclTree1)),
  TestCase (assertEqual "typeDeclTree2" [mkIdent "a"]
                        (tvDefinedBy $ typeDeclTree2)),
  TestCase (assertEqual "classDeclTree1" [mkIdent "a"]
                        (tvDefinedBy $ typeClassDeclTree1)),
  TestCase (assertEqual "classDeclTree2" [mkIdent "a"]
                        (tvDefinedBy $ typeClassDeclTree2)),
  TestCase (assertEqual "classDeclTree3" [mkIdent "m"]
                        (tvDefinedBy $ typeClassDeclTree3)),
  TestCase (assertEqual "instanceDeclTree1" []
                        (tvDefinedBy $ instanceDeclTree1)),
  TestCase (assertEqual "instanceDeclTree2" []
                        (tvDefinedBy $ instanceDeclTree2)),
  TestCase (assertEqual "instanceDeclTree3" []
                        (tvDefinedBy $ instanceDeclTree3))
  ])



-- tcUsedBy

testTcUsedBy = TestLabel "tcUsedBy" (TestList [
  TestCase (assertEqual "dataDeclTree1" [qualifyWith mid $ mkIdent "TC1",
                                         qualifyWith mid $ mkIdent "TC2"]
                        (tcUsedBy dataDeclTree1)),
  TestCase (assertEqual "dataDeclTree2" [qualifyWith mid $ mkIdent "TC2",
                                         qualifyWith preludeMIdent $ mkIdent "Int",
                                         qualifyWith mid $ mkIdent "TC1"]
                        (tcUsedBy dataDeclTree2)),
  TestCase (assertEqual "dataDeclTree3" []
                        (tcUsedBy dataDeclTree3)),
  TestCase (assertEqual "newtypeDeclTree1" [qualifyWith mid $ mkIdent "BinTree",
					    qTupleId 2]
                        (tcUsedBy newtypeDeclTree1)),
  TestCase (assertEqual "typeDeclTree1" [qualifyWith mid $ mkIdent "BinTree",
                                         qualifyWith preludeMIdent $ mkIdent "Int"]
                        (tcUsedBy typeDeclTree1)),
  TestCase (assertEqual "typeDeclTree2" [qualifyWith mid $ mkIdent "TC1",
					 qListId]
                        (tcUsedBy typeDeclTree2)),
  TestCase (assertEqual "typeClassDeclTree1" [qArrowId,
					      qualifyWith preludeMIdent boolId]
                        (tcUsedBy typeClassDeclTree1)),
  TestCase (assertEqual "typeClassDeclTree2" [qualifyWith mid $ mkIdent "Eq",
                                              qArrowId,
					      qualifyWith preludeMIdent boolId]
                        (tcUsedBy typeClassDeclTree2)),
  TestCase (assertEqual "typeClassDeclTree3" [qualifyWith mid $ mkIdent "Pickable",
                                              qualifyWith mid $ mkIdent "Eq",
                                              qArrowId,
					      qualifyWith mid $ mkIdent "Position"]
                        (tcUsedBy typeClassDeclTree3)),
  TestCase (assertEqual "instanceDeclTree1" [qualifyWith mid $ mkIdent "Eq",
                                             qualifyWith preludeMIdent intId]
                        (tcUsedBy instanceDeclTree1)),
  TestCase (assertEqual "instanceDeclTree2" [qualifyWith mid $ mkIdent "Eq",
                                             qualifyWith mid $ mkIdent "BinTree"]
                        (tcUsedBy instanceDeclTree2)),
  TestCase (assertEqual "instanceDeclTree3" [qualifyWith mid $ mkIdent "Eq",
                                             qListId]
                        (tcUsedBy instanceDeclTree3))
  ])


-- tvUsedBy

testTvUsedBy = TestLabel "tvUsedBy" (TestList [
  TestCase (assertEqual "dataDeclTree1" [mkIdent "a"]
                        (tvUsedBy dataDeclTree1)),
  TestCase (assertEqual "dataDeclTree2" [mkIdent "a"]
                        (tvUsedBy dataDeclTree2)),
  TestCase (assertEqual "dataDeclTree3" [mkIdent "a"]
                        (tvUsedBy dataDeclTree3)),
  TestCase (assertEqual "newtypeDeclTree1" [mkIdent "t1",mkIdent "t2"]
                        (tvUsedBy newtypeDeclTree1)),
  TestCase (assertEqual "typeDeclTree1" []
                        (tvUsedBy typeDeclTree1)),
  TestCase (assertEqual "typeDeclTree2" [mkIdent "a"]
                        (tvUsedBy typeDeclTree2)),
  TestCase (assertEqual "typeClassDeclTree1" [mkIdent "a"]
                        (tvUsedBy typeClassDeclTree1)),
  TestCase (assertEqual "typeClassDeclTree2" [mkIdent "a"]
                        (tvUsedBy typeClassDeclTree2)),
  TestCase (assertEqual "typeClassDeclTree3" [mkIdent "m"]
                        (tvUsedBy typeClassDeclTree3)),
  TestCase (assertEqual "instanceDeclTree1" []
                        (tvUsedBy instanceDeclTree1)),
  TestCase (assertEqual "instanceDeclTree2" [mkIdent "a"]
                        (tvUsedBy instanceDeclTree2)),
  TestCase (assertEqual "instanceDeclTree3" [mkIdent "a"]
                        (tvUsedBy instanceDeclTree3))
  ])


-- collects all tests

tests = TestList [
  testTcDefinedBy,
  testTvDefinedBy,
  testTcUsedBy,
  testTvUsedBy
  ]

main = runTestTT tests
