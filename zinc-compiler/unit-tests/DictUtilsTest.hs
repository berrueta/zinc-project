{-
  $Id: DictUtilsTest.hs 1003 2004-08-14 10:45:18Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module DictUtilsTest where

import HUnit
import DictUtils
import TopEnv
import TypeClassEnv
import TypeExpr
import Ident
import CurryPP
import Types
import TypeTrans
import List
import TypeExprSubst
import CurrySyntaxUtils

mid :: ModuleIdent
mid = mkMIdent ["mid"]

tcid1, tcid2, tcid3, tcid4, tcid5, tcid6, tcid7, tcid8, tcid9, tcid10  :: Ident
tcid11, tcid12, tcid13, tcid14, tcid15 :: Ident
tcid1  = mkIdent "A"
tcid2  = mkIdent "B"
tcid3  = mkIdent "C"
tcid4  = mkIdent "D"
tcid5  = mkIdent "E"
tcid6  = mkIdent "F"
tcid7  = mkIdent "G"
tcid8  = mkIdent "H"
tcid9  = mkIdent "I"
tcid10 = mkIdent "J"
tcid11 = mkIdent "K"
tcid12 = mkIdent "L"
tcid13 = mkIdent "M"
tcid14 = mkIdent "N"
tcid15 = mkIdent "O"

tcqid1,tcqid2,tcqid3,tcqid4,tcqid5,tcqid6,tcqid7,tcqid8,tcqid9,tcqid10 :: QualIdent
tcqid11,tcqid12,tcqid13,tcqid14,tcqid15 :: QualIdent
tcqid1  = qualifyWith mid tcid1
tcqid2  = qualifyWith mid tcid2
tcqid3  = qualifyWith mid tcid3
tcqid4  = qualifyWith mid tcid4
tcqid5  = qualifyWith mid tcid5
tcqid6  = qualifyWith mid tcid6
tcqid7  = qualifyWith mid tcid7
tcqid8  = qualifyWith mid tcid8
tcqid9  = qualifyWith mid tcid9
tcqid10 = qualifyWith mid tcid10
tcqid11 = qualifyWith mid tcid11
tcqid12 = qualifyWith mid tcid12
tcqid13 = qualifyWith mid tcid13
tcqid14 = qualifyWith mid tcid14
tcqid15 = qualifyWith mid tcid15

tvid1,tvid2,tvid3,tvid4 :: Ident
tvid1 = mkIdent "a"
tvid2 = mkIdent "b"
tvid3 = mkIdent "c"
tvid4 = mkIdent "d"

metid1,metid2,metid3,metid4 :: Ident
metid1 = mkIdent "m1"
metid2 = mkIdent "m2"
metid3 = mkIdent "m3"
metid4 = mkIdent "m4"

mettyexpr1,mettyexpr2,mettyexpr3 :: TypeExprWithContext
mettyexpr1 =
  TypeExprWithContext
    emptyTypeExprContext
    (typeExprApplyArrowConstructor
      (TypeExprVariable tvid1)
      (TypeExprConstructor qBoolId))
mettyexpr2 =
  TypeExprWithContext
    emptyTypeExprContext
    (typeExprApplyArrowConstructor
      (TypeExprVariable tvid1)
      (TypeExprVariable tvid2))
mettyexpr3 =
  TypeExprWithContext
    emptyTypeExprContext
    (typeExprApplyArrowConstructor
      (TypeExprVariable tvid3)
      (TypeExprVariable tvid1))
mettyexpr4 =
  TypeExprWithContext
    (TypeExprContext [TypeExprClassConstraint tcqid1 (TypeExprVariable tvid2)])
    (typeExprApplyArrowConstructor
      (TypeExprVariable tvid1)
      (typeExprApplyArrowConstructor
        (TypeExprVariable tvid2)
        (TypeExprVariable tvid3)))

renameMethods :: [(Ident,TypeExprWithContext)] -> Int ->
                 [(Ident,TypeExprWithContext)]
renameMethods ms n = map (renameMethod n) ms
  where renameMethod n (metId,tyWC) = (metId,substTypeExpr sigma tyWC)
          where tvs = nub (tvUsedBy tyWC)
                sigma = foldr (\tv -> bindTypeExprVar tv (TypeExprVariable (renameIdent tv n))) idSubst tvs

typeClassEnv :: TypeClassEnv
typeClassEnv = bindTypeClass mid tcid1
                 (renameIdent tvid1 1) [] []
             $ bindTypeClass mid tcid2
                 (renameIdent tvid1 2) []
                 (renameMethods [(metid1,mettyexpr1)] 2)
             $ bindTypeClass mid tcid3
                 (renameIdent tvid1 3) []
                 (renameMethods [(metid2,mettyexpr2)] 3)
             $ bindTypeClass mid tcid4
                 (renameIdent tvid1 4) []
                 (renameMethods [(metid3,mettyexpr3)] 4)
             $ bindTypeClass mid tcid5
                 (renameIdent tvid1 5) []
                 (renameMethods [(metid2,mettyexpr2),
                                 (metid3,mettyexpr3)] 5)
             $ bindTypeClass mid tcid6
                 (renameIdent tvid1 6) [tcqid1] []
             $ bindTypeClass mid tcid7
                 (renameIdent tvid1 7) [tcqid3] []
             $ bindTypeClass mid tcid8
                 (renameIdent tvid1 8) [tcqid3]
                 (renameMethods [(metid2,mettyexpr2)] 8)
             $ bindTypeClass mid tcid9
                 (renameIdent tvid1 9) [tcqid3,tcqid4] []
             $ bindTypeClass mid tcid10
                 (renameIdent tvid1 10) []
                 (renameMethods [(metid4,mettyexpr4)] 10)
             $ bindTypeClass mid tcid11
                 (renameIdent tvid1 11) [tcqid2]
                 (renameMethods [(metid4,mettyexpr4)] 11)
             $ bindTypeClass mid tcid12
                 (renameIdent tvid1 12) [tcqid5]
                 (renameMethods [(metid4,mettyexpr4)] 12)
             $ bindTypeClass mid tcid13
                 (renameIdent tvid1 13) [tcqid10]
                 (renameMethods [(metid2,mettyexpr2)] 13)
             $ bindTypeClass mid tcid14
                 (renameIdent tvid1 14) [tcqid13]
                 (renameMethods [(metid4,mettyexpr4)] 14)
             $ emptyTopEnv

-- dictTypeClassDataLhs

testDictTypeClassDataLhs = TestLabel "dictTypeClassDataLhs" (TestList [
  TestCase (assertEqual "A" "mid._dictData_A a"
              (f tcqid1)),
  TestCase (assertEqual "B" "mid._dictData_B a"
              (f tcqid2)),
  TestCase (assertEqual "C" "mid._dictData_C a b"
              (f tcqid3)),
  TestCase (assertEqual "D" "mid._dictData_D a c"
              (f tcqid4)),
  TestCase (assertEqual "E" "mid._dictData_E a b c"
              (f tcqid5)),
  TestCase (assertEqual "F" "mid._dictData_F a"
              (f tcqid6)),
  TestCase (assertEqual "G" "mid._dictData_G a mid_C_b"
              (f tcqid7)),
  TestCase (assertEqual "H" "mid._dictData_H a mid_C_b b"
              (f tcqid8)),
  TestCase (assertEqual "I" "mid._dictData_I a mid_C_b mid_D_c"
              (f tcqid9)),
  TestCase (assertEqual "J" "mid.A b => mid._dictData_J a b c"
              (f tcqid10)),
  TestCase (assertEqual "K" "mid.A b => mid._dictData_K a b c"
              (f tcqid11)),
  TestCase (assertEqual "L" "mid.A b => mid._dictData_L a mid_E_b mid_E_c b c"
              (f tcqid12)),
  TestCase (assertEqual "M" "mid.A mid_J_b => mid._dictData_M a mid_J_b mid_J_c b"
              (f tcqid13)),
  TestCase (assertEqual "N" "(mid.A mid_M_mid_J_b, mid.A b) => mid._dictData_N a mid_M_mid_J_b\n\t\t\t\t\t\t  mid_M_mid_J_c mid_M_b b c"
              (f tcqid14))
  ])
  where f = show . ppTypeExprWithContext . dictTypeClassDataLhs typeClassEnv

-- dictTypeClassDataRhs

testDictTypeClassDataRhs = TestLabel "dictTypeClassRhs" (TestList [
  TestCase (assertEqual "A" "mid._DictData_A"
              (f tcqid1)),
  TestCase (assertEqual "B" "mid._DictData_B (a -> Bool)"
              (f tcqid2)),
  TestCase (assertEqual "C" "mid._DictData_C (a -> b)"
              (f tcqid3)),
  TestCase (assertEqual "D" "mid._DictData_D (c -> a)"
              (f tcqid4)),
  TestCase (assertEqual "E" "mid._DictData_E (a -> b) (c -> a)"
              (f tcqid5)),
  TestCase (assertEqual "F" "mid._DictData_F (mid._dictData_A a)"
              (f tcqid6)),
  TestCase (assertEqual "G" "mid._DictData_G (mid._dictData_C a mid_C_b)"
              (f tcqid7)),
  TestCase (assertEqual "H" "mid._DictData_H (mid._dictData_C a mid_C_b) (a -> b)"
              (f tcqid8)),
  TestCase (assertEqual "I" "mid._DictData_I (mid._dictData_C a mid_C_b)\n\t\t(mid._dictData_D a mid_D_c)"
              (f tcqid9)),
  TestCase (assertEqual "J" "mid.A b => mid._DictData_J (a -> b -> c)"
              (f tcqid10)),
  TestCase (assertEqual "K" "mid.A b => mid._DictData_K (mid._dictData_B a) (a -> b -> c)"
              (f tcqid11)),
  TestCase (assertEqual "L" "mid.A b => mid._DictData_L (mid._dictData_E a mid_E_b mid_E_c)\n\t\t\t   (a -> b -> c)"
              (f tcqid12)),
  TestCase (assertEqual "M" "mid.A mid_J_b => mid._DictData_M (mid._dictData_J a mid_J_b\n\t\t\t\t\t\t  mid_J_c)\n\t\t\t\t (a -> b)"
              (f tcqid13)),
  TestCase (assertEqual "N" "(mid.A mid_M_mid_J_b,\n mid.A b) => mid._DictData_N (mid._dictData_M a mid_M_mid_J_b\n\t\t\t\t\t      mid_M_mid_J_c mid_M_b)\n\t\t\t     (a -> b -> c)"
              (f tcqid14))
  ])
  where f = show . ppTypeExprWithContext . fst . dictTypeClassDataRhs typeClassEnv

-- traverserFunctionTypeExpr

testTraverserFunctionTypeExpr = TestLabel "traverserFunctionTypeExpr" (TestList [
  TestCase (assertEqual "01" "mid._dictData_F a -> mid._dictData_A a"
              (f tcqid6 tcqid1)),
  TestCase (assertEqual "02" "mid._dictData_G a b -> mid._dictData_C a b"
              (f tcqid7 tcqid3)),
  TestCase (assertEqual "03" "mid._dictData_H a b c -> mid._dictData_C a b"
              (f tcqid8 tcqid3)),
  TestCase (assertEqual "04" "mid._dictData_I a b c -> mid._dictData_C a b"
              (f tcqid9 tcqid3)),
  TestCase (assertEqual "05" "mid._dictData_I a c b -> mid._dictData_D a b"
              (f tcqid9 tcqid4)),
  TestCase (assertEqual "06" "(mid._dictData_A b -> mid._dictData_K a b c) -> mid._dictData_B a"
              (f tcqid11 tcqid2)),
  TestCase (assertEqual "07" "(mid._dictData_A d -> mid._dictData_L a b c d e) ->\nmid._dictData_E a b c"
              (f tcqid12 tcqid5)),
  TestCase (assertEqual "08" "(mid._dictData_A b -> mid._dictData_M a b c d) ->\nmid._dictData_A b -> mid._dictData_J a b c"
              (f tcqid13 tcqid10)),
  TestCase (assertEqual "09" "(mid._dictData_A b -> mid._dictData_A e ->\n mid._dictData_N a b c d e f) ->\nmid._dictData_A b -> mid._dictData_M a b c d"
              (f tcqid14 tcqid13))
  ])
  where f tc stc = show $ ppTypeExpr 0 $ traverserFunctionTypeExpr typeClassEnv tc stc

-- dictTransType

ty1, ty2, ty3 :: Type
ty1 = TypeVariable 0
ty2 = typeArrow (TypeVariable 0) (TypeVariable 1)
ty3 = typeArrow (TypeVariable 0) (typeArrow (TypeVariable 1) (TypeConstructor qBoolId))

tcc1,tcc2,tcc3,tcc4,tcc5,tcc6,tcc7 :: TypeClassConstraint
tcc1 = TypeClassConstraint tcqid1 (TypeVariable 0)
tcc2 = TypeClassConstraint tcqid6 (TypeVariable 0)
tcc3 = TypeClassConstraint tcqid10 (TypeVariable 0)
tcc4 = TypeClassConstraint tcqid10 (TypeVariable 1)
tcc5 = TypeClassConstraint tcqid12 (TypeVariable 0)
tcc6 = TypeClassConstraint tcqid13 (TypeVariable 0)
tcc7 = TypeClassConstraint tcqid1 (TypeVariable 1)

tyWC01, tyWC02, tyWC03, tyWC04, tyWC05, tyWC06, tyWC07, tyWC08 :: TypeWithContext
tyWC09, tyWC10, tyWC11 :: TypeWithContext
tyWC01 = TypeWithContext emptyTypeContext ty1
tyWC02 = TypeWithContext emptyTypeContext ty2
tyWC03 = TypeWithContext emptyTypeContext ty3
tyWC04 = TypeWithContext (TypeContext [tcc1]) ty1
tyWC05 = TypeWithContext (TypeContext [tcc2]) ty1
tyWC06 = TypeWithContext (TypeContext [tcc1,tcc2]) ty1
tyWC07 = TypeWithContext (TypeContext [tcc3]) ty1
tyWC08 = TypeWithContext (TypeContext [tcc3]) ty3
tyWC09 = TypeWithContext (TypeContext [tcc3,tcc4]) ty3
tyWC10 = TypeWithContext (TypeContext [tcc5]) ty3
tyWC11 = TypeWithContext (TypeContext [tcc7,tcc6]) ty3

testDictTransType = TestLabel "dictTransType" (TestList [
  TestCase (assertEqual "01" "a"
              (f tyWC01)),
  TestCase (assertEqual "02" "a -> b"
              (f tyWC02)),
  TestCase (assertEqual "03" "a -> b -> Bool"
              (f tyWC03)),
  TestCase (assertEqual "04" "_dictData_A a -> a"
              (f tyWC04)),
  TestCase (assertEqual "05" "_dictData_F a -> a"
              (f tyWC05)),
  TestCase (assertEqual "06" "_dictData_A a -> _dictData_F a -> a"
              (f tyWC06)),
  TestCase (assertEqual "07" "(_dictData_A b -> _dictData_J a b c) -> a"
              (f tyWC07)),
  TestCase (assertEqual "08" "(_dictData_A c -> _dictData_J a c d) -> a -> b -> Bool"
              (f tyWC08)),
  TestCase (assertEqual "09" "(_dictData_A e -> _dictData_J a e f) ->\n(_dictData_A c -> _dictData_J b c d) -> a -> b -> Bool"
              (f tyWC09)),
  TestCase (assertEqual "10" "(_dictData_A e -> _dictData_L a c d e f) -> a -> b -> Bool"
              (f tyWC10)),
  TestCase (assertEqual "11" "_dictData_A b -> (_dictData_A c -> _dictData_M a c d e) -> a ->\nb -> Bool"
              (f tyWC11))
  ])
  where f = show . ppType mid . dictTransType typeClassEnv

-- collects all tests

tests = TestList [
  testDictTypeClassDataLhs,
  testDictTypeClassDataRhs,
  testTraverserFunctionTypeExpr,
  testDictTransType
  ]

main = runTestTT tests
