{-
  $Id: TypeTransTest.hs 1289 2004-09-03 13:56:57Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeTransTest where

import HUnit
import TypeTrans
import Types
import Ident
import TypeExpr

mid :: ModuleIdent
mid = mkMIdent ["mid"]

ty1,ty2,ty3,ty4,ty5,ty6,ty7,ty8,ty9,ty10 :: Type
ty11,ty12,ty13,ty14,ty17 :: Type
ty1  = typeVar 0
ty2  = typeVar 1
ty3  = unitType
ty4  = boolType
ty5  = stringType
ty6  = listType ty4
ty7  = successType
ty8  = ioType ty1
ty9  = tupleType [ty4,ty1]
ty10 = listType ty1
ty11 = typeArrow ty5 ty1
ty12 = typeArrow ty5 (typeArrow ty1 ty2)
ty13 = typeArrow (typeArrow ty5 ty1) ty2
ty14 = typeApply (TypeConstructor $ qualifyWith mid $ mkIdent "tc1") [ty1]
ty17 = ioType ty11

tyExpr1,tyExpr2,tyExpr3,tyExpr4,tyExpr5,tyExpr6 :: TypeExpr
tyExpr7,tyExpr8,tyExpr9,tyExpr10,tyExpr11,tyExpr12 :: TypeExpr
tyExpr13,tyExpr15,tyExpr16,tyExpr17 :: TypeExpr
tyExpr1  = TypeExprVariable (mkIdent "a")
tyExpr2  = TypeExprVariable (mkIdent "b")
tyExpr3  = typeExprUnitConstructor
tyExpr4  = TypeExprConstructor (qualify $ mkIdent "Bool")
tyExpr5  = typeExprApplyListConstructor (TypeExprConstructor (qualify $ mkIdent "Char"))
tyExpr6  = typeExprApplyListConstructor tyExpr4
tyExpr7  = TypeExprConstructor (qualify $ mkIdent "Success")
tyExpr8  = typeExprApplyConstructor (qualify $ mkIdent "IO") [tyExpr1]
tyExpr9  = typeExprApplyTupleConstructor [tyExpr4,tyExpr1]
tyExpr10 = typeExprApplyListConstructor tyExpr1
tyExpr11 = typeExprApplyArrowConstructor tyExpr5 tyExpr1
tyExpr12 = typeExprApplyArrowConstructor tyExpr5 (typeExprApplyArrowConstructor tyExpr1 tyExpr2)
tyExpr13 = typeExprApplyArrowConstructor (typeExprApplyArrowConstructor tyExpr5 tyExpr1) tyExpr2
tyExpr15 = TypeExprVariable (mkIdent "_")
tyExpr16 = typeExprApplyTupleConstructor [tyExpr1,tyExpr15,tyExpr2]
tyExpr17 = typeExprApplyConstructor (qualify $ mkIdent "IO") [tyExpr11]

-- toType:

testToType = TestLabel "toType" (TestList [
  TestCase (assertEqual "tyExpr1"  (unqualifyIdentsIn preludeMIdent ty1)
             (toType [] tyExpr1)),
  TestCase (assertEqual "tyExpr2"  (unqualifyIdentsIn preludeMIdent ty1)
             (toType [] tyExpr2)),
  TestCase (assertEqual "tyExpr3"  (unqualifyIdentsIn preludeMIdent ty3)
             (toType [] tyExpr3)),
  TestCase (assertEqual "tyExpr4"  (unqualifyIdentsIn preludeMIdent ty4)
             (toType [] tyExpr4)),
  TestCase (assertEqual "tyExpr5"  (unqualifyIdentsIn preludeMIdent ty5)
             (toType [] tyExpr5)),
  TestCase (assertEqual "tyExpr6"  (unqualifyIdentsIn preludeMIdent ty6)
             (toType [] tyExpr6)),
  TestCase (assertEqual "tyExpr7"  (unqualifyIdentsIn preludeMIdent ty7)
             (toType [] tyExpr7)),
  TestCase (assertEqual "tyExpr8"  (unqualifyIdentsIn preludeMIdent ty8)
             (toType [] tyExpr8)),
  TestCase (assertEqual "tyExpr9"  (unqualifyIdentsIn preludeMIdent ty9)
             (toType [] tyExpr9)),
  TestCase (assertEqual "tyExpr10" (unqualifyIdentsIn preludeMIdent ty10)
             (toType [] tyExpr10)),
  TestCase (assertEqual "tyExpr11"  (unqualifyIdentsIn preludeMIdent ty11)
             (toType [] tyExpr11)),
  TestCase (assertEqual "tyExpr12"  (unqualifyIdentsIn preludeMIdent ty12)
             (toType [] tyExpr12)),
  TestCase (assertEqual "tyExpr13" (unqualifyIdentsIn preludeMIdent ty13)
             (toType [] tyExpr13)),
  TestCase (assertEqual "tyExpr17" (unqualifyIdentsIn preludeMIdent ty17)
             (toType [] tyExpr17))
  ])

-- toTypes: TODO: write test

-- toTypeWithContext

qid1 :: QualIdent
qid1 = qualifyWith mid $ mkIdent "MyEq"

tyWC1,tyWC2 :: TypeWithContext
tyWC1 = TypeWithContext
          (TypeContext [TypeClassConstraint qid1 $ TypeVariable 1])
          (TypeApplication (TypeVariable 0) (TypeVariable 1))
tyWC2 = TypeWithContext
          (TypeContext [TypeClassConstraint qid1 $ TypeVariable 0])
          (TypeApplication (TypeVariable 1) (TypeVariable 0))

tyexprWC1,tyexprWC2 :: TypeExprWithContext
tyexprWC1 = TypeExprWithContext
              (TypeExprContext [TypeExprClassConstraint qid1 $ TypeExprVariable $ mkIdent "b"])
              (TypeExprApplication (TypeExprVariable $ mkIdent "a")
                                   (TypeExprVariable $ mkIdent "b"))
tyexprWC2 = TypeExprWithContext
              (TypeExprContext [TypeExprClassConstraint qid1 $ TypeExprVariable $ mkIdent "a"])
              (TypeExprApplication (TypeExprVariable $ mkIdent "b")
                                   (TypeExprVariable $ mkIdent "a"))

testToTypeWithContext = TestLabel "toTypeWithContext" (TestList [
  TestCase (assertEqual "tyWC1"  tyWC1 (toTypeWithContext [] tyexprWC1)),
  TestCase (assertEqual "tyWC2"  tyWC1 (toTypeWithContext [] tyexprWC2)),
  TestCase (assertEqual "tyWC2'" tyWC2 (toTypeWithContext [mkIdent "b"] tyexprWC1))
  ])

-- fromType

testFromType = TestLabel "fromType" (TestList [
  TestCase (assertEqual "ty1"  tyExpr1 
             (fromType $ unqualifyIdentsIn preludeMIdent ty1 )),
  TestCase (assertEqual "ty2"  tyExpr2 
             (fromType $ unqualifyIdentsIn preludeMIdent ty2 )),
  TestCase (assertEqual "ty3"  tyExpr3 
             (fromType $ unqualifyIdentsIn preludeMIdent ty3 )),
  TestCase (assertEqual "ty4"  tyExpr4 
             (fromType $ unqualifyIdentsIn preludeMIdent ty4 )),
  TestCase (assertEqual "ty5"  tyExpr5 
             (fromType $ unqualifyIdentsIn preludeMIdent ty5 )),
  TestCase (assertEqual "ty6"  tyExpr6 
             (fromType $ unqualifyIdentsIn preludeMIdent ty6 )),
  TestCase (assertEqual "ty7"  tyExpr7 
             (fromType $ unqualifyIdentsIn preludeMIdent ty7 )),
  TestCase (assertEqual "ty8"  tyExpr8 
             (fromType $ unqualifyIdentsIn preludeMIdent ty8 )),
  TestCase (assertEqual "ty9"  tyExpr9 
             (fromType $ unqualifyIdentsIn preludeMIdent ty9 )),
  TestCase (assertEqual "ty10" tyExpr10
             (fromType $ unqualifyIdentsIn preludeMIdent ty10)),
  TestCase (assertEqual "ty11" tyExpr11
             (fromType $ unqualifyIdentsIn preludeMIdent ty11)),
  TestCase (assertEqual "ty12" tyExpr12
             (fromType $ unqualifyIdentsIn preludeMIdent ty12)),
  TestCase (assertEqual "ty13" tyExpr13
             (fromType $ unqualifyIdentsIn preludeMIdent ty13)),
  TestCase (assertEqual "ty17" tyExpr17
             (fromType $ unqualifyIdentsIn preludeMIdent ty17))
  ])

-- fromTypeWithContext

testFromTypeWithContext = TestLabel "fromTypeWithContext" (TestList [
  TestCase (assertEqual "tyWC1"  tyexprWC1
             (fromTypeWithContext $ unqualifyIdentsIn preludeMIdent tyWC1 )),
  TestCase (assertEqual "tyWC2"  tyexprWC2
             (fromTypeWithContext $ unqualifyIdentsIn preludeMIdent tyWC2 ))
  ])

-- qualifyIdentsIn:

testQualifyIdentsIn = TestLabel "qualifyIndentsIn" (TestList [
  TestCase (assertEqual "ty1"  ty1 (qualifyIdentsIn mid ty1 )),
  TestCase (assertEqual "ty3"  ty3 (qualifyIdentsIn mid ty3 )),
  TestCase (assertEqual "ty4"  ty4 (qualifyIdentsIn mid ty4 )),
  TestCase (assertEqual "ty4b" ty4 (qualifyIdentsIn preludeMIdent ty4 )),
  TestCase (assertEqual "ty14"  ty14 (qualifyIdentsIn mid ty14 )),
  TestCase (assertEqual "ty14b" ty14 (qualifyIdentsIn preludeMIdent ty14 )),
  TestCase (assertEqual "ty14c" ty14 (qualifyIdentsIn mid (typeApply (TypeConstructor $ qualify $ mkIdent "tc1") [ty1]) ))
  ])

-- unqualifyIdentsIn

testUnqualifyIdentsIn = TestLabel "unqualifyIndentsIn" (TestList [
  TestCase (assertEqual "ty1"  ty1 (unqualifyIdentsIn mid ty1 )),
  TestCase (assertEqual "ty3"  ty3 (unqualifyIdentsIn mid ty3 )),
  TestCase (assertEqual "ty4"  ty4 (unqualifyIdentsIn mid ty4 )),
  TestCase (assertEqual "ty4b" (TypeConstructor $ qualify boolId) (unqualifyIdentsIn preludeMIdent ty4 )),
  TestCase (assertEqual "ty14"  (typeApply (TypeConstructor $ qualify $ mkIdent "tc1") [ty1]) (unqualifyIdentsIn mid ty14 )),
  TestCase (assertEqual "ty14b" ty14 (unqualifyIdentsIn preludeMIdent ty14 ))
  ])

-- ppType:

testPPType = TestLabel "ppType" (TestList [
  TestCase (assertEqual "ty1"  "a" (show $ ppType mid ty1 )),
  TestCase (assertEqual "ty2"  "b" (show $ ppType mid ty2 )),
  TestCase (assertEqual "ty3"  "()" (show $ ppType mid ty3 )),
  TestCase (assertEqual "ty4"  "prelude.Bool" (show $ ppType mid ty4 )),
  TestCase (assertEqual "ty5"  "[prelude.Char]" (show $ ppType mid ty5 )),
  TestCase (assertEqual "ty6"  "[prelude.Bool]" (show $ ppType mid ty6 )),
  TestCase (assertEqual "ty7"  "prelude.Success" (show $ ppType mid ty7 )),
  TestCase (assertEqual "ty8"  "prelude.IO a" (show $ ppType mid ty8 )),
  TestCase (assertEqual "ty9"  "(prelude.Bool, a)" (show $ ppType mid ty9 )),
  TestCase (assertEqual "ty10" "[a]" (show $ ppType mid ty10)),
  TestCase (assertEqual "ty11" "[prelude.Char] -> a" (show $ ppType mid ty11)),
  TestCase (assertEqual "ty12" "[prelude.Char] -> a -> b" (show $ ppType mid ty12)),
  TestCase (assertEqual "ty13" "([prelude.Char] -> a) -> b" (show $ ppType mid ty13)),
  TestCase (assertEqual "ty14" "tc1 a" (show $ ppType mid ty14)),
  TestCase (assertEqual "ty17" "prelude.IO ([prelude.Char] -> a)" (show $ ppType mid ty17))
  ])

-- ppTypeScheme:

tyScheme1,tyScheme2 :: TypeScheme
tyScheme1 = ForAll 0 (TypeWithContext emptyTypeContext ty1)
tyScheme2 = ForAll 1 (TypeWithContext emptyTypeContext ty9)

testPPTypeScheme = TestLabel "ppTypeScheme" (TestList [
  TestCase (assertEqual "tyScheme1"  "a" (show $ ppTypeScheme mid tyScheme1 )),
  TestCase (assertEqual "tyScheme2"  "(prelude.Bool, a)" (show $ ppTypeScheme mid tyScheme2 ))
  ])

-- nameSigType

tyExpr1',tyExpr2',tyExpr15',tyExpr16' :: TypeExpr
tyExpr1'  = tyExpr1
tyExpr2'  = tyExpr2
tyExpr15' = TypeExprVariable (mkIdent "a")
tyExpr16' = typeExprApplyTupleConstructor [tyExpr1,TypeExprVariable (mkIdent "c"),tyExpr2]

testNameSigType = TestLabel "nameSigType" (TestList [
  TestCase (assertEqual "tyExpr1"  tyExpr1'  (nameSigType tyExpr1 )), 
  TestCase (assertEqual "tyExpr2"  tyExpr2'  (nameSigType tyExpr2 )),
  TestCase (assertEqual "tyExpr15" tyExpr15' (nameSigType tyExpr15)),
  TestCase (assertEqual "tyExpr16" tyExpr16' (nameSigType tyExpr16))
 ])

-- Collects all tests

tests = TestList [
  testToType,
  testToTypeWithContext,
  testFromType,
  testFromTypeWithContext,
  testQualifyIdentsIn,
  testUnqualifyIdentsIn,
  testPPType,
  testPPTypeScheme,
  testNameSigType
  ]

main = runTestTT tests
