{-
  $Id: PredefTypesTest.hs 1337 2004-09-06 18:44:31Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module PredefTypesTest where

import HUnit
import PredefTypes
import PEnv
import TypeConstructorEnv
import ValueEnv
import KindEnv
import Ident
import CurrySyntax
import Types
import Kind

mid :: ModuleIdent
mid = mkMIdent ["demoModule"]

id1 :: Ident
id1 = mkIdent "id1"

qId1 :: QualIdent
qId1 = qualifyWith mid id1

-- initPEnv

testInitPEnv = TestLabel "initPEnv" (TestList [
  TestCase (assertEqual "cons"         [consPrecInfo] 
              (lookupP consId initPEnv)), 
  TestCase (assertEqual "cons (qual)"  [consPrecInfo] 
              (qualLookupP qConsId initPEnv)),
  TestCase (assertEqual "id1"          [] 
              (lookupP id1 initPEnv)), 
  TestCase (assertEqual "id1 (qual)"   [] 
              (qualLookupP qId1 initPEnv))
  ])
  where consPrecInfo = (PrecInfo qConsId (OpPrec InfixR 5))

-- initTCEnv

testInitTCEnv = TestLabel "initPEnv" (TestList [
  TestCase (assertEqual "id1"         [] 
              (lookupTC id1 initTCEnv)), 
  TestCase (assertEqual "id1 (qual)"  [] 
              (qualLookupTC qId1 initTCEnv)),
  TestCase (assertEqual "()"          [unitTypeInfo] 
              (lookupTC unitId initTCEnv)), 
  TestCase (assertEqual "() (qual)"   [unitTypeInfo] 
              (qualLookupTC qUnitId initTCEnv)),
  TestCase (assertEqual "[]"          [listTypeInfo] 
              (lookupTC listId initTCEnv)), 
  TestCase (assertEqual "[] (qual)"   [listTypeInfo] 
              (qualLookupTC qListId initTCEnv)),
  TestCase (assertEqual "->"          [arrowTypeInfo] 
              (lookupTC arrowId initTCEnv)), 
  TestCase (assertEqual "-> (qual)"   [arrowTypeInfo] 
              (qualLookupTC qArrowId initTCEnv))
  ])
  where unitTypeInfo = DataType qUnitId 0 [Just (Data unitId 0 [])]
        listTypeInfo = DataType qListId 1 [Just (Data listId 0 []),Just (Data consId 0 [TypeVariable 0,typeApply (TypeConstructor qListId) [TypeVariable 0]])]
        arrowTypeInfo = DataType qArrowId 2 [Just (Data arrowId 0 [])]

-- initDCEnv:

testInitDCEnv = TestLabel "initDCEnv" (TestList [
  TestCase (assertEqual "id1"         [] 
              (lookupValue id1 initDCEnv)), 
  TestCase (assertEqual "id1 (qual)"  [] 
              (qualLookupValue qId1 initDCEnv)),
  TestCase (assertEqual "()"          [noDataValueInfo] 
              (lookupValue noDataId initDCEnv)), 
  TestCase (assertEqual "() (qual)"   [noDataValueInfo] 
              (qualLookupValue qNoDataId initDCEnv)),
  TestCase (assertEqual ":"           [consValueInfo] 
              (lookupValue consId initDCEnv)), 
  TestCase (assertEqual ": (qual)"    [consValueInfo] 
              (qualLookupValue qConsId initDCEnv)),
  TestCase (assertEqual "[]"           [nilValueInfo] 
              (lookupValue nilId initDCEnv)), 
  TestCase (assertEqual "[] (qual)"    [nilValueInfo] 
              (qualLookupValue qNilId initDCEnv)),
  TestCase (assertEqual "->"           [arrowValueInfo] 
              (lookupValue arrowId initDCEnv)), 
  TestCase (assertEqual "-> (qual)"    [arrowValueInfo] 
              (qualLookupValue qArrowId initDCEnv))
  ])
  where noDataValueInfo = DataConstructor qNoDataId (ForAllExist 0 0 (TypeConstructor qUnitId))
        consValueInfo = DataConstructor qConsId (ForAllExist 1 0 (typeApply (TypeConstructor qArrowId) [TypeVariable 0,typeApply (TypeConstructor qArrowId) [typeApply (TypeConstructor qListId) [TypeVariable 0],typeApply (TypeConstructor qListId) [TypeVariable 0]]]))
        nilValueInfo = DataConstructor qNilId (ForAllExist 1 0 (typeApply (TypeConstructor qListId) [TypeVariable 0]))
        arrowValueInfo = DataConstructor qArrowId (ForAllExist 2 0 (typeApply (TypeConstructor qArrowId) [TypeVariable 0,TypeVariable 1]))

-- initKindEnv

testInitKindEnv = TestLabel "initKindEnv" (TestList [
  TestCase (assertEqual "unit"
              [KindInfo (qUnitId) Star]
              (lookupKind unitId kindEnv)),
  TestCase (assertEqual "unit (qual)"
              [KindInfo (qUnitId) Star]
              (qualLookupKind qUnitId kindEnv)),
  TestCase (assertEqual "[]"
              [KindInfo (qListId) (KFun Star Star)]
              (lookupKind listId kindEnv)),
  TestCase (assertEqual "[] (qual)"
              [KindInfo (qListId) (KFun Star Star)]
              (qualLookupKind qListId kindEnv)),
  TestCase (assertEqual "->"
              [KindInfo (qArrowId) (KFun Star $ KFun Star Star)]
              (lookupKind arrowId kindEnv)),
  TestCase (assertEqual "-> (qual)"
              [KindInfo (qArrowId) (KFun Star $ KFun Star Star)]
              (qualLookupKind qArrowId kindEnv)),
  TestCase (assertEqual "2-tuple"
              [KindInfo (qTupleId 2) (KFun Star $ KFun Star Star)]
              (lookupKind (tupleId 2) kindEnv)),
  TestCase (assertEqual "2-tuple (qual)"
              [KindInfo (qTupleId 2) (KFun Star $ KFun Star Star)]
              (qualLookupKind (qTupleId 2) kindEnv)),
  TestCase (assertEqual "3-tuple"
              [KindInfo (qTupleId 3) (KFun Star $ KFun Star $ KFun Star Star)]
              (lookupKind (tupleId 3) kindEnv)),
  TestCase (assertEqual "3-tuple (qual)"
              [KindInfo (qTupleId 3) (KFun Star $KFun Star $ KFun Star Star)]
              (qualLookupKind (qTupleId 3) kindEnv))
  ])
  where kindEnv = initKindEnv

-- Collects all tests

tests = TestList [
  testInitPEnv,
  testInitTCEnv,
  testInitDCEnv,
  testInitKindEnv
  ]

main = runTestTT tests
