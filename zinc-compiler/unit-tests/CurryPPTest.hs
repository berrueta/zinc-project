{-
  $Id: CurryPPTest.hs 1289 2004-09-03 13:56:57Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module CurryPPTest where

import HUnit
import CurryPP
import Pretty
import CurryLexer
import LLParseComb
import CurrySyntax
import CurryParser
import Error
import TypeExpr
import Ident
import qualified CurryParserTest


-- ppModule, ppModuleHeader, ppExportSpec, ppExport: TODO write tests
-- ppBlock

-- ppDecl: TODO: incomplete test

testPpDecl = TestLabel "ppDecl" (TestList [
  TestCase (assertEqual "typeClassDecl1" CurryParserTest.typeClassDeclCode1
    (show (ppDecl $ CurryParserTest.typeClassDeclTree1 1))),
  TestCase (assertEqual "typeClassDecl2" CurryParserTest.typeClassDeclCode2
    (show (ppDecl $ CurryParserTest.typeClassDeclTree2 1))),
  TestCase (assertEqual "typeClassDecl3" CurryParserTest.typeClassDeclCode3
    (show (ppDecl $ CurryParserTest.typeClassDeclTree3 1))),
  TestCase (assertEqual "instanceDecl1" CurryParserTest.instanceDeclCode1
    (show (ppDecl $ CurryParserTest.instanceDeclTree1 1))),
  TestCase (assertEqual "instanceDecl2" CurryParserTest.instanceDeclCode2
    (show (ppDecl $ CurryParserTest.instanceDeclTree2 1))),
  TestCase (assertEqual "instanceDecl3" CurryParserTest.instanceDeclCode3
    (show (ppDecl $ CurryParserTest.instanceDeclTree3 1)))
  ])

-- ppImportSpec, ppImport, ppPrec: TODO write tests
-- ppTypeDeclLhs, ppConstr, ppNConstr, ppEquation: TODO write tests
-- ppRule, ppLocalDefs: TODO write tests
-- ppInterface: TODO: write tests

-- ppIDecl

testPpIDecl = TestLabel "ppIDecl" (TestList [
  TestCase (assertEqual "typeClassDecl1" CurryParserTest.iTypeClassDeclCode1
    (show (ppIDecl $ CurryParserTest.iTypeClassDeclTree1 1))),
  TestCase (assertEqual "typeClassDecl2" CurryParserTest.iTypeClassDeclCode2
    (show (ppIDecl $ CurryParserTest.iTypeClassDeclTree2 1))),
  TestCase (assertEqual "typeClassDecl3" CurryParserTest.iTypeClassDeclCode3
    (show (ppIDecl $ CurryParserTest.iTypeClassDeclTree3 1)))
  ])

-- ppITypeDeclLhs: TODO: write tests

-- ppTypeExpr

typeCodeB1 :: String
typeCodeB1 = "(b -> c) -> (a -> b) -> a -> c"

parseType0 :: String -> TypeExpr
parseType0 s = ok $ applyParser type0 lexer CurryParserTest.filePath s

testPpTypeExpr = TestLabel "ppTypeExpr" (TestList [
  TestCase (assertEqual "typeCode1" CurryParserTest.typeCode1
    (show (ppTypeExpr 0 CurryParserTest.typeExpr1))),
  TestCase (assertEqual "typeCode2" CurryParserTest.typeCode2
    (show (ppTypeExpr 0 CurryParserTest.typeExpr2))),
  TestCase (assertEqual "typeCode3" CurryParserTest.typeCode3
    (show (ppTypeExpr 0 CurryParserTest.typeExpr3))),
  TestCase (assertEqual "typeCode4" CurryParserTest.typeCode4
    (show (ppTypeExpr 0 CurryParserTest.typeExpr4))),
  TestCase (assertEqual "typeCode5" CurryParserTest.typeCode5
    (show (ppTypeExpr 0 CurryParserTest.typeExpr5))),
  TestCase (assertEqual "typeCode6" CurryParserTest.typeCode6
    (show (ppTypeExpr 0 CurryParserTest.typeExpr6))),
  TestCase (assertEqual "typeCode7" CurryParserTest.typeCode7
    (show (ppTypeExpr 0 CurryParserTest.typeExpr7))),
  TestCase (assertEqual "typeCode8" CurryParserTest.typeCode8
    (show (ppTypeExpr 0 CurryParserTest.typeExpr8))),
  TestCase (assertEqual "typeCode9" CurryParserTest.typeCode9
    (show (ppTypeExpr 0 CurryParserTest.typeExpr9))),
  TestCase (assertEqual "typeCode10" CurryParserTest.typeCode10
    (show (ppTypeExpr 0 CurryParserTest.typeExpr10))),
  TestCase (assertEqual "typeCode11" CurryParserTest.typeCode11
    (show (ppTypeExpr 0 CurryParserTest.typeExpr11))),
  TestCase (assertEqual "typeCode12" CurryParserTest.typeCode1
    (show (ppTypeExpr 0 CurryParserTest.typeExpr12))),
  TestCase (assertEqual "typeCode13" CurryParserTest.typeCode13
    (show (ppTypeExpr 0 CurryParserTest.typeExpr13)))
  ])

testPpTypeExprB = TestLabel "ppTypeExprB" (TestList [
  TestCase (assertEqual "typeCodeB1" typeCodeB1
    (show $ ppTypeExpr 0 $ parseType0 typeCodeB1))
  ])

-- ppTypeExprContextPrefix

typeExprContext1, typeExprContext2, typeExprContext3 :: TypeExprContext
typeExprContext1 = TypeExprContext []
typeExprContext2 = TypeExprContext
  [TypeExprClassConstraint (qualify $ mkIdent "Eq") (TypeExprVariable $ mkIdent "a")]
typeExprContext3 = TypeExprContext
  [TypeExprClassConstraint (qualify $ mkIdent "Eq") (TypeExprVariable $ mkIdent "a"),
   TypeExprClassConstraint (qualify $ mkIdent "Ord") (TypeExprVariable $ mkIdent "b")]

testPpTypeExprContextPrefix = TestLabel "ppTypeExprContextPrefix" (TestList [
  TestCase (assertEqual "typeExprContext1" ""
    (show (ppTypeExprContextPrefix typeExprContext1))),
  TestCase (assertEqual "typeExprContext2" "Eq a =>"
    (show (ppTypeExprContextPrefix typeExprContext2))),
  TestCase (assertEqual "typeExprContext3" "(Eq a, Ord b) =>"
    (show (ppTypeExprContextPrefix typeExprContext3)))
  ])

-- ppTypeExprContext

testPpTypeExprContext = TestLabel "ppTypeExprContext" (TestList [
  TestCase (assertEqual "typeExprContext1" ""
    (show (ppTypeExprContext typeExprContext1))),
  TestCase (assertEqual "typeExprContext2" "Eq a"
    (show (ppTypeExprContext typeExprContext2))),
  TestCase (assertEqual "typeExprContext3" "(Eq a, Ord b)"
    (show (ppTypeExprContext typeExprContext3)))
  ])

-- ppLiteral: TODO write tests

-- ppLhs, ppConstrTerm: TODO write tests

-- ppCondExpr, ppExpr, ppStmt, ppAlt, ppOp: TODO write tests

-- ppGoal: TODO write tests

-- ppIdent, ppQIdent, ppInfixOp, ppQInfixOp, ppMIdent: TODO write tests

-- maybePP, parenExp, backQuoteExp, list, parenList, bracketList,
-- braceList: TODO write tests

-- collects all tests

tests = TestList [
  testPpDecl,
  testPpIDecl,
  testPpTypeExpr,
  testPpTypeExprB,
  testPpTypeExprContextPrefix,
  testPpTypeExprContext
  ]

main = runTestTT tests
