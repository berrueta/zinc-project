{-
  $Id: TypeInstGenTest.hs 1523 2005-02-09 18:40:22Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeInstGenTest where

import HUnit
import TypeInstGen
import TypeInferenceMonad
import Types
import ValueEnv
import PredefTypes
import Set

-- vars

valueEnv :: ValueEnv
valueEnv = initDCEnv


-- freshTypeVar

testFreshTypeVar = TestLabel "freshTypeVar" (TestList [
  TestCase (assertEqual "test1" (TypeVariable $ -1) (run freshTypeVar valueEnv)),
  TestCase (assertEqual "test2" (TypeVariable $ -3) (run (freshTypeVar >>
                                                          freshTypeVar >>
                                                          freshTypeVar) valueEnv))
  ])

-- freshConstrainedWithContext

testFreshConstrainedWithContext =
  TestLabel "freshFreshConstrainedWithContext" (TestList [
    TestCase (assertEqual "test1" (TypeWithContext emptyTypeContext
                                    (TypeConstrained [] (-1)))
                                  (run (freshConstrainedWithContext []) valueEnv)),
    TestCase (assertEqual "test2" (TypeWithContext emptyTypeContext 
                                    (TypeConstrained [] (-3)))
                                  (run (freshConstrainedWithContext [] >>
                                        freshTypeVar   >>
                                        freshConstrainedWithContext []) valueEnv))
    ])

-- inst

typeWithContext1 :: TypeWithContext
typeWithContext1 = TypeWithContext emptyTypeContext $ TypeVariable (-1)

ts1,ts2 :: TypeScheme
ts1 = ForAll 0 typeWithContext1
ts2 = ForAll 1 typeWithContext1

testInst = TestLabel "inst" (TestList [
  TestCase (assertEqual "ts1" typeWithContext1 (run (inst ts1) valueEnv)),
  TestCase (assertEqual "ts2" typeWithContext1 (run (inst ts2) valueEnv))
  ])

-- instExist

ets1,ets2 :: ExistTypeScheme
ets1 = ForAllExist 0 0 (removeTypeContext typeWithContext1)
ets2 = ForAllExist 1 0 (removeTypeContext typeWithContext1)
ets3 = ForAllExist 0 1 (removeTypeContext typeWithContext1)

testInstExist = TestLabel "instExist" (TestList [
  TestCase (assertEqual "ets1" typeWithContext1 (run (instExist ets1) valueEnv)),
  TestCase (assertEqual "ets2" typeWithContext1 (run (instExist ets2) valueEnv)),
  TestCase (assertEqual "ets3" typeWithContext1 (run (instExist ets3) valueEnv))
  ])

-- skol

testSkol = TestLabel "skol" (TestList [
  TestCase (assertEqual "ets1" typeWithContext1 (run (skol ets1) valueEnv)),
  TestCase (assertEqual "ets2" typeWithContext1 (run (skol ets2) valueEnv)),
  TestCase (assertEqual "ets3" typeWithContext1 (run (skol ets3) valueEnv))
  ])

-- gen

set1,set2 :: Set Int
set1 = fromListSet []
set2 = fromListSet [-1]

ts1',ts2' :: TypeScheme
ts1' = ForAll 1 (TypeWithContext emptyTypeContext (TypeVariable    0))
ts2' = ForAll 0 (TypeWithContext emptyTypeContext (TypeVariable $ -1))

testGen = TestLabel "gen" (TestList [
  TestCase (assertEqual "set1" ts1' (gen set1 typeWithContext1)),
  TestCase (assertEqual "set2" ts2' (gen set2 typeWithContext1))
  ])


-- Collects all tests

tests = TestList [
  testFreshTypeVar,
  testFreshConstrainedWithContext,
  testInst,
  testInstExist,
  testSkol,
  testGen
  ]

main = runTestTT tests
