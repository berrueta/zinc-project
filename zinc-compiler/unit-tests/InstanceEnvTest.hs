{-
  $Id: InstanceEnvTest.hs 827 2004-06-21 16:14:35Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module InstanceEnvTest where
import HUnit
import InstanceEnv
import Ident
import DualTopEnv

mid :: ModuleIdent
mid = mkMIdent ["mid"]

typeClassId1,typeClassId2,typeClassId3 :: QualIdent
typeClassId1 = qualifyWith preludeMIdent (mkIdent "Eq")
typeClassId2 = qualifyWith preludeMIdent (mkIdent "Ord")
typeClassId3 = qualifyWith preludeMIdent (mkIdent "Functor")

tcId1,tcId2,tcId3 :: QualIdent
tcId1 = qualifyWith mid (mkIdent "Tree")
tcId2 = qualifyWith preludeMIdent intId
tcId3 = qualifyWith preludeMIdent charId

instanceEnv :: InstanceEnv
instanceEnv = bindInstance typeClassId1 tcId1 mid [[]] [] []
            $ bindInstance typeClassId2 tcId1 mid [] [] []
            $ bindInstance typeClassId1 tcId2 mid [] [] []
            $ emptyDualTopEnv

-- bindInstance: indirectly tested

-- lookupInstance

testLookupInstance = TestLabel "lookupInstance" (TestList [
  TestCase (assertEqual "1" False
              (null $ lookupInstance typeClassId1 tcId1 instanceEnv)),
  TestCase (assertEqual "2" False
              (null $ lookupInstance typeClassId1 tcId2 instanceEnv)),
  TestCase (assertEqual "3" False
              (null $ lookupInstance typeClassId2 tcId1 instanceEnv)),
  TestCase (assertEqual "4" True
              (null $ lookupInstance typeClassId2 tcId2 instanceEnv)),
  TestCase (assertEqual "5" True
              (null $ lookupInstance typeClassId3 tcId1 instanceEnv))
  ])

-- hasInstance

testHasInstance = TestLabel "hasInstance" (TestList [
  TestCase (assertEqual "1" True
              (hasInstance typeClassId1 tcId1 instanceEnv)),
  TestCase (assertEqual "2" True
              (hasInstance typeClassId1 tcId2 instanceEnv)),
  TestCase (assertEqual "3" True
              (hasInstance typeClassId2 tcId1 instanceEnv)),
  TestCase (assertEqual "4" False
              (hasInstance typeClassId2 tcId2 instanceEnv)),
  TestCase (assertEqual "5" False
              (hasInstance typeClassId3 tcId1 instanceEnv))
  ])

-- collects all tests

tests = TestList [
  testLookupInstance,
  testHasInstance
  ]

main = runTestTT tests