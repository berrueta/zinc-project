{-
  $Id: KindEnvTest.hs 1337 2004-09-06 18:44:31Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module KindEnvTest where
import HUnit
import KindEnv
import Kind
import Ident
import TopEnv(emptyTopEnv)

mid :: ModuleIdent
mid = mkMIdent ["mid"]

id1,id2,id3 :: Ident
id1 = mkIdent "id1"
id2 = mkIdent "id2"
id3 = mkIdent "id3"

qid1,qid2,qid2',qid3 :: QualIdent
qid1 = qualifyWith mid id1
qid2 = qualifyWith mid id2
qid2' = qualifyWith (mkMIdent ["mid2"]) id2
qid3 = qualifyWith mid id3

kind1,kind2 :: Kind
kind1 = KFun Star Star
kind2 = Star

kindEnv :: KindEnv
kindEnv = bindKind mid id1 kind1 
        $ bindKind mid id2 kind2
        $ emptyTopEnv

-- bindKind : indirectly tested

-- lookupKind

testLookupKind = TestLabel "lookupKind" (TestList [
  TestCase (assertEqual "id1" [KindInfo qid1 kind1]
              (lookupKind id1 kindEnv)),
  TestCase (assertEqual "id2" [KindInfo qid2 kind2]
              (lookupKind id2 kindEnv)),
  TestCase (assertEqual "id3" []
              (lookupKind id3 kindEnv)),
  TestCase (assertEqual "2-tuple" [KindInfo (qTupleId 2)
                                       (KFun Star (KFun Star Star))]
              (lookupKind (tupleId 2) kindEnv)),
  TestCase (assertEqual "3-tuple" [KindInfo (qTupleId 3)
                                       (KFun Star (KFun Star (KFun Star Star)))]
              (lookupKind (tupleId 3) kindEnv))
  ])

-- qualLookupKind

testQualLookupKind = TestLabel "qualLookupKind" (TestList [
  TestCase (assertEqual "qid1" [KindInfo qid1 kind1]
              (qualLookupKind qid1 kindEnv)),
  TestCase (assertEqual "qid2" [KindInfo qid2 kind2]
              (qualLookupKind qid2 kindEnv)),
  TestCase (assertEqual "qid2'" []
              (qualLookupKind qid2' kindEnv)),
  TestCase (assertEqual "qid3" []
              (qualLookupKind qid3 kindEnv)),
  TestCase (assertEqual "2-tuple" [KindInfo (qTupleId 2)
                                       (KFun Star (KFun Star Star))]
              (qualLookupKind (qTupleId 2) kindEnv)),
  TestCase (assertEqual "3-tuple" [KindInfo (qTupleId 3)
                                       (KFun Star (KFun Star (KFun Star Star)))]
              (qualLookupKind (qTupleId 3) kindEnv))
  ])

-- collects all tests

tests = TestList [
  testLookupKind,
  testQualLookupKind
  ]

main = runTestTT tests