{-
  $Id: TypeExprCheckTest.hs 1523 2005-02-09 18:40:22Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeExprCheckTest where
import HUnit
import TypeExprCheck
import Ident
import TypeConstructorEnv
import TypeClassEnv
import PredefTypes
import CurrySyntax
import Position
import TypeExpr
import qualified CurryParserTest
import qualified TypeExprDisambiguateTest


mid :: ModuleIdent
mid = CurryParserTest.mid

tcEnv :: TCEnv
tcEnv = TypeExprDisambiguateTest.tcEnv

typeClassEnv :: TypeClassEnv
typeClassEnv = bindTypeClass mid (mkIdent "Pickable") (mkIdent "a") [] [] $
               initTypeClassEnv

-- declarations used in other test modules

someDecls :: [Decl]
someDecls = checkTypeExprDecls mid tcEnv typeClassEnv
  TypeExprDisambiguateTest.someDecls

-- checkTypeExprDecls: TODO: write test

tree1orig,tree2orig,tree3orig,tree4orig,tree5orig,tree6orig,tree7orig :: [Decl]
tree1orig = TypeExprDisambiguateTest.tree1expected 1
tree2orig = TypeExprDisambiguateTest.tree2expected 1
tree3orig = TypeExprDisambiguateTest.tree3expected 1
tree4orig = TypeExprDisambiguateTest.tree4expected 1
tree5orig = TypeExprDisambiguateTest.tree5expected 1
tree6orig = TypeExprDisambiguateTest.tree6expected 1
tree7orig = TypeExprDisambiguateTest.tree7expected 1

tree1expected,tree2expected,tree3expected,tree4expected :: Int -> [Decl]
tree5expected,tree6expected,tree7expected :: Int -> [Decl]
tree1expected l =
  [DataDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (mkIdent "Tree")
    [(mkIdent "a")]
    [ConstrDecl
      (Position {file=CurryParserTest.filePath,line=l,column=15})
      []
      (mkIdent "Leaf")
      [TypeExprVariable (mkIdent "a")],
     ConstrDecl
      (Position {file=CurryParserTest.filePath,line=l,column=24})
      []
      (mkIdent "Branch")
      [TypeExprApplication
        (TypeExprConstructor (qualifyWith mid $ mkIdent "Tree"))
        (TypeExprVariable (mkIdent "a")),
       TypeExprApplication
        (TypeExprConstructor (qualifyWith mid $ mkIdent "Tree"))
        (TypeExprVariable (mkIdent "a"))
      ]
    ]
    []
  ]
tree2expected l =
  [NewtypeDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (mkIdent "IntTree")
    []
    (NewConstrDecl
      (Position {file=CurryParserTest.filePath,line=l,column=19})
      []
      (mkIdent "IntTree")
      (TypeExprApplication
        (TypeExprConstructor $ qualifyWith mid $ mkIdent "Tree")
        (TypeExprConstructor $ qualifyWith preludeMIdent $ mkIdent "Int")
      )
    )
    []
  ]
tree3expected l =
  [TypeDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (mkIdent "IntTuple")
    []
    (TypeExprApplication
      (TypeExprApplication
        (TypeExprConstructor $ qTupleId 2)
        (TypeExprConstructor $ qualifyWith preludeMIdent $ mkIdent "Int")
      )
      (TypeExprConstructor $ qualifyWith preludeMIdent $ mkIdent "Int")
    )
  ]
tree4expected l =
  [DataDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (mkIdent "Comb")
    [mkIdent "a",mkIdent "b"]
    [ConstrDecl
      (Position {file=CurryParserTest.filePath,line=l,column=17})
      []
      (mkIdent "C")
      [TypeExprApplication
        (TypeExprVariable $ mkIdent "a")
        (TypeExprVariable $ mkIdent "b")]
    ]
    []
  ]
tree5expected l =
  [FunctionDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (mkIdent "f")
    [Equation
      (Position {file=CurryParserTest.filePath,line=l,column=1})
      (FunLhs (mkIdent "f") [VariablePattern $ mkIdent "x"])
      (SimpleRhs
        (Position {file=CurryParserTest.filePath,line=l,column=7})
        (Apply
          (Variable $ qualify $ mkIdent "g")
          (Variable $ qualify $ mkIdent "x"))
        [TypeSig
          (Position {file=CurryParserTest.filePath,line=l,column=17})
          [mkIdent "g"]
          (TypeExprWithContext emptyTypeExprContext
            (typeExprApplyArrowConstructor
              (TypeExprVariable $ mkIdent "a")
              (TypeExprApplication
                (TypeExprConstructor $ qualifyWith mid $ mkIdent "Tree")
                (TypeExprVariable $ mkIdent "a"))))])]]
tree6expected l = tree6orig
tree7expected l =
  [TypeClassDecl
    (Position {file=CurryParserTest.filePath,line=l,column=1})
    (TypeExprContext
      [TypeExprClassConstraint (qualifyWith mid $ mkIdent "Eq") (TypeExprVariable $ mkIdent "a")])
    (mkIdent "Ord")
    (mkIdent "a")
    []
   ]

testCheckTypeExprDecls = TestLabel "checkTypeExprDecls" (TestList [
  TestCase (assertEqual "code1"
    (tree1expected 1)
    (checkTypeExprDecls mid tcEnv typeClassEnv $ tree1orig)),
  TestCase (assertEqual "code3"
    (tree3expected 1)
    (checkTypeExprDecls mid tcEnv typeClassEnv $ tree3orig)),
  TestCase (assertEqual "code4"
    (tree4expected 1)
    (checkTypeExprDecls mid tcEnv typeClassEnv $ tree4orig)),
  TestCase (assertEqual "code1+code2+code3+code4+code5+code6+code7"
    (tree1expected 1 ++ tree2expected 1 ++ tree3expected 1 ++
     tree4expected 1 ++ tree5expected 1 ++ tree6expected 1 ++
     tree7expected 1)
    (checkTypeExprDecls mid tcEnv typeClassEnv $
       tree1orig ++ tree2orig ++ tree3orig ++ tree4orig ++ tree5orig ++
       tree6orig ++ tree7orig))
  ])

-- collects all tests

tests = TestList [
  testCheckTypeExprDecls
  ]

main = runTestTT tests
