{-
  $Id: TypeExprSubstTest.hs 1003 2004-08-14 10:45:18Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeExprSubstTest where
import HUnit
import TypeExprSubst
import Ident
import TypeExpr
import Subst

id1,id2 :: Ident
id1 = mkIdent "a"
id2 = mkIdent "b"

-- bindTypeExprVar/substTypeExprVar

testBindTypeExprVar = TestLabel "bindTypeExprVar" (TestList [
  TestCase (assertEqual "a" typeExprUnitConstructor
    (substTypeExprVar (bindTypeExprVar id1 typeExprUnitConstructor idSubst) id1)),
  TestCase (assertEqual "b" (TypeExprVariable id2)
    (substTypeExprVar (bindTypeExprVar id1 typeExprUnitConstructor idSubst) id2))
  ])

-- TODO: test other instantiations

subst1 :: TypeExprSubst
subst1 = bindTypeExprVar id1 (TypeExprVariable $ mkIdent "_1") idSubst

typeExprContext1 :: TypeExprContext
typeExprContext1 = TypeExprContext
  [TypeExprClassConstraint (qualify $ mkIdent "C1") (TypeExprVariable $ mkIdent "b"),
   TypeExprClassConstraint (qualify $ mkIdent "C2") (TypeExprVariable $ mkIdent "a")]

expectedTypeExprContext1 :: TypeExprContext
expectedTypeExprContext1 = TypeExprContext
  [TypeExprClassConstraint (qualify $ mkIdent "C1") (TypeExprVariable $ mkIdent "b"),
   TypeExprClassConstraint (qualify $ mkIdent "C2") (TypeExprVariable $ mkIdent "_1")]

testSubstTypeExpr_TypeExprContext =
  TestLabel "testSubstTypeExpr_TypeExprContext" (TestList [
  TestCase (assertEqual "typeExprContext1" expectedTypeExprContext1
    (substTypeExpr subst1 typeExprContext1))
  ])

-- collects all tests

tests = TestList [
  testBindTypeExprVar,
  testSubstTypeExpr_TypeExprContext
  ]

main = runTestTT tests