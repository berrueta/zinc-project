{-
  $Id: TypeUnificationTest.hs 270 2004-04-09 21:41:19Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeUnificationTest where

import HUnit
import TypeUnification
import Position
import Ident
import Pretty
import TypeInferenceMonad
import ValueEnv
import PredefTypes
import Types
import TypeSubst
import Combined
import Subst

-- some variables

filePath :: String
filePath = "testfile"

position :: Position
position = Position { file=filePath, column=1, line=1 }

what :: String
what = "Unification test"

mid :: ModuleIdent
mid = mkMIdent ["midtest"]

doc :: Doc
doc = text "doctest"

valueEnv :: ValueEnv
valueEnv = initDCEnv

-- types

ty1,ty2,ty3,ty4,ty5,ty6,ty7,ty8,ty9,ty10 :: Type
ty1 = TypeVariable 1
ty2 = TypeVariable 2
ty3 = TypeConstructor $ qualify $ mkIdent "tyCon3"
ty4 = TypeConstructor $ qualify $ mkIdent "tyCon4"
ty5 = typeApply (TypeConstructor $ qualify $ mkIdent "tyCon5") [ty1]
ty6 = typeApply (TypeConstructor $ qualify $ mkIdent "tyCon6") [ty1,ty2]
ty7 = typeApply (TypeConstructor $ qualify $ mkIdent "tyCon6") [ty1,ty1] -- be careful!
ty8 = typeArrow ty2 ty2
ty9 = typeArrow ty1 ty2
ty10 = typeArrow ty3 ty4

-- unify

testUnify = TestLabel "unify" (TestList [
  TestCase (assertEqual "ty1-ty1" [] (doUnify ty1 ty1)),
  TestCase (assertEqual "ty1-ty2" [(1,ty2)] (doUnify ty1 ty2)),
  TestCase (assertEqual "ty2-ty1" [(2,ty1)] (doUnify ty2 ty1)),
  TestCase (assertEqual "ty1-ty3" [(1,ty3)] (doUnify ty1 ty3)),
--TestCase (assertEqual "ty1-ty5" [(1,ty5)] (doUnify ty1 ty5)),
  TestCase (assertEqual "ty2-ty5" [(2,ty5)] (doUnify ty2 ty5)),
  TestCase (assertEqual "ty5-ty2" [(2,ty5)] (doUnify ty5 ty2)),
  TestCase (assertEqual "ty3-ty3" [] (doUnify ty3 ty3)),
  TestCase (assertEqual "ty5-ty5" [] (doUnify ty5 ty5)),
  TestCase (assertEqual "ty6-ty6" [] (doUnify ty6 ty6)),
  TestCase (assertEqual "ty7-ty7" [] (doUnify ty7 ty7)),
  TestCase (assertEqual "ty7-ty7" [] (doUnify ty7 ty7)),
--TestCase (assertEqual "ty1-ty7" [(1,ty7)] (doUnify ty1 ty7)),
  TestCase (assertEqual "ty6-ty7" [(2,ty1)] (doUnify ty6 ty7)),
  TestCase (assertEqual "ty7-ty6" [(1,ty2)] (doUnify ty7 ty6)),
  TestCase (assertEqual "ty1-ty8" [(1,ty8)] (doUnify ty1 ty8)),
  TestCase (assertEqual "ty8-ty8" [] (doUnify ty8 ty8)),
  TestCase (assertEqual "ty9-ty9" [] (doUnify ty9 ty9)),
  TestCase (assertEqual "ty10-ty10" [] (doUnify ty10 ty10)),
  TestCase (assertEqual "ty2-ty10" [(2,ty10)] (doUnify ty2 ty10)),
  TestCase (assertEqual "ty9-ty10" [(1,ty3),(2,ty4)] (doUnify ty9 ty10))

  ])

-- Collects alls tests

tests = TestList [
  testUnify
  ]

main = runTestTT tests

fetchSubst :: Type -> Type -> TcState TypeSubst
fetchSubst ty1 ty2 = do unify position what doc mid ty1 ty2
                        liftSt fetchSt

doUnify :: Type -> Type -> [(Int,Type)]
doUnify ty1 ty2 = substToList $ run (fetchSubst ty1 ty2) valueEnv