{-
  $Id: AllTests.hs 1413 2004-09-11 17:50:21Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module Main where

import HUnit
import qualified IdentTest
import qualified TypesTest
import qualified SubstTest
import qualified CurryParserTest
import qualified SCCTest
import qualified CurryPPTest
import qualified TypeUnificationTest
import qualified TypeInstGenTest
import qualified TypeExpansionTest
import qualified TypeTransTest
import qualified TypeBindingTest
import qualified TypeConstructorEnvTest
import qualified PredefTypesTest
import qualified TypeExprSubstTest
import qualified KindEnvTest
import qualified KindInferenceTest
import qualified TypeClassEnvTest
import qualified KindTest
import qualified KindUnificationTest
import qualified TypeExprDisambiguateTest
import qualified CurrySyntaxUtilsTest
import qualified TypeExprRenamingTest
import qualified TypeCheckTest
import qualified TypingTest
import qualified TypeClassBindingTest
import qualified IntfExpansionTest
import qualified TypeExprCheckTest
import qualified InstanceEnvTest
import qualified DictTransDeclsTest
import qualified ContextReductionTest
import qualified DictUtilsTest
import qualified DictGenerationTest

-- Collects all tests

tests = TestList [
  TestLabel "Ident"             IdentTest.tests,
  TestLabel "Types"             TypesTest.tests,
  TestLabel "Subst"             SubstTest.tests,
  TestLabel "CurryParser"       CurryParserTest.tests,
  TestLabel "SCC"               SCCTest.tests,
  TestLabel "CurryPP"           CurryPPTest.tests,
  TestLabel "TypeUnification"   TypeUnificationTest.tests,
  TestLabel "TypeInstGen"       TypeInstGenTest.tests,
  TestLabel "TypeExpansion"     TypeExpansionTest.tests,
  TestLabel "TypeTrans"         TypeTransTest.tests,
  TestLabel "TypeBinding"       TypeBindingTest.tests,
  TestLabel "TypeConstructor"   TypeConstructorEnvTest.tests,
  TestLabel "PredefTypes"       PredefTypesTest.tests,
  TestLabel "TypeExprSubst"     TypeExprSubstTest.tests,
  TestLabel "KindEnv"           KindEnvTest.tests,
  TestLabel "KindInference"     KindInferenceTest.tests,
  TestLabel "TypeClassEnv"      TypeClassEnvTest.tests,
  TestLabel "Kind"              KindTest.tests,
  TestLabel "KindUnification"   KindUnificationTest.tests,
  TestLabel "TypeExprDisambiguate" TypeExprDisambiguateTest.tests,
  TestLabel "CurrySyntaxUtils"  CurrySyntaxUtilsTest.tests,
  TestLabel "TypeExprRenaming"  TypeExprRenamingTest.tests,
  TestLabel "TypeCheck"         TypeCheckTest.tests,
  TestLabel "Typing"            TypingTest.tests,
  TestLabel "TypeClassBinding"  TypeClassBindingTest.tests,
  TestLabel "IntfExpansion"     IntfExpansionTest.tests,
  TestLabel "TypeExprCheck"     TypeExprCheckTest.tests,
  TestLabel "InstanceEnv"       InstanceEnvTest.tests,
  TestLabel "DictTransDecls"    DictTransDeclsTest.tests,
  TestLabel "ContextReduction"  ContextReductionTest.tests,
  TestLabel "DictUtils"         DictUtilsTest.tests,
  TestLabel "DictGeneration"    DictGenerationTest.tests
  ]

main = runTestTT tests