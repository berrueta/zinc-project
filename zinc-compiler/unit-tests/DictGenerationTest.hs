{-
  $Id: DictGenerationTest.hs 1289 2004-09-03 13:56:57Z berrueta $

  Copyright (c) 2003, Diego Berrueta
  See LICENSE for the full license.
-}

module DictGenerationTest where

import HUnit
import DictUtils
import DictGeneration
import CurrySyntax
import CurryPP
import TypeClassEnv
import qualified DictUtilsTest

-- genTraverserFunctions

testGenTraverserFunctions = TestLabel "genTraverserFunctions" (TestList [
  TestCase (assertEqual "A"
              ""
              (f DictUtilsTest.tcqid1)),
  TestCase (assertEqual "B"
              ""
              (f DictUtilsTest.tcqid2)),
  TestCase (assertEqual "C"
              ""
              (f DictUtilsTest.tcqid3)),
  TestCase (assertEqual "D"
              ""
              (f DictUtilsTest.tcqid4)),
  TestCase (assertEqual "E"
              ""
              (f DictUtilsTest.tcqid5)),
  TestCase (assertEqual "F"
              ("_dictTrav_mid_A_from_mid_F f = trav (f)\n" ++
               "  where trav (mid._DictData_F x) = x")
              (f DictUtilsTest.tcqid6)),
  TestCase (assertEqual "G"
              ("_dictTrav_mid_C_from_mid_G f = trav (f)\n" ++
               "  where trav (mid._DictData_G x) = x")
              (f DictUtilsTest.tcqid7)),
  TestCase (assertEqual "H"
              ("_dictTrav_mid_C_from_mid_H f = trav (f)\n" ++
	       "  where trav (mid._DictData_H x _) = x")
              (f DictUtilsTest.tcqid8)),
  TestCase (assertEqual "I"
              ("_dictTrav_mid_C_from_mid_I f = trav (f)\n" ++
	       "  where trav (mid._DictData_I x _) = x\n" ++
	       "_dictTrav_mid_D_from_mid_I f = trav (f)\n" ++
	       "  where trav (mid._DictData_I _ x) = x")
              (f DictUtilsTest.tcqid9)),
  TestCase (assertEqual "J"
              ""
              (f DictUtilsTest.tcqid10)),
  TestCase (assertEqual "K"
              ("_dictTrav_mid_B_from_mid_K f = trav (f undefined)\n" ++
	       "  where trav (mid._DictData_K x _) = x")
              (f DictUtilsTest.tcqid11)),
  TestCase (assertEqual "L"
              ("_dictTrav_mid_E_from_mid_L f = trav (f undefined)\n" ++
	       "  where trav (mid._DictData_L x _) = x")
              (f DictUtilsTest.tcqid12)),
  TestCase (assertEqual "M"
              ("_dictTrav_mid_J_from_mid_M f = \\mid_J_b_10 -> trav (f mid_J_b_10)\n" ++
	       "  where trav (mid._DictData_M x _) = x")
              (f DictUtilsTest.tcqid13)),
  TestCase (assertEqual "N"
              ("_dictTrav_mid_M_from_mid_N f =\n" ++
	       "  \\mid_M_mid_J_b_10 -> trav (f mid_M_mid_J_b_10 undefined)\n" ++
	       "  where trav (mid._DictData_N x _) = x")
              (f DictUtilsTest.tcqid14))
  ])
  where f tc = case qualLookupTypeClass tc DictUtilsTest.typeClassEnv of
                 [TypeClassInfo _ _ superTypeClasses _] ->
                   show $ ppBlock $ (\xs -> drop ((length xs) `div` 2) xs)
                     (genTraverserFunctions DictUtilsTest.typeClassEnv
                                            tc superTypeClasses)


-- collects all tests

tests = TestList [
  testGenTraverserFunctions
  ]

main = runTestTT tests
