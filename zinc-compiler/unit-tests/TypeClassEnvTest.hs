{-
  $Id: TypeClassEnvTest.hs 827 2004-06-21 16:14:35Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module TypeClassEnvTest where
import HUnit
import TypeClassEnv
import TypeExpr
import Ident
import Env(bindEnv,emptyEnv)
import TopEnv(emptyTopEnv)

mid :: ModuleIdent
mid = mkMIdent ["mid"]

id1,id2,id3 :: Ident
id1 = mkIdent "id1"
id2 = mkIdent "id2"
id3 = mkIdent "id3"

tvId1 :: Ident
tvId1 = mkIdent "a"

qid1,qid2,qid2',qid3 :: QualIdent
qid1 = qualifyWith mid id1
qid2 = qualifyWith mid id2
qid2' = qualifyWith (mkMIdent ["mid2"]) id2
qid3 = qualifyWith mid id3

typeClassSuperclasses1,typeClassSuperclasses2 :: [QualIdent]
typeClassSuperclasses1 = []
typeClassSuperclasses2 = [qid1]

typeClassEnv :: TypeClassEnv
typeClassEnv = bindTypeClass mid id1 tvId1 typeClassSuperclasses1 []
             $ bindTypeClass mid id2 tvId1 typeClassSuperclasses2
                [(mkIdent "===",TypeExprWithContext emptyTypeExprContext (TypeExprVariable tvId1))]
             $ emptyTopEnv

-- bindTypeClass : indirectly tested

-- lookupKind / qualLook

testLookupTypeClass = TestLabel "lookupTypeClass" (TestList [
  TestCase (assertEqual "id1" [TypeClassInfo qid1 tvId1 [] emptyEnv]
              (lookupTypeClass id1 typeClassEnv)),
  TestCase (assertEqual "id2" [TypeClassInfo qid2 tvId1 [qid1] (bindEnv (mkIdent "===") (TypeExprWithContext emptyTypeExprContext (TypeExprVariable tvId1)) emptyEnv)]
              (lookupTypeClass id2 typeClassEnv)),
  TestCase (assertEqual "id3" []
              (lookupTypeClass id3 typeClassEnv))
  ])

-- qualLookupTypeClass

testQualLookupTypeClass = TestLabel "qualLookupTypeClass" (TestList [
  TestCase (assertEqual "qid1" [TypeClassInfo qid1 tvId1 [] emptyEnv]
              (qualLookupTypeClass qid1 typeClassEnv)),
  TestCase (assertEqual "qid2" [TypeClassInfo qid2 tvId1 [qid1] (bindEnv (mkIdent "===") (TypeExprWithContext emptyTypeExprContext (TypeExprVariable tvId1)) emptyEnv)]
              (qualLookupTypeClass qid2 typeClassEnv)),
  TestCase (assertEqual "qid2'" []
              (qualLookupTypeClass qid2' typeClassEnv))
  ])

-- collects all tests

tests = TestList [
  testLookupTypeClass,
  testQualLookupTypeClass
  ]

main = runTestTT tests