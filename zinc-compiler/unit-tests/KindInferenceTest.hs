{-
  $Id: KindInferenceTest.hs 1413 2004-09-11 17:50:21Z berrueta $

  Copyright (c) 2004, Diego Berrueta
  See LICENSE for the full license.
-}

module KindInferenceTest where
import HUnit
import Kind
import KindInference
import KindUnification
import Ident
import CurrySyntax
import TypeExpr
import PredefTypes
import Position
import Error
import LLParseComb
import CurryLexer
import CurryParser
import KindEnv
import Combined
import Monad
import TypeExprDisambiguate
import TypeExprRenaming
import Subst
import TypeExprSubst
import TypeExprCheck
import TypeConstructorEnv
import TypeClassEnv
import KindInferenceMonad
import qualified TypeExprCheckTest
import qualified CurryParserTest

mid :: ModuleIdent
mid = CurryParserTest.mid

demoPosition :: Position
demoPosition = Position {file=CurryParserTest.filePath,line=1,column=1}

someDecls :: [Decl]
someDecls = TypeExprCheckTest.someDecls

-- groupDecls

tcEnv :: TCEnv
tcEnv = bindLocalTypeInfo DataType preludeMIdent intId [] [] $
        initTCEnv

typeClassEnv :: TypeClassEnv
typeClassEnv = initTypeClassEnv

kindEnv :: KindEnv
kindEnv = bindKind preludeMIdent intId Star 
          initKindEnv

groupedDecls :: [[Decl]]
groupedDecls = [[someDecls !!  0,
                 someDecls !!  1],
                [someDecls !!  2],
                [someDecls !!  3],
                [someDecls !!  4],
                [someDecls !!  5],
                [someDecls !!  6],
                [someDecls !!  7],
                [someDecls !!  8],
                [someDecls !!  9],
                [someDecls !! 10],
                [someDecls !! 11]
               ]

testGroupDecls = TestLabel "groupDecls" (TestList [
  TestCase (assertEqual "decls" groupedDecls
                        (groupDecls mid someDecls))
  ])

-- kindInference

code1,code2,code3,code4,code4',code5,code6,code7,code8,code9,code10 :: String
code1 =
  "class Eq a where\n" ++
  "  (==) :: a -> a -> ()"
code2 = 
  "class Functor f where\n" ++
  "  map :: (a -> b) -> (f a -> f b)"
code3 =
  "data Tree a = Leaf | Branch (Tree a) (Tree a)"
code4 =
  "type Subst a b = [(a,b)]"
code4' =
  "type SubstBis a = Subst a a"
code5 =
  "type NullTree = Tree ()"
code6 =
  "data Color = Red | Green | Blue"
code7 =
  "data OT a b = O (b a)"
code8 =
  "data OT2 a b = O2 (a b)"
code9 =
  "instance Eq a => Eq [a] where (==) = undefined"
code10 =
  "instance Functor Tree where map = undefined"

allCode :: String
allCode = unlines [code1,code2,code3,code4,code4',code5,code6,code7,code8,
                   code9,code10]

testKindInference = TestLabel "kindInference" (TestList [
  TestCase (assertEqual "Eq"
     [KindInfo (qualifyWith mid $ mkIdent "Eq") Star]
     (lookup "Eq")),
  TestCase (assertEqual "Functor"
     [KindInfo (qualifyWith mid $ mkIdent "Functor") (KFun Star Star)]
     (lookup "Functor")),
  TestCase (assertEqual "Tree"
     [KindInfo (qualifyWith mid $ mkIdent "Tree") (KFun Star Star)]
     (lookup "Tree")),
  TestCase (assertEqual "Subst"
     [KindInfo (qualifyWith mid $ mkIdent "Subst") (KFun Star $ KFun Star Star)]
     (lookup "Subst")),
  TestCase (assertEqual "SubstBis"
     [KindInfo (qualifyWith mid $ mkIdent "SubstBis") (KFun Star Star)]
     (lookup "SubstBis")),
  TestCase (assertEqual "NullTree"
     [KindInfo (qualifyWith mid $ mkIdent "NullTree") (Star)]
     (lookup "NullTree")),
  TestCase (assertEqual "Color"
     [KindInfo (qualifyWith mid $ mkIdent "Color") (Star)]
     (lookup "Color")),
  TestCase (assertEqual "OT"
     [KindInfo (qualifyWith mid $ mkIdent "OT") (KFun Star (KFun (KFun Star Star) Star))]
     (lookup "OT")),
  TestCase (assertEqual "OT2"
     [KindInfo (qualifyWith mid $ mkIdent "OT2") (KFun (KFun Star Star) (KFun Star Star))]
     (lookup "OT2"))
  ])
  where env = demoKindInference
            $ typeExprRenaming
            $ demoDisambiguate
            $ demoParse
            $ allCode
        lookup id = lookupKind (mkIdent id) env

-- collects all tests

tests = TestList [
  testGroupDecls,
  testKindInference
  ]

main = runTestTT tests

-- auxiliary functions

demoInitAssumpts :: [Decl] -> KindEnv
demoInitAssumpts ds = runKiState initKindEnv
                        (initAssumpts mid (typeExprRenaming ds) >>
                         fetchSt >>= return)

demoKindInference :: [Decl] -> KindEnv
demoKindInference ds = snd $ kindInference mid ds initKindEnv

demoKindInferenceGoal :: Goal -> Goal
demoKindInferenceGoal g = kindInferenceGoal kindEnv g

demoDisambiguate :: [Decl] -> [Decl]
demoDisambiguate = disambiguateDecls mid initTCEnv

demoParse :: String -> [Decl]
demoParse code = ok $ applyParser decls lexer CurryParserTest.filePath code

demoParseGoal :: String -> Goal
demoParseGoal code = ok $ applyParser goal lexer CurryParserTest.filePath code

demoKindInferenceDecls :: [Decl] -> KindEnv
demoKindInferenceDecls ds = runKiState initKindEnv
                              (initAssumpts mid ds' >>
                               kindInf mid noPos ds' >>
                               fetchSt >>= return)
  where ds' = typeExprRenaming ds

demoKindInferenceGroup ds = snd $ kindInfGroup mid ([],initKindEnv) ds

failCode1 :: String
failCode1 =
  "data OT a b = O (b a)\n" ++   -- ok:   OT   ==> * -> (* -> *) -> *
  "type PT c = OT c ()"          -- bad!: () !==> * -> *

failGoalCode1 :: String
failGoalCode1 =
  "x == 10 where\n" ++
  "  f = 10\n" ++
  "  f :: prelude.Int prelude.Int"

demoPrelude =
  do preludeCode <- readFile "stdlib/prelude.curry"
     putStrLn $ show $ newKindEnv $ preproc $ decls preludeCode
  where decls code = demoParse $ unlines $ drop 1 $ lines $ code
        preproc decls = checkTypeExprDecls mid tcEnv typeClassEnv $
                        disambiguateDecls preludeMIdent tcEnv decls
        group decls = groupDecls preludeMIdent $ filter isTypeDecl decls
        newKindEnv decls = kindInference preludeMIdent (filter isTypeDecl decls) initKindEnv
