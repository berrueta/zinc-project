# Specific rules for building with ghc
# $Id: ghc.mk 1523 2005-02-09 18:40:22Z berrueta $
#
# Copyright (c) 2002-2004, Wolfgang Lux
# See LICENSE for the full license.
#

# specific definitions for ghc
GHC_HCFLAGS = -i$(top_srcdir) -H12m # -Rghc-timing
LINK=$(HC) -o $@
zycc_LDFLAGS=$(HCFLAGS) $(GHC_HCFLAGS)
zymk_LDFLAGS=$(HCFLAGS) $(GHC_HCFLAGS)
newer_LDFLAGS=$(HCFLAGS) $(GHC_HCFLAGS)
mach_LDFLAGS=$(HCFLAGS) $(GHC_HCFLAGS)

%.hi: %.o
	@test -f $@ || \
	(echo "$@ does not exist!"; \
	 echo "Remove $< and run make again."; exit 1)

# Dependencies

%.ld: %.lhs $(BUILT_SOURCES)
	$(HC) $(HCFLAGS) $(GHC_HCFLAGS) -M -optdep-f -optdep$@ $<
	echo "$@ : $<" >> $@
%.d: %.hs $(BUILT_SOURCES)
	$(HC) $(HCFLAGS) $(GHC_HCFLAGS) -M -optdep-f -optdep$@ $<
	echo "$@ : $<" >> $@

