% $Id: DeclGen.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{DeclGen.lhs}
\codesection{Declaration generalization}
This module performs the generalization of the function and patterns
declarations, after the type inference. Generalization is well-described
stage of the Hindley-Milner type system.

At the same time, it performs some checks: it compares the inferred type
with the user-provided signature, and searchs for unresolved overloading.
\begin{lstlisting}

> module DeclGen where
> import Error
> import TypeInferenceMonad
> import Types
> import TypeSubst
> import Pretty
> import SigEnv
> import ValueEnv
> import TypeConstructorEnv
> import CurrySyntax
> import Ident
> import ContextReduction
> import Combined
> import Set
> import TypeClassEnv
> import InstanceEnv
> import Position
> import TypeErrors
> import TypeTrans
> import TypeInstGen
> import CurryPP(ppIdent)
> import TypeExpansion
> import Expr(bv)
> import TypeCheckUtils
> import ValueEnv

\end{lstlisting}
In Curry we cannot generalize the types of let-bound variables because
they can refer to logic variables. Without this monomorphism
restriction unsound code like
\begin{lstlisting}
bug = x =:= 1 & x =:= 'a'
  where x :: a
        x = fresh
fresh :: a
fresh = x where x free
\end{lstlisting}
could be written. Note that \texttt{fresh} has the polymorphic type
$\forall\alpha.\alpha$. This is correct because \texttt{fresh} is a
function and therefore returns a different variable at each
invocation.

The code in \texttt{genVar} below also verifies that the inferred type
for a variable or function matches the type declared in a type
signature. As the declared type is already used for assigning an initial
type to a variable when it is used, the inferred type can only be more
specific. Therefore, if the inferred type does not match the type
signature the declared type must be too general.
\begin{lstlisting}

> genDecl :: Bool -> ModuleIdent ->
>            TypeClassEnv -> InstanceEnv -> TCEnv -> SigEnv ->
>            Set Int -> TypeSubst -> TypeContext ->
>            Decl -> TcState TypeContext
> genDecl topLevel m typeClassEnv instEnv tcEnv sigs
>         lvs theta ctx (FunctionDecl p f eqs) =
>   do tyEnv <- fetchSt
>      let (tyEnv',ctx'') =
>            genVar True topLevel m typeClassEnv instEnv tcEnv
>                   sigs lvs theta ctx' p f tyEnv
>      let tyEnv'' = redoContextReduction eqs m typeClassEnv instEnv
>                                         (subst theta tyEnv')
>      changeSt tyEnv''
>      return ctx''
>   where ctx' = contextReduction p typeClassEnv instEnv ctx
> genDecl topLevel m typeClassEnv instEnv tcEnv sigs
>         lvs theta ctx (PatternDecl p t rhs) =
>   do tyEnv <- fetchSt
>      let (tyEnv',ctx'') =
>            genVars False False m typeClassEnv instEnv tcEnv sigs
>                    lvs theta ctx' p (bv t) tyEnv 
>      let tyEnv'' = redoContextReduction rhs m typeClassEnv instEnv tyEnv'
>      changeSt tyEnv''
>      return ctx''
>   where ctx' = contextReduction p typeClassEnv instEnv ctx

> genVars :: Bool -> Bool -> ModuleIdent -> TypeClassEnv -> InstanceEnv ->
>            TCEnv -> SigEnv -> Set Int -> TypeSubst -> TypeContext ->
>            Position -> [Ident] -> ValueEnv -> (ValueEnv,TypeContext)
> genVars poly topLevel m typeClassEnv instEnv tcEnv sigs lvs theta ctx
>         p vs tyEnv =
>   foldl (\(tyEnv,ctx') v ->
>            let (tyEnv',ctx'') =
>                  genVar poly topLevel m typeClassEnv instEnv tcEnv sigs
>                         lvs theta ctx p v tyEnv   -- note the ctx, not ctx'
>            in  (tyEnv',composeTypeContext ctx' ctx''))
>         (tyEnv,emptyTypeContext) vs

> genVar :: Bool -> Bool -> ModuleIdent -> TypeClassEnv -> InstanceEnv ->
>           TCEnv -> SigEnv -> Set Int -> TypeSubst -> TypeContext ->
>           Position -> Ident -> ValueEnv -> (ValueEnv,TypeContext)
> genVar poly topLevel m typeClassEnv instEnv tcEnv sigs
>       lvs theta globalCtx p v tyEnv
>   | checkSignature m what typeClassEnv instEnv tcEnv sigs p v genSigma &&
>     checkUnresolvedOverloading topLevel what p globalCtx' =
>     (tyEnv',globalCtx')
>   where what = text (if poly then "Function:" else "Variable:") <+> ppIdent v
>         tyEnv'    = rebindFun m v genSigma tyEnv
>         genSigma,infSigma :: TypeScheme
>         genSigma           = genType poly infSigma
>         (infSigma,globalCtx')  = inferredSigmaVar theta globalCtx v tyEnv
>         genType poly (ForAll n tyWC)
>           | n > 0     = internalError "genVar"
>           | poly      = gen lvs tyWC
>           | otherwise = monoTypeWithContext tyWC

> checkSignature :: ModuleIdent -> Doc ->
>                   TypeClassEnv -> InstanceEnv -> TCEnv -> SigEnv ->
>                   Position -> Ident -> TypeScheme -> Bool
> checkSignature m what typeClassEnv instEnv tcEnv sigs p v infSigma =
>   case lookupTypeSig v sigs of
>     Just sigTyExprWC
>       | infSigma == sigSigma (sigTyExprWC) -> True
>       | otherwise -> errorAt p (typeSigTooGeneral m what sigTyExprWC infSigma)
>     Nothing -> True
>   where sigSigma sigTyExprWC =
>           case expandPolyType tcEnv sigTyExprWC of
>             ForAll n (TypeWithContext ctx ty) ->
>               let ctx' = contextReduction p typeClassEnv instEnv ctx
>               in  ForAll n (TypeWithContext ctx' ty)

> checkUnresolvedOverloading :: Bool -> Doc -> Position -> TypeContext ->
>                               Bool
> checkUnresolvedOverloading topLevel what p (TypeContext tccs)
>   | (not topLevel) || length tccs == 0 = True
>   | otherwise = errorAt p (unresolvedOverloading what tccs)

> inferredSigmaVar :: TypeSubst -> TypeContext -> Ident ->
>                     ValueEnv -> (TypeScheme,TypeContext)
> inferredSigmaVar theta globalCtx v tyEnv =
>   (infSigma,globalCtx')
>   where (ForAll n (TypeWithContext _ ty)) = subst theta (varType v tyEnv)
>         tyWC = TypeWithContext globalCtx ty
>         (tyWC',globalCtx') = relevantTypeContext tyWC
>         infSigma = ForAll n (sortTypeContext tyWC')

\end{lstlisting}
After generalizing the type of a variable, the context of all the
local variables should be recomputed. Some type variable may have
been bound in the generalization, so types (and contexts) of the local
variables may have changed.
\begin{lstlisting}

> tyEnvComp :: (ModuleIdent -> TypeClassEnv ->
>               InstanceEnv -> ValueEnv -> ValueEnv) ->
>              (ModuleIdent -> TypeClassEnv ->
>               InstanceEnv -> ValueEnv -> ValueEnv) ->
>              ModuleIdent -> TypeClassEnv ->
>              InstanceEnv -> ValueEnv -> ValueEnv
> tyEnvComp f g m typeClassEnv instEnv =
>   f m typeClassEnv instEnv . g m typeClassEnv instEnv

> redoContextReductionId = (\_ _ _ -> id)

> class RedoContextReduction a where
>   redoContextReduction :: a -> ModuleIdent ->
>                           TypeClassEnv -> InstanceEnv ->
>                           ValueEnv -> ValueEnv

> instance RedoContextReduction a => RedoContextReduction [a] where   
>   redoContextReduction xs m typeClassEnv instEnv tyEnv =
>     foldr (\x -> redoContextReduction x m typeClassEnv instEnv) tyEnv xs

> instance RedoContextReduction Decl where
>   redoContextReduction (FunctionDecl p f eqs) =
>     redoContextReductionVar p f `tyEnvComp` redoContextReduction eqs
>   redoContextReduction (PatternDecl p t rhs) =
>     redoContextReductionVars p (bv t) `tyEnvComp`
>     redoContextReduction rhs
>   redoContextReduction _ = redoContextReductionId

> instance RedoContextReduction Equation where
>   redoContextReduction (Equation _ _ rhs) =
>     redoContextReduction rhs

> instance RedoContextReduction Rhs where
>   redoContextReduction (SimpleRhs _ expr decls) =
>     redoContextReduction expr `tyEnvComp`
>     redoContextReduction decls
>   redoContextReduction (GuardedRhs condExprs decls) =
>     redoContextReduction condExprs `tyEnvComp`
>     redoContextReduction decls

> instance RedoContextReduction CondExpr where
>   redoContextReduction (CondExpr _ expr1 expr2) =
>     redoContextReduction [expr1,expr2]

> instance RedoContextReduction Expression where
>   redoContextReduction (Literal _) = redoContextReductionId
>   redoContextReduction (Variable _) = redoContextReductionId
>   redoContextReduction (Constructor _) = redoContextReductionId
>   redoContextReduction (Paren expr) =
>     redoContextReduction expr
>   redoContextReduction (Typed expr _) =
>     redoContextReduction expr
>   redoContextReduction (Tuple exprs) =
>     redoContextReduction exprs
>   redoContextReduction (List exprs) =
>     redoContextReduction exprs
>   redoContextReduction (ListCompr expr stmts) =
>     redoContextReduction expr `tyEnvComp` redoContextReduction stmts
>   redoContextReduction (EnumFrom expr) =
>     redoContextReduction expr
>   redoContextReduction (EnumFromThen expr1 expr2) =
>     redoContextReduction [expr1,expr2]
>   redoContextReduction (EnumFromTo expr1 expr2) =
>     redoContextReduction [expr1,expr2]
>   redoContextReduction (EnumFromThenTo expr1 expr2 expr3) =
>     redoContextReduction [expr1,expr2,expr3]
>   redoContextReduction (UnaryMinus _ expr) =
>     redoContextReduction expr
>   redoContextReduction (Apply expr1 expr2) =
>     redoContextReduction [expr1,expr2]
>   redoContextReduction (InfixApply expr1 _ expr2) =
>     redoContextReduction [expr1,expr2]
>   redoContextReduction (LeftSection expr _) =
>     redoContextReduction expr
>   redoContextReduction (RightSection _ expr) =
>     redoContextReduction expr
>   redoContextReduction (Lambda _ expr) =
>     redoContextReduction expr
>   redoContextReduction (Let decls expr) =
>     redoContextReduction decls `tyEnvComp` redoContextReduction expr
>   redoContextReduction (Do stmts expr) =
>     redoContextReduction stmts `tyEnvComp` redoContextReduction expr
>   redoContextReduction (IfThenElse expr1 expr2 expr3) =
>     redoContextReduction [expr1,expr2,expr3]
>   redoContextReduction (Case expr alts) =
>     redoContextReduction expr `tyEnvComp` redoContextReduction alts

> instance RedoContextReduction Statement where
>   redoContextReduction (StmtExpr expr) =
>     redoContextReduction expr
>   redoContextReduction (StmtDecl decls) =
>     redoContextReduction decls
>   redoContextReduction (StmtBind _ expr) =
>     redoContextReduction expr

> instance RedoContextReduction Alt where
>   redoContextReduction (Alt _ _ rhs) =
>     redoContextReduction rhs

> redoContextReductionVars :: Position -> [Ident] ->
>                             ModuleIdent -> TypeClassEnv -> InstanceEnv ->
>                             ValueEnv -> ValueEnv
> redoContextReductionVars p xs m typeClassEnv instEnv tyEnv =
>   foldr (\x -> redoContextReductionVar p x m typeClassEnv instEnv) tyEnv xs

> redoContextReductionVar :: Position -> Ident ->
>                            ModuleIdent -> TypeClassEnv -> InstanceEnv ->
>                            ValueEnv -> ValueEnv
> redoContextReductionVar p v m typeClassEnv instEnv tyEnv =
>   rebindFun m v newSigma tyEnv
>   where (ForAll n tyWC) = funType (qualify v) tyEnv
>         tyWC' = mapTypeContext (contextReduction p typeClassEnv instEnv) tyWC
>         newSigma = ForAll n tyWC'

\end{lstlisting}
