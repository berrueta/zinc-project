% $Id: DataConstrBinding.lhs 1065 2004-08-20 11:25:54Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{DataConstrsBinding.lhs}
\codesection{Defining Data Constructors}
\begin{lstlisting}

> module DataConstrBinding(bindLocalDataConstrs) where
> import Ident
> import TypeConstructorEnv
> import ValueEnv
> import TopEnv(localBindings)
> import Maybe
> import Types

\end{lstlisting}
The types of all data constructors are entered into
the type environment using the information just entered into the type
constructor environment. Thus, we can be sure that all type variables
have been properly renamed and all type synonyms are already expanded.
\begin{lstlisting}

> bindLocalDataConstrs :: ModuleIdent -> TCEnv -> ValueEnv -> ValueEnv
> bindLocalDataConstrs m tcEnv tyEnv = tyEnv'
>   where tyEnv'         = foldr (bindLocalData m) tyEnv localTypeInfos
>         localTypeInfos = map snd $ localBindings tcEnv

> bindLocalData :: ModuleIdent -> TypeInfo -> ValueEnv -> ValueEnv
> bindLocalData m (DataType tc n cs) tyEnv =
>   foldr (bindConstr m n (constrType tc n)) tyEnv (catMaybes cs)
>   where bindConstr :: ModuleIdent -> Int -> Type -> Data [Type] ->
>                       ValueEnv -> ValueEnv
>         bindConstr m n ty (Data c n' tys) =
>           bindLocalGlobalInfo DataConstructor m c ets
>           where ets = ForAllExist n n' (foldr typeArrow ty tys)
> bindLocalData m (RenamingType tc n (Data c n' ty)) tyEnv =
>   bindLocalGlobalInfo NewtypeConstructor m c ets tyEnv
>   where ets = ForAllExist n n' (typeArrow ty (constrType tc n))
> bindLocalData m (AliasType _ _ _) tyEnv = tyEnv

> constrType :: QualIdent -> Int -> Type
> constrType tc n = typeApply (TypeConstructor tc) (map TypeVariable [0..n-1])

\end{lstlisting}
