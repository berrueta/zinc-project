% $Id: KindEnv.lhs 1337 2004-09-06 18:44:31Z berrueta $
%
% Copyright (c) 2003-2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{KindEnv.lhs}
\codesection{Kind environment}
\begin{lstlisting}

> module KindEnv where
> import Ident
> import TopEnv
> import Kind
> import Maybe
> import Utils
> import Set

\end{lstlisting}
The kind environment contains information about the kind of type constructors
and type classes. Both are stored in the same environment (so they will
collide).
\begin{lstlisting}

> data KindInfo = KindInfo QualIdent Kind
>               deriving (Show,Eq)

> instance Entity KindInfo where
>   origName (KindInfo qid _) = qid

\end{lstlisting}
Basic operations on kind environments.
\begin{lstlisting}

> type KindEnv = TopEnv KindInfo

> bindKind :: ModuleIdent -> Ident -> Kind -> KindEnv -> KindEnv
> bindKind m t k
>   | uniqueId t == 0 = bindLocalTopEnv t ki . qualBindLocalTopEnv t' ki
>   | otherwise       = bindLocalTopEnv t ki
>   where t' = qualifyWith m t
>         ki = KindInfo t' k

> lookupKind :: Ident -> KindEnv -> [KindInfo]
> lookupKind x kindEnv = lookupTopEnv x kindEnv ++! lookupTuple x

> qualLookupKind :: QualIdent -> KindEnv -> [KindInfo]
> qualLookupKind x kindEnv = qualLookupTopEnv x kindEnv ++! lookupTuple (unqualify x)

> lookupTuple :: Ident -> [KindInfo]
> lookupTuple x
>   | isTupleId x = [KindInfo (qTupleId $ tupleArity x) (tupleKind x)]
>   | otherwise   = []

\end{lstlisting}
The kinds of tuple type constructors can not be loaded into the environment,
because there are an infinite number of them. So they are computed:
\begin{lstlisting}

> tupleKind :: Ident -> Kind
> tupleKind id = foldr ( \_ k -> KFun Star k) Star [1..(tupleArity id)]

\end{lstlisting}
The function \texttt{fvEnv} computes the set of free kind variables
on the kind environment.
\begin{lstlisting}

> fvEnv :: KindEnv -> Set Ident
> fvEnv kindEnv =
>   fromListSet [kv | kind <- localKinds kindEnv, kv <- kindVars kind]

\end{lstlisting}
Auxiliary function.
\begin{lstlisting}

> localKinds :: KindEnv -> [Kind]
> localKinds kindEnv = [kind | (_,KindInfo _ kind) <- localBindings kindEnv]

\end{lstlisting}
