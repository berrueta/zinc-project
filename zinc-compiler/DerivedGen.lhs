% $Id: DerivedGen.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{DerivedGen.lhs}
\codesection{Generation of derived instances}
\begin{lstlisting}

> module DerivedGen(genDerived) where
> import CurrySyntax
> import TypeExpr
> import Position
> import Ident
> import Maybe
> import List
> import Error(errorAt)

\end{lstlisting}
Generation of derived instances is a syntactical procedure, described
in the Haskell Report (chapter 10). It can
be done only for a few predefined (well-known) type classes
\begin{lstlisting}

> genDerived :: [Decl] -> [Decl]
> genDerived ds = ds ++ derivedDs
>   where derivedDs = concat $ map genDerivedDecl ds

> genDerivedDecl :: Decl -> [Decl]
> genDerivedDecl decl@(DataDecl _ _ _ _ derivedTC) =
>   map (genDerivedTC decl) (nub derivedTC)
> genDerivedDecl decl@(NewtypeDecl _ _ _ _ derivedTC) =
>   map (genDerivedTC decl) (nub derivedTC)
> genDerivedDecl _ = []

\end{lstlisting}
Each type class uses different function to generate the instance.
\begin{lstlisting}

> genDerivedTC :: Decl -> Ident -> Decl
> genDerivedTC decl@(DataDecl p _ _ constrs _) tc
>   | null constrs = errorAt p noConcreteType
>   | tc == eqId   = genInstance decl tc commonCtx (eqMethods  p constrs)
>   | tc == ordId  = genInstance decl tc commonCtx (ordMethods p constrs)
>   | tc == enumId = genInstance decl tc commonCtx (enumMethods p constrs)
>   | tc == boundedId =
>       genInstance decl tc commonCtx (boundedMethods p constrs)
>   | tc == showId = genInstance decl tc commonCtx (showMethods p constrs)
>   | otherwise = errorAt p (unknownDerived tc)
>   where commonCtx = commonTypeContext decl tc
> genDerivedTC (NewtypeDecl p nc tvs (NewConstrDecl p' evs c ty) derivedTC) tc =
>   genDerivedTC (DataDecl p nc tvs [ConstrDecl p' evs c [ty]] derivedTC) tc

> genInstance :: Decl -> Ident -> TypeExprContext -> [Decl] -> Decl
> genInstance (DataDecl p tcId tvs _ _) typeClassId ctx methods =
>   InstanceDecl p ctx typeClassQId typeExpr methods
>   where typeClassQId = qualifyWith preludeMIdent typeClassId
>         typeExpr = typeExprApplyConstructor (qualify tcId)
>                                             (map TypeExprVariable tvs)

> commonTypeContext :: Decl -> Ident -> TypeExprContext
> commonTypeContext (DataDecl _ _ tvs _ _) typeClassId = TypeExprContext
>   (map (TypeExprClassConstraint typeClassQId . TypeExprVariable) tvs)
>   where typeClassQId = qualifyWith preludeMIdent typeClassId

\end{lstlisting}
The derived instances of \lstinline{Ord} compare their arguments
lexicographically, i.e., earlier constructors in the datatype declaration
counting as smaller than later ones.

Derived comparisons always traverse constructors from left to right.

\ToDo{All derived operations of class Eq and Ord should be strict in both
arguments. For example, $False \leq \perp$ is $\perp$, even though
$False$ is the first constructor of the Bool type.}
\begin{lstlisting}

> eqMethods :: Position -> [ConstrDecl] -> [Decl]
> eqMethods p constrs =
>   evalAnnotRigid p eqIdent :
>   (map (FunctionDecl p eqIdent . (:[]) . compEquation p eqIdent constrComp
>                                                         termComp exprComb)
>        cartesian)
>   where eqIdent = mkIdent "=="
>         cartesian = [(x,y) | x <- constrs, y <- constrs]
>         constrComp :: Ident -> Ident -> Expression
>         constrComp x y = if x == y then trueVal else falseVal
>         termComp :: Expression -> Expression -> Expression
>         termComp x y = Apply (Apply eqFun x) y
>         exprComb :: [Expression] -> Expression
>         exprComb es
>           | null es = trueVal
>           | otherwise = foldr1 (\x y -> Apply (Apply andFun x) y) es

> ordMethods :: Position -> [ConstrDecl] -> [Decl]
> ordMethods p constrs =
>   evalAnnotRigid p compareIdent :
>   (map (FunctionDecl p compareIdent . (:[]) . compEquation p compareIdent constrComp
>                                                              termComp exprComb)
>        cartesian)
>   where compareIdent = mkIdent "compare"
>         cartesian = [(x,y) | x <- constrs, y <- constrs]
>         constrComp :: Ident -> Ident -> Expression
>         constrComp x y =
>           case compare (fromJust $ elemIndex x lexicographicalOrder)
>                        (fromJust $ elemIndex y lexicographicalOrder) of
>             LT -> ltVal
>             GT -> gtVal
>             EQ -> eqVal
>         termComp :: Expression -> Expression -> Expression
>         termComp x y = Apply (Apply compareFun x) y
>         exprComb :: [Expression] -> Expression
>         exprComb es
>           | null es = eqVal
>           | otherwise =
>               foldr1 (\x y -> Case x [Alt p ltCT (SimpleRhs p ltVal []),
>                                       Alt p gtCT (SimpleRhs p gtVal []),
>                                       Alt p eqCT (SimpleRhs p y     [])])
>                      es
>         lexicographicalOrder :: [Ident]
>         lexicographicalOrder = map (\(ConstrDecl _ _ dc _) -> dc) constrs

> compEquation :: Position -> Ident ->
>                 (Ident -> Ident -> Expression) ->
>                 (Expression -> Expression -> Expression) ->
>                 ([Expression] -> Expression) ->
>                 (ConstrDecl,ConstrDecl) ->
>                 Equation
> compEquation p funId constrComp termComp exprComb
>   (ConstrDecl _ _ dataConstrId1 tes1,ConstrDecl _ _ dataConstrId2 tes2) =
>   simpleEqn p funId [constrTerm1,constrTerm2] expr
>   where constrTerm1,constrTerm2 :: ConstrTerm
>         constrTerm1 = ConstructorPattern (qualify $ dataConstrId1)
>                                          (map VariablePattern vars1)
>         constrTerm2 = ConstructorPattern (qualify $ dataConstrId2)
>                                          (map VariablePattern vars2)
>         compArgs :: Expression
>         compArgs = exprComb (zipWith termComp (map (Variable . qualify) vars1)
>                                               (map (Variable . qualify) vars2))
>         expr :: Expression
>         expr = if (dataConstrId1 == dataConstrId2)
>                  then compArgs
>                  else constrComp dataConstrId1 dataConstrId2
>         vars1, vars2 :: [Ident]
>         vars1 = [mkIdent ("x" ++ show i) | i <- [1..(length tes1)]]
>         vars2 = [mkIdent ("y" ++ show i) | i <- [1..(length tes2)]]

\end{lstlisting}
\lstinline{Enum} instances can be derived only for enumeration types,
i.e., types where all constructors have no arguments. The definition
of methods \lstinline{succ}, \lstinline{pred}, \lstinline{toEnum}, and
\lstinline{fromEnum} is straight forward. Note that these definitions
are flexible in constrast to the \lstinline{Eq} and \lstinline{Ord}
methods.
\begin{lstlisting}

> enumMethods :: Position -> [ConstrDecl] -> [Decl]
> enumMethods p constrs
>   | isEnumeration constrs =
>       enumMethodDecls p predId predSuccEqn (tail cs) cs ++
>       enumMethodDecls p succId predSuccEqn cs (tail cs) ++
>       enumMethodDecls p toEnumId toEnumEqn [0..] cs ++
>       enumMethodDecls p fromEnumId fromEnumEqn cs [0..]
>   | otherwise = errorAt p (noEnumerationType enumId)
>   where cs = [c | ConstrDecl _ _ c _ <- constrs]
>         predId = mkIdent "pred"
>         succId = mkIdent "succ"
>         toEnumId = mkIdent "toEnum"
>         fromEnumId = mkIdent "fromEnum"

> isEnumeration :: [ConstrDecl] -> Bool
> isEnumeration = all isConstant
>   where isConstant (ConstrDecl _ _ _ tys) = null tys

> enumMethodDecls :: Position -> Ident -> (Position -> Ident -> a -> b -> Equation)
>                 -> [a] -> [b] -> [Decl]
> enumMethodDecls p f mkEqn xs ys = map (FunctionDecl p f . (:[]))
>                                       (zipWith (mkEqn p f) xs ys)

> predSuccEqn :: Position -> Ident -> Ident -> Ident -> Equation
> predSuccEqn p funId c1 c2 =
>   simpleEqn p funId [VariablePattern c1] (Constructor (qualify c2))

> toEnumEqn :: Position -> Ident -> Int -> Ident -> Equation
> toEnumEqn p funId i c =
>   simpleEqn p funId [LiteralPattern intLiteral] (Constructor (qualify c))
>   where intLiteral = Int (mkIdent (show i)) i

> fromEnumEqn :: Position -> Ident -> Ident -> Int -> Equation
> fromEnumEqn p funId c i =
>   simpleEqn p funId [ConstructorPattern (qualify c) []] (Literal intLiteral)
>   where intLiteral = Int (mkIdent (show i)) i

\end{lstlisting}
\lstinline{Bounded} instances can be derived only for enumeration
types and for types with a single constructor. For enumerations
\lstinline{minBound} and \lstinline{maxBound} are determined by the
first and last data constructor, respectively. For single constructor
types, \lstinline{minBound} and \lstinline{maxBound} are determined by
the respective bounds of all arguments.
\begin{lstlisting}

> boundedMethods :: Position -> [ConstrDecl] -> [Decl]
> boundedMethods p constrs
>   | isEnumeration constrs =
>       map (enumBoundDecl p) [(minBoundId,minConstr),(maxBoundId,maxConstr)] 
>   | length constrs == 1 =
>       map (tupleBoundDecl p minConstr (length tys)) [minBoundId,maxBoundId]
>   | otherwise = errorAt p (noBoundedType boundedId)
>   where minBoundId = mkIdent "minBound"
>         maxBoundId = mkIdent "maxBound"
>         ConstrDecl _ _ minConstr tys = head constrs
>         ConstrDecl _ _ maxConstr _   = last constrs

> enumBoundDecl :: Position -> (Ident,Ident) -> Decl
> enumBoundDecl p (f,c) =
>   FunctionDecl p f [simpleEqn p f [] (Constructor (qualify c))]

> tupleBoundDecl :: Position -> Ident -> Int -> Ident -> Decl
> tupleBoundDecl p c n f = FunctionDecl p f [simpleEqn p f [] (bound c n f)]
>   where bound c n f =
>           foldl Apply (Constructor (qualify c))
>                 (replicate n (Variable (qualifyWith preludeMIdent f)))

\end{lstlisting}
The \lstinline{show} method is simpler than the previous comparision
methods. Even a Curry-based implementation is possible (it is shown
below), at the present we prefer the primitive operation.
\begin{lstlisting}

> showMethods :: Position -> [ConstrDecl] -> [Decl]
> showMethods p _ =
>   [evalAnnotRigid p showIdent,
>    FunctionDecl p showIdent [simpleEqn p showIdent [] showPrim]]
>   where showIdent = mkIdent "show"

showMethods :: Position -> [ConstrDecl [TypeExpr]] -> [Decl]
showMethods p constrs =
  [evalAnnotRigid p showIdent,
   FunctionDecl p showIdent (map (showEquation p) constrs)]
  where showIdent = mkIdent "show"

showEquation :: Position -> ConstrDecl [TypeExpr] -> Equation
showEquation p (ConstrDecl _ dataConstrId _ tes) =
  simpleEqn p [constrTerm] expr
  where constrTerm = ConstructorPattern (qualify $ dataConstrId)
                                        (map VariablePattern vars)
        constrExpr = Literal (String (name $ dataConstrId))
        argsExprs = map (Apply showParensFun . Variable . qualify) vars
        expr = foldl (\x y -> let lp = InfixApply x concatOp spaceString
                              in  InfixApply lp concatOp y)
                     constrExpr argsExprs
        vars = [mkIdent ("x" ++ show i) | i <- [1..(length tes)]]

\end{lstlisting}
Common functions:
\begin{lstlisting}

> evalAnnotRigid :: Position -> Ident -> Decl
> evalAnnotRigid p f = EvalAnnot p [f] EvalRigid

> simpleEqn :: Position -> Ident -> [ConstrTerm] -> Expression -> Equation
> simpleEqn p funId ts e = Equation p lhs rhs
>   where lhs = FunLhs funId ts
>         rhs = SimpleRhs p e []

> comparePrim,showPrim :: Expression
> comparePrim = Variable $ qualifyWith preludeMIdent $ mkIdent "primCompare"
> showPrim    = Variable $ qualifyWith preludeMIdent $ mkIdent "primShow"

> trueVal,falseVal,ltVal,gtVal,eqVal :: Expression
> trueVal  = Variable $ qualifyWith preludeMIdent $ mkIdent "True"
> falseVal = Variable $ qualifyWith preludeMIdent $ mkIdent "False"
> ltVal    = Variable $ qualifyWith preludeMIdent $ mkIdent "LT"
> gtVal    = Variable $ qualifyWith preludeMIdent $ mkIdent "GT"
> eqVal    = Variable $ qualifyWith preludeMIdent $ mkIdent "EQ"

> ltCT,gtCT,eqCT :: ConstrTerm
> ltCT = ConstructorPattern (qualifyWith preludeMIdent $ mkIdent "LT") []
> gtCT = ConstructorPattern (qualifyWith preludeMIdent $ mkIdent "GT") []
> eqCT = ConstructorPattern (qualifyWith preludeMIdent $ mkIdent "EQ") []

> andFun,eqFun,compareFun,showFun,showParensFun :: Expression
> andFun        = Variable $ qualifyWith preludeMIdent $ mkIdent "&&"
> eqFun         = Variable $ qualifyWith preludeMIdent $ mkIdent "=="
> compareFun    = Variable $ qualifyWith preludeMIdent $ mkIdent "compare"
> showFun       = Variable $ qualifyWith preludeMIdent $ mkIdent "show"
> showParensFun = Variable $ qualifyWith preludeMIdent $ mkIdent "showParens"

> concatOp :: InfixOp
> concatOp = InfixOp $ qualifyWith preludeMIdent $ mkIdent "++"

> spaceString :: Expression
> spaceString = Literal (String " ")

\end{lstlisting}
Error messages:
\begin{lstlisting}

> unknownDerived :: Ident -> String
> unknownDerived tc =
>  "Cannot generate derived instance for type class " ++ name tc

> noConcreteType :: String
> noConcreteType = "Cannot derive instances for abstract data types"

> noEnumerationType :: Ident -> String
> noEnumerationType tc =
>   name tc ++ " instances can be derived only for enumerations"

> noBoundedType :: Ident -> String
> noBoundedType tc =
>   name tc ++
>   " instances can be derived only for enumerations and single constructor types"

\end{lstlisting}
