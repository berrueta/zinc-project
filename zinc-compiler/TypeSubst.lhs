% $Id: TypeSubst.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeSubst.lhs}
\codesection{Type Substitutions}
This module implements substitutions on types.
\begin{lstlisting}

> module TypeSubst(module TypeSubst, idSubst,bindSubst,compose) where
> import Types
> import ValueEnv
> import Ident
> import TopEnv
> import Maybe
> import List
> import Subst

> type TypeSubst = Subst Int Type

> class SubstType a where
>   subst :: TypeSubst -> a -> a

> bindVar :: Int -> Type -> TypeSubst -> TypeSubst
> bindVar tv ty = compose (bindSubst tv ty idSubst)

> substVar :: TypeSubst -> Int -> Type
> substVar = substVar' TypeVariable subst

> instance SubstType Type where
>   subst sigma (TypeConstructor tc) =
>     TypeConstructor tc
>   subst sigma (TypeVariable tv) = substVar sigma tv
>   subst sigma (TypeConstrained tys tv) =
>     case substVar sigma tv of
>       TypeVariable tv -> TypeConstrained tys tv
>       ty -> ty
>   subst sigma (TypeSkolem k) = TypeSkolem k
>   subst sigma (TypeApplication ty1 ty2) =
>     TypeApplication (subst sigma ty1) (subst sigma ty2)

> instance SubstType TypeScheme where
>   subst sigma (ForAll n ty) =
>     ForAll n (subst (foldr unbindSubst sigma [0..n-1]) ty)

> instance SubstType ExistTypeScheme where
>   subst sigma (ForAllExist n n' ty) =
>     ForAllExist n n' (subst (foldr unbindSubst sigma [0..n+n'-1]) ty)

> instance SubstType TypeWithContext where
>   subst sigma (TypeWithContext ctx ty) =
>     TypeWithContext (subst sigma ctx) (subst sigma ty)

> instance SubstType TypeContext where
>   subst sigma (TypeContext classPreds) =
>     TypeContext (subst sigma classPreds)

> instance SubstType TypeClassConstraint where
>   subst sigma (TypeClassConstraint tc ty) =
>     TypeClassConstraint tc (subst sigma ty)

> instance SubstType a => SubstType [a] where
>   subst sigma = map (subst sigma)

> instance SubstType ValueInfo where
>   subst theta (DataConstructor c ty) = DataConstructor c ty
>   subst theta (NewtypeConstructor c ty) = NewtypeConstructor c ty
>   subst theta (Value v ty) = Value v (subst theta ty)

> instance SubstType a => SubstType (TopEnv a) where
>   subst = fmap . subst

\end{lstlisting}
