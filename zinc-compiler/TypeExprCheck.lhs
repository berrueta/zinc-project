% $Id: TypeExprCheck.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeExprCheck.lhs}
\codesection{Type expression checking}
After disambiguating type expressions, they are checked. Some tests are
run on them. As a side effect, type constructors and type classes
are fully-qualified.
\begin{lstlisting}

> module TypeExprCheck(checkTypeExprDecls,checkTypeExprGoal) where
> import TypeExpr
> import CurrySyntax
> import Ident
> import TypeConstructorEnv
> import TypeClassEnv
> import CurrySyntaxUtils
> import TopEnv
> import Position
> import Base
> import CurryPP
> import Error(errorAt,internalError)

\end{lstlisting}
Blocks of declarations. It also checks for multiple declaration of the
same type constructor or type class identifier.
\begin{lstlisting}

> checkTypeExprDecls :: ModuleIdent -> TCEnv -> TypeClassEnv ->
>                       [Decl] -> [Decl]
> checkTypeExprDecls m tcEnv typeClassEnv decls =
>   checkTypeExprDecls' m [] [] tcEnv typeClassEnv decls'
>   where typeDs = filter (\d -> isTypeDecl d || isTypeClassDecl d) decls
>         decls' = case linear (map tcDefinedByPos typeDs) of
>           Linear                  -> decls
>           NonLinear (PIdent p tc) -> errorAt p (duplicateType tc)


> checkTypeExprDecls' :: ModuleIdent -> [QualIdent] -> [QualIdent] ->
>                        TCEnv -> TypeClassEnv ->
>                        [Decl] -> [Decl]
> checkTypeExprDecls' m localTc localTypeClasses tcEnv typeClassEnv decls =
>   map (checkTypeExpr m noPos localTc' localTypeClasses' tcEnv typeClassEnv) decls
>   where localTc',localTypeClasses' :: [QualIdent]
>         localTc' = localTc ++
>                    (map (qualifyWith m) $ concat $ map tcDefinedBy tcDecls)
>         localTypeClasses' = localTypeClasses ++
>                             (map (qualifyWith m) $ concat $
>                                map tcDefinedBy typeClassDecls)
>         tcDecls,typeClassDecls :: [Decl]
>         tcDecls = filter isTypeDecl decls
>         typeClassDecls = filter isTypeClassDecl decls

\end{lstlisting}
Goals.
\begin{lstlisting}

> checkTypeExprGoal :: TCEnv -> TypeClassEnv -> Goal -> Goal
> checkTypeExprGoal tcEnv typeClassEnv goal =
>   checkTypeExpr m noPos [] [] tcEnv typeClassEnv goal
>   where m = undefined

\end{lstlisting}
These two functions perform the real qualification work. As identifiers
of type constructors and type classes must collide, the search is
performed in both environments.
\begin{lstlisting}

> qualTypeConstructor :: ModuleIdent -> Position ->
>                        [QualIdent] -> [QualIdent] ->
>                        TCEnv -> TypeClassEnv ->
>                        QualIdent -> QualIdent
> qualTypeConstructor m p localTc localTypeClasses tcEnv typeClassEnv qid =
>   case candidatesTC of
>     [qid'] -> if null candidatesTypeClass
>                  then qid'
>                  else errorAt p (collisionWithTypeClass qid candidatesTypeClass)
>     []     -> errorAt p (undefinedType qid)
>     xs     -> errorAt p (ambiguousType qid xs)
>   where candidatesTC = localCandidatesTC ++ importedCandidatesTC
>         candidatesTypeClass = localCandidatesTypeClass ++
>                               importedCandidatesTypeClass
>         importedCandidatesTC =
>           map origName $ qualLookupTC qid tcEnv
>         importedCandidatesTypeClass =
>           map origName $ qualLookupTypeClass qid typeClassEnv
>         lqid = qualQualify m qid
>         localCandidatesTC        = filter (==lqid) localTc
>         localCandidatesTypeClass = filter (==lqid) localTypeClasses

> qualTypeClass :: ModuleIdent -> Position ->
>                  [QualIdent] -> [QualIdent] ->
>                  TCEnv -> TypeClassEnv ->
>                  QualIdent -> QualIdent
> qualTypeClass m p localTc localTypeClasses tcEnv typeClassEnv qid =
>   case candidatesTypeClass of
>     [qid'] -> if null candidatesTC
>                  then qid'
>                  else errorAt p (collisionWithType qid candidatesTC)
>     []     -> errorAt p (undefinedTypeClass qid)
>     xs     -> errorAt p (ambiguousTypeClass qid xs)
>   where candidatesTC = localCandidatesTC ++ importedCandidatesTC
>         candidatesTypeClass = localCandidatesTypeClass ++
>                               importedCandidatesTypeClass
>         importedCandidatesTC =
>           map origName $ qualLookupTC qid tcEnv
>         importedCandidatesTypeClass =
>           map origName $ qualLookupTypeClass qid typeClassEnv
>         lqid = qualQualify m qid
>         localCandidatesTC        = filter (==lqid) localTc
>         localCandidatesTypeClass = filter (==lqid) localTypeClasses

\end{lstlisting}
A type class describing entities that can contain type expressions
which can be checked.
\begin{lstlisting}

> class TypeExprCheckable a where
>   checkTypeExpr :: ModuleIdent   ->
>                   Position      ->
>                   [QualIdent]   ->  -- local type constructors
>                   [QualIdent]   ->  -- local type classes
>                   TCEnv         ->  -- type constructor environment
>                   TypeClassEnv  ->  -- type class environment
>                   a -> a

\end{lstlisting}
Generic instantiations.
\begin{lstlisting}

> instance TypeExprCheckable a => TypeExprCheckable [a] where
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv =
>     map (checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv)

> instance TypeExprCheckable a => TypeExprCheckable (Maybe a) where
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv (Nothing) =
>     Nothing
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv (Just x ) =
>     (Just (checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv x))

\end{lstlisting}
Type expressions. Here is where the real qualification takes place.
\begin{lstlisting}

> instance TypeExprCheckable TypeExpr where
>
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (TypeExprConstructor qid ) =
>     (TypeExprConstructor qid')
>     where qid' = qualTypeConstructor m p localTc localTypeClasses
>                                          tcEnv typeClassEnv qid
>
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (TypeExprVariable id) =
>     (TypeExprVariable id)
>
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (TypeExprApplication ty1  ty2 ) =
>     (TypeExprApplication ty1' ty2')
>     where ty1' = checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv ty1
>           ty2' = checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv ty2

\end{lstlisting}
Type contexts. We also check for unbounded type variables on the
context (i.e.: type variables used in the context but not in the
type expression), and avoid anonymous type variables on type
contexts.
\begin{lstlisting}

> instance TypeExprCheckable TypeExprWithContext where
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (TypeExprWithContext   ctx  ty )
>     | checkUnboundTypeVars p (tvUsedBy ty') (tvUsedBy ctx') =
>       (TypeExprWithContext ctx' ty')
>     where ctx'  = checkTypeExpr m p localTc localTypeClasses
>                                     tcEnv typeClassEnv ctx
>           ty'   = checkTypeExpr m p localTc localTypeClasses
>                                     tcEnv typeClassEnv ty

> instance TypeExprCheckable TypeExprContext where
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (TypeExprContext classPreds ) =
>     (TypeExprContext classPreds')
>     where classPreds' = checkTypeExpr m p localTc localTypeClasses
>                                           tcEnv typeClassEnv classPreds

> instance TypeExprCheckable TypeExprClassConstraint where
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (TypeExprClassConstraint   qid  ty )
>     | checkAnonTypeVars p (tvUsedBy ty') =
>       (TypeExprClassConstraint qid' ty')
>     where qid' = qualTypeClass m p localTc localTypeClasses
>                                    tcEnv typeClassEnv qid
>           ty'  = TypeExprVariable $
>                    checkTypeVariable m p localTc localTypeClasses
>                                      tcEnv typeClassEnv (head (tvUsedBy ty))


\end{lstlisting}
Declarations.
\begin{lstlisting}

> instance TypeExprCheckable Decl where
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (TypeClassDecl   p ctx  tc tv decls )
>     | checkAnonTypeVars    p [tv'] &&
>       checkUnboundTypeVars p [tv'] (tvUsedBy ctx') =
>       (TypeClassDecl p ctx' tc tv' decls')
>     where ctx'   = checkTypeExpr m p localTc localTypeClasses
>                                      tcEnv typeClassEnv ctx
>           tv'    = checkTypeVariable m  p localTc localTypeClasses
>                                         tcEnv typeClassEnv tv
>           decls' = checkTypeExpr m p localTc localTypeClasses
>                                      tcEnv typeClassEnv decls
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (InstanceDecl   p ctx  tc  tyexpr  decls )
>     | checkAnonTypeVars     p (tvUsedBy tyexpr') &&
>       checkSimpleTypeExpr   p tyexpr' &&
>       checkRepeatedTypeVars p (tvUsedBy tyexpr') &&
>       checkUnboundTypeVars  p (tvUsedBy tyexpr') (tvUsedBy ctx') =
>       (InstanceDecl p ctx' tc' tyexpr' decls')
>     where ctx'    = checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv ctx
>           tc'     = qualTypeClass m p localTc localTypeClasses
>                                       tcEnv typeClassEnv tc
>           tyexpr' = checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv tyexpr
>           decls'  = checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv decls
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (DataDecl   p tc tvs  constrs derivedTC)
>     | checkRepeatedTypeVars p tvs &&
>       all checkAnonTypeVarsConstrDecl constrs' &&
>       all (checkUnboundTypeVarsConstrDecl tvs') constrs'  =
>       (DataDecl p tc tvs' constrs' derivedTC)
>     where constrs' = checkTypeExpr m p localTc localTypeClasses
>                                        tcEnv typeClassEnv constrs
>           tvs'     = map (checkTypeVariable m p localTc localTypeClasses
>                                                 tcEnv typeClassEnv) tvs
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (NewtypeDecl p tc tvs constr derivedTC)
>     | checkRepeatedTypeVars p tvs &&
>       checkAnonTypeVarsNewConstrDecl constr' &&
>       checkUnboundTypeVarsNewConstrDecl tvs' constr' =
>     (NewtypeDecl p tc tvs' constr' derivedTC)
>     where constr' = checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv constr
>           tvs'    = map (checkTypeVariable m p localTc localTypeClasses
>                                                tcEnv typeClassEnv) tvs
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (TypeDecl   p tc tvs ty )
>     | checkRepeatedTypeVars p tvs' &&
>       checkAnonTypeVars     p (tvUsedBy ty') &&
>       checkUnboundTypeVars  p tvs' (tvUsedBy ty') =
>       (TypeDecl p tc tvs' ty')
>     where ty'  = checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv ty
>           tvs' = map (checkTypeVariable m p localTc localTypeClasses
>                                             tcEnv typeClassEnv) tvs
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (TypeSig p funIds tyWC) = TypeSig p funIds tyWC'
>     where tyWC' = checkTypeExpr m p localTc localTypeClasses
>                                     tcEnv typeClassEnv tyWC
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (FunctionDecl p i eqs ) =
>     (FunctionDecl p i eqs')
>     where eqs' = checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv eqs
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (ExternalDecl p cc s i tyexpr ) =
>     (ExternalDecl p cc s i tyexpr')
>     where tyexpr' = checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv tyexpr
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (PatternDecl p constr rhs ) =
>     (PatternDecl p constr rhs')
>     where rhs' = checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv rhs
>
>   checkTypeExpr _ _ _ _ _ _ decl@(ImportDecl     _ _ _ _ _) = decl
>   checkTypeExpr _ _ _ _ _ _ decl@(InfixDecl      _ _ _ _  ) = decl
>   checkTypeExpr _ _ _ _ _ _ decl@(EvalAnnot      _ _ _    ) = decl
>   checkTypeExpr _ _ _ _ _ _ decl@(ExtraVariables _ _      ) = decl


> instance TypeExprCheckable IDecl where
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (ITypeClassDecl   p ctx  tc tv decls )
>     | checkAnonTypeVars    p [tv'] &&
>       checkUnboundTypeVars p [tv'] (tvUsedBy ctx') =
>       (ITypeClassDecl p ctx' tc tv' decls')
>     where ctx'   = checkTypeExpr m p localTc localTypeClasses
>                                      tcEnv typeClassEnv ctx
>           tv'    = checkTypeVariable m  p localTc localTypeClasses
>                                         tcEnv typeClassEnv tv
>           decls' = checkTypeExpr m p localTc localTypeClasses
>                                      tcEnv typeClassEnv decls
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (IInstanceDecl   p ctx  tc  tyexpr )
>     | checkAnonTypeVars     p (tvUsedBy tyexpr') &&
>       checkSimpleTypeExpr   p tyexpr' &&
>       checkRepeatedTypeVars p (tvUsedBy tyexpr') &&
>       checkUnboundTypeVars  p (tvUsedBy tyexpr') (tvUsedBy ctx') =
>       (IInstanceDecl p ctx' tc' tyexpr')
>     where ctx'    = checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv ctx
>           tc'     = qualTypeClass m p localTc localTypeClasses
>                                       tcEnv typeClassEnv tc
>           tyexpr' = checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv tyexpr
>           
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (IDataDecl   p tc tvs constrs )
>     | checkAnonTypeVars p (tvUsedBy constrs') &&
>       checkUnboundTypeVars  p tvs' (tvUsedBy constrs') =
>       (IDataDecl p tc tvs' constrs')
>     where constrs' = checkTypeExpr m p localTc localTypeClasses
>                                        tcEnv typeClassEnv constrs
>           tvs'     = map (checkTypeVariable m p localTc localTypeClasses
>                                                 tcEnv typeClassEnv) tvs
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (INewtypeDecl   p tc tvs constr )
>     | checkAnonTypeVarsNewConstrDecl constr' &&
>       checkUnboundTypeVars  p tvs' (tvUsedBy constr') =
>       (INewtypeDecl p tc tvs' constr')
>     where constr' = checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv constr
>           tvs'    = map (checkTypeVariable m p localTc localTypeClasses
>                                                tcEnv typeClassEnv) tvs
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (ITypeDecl   p tc tvs ty )
>     | checkRepeatedTypeVars p tvs' &&
>       checkAnonTypeVars     p (tvUsedBy ty') &&
>       checkUnboundTypeVars  p tvs' (tvUsedBy ty') =
>       (ITypeDecl p tc tvs' ty')
>     where ty'  = checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv ty
>           tvs' = map (checkTypeVariable m p localTc localTypeClasses
>                                             tcEnv typeClassEnv) tvs
>
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (IFunctionDecl p funId ty ) =
>     (IFunctionDecl p funId ty')
>     where ty' = checkTypeExpr m p localTc localTypeClasses
>                                   tcEnv typeClassEnv ty
>
>   checkTypeExpr _ _ _ _ _ _ idecl@(IImportDecl    _ _    ) = idecl
>   checkTypeExpr _ _ _ _ _ _ idecl@(IInfixDecl     _ _ _ _) = idecl
>   checkTypeExpr _ _ _ _ _ _ idecl@(HidingDataDecl _ _ _  ) = idecl
>   checkTypeExpr _ _ _ _ _ _ idecl@(IKindDecl      _ _ _  ) = idecl

> instance TypeExprCheckable ConstrDecl where
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (ConstrDecl p tv dc tys) =
>     ConstrDecl p tv dc (checkTypeExpr m p localTc localTypeClasses
>                                           tcEnv typeClassEnv tys)
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (ConOpDecl p tv ty1 op ty2) =
>     ConOpDecl p tv (checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv ty1) op
>                    (checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv ty2)

> instance TypeExprCheckable NewConstrDecl where
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (NewConstrDecl p tv dc tyexpr) =
>     NewConstrDecl p tv dc (checkTypeExpr m p localTc localTypeClasses
>                                              tcEnv typeClassEnv tyexpr)

> instance TypeExprCheckable Equation where
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (Equation p cts rhs) =
>     (Equation p cts (checkTypeExpr m p localTc localTypeClasses
>                                        tcEnv typeClassEnv rhs))

> instance TypeExprCheckable Rhs where
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (SimpleRhs p expr decls) =
>     (SimpleRhs p (checkTypeExpr m p localTc localTypeClasses
>                                     tcEnv typeClassEnv expr)
>                  (checkTypeExpr m p localTc localTypeClasses
>                                     tcEnv typeClassEnv decls))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (GuardedRhs cond decls) =
>     (GuardedRhs (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv cond)
>                 (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv decls))

> instance TypeExprCheckable CondExpr where
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (CondExpr p expr1 expr2) =
>     (CondExpr p (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv expr1)
>                 (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv expr2))

> instance TypeExprCheckable Expression where
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Literal l) = (Literal l)
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Variable v) = (Variable v)
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Constructor c) = (Constructor c)
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Paren expr) =
>     (Paren (checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv expr))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Typed expr tyexpr) =
>     (Typed (checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv expr)
>            (checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv tyexpr))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Tuple exprs) =
>     (Tuple (checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv exprs))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (List exprs) =
>     (List (checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv exprs))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (ListCompr expr stats) =
>     (ListCompr (checkTypeExpr m p localTc localTypeClasses
>                                   tcEnv typeClassEnv expr)
>                (checkTypeExpr m p localTc localTypeClasses
>                                   tcEnv typeClassEnv stats))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (EnumFrom expr) =
>     (EnumFrom (checkTypeExpr m p localTc localTypeClasses
>                                  tcEnv typeClassEnv expr))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (EnumFromThen expr1 expr2) =
>     (EnumFromThen (checkTypeExpr m p localTc localTypeClasses
>                                      tcEnv typeClassEnv expr1)
>                   (checkTypeExpr m p localTc localTypeClasses
>                                      tcEnv typeClassEnv expr2))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (EnumFromTo expr1 expr2) =
>     (EnumFromTo (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv expr1)
>                 (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv expr2))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (EnumFromThenTo expr1 expr2 expr3) =
>     (EnumFromThenTo (checkTypeExpr m p localTc localTypeClasses
>                                        tcEnv typeClassEnv expr1)
>                     (checkTypeExpr m p localTc localTypeClasses
>                                        tcEnv typeClassEnv expr2)
>                     (checkTypeExpr m p localTc localTypeClasses
>                                        tcEnv typeClassEnv expr3))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (UnaryMinus id expr) =
>     (UnaryMinus id (checkTypeExpr m p localTc localTypeClasses
>                                       tcEnv typeClassEnv expr))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Apply expr1 expr2) =
>     (Apply (checkTypeExpr m p localTc localTypeClasses
>                               tcEnv typeClassEnv expr1)
>            (checkTypeExpr m p localTc localTypeClasses
>                               tcEnv typeClassEnv expr2))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (InfixApply expr1 op expr2) =
>     (InfixApply (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv expr1) op
>                 (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv expr2))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (LeftSection expr op) =
>     (LeftSection (checkTypeExpr m p localTc localTypeClasses
>                                     tcEnv typeClassEnv expr) op)
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (RightSection op expr) =
>     (RightSection op (checkTypeExpr m p localTc localTypeClasses
>                                         tcEnv typeClassEnv expr))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Lambda constrs expr) =
>     (Lambda constrs (checkTypeExpr m p localTc localTypeClasses
>                                        tcEnv typeClassEnv expr))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Let decls expr) =
>     (Let (checkTypeExpr m p localTc localTypeClasses
>                             tcEnv typeClassEnv decls)
>          (checkTypeExpr m p localTc localTypeClasses
>                             tcEnv typeClassEnv expr))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Do stats expr) =
>     (Do (checkTypeExpr m p localTc localTypeClasses
>                            tcEnv typeClassEnv stats)
>         (checkTypeExpr m p localTc localTypeClasses
>                            tcEnv typeClassEnv expr))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (IfThenElse expr1 expr2 expr3) =
>     (IfThenElse (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv expr1)
>                 (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv expr2)
>                 (checkTypeExpr m p localTc localTypeClasses
>                                    tcEnv typeClassEnv expr3))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (Case expr alts) =
>     (Case (checkTypeExpr m p localTc localTypeClasses
>                              tcEnv typeClassEnv expr)
>           (checkTypeExpr m p localTc localTypeClasses
>                              tcEnv typeClassEnv alts))

> instance TypeExprCheckable Statement where
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (StmtExpr expr) =
>     (StmtExpr (checkTypeExpr m p localTc localTypeClasses
>                                  tcEnv typeClassEnv expr))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (StmtDecl decls) =
>     (StmtDecl (checkTypeExpr m p localTc localTypeClasses
>                                  tcEnv typeClassEnv decls))
>   checkTypeExpr m p localTc localTypeClasses tcEnv typeClassEnv
>     (StmtBind constr expr) =
>     (StmtBind constr (checkTypeExpr m p localTc localTypeClasses
>                                         tcEnv typeClassEnv expr))

> instance TypeExprCheckable Alt where
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (Alt p constr rhs) =
>     (Alt p constr (checkTypeExpr m p localTc localTypeClasses
>                                      tcEnv typeClassEnv rhs))

> instance TypeExprCheckable Goal where
>   checkTypeExpr m _ localTc localTypeClasses tcEnv typeClassEnv
>     (Goal p expr  decls) =
>     (Goal p expr' decls')
>     where expr'  = checkTypeExpr m p localTc localTypeClasses
>                                      tcEnv typeClassEnv expr
>           decls' = checkTypeExpr m p localTc localTypeClasses
>                                      tcEnv typeClassEnv decls

\end{lstlisting}
This module also performs three tests. The first one searches for
anonymous type variables on unauthorized places. The second one
searches unbound type variables (variables used in the rhs but
not defined in the lhs). Finally, the third one checks for
repeated type vars on the lhs.
\begin{lstlisting}

> checkAnonTypeVars :: Position -> [Ident] -> Bool
> checkAnonTypeVars p tvs
>   | any isAnonId tvs = errorAt p invalidAnonTypeVar
>   | otherwise        = True

> checkUnboundTypeVars :: Position -> [Ident] -> [Ident] -> Bool
> checkUnboundTypeVars p knownTvs usedTvs
>   | null unboundTvs = True
>   | otherwise       = errorAt p $ unboundTypeVar (head unboundTvs)
>   where unboundTvs = filter (`notElem` knownTvs) usedTvs

> checkRepeatedTypeVars :: Position -> [Ident] -> Bool
> checkRepeatedTypeVars p tvs =
>   case linear tvs of
>     Linear       -> True
>     NonLinear tv -> errorAt p (repeatedTypeVar tv)

\end{lstlisting}
In order to provide more precise error messages, the anonymous type
variables and unbound variable checks descend by the constructor
declarations.
\begin{lstlisting}

> checkAnonTypeVarsConstrDecl :: ConstrDecl -> Bool
> checkAnonTypeVarsConstrDecl (ConstrDecl p _ _ tyexprs) =
>   checkAnonTypeVars p (tvUsedBy tyexprs)
> checkAnonTypeVarsConstrDecl (ConOpDecl p _ tyexpr1 op tyexpr2) =
>   checkAnonTypeVars p (tvUsedBy [tyexpr1,tyexpr2])

> checkAnonTypeVarsNewConstrDecl :: NewConstrDecl -> Bool
> checkAnonTypeVarsNewConstrDecl (NewConstrDecl p _ _ tyexpr) =
>   checkAnonTypeVars p (tvUsedBy tyexpr)

> checkUnboundTypeVarsConstrDecl :: [Ident] -> ConstrDecl -> Bool
> checkUnboundTypeVarsConstrDecl knownTvs (ConstrDecl p _ _ tyexprs) =
>   checkUnboundTypeVars p knownTvs (tvUsedBy tyexprs)
> checkUnboundTypeVarsConstrDecl knownTvs (ConOpDecl p _ tyexpr1 op tyexpr2) =
>   checkUnboundTypeVars p knownTvs (tvUsedBy [tyexpr1,tyexpr2])

> checkUnboundTypeVarsNewConstrDecl :: [Ident] -> NewConstrDecl -> Bool
> checkUnboundTypeVarsNewConstrDecl knownTvs (NewConstrDecl p _ _ tyexpr) =
>   checkUnboundTypeVars p knownTvs (tvUsedBy tyexpr)

\end{lstlisting}
At certain points of the syntax tree, we expect a type expression
variable, and in order to prevent confusion, it can't share the
identifier with a type constructor or type expression. This function
checks that a type variable identifier can't be confused with a type
identifier.
\begin{lstlisting}

> checkTypeVariable :: ModuleIdent -> Position ->
>                      [QualIdent] -> [QualIdent] ->
>                      TCEnv -> TypeClassEnv ->
>                      Ident -> Ident
> checkTypeVariable m p localTc localTypeClasses tcEnv typeClassEnv id =
>   case candidatesTC of
>     []     -> id
>     xs     -> errorAt p (expectedTypeVar id (head xs))
>   where candidatesTC = localCandidatesTC ++ importedCandidatesTC
>         candidatesTypeClass = localCandidatesTypeClass ++
>                               importedCandidatesTypeClass
>         importedCandidatesTC =
>           map origName $ lookupTC id tcEnv
>         importedCandidatesTypeClass =
>           map origName $ lookupTypeClass id typeClassEnv
>         lqid = qualifyWith m id
>         localCandidatesTC        = filter (==lqid) localTc
>         localCandidatesTypeClass = filter (==lqid) localTypeClasses

\end{lstlisting}
Other points require a restricted type expression, with must be
constructor-rooted, and can only have variables as arguments.

TODO: this function could be splitted to improve the quality of
error messages.
\begin{lstlisting}

> checkSimpleTypeExpr :: Position -> TypeExpr -> Bool
> checkSimpleTypeExpr p tyexpr
>   | isTypeExprConstructor (leftmostTypeExpr tyexpr) &&
>     all isTypeExprVariable (typeExprArguments tyexpr) = True
>   | otherwise = errorAt p (invalidTypeExpr tyexpr)

\end{lstlisting}
Auxiliary functions:
\begin{lstlisting}

> noPos :: Position
> noPos = internalError "TypeExprCheck.noPos"

> tcDefinedByPos :: Decl -> PIdent
> tcDefinedByPos (DataDecl      p   tc _ _ _) = PIdent p tc
> tcDefinedByPos (NewtypeDecl   p   tc _ _ _) = PIdent p tc
> tcDefinedByPos (TypeDecl      p   tc _ _  ) = PIdent p tc
> tcDefinedByPos (TypeClassDecl p _ tc _ _  ) = PIdent p tc
> tcDefinedByPos _ = internalError "tcDefinedByPos"

\end{lstlisting}
Error messages:
\begin{lstlisting}

> duplicateType :: Ident -> String
> duplicateType tc = "More than one definition for type " ++ name tc

> undefinedType :: QualIdent -> String
> undefinedType tc = "Undefined type " ++ qualName tc

> ambiguousType :: QualIdent -> [QualIdent] -> String
> ambiguousType tc xs = "Ambiguous type " ++ qualName tc ++ "\n" ++
>                       "Could refer to: " ++ (show xs)

> undefinedTypeClass :: QualIdent -> String
> undefinedTypeClass tc = "Undefined type class " ++ qualName tc

> ambiguousTypeClass :: QualIdent -> [QualIdent] -> String
> ambiguousTypeClass tc xs = "Ambiguous type class " ++ qualName tc ++ "\n" ++
>                            "Could refer to: " ++ (show xs)

> collisionWithTypeClass :: QualIdent -> [QualIdent] -> String
> collisionWithTypeClass tc xs = "Ambiguous type " ++ qualName tc ++ "\n" ++
>                                "Collides with type classes: " ++ (show xs)

> collisionWithType :: QualIdent -> [QualIdent] -> String
> collisionWithType tc xs = "Ambiguous type class " ++ qualName tc ++ "\n" ++
>                           "Collides with types: " ++ (show xs)

> invalidAnonTypeVar :: String
> invalidAnonTypeVar = "Anonymous type variable not allowed on right hand side " ++
>                      "of a type declaration"

> unboundTypeVar :: Ident -> String
> unboundTypeVar v = "Unbound type variable " ++ name v

> repeatedTypeVar :: Ident -> String
> repeatedTypeVar v = "Type variable " ++ name v ++ " occurs more than once on " ++
>                     "left hand side of type declaration"

> expectedTypeVar :: Ident -> QualIdent -> String
> expectedTypeVar tv tc = "Type variable was expected in " ++ name tv ++
>                         ", but found type " ++ qualName tc

> invalidTypeExpr :: TypeExpr -> String
> invalidTypeExpr tyexpr = "Invalid type expression " ++ (show $ ppTypeExpr 0 tyexpr)

\end{lstlisting}
