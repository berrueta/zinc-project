% $Id: EvalEnv.lhs 970 2004-08-08 12:05:32Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2003, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{EvalEnv.lhs}
\codesection{Evaluation modes}
\begin{lstlisting}

> module EvalEnv where
> import Ident
> import Env
> import CurrySyntax

\end{lstlisting}
The compiler has to collect the evaluation annotations for a program
in an environment. As these annotations affect only local declarations,
a flat environment mapping unqualified names onto annotations is
sufficient.
\begin{lstlisting}

> type EvalEnv = Env Ident EvalAnnotation

> bindEval :: Ident -> EvalAnnotation -> EvalEnv -> EvalEnv
> bindEval = bindEnv

> lookupEval :: Ident -> EvalEnv -> Maybe EvalAnnotation
> lookupEval f evEnv = lookupEnv f evEnv

\end{lstlisting}
