% $Id: Exports.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2000-2004, Wolfgang Lux
% Copyright (c) 2003-2005, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{Exports.lhs}
\codesection{Creating Interfaces}
This section describes how the exported interface of a compiled module
is computed.
\begin{lstlisting}

> module Exports(exportInterface) where
> import Base
> import CurrySyntax
> import TypeConstructorEnv
> import ValueEnv
> import Ident
> import Position
> import PEnv
> import Types
> import List
> import Set
> import Maybe
> import Error(errorAt,internalError)
> import TypeTrans
> import TypeExpr
> import Kind
> import KindEnv
> import TypeClassEnv
> import InstanceEnv
> import CurrySyntaxUtils
> import Env
> import TypeExprSubst
> import TypeExpansion(expandAliasTypeWithContext)
> import DualTopEnv(localBindingsDualTopEnv)

\end{lstlisting}
The function \texttt{exportInterface} uses the expanded
specifications and the corresponding environments in order to compute
to the interface of the module.

After checking that the interface is not ambiguous (done in the function
expandInterface), the compiler
generates the interface's declarations from the list of exported
functions and values. In order to make the interface more stable
against private changes in the module, we remove the hidden data
constructors of a data type in the interface when they occur
right-most in the declaration. In addition, newtypes whose constructor
is not exported are transformed into (abstract) data types.

If a type is imported from another module, its name is qualified with
the name of the module where it is defined. The same applies to an
exported function.

\ToDo{Include instance declarations in export list.}
\begin{lstlisting}

> exportInterface :: ModuleIdent -> Maybe ExportSpec ->
>                    (PEnv,TCEnv,TypeClassEnv,InstanceEnv,KindEnv,ValueEnv) ->
>                    Interface
> exportInterface m (Just (Exporting _ es))
>                 (pEnv,tcEnv,typeClassEnv,instEnv,kEnv,tyEnv) =
>   Interface m (imports ++ precs ++ hidden ++ decls)
>   where imports   = map (IImportDecl noPos) modList
>         modList   = usedModules decls
>         precs     = foldr (infixDecl m pEnv) [] es
>         hidden    = map (hiddenTypeDecl m tcEnv) (hiddenTypes typeDecls)
>         decls     = typeClassDecls ++ typeDecls ++ kindDecls ++
>                     instDecls ++ funDecls
>         typeClassDecls = foldr (typeClassDecl m typeClassEnv tyEnv) [] es
>         typeDecls      = foldr (typeDecl      m tcEnv             ) [] es
>         kindDecls      = foldr (kindDecl      m kEnv              ) [] es
>         funDecls       = foldr (funDecl       m tyEnv             ) [] es
>         instDecls      = instDecl m instEnv
> exportInterface _ Nothing _ =
>   internalError "exportInterface"

> infixDecl :: ModuleIdent -> PEnv -> Export -> [IDecl] -> [IDecl]
> infixDecl m pEnv (Export f) ds = iInfixDecl m pEnv f ds
> infixDecl m pEnv (ExportTypeWith tc cs) ds =
>   foldr (iInfixDecl m pEnv . qualifyLike tc) ds cs
> infixDecl m pEnv (ExportTypeClassWith _ mets) ds =
>   foldr (infixDecl m pEnv) ds (map (Export . qualifyWith m) mets)

> iInfixDecl :: ModuleIdent -> PEnv -> QualIdent -> [IDecl] -> [IDecl]
> iInfixDecl m pEnv op ds =
>   case qualLookupP op pEnv of
>     [] -> ds
>     [PrecInfo _ (OpPrec fix pr)] ->
>       IInfixDecl noPos fix pr (qualUnqualify m op) : ds
>     _ -> internalError "infixDecl"

> kindDecl :: ModuleIdent -> KindEnv -> Export -> [IDecl] -> [IDecl]
> kindDecl _ _ (Export _) ds = ds
> kindDecl m kEnv (ExportTypeWith tc cs) ds =
>   case (qualLookupKind tc kEnv) of
>     [KindInfo tc' kind] ->
>       (IKindDecl noPos (qualUnqualify m tc') kind) : ds
>     _ -> internalError "kindDecl"
> kindDecl m kEnv (ExportTypeClassWith tc _) ds =
>   case (qualLookupKind tc kEnv) of
>     [KindInfo tc' kind] ->
>       (IKindDecl noPos (qualUnqualify m tc') kind) : ds
>     _ -> internalError "kindDecl"

> typeClassDecl :: ModuleIdent -> TypeClassEnv -> ValueEnv -> Export ->
>                  [IDecl] -> [IDecl]
> typeClassDecl _ _            _     (Export         _  ) ds = ds
> typeClassDecl _ _            _     (ExportTypeWith _ _) ds = ds
> typeClassDecl m typeClassEnv tyEnv (ExportTypeClassWith tc methodIds) ds =
>   (ITypeClassDecl noPos ctx (qualUnqualify m tc') (head nameSupply)
>                   (map methodIDecl methodIds)) : ds
>  where methodIDecl :: Ident -> IDecl
>        methodIDecl methodId =
>          IFunctionDecl noPos (qualify methodId)
>                        (unqualifyIdentsIn m $
>                           fromTypeWithContext $
>                             expand $
>                               funType $
>                                 qualifyLikeOr tc m methodId)
>          where funType f = case qualLookupValue f tyEnv of
>                                   [Value _ (ForAll n tyWC)] -> tyWC
>                                   _ -> internalError("Exports.funType " ++ show f)
>                expand (TypeWithContext (TypeContext
>                          (TypeClassConstraint tc tyc:tccs)) ty) =
>                  let [tv] = typeVars tyc
>                  in expandAliasTypeWithContext
>                       (map TypeVariable ([1..tv] ++ [0] ++ [tv+1..]))
>                       (TypeWithContext (TypeContext tccs) ty)
>        (TypeClassInfo tc' tv super methodSigs) =
>          case qualLookupTypeClass tc typeClassEnv of
>            [x] -> x
>            _   -> internalError "typeClassDecl"
>        ctx = TypeExprContext $ map (\cls -> TypeExprClassConstraint
>                                               (qualUnqualify m cls)
>                                               (TypeExprVariable tv)) super
>                

> instDecl :: ModuleIdent -> InstanceEnv -> [IDecl]
> instDecl m instEnv = map exportInstance (localBindingsDualTopEnv instEnv)
>   where exportInstance :: (QualIdent,QualIdent,InstanceInfo) -> IDecl
>         exportInstance (_,_,InstanceInfo typeClass typeConstr m ctx' _ _) =
>           IInstanceDecl noPos ctx (qualUnqualify m typeClass)
>                         (typeExprApplyConstructor
>                            (qualUnqualify m typeConstr)
>                            (map TypeExprVariable tvIdents))
>           where ctx = unqualifyIdentsIn m
>                     $ instanceContextToTypeExprContext ctx' tvIdents
>                 tvIdents = [mkIdent ('v' : show i) | i <- [1..(length ctx')]]

> typeDecl :: ModuleIdent -> TCEnv -> Export -> [IDecl] -> [IDecl]
> typeDecl _ _ (Export _) ds = ds
> typeDecl _ _ (ExportTypeClassWith _ _) ds = ds
> typeDecl m tcEnv (ExportTypeWith tc cs) ds =
>   case qualLookupTC tc tcEnv of
>     [DataType tc n cs'] ->
>       iTypeDecl IDataDecl m tc n
>          (constrDecls m (drop n nameSupply) cs cs') : ds
>     [RenamingType tc n (Data c n' ty)]
>       | c `elem` cs ->
>           iTypeDecl INewtypeDecl m tc n (NewConstrDecl noPos tvs c ty') : ds
>       | otherwise -> iTypeDecl IDataDecl m tc n [] : ds
>       where tvs = take n' (drop n nameSupply)
>             ty' = fromType $ unqualifyIdentsIn m ty
>     [AliasType tc n ty] ->
>       iTypeDecl ITypeDecl m tc n (fromType $ unqualifyIdentsIn m ty) : ds
>     _ -> internalError "typeDecl"

> iTypeDecl :: (Position -> QualIdent -> [Ident] -> a -> IDecl)
>            -> ModuleIdent -> QualIdent -> Int -> a -> IDecl
> iTypeDecl f m tc n = f noPos (qualUnqualify m tc) (take n nameSupply)

> constrDecls :: ModuleIdent -> [Ident] -> [Ident] -> [Maybe (Data [Type])]
>             -> [Maybe ConstrDecl]
> constrDecls m tvs cs = clean . map (>>= constrDecl m tvs)
>   where clean = reverse . dropWhile isNothing . reverse
>         constrDecl m tvs (Data c n tys)
>           | c `elem` cs =
>               Just (iConstrDecl (take n tvs) c
>                                 (map (fromType . unqualifyIdentsIn m) tys))
>           | otherwise = Nothing

> iConstrDecl :: [Ident] -> Ident -> [TypeExpr] -> ConstrDecl
> iConstrDecl tvs op [ty1,ty2]
>   | isInfixOp op = ConOpDecl noPos tvs ty1 op ty2
> iConstrDecl tvs c tys = ConstrDecl noPos tvs c tys

> funDecl :: ModuleIdent -> ValueEnv -> Export -> [IDecl] -> [IDecl]
> funDecl m tyEnv (Export f) ds =
>   case qualLookupValue f tyEnv of
>     [Value _ (ForAll _ tyWC)] ->
>       IFunctionDecl noPos (qualUnqualify m f)
>                           (fromTypeWithContext $ unqualifyIdentsIn m tyWC) : ds
>     _ -> internalError "funDecl"
> funDecl _ _ (ExportTypeWith      _ _) ds = ds
> funDecl _ _ (ExportTypeClassWith _ _) ds = ds

\end{lstlisting}
The compiler determines the list of imported modules from the set of
module qualifiers that are used in the interface. Careful readers
probably will have noticed that the functions above carefully strip
the module prefix from all entities that are defined in the current
module. Note that the list of modules returned from
\texttt{usedModules} is not necessarily a subset of the modules that
were imported into the current module. This will happen when an
imported module re-exports entities from another module. E.g., given
the three modules
\begin{lstlisting}
module A where { data A = A; }
module B(A(..)) where { import A; }
module C where { import B; x = A; }
\end{lstlisting}
the interface for module \texttt{C} will import module \texttt{A} but
not module \texttt{B}.
\begin{lstlisting}

> usedModules :: [IDecl] -> [ModuleIdent]
> usedModules ds = filter notInternalType $ nub $ catMaybes $ map modul
>                           (tcUsedBy ds ++ concatMap funIds ds)
>   where nub = toListSet . fromListSet
>         modul = fst . splitQualIdent
>         notInternalType = not . (==) internalTypeMIdent

> funIds :: IDecl -> [QualIdent]
> funIds (IFunctionDecl _ f _) = [f]
> funIds _                     = []

\end{lstlisting}
After the interface declarations have been computed, the compiler
eventually must add hidden (data) type declarations to the interface
for all those types which were used in the interface but not exported
from the current module, so that these type constructors can always be
distinguished from type variables.
\begin{lstlisting}

> hiddenTypeDecl :: ModuleIdent -> TCEnv -> QualIdent -> IDecl
> hiddenTypeDecl m tcEnv tc =
>   case qualLookupTC (qualQualify m tc) tcEnv of
>     [DataType _ n _] -> hidingDataDecl tc n
>     [RenamingType _ n _] -> hidingDataDecl tc n
>     _ ->  internalError ("hiddenTypeDecl: " ++ show (qualQualify m tc))
>   where hidingDataDecl tc n =
>           HidingDataDecl noPos (unqualify tc) (take n nameSupply)

> hiddenTypes :: [IDecl] -> [QualIdent]
> hiddenTypes ds = [tc | tc <- toListSet tcs, not (isQualified tc)]
>   where tcs = foldr deleteFromSet (fromListSet (usedTypes ds))
>                     (definedTypes ds)

\end{lstlisting}
Note that we need to remove the special constructors, as they have
no valid representation in Curry syntax (in the interface file).

\ToDo{Convert the following functions in a method of a type class.}
\begin{lstlisting}

> usedTypes :: [IDecl] -> [QualIdent]
> usedTypes ds = foldr usedTypesDecl [] ds

> usedTypesDecl :: IDecl -> [QualIdent] -> [QualIdent]
> usedTypesDecl (IDataDecl _ _ _ cs) tcs =
>   foldr usedTypesConstrDecl tcs (catMaybes cs)
> usedTypesDecl (INewtypeDecl _ _ _ nc) tcs = usedTypesNewConstrDecl nc tcs
> usedTypesDecl (ITypeDecl _ _ _ ty) tcs = usedTypesType ty tcs
> usedTypesDecl (IFunctionDecl _ _ tyWC) tcs = usedTypesTypeWithContext tyWC tcs
> usedTypesDecl (IKindDecl _ _ _) tcs = tcs

> usedTypesConstrDecl :: ConstrDecl -> [QualIdent] -> [QualIdent]
> usedTypesConstrDecl (ConstrDecl _ _ _ tys) tcs = foldr usedTypesType tcs tys
> usedTypesConstrDecl (ConOpDecl _ _ ty1 _ ty2) tcs =
>   usedTypesType ty1 (usedTypesType ty2 tcs)

> usedTypesNewConstrDecl :: NewConstrDecl -> [QualIdent] -> [QualIdent]
> usedTypesNewConstrDecl (NewConstrDecl _ _ _ ty) tcs = usedTypesType ty tcs

> usedTypesTypeWithContext :: TypeExprWithContext -> [QualIdent] -> [QualIdent]
> usedTypesTypeWithContext (TypeExprWithContext ctx ty) tcs =
>   usedTypesType ty (usedTypesTypeContext ctx tcs)

> usedTypesTypeContext :: TypeExprContext -> [QualIdent] -> [QualIdent]
> usedTypesTypeContext (TypeExprContext classPreds) tcs =
>   foldr usedTypesTypeClassConstraint tcs classPreds

> usedTypesTypeClassConstraint :: TypeExprClassConstraint -> [QualIdent] ->
>                                 [QualIdent]
> usedTypesTypeClassConstraint (TypeExprClassConstraint tc tv) tcs =
>   tcs

> usedTypesType :: TypeExpr -> [QualIdent] -> [QualIdent]
> usedTypesType ty@(TypeExprConstructor tc) tcs 
>   | isInternalTypeExprConstructor ty = tcs
>   | otherwise = tc : tcs
> usedTypesType (TypeExprVariable _) tcs = tcs
> usedTypesType (TypeExprApplication ty1 ty2) tcs =
>   usedTypesType ty2 (usedTypesType ty1 tcs)

> definedTypes :: [IDecl] -> [QualIdent]
> definedTypes ds = foldr definedType [] ds

> definedType :: IDecl -> [QualIdent] -> [QualIdent]
> definedType (IDataDecl _ tc _ _) tcs = tc : tcs
> definedType (INewtypeDecl _ tc _ _) tcs = tc : tcs
> definedType (ITypeDecl _ tc _ _) tcs = tc : tcs
> definedType (IFunctionDecl _ _ _)  tcs = tcs
> definedType (IKindDecl _ _ _) tcs = tcs

\end{lstlisting}
Auxiliary definitions
\begin{lstlisting}

> noPos :: Position
> noPos = undefined

\end{lstlisting}
