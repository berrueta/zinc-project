% $Id: TypeClassBinding.lhs 1283 2004-09-03 10:51:59Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeClassBinding.lhs}
\codesection{Type Class Binding}
This module enters the type classes into their environment. At the same
time performs some checks on the declarations, detecting non-existing
parent type classes and recursive dependencies.
\begin{lstlisting}

> module TypeClassBinding where
> import CurrySyntax 
> import TypeClassEnv
> import Ident
> import SCC
> import Position
> import CurrySyntaxUtils
> import TopEnv
> import TypeExpr
> import Error(errorAt)
> import Base
> import Env
> import List
> import Expr(bv)

\end{lstlisting}
This function is the entry point of the module. First, it filters all
the type classes declarations, then sorts them by dependencies, and
finally binds each declaration into the environment.
\begin{lstlisting}

> bindTypeClasses :: ModuleIdent -> [Decl] -> TypeClassEnv -> TypeClassEnv
> bindTypeClasses m ds typeClassEnv =
>   foldl (flip $ bindDecl m) typeClassEnv tcDecls'
>   where tcDecls  = filter isTypeClassDecl ds
>         tcDecls' = sortTypeClassDecls m tcDecls

\end{lstlisting}
We don't need to check parent type classes before binding the type class,
because such a test has been performed before by the module TypeExprQual.
\begin{lstlisting}

> bindDecl :: ModuleIdent -> Decl -> TypeClassEnv -> TypeClassEnv
> bindDecl m decl@(TypeClassDecl p ctx tc tv methodDecls) typeClassEnv
>   | checkMethodDecls m tc tv methodDecls &&
>     checkLinearSigs methodDecls =
>     bindTypeClass m tc tv parents methodSigs typeClassEnv
>   where parents = parentTypeClassesDecl decl
>         methodSigs = methodSigsDecls tv methodDecls

\end{lstlisting}
Before the binding, type classes declarations must be sorted, so we
first create dependency groups with the declarations. If a group contains
more than one type class declaration, we have found a recursive dependency.
\begin{lstlisting}

> sortTypeClassDecls :: ModuleIdent -> [Decl] -> [Decl]
> sortTypeClassDecls m ds = concat $ map checkGroup grouped
>   where grouped = scc (map (qualifyWith m) . tcDefinedBy) typeClassesUsedBy ds
>         typeClassesUsedBy (TypeClassDecl _ ctx _ _ decls) =
>           tcUsedBy ctx ++
>           concatMap (\(TypeSig _ _ tyexprWC) -> tcUsedBy (typeExprContext tyexprWC))
>                     (filter isTypeSig decls)

> checkGroup :: [Decl] -> [Decl]
> checkGroup group
>   | length group == 1 = group
>   | otherwise = errorAt (declPosition $ head group)
>                         (recursiveDependency group)

\end{lstlisting}
Return the list of method signatures.

\ToDo{Overloaded methods are still prone to fail. The commented lines
in this function check for the presence of such methods, and stop
the processing.}
\begin{lstlisting}

> methodSigsDecls :: Ident -> [Decl] -> [(Ident,TypeExprWithContext)]
> methodSigsDecls tv = concatMap methodSigsDecl . filter isTypeSig
>   where methodSigsDecl :: Decl -> [(Ident,TypeExprWithContext)]
>         methodSigsDecl (TypeSig p ids tyWC)
>           -- | (typeExprContext tyWC) /= emptyTypeExprContext =
>           --    errorAt p overloadedMethodsNotSupported
>           | tv `notElem` (tvUsedBy (removeTypeExprContext tyWC)) =
>               errorAt p typeClassVariableNotPresent
>           | otherwise = zip ids (repeat tyWC)

> methodSigsIDecls :: [IDecl] -> [(Ident,TypeExprWithContext)]
> methodSigsIDecls = map methodSigsIDecl
>   where methodSigsIDecl :: IDecl -> (Ident,TypeExprWithContext)
>         methodSigsIDecl (IFunctionDecl _ qid tyWC) = (unqualify qid,tyWC)

\end{lstlisting}
Declaration checking.
\begin{lstlisting}

> checkMethodDecls :: ModuleIdent -> Ident -> Ident -> [Decl] -> Bool
> checkMethodDecls m tc tv = all checkMethodDecl
>   where checkMethodDecl (EvalAnnot p _ _) =
>           errorAt p evalAnnotNotAllowed
>         checkMethodDecl (FunctionDecl p _ _) =
>           errorAt p defaultMethodsNotSupported
>         checkMethodDecl (TypeSig p ids tyexprWC) =
>           True

> checkLinearSigs :: [Decl] -> Bool
> checkLinearSigs methodDecls =
>   case linear (concat (map vars (filter isTypeSig methodDecls))) of
>     Linear -> True
>     NonLinear (PIdent p v) -> errorAt p (duplicateTypeSig v)
>   where vars (TypeSig p fs _) = map (PIdent p) fs

\end{lstlisting}
Obtain the list of parent type classes.

\ToDo{Ordering should not be based on user-provided identifiers,
because they may be different to the real ones (for example, when
renamed imports are used). In fact, this function should not be
resposible of identifiers ordering.}
\begin{lstlisting}

> parentTypeClassesDecl :: Decl -> [QualIdent]
> parentTypeClassesDecl (TypeClassDecl _ ctx _ _ _) = sort $ tcUsedBy ctx

> parentTypeClassesIDecl :: IDecl -> [QualIdent]
> parentTypeClassesIDecl (ITypeClassDecl _ ctx _ _ _) = sort $ tcUsedBy ctx

\end{lstlisting}
Obtain the list of methods.
\begin{lstlisting}

> methodsOfTypeClassDecl :: Decl -> [Ident]
> methodsOfTypeClassDecl typeClassDecl =
>   [m | d <- (declsOfTypeClassDecl typeClassDecl), isTypeSig d, m <- bv d]

> declsOfTypeClassDecl :: Decl -> [Decl]
> declsOfTypeClassDecl (TypeClassDecl _ _ _ _ metDs) = metDs

\end{lstlisting}
\codeparagraph{Errors}
\begin{lstlisting}

> recursiveDependency :: [Decl] -> String
> recursiveDependency decls =
>   "Recursive dependency graph for type classes " ++
>   (show $ concat $ map tcDefinedBy decls)

> duplicateTypeSig :: Ident -> String
> duplicateTypeSig v = "More than one type signature for " ++ name v

> typeClassVariableNotPresent :: String
> typeClassVariableNotPresent =
>   "Type class variable must appear in method signature"

> defaultMethodsNotSupported :: String
> defaultMethodsNotSupported =
>   "Default methods are not supported in this version"

> evalAnnotNotAllowed :: String
> evalAnnotNotAllowed = "Eval annotations are not allowed in type class declarations"

\end{lstlisting}
