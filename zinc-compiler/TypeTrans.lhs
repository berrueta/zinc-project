% $Id: TypeTrans.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2003-2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TypeTrans.lhs}
\codesection{Type representation transformations}
\begin{lstlisting}

> module TypeTrans(toType,toTypes,toTypeWithContext,
>                  fromType,fromTypeWithContext,fromTypeContext,
>                  ContainsQualIdents(..),
>                  ppType,ppTypeScheme,ppTypeWithContext,
>                  nameSigTypeWithContext,nameSigType) where
> import Ident
> import Types
> import CurryPP(Doc,ppTypeExpr,ppTypeExprWithContext)
> import List(nub)
> import Map(FM,fromListFM,lookupFM)
> import Expr
> import Error(internalError)
> import TypeExpr

\end{lstlisting}
The functions \texttt{toType}, \texttt{toTypes}, and \texttt{fromType}
convert Curry type expressions into types and vice versa.

When Curry type expression are converted with \texttt{toType} or
\texttt{toTypes}, type variables are assigned ascending indices in the
order of their occurrence. It is possible to pass a list of additional
type variables to both functions which are assigned indices before
those variables occurring in the type. This allows preserving the
order of type variables in the left hand side of a type declaration.
\begin{lstlisting}

> toType :: [Ident] -> TypeExpr -> Type
> toType tvs ty = toType' (fromListFM (zip (tvs ++ tvs') [0..])) ty
>   where tvs' = [tv | tv <- nub (fv ty), tv `notElem` tvs]

> toTypes :: [Ident] -> [TypeExpr] -> [Type]
> toTypes tvs tys = map (toType' (fromListFM (zip (tvs ++ tvs') [0..]))) tys
>   where tvs' = [tv | tv <- nub (concatMap fv tys), tv `notElem` tvs]

> toType' :: FM Ident Int -> TypeExpr -> Type
> toType' tvs (TypeExprConstructor tc) =
>   TypeConstructor tc
> toType' tvs (TypeExprVariable tv) =
>   maybe (internalError ("toType " ++ show tv)) TypeVariable (lookupFM tv tvs)
> toType' tvs (TypeExprApplication tyexpr1 tyexpr2) =
>   TypeApplication (toType' tvs tyexpr1) (toType' tvs tyexpr2)

> toTypeWithContext :: [Ident] -> TypeExprWithContext -> TypeWithContext
> toTypeWithContext tvs (TypeExprWithContext (TypeExprContext classPreds) ty) =
>   TypeWithContext (TypeContext classPreds') ty'
>   where tvs' = [tv | tv <- nub (fv ty), tv `notElem` tvs]
>         fm = fromListFM (zip (tvs ++ tvs') [0..])
>         ty' = toType' fm ty
>         classPreds' = map (toTypeClassConstraint fm) classPreds
>         toTypeClassConstraint fm (TypeExprClassConstraint tc ty) =
>           TypeClassConstraint tc (toType' fm ty)

\end{lstlisting}
The function \texttt{fromType} does the reverse task, i.e., transforms
types into type expressions.
\begin{lstlisting}

> fromType :: Type -> TypeExpr
> fromType (TypeConstructor tc) =
>   TypeExprConstructor tc
> fromType (TypeVariable tv) =
>   TypeExprVariable (if tv >= 0 then nameSupply !! tv
>                                else mkIdent ('_' : show (-tv)))
> fromType (TypeConstrained tys _) = fromType (head tys)
> fromType (TypeSkolem k) = TypeExprVariable (mkIdent ("_?" ++ show k))
> fromType (TypeApplication ty1 ty2) =
>   TypeExprApplication (fromType ty1) (fromType ty2)

> fromTypeWithContext :: TypeWithContext -> TypeExprWithContext
> fromTypeWithContext (TypeWithContext ctx ty) =
>   TypeExprWithContext ctx' ty'
>   where ctx' = fromTypeContext ctx
>         ty' = fromType ty

> fromTypeContext :: TypeContext -> TypeExprContext
> fromTypeContext (TypeContext classPreds) =
>   (TypeExprContext $ map fromTypeClassConstraint classPreds)

> fromTypeClassConstraint :: TypeClassConstraint -> TypeExprClassConstraint
> fromTypeClassConstraint (TypeClassConstraint tc ty) =
>   TypeExprClassConstraint tc (fromType ty)

\end{lstlisting}
The functions \texttt{qualifyIdentsIn} and
\texttt{unqualifyIdentsIn} add and remove module qualifiers in a
type, respectively.
\begin{lstlisting}

> class ContainsQualIdents a where
>   qualifyIdentsIn   :: ModuleIdent -> a -> a
>   unqualifyIdentsIn :: ModuleIdent -> a -> a

> instance ContainsQualIdents a => ContainsQualIdents [a] where
>   qualifyIdentsIn m = map (qualifyIdentsIn m)
>   unqualifyIdentsIn m = map (unqualifyIdentsIn m)

> instance ContainsQualIdents Type where
> 
>   qualifyIdentsIn m (TypeConstructor tc) =
>     TypeConstructor (qualQualify m tc)
>   qualifyIdentsIn _ (TypeVariable tv) = TypeVariable tv
>   qualifyIdentsIn m (TypeConstrained tys tv) =
>     TypeConstrained (qualifyIdentsIn m tys) tv
>   qualifyIdentsIn _ (TypeSkolem k) = TypeSkolem k
>   qualifyIdentsIn m (TypeApplication ty1 ty2) =
>     TypeApplication (qualifyIdentsIn m ty1) (qualifyIdentsIn m ty2)
>
>   unqualifyIdentsIn m (TypeConstructor tc) =
>     TypeConstructor (qualUnqualify m tc)
>   unqualifyIdentsIn _ (TypeVariable tv) = TypeVariable tv
>   unqualifyIdentsIn m (TypeConstrained tys tv) =
>     TypeConstrained (unqualifyIdentsIn m tys) tv
>   unqualifyIdentsIn m (TypeSkolem k) = TypeSkolem k
>   unqualifyIdentsIn m (TypeApplication ty1 ty2) =
>     TypeApplication (unqualifyIdentsIn m ty1) (unqualifyIdentsIn m ty2)

> instance ContainsQualIdents TypeWithContext where
>   qualifyIdentsIn m (TypeWithContext ctx ty) =
>     TypeWithContext (qualifyIdentsIn m ctx)
>                     (qualifyIdentsIn m ty)
>   unqualifyIdentsIn m (TypeWithContext ctx ty) =
>     TypeWithContext (unqualifyIdentsIn m ctx)
>                     (unqualifyIdentsIn m ty)

> instance ContainsQualIdents TypeContext where
>   qualifyIdentsIn m (TypeContext classPreds) =
>     TypeContext (qualifyIdentsIn m classPreds)
>   unqualifyIdentsIn m (TypeContext classPreds) =
>     TypeContext (unqualifyIdentsIn m classPreds)

> instance ContainsQualIdents TypeClassConstraint where
>   qualifyIdentsIn m (TypeClassConstraint tc ty) =
>     TypeClassConstraint (qualQualify m tc)
>                         (qualifyIdentsIn m ty)
>   unqualifyIdentsIn m (TypeClassConstraint tc ty) =
>     TypeClassConstraint (qualUnqualify m tc)
>                         (qualifyIdentsIn m ty)

> instance ContainsQualIdents TypeExprContext where
>   qualifyIdentsIn m (TypeExprContext classPreds) =
>     TypeExprContext (qualifyIdentsIn m classPreds)
>   unqualifyIdentsIn m (TypeExprContext classPreds) =
>     TypeExprContext (unqualifyIdentsIn m classPreds)

> instance ContainsQualIdents TypeExprWithContext where
>   qualifyIdentsIn m (TypeExprWithContext ctx tyexpr) =
>     TypeExprWithContext (qualifyIdentsIn m ctx)
>                         (qualifyIdentsIn m tyexpr)
>   unqualifyIdentsIn m (TypeExprWithContext ctx tyexpr) =
>     TypeExprWithContext (unqualifyIdentsIn m ctx)
>		          (unqualifyIdentsIn m tyexpr)

> instance ContainsQualIdents TypeExprClassConstraint where
>   qualifyIdentsIn m (TypeExprClassConstraint tc tv) =
>     TypeExprClassConstraint (qualQualify m tc) tv
>   unqualifyIdentsIn m (TypeExprClassConstraint tc tv) =
>     TypeExprClassConstraint (qualUnqualify m tc) tv

\end{lstlisting}
The same on type expressions.
\begin{lstlisting}

> instance ContainsQualIdents TypeExpr where
>
>   qualifyIdentsIn m (TypeExprConstructor tc) =
>     TypeExprConstructor (qualQualify m tc)
>   qualifyIdentsIn m (TypeExprVariable tv) =
>     TypeExprVariable tv
>   qualifyIdentsIn m (TypeExprApplication ty1 ty2) =
>     TypeExprApplication (qualifyIdentsIn m ty1) (qualifyIdentsIn m ty2)
>
>   unqualifyIdentsIn m (TypeExprConstructor tc) =
>     TypeExprConstructor (qualUnqualify m tc)
>   unqualifyIdentsIn m (TypeExprVariable tv) =
>     TypeExprVariable tv
>   unqualifyIdentsIn m (TypeExprApplication ty1 ty2) =
>     TypeExprApplication (unqualifyIdentsIn m ty1) (unqualifyIdentsIn m ty2)

\end{lstlisting}
The following functions implement pretty-printing for types.
\begin{lstlisting}

> ppType :: ModuleIdent -> Type -> Doc
> ppType m = ppTypeExpr 0 . fromType . unqualifyIdentsIn m

> ppTypeScheme :: ModuleIdent -> TypeScheme -> Doc
> ppTypeScheme m (ForAll _ ty) = ppTypeWithContext m ty

> ppTypeWithContext :: ModuleIdent -> TypeWithContext -> Doc
> ppTypeWithContext m = ppTypeExprWithContext . fromTypeWithContext .
>                       unqualifyIdentsIn m

\end{lstlisting}
Substitution anonymous type variables by fresh named variables.
\begin{lstlisting}

> nameSigTypeWithContext :: TypeExprWithContext -> TypeExprWithContext
> nameSigTypeWithContext (TypeExprWithContext ctx tyexpr) =
>   TypeExprWithContext ctx (nameSigType tyexpr)

> nameSigType :: TypeExpr -> TypeExpr
> nameSigType ty = fst (nameType ty (filter (`notElem` fv ty) nameSupply))

> nameTypes :: [TypeExpr] -> [Ident] -> ([TypeExpr],[Ident])
> nameTypes (ty:tys) tvs = (ty':tys',tvs'')
>   where (ty',tvs') = nameType ty tvs
>         (tys',tvs'') = nameTypes tys tvs'
> nameTypes [] tvs = ([],tvs)

> nameType :: TypeExpr -> [Ident] -> (TypeExpr,[Ident])
> nameType (TypeExprConstructor tc) tvs = (TypeExprConstructor tc,tvs)
> nameType (TypeExprVariable tv) (tv':tvs)
>   | tv == anonId = (TypeExprVariable tv',tvs)
>   | otherwise = (TypeExprVariable tv,tv':tvs)
> nameType (TypeExprApplication ty1 ty2) tvs =
>   (TypeExprApplication ty1' ty2',tvs'')
>   where (ty1',tvs' ) = nameType ty1 tvs
>         (ty2',tvs'') = nameType ty2 tvs'
        
\end{lstlisting}
