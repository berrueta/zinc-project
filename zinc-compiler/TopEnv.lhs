% $Id: TopEnv.lhs 977 2004-08-08 14:28:54Z berrueta $
%
% Copyright (c) 1999-2003, Wolfgang Lux
% Copyright (c) 2003-2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{TopEnv.lhs}
\codesection{Top-Level Environments}\label{sec:toplevel-env}
The module \texttt{TopEnv} implements environments for qualified and
possibly ambiguous identifiers. An identifier is ambiguous if two
different entities are imported under the same name or if a local
definition uses the same name as an imported entity. Following an idea
presented in \cite{DiatchkiJonesHallgren02:ModuleSystem}, an
identifier is associated with a list of entities in order to handle
ambiguous names properly.

In general, two entities are considered equal if the names of their
original definitions match.  However, in the case of algebraic data
types it is possible to hide some or all of their data constructors on
import and export, respectively. In this case we have to merge both
imports such that all data constructors which are visible through any
import path are visible in the current module. The class
\texttt{Entity} is used to handle this merge.

The code in this module ensures that the list of entities returned by
the functions \texttt{lookupTopEnv} and \texttt{qualLookupTopEnv}
contains exactly one element for each imported entity regardless of
how many times and from which module(s) it was imported. Thus, the
result of these function is a list with exactly one element if and
only if the identifier is unambiguous. The module names associated
with an imported entity identify the modules from which the entity was
imported.
\begin{lstlisting}

> module TopEnv(TopEnv, Entity(..), emptyTopEnv,
>               bindPredefTopEnv,
>               bindImportTopEnv   ,qualBindImportTopEnv,
>               bindLocalTopEnv    ,qualBindLocalTopEnv,
>               rebindLocalTopEnv  ,qualRebindLocalTopEnv,
>               unbindLocalTopEnv,
>               lookupTopEnv,qualLookupTopEnv,
>               allImports,moduleImports,
>               allBindings,localBindings,
>               ppTopEnv) where
> import Env
> import Ident
> import Maybe
> import Utils
> import Pretty
> import CurryPP(ppQIdent)

> data Source = Local
>             | Import [ModuleIdent]
>             deriving (Eq,Show)

> class Entity a where
>  origName :: a -> QualIdent
>  merge    :: a -> a -> Maybe a
>  merge x y
>    | origName x == origName y = Just x
>    | otherwise                = Nothing

> newtype TopEnv a = TopEnv (Env QualIdent [(Source,a)])
>                  deriving (Show, Eq)

> instance Functor TopEnv where
>   fmap f (TopEnv env) = TopEnv (fmap (map (apSnd f)) env)

> emptyTopEnv :: TopEnv a
> emptyTopEnv = TopEnv emptyEnv

\end{lstlisting}
Binding predefined entities.
\begin{lstlisting}

> bindPredefTopEnv :: Entity a => QualIdent -> a -> TopEnv a -> TopEnv a
> bindPredefTopEnv x y (TopEnv env) =
>   case lookupEnv x env of
>     Just _  -> error ("internal error: TopEnv.bindPredefTopEnv: " ++ qualName x)
>     Nothing -> TopEnv (bindEnv x [(Import [],y)] env)

\end{lstlisting}
Binding imported entities.
\begin{lstlisting}

> bindImportTopEnv :: Entity a => ModuleIdent -> Ident -> a ->
>                                 TopEnv a -> TopEnv a
> bindImportTopEnv m x y (TopEnv env) =
>   TopEnv (bindEnv x' (mergeImport m y (entities x' env)) env)
>   where x' = qualify x

> qualBindImportTopEnv :: Entity a => ModuleIdent -> Ident -> a ->
>                                     TopEnv a -> TopEnv a
> qualBindImportTopEnv m x y (TopEnv env) =
>   TopEnv (bindEnv x' (mergeImport m y (entities x' env)) env)
>   where x' = qualifyWith m x

\end{lstlisting}
Binding local entities.
\begin{lstlisting}

> bindLocalTopEnv :: Ident -> a -> TopEnv a -> TopEnv a
> bindLocalTopEnv = qualBindLocalTopEnv . qualify

> qualBindLocalTopEnv :: QualIdent -> a -> TopEnv a -> TopEnv a
> qualBindLocalTopEnv qid y (TopEnv env) =
>   TopEnv (bindEnv qid (bindLocal y (entities qid env)) env)
>   where bindLocal x xs
>           | null [x' | (Local,x') <- xs] = (Local,x) : xs
>           | otherwise = error ("internal error: qualBindTopEnv: " ++ show qid)

> rebindLocalTopEnv :: Ident -> a -> TopEnv a -> TopEnv a
> rebindLocalTopEnv = qualRebindLocalTopEnv . qualify

> qualRebindLocalTopEnv :: QualIdent -> a -> TopEnv a -> TopEnv a
> qualRebindLocalTopEnv x y (TopEnv env) =
>   TopEnv (bindEnv x (rebindLocal (entities x env)) env)
>   where rebindLocal [] = error "internal error: qualRebindTopEnv"
>         rebindLocal ((Local    ,_) : ys) = (Local    ,y) : ys
>         rebindLocal ((Import ms,y) : ys) = (Import ms,y) : rebindLocal ys

> unbindLocalTopEnv :: Ident -> TopEnv a -> TopEnv a
> unbindLocalTopEnv x (TopEnv env) =
>   TopEnv (bindEnv x' (unbindLocal (entities x' env)) env)
>   where x' = qualify x
>         unbindLocal [] = error "internal error: unbindTopEnv"
>         unbindLocal ((Local    ,_) : ys) = ys
>         unbindLocal ((Import ms,y) : ys) = (Import ms,y) : unbindLocal ys

\end{lstlisting}
Looking up for entities.
\begin{lstlisting}

> lookupTopEnv :: Ident -> TopEnv a -> [a]
> lookupTopEnv = qualLookupTopEnv . qualify

> qualLookupTopEnv :: QualIdent -> TopEnv a -> [a]
> qualLookupTopEnv x (TopEnv env) = map snd (entities x env)

\end{lstlisting}
Dumping the environment to a list.
\begin{lstlisting}

> allImports :: TopEnv a -> [(QualIdent,a)]
> allImports (TopEnv env) =
>   [(x,y) | (x,ys) <- envToList env, (Import _,y) <- ys]

> unqualBindings :: TopEnv a -> [(Ident,(Source,a))]
> unqualBindings (TopEnv env) =
>   [(x',y) | (x,ys) <- takeWhile (not . isQualified . fst) (envToList env),
>             let x' = unqualify x, y <- ys]

> moduleImports :: ModuleIdent -> TopEnv a -> [(Ident,a)]
> moduleImports m env =
>   [(x,y) | (x,(Import ms,y)) <- unqualBindings env, m `elem` ms]

> allBindings :: TopEnv a -> [(QualIdent,a)]
> allBindings (TopEnv env) =
>   [(x,y) | (x,ys) <- envToList env, (_,y) <- ys]

> localBindings :: TopEnv a -> [(Ident,a)]
> localBindings env = [(x,y) | (x,(Local,y)) <- unqualBindings env]

\end{lstlisting}
Pretty-print the environment.
\begin{lstlisting}

> ppTopEnv :: (Show a) => TopEnv a -> Doc
> ppTopEnv = vcat . map (uncurry ppContent) . topEnvToList
>   where topEnvToList :: TopEnv a -> [(QualIdent,[(Source,a)])]
>         topEnvToList (TopEnv env) = envToList env
>         ppContent :: (Show a) => QualIdent -> [(Source,a)] -> Doc
>         ppContent qid dt = hsep [ppQIdent qid, vcat $ map (text . show) dt]

\end{lstlisting}
Auxiliary functions.
\begin{lstlisting}

> mergeImport :: Entity a => ModuleIdent -> a -> [(Source,a)] -> [(Source,a)]
> mergeImport m x [] = [(Import [m],x)]
> mergeImport m x ((Local,x') : xs) = (Local,x') : mergeImport m x xs
> mergeImport m x ((Import ms,x') : xs) =
>   case merge x x' of
>     Just x'' -> (Import (m:ms),x'') : xs
>     Nothing  -> (Import ms    ,x' ) : mergeImport m x xs

\end{lstlisting}
