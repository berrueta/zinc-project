% $Id: InstanceEnv.lhs 1003 2004-08-14 10:45:18Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{InstanceEnv.lhs}
\codesection{Instance environment}
This module defines the instance environment. It stores information
about what type constructors are instances of a given type class.

This environment has some differences with respect to other
environments, as each key is composed of two qualified identifiers
(the type class identifier and the type constructor identifier).
So the first task is to define a new sort of top environment.
\begin{lstlisting}

> module InstanceEnv where
> import Ident
> import TypeExpr
> import CurrySyntax
> import CurrySyntaxUtils
> import DualTopEnv
> import Error(internalError)
> import List
> import EvalEnv
> import Env

\end{lstlisting}
We specialize the dual top environment to hold information about
instances.
\begin{lstlisting}

> data InstanceInfo =
>    InstanceInfo
>       QualIdent      -- full qualified identifier of the type class
>       QualIdent      -- type constructor
>       ModuleIdent    -- module of declaration
>       [[QualIdent]]  -- context
>       EvalEnv        -- eval annotations
>       [Decl]         -- methods implementation
>    deriving(Show)

> instance DualEntity InstanceInfo where
>   dualEntityOrigName (InstanceInfo qid1 qid2 _ _ _ _) = (qid1,qid2)

> type InstanceEnv = DualTopEnv InstanceInfo

\end{lstlisting}
Basic operations on instance environments.
\begin{lstlisting}

> bindInstance :: QualIdent -> QualIdent -> ModuleIdent -> [[QualIdent]] ->
>                 [(Ident,EvalAnnotation)] -> [Decl] ->
>                 InstanceEnv -> InstanceEnv
> bindInstance typeClass typeConstructor m ctx evalAnnots decls =
>   bindLocalDualTopEnv typeClass typeConstructor tci
>   where tci = InstanceInfo typeClass typeConstructor m ctx evalEnv decls
>         evalEnv :: EvalEnv
>         evalEnv = foldr (uncurry bindEval) emptyEnv evalAnnots

> lookupInstance :: QualIdent -> QualIdent -> InstanceEnv -> [InstanceInfo]
> lookupInstance = lookupDualTopEnv

> hasInstance :: QualIdent -> QualIdent -> InstanceEnv -> Bool
> hasInstance typeClass typeConstructor =
>   not . null . lookupInstance typeClass typeConstructor

> importedInstance :: ModuleIdent -> QualIdent -> QualIdent -> [[QualIdent]] ->
>                     InstanceInfo
> importedInstance m typeClass typeConstr instCtx =
>   InstanceInfo typeClass typeConstr m instCtx emptyEnv []

\end{lstlisting}
Auxiliary functions.
\begin{lstlisting}

> extractTypeConstr :: InstanceInfo -> QualIdent
> extractTypeConstr (InstanceInfo _ tc _ _ _ _) = tc

> noInstanceEnv :: InstanceEnv
> noInstanceEnv = internalError "noInstanceEnv"

\end{lstlisting}
Helper functions to convert between syntactical type expression contexts
and the more concise representation used in the instance environment.
\begin{lstlisting}

> instanceContextToTypeExprContext :: [[QualIdent]] -> [Ident] -> TypeExprContext
> instanceContextToTypeExprContext ctx' tvIdents =
>   TypeExprContext (concat $ map buildClassConstraints (zip ctx' tvIdents))
>   where buildClassConstraints :: ([QualIdent],Ident) -> [TypeExprClassConstraint]
>         buildClassConstraints (tcs,tv) =
>           map ((flip TypeExprClassConstraint) (TypeExprVariable tv)) tcs

> typeExprContextToInstanceContext :: TypeExprContext -> TypeExpr -> [[QualIdent]]
> typeExprContextToInstanceContext (TypeExprContext classConstrs) tyexpr =
>   map (nub . sort . tcUsedBy . predsOfTypeVar classConstrs)
>       (typeExprArguments tyexpr)
>   where predsOfTypeVar :: [TypeExprClassConstraint] -> TypeExpr ->
>                           [TypeExprClassConstraint]
>         predsOfTypeVar classConstrs (TypeExprVariable tv) =
>           filter (\p -> tv `elem` tvUsedBy p) classConstrs

\end{lstlisting}
