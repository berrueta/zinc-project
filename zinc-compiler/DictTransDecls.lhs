% $Id: DictTransDecls.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{DictTransDecls.lhs}
\codesection{Dictionary transformation of declarations and expressions}
\begin{lstlisting}

> module DictTransDecls(dictTransDecls,dictTransGoal) where
> import Ident
> import TypeExpr
> import ValueEnv
> import CurrySyntax
> import TypeClassEnv
> import InstanceEnv
> import Env
> import TopEnv
> import Types
> import Maybe
> import List
> import TypeSubst
> import DictUtils
> import DictParEnv
> import Error
> import Monad
> import Combined
> import RenameState
> import TypingMonad
> import TypeTrans
> import TypeExpansion
> import TypeConstructorEnv
> import qualified Typing

\end{lstlisting}
This module performs the dictionary transformation of functions and
data expressions. It implements an algorithm based on
\cite{peterson93implementing}, with two (main) stages.
\begin{lstlisting}

> dictTransDecls :: TypeClassEnv -> InstanceEnv -> TCEnv -> ValueEnv -> Int ->
>                   [Decl] -> [Decl]
> dictTransDecls typeClassEnv instEnv tcEnv tyEnv initKey decls =
>   decls''
>   where metEnv                = buildMethodEnv typeClassEnv
>         (decls',parEnv,sigma) =
>           run (doInsertPHDecls typeClassEnv tcEnv tyEnv metEnv decls) tyEnv initKey
>         expandedParEnv        = expandParEnv typeClassEnv (substParEnv sigma parEnv)
>         decls''               = resolvePH instEnv expandedParEnv
>                                           sigma noEquationNumber decls'

> dictTransGoal :: TypeClassEnv -> InstanceEnv -> TCEnv -> ValueEnv -> Int ->
>                  Goal -> Goal
> dictTransGoal typeClassEnv instEnv tcEnv tyEnv initKey
>               (Goal p expr decls) =
>   (Goal p' expr'' decls'')
>   where metEnv                = buildMethodEnv typeClassEnv
>         (SimpleRhs p' expr' decls',parEnv,sigma) =
>           run (doInsertPHRhs typeClassEnv tcEnv tyEnv metEnv
>                              (SimpleRhs p expr decls))
>               tyEnv initKey
>         expandedParEnv        = expandParEnv typeClassEnv (substParEnv sigma parEnv)
>         expr''                = resolvePH instEnv expandedParEnv
>                                           sigma noEquationNumber expr'
>         decls''               = resolvePH instEnv expandedParEnv
>                                           sigma noEquationNumber decls'

\end{lstlisting}
Running the typing monad.
\begin{lstlisting}

> run :: TyState a -> ValueEnv -> Int -> a
> run m tyEnv initKey = runTyStateStartIn m tyEnv initKey

> doInsertPHDecls :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> [Decl] ->
>                    TyState ([Decl],ParEnv,TypeSubst)
> doInsertPHDecls typeClassEnv tcEnv tyEnv metEnv decls =
>   do (decls',parEnv) <- insertPHDecls typeClassEnv tcEnv tyEnv metEnv decls
>      sigma <- fetchSt
>      return (decls',parEnv,sigma)

> doInsertPHRhs :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> Rhs ->
>                  TyState (Rhs,ParEnv,TypeSubst)
> doInsertPHRhs typeClassEnv tcEnv tyEnv metEnv rhs =
>   do (rhs',_,parEnv) <- insertPHRhs typeClassEnv tcEnv tyEnv metEnv rhs
>      sigma <- fetchSt
>      return (rhs',parEnv,sigma)

\end{lstlisting}
\codesubsection{Inserting placeholders}
This functions also inserts dictionary parameters,
build the parameter environment and transform type signatures.

Dictionary parameters are generated only for constraints on universally
qualified type variables.

\ToDo{Type signatures are discarded because they cannot be transformed.}
\begin{lstlisting}

> insertPHDecls :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> [Decl] ->
>                  TyState ([Decl],ParEnv)
> insertPHDecls typeClassEnv tcEnv tyEnv metEnv decls =
>   do dpenvs <- mapM (insertPHDecl typeClassEnv tcEnv tyEnv metEnv)
>                     (filter (not . isTypeSig') decls)
>      return $ foldr (\(d,penv) (ds,penvs) -> (d:ds,composeParEnvs penv penvs))
>                     ([],emptyEnv) dpenvs
>   where isTypeSig' (TypeSig _ _ (TypeExprWithContext
>                       (TypeExprContext (_:_)) _)) = True
>         isTypeSig' _               = False

> insertPHDecl :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> Decl ->
>                 TyState (Decl,ParEnv)
> insertPHDecl typeClassEnv tcEnv tyEnv metEnv decl@(FunctionDecl p id eqs) =
>   do (decl' ,tyRhs,parEnv ) <-
>         insertPHFunctionRhs typeClassEnv tcEnv tyEnv metEnv decl
>      (decl'',tyLhs,parEnv') <-
>         insertPHFunctionLhs tyEnv decl'
>      Typing.unify tyLhs tyRhs
>      return (decl'',composeParEnvs parEnv parEnv')
> insertPHDecl typeClassEnv tcEnv tyEnv metEnv (PatternDecl p constrTerm rhs) =
>   do tyLhs <- Typing.argType tyEnv constrTerm
>      (rhs',tyRhs,parEnv') <- insertPHRhs typeClassEnv tcEnv tyEnv metEnv rhs
>      Typing.unify tyLhs tyRhs
>      return (PatternDecl p constrTerm rhs',parEnv')
> insertPHDecl typeClassEnv tcEnv tyEnv metEnv decl =
>   return (decl,emptyEnv)

> insertPHFunctionLhs :: ValueEnv -> Decl ->
>                        TyState (Decl,Type,ParEnv)
> insertPHFunctionLhs tyEnv decl@(FunctionDecl p id eqs) =
>   do tyWC   <- Typing.instUnivTypeWithContext
>              $ mapTypeScheme (mapTypeContext removeExistTypeClassConstraints)
>              $ Typing.varType id tyEnv
>      key    <- liftSt $ updateSt (1 +)
>      let parEnv = buildParEnv (typeContext tyWC) key
>      let decl'  = insertDictArgsDecl parEnv decl
>      let ty     = removeTypeContext tyWC
>      return (decl',ty,parEnv)

> insertDictArgsDecl :: ParEnv -> Decl -> Decl
> insertDictArgsDecl parEnv (FunctionDecl p id eqs) =
>   FunctionDecl p id (zipWith (insertDictArgsEquation parEnv) [1..] eqs)

> insertDictArgsEquation :: ParEnv -> Int -> Equation -> Equation
> insertDictArgsEquation parEnv n (Equation p lhs rhs) =
>   Equation p (insertDictArgsLhs parEnv n lhs) rhs

> insertDictArgsLhs :: ParEnv -> Int -> Lhs -> Lhs
> insertDictArgsLhs parEnv n (FunLhs f ts) =
>   FunLhs f (dictParConstrTerms parEnv n ++ ts)
> insertDictArgsLhs parEnv n (OpLhs constr1 op constr2) =
>   if null extraTs then OpLhs constr1 op constr2
>                   else FunLhs op (extraTs ++ [constr1,constr2])
>   where extraTs = dictParConstrTerms parEnv n
> insertDictArgsLhs parEnv n (ApLhs lhs ts) =
>   ApLhs (insertDictArgsLhs parEnv n lhs) ts

> insertPHFunctionRhs :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> Decl ->
>                        TyState (Decl,Type,ParEnv)
> insertPHFunctionRhs typeClassEnv tcEnv tyEnv metEnv (FunctionDecl p id eqs) =
>   do (eqs',tys,parEnvs) <-
>        mapM (insertPHEquation typeClassEnv tcEnv tyEnv metEnv) eqs >>=
>        (return . unzip3)
>      ty <- Typing.freshTypeVar
>      mapM_ (Typing.unify ty) tys
>      let parEnv = foldr composeParEnvs emptyEnv parEnvs
>      return (FunctionDecl p id eqs',ty,parEnv)

> insertPHEquation :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> Equation ->
>                     TyState (Equation,Type,ParEnv)
> insertPHEquation typeClassEnv tcEnv tyEnv metEnv (Equation p lhs rhs) =
>   do
>     tys <- insertPHLhs tyEnv lhs
>     (rhs',ty,parEnv) <- insertPHRhs typeClassEnv tcEnv tyEnv metEnv rhs
>     return (Equation p lhs rhs',foldr typeArrow ty tys,parEnv)

> insertPHLhs :: ValueEnv -> Lhs -> TyState [Type]
> insertPHLhs tyEnv (FunLhs i ts) =
>   mapM (Typing.argType tyEnv) ts
> insertPHLhs tyEnv (OpLhs constr1 op constr2) =
>   do
>     ty1 <- Typing.argType tyEnv constr1
>     ty2 <- Typing.argType tyEnv constr2
>     return [ty1,ty2]
> insertPHLhs tyEnv (ApLhs lhs ts) =
>   do
>     tys  <- insertPHLhs tyEnv lhs
>     tys' <- mapM (Typing.argType tyEnv) ts
>     return (tys ++ tys')

> insertPHRhs :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> Rhs ->
>                TyState (Rhs,Type,ParEnv)
> insertPHRhs typeClassEnv tcEnv tyEnv metEnv (SimpleRhs p e ds) =
>   do
>     (ds',  parEnv)  <- insertPHDecls typeClassEnv tcEnv tyEnv metEnv ds
>     (e',ty,parEnv') <- insertPHExpr  typeClassEnv tcEnv tyEnv metEnv e
>     return (SimpleRhs p e' ds',ty,composeParEnvs parEnv parEnv')
> insertPHRhs typeClassEnv tcEnv tyEnv metEnv (GuardedRhs es ds) =
>   do
>     (es',ty,parEnv)  <- insertPHCondExprs typeClassEnv tcEnv tyEnv metEnv es
>     (ds',   parEnv') <- insertPHDecls typeClassEnv tcEnv tyEnv metEnv ds
>     return (GuardedRhs es' ds',ty,composeParEnvs parEnv parEnv')

> insertPHCondExprs :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> [CondExpr] ->
>                      TyState ([CondExpr],Type,ParEnv)
> insertPHCondExprs typeClassEnv tcEnv tyEnv metEnv es =
>   do gty <- if length es > 1 then return boolType
>                              else Typing.freshTypeVar
>      ty  <- Typing.freshTypeVar
>      (es',parEnvs) <- mapM (insertPHCondExpr gty ty) es >>= (return . unzip)
>      let parEnv = foldr composeParEnvs emptyEnv parEnvs
>      return (es',ty,parEnv)
>   where insertPHCondExpr gty ty (CondExpr p guard expr) =
>           do (guard',gty',gParEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv guard
>              Typing.unify gty gty'
>              (expr',ty',eParEnv)   <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>              Typing.unify ty ty'
>              return (CondExpr p guard' expr',composeParEnvs gParEnv eParEnv)

> insertPHExpr :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> Expression ->
>                 TyState (Expression,Type,ParEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Literal l) =
>   do ty <- Typing.litType tyEnv l
>      return (Literal l,ty,emptyEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Variable v) =
>   do (expr,ty) <- dictTransVariable typeClassEnv tyEnv metEnv v
>      return (expr,ty,emptyEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Constructor c) =
>   do ty <- Typing.instUnivExist (Typing.constrType c tyEnv)
>      return (Constructor c,ty,emptyEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Paren expr) =
>   insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Typed expr sig) =
>   do (expr',ty,parEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      Typing.instUnivTypeWithContext declSigma >>= Typing.unify ty . removeTypeContext
>      return (Typed expr' sig,ty,parEnv)
>   where sig' = nameSigTypeWithContext sig
>         declSigma = expandPolyType tcEnv sig'
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Tuple exprs)
>   | null exprs = return (Tuple exprs,unitType,emptyEnv)
>   | otherwise =
>       do (exprs',tys,parEnvs) <-
>             mapM (insertPHExpr typeClassEnv tcEnv tyEnv metEnv) exprs >>=
>             (return . unzip3)
>          let ty = tupleType tys
>          let parEnv = foldr composeParEnvs emptyEnv parEnvs
>          return (Tuple exprs',ty,parEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (List exprs) =
>   do elemTy <- Typing.freshTypeVar
>      (exprs',elemTys,parEnvs) <-
>         mapM (insertPHExpr typeClassEnv tcEnv tyEnv metEnv) exprs >>= (return . unzip3)
>      mapM_ (Typing.unify elemTy) elemTys
>      let parEnv = foldr composeParEnvs emptyEnv parEnvs
>      return (List exprs',listType elemTy,parEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (ListCompr expr quals) =
>   do (expr',exprTy,parEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      (quals',parEnvs) <- mapM (insertPHQual typeClassEnv tcEnv tyEnv metEnv) quals >>=
>                          (return . unzip)
>      return (ListCompr expr' quals',listType exprTy,
>              foldr composeParEnvs parEnv parEnvs)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (EnumFrom expr1) =
>   do (expr1',ty1,parEnv1) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr1
>      Typing.unify ty1 intType
>      return (EnumFrom expr1',listType ty1,parEnv1)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (EnumFromThen expr1 expr2) =
>   do (expr1',ty1,parEnv1) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr1
>      (expr2',ty2,parEnv2) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr2
>      mapM_ (Typing.unify intType) [ty1,ty2]
>      let parEnv = foldr composeParEnvs emptyEnv [parEnv1,parEnv2]
>      return (EnumFromThen expr1' expr2',listType ty1,parEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (EnumFromTo expr1 expr2) =
>   do (expr1',ty1,parEnv1) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr1
>      (expr2',ty2,parEnv2) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr2
>      mapM_ (Typing.unify intType) [ty1,ty2]
>      let parEnv = foldr composeParEnvs emptyEnv [parEnv1,parEnv2]
>      return (EnumFromTo expr1' expr2',listType ty1,parEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (EnumFromThenTo expr1 expr2 expr3) =
>   do (expr1',ty1,parEnv1) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr1
>      (expr2',ty2,parEnv2) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr2
>      (expr3',ty3,parEnv3) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr3
>      mapM_ (Typing.unify intType) [ty1,ty2,ty3]
>      let parEnv = foldr composeParEnvs emptyEnv [parEnv1,parEnv2,parEnv3]
>      return (EnumFromThenTo expr1' expr2' expr3',listType ty1,parEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (UnaryMinus op expr) =
>   do (expr',ty,parEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      Typing.unify ty opType
>      return (UnaryMinus op expr',opType,parEnv)
>   where opType
>           | op == minusId = intType
>           | op == fminusId = floatType
>           | otherwise = internalError ("exprType unary " ++ name op)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Apply expr1 expr2) =
>   do (expr1',ty1,parEnv ) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr1
>      (expr2',ty2,parEnv') <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr2
>      (tya,tyb)            <- Typing.unifyArrow ty1
>      Typing.unify tya ty2
>      return (Apply expr1' expr2',tyb,composeParEnvs parEnv parEnv')
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (InfixApply expr1 op expr2) =
>   do (expr',ty,parEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv
>                             (Apply (Apply (infixOp op) expr1) expr2)
>      return (restoreInfixApply expr',ty,parEnv)
>   where restoreInfixApply (Apply (Apply (Variable v) e1) e2) =
>           InfixApply e1 (InfixOp v) e2
>         restoreInfixApply (Apply (Apply (Constructor c) e1) e2) =
>           InfixApply e1 (InfixConstr c) e2
>         restoreInfixApply e = e
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (LeftSection expr op) =
>   do (expr',ty,parEnv) <-
>         insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Apply (infixOp op) expr)
>      return (restoreLeftSection expr',ty,parEnv)
>   where restoreLeftSection (Apply (Variable v) e) =
>           LeftSection e (InfixOp v)
>         restoreLeftSection (Apply (Constructor c) e) =
>           LeftSection e (InfixConstr c)
>         restoreLeftSection e = e
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (RightSection op expr) =
>   do (expr',ty,parEnv) <-
>         insertPHExpr typeClassEnv tcEnv tyEnv metEnv flippedExpr
>      return (expr',ty,parEnv)
>   where flippedExpr = Apply (Apply (Variable flipFun) (infixOp op)) expr
>         flipFun = qualifyWith preludeMIdent $ mkIdent "flip"
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Lambda constrTerms expr) =
>   do tys <- mapM (Typing.argType tyEnv) constrTerms
>      (expr',ty,parEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      return (Lambda constrTerms expr',foldr typeArrow ty tys,parEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Let decls expr) =
>   do (decls',parEnv1) <- insertPHDecls typeClassEnv tcEnv tyEnv metEnv decls
>      (expr',ty,parEnv2) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      return (Let decls' expr',ty,composeParEnvs parEnv1 parEnv2)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Do stats expr) =
>   do (stats',tys,parEnvs) <- mapM (insertPHStmt typeClassEnv tcEnv tyEnv metEnv) stats >>=
>                              (return . unzip3)
>      (expr',ty,parEnvExpr) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      let parEnv = foldr composeParEnvs parEnvExpr parEnvs
>      return (Do stats' expr',ty,parEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (IfThenElse expr1 expr2 expr3) =
>   do (expr1',ty1,parEnv1) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr1
>      (expr2',ty2,parEnv2) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr2
>      (expr3',ty3,parEnv3) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr3
>      Typing.unify ty1 boolType
>      Typing.unify ty2 ty3
>      let parEnv = foldr composeParEnvs emptyEnv [parEnv1,parEnv2,parEnv3]
>      return (IfThenElse expr1' expr2' expr3',ty3,parEnv)
> insertPHExpr typeClassEnv tcEnv tyEnv metEnv (Case expr alts) =
>   do (expr',exprTy,exprParEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      altsTy <- Typing.freshTypeVar
>      (alts',altParEnvs) <- mapM (insertPHAlt typeClassEnv tcEnv tyEnv metEnv
>                                              exprTy altsTy) alts >>=
>                            (return . unzip)
>      let parEnv = foldr composeParEnvs exprParEnv altParEnvs
>      return (Case expr' alts',altsTy,parEnv)

> insertPHAlt :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> Type -> Type -> Alt ->
>                TyState (Alt,ParEnv)
> insertPHAlt typeClassEnv tcEnv tyEnv metEnv exprTy altTy (Alt p constrTerm rhs) =
>   do constrTermTy <- Typing.argType tyEnv constrTerm
>      Typing.unify constrTermTy exprTy
>      (rhs',rhsTy,parEnv) <- insertPHRhs typeClassEnv tcEnv tyEnv metEnv rhs
>      Typing.unify altTy rhsTy
>      return (Alt p constrTerm rhs',parEnv)

> insertPHQual :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> Statement ->
>                 TyState (Statement,ParEnv)
> insertPHQual typeClassEnv tcEnv tyEnv metEnv (StmtExpr expr) =
>   do (expr',ty,parEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      Typing.unify ty boolType
>      return (StmtExpr expr',parEnv)
> insertPHQual typeClassEnv tcEnv tyEnv metEnv (StmtBind constrTerm expr) =
>   do constrTermTy <- Typing.argType tyEnv constrTerm
>      (expr',exprTy,parEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      Typing.unify (listType constrTermTy) exprTy
>      return (StmtBind constrTerm expr',parEnv)
> insertPHQual typeClassEnv tcEnv tyEnv metEnv (StmtDecl decls) =
>   do (decls',parEnv) <- insertPHDecls typeClassEnv tcEnv tyEnv metEnv decls
>      return (StmtDecl decls',parEnv)

> insertPHStmt :: TypeClassEnv -> TCEnv -> ValueEnv -> MethodEnv -> Statement ->
>                 TyState (Statement,Type,ParEnv)
> insertPHStmt typeClassEnv tcEnv tyEnv metEnv (StmtExpr expr) =
>   do (expr',exprTy,parEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      ty <- Typing.freshTypeVar
>      Typing.unify (ioType ty) exprTy
>      return (StmtExpr expr',exprTy,parEnv)
> insertPHStmt typeClassEnv tcEnv tyEnv metEnv (StmtBind constrTerm expr) =
>   do constrTermTy <- Typing.argType tyEnv constrTerm
>      (expr',exprTy,parEnv) <- insertPHExpr typeClassEnv tcEnv tyEnv metEnv expr
>      Typing.unify (ioType constrTermTy) exprTy
>      return (StmtBind constrTerm expr',noType,parEnv)
> insertPHStmt typeClassEnv tcEnv tyEnv metEnv (StmtDecl decls) =
>   do (decls',parEnv) <- insertPHDecls typeClassEnv tcEnv tyEnv metEnv decls
>      return (StmtDecl decls',noType,parEnv)

> noType :: Type
> noType = internalError "noType"

\end{lstlisting}
The transformation of expressions only takes place on variables.
But the variables in the expressions must fall in one of these two
categories:

\begin{itemize}
\item Methods.
\item (Possibly) overloaded variables.
\end{itemize}

The function \texttt{dictTransVariable} categorizes the variable and calls
the appropiate function.
\begin{lstlisting}

> dictTransVariable :: TypeClassEnv -> ValueEnv -> MethodEnv -> QualIdent ->
>                      TyState (Expression,Type)
> dictTransVariable typeClassEnv tyEnv metEnv v
>   | isMethod  = dictTransMethod typeClassEnv tyEnv metEnv v typeClass
>   | otherwise = dictTransOverloadedVariable tyEnv metEnv v
>   where isMethod = isJust mi
>         mi = lookupEnv v metEnv
>         typeClass = fromJust mi

\end{lstlisting}
The \texttt{transMethod} performs the translation of a method call.
The variable must be changed by an application of the apropiate
dictionary variable to the selector.
\begin{lstlisting}

> dictTransMethod :: TypeClassEnv -> ValueEnv -> MethodEnv ->
>                    QualIdent -> QualIdent ->
>                    TyState (Expression,Type)
> dictTransMethod typeClassEnv tyEnv metEnv met typeClass =
>   do metTypeWC      <- Typing.instUnivTypeWithContext
>                        (Typing.funType met tyEnv)
>      selectorTypeWC <- Typing.instUnivTypeWithContext
>                          (Typing.funType selectorQId tyEnv)
>      lhsTypeWC      <- Typing.instUnivTypeWithContext
>                          (polyTypeWithContext $ toTypeWithContext []
>                            (dictTypeClassDataLhs typeClassEnv typeClass))
>      (lhsAlpha,lhsBeta) <- Typing.unifyArrow (removeTypeContext selectorTypeWC)
>      Typing.unify (removeTypeContext metTypeWC) lhsBeta
>      Typing.unify (removeTypeContext lhsTypeWC) lhsAlpha
>      let classTyvar = case typeContext metTypeWC of
>                         (TypeContext (TypeClassConstraint _ tv:_)) -> tv
>          classDict = OverloadedVariablePH typeClass classTyvar
>          extraPHs = map genExtraPH (typeClassConstraints $ typeContext lhsTypeWC)
>          genExtraPH (TypeClassConstraint tc tv) =
>            foldl Apply (OverloadedVariablePH tc tv)
>                        (replicate (length $
>                                    typeExprClassConstraints $
>                                    typeExprContext $
>                                    dictTypeClassDataLhs typeClassEnv tc)
>                                   nullDictionary)
>          e  = foldl Apply classDict extraPHs
>          e' = Apply (Variable selectorQId) (Paren e)
>      return (e',removeTypeContext metTypeWC)
>   where selectorQId = dictSelectorQIdent (qualQualify m met)
>         m = fromJust $ fst $ splitQualIdent typeClass

\end{lstlisting}
When an overloaded variable is found, there is no need to perform
a renaming, but the appropiate dictionary variables must be applied
to the overloaded variables.
\begin{lstlisting}

> dictTransOverloadedVariable :: ValueEnv -> MethodEnv -> QualIdent ->
>                                TyState (Expression,Type)
> dictTransOverloadedVariable tyEnv metEnv v =
>   do tyWC <- Typing.instUnivTypeWithContext sigma
>      let e = dictTransExpression (typeClassConstraints $ typeContext tyWC)
>                                  (Variable v)
>      return (e,removeTypeContext tyWC)
>   where sigma = mapTypeScheme (mapTypeContext removeExistTypeClassConstraints)
>               $ Typing.funType v tyEnv 

> dictTransExpression :: [TypeClassConstraint] -> Expression -> Expression
> dictTransExpression tccs expr =
>   foldl Apply expr placeholders
>   where placeholders = map constraintToPH tccs
>         constraintToPH (TypeClassConstraint tc ty) = OverloadedVariablePH tc ty

\end{lstlisting}
\codesubsection{Resolving placeholders}
First, the syntax tree is examined to find placeholders.
\begin{lstlisting}

> class ResolvePHClass a where
>   resolvePH :: InstanceEnv -> ParEnv -> TypeSubst -> Int -> a -> a

> instance ResolvePHClass a => ResolvePHClass [a] where
>   resolvePH instEnv parEnv sigma eqNum = map (resolvePH instEnv parEnv sigma eqNum)

> instance ResolvePHClass Decl where
>   resolvePH instEnv parEnv sigma eqNum (FunctionDecl p id eqs) =
>     FunctionDecl p id (map (uncurry (resolvePH instEnv parEnv sigma))
>                            (zip [1..] eqs))
>   resolvePH instEnv parEnv sigma eqNum (PatternDecl p constrTerms rhs) =
>     PatternDecl p constrTerms (resolvePH instEnv parEnv sigma eqNum rhs)
>   resolvePH instEnv parEnv sigma eqNum decl = decl

> instance ResolvePHClass Equation where
>   resolvePH instEnv parEnv sigma eqNum (Equation p constrTerms rhs) =
>     Equation p constrTerms (resolvePH instEnv parEnv sigma eqNum rhs)

> instance ResolvePHClass Rhs where
>   resolvePH instEnv parEnv sigma eqNum (SimpleRhs p expr decls) =
>     SimpleRhs p (resolvePH instEnv parEnv sigma eqNum expr)
>                 (resolvePH instEnv parEnv sigma eqNum decls)
>   resolvePH instEnv parEnv sigma eqNum (GuardedRhs condExprs decls) =
>     GuardedRhs (resolvePH instEnv parEnv sigma eqNum condExprs)
>                (resolvePH instEnv parEnv sigma eqNum decls)

> instance ResolvePHClass CondExpr where
>   resolvePH instEnv parEnv sigma eqNum (CondExpr p expr1 expr2) =
>     CondExpr p (resolvePH instEnv parEnv sigma eqNum expr1)
>                (resolvePH instEnv parEnv sigma eqNum expr2)

> instance ResolvePHClass Expression where
>   resolvePH instEnv parEnv sigma eqNum (Literal l) = Literal l
>   resolvePH instEnv parEnv sigma eqNum (Variable v) = Variable v
>   resolvePH instEnv parEnv sigma eqNum (Constructor c) = Constructor c
>   resolvePH instEnv parEnv sigma eqNum (Paren expr) =
>     Paren (resolvePH instEnv parEnv sigma eqNum expr)
>   resolvePH instEnv parEnv sigma eqNum (Typed expr tyexprWC) =
>     Typed (resolvePH instEnv parEnv sigma eqNum expr) tyexprWC
>   resolvePH instEnv parEnv sigma eqNum (Tuple exprs) =
>     Tuple (resolvePH instEnv parEnv sigma eqNum exprs)
>   resolvePH instEnv parEnv sigma eqNum (List exprs) =
>     List (resolvePH instEnv parEnv sigma eqNum exprs)
>   resolvePH instEnv parEnv sigma eqNum (ListCompr expr stats) =
>     ListCompr (resolvePH instEnv parEnv sigma eqNum expr)
>               (resolvePH instEnv parEnv sigma eqNum stats)
>   resolvePH instEnv parEnv sigma eqNum (EnumFrom expr) =
>     EnumFrom (resolvePH instEnv parEnv sigma eqNum expr)
>   resolvePH instEnv parEnv sigma eqNum (EnumFromThen expr1 expr2) =
>     EnumFromThen (resolvePH instEnv parEnv sigma eqNum expr1)
>                  (resolvePH instEnv parEnv sigma eqNum expr2)
>   resolvePH instEnv parEnv sigma eqNum (EnumFromTo expr1 expr2) =
>     EnumFromTo (resolvePH instEnv parEnv sigma eqNum expr1)
>                (resolvePH instEnv parEnv sigma eqNum expr2)
>   resolvePH instEnv parEnv sigma eqNum (EnumFromThenTo expr1 expr2 expr3) =
>     EnumFromThenTo (resolvePH instEnv parEnv sigma eqNum expr1)
>                    (resolvePH instEnv parEnv sigma eqNum expr2)
>                    (resolvePH instEnv parEnv sigma eqNum expr3)
>   resolvePH instEnv parEnv sigma eqNum (UnaryMinus id expr) =
>     UnaryMinus id (resolvePH instEnv parEnv sigma eqNum expr)
>   resolvePH instEnv parEnv sigma eqNum (Apply expr1 expr2) =
>     Apply (resolvePH instEnv parEnv sigma eqNum expr1)
>           (resolvePH instEnv parEnv sigma eqNum expr2)
>   resolvePH instEnv parEnv sigma eqNum (InfixApply expr1 op expr2) =
>     InfixApply (resolvePH instEnv parEnv sigma eqNum expr1) op
>                (resolvePH instEnv parEnv sigma eqNum expr2)
>   resolvePH instEnv parEnv sigma eqNum (LeftSection expr op) =
>     LeftSection (resolvePH instEnv parEnv sigma eqNum expr) op
>   resolvePH instEnv parEnv sigma eqNum (RightSection op expr) =
>     RightSection op (resolvePH instEnv parEnv sigma eqNum expr)
>   resolvePH instEnv parEnv sigma eqNum (Lambda constrTerms expr) =
>     Lambda constrTerms (resolvePH instEnv parEnv sigma eqNum expr)
>   resolvePH instEnv parEnv sigma eqNum (Let decls expr) =
>     Let (resolvePH instEnv parEnv sigma eqNum decls)
>         (resolvePH instEnv parEnv sigma eqNum expr)
>   resolvePH instEnv parEnv sigma eqNum (Do stats expr) =
>     Do (resolvePH instEnv parEnv sigma eqNum stats)
>        (resolvePH instEnv parEnv sigma eqNum expr)
>   resolvePH instEnv parEnv sigma eqNum (IfThenElse expr1 expr2 expr3) =
>     IfThenElse (resolvePH instEnv parEnv sigma eqNum expr1)
>                (resolvePH instEnv parEnv sigma eqNum expr2)
>                (resolvePH instEnv parEnv sigma eqNum expr3)
>   resolvePH instEnv parEnv sigma eqNum (Case expr alts) =
>     Case (resolvePH instEnv parEnv sigma eqNum expr)
>          (resolvePH instEnv parEnv sigma eqNum alts)
>   resolvePH instEnv parEnv sigma eqNum (OverloadedVariablePH typeClass ty) =
>     resolveOverloadedVariablePH instEnv parEnv eqNum typeClass (subst sigma ty)

> instance ResolvePHClass Statement where
>   resolvePH instEnv parEnv sigma eqNum (StmtExpr expr) =
>     StmtExpr (resolvePH instEnv parEnv sigma eqNum expr)
>   resolvePH instEnv parEnv sigma eqNum (StmtDecl decls) =
>     StmtDecl (resolvePH instEnv parEnv sigma eqNum decls)
>   resolvePH instEnv parEnv sigma eqNum (StmtBind constrTerm expr) =
>     StmtBind constrTerm (resolvePH instEnv parEnv sigma eqNum expr)

> instance ResolvePHClass Alt where
>   resolvePH instEnv parEnv sigma eqNum (Alt p constrTerm rhs) =
>     Alt p constrTerm (resolvePH instEnv parEnv sigma eqNum rhs)

\end{lstlisting}
For each placeholder, these functions select the appropiate substitution.
It must be determined if the type expression is a type constructor
application or a type variable.

Some optimization can be done here.
\begin{lstlisting}

> resolveOverloadedVariablePH :: InstanceEnv -> ParEnv -> Int ->
>                                QualIdent -> Type -> Expression
> resolveOverloadedVariablePH instEnv parEnv eqNum typeClass ty =
>   case leftmostType ty of
>     TypeConstructor tc -> dictExprTypeConstructor instEnv parEnv eqNum typeClass
>                                                   tc (typeArguments ty)
>     TypeVariable tv    -> dictExprTypeVariable parEnv eqNum typeClass tv

> dictExprTypeConstructor :: InstanceEnv -> ParEnv -> Int -> QualIdent ->
>                            QualIdent -> [Type] -> Expression
> dictExprTypeConstructor instEnv parEnv eqNum typeClass typeConstr tys =
>   foldl composeExpr instVar constraints
>   where [InstanceInfo _ _ m ctx _ _] =
>           lookupInstance typeClass typeConstr instEnv
>         instVar = Variable $ qualifyWith m $
>                     dictInstanceIdent typeClass typeConstr
>         constraints :: [TypeClassConstraint]
>         constraints = zipExtLWith TypeClassConstraint ctx tys
>         composeExpr e (TypeClassConstraint ctxTC ctxTy) =
>           Apply e (resolveOverloadedVariablePH instEnv parEnv eqNum ctxTC ctxTy)

> dictExprTypeVariable :: ParEnv -> Int -> QualIdent -> Int -> Expression
> dictExprTypeVariable parEnv eqNum typeClass tv = lookupParEnv tcc eqNum parEnv
>   where tcc = TypeClassConstraint typeClass (TypeVariable tv)

> noEquationNumber :: Int
> noEquationNumber = internalError "noEquationNumber"

\end{lstlisting}
\codeparagraph{Method environment}
The method environment maps method names with their type class.
\begin{lstlisting}

> type MethodEnv = Env QualIdent QualIdent

> buildMethodEnv :: TypeClassEnv -> MethodEnv
> buildMethodEnv typeClassEnv = foldr bindMethods emptyEnv typeClassList
>   where typeClassList = allBindings typeClassEnv
>         bindMethods :: (QualIdent,TypeClassInfo) -> MethodEnv -> MethodEnv
>         bindMethods ((qid,TypeClassInfo tc _ _ sigEnv)) metEnv =
>           foldr (bindMet tc (fst $ splitQualIdent qid)) metEnv
>                 (map fst $ envToList sigEnv)
>         bindMet tc m met =
>           bindEnv (maybe (qualify met) ((flip qualifyWith) met) m) tc

\end{lstlisting}
