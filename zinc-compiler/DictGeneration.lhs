% $Id: DictGeneration.lhs 1523 2005-02-09 18:40:22Z berrueta $
%
% Copyright (c) 2004, Diego Berrueta
% See LICENSE for the full license.
%
\nwfilename{DictGeneration.lhs}
\codesection{Dictionary generation}
This module uses the stored information about local type classes and
local instances to generate Curry declarations (as new syntax tree elements,
not source code). This module doesn't perform any binding for the generated
declarations.
\begin{lstlisting}

> module DictGeneration where
> import Ident
> import TypeClassEnv
> import InstanceEnv
> import CurrySyntax
> import TypeExpr
> import DualTopEnv
> import TopEnv
> import Position
> import List
> import DictUtils
> import CurrySyntaxUtils
> import Error(internalError)
> import Maybe
> import EvalEnv
> import TypeConstructorEnv

\end{lstlisting}
The entry point for this module is the function \texttt{genDict}. All
the required information is already stored into the type class and instance
environments.
\begin{lstlisting}

> genDict :: ModuleIdent -> TypeClassEnv -> InstanceEnv -> TCEnv -> [Decl]
> genDict m typeClassEnv instanceEnv tcEnv =
>   (concatMap (genTypeClass m typeClassEnv) localTypeClasses) ++
>   (concatMap (uncurry $ genInstance typeClassEnv instanceEnv tcEnv) localInstances)
>   where localTypeClasses = map fst (localBindings typeClassEnv)
>         localInstances   = map (\(x,y,_) -> (x,y))
>                                (localBindingsDualTopEnv instanceEnv)

\end{lstlisting}
\codesubsection{Dictionary generation for type class declarations}

The methods are sorted by their identifier.
\begin{lstlisting}

> genTypeClass :: ModuleIdent -> TypeClassEnv -> Ident -> [Decl]
> genTypeClass m typeClassEnv typeClass =
>   [genTypeClassData m typeClassEnv typeClass] ++
>   (genTraverserFunctions typeClassEnv qtc superQIds) ++
>   (genSelectorFunctions typeClassEnv qtc superQIds methods)
>   where (TypeClassInfo qtc tv superQIds sigEnv) =
>           head $ lookupTypeClass typeClass typeClassEnv
>         methods = sortMethods sigEnv

\end{lstlisting}
Dictionary datatype declaration.
\begin{lstlisting}

> genTypeClassData :: ModuleIdent -> TypeClassEnv -> Ident -> Decl
> genTypeClassData m typeClassEnv typeClass  =
>   DataDecl genPos tc tvs [ConstrDecl genPos [] dc sigs] []
>   where tc   = dictTypeClassDataIdent typeClass
>         dc   = dictTypeClassDataConstrIdent typeClass
>         (tyexprWC,_) = dictTypeClassDataRhs typeClassEnv (qualifyWith m typeClass)
>         sigs = typeExprArguments (removeTypeExprContext tyexprWC)
>         lhs  = removeTypeExprContext
>              $ dictTypeClassDataLhs typeClassEnv (qualifyWith m typeClass)
>         tvs  = tvUsedBy lhs

\end{lstlisting}
Selector functions.
\begin{lstlisting}

> genSelectorFunctions :: TypeClassEnv -> QualIdent -> [QualIdent] ->
>                         [(Ident,TypeExprWithContext)] -> [Decl]
> genSelectorFunctions typeClassEnv typeClass superQIds methods = 
>   map (uncurry (genSelectorFunctionSig typeClassEnv typeClass)) methods ++
>   map (genSelectorFunction typeClass constrPattern . fst) methods
>   where constrPattern = ConstructorPattern dc patternTvs
>         patternTvs = map (VariablePattern . mkIdent . encodeQualIdent) superQIds ++
>                      map (VariablePattern . fst) methods
>         dc  = dictTypeClassDataConstrQIdent typeClass

> genSelectorFunctionSig :: TypeClassEnv -> QualIdent -> Ident ->
>                           TypeExprWithContext -> Decl
> genSelectorFunctionSig typeClassEnv typeClass method methodTypeExprWC =
>   TypeSig genPos [dictSelectorIdent method]
>     (TypeExprWithContext emptyTypeExprContext
>        (selectorFunctionTypeExpr typeClassEnv typeClass method methodTypeExprWC))

> genSelectorFunction :: QualIdent -> ConstrTerm -> Ident -> Decl
> genSelectorFunction typeClass constrPattern method =
>   FunctionDecl genPos funId [eq]
>   where eq = Equation genPos lhs rhs
>         lhs = FunLhs funId [constrPattern]
>         rhs = SimpleRhs genPos (Variable $ qualify method) []
>         funId = dictSelectorIdent method

\end{lstlisting}
Traverser functions.
\begin{lstlisting}

> genTraverserFunctions :: TypeClassEnv -> QualIdent -> [QualIdent] -> [Decl]
> genTraverserFunctions typeClassEnv typeClass superTypeClasses =
>   map (genTraverserFunctionSig typeClassEnv typeClass) superTypeClasses ++
>   map (genTraverserFunction typeClassEnv typeClass) superTypeClasses

> genTraverserFunctionSig :: TypeClassEnv -> QualIdent -> QualIdent -> Decl
> genTraverserFunctionSig typeClassEnv typeClass superTypeClass =
>   TypeSig genPos [dictTraverserIdent typeClass superTypeClass]
>     (TypeExprWithContext emptyTypeExprContext
>        (traverserFunctionTypeExpr typeClassEnv typeClass superTypeClass))

> genTraverserFunction :: TypeClassEnv -> QualIdent -> QualIdent -> Decl
> genTraverserFunction typeClassEnv typeClass superTypeClass =
>   FunctionDecl genPos funId [eq]
>   where funId = dictTraverserIdent typeClass superTypeClass
>         eq  = Equation genPos lhs rhs
>         lhs = FunLhs funId [ConstructorPattern argQId []]
>         rhs = SimpleRhs genPos expr
>                         [genSimpleTraverserFunction typeClassEnv typeClass
>                                                     superTypeClass]
>         expr = let expr' = Apply
>                              (Variable $ qualify travId)
>                              (Paren
>                                (foldl Apply (Variable argQId)
>                                  (map (Variable . qualify . addId) upVars)))
>                in if (null constrVars)
>                      then expr'
>                      else Lambda (map (VariablePattern . addId) constrVars)
>                                  expr'
>         dataLhs = dictTypeClassDataLhs typeClassEnv typeClass
>         superDataLhs = dictTypeClassDataLhs typeClassEnv superTypeClass
>         addId :: Ident -> Ident
>         addId id = if (id == undefinedId)
>                      then undefinedId
>                      else mkIdent (name id ++ "_" ++ show (uniqueId id))
>         upVars, constrVars :: [Ident]
>         upVars = map (\tcc -> let [tv] = tvUsedBy tcc
>                               in if (tv `elem` constrVars) then tv else undefinedId)
>                      (typeExprClassConstraints $ typeExprContext dataLhs)
>         constrVars = map (\tcc -> let [tv] = tvUsedBy tcc
>                                   in dictTypeVariablePrefix superTypeClass tv)
>                          (typeExprClassConstraints $ typeExprContext superDataLhs)
>         argQId = qualify $ mkIdent "f"

> genSimpleTraverserFunction :: TypeClassEnv -> QualIdent -> QualIdent -> Decl
> genSimpleTraverserFunction typeClassEnv typeClass superTypeClass =
>   FunctionDecl genPos travId [eq]
>   where eq = Equation genPos lhs rhs
>         lhs = FunLhs travId [constrPat]
>         rhs = SimpleRhs genPos (Variable $ qualify xId) []
>         constrPat = ConstructorPattern
>                       (dictTypeClassDataConstrQIdent typeClass)
>                       (map typeExprToConstrPat (typeExprArguments dataRhs))
>         dataRhs = removeTypeExprContext
>                     (fst $ dictTypeClassDataRhs typeClassEnv typeClass)
>         typeExprToConstrPat typeExpr = case leftmostTypeExpr typeExpr of
>           TypeExprConstructor tc
>             | tc == (dictTypeClassDataQIdent superTypeClass) ->
>                VariablePattern xId
>             | otherwise -> VariablePattern anonId
>           _ -> internalError "genTrav"

\end{lstlisting}
\codesubsection{Dictionary generation for instance declarations}

\ToDo{Include a real position on all new syntactical elements, and
in the error message of checkAliasType.}
\begin{lstlisting}

> genInstance :: TypeClassEnv -> InstanceEnv -> TCEnv ->
>                QualIdent -> QualIdent -> [Decl]
> genInstance typeClassEnv instEnv tcEnv typeClass typeConstr
>   | checkAliasType tcEnv typeConstr =
>     [genInstanceValueSig typeClassEnv typeClass typeConstr ctx] ++
>     [genInstanceValue m instEnv typeClass typeConstr sSuperQIds methodIds] ++
>     (genLiftedMethodEvalAnnots typeClass typeConstr methodIds evalEnv) ++
>     (genLiftedMethodSigs instEnv typeClass typeConstr tv methods) ++
>     (genLiftedMethods instEnv typeClass typeConstr methodIds)
>   where TypeClassInfo qtc tv superQIds sigEnv =
>           head $ qualLookupTypeClass typeClass typeClassEnv
>         InstanceInfo _ _ m ctx evalEnv decls =
>           head $ lookupInstance typeClass typeConstr instEnv
>         methods    = sortMethods sigEnv
>         methodIds  = map fst methods
>         methodSigs = map snd methods
>         sSuperQIds = sort superQIds

> checkAliasType :: TCEnv -> QualIdent -> Bool
> checkAliasType tcEnv tc = case qualLookupTC tc tcEnv of
>   [AliasType _ _ _] -> error ("\n" ++ noInstancesForAliasTypes)
>   [_]               -> True

> genInstanceValueSig :: TypeClassEnv -> QualIdent -> QualIdent ->
>                        [[QualIdent]] -> Decl
> genInstanceValueSig typeClassEnv typeClass typeConstr instCtx =
>   TypeSig genPos [dictInstanceIdent typeClass typeConstr]
>             (instanceValueTypeExpr typeClassEnv typeClass typeConstr instCtx)

> genInstanceValue :: ModuleIdent -> InstanceEnv -> QualIdent -> QualIdent ->
>                     [QualIdent] -> [Ident] -> Decl
> genInstanceValue m instEnv typeClass typeConstr superQIds methodIds =
>   FunctionDecl genPos funId [eq]
>   where funId = dictInstanceIdent typeClass typeConstr
>         eq   = Equation genPos lhs rhs
>         lhs  = FunLhs funId []
>         rhs  = SimpleRhs genPos expr []
>         expr = foldl (\e i -> Apply e (Variable i))
>                  (Variable $ dictTypeClassDataConstrQIdent typeClass)
>                  exprArgs
>         exprArgs,superInstArgs,methodsArgs :: [QualIdent]
>         exprArgs = superInstArgs ++ methodsArgs
>         superInstArgs = map f superQIds
>           where f superTC = let [InstanceInfo _ _ instMIdent _ _ _] =
>                                   lookupInstance superTC typeConstr instEnv
>                             in qualifyWith instMIdent
>                                  (dictInstanceIdent superTC typeConstr)
>         methodsArgs = map f methodIds
>           where f = qualifyWith m . dictLiftedMethodIdent typeClass typeConstr

> genLiftedMethodEvalAnnots :: QualIdent -> QualIdent -> [Ident] -> EvalEnv ->
>                              [Decl]
> genLiftedMethodEvalAnnots typeClass typeConstr methods evalEnv =
>   mapMaybe liftMethodEvalAnnot methods
>   where liftMethodEvalAnnot method =
>           fmap (EvalAnnot genPos [dictLiftedMethodIdent typeClass typeConstr method])
>                (lookupEval method evalEnv)

> genLiftedMethodSigs :: InstanceEnv -> QualIdent -> QualIdent -> Ident ->
>                     [(Ident,TypeExprWithContext)] -> [Decl]
> genLiftedMethodSigs instEnv typeClass typeConstr tv methods =
>   map liftMethodSig methods
>   where [InstanceInfo _ _ _ ctx _ _] =
>           lookupInstance typeClass typeConstr instEnv
>         liftMethodSig (id,tyexprWC) =
>           TypeSig genPos [dictLiftedMethodIdent typeClass typeConstr id]
>                          (liftedMethodTypeExpr typeConstr tv ctx tyexprWC)

> genLiftedMethods :: InstanceEnv -> QualIdent -> QualIdent -> [Ident] -> [Decl]
> genLiftedMethods instEnv typeClass typeConstr methodIds =
>   map liftDecl decls
>   where [InstanceInfo _ _ _ ctx _ decls] =
>           lookupInstance typeClass typeConstr instEnv
>         liftDecl (FunctionDecl p id eqs) =
>           let newFunId = dictLiftedMethodIdent typeClass typeConstr id
>           in FunctionDecl p newFunId (map (genLiftedEq newFunId) eqs)
>         genLiftedEq newFunId (Equation p lhs rhs) =
>           Equation p (genLiftedLhs newFunId lhs) rhs
>         genLiftedLhs newFunId (FunLhs _ ts) =
>           FunLhs newFunId ts
>         genLiftedLhs newFunId (OpLhs constr1 _ constr2) =
>           OpLhs constr1 newFunId constr2
>         genLiftedLhs newFunId (ApLhs lhs ts) =
>           ApLhs (genLiftedLhs newFunId lhs) ts

\end{lstlisting}
Auxiliary functions:
\begin{lstlisting}

> xId,travId,undefinedId :: Ident
> xId         = mkIdent "x"
> travId      = mkIdent "trav"
> undefinedId = mkIdent "undefined"

\end{lstlisting}
Error messages:
\begin{lstlisting}

> noInstancesForAliasTypes :: String
> noInstancesForAliasTypes =
>   "Instance declarations for alias types are not allowed"

\end{lstlisting}
