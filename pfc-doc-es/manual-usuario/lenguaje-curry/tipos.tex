%
% tipos.tex
%

\section{Sistema de tipos\label{lenguaje-curry:tipos}}

\Curry{} es un lenguaje fuertemente tipado con un polimorfismo al
estilo del descrito por Hindley y Milner\cite{smith94principal}. Cada
elemento del programa tiene un �nico tipo\index{tipos}, y al igual que
en Haskell, no es necesario realizar declaraciones de tipos para las
variables y las funciones, ya que se pueden inferir, pero resulta
conveniente como medida de prevenci�n ante errores.

Para este proyecto, la definici�n de \Zinc{} extiende el sistema de
tipos de \Curry{} con
\emph{clases de tipos}\index{tipos!clases de tipos}.

Aunque los siguientes conceptos se definir�n en la pr�ximas p�ginas,
se presenta ahora toda la notaci�n:

\begin{itemize}
\item $\Gamma$ es una clase de tipos.
\item $\theta$ es un contexto de tipo.
\item $\mathcal{X}$ es un constructor de tipo.
\item $C$ es un constructor de datos.
\item $\alpha$ es una variable de tipo.
\item $\tau$ es una expresi�n de tipo.
\item $\varphi$ es una funci�n o un constructor de datos.
\item $f, g, h, \ldots$ son funciones.
\item $e$ es una expresi�n de datos.
\item $c$ es una restricci�n (\toriginal{constraint} en ingl�s).
\item $t$ es un t�rmino de datos.
\item $p$ es un patr�n, es decir, un t�rmino de datos con variables.
\item $x$ es una variable de datos.
\item $l$ es una lista de elementos.
\end{itemize}

Una \emph{expresi�n de datos}\index{expresi�n} es una variable o una
aplicaci�n (posiblemente parcial) de la forma $\varphi\; e_1\;
\ldots\; e_m$, donde cada $e_i$ es una expresi�n de datos.

Un \emph{t�rmino de datos}\index{t�rmino!de datos} es una variable
$x$ o la aplicaci�n de un constructor de datos $C\; t_1\; \ldots\;
t_n$. Obs�rvese que las expresiones de datos son un concepto m�s
general que los t�rminos de datos.

Un \emph{constructor de datos} es un elemento final (irreducible e
insustituible) de las expresiones de datos.

Una expresi�n de datos (y por tanto, un t�rmino de datos) se denomina
\emph{b�sica} (en ingl�s, \emph{ground})\index{expresi�n!b�sica}
si no contiene ninguna variable. Los t�rminos de datos b�sicos
corresponden a valores en el dominio del programa. Las expresiones
que contienen funciones deben ser evaluadas durante la ejecuci�n del
programa para convertirlas en t�rminos de datos.

Una \emph{expresi�n de tipo}\index{expresi�n!de tipo} es una variable
de tipo, un constructor de tipo de aridad cero, o la aplicaci�n de un
constructor de tipo de
la forma $\simboloCurry{(}\mathcal{X}\; \tau_1\;
\tau_2\ldots\tau_n\simboloCurry{)}$. Un \emph{constructor de tipo} es
un elemento final (irreducible e insustituible) de las expresiones de
tipos.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Declaraci�n de tipos de datos algebraicos}

En \Curry{} existen ciertos tipos prefinidos (se describen en el
apartado \ref{lenguaje-curry:tipos:tipos-predefinidos}), pero son
insuficientes para describir el dominio
de la mayor parte de las aplicaciones. Por esa raz�n, el programador
puede declarar nuevos tipos de datos y sus constructores. Para ello se
utiliza la sintaxis:

\begin{align*}
\simboloCurry{data}\; \mathcal{X}\; \alpha_{1} \ldots \alpha_{n}\;& \simboloCurry{=}\; C_{1}\; \tau_{11} \ldots \tau_{1n_{1}} \\
 & \simboloCurry{|}\; \ldots \\
 & \simboloCurry{|}\; C_{k}\; \tau_{k1} \ldots \tau_{kn_{k}} 
\end{align*}

El identificador $\mathcal{X}$ se convierte en un nuevo
\emph{constructor de tipo}.

Cada una de las alternativas de la derecha define un
\emph{constructor de datos},
que tambi�n se comporta como una funci�n cuya signatura, para cada
$C_{i}$, es:

\begin{equation*}
C_{i}\; \simboloCurry{::}\; \tau_{i1}\; \flechaFuncion\; \cdots\; \flechaFuncion{}\; \tau_{in_{i}}\; \flechaFuncion{}\;
\mathcal{X}\; \alpha_{1} \ldots \alpha_{n}
\end{equation*}

Los constructores de datos 0-arios
son denominados \emph{constantes de datos}. Los tipos de datos pueden
ser \emph{polim�rficos}, al estar parametrizados mediante variables de
tipo ($\alpha_{1},\ldots,\alpha_{n}$).

Algunos ejemplos:

\begin{lstlisting}
-- Un tipo finito, similar a una enumeraci�n
data PrimaryColor = Red | Green | Blue

-- Un tipo infinito
data IntTree = Leaf
             | Node IntTree Int IntTree

-- Un tipo infinito polim�rfico
data Tree a  = Leaf
             | Node (Tree a) a (Tree a)
\end{lstlisting}

Los dos �ltimos ejemplos son tipos de datos infinitos. \Zinc{} puede
manejar c�modamente estructuras de datos infinitas, gracias a la
evaluaci�n perezosa.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Declaraci�n de sin�nimos de tipos}

La definici�n de un sin�nimo de tipo introduce un \tqq{alias} de una
expresi�n de tipo ya existente. La forma es:

\begin{equation*}
\texttt{type}\; \mathcal{X}\; \alpha_{1}\; \ldots\; \alpha_{n}\; \simboloCurry{=}\; \tau
\end{equation*}

Por ejemplo:

\begin{lstlisting}
type Name = [Char]

-- Especializando el �rbol polim�rfico del apartado anterior
type CharTree = Tree Char
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Tipos de datos renombrados}

No forman parte del est�ndar de \Curry{}, pero algunas
implementaciones (y en concreto, la de este proyecto) incorporan los
\emph{tipos de datos renombrados}. Se trata de un concepto prestado
de Haskell, y cuya sintaxis es:

\begin{equation*}
\texttt{newtype}\; \mathcal{X}\; \alpha_{1}\; \ldots\; \alpha_{n}\;
\simboloCurry{=}\; C\; \tau
\end{equation*}

A diferencia de un sin�nimo de tipo, una declaraci�n de este estilo s�
introduce un nuevo constructor de tipo y un constructor de datos. El
nuevo constructor de tipo $\mathcal{X}$ es diferente al tipo sobre el
que se define.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Tipos predefinidos\label{lenguaje-curry:tipos:tipos-predefinidos}}

El lenguaje posee un peque�o conjunto de constructores de tipos
predefinidos, y el est�ndar \Prelude{} define una serie de operaciones
sobre ellos. Los constructores de tipos predefinidos son:

\begin{multicols}{2}
\begin{itemize}
\item Funciones.
\item Booleanos.
\item Restricciones (\emph{constraints}).
\item N�meros enteros.
\item N�meros reales.
\item Caracteres.
\item Listas.
\item Cadenas de caracteres.
\item Tuplas.
\item Tipo unitario o nulo.
\end{itemize}
\end{multicols}

Tres de ellos (funciones, listas y tuplas) tienen una sintaxis
especial, que se describe en el subapartado correspondiente.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Funciones}

El tipo $\tau_1\; \flechaFuncion\; \tau_2$ es el tipo de una funci�n que,
para cada argumento de tipo $\tau_1$ produce un resultado de tipo
$\tau_2$. El operador de tipo infijo $\flechaFuncion$ es asociativo a la
derecha, de modo que la expresi�n:

\begin{equation*}
\tau_1\; \flechaFuncion\; \tau_2\; \flechaFuncion\; \cdots\; \flechaFuncion\;
\tau_{n}\; \flechaFuncion\; \tau_{n+1}
\end{equation*}

y es el tipo de una funci�n $n$-aria (con una sintaxis
\emph{currificada}). La �ltima expresi�n de tipo ($\tau_{n+1}$) no
debe ser un tipo de funci�n (como los que se describen en este
apartado).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Booleanos}

El tipo booleano est� predefinido de la siguiente forma:

\begin{lstlisting}
data Bool = False | True
\end{lstlisting}

Las funciones cuyo resultado es de tipo \lstinline|Bool| se llaman
\emph{predicados}\index{predicado}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{\label{lenguaje-curry:tipos:restricciones}Restricciones}

Se denota mediante \lstinline|Success| el tipo de las condiciones
(\emph{restricciones}\index{restricci�n}) que deben ser evaluadas con
�xito para aplicar una regla (ver el apartado
\ref{lenguaje-curry:funciones:declaracion-funciones}
m�s adelante). Por extensi�n, las funciones cuyo resultado es de tipo
\lstinline|Success| tambi�n se denominan restricciones.

Este tipo s�lo tiene un constructor de datos, denominado
\lstinline|success|, que
representa la restricci�n trivial, es decir, aquella que se satisface
siempre.

Las restricciones y las expresiones de tipo booleano tienen una
diferencia sem�ntica: mientras las segundas son evaluadas hasta
reducirlas a \lstinline|True| o \lstinline|False|, de las primeras se
busca su
\emph{satisfacibilidad}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{N�meros enteros}

Este tipo se denota por \lstinline|Int|, y las constantes enteras
introducidas en un programa son sus constructores de datos. Al igual
que en
muchos lenguajes declarativos, el tipo entero tiene un rango infinito,
aunque en la pr�ctica, los compiladores establecen ciertos
l�mites\footnote{En el caso del compilador MCC, y por
consiguiente, de Zinc, se usan 32 bits para representar los enteros.}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{N�meros reales}

En este caso, el tipo se denomina \lstinline|Float|, y las constantes
reales introducidas en un programa son sus constructores. No existe
un mecanismo autom�tico de promoci�n de tipos num�ricos, de tal forma
que no puede utilizarse un \lstinline|Int| donde se requiere un
\lstinline|Float|.

Sobre el papel, el tipo real tiene rango y precisi�n infinita, aunque
por motivos pr�cticos, los compiladores se ven obligados a establecer
ciertos l�mites.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Caracteres}

El tipo \lstinline|Char| contiene el conjunto de los caracteres, y las
constantes de car�cter como \lstinline|'a'| (con comillas simples) son
constructores de datos para este tipo. Est�n disponibles tambi�n las
secuencias de escape habituales, como \lstinline|'\n'|.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Listas}

La expresi�n de tipo $\simboloCurry{[}\tau{}\simboloCurry{]}$
representa a todas las listas cuyos
elementos son valores del tipo $\tau$. Este tipo est� predefinido,
aunque no mediante el lenguaje, pues aunque la siguiente descripci�n
ser�a sem�nticamente correcta, no lo es desde un punto de vista sint�ctico:

\begin{lstlisting}
data [a] = []
         | a : [a]
\end{lstlisting}

Esta representaci�n es muy habitual de los lenguajes l�gicos y
funcionales. El constructor de datos \lstinline|[]| representa la
lista vac�a,
y el constructor infijo \lstinline|x:xs| representa la lista cuyo
primer elemento es \lstinline|x| y \lstinline|xs| es la lista que
contiene el resto de los elementos. El operador infijo \lstinline|:|
es asociativo a la derecha (en este caso, no se trata de un operador
en el sentido de los operadores del apartado
\ref{lenguaje-curry:funciones:operadores}, sino de un
constructor de datos).

Por simple comodidad sint�ctica, se permite, la notaci�n:

\begin{equation*}
\simboloCurry{[}e_1\simboloCurry{,}e_2\simboloCurry{,}\ldots,e_n\simboloCurry{]}
\end{equation*}

es equivalente a

\begin{equation*}
e_1\simboloCurry{:}e_2\simboloCurry{:}\cdots\simboloCurry{:}e_n\simboloCurry{:}\simboloCurry{[]}
\end{equation*}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Cadenas de caracteres}

Est� predefinido el sin�nimo:

\begin{lstlisting}
type String = [Char]
\end{lstlisting}

Las
constantes de cadena como \lstinline|"Curry"| (con comillas dobles)
son una comodidad sint�ctica, y equivalen a listas como:

\begin{lstlisting}
['C','u','r','r','y']
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Tuplas}

La expresi�n
$\simboloCurry{(}\tau_1\simboloCurry{,}\tau_2\simboloCurry{,}\ldots\simboloCurry{,}\tau_n\simboloCurry{)}$,
con $n>1$, es el tipo de todas las $n$-tuplas de la forma
$\simboloCurry{(}e_1\simboloCurry{,}e_2\simboloCurry{,}\ldots\simboloCurry{,}e_n\simboloCurry{)}$
donde cada $e_i$ es una expresi�n de datos con tipo $t_i$.

El \emph{tipo unitario}, denotado por \lstinline|()| tan s�lo contiene
un constructor de datos, que se representa tambi�n como
\lstinline|()|. Debido a la
estricta separaci�n sint�ctica de tipos y expresiones, no existe
ambig�edad. El
tipo unitario se puede considerar predefinido mediante la siguiente
linea (que no es sint�cticamente v�lida en \Curry{}):

\begin{lstlisting}
data () = ()
\end{lstlisting}

Se menciona aqu� el tipo unitario porque puede ser considerado como el
caso particular de las tuplas cuando $n=0$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Contexto de tipo}

La principal extensi�n que incorpora \Zinc{} respecto a \Curry{} en lo
tocante a las expresiones de tipo es la posibilidad de indicar el
contexto de una expresi�n de tipo. Un contexto $\theta$ tiene la
forma:

\begin{equation*}
( \Gamma_1\; \tau_1, \ldots, \Gamma_n\; \tau_n )
\end{equation*}

Cuando el contexto agrega informaci�n a una expresi�n de tipo, se
coloca como prefijo, separado por el s�mbolo $\flechaContexto$. Si
$n = 1$, los par�ntesis son opcionales.

Un contexto de tipo establece restricciones en el rango de las
variables de tipo. Lo m�s habitual es que las expresiones de tipo
$\tau_i$ sean variables de tipo ($\alpha_i$), aunque en determinados
casos pueden aparecer expresiones m�s complejas.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Declaraci�n de signaturas de funciones}

Adem�s de la propia definici�n de una funci�n, que se describe a
continuaci�n, el programador
puede realizar una declaraci�n de su tipo (\emph{signatura}). Aunque
el compilador puede
inferir el tipo de la funci�n, declarar expl�citamente el tipo de una
funci�n ayuda a comprender mejor su funcionamiento y previene posibles
errores al escribir el c�digo.

Anteriormente, se han descrito los tipos de funci�n. Para declarar el
tipo de una funci�n $f$ $n$-aria, se utiliza la sintaxis:

\begin{equation*}
f\; \simboloCurry{::}\; \theta{} \flechaContexto{}\; \tau_1\; \flechaFuncion\; \tau_2\;
\flechaFuncion\; \cdots\; \flechaFuncion\; \tau_{n}\; \flechaFuncion\; \tau_{n+1}
\end{equation*}

La parte del contexto de tipo es opcional, y en caso de no existir, se
prescinde tambi�n del s�mbolo $\flechaContexto$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Limitaciones de la inferencia de tipos}
%
%Existen distintos algoritmos de inferencia de tipos, pero no se
%conoce ninguno que funcione en
%todas las situaciones. En algunos casos muy particulares, el
%programador debe declarar expl�citamente el tipo de una funci�n.

%Cuando se utilizan m�dulos (secci�n \ref{lenguaje-curry:modulos}), el sistema
%de inferencia de tipos s�lo puede trabajar con los tipos visibles
%desde cada m�dulo.
