%
% busqueda-encapsulada.tex
%

\section{B�squeda encapsulada}

Al tratarse de un lenguaje integrador de paradigmas, \Curry{} tiene
que permitir la b�squeda no-determinista de soluciones, heredada de la
programaci�n l�gica. Sin embargo, hay ocasiones en las que el
no-determinismo no s�lo es poco conveniente, sino directamente
indeseable. Uno de esos casos se da, por ejemplo, con la
entrada/salida (descrita en \ref{lenguaje-curry:entrada-salida}).

Un programa interactivo genera como resultado una acci�n (posiblemente
una secuencia de acciones) que es ejecutada cuando, a su vez, el
programa es evaluado. Un programa en \Curry{} puede producir una
disyunci�n de posibles resultados, pero dado que las acciones
modifican el mundo exterior, es inconcebible que un programa
interactivo genere como resultado una disyunci�n de acciones. Entre
otras razones, las acciones no se pueden deshacer por motivos
pr�cticos (deshacer una acci�n referente a un sistema de ficheros
supondr�a tener que restaurar una copia completa del estado anterior,
o restaurar el estado anterior de Internet, si se trata de una acci�n
de red).

Todo el no-determinismo provocado por la b�squeda de soluciones tiene
que ser encapsulado para poder hacerlo compatible con la
entrada/salida. La forma de hacerlo est� prevista en el lenguaje y
formalmente descrita en \cite{HanusSteiner98PLILPALP}, incluyendo
los teoremas de correcci�n y completitud.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Objetivo de la b�squeda}

En el paradigma declarativo, toda b�squeda es un intento de encontrar
soluciones que satisfagan una restricci�n. Los resultados de esa
b�squeda son valores de una determinada variable contenida en la
restricci�n, que se denomina \textit{variable de b�squeda} (la
generalizaci�n a m�s de una variable es inmediata utilizando tuplas).

La variable de b�squeda se puede abstraer de la restricci�n que se
pretende satisfacer mediante el concepto de \textit{objetivo de la
b�squeda}, que es una funci�n del tipo

\begin{lstlisting}
a -> Success
\end{lstlisting}

Es decir, si $c$ es una restricci�n que contiene la variable de
b�squeda $x$ y se buscan soluciones para $x$, es decir, valores de $x$
que satisfagan $c$, entonces el objetivo de b�squeda tiene la forma
$\verb|\|x \flechaFuncion c$.

Por ejemplo, el objetivo de b�squeda
\lstinline|\X -> append([1],X) =:= [1,2]|
representa la situaci�n en la que se buscan valores para la variable
\lstinline|X| tales que al concatenarlos a la lista \lstinline|[1]|
produzcan la lista \lstinline|[1,2]|.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{La primitiva de control de b�squeda}

La funci�n \lstinline|try| es la primitiva que proporciona el lenguaje
para el control de b�squeda. Su tipo es:

\begin{lstlisting}
try :: (a -> Success) -> [a -> Success]
\end{lstlisting}

Se trata de una funci�n de bajo nivel con caracter�sticas de
metaprogramaci�n y reflectividad. Su comportamiento consiste en
evaluar la restricci�n del objetivo de b�squeda que recibe como
par�metro hasta que la evaluaci�n termina o llega a un punto
no-determinista. En el segundo caso, la evaluaci�n es suspendida y la
funci�n devuelve los objetivos de b�squeda correspondientes a las
diferentes instanciaciones. La sem�ntica operacional est� descrita con
precisi�n en \cite{HanusSteiner98PLILPALP}, aunque se puede resumir
considerando los tres posibles resultados de la evaluaci�n de una
llamada $\simboloCurry{try}\; ( \barraLambda{} x \flechaFuncion c )$:

\begin{itemize}

\item Una lista vac�a, indicando que no se conoce ninguna soluci�n
para el objetivo de b�squeda. Por ejemplo, la expresi�n
\lstinline|try (\x -> 1=:=2)| produce como resultado la lista vac�a.

\item Una lista con un �nico elemento, indicando que el objetivo de
b�squeda se puede evaluar de forma determinista a una �nica
soluci�n. Por ejemplo, la expresi�n \lstinline|try (\x -> [x]=:=[0])|
produce como resultado \lstinline|[\x -> x=:=0]|, que es una lista
cuyo �nico elemento representa, en forma de restricci�n, la soluci�n
encontrada.

\item Una lista con m�ltiples elementos, donde cada uno de ellos
representa, como una restricci�n, una de las posibles alternativas del
primer punto de no-determinismo encontrado al tratar de resolver el
objetivo de la b�squeda. Por ejemplo, considerando definida la
funci�n

\begin{lstlisting}
f 'a' = 'c'
f 'b' = 'd'
\end{lstlisting}

entonces la llamada \lstinline|try (\x -> f x =:= 'd')| produce la lista

\begin{lstlisting}
[\x -> x=:='a' & f 'a' =:= 'd', \x-> x=:='b' & f 'b' =:= 'd']
\end{lstlisting}

que corresponde a las posibles instanciaciones no-deterministas de la
variable \lstinline|x| como argumento de la funci�n \lstinline|f|. La
computaci�n se detiene en ese momento.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Operadores de b�squeda}

Sobre esta primitiva se definen, ya dentro del lenguaje,
operadores de b�squeda implementando distintas estrategias. El
\Prelude{} define varios, entre otros la b�squeda en profundidad de
todas las soluciones (\lstinline|solveAll|), la primera soluci�n en
profundidad (\lstinline|once|) o la mejor seg�n alg�n criterio de
comparaci�n proporcionado (\lstinline|best|).

Como muestra, el primero de esos operadores est� implementado de esta
forma:

\begin{lstlisting}
solveAll :: (a -> Success) -> [a -> Success]
solveAll g = collect (try g)
           where collect []    = []
                 collect [g]   = [g]
                 collect (g1:g2:gs) = concat (map solveAll (g1:g2:gs))
\end{lstlisting}

Por ejemplo, la evaluaci�n de

\begin{lstlisting}
solveAll (\l -> l ++ [1] =:= [0,1])
\end{lstlisting}

genera el resultado \lstinline|[\l->l=:=[0]]|.

Es conveniente hacer notar que, gracias a la evaluaci�n perezosa, es
posible tratar con b�squedas con un n�mero infinito de
soluciones. Para demostrarlo, la funci�n \lstinline|once|
anteriormente referida, est� definida de la siguiente forma:

\begin{lstlisting}
once :: (a -> Success) -> (a -> Success)
once g = head (solveAll g)
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Acceso a las soluciones}

Al terminar las operaciones de b�squeda, resulta conveniente
transformar las soluciones proporcionadas como una lista de
restricciones en los valores que satisfacen las restricciones. Se
puede conseguir asociando la restricci�n a una variable. Por ejemplo:

\begin{lstlisting}
let x, g free in (solveAll (\l -> l ++ [1] =:= [0,1])) =:= [g] & g x
\end{lstlisting}

De esta forma, \lstinline|g| queda asociada a \lstinline|\l->l=:=[0]|,
y \lstinline|x| queda asociada a \lstinline|[0]|. Por ser muy com�n,
\Prelude{} define la funci�n \lstinline|unpack| que realiza esta
tarea, as� como \lstinline|findall|, que aplica \lstinline|unpack|
sobre los resultados de \lstinline|solveAll|.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Variables locales}

La b�squeda encapsulada es un proceso para mantener controlado el
no-determinismo. Por tanto, las variables libres declaradas fuera del
objetivo de la b�squeda nunca son ligadas por
medio de la b�squeda encapsulada. Por ejemplo, la evaluaci�n de:

\begin{lstlisting}
first (\l2 -> append l1 l2 =:= [0,1])
\end{lstlisting}

es suspendida porque no es posible ligar \lstinline|l1|. En cambio, la
evaluaci�n de:

\begin{lstlisting}
s =:= (first (l2 -> append l1 l2 =:= [0,1])) & l1 =:= [0]
\end{lstlisting}

puede terminar, porque aunque inicialmente la primera restricci�n
provoca una suspensi�n, la segunda liga la variable y permite
continuar a la primera.

Pero a veces es interesante tener variables locales en la restricci�n
del objetivo de la b�squeda. Esto puede conseguirse mediante
\lstinline|let|. Por ejemplo:

\begin{lstlisting}
findall (\e -> let l free in l++e =:= [3,4,5])
\end{lstlisting}

que produce como resultado \lstinline|[[3,4,5],[4,5],[5],[]]|.
