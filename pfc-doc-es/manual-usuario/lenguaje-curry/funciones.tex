%
% funciones.tex
%

\section{Funciones}

Las funciones en \Curry{} no son secuencias de instrucciones (como en
C, por poner un ejemplo) a las que se puede transferir el flujo de
ejecuci�n de un programa, sino que tienen la misma sem�ntica que en
las matem�ticas. Describen una relaci�n de equivalencia, o m�s
concretamente, la capacidad de reemplazar unos t�rminos (lado
izquierdo) por otros (lado derecho).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Declaraci�n de
funciones\label{lenguaje-curry:funciones:declaracion-funciones}}

Una funci�n $n$-aria $f$ se define por una o varias \emph{ecuaciones
definitorias} o \emph{reglas de reescritura} como la siguiente:

\begin{equation*}
f\; t_1\; \ldots t_n\; \simboloCurry{=}\; e
\end{equation*}

Los t�rminos a ambos lados del s�mbolo \verb|=| se denominan
\emph{lado izquierdo} y \emph{lado derecho}. En lado izquierdo no
puede contener m�s de una instancia de la misma variable.

La utilizaci�n de varias reglas es posible gracias al \emph{encaje de
patrones}. El s�mbolo \verb|_| representa un patr�n con el que encaja
cualquier expresi�n.

Unos ejemplos de funciones:

\begin{lstlisting}
-- Negaci�n booleana
not :: Bool -> Bool
not True  = False
not False = True

-- Primer elemento de una lista
head :: [a] -> a
head (x:_) = x

-- Concatenaci�n de dos listas
append :: [a] -> [a] -> [a]
append []     ys = ys
append (x:xs) ys = x : append xs ys

-- Longitud de una lista
length :: [a] -> Int
length []     = 0
length (_:xs) = 1 + length xs
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Ecuaciones condicionales con restricciones}

Una forma m�s sofisticada de definir la funci�n $f$ es utilizar una o
varias \emph{ecuaciones condicionales}, cuya forma es:

\begin{equation*}
f\; t_1\; \ldots t_n\; \simboloCurry{|}\; c\; \simboloCurry{=}\; e
\end{equation*}

donde la condici�n $c$ es una \emph{restricci�n}
(v�ase \ref{lenguaje-curry:tipos:restricciones}). Esta restricci�n
debe ser satisfecha para poder aplicar la ecuaci�n condicional.

La definici�n de funciones mediante ecuaciones con restricciones es
una herramienta muy poderosa combinada con la utilizaci�n de variables
l�gicas, que se describen en la secci�n \ref{lenguaje-curry:variables}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Ecuaciones condicionales con guardas}

Buscando la compatibilidad con Haskell, \Zinc{} tambi�n permite
escribir ecuaciones condicionales con guardas:

\begin{align*}
f\; t_1\; \ldots t_n\;
 & \simboloCurry{|}\; b_1\; \simboloCurry{=}\; e_1 \\
 & \simboloCurry{|}\; \ldots \\
 & \simboloCurry{|}\; b_k\; \simboloCurry{=}\; e_k
\end{align*}

En este caso, las expresiones $b_1,\ldots,b_k$ (\emph{guardas}) son de
tipo booleano. La sem�ntica es la misma que en Haskell: las guardas
son evaluadas en el mismo orden en el que est�n definidas, y se aplica
\emph{la primera} ecuaci�n para la que su guarda resulte ser
cierta. El mismo efecto puede conseguirse con construcciones del tipo
\lstinline|if_then_else|, que se examinan m�s adelante
(\ref{subapartado:lenguaje-curry:funciones:predefinidas:booleanos}).

Un ejemplo:

\begin{lstlisting}
max x y | x < y     = y
        | otherwise = x
\end{lstlisting}

No se pueden mezclar ecuaciones condicionales con restricciones y
ecuaciones condicionales con guardas en la definici�n de una misma
funci�n, ya que su sem�ntica es diferente:

\begin{itemize}
\item Cuando se utilizan guardas, �stas son evaluadas en el orden de
definici�n, y el resultado de la funci�n es el de la evaluaci�n del
lado derecho de \emph{la primera} regla cuya guarda se eval�e como
verdadera.
\item Cuando se utiliza restricciones, todas las restricciones son
comprobadas, y \emph{todas} aquellas que reglas cuya restricci�n sea
satisfacible son seleccionadas para su evaluaci�n.
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Funciones no deterministas}

Es perfectamente v�lido definir funciones no deterministas mediante
la utilizaci�n de reglas cuyos lados izquierdos se solapan. Por
ejemplo, esta funci�n inserta un elemento en una posici�n arbitraria
de una lista:

\begin{lstlisting}
insert :: a -> [a] -> [a]
insert x (y:ys) = x : y : ys
insert x (y:ys) = y : insert x ys
\end{lstlisting}

Afortunadamente, las funciones no deterministas se integran
perfectamente en la sem�ntica del lenguaje, gracias a que utilizan los
mecanismos ya dispuestos para la programaci�n l�gica. Adem�s, permiten
realizar programas eficientes y m�s expresivos. Se debe utilizar una
funci�n no determinista cuando todas sus posibles respuestas son
igualmente deseables.

Se conocen unos conjuntos de condiciones\cite{Hanus03CurryReport} que
permiten decidir en tiempo de compilaci�n si las distintas reglas
que definen una funci�n provocan indeterminismo.

Las funciones no deterministas pueden provocar efectos sorprendentes
al combinarse con otras caracter�sticas del lenguaje, como la
evaluaci�n perezosa. Por ejemplo, dadas estas
dos funciones, de las cuales \lstinline|coin| es no determinista:

\begin{lstlisting}
double x = x + x

coin = 0
coin = 1
\end{lstlisting}

La evaluaci�n de
la expresi�n \lstinline|coin| produce como resultado $0$ � $1$. La
evaluaci�n de la expresi�n \lstinline|double coin| produce como
resultado $0$ � $2$. En ning�n caso puede producir $1$, ya que
las instancias de una variable se comparten.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Operadores\label{lenguaje-curry:funciones:operadores}}

El lenguaje permite declarar operadores\index{operador} para
escribir programas m�s legibles.
Un operador es una funci�n con unas reglas de sintaxis particulares.
El identificador de un operador
consiste en una cadena de cualquier longitud formada por los
siguientes caracteres:

\begin{verbatim}
~ ! @ # $ % ^ & * + - = < > ? . / | \ :
\end{verbatim}

Los operadores tambi�n pueden tener identificadores alfanum�ricos,
siempre que se encierre este identificador entre comillas invertidas
(por ejemplo, \lstinline|`mod`|).

Los operadores son siempre infijos\index{operador!infijo}, aunque
tambi�n pueden usarse de forma prefija\index{operador!prefijo}, pero
en ese caso, su identificador debe estar contenido entre
par�ntesis. Se declaran de la misma forma que las funciones. Por
ejemplo, \Prelude{} define, entre otros, los siguientes operadores:

\begin{lstlisting}
-- Conjunci�n booleana secuencial (utilizando la notaci�n prefija)
(&&) :: Bool -> Bool -> Bool
(&&) True  x = x
(&&) False x = False

-- Concatenaci�n de listas (utilizando la notaci�n infija)
(++) :: [a] -> [a] -> [a]
[]     ++ ys = ys
(x:xs) ++ ys = x : xs++ys
\end{lstlisting}

Para declarar el tipo de un operador, es necesario utilizar la
notaci�n de par�ntesis, como en los dos ejemplos anteriores.

Aunque suele ser m�s legible, la notaci�n infija tiene la desventaja
de introducir ambig�edad en la interpretaci�n sint�ctica de las
expresiones. Para deshacerla, el programador debe declarar la
asociatividad\index{operador!asociatividad} (\emph{a izquierdas},
\emph{a derechas}, o \emph{no asociativo}) y
precedencia\index{operador!precedencia} de cada operador.

Para la asociatividad se utilizan las palabras clave \lstinline|infixl|,
\lstinline|infixr| e \lstinline|infix| (respectivamente), y para la
precedencia, n�meros enteros en el rango $[0,9]$, siendo $9$ el nivel
de precedencia m�s fuerte.

Estas declaraciones deben hacerse al principio del m�dulo (los m�dulos
se describen posteriormente, en la secci�n
\ref{lenguaje-curry:modulos}) donde se definen
los operadores\footnote{Aunque el est�ndar de \Curry{} lo indica de
esta manera, el
MCC y \Zinc{} relajan esta restricci�n, y no obligan a concentrar las
declaraciones de asociatividad al comienzo del m�dulo.}. Por
ejemplo, el est�ndar \Prelude{} declara de la siguiente forma la
asociatividad y precedencia de los operadores anteriores:

\begin{lstlisting}
infixr 5 ++
infixr 3 &&
\end{lstlisting}

En caso de omitirse tal declaraci�n para alg�n operador, se asume que
es \lstinline|infixl 9|.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Aplicaci�n de funciones}

Una funci�n $f$ se aplica a un argumento $x$ escribiendo $f\; x$. La
aplicaci�n de funciones es asociativa a la izquierda, es decir, la
expresi�n

\begin{equation*}
f\; e_1\; e_2\; \ldots\; e_n
\end{equation*}

es equivalente a

\begin{equation*}
\simboloCurry{(}\ldots\simboloCurry{(}\simboloCurry{(}f\;
e_1\simboloCurry{)}\; e_2\simboloCurry{)}\; \ldots\; e_n
\simboloCurry{)}
\end{equation*}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Funciones y operadores predefinidos}

El est�ndar \Prelude{} define un conjunto de funciones y operadores,
que conjuntamente con los tipos predefinidos, constituyen la base de
cualquier programa en \Zinc{}. A continuaci�n se presentan las
funciones y operadores m�s significativos, clasificados seg�n el tipo
con el que trabajan.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Booleanos\label{subapartado:lenguaje-curry:funciones:predefinidas:booleanos}}

Est�n definidos los operadores habituales, como la conjunci�n
secuencial (\lstinline@&&@), la disyunci�n secuencial (\lstinline@||@)
y la negaci�n (\lstinline|not|), as� como la funci�n
\lstinline|otherwise| (que siempre vale \lstinline|True|).

La construcci�n condicional est� implementada en una funci�n llamada
\lstinline|if_then_else|, definida de la siguiente forma:

\begin{lstlisting}
if_then_else :: Bool -> a -> a -> a
if_then_else True  x y = x
if_then_else False x y = y
\end{lstlisting}

Buscando la similitud con otros lenguajes, se puede usar tambi�n
la f�rmula \lstinline|if b then x else y|, aunque esto no es
m�s que una comodidad sint�ctica.

Tambi�n est�n definidos los predicados (funciones cuyo resultado es de
tipo \lstinline|Bool|) m�s habituales para comparaci�n: los operadores
\lstinline|<|, \lstinline|<=|, \lstinline|>|, \lstinline|>=| (en la
clase de tipos \lstinline{Ord}) y \lstinline|==| (en la clase de tipos
\lstinline{Eq}). Este
�ltimo realiza una comprobaci�n estricta de igualdad,
que en la pr�ctica significa que se verifica cuando los operandos se
pueden evaluar a t�rminos de datos id�nticos. Este operador no liga
variables (el concepto de variable ligada se presenta m�s adelante).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Restricciones}

El operador m�s com�n es \lstinline|=:=|, que da lugar a las llamadas
\emph{restricciones ecuacionales}, de la forma $e_1 \simboloCurry{=:=}
e_2$. Esta restricci�n es resoluble si las expresiones $e_1$ y $e_2$
son evaluables a sendos t�rminos de datos unificables entre s�. En
este aspecto radica la gran diferencia entre condiciones booleanas y
restricciones. El operador \lstinline{=:=} puede ligar variables.

Otros dos operadores sobre restricciones son los de conjunci�n:
\lstinline|&| y \lstinline|&>|. En el primer caso, los operandos
pueden ser evaluados concurrentemente, mientras que en el segundo
obliga a una evaluaci�n secuencial.

Aunque todav�a no se han presentado todos los conceptos necesarios
para definir rigurosamente el comportamiento de las restricciones, los
siguientes ejemplos pretenden formar una idea intuitiva del
funcionamiento de las restricciones y su contraste con las expresiones
de tipo booleano:

\begin{lstlisting}
-- Dos expresiones booleanas
0==0   -- Resultado: True
0==1   -- Resultado: False

-- Dos restricciones
0=:=0  -- Resultado: Success
0=:=1  -- Resultado: "sin soluci�n"

-- Ahora intervienen variables no ligadas
x==0 where x free    -- Resultado: "ejecuci�n suspendida"
x=:=0 where x free   -- Resultado: {x=0} Success
\end{lstlisting}

El operador \lstinline|&| es la clave del modelo de concurrencia de
\Zinc{}, ya que permite al programador especificar qu� computaciones
pueden realizarse en paralelo. Sin la presencia de este operador, toda
la computaci�n de un programa en \Zinc{} se produce de forma
secuencial.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{N�meros enteros}

El constructor de tipo \lstinline|Int| pertenece a la clase de tipos
\lstinline|Num|, por lo que sobre �l est�n definidos los operadores
aritm�ticos \lstinline|+|,
\lstinline|-| y \lstinline|*|. Tambi�n est�n definidos los operadores 
\lstinline|`div`| y \lstinline|`mod`|.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{N�meros reales}

El constructor de tipo \lstinline|Float| pertenece a la clase de tipos
\lstinline|Num|, por lo que sobre �l est�n definidos los operadores
aritm�ticos \lstinline|+|, \lstinline|-| y \lstinline|*|. Aunque no es
un m�todo de la clase, tambi�n est� definido el operador
\lstinline|/|.

Aqu� se hace notar una diferencia entre \Curry{} y \Zinc{}. Como el
primero no admite clases de tipos, no es capaz de tratar la
sobrecarga, y por tanto, los operadores sobre n�meros reales tienen
otro nombre (\lstinline|+.|, \lstinline|-.|,
\lstinline|*.| y \lstinline|/.|). Al a�adir clases de tipos, \Zinc{}
s� permite sobrecargar los operadores, por lo que pueden usarse los
mismos \lstinline|+|, \lstinline|-| y \lstinline|*| que en el caso de
los enteros. Por motivos de compatibilidad hacia atr�s, se conservan
tambi�n los operadores de \Curry{}, aunque se recomienda el uso de los
nuevos operadores sobrecargados.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Caracteres}

Dos funciones permiten convertir caracteres en enteros y viceversa
(\lstinline|ord| y \lstinline|chr|) utilizando la tabla ASCII.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Listas}

De todas las funciones y operadores definidos en \Prelude{} para
trabajar con listas, los m�s com�nmente utilizados son el operador de
concatenaci�n de listas (\lstinline|++|), el operador de acceso
ordinal (\lstinline|!!|) y las funciones para extraer el primer
elemento de una lista (\lstinline|head|), los primeros $n$ elementos
(\lstinline|take|), todos los elementos menos el primero
(\lstinline|tail|) y para calcular la longitud de la lista
(\lstinline|length|).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Cadenas de caracteres}

Al tratarse de una especializaci�n de las listas, todas las funciones
aplicables a aquellas tambi�n lo son a las cadenas de
caracteres. Adem�s, \Zinc{} proporciona la clase de tipos
\lstinline|Show|, cuyo m�todo \lstinline|show|
convierte un t�rmino de datos en una cadena. Por ejemplo,
el resultado de \lstinline|show True| es la lista
\lstinline|['T','r','u','e']|.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Funciones de orden superior}

Los argumentos de una funci�n pueden ser, a su vez, funciones, en cuyo
caso, a la primera funci�n se la considera de \emph{orden
superior}. Esta es una caracter�stica habitual en los lenguajes
funcionales, pero muy poco extendida en los lenguajes imperativos,
donde hay que recurrir a artificios como los punteros a funciones (en
C) o los interfaces (en Java). En
\Zinc{}, las funciones de orden superior se integran de forma
transparente en el lenguaje.

En otro ejemplo tomado del est�ndar \Prelude{}, la siguiente funci�n
extrae la sublista prefija m�xima cuyos elementos verifican un cierto
predicado:

\begin{lstlisting}
takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile _ []     = []
takeWhile p (x:xs) = if p x then x : takeWhile p xs
                            else [] 
\end{lstlisting}

La expresi�n \lstinline|takeWhile (>0) [3,6,1,4,-5,2,4]| produce
como resultado la lista \lstinline|[3,6,1,4]|.

En \Prelude{} se pueden encontrar ya definidas muchas funciones de
orden superior de uso muy frecuente, entre ellas las siguientes:

\begin{lstlisting}
map :: (a -> b) -> [a] -> [b]
map _ []     = []
map f (x:xs) = f x : map f xs

foldl :: (a -> b -> a) -> a -> [b] -> a
foldl _ z []      = z
foldl f z (x:xs) = foldl f (f z x) xs

foldr :: (a -> b -> b) -> [a] -> b
foldr _ z []     = z
foldr f z (x:xs) = f x (foldr f z xs)

filter :: (a -> Bool) -> [a] -> [a]
filter _ []     = []
filter p (x:xs) = if p x then x : filter p xs
                         else filter p xs
\end{lstlisting}

Las siguientes funciones, que se aprovechan de las anteriores,
explotan la potencia de las funciones de orden superior:

\begin{lstlisting}
doubleList :: [Int] -> [Int]
doubleList l = map (*2) l

sumList :: [Int] -> Int
sumList l = foldr (+) 0 l

positives :: [Int] -> [Int]
positives l = filter (>=0) l
\end{lstlisting}

Las funciones no s�lo pueden ser par�metros de otras funciones, sino
tambi�n el resultado. El ejemplo anterior puede escribirse tambi�n
as�, utilizando aplicaciones parciales:

\begin{lstlisting}
doubleList :: [Int] -> [Int]
doubleList = map (*2)

sumList :: [Int] -> Int
sumList = foldr (+) 0

positives :: [Int] -> [Int]
positives = filter (>=0)
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Funciones an�nimas}

En ocasiones, una funci�n utiliza tan s�lo en una expresi�n, por
ejemplo, como par�metro de una funci�n de orden superior, y resulta
inc�modo tener que definirla previamente. \Zinc{} soporta la
definici�n de funciones an�nimas (llamadas
\emph{$\lambda$-abstracciones} o $\lambda$-funciones), de la siguiente
forma:

\begin{equation*}
\barraLambda{}x_1 \ldots x_n \flechaFuncion e
\end{equation*}

Por ejemplo, la siguiente funci�n eleva al cuadrado todos los
elementos de una lista de enteros:

\begin{lstlisting}
squareList :: [Int] -> [Int]
squareList l = map (\x -> x*x ) l
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Operadores sobre funciones}

El est�ndar \Prelude{} proporciona tres operadores que permiten
trabajar m�s c�modamente con funciones y mejorar la legibilidad de los
programas al eliminar un exceso de s�mbolos (par�ntesis).

\begin{itemize}

\item La composici�n de funciones, asociativa a la derecha, definida
de la forma:

\begin{lstlisting}
(.) :: (b -> c) -> (a -> b) -> (a -> c)
f . g = \x -> f ( g x )
\end{lstlisting}

\item La aplicaci�n de funciones, asociativa a la izquierda:

\begin{lstlisting}
($) :: (a -> b) -> a -> b
f $ x = f x
\end{lstlisting}

\item La aplicaci�n de funciones, asociativa a la izquierda, con
evaluaci�n estricta de su argumento. En este caso, el operador es
\lstinline|$!|.

\end{itemize}

La diferencia entre los operadores \lstinline|$| y \lstinline|$!| es
evidente con el siguiente ejemplo: si las funciones \lstinline|f| e
\lstinline|inf| est�n
definidas de la siguiente forma (obs�rvese que \lstinline|inf| es una
funci�n infinita, su evaluaci�n no termina nunca):

\begin{lstlisting}
f _ = 0

inf = inf
\end{lstlisting}

en esta situaci�n, la expresi�n \lstinline|f $ inf| se eval�a a cero,
mientras que \lstinline|f $! inf| no termina nunca de evaluarse.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Anotaci�n de funciones\label{lenguaje-curry:funciones:anotacion-funciones}}

El programador puede etiquetar los s�mbolos de funci�n y los m�todos
con \emph{anotaciones} que indican al sistema particularidades de su
evaluaci�n, o modifican comportamientos por defecto del compilador.

Son posibles los siguientes tipos de anotaci�n:

\begin{itemize}

\item \lstinline|choice|

\item \lstinline|flex| o \lstinline|rigid|

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Anotaci�n \lstinline{choice}}

El objetivo que se persigue con esta anotaci�n\footnote{Por
completitud, se describe en este
manual la anotaci�n \lstinline{choice} tal y como aparece en la
especificaci�n de \Curry{}, aunque ni MCC ni \Zinc{} (ni
otros compiladores) la incorporan.}
es proporcionar
distintas alternativas que son igualmente deseables. Aunque su
sem�ntica es algo compleja, intuitivamente su comportamiento equivale
a que el sistema elija (de forma justa) una y s�lo una de las reglas,
aunque existan varias que sean aplicables. Por ejemplo, la siguiente
funci�n mezcla dos listas:

\begin{lstlisting}
merge :: [a] -> [a] -> [a]
merge eval choice
merge [] ys    = ys
merge xs []    = xs
merge (x:xs) ys = x : merge xs ys
merge xs (y:ys) = y : merge xs ys
\end{lstlisting}

Esta funci�n es capaz de mezclar dos listas de las cuales, en una de
ellas, se conoce si es vac�a o tiene alg�n elemento. Esto permite
trabajar con listas parcialmente instanciadas.

Sin embargo, la anotaci�n \lstinline|choice| no es de uso frecuente, y
est� poco soportada por las distintas implementaciones de
\Curry{}. Su principal problema es que perjudica al modelo de
computaci�n, pues destruye la transparencia
referencial\cite{bois-implementing}. Adem�s, hace menos
obvio el significado de los programas.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Anotaci�n \lstinline{flex} o \lstinline{rigid}}

El sustrato de \Zinc{} es un modelo de computaci�n
unificado que permite
que cada funci�n utilice un principio operacional distinto. Las
funciones que
utilizan \toriginal{residuation} se denominan \emph{r�gidas} y las que
utilizan \toriginal{narrowing} se denominan \emph{flexibles}. El
lenguaje establece una elecci�n por defecto que es razonable: todas
las funciones son flexibles, excepto:

\begin{itemize}

\item las relacionadas con la entrada/salida mediante m�nadas (se
describen m�s adelante en la secci�n
\ref{lenguaje-curry:entrada-salida}) y

\item las operaciones aritm�ticas

\end{itemize}

que son r�gidas.

El programador puede cambiar este comportamiento por defecto para las
funciones que define. Por ejemplo, es posible definir un operador de
concatenaci�n de listas que sea r�gido:

\begin{lstlisting}
infixr 5 +++
(+++) eval rigid
(+++) :: [a] -> [a] -> [a]
[]     +++ ys = ys
(x:xs) +++ ys = x : xs +++ ys
\end{lstlisting}

