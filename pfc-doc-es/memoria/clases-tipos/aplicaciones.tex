%
% aplicaciones.tex
%

\section{\label{seccion:clases-tipos:aplicaciones}Aplicaciones de las clases de tipos}

Las clases de tipos no s�lo proporcionan una soluci�n sistem�tica,
elegante y flexible a los problemas asociados a la sobrecarga de
operaciones primitivas como la comparaci�n de igualdad o la
aritm�tica. En manos de los programadores, pueden ser empleadas para
escribir programas m�s f�cilmente comprensibles y con un sustancial
ahorro sint�ctico. La experiencia con Haskell as� lo demuestra, hasta
el punto de que muchos programadores de este lenguaje manejan las
clases de tipos como abstracci�n fundamental durante el dise�o de sus
aplicaciones, de manera similar a como los dise�adores instruidos en
orientaci�n a objetos manejan el concepto de objeto.

El modelo de clases de tipos de las primeras versiones de Haskell era,
hasta cierto punto, experimental, y por tanto, se vio sometido a
mejoras sucesivas. Una de las m�s interesantes fue la
introducci�n\cite{jones95system} de \emph{clases constructoras}
(\toriginal{constructor classes} en la bibliograf�a en ingl�s). Se
trata de una generalizaci�n de las clases de tipos que pretende
resolver los problemas que presentaban las clases de tipos originales
para permitir la sobrecarga de la funci�n \lstinline{map}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sobrecarga de la funci�n \lstinline{map}\label{apartado:clases-tipos:aplicaciones:sobrecarga-map}}

La funci�n \lstinline{map} es muy conocida en el �mbito de la
programaci�n funcional, dada su extrema utilidad. Su definici�n en
Curry (o en Haskell) es:

\begin{lstlisting}
map :: (a -> b) -> [a] -> [b]
map f []       = []
map f (x : xs) = (f x : map f xs)
\end{lstlisting}

Su cometido es aplicar una transformaci�n a todos los elementos de una
lista, produciendo como resultado la lista de los elementos
transformados. Es una aplicaci�n elemental de las funciones de orden
superior, y permite escribir programas que procesen listas sin
preocuparse por la naturaleza recursiva de �stas. Por ejemplo:

\begin{lstlisting}
toUpperString :: [Char] -> [Char]
toUpperString str = map toUpperChar str

evenNumbers :: [Int]
evenNumbers = map (*2) [1..]
\end{lstlisting}

Sin embargo, resulta frecuente encontrar en los programas funciones
como la siguiente:

\begin{lstlisting}
-- un tipo de dato algebraico muy conocido
data Maybe a = Nothing
             | Just a

-- funci�n para aplicar una transformaci�n al tipo anterior
mapMaybe :: (a -> b) -> Maybe a -> Maybe b
mapMaybe _ Nothing  = Nothing
mapMaybe f (Just x) = Just (f x)

-- definici�n del �rbol binario
data BinTree a = Leaf
               | Branch a (BinTree a) (BinTree a)

-- funci�n para aplicar una transformaci�n al contenido del �rbol
mapBinTree :: (a -> b) -> BinTree a -> BinTree b
mapBinTree _ (Leaf            ) = Leaf
mapBinTree f (Branch x sb1 sb2) =
   Branch (f x) (mapBinTree f sb1) (mapBinTree f sb2)
\end{lstlisting}

La aparici�n de este g�nero de funciones es se�al de que
\lstinline{map}, pese a lo flexible que resulta gracias al
polimorfismo param�trico, es a�n demasiado r�gida. El
problema se origina porque la funci�n est� definida para un
tipo concreto de estructura de datos (la lista), y sin embargo, se
requiere con frecuencia su uso sobre otras estructuras de datos. Tan
s�lo con el arma del polimorfismo param�trico no resulta posible
definir la
funci�n \lstinline{map} de manera que pueda emplearse sobre distintas
estructuras de datos, y el
programador se ve arrastrado a la necesidad de definir funciones con
nombres distintos para una misma sem�ntica. La soluci�n pasa por la
sobrecarga de la funci�n.

Por desgracia, las clases de tipos
presentes en las primeras versiones del lenguaje Haskell no eran
capaces de efectuar correctamente esta sobrecarga. Las \emph{clases
constructoras}\cite{jones95system} aportaron la soluci�n y han sido
incorporadas en las versiones recientes de Haskell. Consisten en una
generalizaci�n muy natural de las primigenias clases de
tipos. Sint�cticamente no presentan ninguna dificultad, como se
observa en el siguiente fragmento de c�digo que, por fin, permite
definir una funci�n \lstinline{map} suficientemente gen�rica:

\begin{lstlisting}
-- definici�n de la clase constructora
class Functor t where
  map :: (a -> b) -> (t a -> t b)

-- instanciaci�n para las listas
instance Functor [] where
  map _ []       = []
  map f (x : xs) = (f x : map f xs)

-- instanciaci�n para el tipo Maybe
instance Functor Maybe where
  map _ Nothing  = Nothing
  map f (Just x) = Just (f x)

-- instanciaci�n para el tipo BinTree
instance Functor BinTree where
  map _ (Leaf            ) = Leaf
  map f (Branch x sb1 sb2) = Branch (f x) (map f sb1) (map f sb2)
\end{lstlisting}

La diferencia de la clase constructora \lstinline{Functor} respecto a
una clase de tipos habitual est� en el uso del par�metro
\lstinline{t}. Como se observa en la signatura del m�todo
\lstinline{map}, juega el rol de un constructor de tipo (un vistazo a
las clases de tipos definidas anteriormente en
este cap�tulo revela la diferencia).

A nivel de an�lisis de tipos, las clases constructoras traen consigo
una dificultad. Resulta evidente que esta instanciaci�n carece
de sentido:

\begin{lstlisting}
instance Functor Int where
  -- sin sentido, la funci�n map no puede tener
  -- el tipo (a -> b) -> (Int a) -> (Int b)
\end{lstlisting}

�A qu� se debe esto? La causa es que las clases constructoras
conllevan la aparici�n de un �sistema de tipos� para los tipos, algo
as� como un �sistema de tipos de segundo orden�. Se trata del
sistema de \toriginal{kinds}, que fue introducido en la secci�n
\ref{seccion:analisis-tipos:kind} (p�gina
\pageref{seccion:analisis-tipos:kind}). Todos los tipos que pertenecen
a una determinada clase de tipos deben tener el mismo
\toriginal{kind}. Por
ejemplo, los aspirantes a pertenecer a \lstinline{Eq} deben tener el
\toriginal{kind} m�s sencillo de todos ($*$), mientras que los de
\lstinline{Functor} deben tener el \toriginal{kind}
$* \rightarrow *$. El \toriginal{kind} de los miembros de una clase de
tipos se infiere a partir de las signaturas de los m�todos de la
clase. Cualquier
intento de instanciar una clase para un tipo de \toriginal{kind}
inapropiado produce un sinsentido como en el ejemplo anterior.

Las clases de tipos originales son, en realidad, el caso
particular de las clases constructoras cuando el \toriginal{kind} es
$*$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Generalizaci�n de las m�nadas}

Aunque ya estaban presentes en el �lgebra desde mucho antes, no fue
hasta principios de los a�os noventa cuando se
plante�\cite{wadler90comprehending,wadler92essence} la aplicaci�n de
las \emph{m�nadas} para resolver un viejo problema de la programaci�n
funcional: la introducci�n de las inevitables caracter�sticas impuras
(como la entrada/salida) en un lenguaje funcional puro, es decir, sin
efectos laterales y con transparencia referencial (v�ase el cap�tulo
\ref{capitulo:programacion-declarativa}).

Las m�nadas establecen una clara distinci�n entre los valores
calculados y el proceso para obtenerlos. Si $M$ es una m�nada, un
objeto de tipo $M\;a$ representa un proceso de computaci�n que
produce como resultado un valor de tipo $a$.

Dejando al margen todo el �lgebra que se esconde tras las m�nadas
(que se puede consultar en \cite{labra01tesis}), para los prop�sitos
de este apartado, se puede considerar que una m�nada es una tripleta
constituida por un constructor de tipo $M$ y dos funciones
polim�rficas, llamadas $return$ (o $unit$) y $bind$. Por una cuesti�n
de compatibilidad, la funci�n $bind$ se suele representar en la pr�ctica
mediante el operador \lstinline{>>=}.

Las clases de tipos (generalizadas seg�n lo descrito en el apartado
anterior) permiten definir un marco de desarrollo para
programar con m�nadas. Esto evita la necesidad de que el
\toriginal{front-end} de un compilador tenga que ser consciente del
concepto de m�nada, ya que �sta se puede definir como una clase
constructora, descendiente de \lstinline{Functor}:

\begin{lstlisting}
-- definici�n m�nima de las m�nadas como clases de tipos
class Functor m => Monad m where
  return :: a -> m a
  (>>=)  :: m a -> (a -> m b) -> m b
\end{lstlisting}

Esta es una definici�n m�nima pero completa. En su biblioteca est�ndar
(\Prelude{}), Haskell ofrece una versi�n m�s completa, con dos m�todos
adicionales (se observa que proporciona tambi�n
implementaciones por defecto para los dos nuevos m�todos. Los m�todos
por defecto se describen m�s adelante como una extensi�n de las clases
de tipos, en el apartado
\ref{apartado:clases-tipos:extensiones:metodos-defecto}):

\begin{lstlisting}
-- definici�n ampliada de las m�nadas (tomada de Haskell)
class Functor m => Monad m where
  return :: a -> m a
  (>>=)  :: m a -> (a -> m b) -> m b
  (>>)   :: m a -> m b -> m b
  fail   :: String -> m a

  -- implementaciones por defecto
  m >> k  = m >>= (\_ -> k)
  fail s  = error s   -- error est� definida en otra parte
\end{lstlisting}

Los lenguajes como Haskell y Curry proporcionan una sentencia
\lstinline{do} que en realidad es tan s�lo una conveniencia
sint�ctica, ya que las construcciones \lstinline{do} pueden ser
trivialmente convertidas a una expresi�n de datos usando el operador
\lstinline{(>>=)}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{M�nada para computaciones con estado}

Una aplicaci�n inmediata de la definici�n de las m�nadas como una
clase de tipos es la encapsulaci�n de procesos de computaci�n con
estado. La manera de lograrlo pasa por la definici�n de un tipo de
dato algebraico que encapsule el estado (\lstinline{s}), y su
instanciaci�n para la clase de tipos \lstinline{Monad} (previamente
hay que instanciarlo para \lstinline{Functor} debido a la relaci�n
entre estas clases):

\begin{lstlisting}
data State s a = StateTransformer (s -> (a,s))

instance Functor (State s) where
  map f (StateTransformer st) =
    StateTransformer (\s -> let (x,s') = st s
                            in (f x,s'))

instance Monad (State s) where
  return x = StateTransformer (\s -> (x,s))
  (StateTransformer m) >>= f =
    StateTransformer (\s -> let (x,s') = m s
                                StateTransformer f' = f x
                            in f' s')

-- funci�n auxiliar muy conveniente en la pr�ctica
runStateTransformer :: State s a -> s -> a
runStateTransformer (StateTransformer m) v = fst (m v)
\end{lstlisting}

Usadas con prudencia, las computaciones con estado permiten definir
funciones breves y f�cilmente comprensibles (por supuesto, como con
cualquier otra caracter�stica que los lenguajes ponen en manos de los
programadores, es f�cil caer en el abuso). Un ejemplo ilustrador de
esta capacidad se recoge a continuaci�n, extra�do de
\cite{jones95system}. Una funci�n pura para etiquetar cada uno de los
elementos contenidos en un �rbol binario con un n�mero entero
consecutivo tendr�a este aspecto:

\begin{lstlisting}
label :: BinTree a -> BinTree (a,Int)
label tree = fst (label' tree 0)
  where label' :: BinTree a -> Int -> (BinTree (a,Int),Int)
        label' (Leaf            ) c = (Leaf,c)
        label' (Branch x sb1 sb2) c = (Branch (x,c') sb1' sb2',c'')
          where (sb1',c')  = label' sb1 c
                (sb2',c'') = label' sb2 (c'+1)
\end{lstlisting}

La funci�n hom�loga usando una m�nada para capturar el estado (en este
caso, el n�mero entero a asignar al siguiente nodo) es:

\begin{lstlisting}
label :: BinTree a -> BinTree (a,Int)
label tree = runStateTransformer (label' tree) 0
  where label' :: BinTree a -> State Int (BinTree (a,Int))
        label' (Leaf            ) = return Leaf
        label' (Branch x sb1 sb2) = do sb1' <- label' sb1
                                       c    <- incr
                                       sb2' <- label' sb2
                                       return (Branch (x,c) sb1' sb2')

-- simple transformador del estado
incr :: State Int Int
incr = StateTransformer (\s -> (s,s+1))
\end{lstlisting}

La funci�n \lstinline{incr} cumple la misi�n de transformar el estado
de la m�nada (incrementa el entero que define ese estado), y al mismo
tiempo devolver el valor original del estado. La tentaci�n de
especializar la clase de tipos para soportar un m�todo capaz de
capturar esta sem�ntica tropieza con la necesidad de las clases de
tipos con m�ltiples par�metros, una extensi�n propuesta que se analiza
m�s adelante (apartado
\ref{apartado:clases-tipos:extensiones:clases-multiparametricas}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{M�nada de entrada/salida}

Uno de los usos m�s interesantes de las m�nadas definidas como una
clase de tipos es la encapsulaci�n de las caracter�sticas impuras de
las operaciones de entrada/salida\cite{peytonjones02tackling}. En
realidad, basta con emplear la
t�cnica anterior para considerar estas operaciones como computaciones
con estado, donde �ste �ltimo es el estado del mundo exterior (al
menos, la parte del mundo que est� al alcance del sistema
inform�tico: sistemas de ficheros, redes de comunicaciones, etc.). Un
tipo de datos opaco llamado \lstinline{World} representa este
estado. Las operaciones de entrada/salida son transformaciones de ese
estado que producen adem�s un resultado:

\begin{lstlisting}
-- definici�n del tipo IO como tipo algebraico
data IO a = IO (World -> (a,World))

-- instanciaci�n de Functor
instance Functor IO where
   map f x =  x >>= (return . f)

-- instanciaci�n de Monad
instance Monad IO where
   return = internalIOReturn
   (>>=)  = internalIOBind
\end{lstlisting}

Las funciones \lstinline{internalIOReturn} e
\lstinline{internalIOBind} no pueden ser descritas en el lenguaje
debido a que \lstinline{World} es un tipo de datos opaco.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Otras aplicaciones}

El sistema de clases tipos puede ser usado por s� solo para realizar
c�lculos, que son ejecutados en tiempo de compilaci�n. Es decir,
este sistema puede ser empleado como un peque�o y limitado lenguaje
funcional. Este curioso empleo de las clases de tipos como
metalenguaje est� descrito en \cite{hallgren01fun}.