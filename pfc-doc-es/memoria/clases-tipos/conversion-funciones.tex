%
% conversion-funciones.tex
%

\subsection{Conversi�n de las funciones y las expresiones de datos}

En este apartado se aborda la cuesti�n de la transformaci�n de las
expresiones de datos y de las funciones, pues est�n estrechamente
ligadas. Debido a que esta conversi�n puede afectar a las funciones
generadas a partir de las declaraciones de clases de tipos y de las
instanciaciones, debe realizarse en �ltimo lugar\footnote{Por la misma
raz�n, para obligar al compilador Zinc a que muestre el resultado de
la transformaci�n se debe emplear la opci�n
\texttt{-\--dump-module-6}.}.

Los elementos del �rbol sint�ctico que pueden verse alterados por esta
conversi�n son:

\begin{itemize}

\item Las expresiones de datos, y concretamente aquellas que consisten
en una variable correspondiente al nombre de un m�todo o de una
funci�n sobrecargada (el comportamiento difiere entre ambos
casos). Estas variables son reemplazadas por otras expresiones
equivalentes.

\item El lado izquierdo (LHS) de la definici�n de una funci�n,
solamente si se trata de una funci�n sobrecargada. En este �rea se
produce una inserci�n de nuevos par�metros, que corresponden a
diccionarios.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{M�todos en expresiones de datos}

La transformaci�n m�s sencilla se produce cuando en una expresi�n de
datos aparece un m�todo de una clase de tipos, y se puede determinar
(gracias a la inferencia de tipos) cu�l es el constructor de tipos
involucrado. Por ejemplo:

\begin{lstlisting}
eq x 10
\end{lstlisting}

La transformaci�n consiste en aplicar el selector del m�todo
\lstinline{eq} sobre el valor del diccionario de la clase
\lstinline{Eq} para los tipos enteros (\lstinline{Int}), pues la
inferencia de tipos permite despejar la variable de tipo (el m�todo
tiene el tipo \lstinline{a -> a -> Bool}, siendo \lstinline{a} el
par�metro de la clase de tipos, y como su segundo argumento
en esta expresi�n es una constante de tipo entero, resulta obvio que
el par�metro \lstinline{a} vale \lstinline{Int}). El resultado es:

\begin{lstlisting}
(selEq dictInstEqInt) x 10
\end{lstlisting}

Los par�ntesis se incluyen tan solo para resaltar el lugar donde se
produce la transformaci�n. Inmediatamente se observa que la evaluaci�n
de la subexpresi�n contenida entre par�ntesis podr�a realizarse en
tiempo de compilaci�n\footnote{Esta optimizaci�n no est� implementada
en el compilador Zinc. La secci�n
\ref{seccion:conclusiones:futuras-lineas} se refiere a esta
optimizaci�n, entre otras.}. La transformaci�n no altera el tipo de la
expresi�n en su conjunto, que en este caso sigue siendo
\lstinline{Bool}, porque el tipo de la subexpresi�n entre par�ntesis
es \lstinline{Int -> Int -> Bool}.

En caso de que, inicialmente, el m�todo hubiese sido aplicado
mediante notaci�n infija, como en:

\begin{lstlisting}
x `eq` 10
\end{lstlisting}

el resultado de la transformaci�n habr�a sido el mismo, lo que
obligar�a a eliminar la notaci�n infija de la expresi�n para poder
aportarle el par�metro adicional (el diccionario).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Inserci�n de par�metros}

En general, no se puede resolver la sobrecarga en todos los casos. Lo
habitual es que la inferencia de tipos no pueda averiguar el
constructor de tipos concreto. En ese caso, se introduce una nueva
variable de datos en la expresi�n. Por ejemplo, ante la expresi�n:

\begin{lstlisting}
eq x y
\end{lstlisting}

la transformaci�n produce:

\begin{lstlisting}
\dEq -> selEq dEq x y
\end{lstlisting}

El lugar que antes ocupaba el identificador de un valor concreto para
el diccionario es ocupado ahora por una variable (\lstinline{dEq}),
cuyo valor no es conocido en tiempo de compilaci�n. La expresi�n queda
confinada en la parte derecha de una $\lambda$-funci�n. Resulta claro
que, a diferencia de lo indicado anteriormente, ahora ya no se
conserva el tipo global de la expresi�n, que pasa de ser
\lstinline{Bool} a \lstinline{EqD a -> Bool}.

Por tanto, la transformaci�n debe proseguir ascendiendo por el �rbol
sint�ctico, ampliando el �mbito de la $\lambda$-funci�n, hasta llegar
a la c�spide de la expresi�n de datos. All� puede encontrarse la
ecuaci�n definitoria de una funci�n (podr�a ser tambi�n la definici�n
de un patr�n, pero ese caso se trata posteriormente). La ecuaci�n se
reformula para incluir un par�metro adicional, precediendo a los que
ya pudieran existir. Por ejemplo, la siguiente funci�n sobrecargada:

\begin{lstlisting}
neq :: Eq a => a -> a -> Bool
neq x y = not (x `eq` y)
\end{lstlisting}

se transforma en:

\begin{lstlisting}
neq :: EqD a -> a -> a -> Bool
neq dEq x y = not (selEq dEq x y)
\end{lstlisting}

En este ejemplo se observa que la signatura de la funci�n sufre una
alteraci�n muy simple: cada predicado restringiendo una variable se
convierte en una expresi�n de tipo relativa a un diccionario. No
obstante,
en posteriores ejemplos se muestra que esta expresi�n puede ser m�s
compleja de lo que aparece aqu�.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Funciones sobrecargadas en expresiones de datos}

En los p�rrafos anteriores se ha mostrado la t�cnica de inserci�n de
par�metros. Obviamente, esto obliga a modificar tambi�n todas las
expresiones de datos en las que aparezca la funci�n, para
proporcionarle los nuevos par�metros. La operaci�n es muy similar a la
que se acomete cuando aparece un m�todo, salvo porque no interviene un
selector. De nuevo pueden darse dos casos: que la inferencia permita
determinar el valor del diccionario, o que tenga que utilizarse una
variable porque no es posible determinar el valor. Las dos situaciones
aparecen en este ejemplo:

\begin{lstlisting}
notZero :: Int -> Bool
notZero x = x `neq` 0

areDifferent :: Eq a => (a,a) -> Bool
areDifferent (x,y) = x `neq` y
\end{lstlisting}

que produce:

\begin{lstlisting}
notZero :: Int -> Bool
notZero x = neq dictInstEqInt x 0

areDifferent :: EqD a -> (a,a) -> Bool
areDifferent dEq (x,y) = neq dEq x y
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Funciones locales}

La inserci�n de par�metros s�lo se produce en las funciones que est�n
sobrecargadas. Las funciones locales siguen la misma regla, como se
muestra en el siguiente ejemplo:

\begin{lstlisting}
alias1 :: Eq a => a -> a -> Bool
alias1 x y = g
  where g = neq x y          -- g tiene tipo Bool

alias2 :: Eq a => a -> a -> Bool
alias2 = g
  where g x' y' = neq x' y'  -- g tiene el mismo tipo que alias2
\end{lstlisting}

para dar lugar a:

\begin{lstlisting}
alias1 :: EqD a -> a -> a -> Bool
alias1 dEq x y = g
  where g = neq dEq x y

alias2 :: EqD a -> a -> a -> Bool
alias2 dEq = g dEq
  where g dEq' x' y' = neq dEq' x' y'
\end{lstlisting}

De la misma forma funcionan las definiciones locales mediante
\lstinline{let}. En la funci�n \lstinline{alias1} del ejemplo anterior
se aprovecha el �mbito del par�metro \lstinline{dEq} para utilizarlo
en el lado derecho de la ecuaci�n definitoria de \lstinline{g}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Patrones}

Los patrones no ofrecen ninguna dificultad respecto a las funciones
locales examinadas anteriormente. Como no admiten sobrecarga, nunca
se produce inserci�n de par�metros. Como muestra:

\begin{lstlisting}
compare :: Eq a => a -> a -> (Bool,Bool)
compare x y = (v,w)
  where (v,w) = (lt x y, le x y)
\end{lstlisting}

se transforma en:

\begin{lstlisting}
compare :: EqD a -> a -> a -> (Bool,Bool)
compare dEq x y = (v,w)
  where (v,w) = (selLt dEq x y, selLe dEq x y)
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Uso de los \toriginal{traversers}}

En ocasiones, es necesario emplear los \toriginal{traversers} para
ascender por la jerarqu�a de clases de tipos. Esto se produce cuando
se requiere un diccionario de una superclase, que debe obtenerse a
partir del diccionario de la subclase. El siguiente fragmento ilustra
la t�cnica:

\begin{lstlisting}
lessOrEqual :: Ord a => a -> a -> Bool
lessOrEqual x y = (x `lt` y) ||
                  (x `eq` y)
\end{lstlisting}

que se transforma en:

\begin{lstlisting}
lessOrEqual :: OrdD a -> a -> a -> Bool
lessOrEqual dOrd x y = (selLt dOrd                 x y) ||
                       (selEq (travEqFromOrd dOrd) x y)
\end{lstlisting}

En este caso, la aplicaci�n del \toriginal{traverser}
\lstinline{travEqFromOrd} al diccionario de la clase de tipos
\lstinline{Ord} permite obtener el diccionario de la clase de tipos
\lstinline{Eq}. Por supuesto, esto puede realizarse tantas veces como
sea necesario para ascender, nivel por nivel, a trav�s de la
jerarqu�a.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsubsection{M�todos sobrecargados en expresiones de datos}
%
%Los m�todos sobrecargados (se entiende que se trata de una sobrecarga
%adicional a la impuesta por la clase de tipos a la que pertenecen)
%plantean dificultades significativas para su conversi�n a
%diccionarios. La transformaci�n de la funci�n que corresponde a la
%definici�n del m�todo elevada modifica su tipo, introduciendo un error
%de tipo. Para ilustrar el problema, se muestra a continuaci�n un
%ejemplo completo. Este es el punto de partida:
%
%\begin{lstlisting}
%class EmptyClass a where
%  method6 :: a -> Bool
%
%class DemoClass a where
%  method7 :: EmptyClass b => a -> b -> Bool
%
%instance EmptyClass Int where
%  method6 x = x > 0
%
%instance DemoClass Char where
%  method7 x y = (x=='a') || (method6 y)
%\end{lstlisting}
%
%Tras la transformaci�n de las declaraciones de las clases de tipos y
%de las instanciaciones, el resultado es:
%
%\begin{lstlisting}
%-- definici�n del diccionario de la clase EmptyClass
%data EmptyClassD a = EmptyClassDict (a -> Bool)
%
%-- selector de la clase EmptyClass
%selMethod6 :: EmptyClassD a -> a -> Bool
%selMethod6 (EmptyClassD m6) = m6
%
%-- definici�n del diccionario de la clase DemoClass
%data DemoClassD a b = DemoClassDict (a -> b -> Bool)
%
%-- selector de la clase DemoClass
%selMethod7 :: EmptyClass b => DemoClassD a b -> a -> b -> Bool
%selMethod7 (DemoClassDict m7) = m7
%
%-- declaraci�n del valor del diccionario
%dictInstEmptyClassInt :: EmptyClassD Int
%dictInstEmptyClassInt = EmptyClassDict dictMethodMethod6Int
%
%-- definici�n del m�todo elevada
%dictMethodMethod6Int :: Int -> Bool
%dictMethodMethod6Int x = x > 0
%
%-- declaraci�n del valor del diccionario
%dictInstDemoClassChar :: EmptyClass b => DemoClassD Char b
%dictInstDemoClassChar = DemoClassDict dictMethodmethod7Char
%
%-- definici�n del m�todo elevada
%dictMethodMethod7Char :: EmptyClass b => Char -> b -> Bool
%dictMethodMethod7Char x y = (x == 'a') || (method6 y)
%\end{lstlisting}

