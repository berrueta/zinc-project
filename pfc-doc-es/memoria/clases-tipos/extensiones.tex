%
% extensiones.tex
%

\section{Extensiones\label{seccion:clases-tipos:extensiones}}

La decisi�n de optar por las clases de tipos como una parte
fundamental del lenguaje Haskell fue arriesgada, debido a la falta de
experiencia. Para no incrementar los riesgos, el comit� eligi� la
propuesta m�s conservadora de entre las posibles.

En los a�os transcurridos desde entonces, las clases de tipos se han
ganado la simpat�a de muchos programadores e investigadores. �stos
�ltimos han desarrollado una importante actividad sobre este
campo. Como resultado, se han propuesto distintas extensiones a las
clases de tipos
b�sicas tal y como fueron descritas en \cite{wadler89how} y adoptadas
en la primera versi�n de Haskell.

El lenguaje Gofer\cite{jones94the} ha sido desarrollado en paralelo a
Haskell y utilizado como taller de pruebas de algunas de estas
extensiones\cite{jones92qualified}. En base a esa experiencia, se ha
llegado a
describir\cite{jones97exploring} de manera exhaustiva el espacio de
dise�o de las clases de tipos, es decir, todas aquellas posibles
extensiones del concepto b�sico de las clases de tipos. Obviamente
s�lo un subconjunto de �stas tiene aut�ntico sentido. En los apartados
siguientes se examinan las m�s extensiones m�s relevantes, aunque
existen otras
muchas\cite{rossberg-beyond,hinze99generic,neubauer02functional,shields01object,hinze00derivable}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Clases constructoras}

Una de las m�s �tiles ya fue descrita en esta secci�n (concretamente
en el apartado
\ref{apartado:clases-tipos:aplicaciones:sobrecarga-map}). Se trata de
las clases constructoras\cite{jones95system}. Por tratarse de una
extensi�n natural y evidentemente pr�ctica, fueron adoptadas por el
lenguaje Haskell a partir de su versi�n Haskell
1.3, y por supuesto siguen vigentes en el est�ndar
actual\cite{Haskell98Report}. Tambi�n el prototipo desarrollado para
este proyecto incorpora las clases constructoras.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{M�todos por defecto\label{apartado:clases-tipos:extensiones:metodos-defecto}}

Con el modelo propuesto, la declaraci�n de los m�todos est� separada
de su definici�n. La primera se encuentra en la declaraci�n de la
clase, la segunda en la instanciaci�n. Sin embargo, en ocasiones la
implementaci�n de los m�todos no var�a para la mayor�a de los
tipos. Por ejemplo, considerando la clase de tipos \lstinline{Ord}:

\begin{lstlisting}
-- definici�n de la clase de tipos Ord
class Ord a where
  (<)  :: a -> a -> Bool
  (>=) :: a -> a -> Bool

-- instanciaci�n de la clase de tipos Ord para los enteros
instance Ord Int where
  x <  y  = primLessThanInt
  x >= y  = not (x < y)

-- instanciaci�n de la clase de tipos Ord para los reales
instance Ord Float where
  x <  y  = primLessThanFloat
  x >= y  = not (x < y)
\end{lstlisting}

Resulta obvio que proporcionando la implementaci�n de uno de los dos
m�todos, se est� aportando informaci�n suficiente para definir el
otro. Para eliminar la redundancia, se introducen los m�todos por
defecto. Conceptualmente, consisten en proporcionar, junto con la
declaraci�n de un m�todo, una definici�n para el mismo. Al mismo
tiempo, la definici�n del m�todo durante la instanciaci�n se convierte
en opcional. Si est� ausente, se asumir� la definici�n adjunta a la
declaraci�n, mientras que si existe, se ignorar� la definici�n adjunta
a la declaraci�n. El ejemplo anterior adoptar�a esta forma:

\begin{lstlisting}
class Ord a where
  (<)  :: a -> a -> Bool
  (>=) :: a -> a -> Bool
  -- definici�n de (>=) adjunta a la declaraci�n
  x >= y  = not (x < y)

instance Ord Int where
  x <  y  = primLessThanInt
  -- omisi�n de la definici�n de (>=)

instance Ord Float where
  x <  y  = primLessThanFloat
  -- omisi�n de la definici�n de (>=)
\end{lstlisting}

En este tipo de situaciones, es habitual que aparezcan m�todos por
defecto con recursividad indirecta, que s�lo puede ser resuelta
mediante la definici�n particular de alg�n m�todo que rompa el
bucle. Aqu� se muestra un ejemplo:

\begin{lstlisting}
class Ord a where
  (<)  :: a -> a -> Bool
  (>=) :: a -> a -> Bool
  -- definiciones de (<) y de (>=) adjuntas a la declaraci�n
  -- atenci�n: recursividad indirecta
  x <  y  = not (x >= y)
  x >= y  = not (x <  y)

instance Ord Int where
  -- definici�n de (>), tiene precedencia sobre la definici�n por defecto
  x <  y  = primLessThanInt
  -- omisi�n de la definici�n de (>=)
  -- la recursividad indirecta queda rota

instance Ord Float where
  -- omisi�n de ambas definiciones
  -- �atenci�n! la evaluaci�n de los operadores (<) o (>=)
  --   sobre operandos reales provocar� un bucle infinito
\end{lstlisting}

La segunda instanciaci�n pretende mostrar el problema. No existe
ning�n mecanismo para forzar al programador a proporcionar
definiciones particulares para un cierto
subconjunto de m�todos, por lo que la precauci�n es la �nica forma de
evitar este tipo de errores.

Los m�todos por defecto est�n presentes en Haskell, pero no en el
prototipo desarrollado para este proyecto.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Instancias derivadas}

El lenguaje Haskell, que es la referencia cuando se trata de clases de
tipos, proporciona una comodidad sint�ctica para hacer m�s f�cil el
uso de esta caracter�stica. Se trata de la generaci�n autom�tica de
instanciaciones de determinadas clases de tipos para los tipos
algebraicos definidos por el programador. Debido a su naturaleza, esta
posibilidad s�lo se ofrece para un peque�o conjunto de clases de tipos
definidas en el est�ndar del lenguaje, pues se realiza de un modo
espec�fico para cada clase de tipos y cada constructor de tipos.

Por ejemplo, en el subapartado
\ref{subapartado:clases-tipos:descripcion:sobrecarga:igualdad} se
instanci� la clase de tipos \lstinline{Eq} para el tipo
\lstinline{BinTree}, lo cual requiri� implementar el operador
\lstinline{(==)}:

\begin{lstlisting}
-- definici�n del tipo algebraico BinTree
data BinTree a = Leaf
               | Branch a (BinTree a) (BinTree a)

-- instanciaci�n manual de Eq para BinTree
instance Eq a => Eq (BinTree a) where
  Leaf               == Leaf                = True
  Leaf               == Branch _ _ _        = False
  Branch _ _ _       == Leaf                = False
  Branch x sbx1 sbx2 == Branch y sby1 sby2  = (x == y) &&
                                              (sbx1 == sby1) &&
                                              (sbx2 == sby2)
\end{lstlisting}

La implementaci�n es tan inmediata que puede ser generada
autom�ticamente, para lo cual s�lo hay que especificarlo al definir el
tipo de datos. El siguiente fragmento reemplaza al anterior:

\begin{lstlisting}
-- definici�n del tipo algebraico BinTree (instanciaci�n autom�tica para Eq)
data BinTree a = Leaf
               | Branch a (BinTree a) (BinTree a)
               deriving Eq
\end{lstlisting}

El mecanismo de generaci�n de las instancias derivadas est� descrito
con precisi�n en el cap�tulo 10 de la definici�n del lenguaje
Haskell\cite{Haskell98Report}. Tambi�n se han
sugerido ampliaciones\cite{hinze99generic} del mismo.

Las instancias por defecto est�n contempladas en el prototipo
desarrollado para este proyecto.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Constructores de tipo con contexto}

Esta extensi�n, presente en Haskell (cap�tulo 4 de su
definici�n\cite{Haskell98Report}), consiste en incorporar un contexto
a los constructores de tipo definidos por el usuario. Este contexto se
propaga a todos los constructores de datos definidos para el tipo,
aunque se restringe al mayor subconjunto del contexto que restringe
s�lo las variables de tipo que intervienen en el constructor de
datos. El encaje de patrones con uno de estos constructores de datos
tambi�n introduce la parte correspondiente del contexto.

Para m�s detalles y alg�n ejemplo, puede consultarse la fuente citada.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Restricciones sobre expresiones de tipos}

Hasta ahora s�lo se han considerado restricciones o predicados sobre
variables de tipo (como en \lstinline{Eq a}).
Eventualmente, pueden surgir tambi�n restricciones
sobre constructores de tipos (\lstinline{Eq Int}), si bien estas
pueden evaluarse
inmediatamente y eliminarse (por ser ciertas) o transformarse en otras
restricciones.

No obstante, pueden plantearse restricciones sobre expresiones de
tipos m�s complejas. Por ejemplo, la siguiente expresi�n de tipo es
v�lida en Haskell:

\begin{lstlisting}
(Eq (f a), Functor f) => (a -> b) -> f a -> f b -> Bool
\end{lstlisting}

En este caso, la restricci�n \lstinline{Eq (f a)} no puede ser
simplificada, ya que las variables \lstinline{f} y \lstinline{a}
est�n cuantificadas universalmente.
Este tipo de restricciones plantean problemas a la hora de realizar la
conversi�n a diccionarios.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Clases de tipos multiparam�tricas\label{apartado:clases-tipos:extensiones:clases-multiparametricas}}

Tambi�n han sido citadas a lo largo de este cap�tulo las \emph{clases
de tipos multiparam�tricas}\cite{jones97exploring}. Aunque no forman
parte del est�ndar Haskell 98, s� que est�n soportadas por algunos
int�rpretes y compiladores (como GHC\cite{ghcmanual} o
Hugs\cite{hugsmanual}).

Las clases de tipos multiparam�tricas surgen como necesidad cuando se
pretende plantear determinadas clases de tipos. Algunos de estos casos
son:

\begin{itemize}

\item Modelado de estructuras matem�ticas (algebraicas) mediante
clases de tipos.

\item Bibliotecas de funciones y estructuras de datos para almacenar
colecciones de elementos (listas, conjuntos, �rboles, tablas
\toriginal{hash}).

\item Descripci�n de relaciones entre tipos (isomorfismos, por
ejemplo).

\item Modelado de una m�nada con estado, como se describi�
anteriormente.

\end{itemize}

Adem�s de los casos anteriores, durante la programaci�n de
aplicaciones generales, las clases de tipos
multiparam�tricas aparecen como soluci�n m�s natural para modelar
determinados comportamientos. Las clases de tipos monoparam�tricas no
son suficientes para estos prop�sitos. Por ejemplo, para describir una
colecci�n de elementos:

\begin{lstlisting}
-- clase de tipos monoparam�trica
class Collection c where
  empty  :: c a
  insert :: a -> c a -> c a
  delete :: a -> c a -> c a
  union  :: c a -> c a -> c a
\end{lstlisting}

El problema con la clase de tipos anterior es que no aporta ninguna
informaci�n sobre el tipo de los elementos de la colecci�n
(representado por la variable \lstinline{a} en el fragmento de
c�digo). Parece razonable que los elementos de una colecci�n deban
soportar al menos la operaci�n de igualdad, es decir, sus tipos
deber�an ser instancias de \lstinline{Eq}. O si la colecci�n es
ordenada, la operaci�n de ordenaci�n, y por tanto, la clase
\lstinline{Ord}. Para hacerlo, debe utilizarse una clase de tipos
multiparam�trica (en realidad, se ha descrito\cite{hughes99restricted}
otra forma de lograr el mismo fin por otra v�a):

\begin{lstlisting}
-- clase de tipos multiparam�trica
class Collection c a where
  empty  :: c a
  insert :: a -> c a -> c a
  delete :: a -> c a -> c a
  union  :: c a -> c a -> c a

-- para algunas colecciones, es suficiente lo siguiente
instance Eq a => Collection Set a where
  -- ...

-- otras colecciones requieren que los elementos puedan ser ordenados
instance Ord a => Collection BinTree a where
  -- ...
\end{lstlisting}

Otro ejemplo puede encontrarse en la necesidad de describir el
comportamiento de los dispositivos de entrada/salida. En este caso,
los par�metros son el manejador del dispositivo y el tipo de dato
le�do o escrito:

\begin{lstlisting}
class IODevice handle a where
  receive :: handle -> IO a
  send    :: handle -> a -> IO a
\end{lstlisting}

Por desgracia, las clases de tipos multiparam�tricas potencian la
aparici�n de ambig�edades de tipo. Dichas ambig�edades son
posibles sin que intervengan estas clases de tipos, pero surgen con m�s
facilidad cuando se emplean clases de tipos multiparam�tricas. No se
ha llegado a encontrar una soluci�n satisfactoria a este problema para
las clases de tipos monoparam�tricas\cite{odersky95second}, por lo que
parece a�n m�s dif�cil
hallarla para las clases de tipos multiparam�tricas.

Este es uno de los motivos por los cuales la adopci�n de las clases de
tipos multiparam�tricas va retrasada respecto a otras extensiones
como las clases constructoras.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dependencias funcionales}

Las \emph{dependencias funcionales} fueron
propuestas\cite{jones00type} como un medio de evitar las ambig�edades
que introducen las clases de tipos multiparam�tricas. Est�n inspiradas
en las bases de datos relacionales. Consisten en
permitir al programador anotar cada definici�n de una clase de tipos
multiparam�trica con un conjunto de dependencias funcionales que hacen
expl�citas ciertas relaciones entre los par�metros. Por cada nueva
instanciaci�n de una clase, el compilador debe verificar la
consistencia respecto a las dependencias funcionales.

Por ejemplo, el siguiente programa contiene una violaci�n de las
dependencias funcionales:

\begin{lstlisting}
-- clase de tipos con dependencias funcionales
class D a b | a -> b where
  -- ... los m�todos no son relevantes

instance D Bool Int where
  -- ...

instance D Bool Char where
  -- ...
\end{lstlisting}

La dependencia \lstinline{a -> b} indica que el par�metro
\lstinline{a} determina inequ�vocamente el par�metro
\lstinline{b}. Esta dependencia se puede interpretar en t�rminos de
bases de datos relacionales. Si cada instanciaci�n es considerada una
tupla en una tabla (donde cada campo es un par�metro de la clase de
tipos multiparam�trica), la dependencia anterior indica que el campo
\lstinline{a} es una clave primaria. Por esa raz�n, aunque las dos
instanciaciones del ejemplo son v�lidas por separado, no es posible
utilizarlas a la vez.

Las dependencias funcionales no est�n incluidas en la especificaci�n
oficial de Haskell, pero uno de los compiladores m�s conocidos (GHC)
las ofrece como extensi�n.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Instanciaciones solapadas}

Las instanciaciones solapadas se producen cuando una clase se
instancia para varios tipos que son unificables entre s�. Por ejemplo:

\begin{lstlisting}
-- clase de los tipos que son representables mediante una cadena
class Show a where
  show :: a -> String

-- instanciaci�n para el constructor de tipo de las listas
instance Show a => Show [a] where
  show = -- ...

-- instanciaci�n para las listas de enteros
instance Show [Int] where
  show = -- ...
\end{lstlisting}

Resulta evidente en este caso que \lstinline{[a]} y \lstinline{[Int]}
son unificables con s�lo sustituir la variable de tipo \lstinline{a}
por el constructor de tipo \lstinline{Int}.

Las instanciaciones solapadas permiten especializar m�s la
implementaci�n de los m�todos. Sin embargo, son una fuente de errores
de programaci�n, y obligan a determinar una estrategia para elegir, de
entre todas las posibles, la implementaci�n adecuada del m�todo.

Gofer permite realizar instanciaciones solapadas. Por el contrario,
Haskell no las soporta, aunque algunas implementaciones (Hugs) s� que
las contemplan.
