%
% tipos-semanticos.tex
%

\section{Lenguajes de datos y de tipos}

El analizador sem�ntico maneja dos lenguajes independientes:
un lenguaje de datos y un lenguaje de tipos. El inter�s de este
cap�tulo se concentra en la forma autom�tica de establecer una
relaci�n entre cada expresi�n concebible del lenguaje de datos y una
expresi�n del lenguaje de tipos. En otros t�rminos: calcular el tipo
de cada expresi�n de datos.

Como punto de partida, se comienza por la descripci�n de los dos
lenguajes.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Lenguaje de datos}

El lenguaje sobre cuyos t�rminos act�a la inferencia de tipos se
denomina \emph{lenguaje de datos}, y sus elementos,
\emph{expresiones de datos}.

En un lenguaje de programaci�n como \Curry{} o Haskell, el
lenguaje de datos es amplio\footnote{La gram�tica que describe el
lenguaje Zinc se encuentra en el tomo dedicado al dise�o.}, porque se
prima la expresividad y la comodidad del programador. Contienen, entre
otras construcciones, listas intensionales o encaje de patrones. En
realidad, muchas de las expresiones de datos de estos lenguajes
son facilidades sint�cticas, y todas pueden ser convertidas a un
lenguaje mucho m�s reducido, sin p�rdida de informaci�n\footnote{Como
ha quedado claro durante la escritura del prototipo que acompa�a a
este proyecto, la conversi�n de las expresiones de datos al lenguaje
reducido se topa con una dificultad: en caso de encontrar errores
sem�nticos en el programa despu�s de realizar la conversi�n, no
podr�an mostrarse mensajes de error significativos, porque las
expresiones originales habr�an sido transformadas y no ser�an
reconocibles por quien leyera el mensaje de error. Por tanto, el
prototipo no realiza esta conversi�n, y en cambio, efect�a la
inferencia sobre el lenguaje completo. Esto convierte la
implementaci�n de la inferencia en algo m�s complicado y engorroso de
lo que realmente es.}.

Por motivos obvios, en lo que sigue se emplea este lenguaje de datos
reducido, que constituye la base del $\lambda$-c�lculo. Su gram�tica
se describe en la figura \ref{figura:gramatica-expresiones}.
Tan s�lo la producci�n que contiene $\simboloCurry{let}$ resulta ajena
al $\lambda$-c�lculo, si
bien no pone en peligro las bases del modelo, ya que puede eliminarse
mediante una sustituci�n de t�rminos.

\begin{figure}[ht]
\begin{center}
\input{analisis-tipos/gramatica-expresiones}
\end{center}
\caption{\label{figura:gramatica-expresiones}Gram�tica del lenguaje de
datos y declaraciones}
\end{figure}

En este lenguaje destacan dos s�mbolos terminales: las variables ($x$)
y las constantes (denominadas aqu� \emph{constructores de datos},
$D$).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Lenguaje de tipos\label{apartado:analisis-tipos:lenguajes:lenguaje-tipos}}

La visi�n de los tipos que tiene el analizador sem�ntico es m�s amplia
que la que posee el usuario al escribir un
programa\footnote{El manual de usuario ofrece m�s detalles sobre la
visi�n del sistema de tipos que el lenguaje ofrece al programador, y
el documento de dise�o describe el sublenguaje de tipos que maneja el
programador.}.

El sistema de tipos etiqueta cada expresi�n de
datos de un programa con un \emph{tipo cuantificado} (en la
bibliograf�a inglesa se denomina \toriginal{type scheme}).
En un sistema de tipos Hindley-Milner
\cite{DamasMilner1982} extendido con clases de tipos, un tipo
cuantificado tiene la forma $\sigma$ descrita por la gram�tica de la figura
\ref{figura:gramatica-tipos-semanticos} (se utiliza la notaci�n de
\cite{faxen02static}).

\begin{figure}[ht]
\begin{center}
\input{analisis-tipos/gramatica-tipos-semanticos}
\end{center}
\caption{\label{figura:gramatica-tipos-semanticos}Gram�tica de los
tipos sem�nticos}
\end{figure}

Para que un tipo cuantificado sea v�lido, toda variable de tipo
$\alpha_i$ que est� libre en el
contexto $\theta$ debe estar tambi�n libre en propio tipo $\tau$.

Cada par $\Gamma_i\;\tau_i$ del contexto se denomina \emph{restricci�n
de tipo} (\toriginal{constraint}), aunque por su car�cter l�gico (una
restricci�n se puede verificar o no) se denominan a veces
\emph{predicados de tipo}.

Como se describe en la gram�tica, los tipos est�n formados por
s�mbolos de dos conjuntos fundamentales:

\begin{itemize}

\item \emph{Constructores de tipo}, que se comportan como
constantes. Un lenguaje de programaci�n puede tener algunos
constructores de tipo predefinidos, como los enteros o los caracteres,
pero tambi�n pueden ser introducidos por el programador (lo que
normalmente se llaman tipos de datos definidos por el usuario). De
todas formas, su origen no es relevante para su procesamiento.

\item \emph{Variables de tipo}. Son t�rminos reemplazables del
lenguaje de tipos.

\end{itemize}

Los elementos de estos conjuntos se combinan mediante la
\emph{aplicaci�n} de unos tipos sobre otros tipos, como indica la
tercera producci�n de la regla de los tipos. La aplicaci�n se
representa mediante la yuxtaposici�n de los dos tipos. Un ejemplo del
uso de esta operaci�n consiste en describir el tipo de las listas de
enteros como la aplicaci�n del constructor de listas (representado en
el lenguaje \Curry{} por el operador \lstinline{[]}) sobre la
constante de tipo entero (\lstinline{Int}), es decir,
\lstinline{[] Int}, o utilizando una notaci�n m�s familiar en este caso,
\lstinline{[Int]}. Otro constructor de tipo predefinido en \Curry{} es
el funcional, denotado por \lstinline{->}, y que habitualmente se
representa en notaci�n infija.

Para referirse a nombres de variables de tipo, se utilizan los
s�mbolos $u_1$, $u_2$\ldots. De la misma forma, $T_1$, $T_2$\ldots son
constructores de tipo, y $C_1$, $C_2$\ldots son clases de tipos.

% Definici�n: monotipo

\paragraph{Tipo monom�rfico o monotipo}
Un \emph{monotipo} es una expresi�n de tipo que no contiene variables
de tipo. Todos los tipos de los lenguajes de programaci�n
convencionales (C, Pascal, Java, etc.) son monotipos.

% Definici�n: polim�rfico

\paragraph{Tipo polim�rfico}
Una expresi�n del lenguaje de tipos es
\emph{polim�rfica} si en ella aparece alguna variable de tipo.

% Definici�n: tipo cuantificado monom�rfico

\paragraph{Tipo cuantificado monom�rfico}
Se dice que un tipo
cuantificado $\forall \bar{\alpha} . \theta \Rightarrow \tau$
es \emph{monom�rfico} respecto a una variable de
tipo $\alpha$ si esa variable est� libre en el tipo cuantificado.

% Definici�n: tipo cuantificado ambiguo

\paragraph{Tipo cuantificado ambiguo}
Un tipo cuantificado $\forall \bar{\alpha} . \theta \Rightarrow \tau$
se denomina \emph{ambiguo} si existe una variable de tipo $\alpha$ en
$\bar{\alpha}$ que aparece en $\theta$ pero no en $\tau$. La presencia
de tipos cuantificados ambiguos en un programa constituye un error
est�tico.

% Sustituci�n de tipos

\paragraph{Sustituci�n de tipos}
El lenguaje de tipos es un sistema de t�rminos, y por tanto,
sus expresiones son susceptibles de ser transformadas mediante
sustituciones. Una sustituci�n de tipos $S$ est� formada por un
conjunto de correspondencias entre variables de tipos y expresiones de
tipos, es decir,
$S =
\{(\alpha_1/\tau_1),(\alpha_2/\tau_2),\ldots,(\alpha_n/\tau_n)\}$.

La aplicaci�n de una sustituci�n de tipos se denota $S(\tau)$ (en
algunas fuentes, se prescinde de los par�ntesis), y representa a la
expresi�n de tipos resultante de reemplazar cada aparici�n de las
variables $\alpha_i$ en $\tau$ por su correspondiente $\tau_i$.

En la bibliograf�a se puede encontrar una notaci�n abreviada para la
descripci�n de sustituciones. Se trata de $[\tau'/\alpha]$, que
equivale a $\{(\alpha/\tau')\}$ (atenci�n al cambio de orden). La
aplicaci�n de estas sustituciones se representa $\tau[\tau'/\alpha]$.

% Renombramiento

\paragraph{Sustituci�n de renombramiento}
Si la sustituci�n $S$ hace corresponder variables a variables, entonces
se dice que $S$ es una sustituci�n de renombramiento.

% Instanciaci�n

\paragraph{Instanciaci�n de tipos}
Si $S$ es una sustituci�n de tipos y $\tau$ es un tipo, se dice que
$\tau' = S(\tau)$ es una instancia de $\tau$.

Si adem�s $S$ es una sustituci�n de renombramiento, se dice que
$\tau'$ es una instancia gen�rica de $\tau$ (la relaci�n es
bidireccional).

% Unificaci�n

\paragraph{Unificaci�n de tipos}
Se dice que $S$ es una sustituci�n de unificaci�n para dos tipos
$\tau_1$ y $\tau_2$ si $S(\tau_1) = S(\tau_2)$. Existen algoritmos
bien conocidos, como el de Robinson, que permiten calcular una
sustituci�n de unificaci�n �ptima para dos tipos, siempre que esta
exista. Si no existe una sustituci�n de unificaci�n para dos tipos, se
dice que �stos no son unificables.

Los conceptos de sustituci�n, instanciaci�n y unificaci�n de tipos
pueden ser extendidos sin dificultad a los tipos cuantificados.

% Generalizaci�n de tipos

\paragraph{Relaci�n de generalizaci�n}
Se puede definir entre los tipos una relaci�n de generalizaci�n. Un
tipo $\tau$ es m�s general que otro $\tau'$ si existe una sustituci�n
$S$ tal que $\tau' = S(\tau)$.

% Tipo director

\paragraph{Tipo director}
El tipo m�s general que se puede asignar a una expresi�n de datos
es llamado el \emph{tipo director} (\toriginal{principal type} en la
bibliograf�a en ingl�s) de la expresi�n. Una propiedad fundamental
de sistema de tipos Hindley-Milner es que el tipo director de
cualquier expresi�n de datos debe existir y ser �nico, de lo
contrario, el programa contiene un error est�tico.


