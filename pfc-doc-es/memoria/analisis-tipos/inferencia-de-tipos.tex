%
% inferencia-de-tipos.tex
%

\section{Inferencia de tipos}

La \emph{inferencia de tipos} consiste en deducir una relaci�n entre
las expresiones del lenguaje de datos y las expresiones del lenguaje
de tipos.

Si no es posible establecer la relaci�n (no puede calcularse, o no es
posible asignar un tipo director) entonces se considera que el programa
contiene un error de tipos, es decir, un error sem�ntico.

En esta secci�n se describe la inferencia de tipos de un modo formal y
abstracto, a trav�s de distintos artefactos de car�cter
matem�tico\cite{DamasMilner1982}. La
implementaci�n de la inferencia de tipos requiere la obtenci�n de un
algoritmo, que se deriva de las nociones expuestas aqu�\footnote{El
documento de dise�o describe los aspectos pr�cticos de la
inferencia.}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Asunciones}

Una \emph{asunci�n} $i : \sigma$ es una asociaci�n entre un elemento
$i$ de la uni�n entre las variables de datos y los constructores de
datos, y un tipo cualificado $\sigma$.

Se llama $A$ a un conjunto de asunciones donde cada variable de
datos aparece a lo sumo en una asunci�n, es decir,
$A = \{x_1 : \sigma_1, x_2 : \sigma_2, \ldots, x_n : \sigma_n\}$ con
todos los $x_i$ diferentes entre s�.

Se utiliza una notaci�n especial para denotar la agregaci�n de una
nueva asunci�n a un conjunto. La expresi�n $A.x:\sigma$ equivale a la
m�s convencional $A \cup \{x:\sigma\}$. Del mismo modo, la uni�n
de dos conjuntos de asunciones se representa $A.B$. La eliminaci�n de
cualquier asunci�n relativa a la variable de datos $x$ del conjunto
$A$ se indica mediante $A_x$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Juicios}

Un \emph{juicio} (en la bibliograf�a inglesa aparece como
\toriginal{judgement} o \toriginal{assertion}) tiene la forma $A
\vdash e : \sigma$ y representa la noci�n de que, tomando como v�lidas
las asunciones del conjunto $A$, puede deducirse que a la expresi�n de
datos $e$ le corresponde el tipo cualificado $\sigma$.

De una forma similar, el juicio con notaci�n $A \vdash d::B$
representa que, tomando como v�lidas las asunciones del conjunto $A$,
puede deducirse que la declaraci�n $d$ introduce un conjunto de
asunciones $B$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Generalizaci�n o cierre}

%Referencia: apuntes MIT

La \emph{generalizaci�n} (o \emph{cierre}) de un tipo es la operaci�n
que introduce el polimorfismo. Consiste en cuantificar las variables
de tipo de una expresi�n de tipo que est�n libres en la expresi�n,
pero no en las asunciones.

Se puede describir de la siguiente forma:

\begin{equation*}
Gen(A,\tau) = \forall \alpha_1 \ldots \alpha_n . \tau
\end{equation*}

donde $\alpha_1 \ldots \alpha_n$ son las variables de tipo libres en
$\tau$ pero no en las asunciones $A$. Si las variables libres de $A$
son $fv(A)$, esto quiere decir que $\alpha_i \notin fv(A)$ para
$i=1..n$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Reglas de inferencia}

Las \emph{reglas de inferencia} de tipos representan el conocimiento
acerca de
la relaci�n entre las expresiones de datos y las asunciones. Fueron
descritas por primera vez en \cite{DamasMilner1982}, y posteriormente
han sido ampliadas y perfeccionadas por otros autores. Tambi�n se
utilizan con otros fines, como la especificaci�n sem�ntica de
lenguajes\cite{labra01tesis}.

Se presentan en forma de un \emph{cat�logo} de reglas, cada una de
ellas etiquetada con un nombre. Uno de los cat�logos m�s completos se 
encuentra en \cite{faxen02static}, aunque en este caso no se trata de
reglas de inferencia de tipos, sino que se emplean para describir la
sem�ntica est�tica del lenguaje Haskell.

La figura \ref{figura:extracto-catalogo-reglas-basicas} contiene las
reglas de inferencia necesarias para realizar la inferencia sobre el
lenguaje de datos reducido. Por supuesto, para el lenguaje de datos
completo, se necesita un n�mero mayor de reglas.

\begin{figure}[htp]
\begin{center}
\input{analisis-tipos/extracto-catalogo-reglas-basicas}
\end{center}
\caption{\label{figura:extracto-catalogo-reglas-basicas}Cat�logo de
reglas de inferencia de tipos}
\end{figure}

La regla de inferencia de tipos m�s b�sica (denominada IDE o
TAUT en la bibliograf�a, nombres abreviados de identidad o
tautolog�a, aunque en esta obra se divide en dos a�n m�s b�sicas, que
se denominan IDE.VAR e IDE.CON) se deriva inmediatamente de la
definici�n de juicio enunciada anteriormente.
La interpretaci�n de la regla IDE.VAR es: �dado un conjunto de
asunciones formado por la inserci�n de $x : \sigma$ en $A$, se puede
deducir que la variable de datos $x$ tiene el tipo cualificado
$\sigma$�. La regla IDE.CON es similar, pero relativa a los
constructores de datos.

Sin embargo, estas reglas son tan triviales que no permite encadenar
el conocimiento (es decir, realizar series de deducciones por
inferencia). De forma
general, las reglas utilizan una notaci�n que
incluye una l�nea horizontal que representa una implicaci�n.

Las reglas COMB, ABS, LET y PAREN hacen referencia a
cada una de las
cuatro �ltimas producciones del s�mbolo $e$ (gram�tica de la figura
\ref{figura:gramatica-expresiones} en la p�gina
\pageref{figura:gramatica-expresiones}). Las dos primeras producciones,
correspondientes a las variables y a los constructores de datos,
quedan cubiertas por las regla IDE.VAR e IDE.CON ya comentadas.

Debe se�alarse aqu� que el operador infijo $\flechaFuncion$ que aparece en
las reglas COMB y ABS afectando a las expresiones de tipo, no forma
parte de la gram�tica de las expresiones de tipo (figura
\ref{figura:gramatica-tipos-semanticos}), sino que se trata de un
constructor de tipo que puede considerarse predefinido, y que se
denomina \emph{funcional}.

La generalizaci�n y la especializaci�n (o instanciaci�n) de tipos
polim�rficos est�n descritas por las reglas GEN y SPEC (tambi�n
llamada INST).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Proceso de inferencia usando reglas}

El objetivo del proceso de inferencia es obtener una relaci�n entre
las expresiones de datos y sus tipos. Cada relaci�n tiene la forma de
una asunci�n. La inferencia de tipos empleando las reglas descritas se
basa en el encadenamiento de las reglas, a partir de la informaci�n
conocida (como el axioma IDE.VAR) hasta llegar a deducir el tipo m�s
general de una expresi�n de datos.

Por ejemplo, el tipo m�s general de la funci�n identidad (cuya
formulaci�n mediante el lenguaje de datos es $\barraLambda{}x \;
\flechaFuncion\; x$) se obtiene empleando tan s�lo tres reglas:

\begin{equation*}
\inferenceRule{
\inferenceRule{\{x:\alpha\} \vdash x:\alpha}
{\{x:\alpha\} \vdash (\barraLambda{}x \; \flechaFuncion\; x) : \alpha
\flechaFuncion \alpha}
}
{\{x:\alpha\} \vdash (\barraLambda{}x \; \flechaFuncion\; x) : \forall\alpha.\alpha
\flechaFuncion \alpha}
\end{equation*}

En este ejemplo, se parte del axioma IDE.VAR, y se aplica primero la
regla ABS y despu�s la regla GEN para generalizar el tipo. Al
llegar a este punto, se puede afirmar que el tipo m�s general de la
funci�n identidad es $\forall\alpha.\alpha \flechaFuncion \alpha$.

Hay que hacer notar que el primer juicio contiene la asunci�n
$x : \alpha$. Esto es as� porque, inicialmente, se realiza una
correspondencia entre cada variable de datos (en este caso, $x$) y una
nueva variable de tipo ($\alpha$). Las variables $\alpha$ desempe�an
el papel de inc�gnitas del sistema.

Inicialmente, las asunciones tambi�n deben contener informaci�n sobre
los constructores de datos definidos por el lenguaje o por el usuario
(por ejemplo, $0$ es un constructor de datos de tipo entero, $3.14$ es
un constructor de datos de tipo real, y $True$ es un constructor de
datos de tipo booleano).

El tipo de la funci�n puede especializarse despu�s si es necesario
restringirla, por ejemplo, a los enteros. Esto lo logra la regla
[SPEC], partiendo del tipo m�s general obtenido anteriormente:

\begin{equation*}
\inferenceRule
{\{x:\alpha\} \vdash (\barraLambda{}x \; \flechaFuncion\; x) : \forall\alpha.\alpha
\flechaFuncion \alpha}
{\{x:int\} \vdash (\barraLambda{}x \; \flechaFuncion\; x) : int \flechaFuncion int}
\end{equation*}

La base fundamental detr�s del proceso no es otra
que el algoritmo de unificaci�n de Robinson.
La unificaci�n puede fallar
cuando se pretende hacer corresponder dos constructores de tipo
distintos (por ejemplo, los enteros y los caracteres), o cuando se
pretende instanciar una variable a un t�rmino que contiene la misma
variable.

Manteniendo el mismo punto de vista abstracto que se ha seguido hasta
ahora, el chequeo de tipos consiste en plantear un sistema de
juicios (donde las variables son variables de tipo) y
posteriormente resolverlo aplicando las reglas de inferencia para
obtener nuevos juicios a partir de los anteriores. Estos sistemas
pueden ser bastante
grandes. Por ejemplo, una funci�n para calcular el n�mero de elementos
de una lista genera un sistema de 25 juicios, que se resuelve
por reducci�n en 9 pasos (la descripci�n exhaustiva del ejemplo se
encuentra en \cite{cardelli87basic}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{El algoritmo $W$}

Para poder implementar la inferencia de tipos en un compilador, el
mecanismo abstracto de las reglas de inferencia debe ser convertido en
un algoritmo.

En la misma obra en la que se describi� el sistema de tipos
Hindley-Milner, los autores propusieron\cite{DamasMilner1982} un
algoritmo de inferencia llamado $W$. Este algoritmo parte de una
expresi�n de datos $e$ y un conjunto de asunciones $A$, y si tiene �xito,
produce una sustituci�n $S$ y el tipo $\sigma$ m�s general tal que
$S(A) \vdash e : \sigma$.

Los autores tambi�n demostraron la correcci�n y la completitud del
algoritmo\footnote{La implementaci�n de la inferencia que utiliza el
prototipo est� basada en este trabajo, pero sus detalles son descritos
en el documento de dise�o.}. El algoritmo $W$ ha sido ampliado
posteriormente por otros autores\cite{nipkow99type,nipkow93type}.