%
% principios-operacionales.tex
%

\section{Principios
operacionales\label{modelo-computacion:principios-operacionales}}

La existencia de distintas formas de combinar las caracter�sticas de
la programaci�n l�gica y la funcional ha conducido a la aparici�n de
varios \emph{principios operacionales}. Se trata de diferentes
estrategias que permiten al modelo computacional tratar las
evaluaciones de funciones insuficientemente instanciadas, es decir,
aquellas para las que alguno de sus argumentos es una variable l�gica
no ligada. M�s concretamente, determinan el comportamiento al alcanzar
un nodo $branch$ del �rbol definitorio de una funci�n.

Los dos principios operacionales, o estrategias, m�s utilizados
son\footnote{Se utilizar�n los t�rminos
en ingl�s para designar estos principios, ya que apenas existe
bibliograf�a en espa�ol. No obstante,
en \cite{Escobar98} se traducen como \emph{residuaci�n} y
\emph{estrechamiento}.}:

\begin{itemize}

\item \toriginal{Residuation}

\item \toriginal{Narrowing}

\end{itemize}

Los modelos de computaci�n de ciertos lenguajes l�gico-funcionales,
como Escher, Le Fun, Oz, NUE-Prolog o LIFE usan la
estrategia \toriginal{residuation}. Otros lenguajes
l�gico-funcionales, como ALF, Babel, K-Leaf, LPG o
SLOG utilizan en cambio la estrategia \toriginal{narrowing}. Una tabla
enumerando m�s lenguajes se encuentra en \cite{hanus94integration}.

El modelo unificado de \Curry{} propone poner ambos principios a
disposici�n del programador, y permitirle que elija el m�s conveniente
para cada funci�n.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\toriginal{Residuation}\label{apartado:modelo-computacion:principios-operacionales:residuation}}

Se trata de un principio conservador, consistente en suspender la
evaluaci�n de una funci�n hasta que se disponga de suficiente
informaci�n como para realizar una evaluaci�n determinista. Los
predicados y su disyunci�n son los �nicos puntos donde se permite la
b�squeda no determinista.

Por ejemplo, considerando la definici�n usual de los n�meros naturales
mediante predicados:

\begin{equation*}
\begin{array}{l}
nat(0) \\
nat(s(X)) \Leftarrow nat(X)
\end{array}
\end{equation*}

y la definici�n de la funci�n \tqq{suma} de n�meros naturales:

\begin{align*}
0 + X_2 & \rightarrow X_2 \\
s(X_1) + X_2 & \rightarrow s(X_1+X_2) \\
\end{align*}

Usando el principio \toriginal{residuation}, el �rbol definitorio
correspondiente a la funci�n \tqq{suma} es:

\begin{equation*}
\begin{split}
branch ( & X_1 + X_2 , 1 , rigid , \\
         & rule ( 0 + X_2 \rightarrow 0 ) , \\
         & rule ( s(X_1) + X_2 \rightarrow s(X_1+X_2) ) ) \\
\end{split}
\end{equation*}

La evaluaci�n de una expresi�n como $Z+0$ (por ejemplo, en el contexto
de un predicado como $Z+0 \doteq s(0)$) es suspendida, ya que no se
dispone de informaci�n suficiente para decidir la condici�n del nodo
$branch$.

En cambio, la evaluaci�n de la disyunci�n $Z+0 \doteq s(0)
\wedge nat(Z)$ produce que la evaluaci�n de la funci�n \tqq{suma} sea
suspendida, hasta que el segundo predicado establece un valor para
$Z$ (concretamente, produce la ligadura $\sigma(Z) = 0$, debido a la
unificaci�n con el predicado $nat(0)$), e inmediatamente se reanuda la
evaluaci�n de la funci�n
\tqq{suma}. Finalmente se obtiene la soluci�n $(Z = s(0) \square true)$,
mientras que de otra forma el programa quedaba suspendido.

El principio operacional \toriginal{residuation} permite combinar la
evaluaci�n determinista de las funciones (programaci�n funcional) con
los predicados (programaci�n l�gica), proporcionando adem�s
caracter�sticas de programaci�n concurrente, donde los elementos de
sincronizaci�n son las variables l�gicas, o m�s concretamente, sus
ligaduras.

Sin embargo, este principio no es completo. No puede evaluar funciones
cuyos argumentos no est�n suficientemente instanciados (como en el
ejemplo anterior: $Z + 0 \doteq s(0)$). En conclusi�n, por
s� mismo, no se basta para proporcionar un modelo de computaci�n con una
sem�ntica operacional completa.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\toriginal{Narrowing}\label{apartado:modelo-computacion:principios-operacionales:narrowing}}

La idea de este principio consiste\cite{hanus94integration} en
instanciar de forma no
determinista las variables, si es necesario para continuar con la
evaluaci�n de una funci�n. De esta forma, las funciones (y no s�lo los
predicados, como en el caso anterior) tambi�n tienen un componente no
determinista.

El ejemplo $Z + 0 \doteq s(0)$ que causaba problemas con
\toriginal{residuation} se eval�a mediante \toriginal{narrowing}
produciendo $(Z = 0 \square 0 \doteq s(0))\; \vee\; (Z=s(Z') \square
s(Z'+0) \doteq s(0+0))$. Los siguientes pasos de reescritura conducen
a una v�a muerta por la primera alternativa, y a la soluci�n
$(Z=s(0)\square true)$ por la segunda alternativa.

La gran ventaja de \toriginal{narrowing} es su completitud, tanto en
el sentido funcional (las formas normales son obtenidas siempre que
existan) como en sentido l�gico (las soluciones son obtenidas siempre
que existan). Sin embargo, es m�s costoso computacionalmente, ya que
crea m�ltiples espacios de b�squeda y realiza c�lculos
innecesarios. Sin
embargo, se puede refinar, dando origen a sus variantes
perezosas
(\toriginal{lazy narrowing}\cite{AFJV02}), de las cuales
la conocida como \toriginal{needed narrowing}
\cite{AntoyHanusMasseySteiner01PPDP,Jul00PhDT} resulta �ptima en
t�rminos
computacionales para cierto subconjunto de programas\footnote{Las
citas bibliogr�ficas ampl�an esta informaci�n.}.

La ventaja de \toriginal{needed narrowing} es que s�lo realiza
instanciaciones cuando es realmente necesario. Por ejemplo, con la
definici�n del predicado \tqq{menor o igual que} de la secci�n
\ref{modelo-computacion:arboles-definitorios}, \toriginal{needed
narrowing} no instancia el segundo argumento en todos los casos. Este
comportamiento est� recogido de manera precisa en el �rbol
definitorio.

Una de las formas de implementar sistemas basados en
\toriginal{narrowing} es mediante Prolog, ya que en Prolog est�n
presentes varios de los elementos claves (variables l�gicas,
unificaci�n y no-determinismo) . Sin embargo, al utilizar
internamente el sistema de \toriginal{backtracking} del Prolog, estos
sistemas sacrifican la completitud. Otra forma habitual de implementar
\toriginal{narrowing} es mediante m�quinas abstractas, con el
consabido problema de eficiencia. Finalmente, tambi�n se han propuesto
\cite{AntoyHanusMasseySteiner01PPDP} implementaciones que generan
c�digo en un lenguaje imperativo, que luego puede ser eficientemente
compilado.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Elecci�n de un principio operacional\label{apartado:modelo-computacion:principios-operacionales:eleccion}}

Anteriormente, al tratar la generaci�n de nodos $branch$ en los
�rboles definitorios, qued� por resolver la cuesti�n de cu�l debe ser
el principio operacional empleado. Los nodos $branch$ tienen un
argumento que puede tomar los valores $rigid$ o $flex$, denotando
respectivamente los principios de \toriginal{residuation} y
\toriginal{narrowing}.

El modelo de computaci�n que se describe en este cap�tulo es un modelo
unificador y de integraci�n. Funciona como superconjunto de otros
modelos. Por ejemplo, el modelo de los lenguajes basados
exclusivamente en el principio de \toriginal{narrowing} se reproduce
si todos los nodos $branch$ de los �rboles definitorios tienen el
argumento $flex$. En cambio, el modelo de los lenguajes basados en el
principio de \toriginal{residuation} y en los que hay una estricta
distinci�n entre funciones y predicados, se obtiene haciendo que los
nodos $branch$ de los predicados tengan el argumento $flex$ y los
nodos $branch$ de las funciones tengan el argumento $rigid$.

Aunque esta segunda opci�n parece muy c�moda, puede producir efectos
no deseados. Para evitarlos, el programador deber�a tener la
posibilidad de establecer el principio para una funci�n o predicado
concreto. Por ejemplo, la funci�n de concatenaci�n de listas:

\begin{align*}
[] +\!+ L & \rightarrow L \\
[E|R] +\!+ L & \rightarrow [E|R +\!+ L]
\end{align*}

Obviamente, la funci�n permite, con ambos principios, concatenar
listas. Pero si, haciendo uso del criterio anterior, se utiliza el
principio \toriginal{residuation} para evaluarla, entonces no puede
usarse para partir listas, porque el predicado $L+\!+[b] \doteq
[a,b]$ producir�a la suspensi�n de la computaci�n.

Otros modelos salvan el problema introduciendo un predicado espec�fico
para dividir listas (que resulta redundante desde una perspectiva
declarativa) o convirtiendo la funci�n de concatenaci�n en un
predicado, lo que equivale a sacrificar las ventajas de integrar la
programaci�n funcional. Pero el problema desaparece si se permite al
programador elegir el principio operacional para la funci�n concreta.
El coste, por supuesto, es que el programador debe ser m�s consciente
de los detalles de la evaluaci�n de la funci�n y del modelo
subyacente; por ejemplo, debe darse cuenta de que el principio
\toriginal{narrowing} es m�s adecuado para la concatenaci�n de listas.
