%
% conceptos-fundamentales.tex
%

\section{\label{notacion-modelo-computacion}Conceptos fundamentales}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sistemas de reescritura}

Los \emph{sistemas de reescritura}\cite{dershowitz90rewrite} son
conjuntos de ecuaciones dirigidas (es decir, producciones)
utilizadas para realizar c�lculos mediante el reemplazo continuado de
subt�rminos de una expresi�n de partida por t�rminos equivalentes, es
decir, realizando reducciones.

El resultado irreducible de una secuencia de reescrituras se denomina
\emph{forma normal}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{S�mbolos}

En �lgebra, se denomina \emph{signatura} a un conjunto de
funciones. En este modelo, se considera una signatura particionada en
un subconjunto $\mathcal{C}$ de \emph{constructores de datos} y un
subconjunto $\mathcal{F}$ de \emph{funciones} definidas (tambi�n
llamadas \emph{operaciones}).

Las instancias (u \emph{ocurrencias}, en cierta bibliograf�a en
espa�ol) de estos dos subconjuntos son s�mbolos que se representan
$c/n \in \mathcal{C}$ y $c/n \in \mathcal{F}$, donde $n$ es la aridad
del s�mbolo (la \emph{aridad} es el n�mero de argumentos que recibe).

Este modelo considera que al menos existen dos constructores de datos:
$true/0$ y $false/0$. Que ambos pertenezcan al tipo $Bool$ no resulta
relevante para la computaci�n (pero s� para el an�lisis est�tico de
tipos).

Se denomina $\mathcal{X}$ al conjunto de \emph{variables}, cuyas
instancias se representan $X, Y, Z, \ldots$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{T�rminos}

El conjunto de \emph{t�rminos} ($t$) construidos con variables de
$\mathcal{X}$, constructores de datos y funciones se
representa $\mathcal{T}(\mathcal{C}\cup\mathcal{F},\mathcal{X})$.

El conjunto de \emph{t�rminos constructores} construidos con variables
de $\mathcal{X}$ y constructores de datos se representa
$\mathcal{T}(\mathcal{C},\mathcal{X})$. Se trata de un subconjunto de
$\mathcal{T}(\mathcal{C}\cup\mathcal{F},\mathcal{X})$, y todos sus
elementos est�n en forma normal (no son reducibles).

$\mathcal{V}ar(t)$ es el conjunto de variables
que aparecen en el t�rmino $t$. Se dice que un t�rmino es
\emph{lineal} si no contiene m�ltiples instancias de una misma
variable.

Un \emph{patr�n} es un t�rmino de la forma $f(t_1,\ldots,t_n)$ donde
$f/n \in \mathcal{F}$ y $t_1,\ldots,t_n \in
\mathcal{T}(\mathcal{C},\mathcal{X})$.

La expresi�n $raiz(t)$ denota el s�mbolo ra�z o cabecera del t�rmino
$t$.

Se dice que un t�rmino es \toriginal{operation-rooted}
(nombre para el cual no se ha encontrado traducci�n de consenso al
espa�ol)
si $raiz(t)$ es un s�mbolo de operaci�n (funci�n), es decir, $raiz(t)
\in \mathcal{F}$.
En caso contrario, se dice que es se encuentra en \emph{forma normal
en cabeza} (abreviado como HNF por el nombre en ingl�s:
\toriginal{head normal form}).

Para poder referenciar \emph{subt�rminos} dentro de un t�rmino $t$, se
define la \emph{posici�n} $p$ como una secuencia de n�meros naturales.
El subt�rmino de $t$ que ocupa la
posici�n $p$ se denota $t|_p$. El reemplazo de ese subt�rmino por otro
t�rmino $s$ se representa $t[s]_p$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sustituciones}

Se consideran las \emph{sustituciones} de variables de la forma:

\begin{equation*}
\sigma(X_i) = \left\{
\begin{array}{rl}
  t_i & i=1,\ldots,n \\
  X_i & \mbox{para todas las dem�s variables}
\end{array}\right.
\end{equation*}

La \emph{sustituci�n identidad} se representa $id$.

Las sustituciones se pueden generalizar a morfismos sobre t�rminos si
para cada t�rmino
$f(t_1,\ldots,t_n)$ se realiza $\sigma(f(t_1,\ldots,t_n)) =
f(\sigma(t_1),\ldots,\sigma(t_n))$.

Un t�rmino $t'$ es una \emph{instancia} de $t$ si existe una
sustituci�n $\sigma$ tal que $\sigma(t) = t'$.

Un \emph{unificador} de dos t�rminos $t_1$ y $t_2$ es una sustituci�n tal
que $\sigma(t_1)=\sigma(t_2)$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Reglas de reescritura}

Se dice que $l \rightarrow r$ es una \emph{regla de reescritura} si
$l$ es una patr�n lineal y adem�s se cumple que $\mathcal{V}ar(r)
\subseteq \mathcal{V}ar(l)$. Los t�rminos $l$ y $r$ se denominan,
respectivamente, \emph{parte izquierda} y \emph{parte derecha} de la
regla de reescritura.

Se dice que una regla de reescritura es una \emph{variante} de otra si
puede obtenerse de aquella simplemente cambiando unas variables por
otras.

Un \emph{programa l�gico-funcional} es un \emph{sistema de reescritura
de t�rminos} $\mathcal{R}$ formado por reglas de reescritura. En este
modelo se establece la condici�n adicional de
que $\mathcal{R}$ s�lo contenga superposiciones triviales
(\toriginal{trivial overlaps}), es decir, si $l_1 \rightarrow r_1$ y
$l_2 \rightarrow r_2$ son variantes y $\sigma$ es un unificador para
sus t�rminos izquierdos ($\sigma(l_1) = \sigma(l_2)$), entonces
$\sigma$ tambi�n es un unificador de sus t�rminos derechos
($\sigma(r_1) = \sigma(r_2)$). Esta condici�n adicional recibe el
nombre de \emph{ortogonalidad d�bil}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Pasos de
reescritura\label{modelo-computacion:conceptos-fundamentales:pasos-de-reescritura}}

Un \emph{paso de reescritura} o \emph{reducci�n} consiste en la
aplicaci�n de una regla de reescritura a un t�rmino $t$, y se denota
$t \rightarrow_\mathcal{R} s$. Para que sea posible ejecutar un paso
de reescritura, debe existir
una regla de reescritura $l \rightarrow r \in \mathcal{R}$, una
posici�n $p$ en $t$ y una sustituci�n $\sigma$ tales que $t|_p =
\sigma(l)$ y $s = t[\sigma(r)]_p$.

Si no es posible aplicar a un t�rmino ninguna regla de reescritura, se
dice que ese t�rmino es \emph{irreductible}, o que est� en
\emph{forma normal}. No se requiere que el sistema de reescritura de
t�rminos de un programa l�gico-funcional termine siempre, por lo que
pueden no existir formas normales.

El predicado de igualdad ($\doteq$) se define como \emph{igualdad
estricta}, es decir, se verifica que $t_1 \doteq t_2$ si $t_1$ y
$t_2$ son reducibles a la misma forma normal. Este tipo de igualdad
puede ser definida mediante reglas de reescritura dentro del modelo.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Expresi�n respuesta}

Las variables l�gicas (definidas en el apartado
\ref{apartado:programacion-declarativa:programacion-logica:variables-logicas})
pueden formar parte de las
expresiones. Su presencia en una expresi�n provoca que �sta pueda ser
reducida a distintos valores ligando las variables l�gicas a
distintos t�rminos.

Es aqu� donde aparece una de las diferencias fundamentales de la
programaci�n l�gica y la funcional. La programaci�n funcional busca el
resultado de una expresi�n (un valor). La programaci�n l�gica busca en
cambio las diferentes ligaduras de las variables (las
\emph{respuestas}).

En este modelo integrador, se consideran la \emph{expresi�n
respuesta}, formada por un par $\sigma \square e$, donde $\sigma$ es
una sustituci�n que contiene la respuesta y $e$ es la expresi�n
resultado. Una expresi�n respuesta est� \emph{resuelta} cuando $e$ es
un t�rmino constructor.

Por cuestiones de comodidad notacional, en ocasiones se omite la
sustituci�n si �sta es la identidad. Es decir, se abrevia $id \square
e$ como $e$.

En general, puede existir m�s de una \emph{expresi�n respuesta} para
una expresi�n inicial que contenga variables libres. Por tanto, se
define la \emph{expresi�n disyuntiva} como un conjunto de expresiones
$\{ \sigma_1\square{}e , \ldots , \sigma_n\square{}e_n \}$, aunque
tambi�n se puede escribir $(\sigma_1\square{}e_1) \vee \cdots \vee
(\sigma_n\square{}e_n )$. El conjunto de todas las expresiones
disyuntivas se denomina $\mathcal{D}$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Pasos de computaci�n}

Un \emph{paso de computaci�n} realiza una reducci�n en una expresi�n
no resuelta de una disyunci�n. Un paso de computaci�n puede ser
determinista (en ese caso, la expresi�n es reemplazada por otra nueva
expresi�n) o no determinista (la expresi�n es reemplazada por una
disyunci�n de nuevas expresiones).

Por ejemplo, si la funci�n $p$ (el uso de la letra $p$ en lugar de la
$f$ es para resaltar que se trata de un predicado, es decir, una
funci�n de tipo $Boolean$) est� definida por las siguientes reglas de
reescritura:

\begin{align*}
p(a) & \rightarrow true \\
p(b) & \rightarrow true \\
\end{align*}

Entonces el resultado de evaluar $p(a)$ es $true$ (comportamiento
determinista), pero el resultado de evaluar $p(X)$ es $(X\; =\;
a\square{}true) \vee (X\;=\;b\square{}true)$.

Se aplica el principio de la evaluaci�n perezosa
(definido en el apartado
\ref{apartado:programacion-declarativa:programacion-funcional:evaluacion-perezosa}),
y por tanto, se selecciona para
evaluaci�n la expresi�n m�s exterior y m�s a la izquierda. Sin
embargo, hay funciones que s�lo pueden evaluarse si sus argumentos
est�n suficientemente instanciados para poder aplicar una reducci�n
determinista. Por tanto, la evaluaci�n de algunas funciones m�s
exteriores y m�s a la izquierda es retrasada. La estrategia concreta a
seguir para cada funci�n se especifica con precisi�n mediante los
�rboles definitorios.
