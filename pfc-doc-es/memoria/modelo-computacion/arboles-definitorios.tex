%
% arboles-definitorios.tex
%

\section{�rboles definitorios\label{modelo-computacion:arboles-definitorios}}

Al llegar a este punto, se requiere un artefacto que describa con precisi�n
la sem�ntica computacional de la definici�n de una funci�n. Con este
fin, se utilizan los �rboles definitorios. La descripci�n que sigue es
la propuesta en \cite{Hanus97POPL}, pero
estos �rboles, con distintas variaciones, ya han sido utilizados para
otros prop�sitos.

Un \emph{�rbol definitorio} es una estructura jer�rquica finita que
contiene todas las reglas de reescritura de una funci�n. Un �rbol
definitorio $\mathcal{T}$ con patr�n $\pi$ s�lo puede tener una de las
siguientes formas:

\begin{itemize}

\item $\mathcal{T}=rule(l \rightarrow{} r)$, donde $l \rightarrow r$
es una variante de una regla tal que $l=\pi$. Todas las hojas del
�rbol son de esta forma.

\item
$\mathcal{T}=branch(\pi,o,r,\mathcal{T}_1,\ldots,\mathcal{T}_k)$,
donde

\begin{itemize}
\item $o$ es una posici�n de una variable en $\pi$
\item $r \in \{rigid,flex\}$
\item $\forall i=1,\ldots,k$,
$\mathcal{T}_k$ es un �rbol definitorio con patr�n
$\pi[c_i(X_1,\ldots,X_n)]_o$, donde $c_i$ es un constructor de la
clase de $\pi|_o$, $n$ es la aridad de $c_i$ y $X_1,\ldots,X_n$ son
nuevas variables. Se exige que $\forall i,j\; /\; i \neq{} j \Rightarrow
c_i \neq c_j$.
\end{itemize}
 
\item $\mathcal{T}=or(\mathcal{T}_1,\ldots,\mathcal{T}_k)$, donde cada
$\mathcal{T}_i$ es un �rbol definitorio con patr�n $\pi$.

\item $\mathcal{T}=and(\mathcal{T}_1,\ldots,\mathcal{T}_k)$, donde
cada $\mathcal{T}_i$ es un �rbol definitorio con patr�n $\pi$.

\end{itemize}

Un �rbol definitorio de una funci�n $n$-aria $f$ tiene el patr�n
$\pi = f(X_1,\ldots,X_n)$, donde cada $X_i$ es una variable distinta. Con
$pattern(\mathcal{T})$ se denota el patr�n $\pi$ de un �rbol definitorio
$\mathcal{T}$.

Por cada regla $l \rightarrow r$ de la definici�n de una funci�n $f$ (por
tanto, $l$ tendr� la forma $f(t_1,\ldots,t_n)$) existir� un nodo
$rule(l' \rightarrow r')$ en el �rbol definitorio de esa funci�n,
donde $l'$ ser� una variante de $l$.

Un �rbol definitorio representa con precisi�n la estrategia de
evaluaci�n de una funci�n, concretamente, la selecci�n de la siguiente
regla de reescritura a aplicar. Comenzando por la ra�z, se desciende
por los nodos interiores hasta llegar a una (o varias) hojas. Las
hojas son siempre nodos $rule$, que indican la regla a aplicar.

Los nodos $branch$ requieren la toma de una decisi�n. Primero debe
evaluarse el subt�rmino indicado en el segundo argumento a una forma
normal (v�ase
\ref{modelo-computacion:conceptos-fundamentales:pasos-de-reescritura}).
Si el resultado no es una variable l�gica, se contin�a por la rama
apropiada seleccionada mediante encaje de patrones. Pero si el
resultado es una variable l�gica, se pueden adoptar dos
posibles estrategias, llamadas \toriginal{narrowing} y
\toriginal{residuation}, y que corresponden respectivamente a los
argumentos $flex$ y $rigid$. Estas estrategias son descritas en la
siguiente secci�n.

Los nodos $or$ aparecen cuando no existe un �nico argumento que
permita discernir la rama. Corresponde a situaciones con reglas que se
solapan, como en este ejemplo:

\begin{align*}
p(a,X) & \rightarrow true \\
p(X,b) & \rightarrow true
\end{align*}

Por �ltimo, los nodos $and$ representan la necesidad de evaluar m�s de
un argumento. S�lo son necesarios para permitir la concurrencia, ya
que siempre se puede construir un �rbol definitorio para cualquier
funci�n sin nodos $and$.

A continuaci�n, se presentan unos ejemplos de funciones y sus
respectivos �rboles definitorios.

La funci�n booleana (predicado) \tqq{menor o igual que} sobre los
n�meros naturales consta de las siguientes reglas de reescritura:

\begin{align*}
0 \leq X_2 & \rightarrow true \\
s(X_1) \leq 0 & \rightarrow false \\
s(X_1) \leq s(X_2) & \rightarrow X_1 \leq X_2
\end{align*}

Su �rbol definitorio (imponiendo una estrategia \toriginal{narrowing})
es:

\begin{equation*}
\begin{split}
branch ( & X_1 \leq X_2 , 1 , flex , \\
         & rule ( 0 \leq X_2 \rightarrow true ) , \\
         & \begin{aligned} branch ( & s(X_1) \leq X_2 , 2 , flex \\
                                    & rule ( s(X_1) \leq 0 \rightarrow false ) , \\
                                    & rule ( s(X_1) \leq s(X_2) \rightarrow X_1 \leq Y_2 )))
           \end{aligned}
\end{split}
\end{equation*}

Su interpretaci�n es la siguiente: para evaluar una expresi�n $t_1 \leq
t_2$, se eval�a $t_1$ hasta forma normal, ya que as� lo indica el
primer $branch$. Si $t_1$ se eval�a a $0$, entonces se desciende por
la primera rama, mientras que si es $s(\ldots)$, se desciende por la
segunda rama. Tambi�n podr�a tratarse de una variable l�gica, pero ese
caso se estudiar� despu�s.

La funci�n \tqq{multiplicaci�n por cero} se compone de dos reglas
que se solapan:

\begin{align*}
0   * X_2 & \rightarrow 0 \\
X_1 * 0   & \rightarrow 0
\end{align*}

Su �rbol definitorio (imponiendo una estrategia \toriginal{residuation}) es:

\begin{equation*}
\begin{split}
or ( & branch ( X_1 * X_2 , 1 , rigid , rule ( 0 * X_2 \rightarrow 0
)) , \\
     & branch ( X_1 * X_2 , 2 , rigid , rule ( X_1 * 0 \rightarrow 0
)))
\end{split}
\end{equation*}

Este �rbol especifica que la expresi�n $t_1 * t_2$ es reducible a $0$ si
alguno de los dos argumentos es reducible a $0$.

Los �rboles definitorios permiten describir estrategias de evaluaci�n
m�s avanzadas que el encaje de patrones perezoso de izquierda a
derecha usado, por ejemplo, en Haskell\cite{Haskell98Report}. Un
ejemplo aparece con la siguiente funci�n:

\begin{align*}
f(0,0) & \rightarrow 0 \\
f(X_1,s(N)) & \rightarrow 0
\end{align*}

Su �rbol definitorio es el siguiente (utilizando
\toriginal{narrowing}, aunque no es relevante para este ejemplo):

\begin{equation*}
\begin{split}
branch ( & f ( X_1 , X_2 ) , 2 , flex , \\
         & branch ( f ( X_1 , 0 ) , 1 , flex , rule ( f(0,0) \rightarrow
0 )) , \\
         & rule ( f ( X_1, s(N) ) \rightarrow 0 )) \\
\end{split}
\end{equation*}

Suponiendo que $\bot$ es una funci�n que no termina nunca, la
expresi�n $f(\bot,s(0))$ se reduce a $0$ usando el �rbol definitorio,
pero no termina nunca usando encaje de patrones (Haskell).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Generaci�n de �rboles definitorios}

La obtenci�n de un �rbol definitorio para una funci�n no es inmediata,
ya que es posible generar distintos �rboles para una misma funci�n,
utilizando distintas estrategias. Queda descartada, por engorrosa, la
posibilidad de que el programador especifique para cada funci�n su
�rbol definitorio. Por tanto, es necesario establecer criterios que
permitan generar los �rboles definitorios a partir de los lados
izquierdos de las reglas que definen una funci�n.

Distintas elecciones de criterios producen otros tantos algoritmos de
construcci�n de �rboles definitorios. El autor del modelo que se
presenta en estas p�ginas propone \cite{Hanus97POPL} dos algoritmos
muy similares, llamados $gt$ y $gt'$. Sus directrices generales son:

\begin{itemize}

\item Realizar encaje de patrones de izquierda a derecha.

\item Generar nodos $or$ en caso de conflicto entre constructores y
variables en la misma posici�n.

\end{itemize}

Se consigue as� reducir el n�mero de nodos $or$, lo que es deseable
(al reducir el indeterminismo, se aumenta la eficiencia). Sin embargo,
para reducir a�n m�s el n�mero de nodos $or$, es necesario relajar la
primera directriz (esta es la diferencia entre $gt$ y $gt'$).

S�lo resta por tomar la decisi�n de cu�ndo usar $flex$ y $rigid$ en
los nodos $branch$. Pero primero se describir�n los principios
operacionales subyacentes.
