%
% punto-partida.tex
%

\section{Punto de partida}

El primer dilema t�ctico con el que se enfrenta el ingeniero cuando
pretende resolver un problema es el de elegir el punto de partida m�s
conveniente. A grandes rasgos, son dos las alternativas: comenzar a
edificar su soluci�n en un terreno virgen, o hacerlo sobre un
artefacto preexistente. Suponiendo que la segunda opci�n existe (que
es el caso de este proyecto), se plantea una disyuntiva ante la cual
deben valorarse una multitud de factores, de los cuales los siguientes
son tan s�lo una muestra:

\begin{itemize}

\item �Son las dos alternativas viables dentro de los condicionantes
del proyecto? Cabe la posibilidad de que alguna de las opciones
exceda los presupuestos. Por ejemplo, puede ser tan compleja que
requiera demasiado esfuerzo para alcanzar los objetivos, u obligue a
recortar el alcance para ajustarla al presupuesto. En el caso de este
proyecto, ninguna de las dos alternativas ofrece esta dificultad, si
bien se estima que la segunda permite obtener un resultado m�s
completo porque no s�lo da satisfacci�n a los objetivos del
proyecto, sino que se mantienen las propiedades del artefacto
original.

\item �Es viable la segunda alternativa desde un punto de vista legal
y t�cnico? La mera existencia de alg�n artefacto preexistente no
garantiza que exista una aut�ntica libertad de elecci�n. Podr�a darse
el caso de que alg�n condicionante legal (en el caso de las
aplicaciones inform�ticas, se tratar�a de la licencia) o t�cnico
impidiese ejecutar la segunda opci�n. Por esa raz�n, en el desarrollo
de este proyecto en concreto, la b�squeda de los compiladores
preexistentes que pudieran ser candidatos a su adaptaci�n queda
condicionada por un criterio: la capacidad legal y t�cnica para
modificarlos. Es decir, s�lo se consideran aquellos compiladores con
licencias que permitan su modificaci�n (capacidad legal) y cuyo c�digo
fuente sea p�blico (capacidad t�cnica). Afortunadamente, los
compiladores ya existentes han surgido como proyectos de
investigaci�n, y sus autores han elegido un�nimemente licencias de
tipo \toriginal{software} libre.

\item �Cu�les son las dificultades m�s importantes de cada
alternativa? De una forma gen�rica, se puede considerar que la opci�n
que podr�a llamarse \tqq{construcci�n desde cero} presenta un mayor
volumen de trabajo neto, pero esta desventaja inicial puede llegar a
verse compensada por la libertad que concede: si se decanta por este
camino, el ingeniero podr� elegir a su conveniencia las herramientas
de trabajo con las que se encuentre m�s c�modo, as� como planificar y
ejecutar un dise�o creado a medida para alcanzar los objetivos
planificados, y s�lo esos. Trat�ndose de una aplicaci�n inform�tica,
esto significa que el ingeniero puede elegir un paradigma y un
lenguaje para el proyecto, y escoger las herramientas de desarrollo
con las que goce de mayor experiencia y seguridad.

La segunda opci�n,
en cambio, pese a que parte de un punto m�s cercano al destino, puede
encontrarse con dificultades que retrasen su avance. En primer lugar,
el ingeniero debe lograr una profunda compresi�n del compilador que
pretende modificar (de lo que se extrae un criterio de gran
importancia a la hora de elegir a �ste: la disponibilidad de
documentaci�n), y realizar las adaptaciones estructurales en caso
necesario. Estas adaptaciones resultan arriesgadas, ya que, si no se
posee un conocimiento �ntimo del compilador, o el dise�o del mismo es
defectuoso, los efectos laterales inherentes a la complejidad pueden
invalidar el trabajo.

\item �Cu�les son las diferencias m�s importantes entre las dos
alternativas? Posiblemente la diferencia m�s relevante sea la que
concierne a la metodolog�a a emplear. La primera alternativa permite
aplicar de forma muy directa las ense�anzas recibidas acerca del
desarrollo de aplicaciones inform�ticas. Por el contrario, usar como
base una aplicaci�n heredada requiere realizar una interpretaci�n
distinta y aplicar t�cnicas diferentes. La misma situaci�n afecta a
otro de los productos de un proyecto: la documentaci�n.

\item �Qu� alternativa proporciona mayores beneficios colaterales? Si
se asume que el objetivo pr�ctico de este proyecto consiste en
construir un \tqq{compilador para una versi�n de \Curry{} con clases de
tipos}, y se consideran colaterales el resto de
objetivos\footnote{Esta postura se ajusta a una visi�n del Proyecto
Fin de Carrera como un proyecto profesional, y no como un ejercicio
acad�mica. Bajo esa segunda perspectiva, la prioridad de los objetivos
ser�a distinta. El autor considera que ambas perspectivas son
v�lidas.}, �cu�l de las dos alternativas proporciona unos subobjetivos
m�s valiosos? Sin duda, una construcci�n desde cero proporciona una
visi�n m�s amplia de las distintas facetas de un compilador, y por
tanto, puede juzgarse que tiene un mayor valor did�ctico. Por otra
parte, la alternativa opuesta obliga a aprender nuevas t�cnicas y
conceptos (aquellos que vienen fijados por el compilador que se toma
como base) y tambi�n genera un producto m�s completo, como ya se
discuti� anteriormente. Esto �ltimo resulta muy deseable si se
pretende utilizar el resultado del proyecto como herramienta �til para
la ense�anza o para el trabajo.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Evaluaci�n de los posibles puntos de partida}

A continuaci�n se expone el resultado de la b�squeda y an�lisis de
los compiladores de \Curry{} existentes en el momento de comenzar el
desarrollo de este proyecto. Se incluyen tambi�n algunos int�rpretes,
ya que, aunque el objetivo del proyecto hace referencia a un
compilador, si se considera de forma m�s amplia, un int�rprete tambi�n
puede satisfacer la necesidad de una herramienta de desarrollo y
experimentaci�n.

Las alternativas encontradas son\footnote{Una lista actualizada de las
implementaciones existentes de \Curry{} es mantenida por Michael Hanus en
la p�gina
\url{http://www.informatik.uni-kiel.de/~mh/curry/implementations.html}}:

\begin{itemize}
\item M�nster Curry Compiler\cite{Lux03MCCUsersGuide}
\item Pakcs\cite{HanusPAKCSUserManual}
\item Narrowminder\cite{AntoyHanusMasseySteiner01PPDP}
\item Sloth\cite{Marino97Tesis}
\item TasteCurry
\item UPV-Curry\cite{AlpuenteEscobarLucasUPVCurry}
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{M�nster Curry Compiler}

El M�nster Curry Compiler (en lo siguiente, MCC) es un proyecto maduro
dirigido por Wolfgang Lux, de la Universidad de M�nster (Alemania). La
versi�n m�s reciente en el momento de escribir estas l�neas es la
0.9.7.

Consiste en un compilador de \Curry{} a lenguaje C, de forma que el
programa puede ser compilado de nuevo para generar un ejecutable,
enlaz�ndolo a un \toriginal{runtime} proporcionado por el
MCC. Junto con el MCC se adjunta una implementaci�n de las biblioteca
est�ndar (\Prelude{}) y otras de gran utilidad. El traductor est�
escrito en Haskell, por lo que es necesario disponer de un
compilador de este lenguaje para construir el MCC, y el
\toriginal{runtime} en C. El \toriginal{runtime} consiste en una
m�quina abstracta bien documentada.

Precisamente la documentaci�n disponible es uno de los puntos m�s
fuertes del MCC. Todo el c�digo fuente (tanto del traductor como del
\toriginal{runtime}) est� profusamente documentado en linea utilizando
el estilo de programaci�n literaria\cite{knuth92lp}. Otros de los
aspectos en los que destaca positivamente son:

\begin{itemize}

\item La optimizaci�n el c�digo generado. Seg�n los
\toriginal{benchmarks}
disponibles\cite{AntoyHanusMasseySteiner01PPDP,AEL-IDL99}, el MCC
genera los ejecutables m�s veloces.

\item La disponibilidad de un interfaz interactivo (\texttt{cyi}) y de
una herramienta para asistir en la construcci�n de aplicaciones con
varios m�dulos (\texttt{cymake}).

\end{itemize}

El MCC incorpora un buen n�mero de extensiones a la versi�n 0.8 del
lenguaje \Curry{}:

\begin{itemize}

\item Operador de desigualdad (\texttt{=/=}).

% \item Operadores de comparaci�n polim�rficos.

\item Aplicaciones parciales en restricciones de igualdad y
desigualdad.

\item Excepciones de entrada/salida, similares a las existentes en
Haskell.

\item Encaje recursivo de patrones.

\item Interfaz con funciones externas similar al FFI de
Haskell\footnote{El FFI (\toriginal{Foreign Function Interface}) est�
descrito en un \toriginal{addendum} al informe que describe el
lenguaje Haskell, y se puede consultar en la p�gina
\url{http://www.cse.unsw.edu.au/~chak/haskell/ffi/}}.

\item Especificaciones de importaci�n y exportaci�n de grano fino (a
nivel de constructores individuales).

\end{itemize}

Las principales carencias del MCC son:

\begin{itemize}

\item Imposibilidad de definir funciones con la anotaci�n
\lstinline|choice| (\toriginal{committed choice}), lo que impide
definir de forma completa
las principales funciones de b�squeda, como \lstinline|tryone|,
\lstinline|one| y \lstinline|condSearch|.

\item La ocultaci�n de identificadores no funciona correctamente y
produce colisi�n de nombres.

\item El rango de enteros est� limitado al de los enteros de $32$
bits, contraviniendo lo indicado en la descripci�n del lenguaje
\Curry{}, que establece un rango infinito.

\item La codificaci�n de caracteres en los programas est� limitada al
c�digo ASCII. No se permite el uso de caracteres Unicode.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Pakcs}

Bajo el paraguas de Pakcs se desarrollan en realidad varios proyectos
de la Universidad de Kiel (Alemania), dirigidos por Michael Hanus, que
es el responsable de las especificaciones del lenguaje \Curry{}. Dos
de ellos son el Curry2Prolog y el Curry2Java, y un tercero es el
TasteCurry, descrito en otro subapartado.

Tal y como sugiere su nombre, Curry2Prolog y Curry2Java son
traductores de \Curry{} a Prolog y Java, respectivamente. Aunque en
realidad, el primero requiere una implementaci�n particular de Prolog
(la del compilador comercial SICStus Prolog) y el segundo genera
c�digo para Pizza\cite{OderskyWalder97Pizza}, una extensi�n de
Java. Por tanto, ninguno de los dos son tan portables como en un
principio se podr�a considerar. De los mismos autores, hay un
traductor a C++ que permite generar binarios mucho m�s r�pidos (sus
creadores dicen que la ganancia es de un factor 50 respecto a la
versi�n que genera Java), pero no est� maduro a�n.

Del traductor a Prolog se puede se�alar que incorpora un gran n�mero
de ejemplos y bibliotecas que permiten, por ejemplo, programar
interfaces gr�ficos de usuario con \Curry{}, representar
gr�ficos, resolver restricciones de dominios finitos o escribir
aplicaciones distribuidas y concurrentes (algunos de estos aspectos
fueron cubiertos en el cap�tulo
\ref{capitulo:programando-con-curry}). Por desgracia, estas
bibliotecas no son
utilizables desde otras implementaciones de \Curry{} ya que dependen
de SICStus Prolog para realizar el interfaz con funciones
externas. Tambi�n se rodea de herramientas �tiles, como
CurryDoc (su prop�sito es similar a Javadoc) o Peval, que es un
evaluador parcial.

En cuanto al traductor a Java, su punto m�s fuerte est� en la
implementaci�n del paralelismo, que usa los hilos de Java.

En cuanto a las limitaciones m�s importantes de estos proyectos,
pueden enumerarse:

\begin{itemize}

\item Imposibilidad de definir funciones con la anotaci�n
\lstinline|choice| (\toriginal{committed choice}).

\item La b�squeda encapsulada tiene serias limitaciones, por ejemplo,
en la funci�n \lstinline|findall|.

\item Importantes limitaciones sint�cticas, que impiden utilizar
$\lambda$-funciones o expresiones \lstinline|let| en determinados
contextos.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Narrowminder}

Creado tambi�n 
bajo la batuta de Michael Hanus, el compilador Narrowminder es
peculiar en varios aspectos. En
primer lugar, es el �nico que est� implementado en \Curry{}. Es
peque�o pero muy completo, y genera c�digo Java, aunque no utiliza
hilos. Pero la caracter�stica m�s destacada es que s�lo consta de un
\toriginal{back-end}, y carece del \toriginal{front-end}. Para poder
hacer uso de �l, hay que alimentarlo con programas convertidos a
FlatCurry\cite{HanusFlatCurry}, una representaci�n intermedia (con
sintaxis XML) de \Curry{}, pero suficientemente flexible como para
representar otros lenguajes l�gico-funcionales.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Sloth}

La implementaci�n de \Curry{} de la Universidad Polit�cnica de Madrid
es un traductor a Prolog de baja eficiencia, que requiere tambi�n
SICStus Prolog. Sus propios autores consideran que se trata de una
implementaci�n altamente experimental, y su desarrollo actual parece
muy lento.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{TasteCurry}

Este int�rprete de \Curry{} ha sido desarrollado por las universidades
de Kiel y Portland, y est� implementado en Prolog. De nuevo, el
dialecto de Prolog debe ser el de SICStus. Sus
principales problemas son la baja eficiencia y ciertas violaciones muy
significativas de la especificaci�n del lenguaje (por ejemplo, ignora
las diferencias entre may�sculas y min�sculas
--\toriginal{case-insensitive}--).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{UPV-Curry}

Se trata de un int�rprete de \Curry{} escrito en SICStus Prolog en la
Universidad Polit�cnica de Valencia. Destaca por su buena
documentaci�n, por permitir trazas de la ejecuci�n y por incluir un
evaluador parcial. Tambi�n cuenta a su favor que haya sido empleado
para desarrollar al menos un aplicaci�n de cierto alcance (una
simulaci�n hidrogr�fica del r�o Segura). Por el otro lado, sus
debilidades consisten en la carencia de b�squeda encapsulada y la
imposibilidad de dividir las aplicaciones en m�dulos.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Elecci�n de un punto de partida}

Tras valorar las opciones disponibles (a saber: realizar una
implementaci�n totalmente nueva, o tomar como punto de partida alguna
de las arriba analizadas), se decide que el punto de partida del
prototipo sea el M�nster Curry Compiler, con las siguientes
justificaciones:

\begin{itemize}

\item El MCC es una de las dos implementaciones m�s completas que
existen de \Curry{} (la otra es Pakcs). De esta forma, el prototipo
hereda la posici�n de ventaja del MCC.

\item La documentaci�n disponible es exhaustiva. La importancia de
este aspecto no puede ser minusvalorada, ya que es necesario
adquirir un profundo conocimiento del MCC para poder afrontar su
reforma.

\item El MCC est� implementado en un lenguaje (Haskell) que el autor
de este proyecto conoce de antemano, eliminando as� una barrera de
entrada.

\item Sus dependencias respecto a software de terceros son las m�s
laxas. Para su utilizaci�n se requiere tan s�lo un compilador de C
(que puede considerarse ubicuo), si bien su compilaci�n requiere
adem�s una implementaci�n de Haskell. Gracias a estas dependencias m�s
relajadas, el MCC puede ser empleado en muchos sistemas tipo
UNIX (incluyendo Linux y MacOS X). El autor tambi�n ha conseguido
llevar con �xito el MCC al sistema operativo Microsoft Windows.

\item El MCC est� disponible bajo una licencia de tipo BSD\footnote{El
texto de la licencia BSD se incluye con el c�digo fuente del programa,
y tambi�n puede ser consultada en
\url{http://www.opensource.org/licenses/bsd-license.php}}, calificada
como \toriginal{open source}, lo que permite legal y t�cnicamente su
modificaci�n. No obstante, el autor de este proyecto se puso en
contacto con Wolfgang Lux con el objetivo de  solicitar expl�citamente
su permiso para emprender la obra. La respuesta no fue s�lo el
consentimiento sino tambi�n el apoyo.

\end{itemize}
