%
% gui.tex
%

\section{Interfaces gr�ficos de usuario\label{programando-con-curry:gui}}

Uno de los requisitos de un lenguaje de programaci�n de prop�sito
general es disponer de una biblioteca que permita crear interfaces
gr�ficos de usuario. Es sabido que los paradigmas declarativos tienen
problemas al tratar cuestiones de entrada/salida. Sin embargo, este
tipo de bibliotecas existen para lenguajes como Haskell, generalmente
como recubrimientos
(\toriginal{wrappers}) de bibliotecas programadas en otros lenguajes.

Este es el caso de la biblioteca que se presenta en esta
secci�n, descrita en \cite{Hanus00PADL}. Se trata de un recubrimiento
de la conocida Tcl/Tk, que permite construir interfaces gr�ficos de
usuario en lenguaje \Curry{} con un estilo declarativo de muy alto
nivel, aprovechando las caracter�sticas l�gicas y funcionales del
lenguaje.

Para manejar la comunicaci�n con la biblioteca Tcl/Tk, se utiliza de
nuevo el concepto de puerto (descrito en la secci�n
\ref{programando-con-curry:programacion-oo} y generalizado en la
secci�n \ref{programando-con-curry:programacion-distribuida}). La
primitiva \lstinline|openWish| crea una nueva ventana y devuelve un
puerto para comunicarse con el interfaz gr�fico, mediante funciones de
m�s alto nivel. El tipo del puerto es \lstinline|Port TkMsg|,
indicando que los mensajes enviados a trav�s de �l son de tipo
\lstinline|TkMsg|.

El contenido del interfaz gr�fico se especifica mediante una
estructura de datos del tipo \lstinline|TkWidget|. La disposici�n en el
interfaz de sus distintos componentes gr�ficos (\toriginal{widgets})
no se realiza en forma de coordenadas, sino de una forma l�gica,
mediante componentes contenedores, de manera similar a otras
bibliotecas gr�ficas como GTK+ o Java Swing.

La representaci�n del interfaz la realiza la funci�n
\lstinline|runWidgetOnPort|, que recibe un puerto (abierto con
\lstinline|openWish|) y una especificaci�n del interfaz. En
aplicaciones sencillas resulta �til la funci�n \lstinline|runWidget|
que se encarga de abrir el puerto y representar la interfaz, aunque en
aplicaciones m�s complejas es conveniente separar esta funcionalidad.

Todo lo descrito hasta ahora se puede observar en la siguiente
aplicaci�n:

\begin{lstlisting}
runWidget "Hola"
  (TkCol [] (TkLabel         [TkText "Hola, mundo"],
             TkButton tkExit [TkText "Salir"      ]])
\end{lstlisting}

Para reaccionar a las acciones del usuario sobre el interfaz, la
biblioteca permite asociar manejadores (\toriginal{handlers}) a los
eventos desencadenados por los componentes. En el ejemplo anterior, el
manejador predefinido \lstinline|tkExit| est� asociado a la acci�n de
pulsar el bot�n. Existen otros manejadores predefinidos que cubren
buena parte de las necesidades, no obstante el programador puede
definir sus manejadores, que
pueden ser de tipo \lstinline|Port TkMsg -> IO()| o 
\lstinline|Port TkMsg -> Success|, dependiendo de si el interfaz se
construye con
\lstinline|runWidget| o \lstinline|runWidgetOnPort|,
respectivamente. El
tipo\lstinline|TkWidget| es polim�rfico precisamente para permitir estas
dos posibilidades.

Es muy habitual que los manejadores de un evento deban hacer
referencia a otro componente gr�fico, para consultar su estado o
modificarlo. Muchas bibliotecas gr�ficas para lenguajes imperativos
resuelven este problema asignando un nombre simb�lico (una cadena) a
cada componente gr�fico, y utilizando estos nombres simb�licos para
establecer las referencias. Sin embargo, esta biblioteca explota las
caracter�sticas del lenguaje \Curry{} y utiliza variables l�gicas para
crear las referencias, lo que permite un estilo de programaci�n mucho
m�s declarativo. La t�cnica se ilustra mediante el siguiente ejemplo:

\begin{lstlisting}
runWidget "Demostraci�n contador"
  (let val free in
    TkCol [] [
      TkEntry [TkRef val, TkText, "0"],
      TkRow [] [
        TkButton (tkUpdate incrText val) [TkText "Incrementar"],
        TkButton (tkSetValue val "0"   ) [TkText "Reiniciar"  ],
        TkButton (tkExit               ) [TkText "Salir"      ]]])
\end{lstlisting}

Se asigna una referencia a un componente gr�fico mediante sus opciones
de configuraciones, descritas en una lista asociada al componente,
como se observa en el ejemplo anterior. Formalmente, la estructura de
datos que describe el interfaz est� descrita principalmente por estos
dos tipos:

\begin{lstlisting}
data TkWidget a = TkButton (Port TkMsg -> a) [TkConfItem a]
                | TkCheckButton              [TkConfItem a]
                | TkEntry                    [TkConfItem a]
                | TkLabel                    [TkConfItem a]
                | ...
                | TkRow [TkCollectionOption] [TkConfItem a]
                | TkCol [TkCollectionOption] [TkConfItem a]

data TkConfItem a = TkRef        TkRefType
                  | TkText       String
                  | TkWidth      Int
                  | TkBackground String
                  | TkCmd        (Port TkMsg -> a)
                  | ...
\end{lstlisting}

Los manejadores predefinidos a los que se hizo alusi�n anteriormente
tienen los siguientes tipos:

\begin{lstlisting}
tkExit     :: Port TkMsg -> IO ()
tkGetValue :: TkRefType -> Port TkMsg -> IO String
tkSetValue :: TkRefType -> String -> Port TkMsg -> IO ()
tkUpdate   :: (String -> String) -> TkRefType -> Port TkMsg -> IO ()
tkConfig   :: TkRefType -> TkConfItem a -> Port TkMsg -> IO ()
\end{lstlisting}

Tambi�n existen versiones de estos manejadores en forma de
restricciones y lugar de acciones de entrada/salida.

En el art�culo que describe esta biblioteca se puede encontrar
un ejemplo sorprendentemente sencillo de c�mo crear un interfaz
gr�fico que accede a una calculadora implementada como un
objeto. Otro ejemplo demuestra la forma de combinar los componentes
gr�ficos primitivos para construir componentes gr�ficos con
comportamientos m�s complejos.
