%
% caracteristicas-lenguaje.tex
%

\section{Explotando las caracter�sticas del lenguaje}

\Curry{} es un lenguaje que hereda las caracter�sticas m�s poderosas
de la programaci�n l�gica y de la programaci�n funcional. Gracias a
esta combinaci�n,
es posible escribir programas de un alto nivel declarativo, muy
f�ciles de leer y comprender, y por tanto, de corregir y ampliar.

En los siguientes apartados, se muestran ejemplos de c�mo sacar
partido a estas caracter�sticas fundamentales del lenguaje.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Programaci�n relacional}

Existen dos formas de representar un conocimiento relacional en
\Curry{}. En primer lugar, se puede hacer utilizando restricciones. El
resultado es un c�digo muy similar al programa an�logo en Prolog, pero
con tipos:

\begin{lstlisting}
-- Las personas son un tipo enumerado
data Person = Christine | Maria | Monica | Alice | Susan |
              Antony | Bill | John | Frank | Peter | Andrew

married :: Person -> Person -> Success
married Christine Antony  = success
married Maria Bill        = success
married Monica John       = success
married Alice Frank       = success

mother :: Person -> Person -> Success
mother Christine John   = success
mother Christine Alice  = success
mother Maria Frank      = success
mother Monica Susan     = success
mother Monica Peter     = success
mother Alice Andrew     = success

father :: Person -> Person -> Success
father f c = let m free in married m f & mother m c

grandfather :: Person -> Person -> Success
grandfather g c = let f free in father g f & father f c
grandfather g c = let m free in father g m & mother m c
\end{lstlisting}

Las expresiones funcionan de forma an�loga a las consultas en
Prolog. Por ejemplo, la expresi�n
\lstinline|let child free in father John child| produce la respuesta:

\begin{verbatim}
{child=Susan} | {child=Peter}
\end{verbatim}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Programaci�n l�gico-funcional}

Pero tambi�n es posible expresar el mismo conocimiento relacional
aprovechando las ventajas de la combinaci�n de la programaci�n l�gica
y la funcional. El siguiente programa es equivalente al ejemplo
anterior:

\begin{lstlisting}
data Person = Christine | Maria | Monica | Alice | Susan |
              Antony | Bill | John | Frank | Peter | Andrew

husband :: Person -> Person
husband Christine  = Antony
husband Maria      = Bill
husband Monica     = John
husband Alice      = Frank

mother :: Person -> Person
mother John    = Christine
mother Alice   = Christine
mother Frank   = Maria
mother Susan   = Monica
mother Peter   = Monica
mother Andrew  = Alice

father :: Person -> Person
father c  = husband (mother c)

grandfather :: Person -> Person -> Success
grandfather g c = g =:= father (father c)
grandfather g c = g =:= father (mother c)
\end{lstlisting}

La funci�n \lstinline|grandfather| es no-determinista. Si se eval�a
\lstinline|let c free in grandfather c =:= Antony|, el resultado es:

\begin{verbatim}
{c=Susan} | {c=Peter} | {c=Andrew}
\end{verbatim}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Explotando la evaluaci�n perezosa}

La evaluaci�n perezosa tiene especial inter�s en combinaci�n con
estructuras de datos infinitas (por ejemplo, listas, pero tambi�n
otras).

La definici�n de listas infinitas es muy sencilla:

\begin{lstlisting}
from :: Int -> [Int]
from n = n : from (x+1)

fibonacci :: [Int]
fibonacci = fibolist 0 1
          where fibolist x0 x1 = x0 : fibolist x1 (x0+x1)

-- lista de n�meros primos, mediante la criba de Erat�stenes
primes :: [Int]
primes = sieve (from 2)

sieve :: [Int] -> [Int]
sieve (x:xs) = x : sieve (filter (\y -> y `mod` x > 0) xs)
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Programaci�n concurrente con restricciones}

\Curry{} puede resolver problemas con restricciones en dominios
finitos de forma m�s eficiente que el mecanismo cl�sico en Prolog
(generar posibles soluciones y comprobar si son correctas). Como
muestra, se toma el problema cl�sico de coloreado de unas
superficies. En este ejemplo, se consideran 4 superficies, donde s�lo
la superficie 1 y la 4 no son adyacentes.

En primer lugar se define la restricci�n de desigualdad:

\begin{lstlisting}
diff :: a -> a -> Success
diff x y = (x==y)=:=False
\end{lstlisting}

Tambi�n se describen los colores:

\begin{lstlisting}
data Color = Red | Green | Yellow | Blue

isColor :: Color -> Success
isColor Red    = success
isColor Yellow = success
isColor Green  = success
isColor Blue   = success
\end{lstlisting}

Por �ltimo, dos restricciones describen una asignaci�n de colores (una
posible soluci�n) y comprueban su validez como soluci�n:

\begin{lstlisting}
coloring :: (Color,Color,Color,Color) -> Success
coloring (l1,l2,l3,l4) = isColor l1 & isColor l2 &
                         isColor l3 & isColor l4

correct :: (a,a,a,a) -> Success
correct (l1,l2,l3,l4) = diff l1 l2 & diff l1 l3 & diff l2 l3 &
                        diff l2 l4 & diff l3 l4
\end{lstlisting}

En primera instancia, se puede obtener una soluci�n aplicando el
patr�n cl�sico de la
programaci�n l�gica (\emph{generar y comprobar}):

\begin{lstlisting}
let sol free in coloring sol & correct sol
-- Resultado: {sol = (Red,Yellow,Green,Red)} Success
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Explotando el principio \toriginal{residuation}}

El problema del coloreado puede resolverse de una manera m�s eficiente
invirtiendo el orden de las restricciones:

\begin{lstlisting}
let sol free in correct sol & coloring sol
-- Resultado: {sol = (Red,Yellow,Green,Red)} Success
\end{lstlisting}

El motivo por el que esta segunda formulaci�n es m�s eficiente est� en
la concurrencia del lenguaje. La evaluaci�n comenzar� por la
restricci�n \lstinline|correct|, pero tras pocos pasos, esta rama tendr�
que suspenderse debido a que las variables no est�n ligadas, como
corresponde al principio operacional \toriginal{residuation} (apartado
\ref{apartado:modelo-computacion:principios-operacionales:residuation}).
La
evaluaci�n proseguir� por \lstinline|coloring|, que ligar� una a una las
variables. Pero tras cada ligadura, la evaluaci�n de
\lstinline|coloring| es suspendida y prosigue la de
\lstinline|correct|. Por tanto, se realiza
una comprobaci�n de la correcci�n de asignaciones parciales, no de
asignaciones completas, lo que reduce el espacio de b�squeda.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Explotando el principio \toriginal{narrowing}}

El principio operacional \toriginal{narrowing} (apartado
\ref{apartado:modelo-computacion:principios-operacionales:narrowing})
puede ser aprovechado para
escribir programas l�gico-funcionales muy expresivos. Por ejemplo, la
siguiente funci�n, presentada en \cite{AntoyHanus02CurryTutorial},
toma una lista de cinco cartas y determina si el
jugador tiene un \toriginal{poker} (cuatro cartas del mismo n�mero):

\begin{lstlisting}
pokerConstraint :: [a] -> a
pokerConstraint hand | hand =:= x++y:z &
                       map rank (x++z) =:= [r,r,r,r]
                       = r
                     where x,y,z,r free
\end{lstlisting}

Se trata de un buen ejemplo del empleo de variables l�gicas y el uso
de la funci�n flexible de concatenaci�n de listas descrita en
\ref{apartado:modelo-computacion:principios-operacionales:eleccion}
para partir una
lista en tres fragmentos, y posteriormente concatenar dos de los
fragmentos para formar un patr�n.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Explotando la b�squeda encapsulada}

La funci�n \lstinline{pokerConstraint} falla cuando el jugador no
tiene un
\toriginal{poker}. Para obtener  esa situaci�n indeseable, se pueden
utilizan procedimientos de b�squeda encapsulada. Por ejemplo:

\begin{lstlisting}
isPoker [a] -> IO ()
isPoker hand = putStrLn (if sorry then "No tiene poker"
                                  else "Poker de " ++ (show rank))
             where score = findall (\r -> pokerConstraint hand =:= r)
                   sorry = score == []
                   rank  = head score
\end{lstlisting}

En esta ejemplo, cabe apreciar que la funci�n \lstinline|head|
nunca falla, porque no llega a aplicarse sobre una lista vac�a en
ning�n caso, gracias a la evaluaci�n perezosa.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Explotando el no-determinismo}

En ocasiones, el no-determinismo puede ser empleado para eliminar una
estructura de datos. Por ejemplo, si la informaci�n sobre los d�as de
guardia de cada m�dico se codifica as�:

\begin{lstlisting}
oncall :: Doctor -> Day
oncall Joan = Monday
oncall Joan = Wednesday
oncall Richard = Monday
oncall Luc = Tuesday
\end{lstlisting}

Entonces se puede escribir un predicado que consulta si un doctor est�
de guardia cierto d�a:

\begin{lstlisting}
available :: Doctor -> Day -> Bool
available doctor day | oncall doctor == day = True
                     | otherwise            = False
\end{lstlisting}

De esta forma se elimina la necesidad de una estructura de datos que
almacene la informaci�n, y adem�s no es necesario codificar la
b�squeda en esa estructura.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Construcci�n de un analizador sint�ctico}

Otro ejemplo de la poderosa combinaci�n de las variables l�gicas y el
principio de \toriginal{narrowing} es la construcci�n de un analizador
sint�ctico \cite{AntoyHanus02CurryTutorial}. Si se considera una
gram�tica sencilla, como la de la figura
\ref{figura:gramatica-ejemplo-parser}.

\begin{figure}[ht]
\begin{center}
\framebox{
\begin{tabular}{rrl}
\noterminal{term} & \simbolo{::=} & \noterminal{identifier} \\
     & \simbolo{|}   & \noterminal{identifier} \terminal{(}
\noterminal{args} \terminal{)} \\
\\
\noterminal{args} & \simbolo{::=} & \noterminal{term} \\
     & \simbolo{|}   &  \noterminal{term} \terminal{,}
\noterminal{args} \\
\\
\noterminal{identifier} & \simbolo{::=} &
[cadena-no-nula-de-caracteres-alfab�ticos]
\end{tabular}
}% framebox
\end{center}
\caption{\label{figura:gramatica-ejemplo-parser}Gram�tica de un
peque�o lenguaje de demostraci�n}
\end{figure}

donde los programas pueden ser capturados mediante un �rbol sint�ctico
definido por esta estructura de datos:

\begin{lstlisting}
data Term = Term String [Term]
\end{lstlisting}

Las reglas de los s�mbolos no-terminales de la gram�tica se convierten
en funciones:

\begin{lstlisting}
import Char

parseTerm :: String -> Term
parseTerm s | all isAlpha s =:= True = Term s []
parseTerm s | s =:= fun ++ "(" ++ args ++ ")" &
              all isAlpha fun =:= True = Term fun (parseArgs args)
            where fun, args free

parseArgs :: String -> [Term]
parseArgs s | parseTerm s =:= result = [result]
            where result free
parseArgs s | s =:= term ++ "," ++ terms &
              parseTerm term =:= result = result : parseArgs terms
            where term, terms, result free
\end{lstlisting}

Se trata de una implementaci�n poco eficiente, pero pr�cticamente
inmediata a partir de la gram�tica.

