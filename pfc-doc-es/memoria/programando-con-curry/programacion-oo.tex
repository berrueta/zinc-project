%
% programacion-oo.tex
%

\section{Programaci�n orientada a objetos\label{programando-con-curry:programacion-oo}}

La programaci�n orientada a objetos no es incompatible con la
programaci�n declarativa, sino que se trata de conceptos
ortogonales. Es posible realizar programaci�n orientada a objetos en
\Curry{} de una manera muy declarativa, si bien para poder utilizar
caracter�sticas como la herencia y el polimorfismo es conveniente
servirse de una extensi�n del lenguaje denominada
\textsf{ObjectCurry}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Primera
aproximaci�n\label{programando-con-curry:programacion-oo:primera-aproximacion}}

En su forma m�s simple, un objeto puede representarse mediante una
estructura de datos que almacena su estado y una serie de operaciones
(funciones) que acceden o modifican su informaci�n. Al renunciar a los
efectos laterales, no es posible modificar realmente la informaci�n,
sino que se realiza una copia.

Desde un punto de vista abstracto, un objeto recibe una secuencia de
mensajes, y evoluciona en funci�n de ellos. Si la secuencia de
mensajes se representa por una lista, el comportamiento del objeto
puede ser descrito por una funci�n de tipo:

\begin{lstlisting}
State -> [Message] -> Success
\end{lstlisting}

El tipo \lstinline|Message| define los mensajes que un objeto puede
recibir. Por ejemplo, si se modela una cuenta bancaria como un objeto,
los mensajes son:

\begin{lstlisting}
data Message = Deposit  Int
             | Withdraw Int
             | Balance  Int
\end{lstlisting}

En este caso, el estado (\lstinline|State|) es tan s�lo un entero. Por
tanto, la funci�n anteriormente referida es:

\begin{lstlisting}
account :: Int -> [Message] -> Success
account eval rigid
account _ []                = success
account n (Deposit  a : ms) = account (n+a) ms
account n (Withdraw a : ms) = account (n-a) ms
account n (Balance  b : ms) = b=:=n & account n ms
\end{lstlisting}

Se trata de una funci�n recursiva, cuyo caso b�sico est� descrito en
la primera regla, y cada una de las tres siguientes reglas caracteriza
el comportamiento ante cada mensaje.

Para crear un nuevo objeto, se puede utilizar una restricci�n:

\begin{lstlisting}
makeAccount :: [Message] -> Success
makeAccount s = account 0 s
\end{lstlisting}

El par�metro \lstinline|s| es la lista de mensajes, que se denomina
\emph{flujo de mensajes} (\toriginal{stream}). Con esta restricci�n,
es posible crear un objeto y enviarle mensajes. Por ejemplo, la
evaluaci�n de:

\begin{lstlisting}
makeAccount s & s=:=[Deposit 200, Deposit 50, Balance b]
\end{lstlisting}

provoca que la variable l�gica \lstinline|b| termine ligada al valor
entero \lstinline|250|, que es el balance de la cuenta corriente tras
los dos dep�sitos. Se trata de una muestra m�s de las posibilidades
que brinda el uso de variables l�gicas, en este caso, para extraer la
informaci�n del estado del objeto.

Gracias a la evaluaci�n perezosa, no es necesario disponer
inicialmente del flujo completo de mensajes, sino que �ste puede
generarse
progresivamente. Por ejemplo, el siguiente programa ejerce de cliente
del objeto, y s�lo finaliza cuando el balance es de $50$
unidades monetarias. Si el balance es inferior, ingresa $70$, y si es
superior, retira $30$:

\begin{lstlisting}
-- Manda un mensaje a un objeto, introduci�ndolo en el stream
sendMsg :: Message -> [Message] -> [Message]
sendMsg msg obj | obj =:= msg:obj1 = obj1 where obj1 free

client :: [Message] -> Success
client s | s1 =:= sendMsg (Balance b) s =
  if b==50 then s1=:=[]
           else if b>50 then client (sendMsg (Withdraw 30) s1)
                        else client (sendMsg (Deposit  70) s1)
   where s1,b free
\end{lstlisting}

Si se eval�a la expresi�n

\begin{lstlisting}
makeAccount s & client (sendMsg (Deposit 100) s)
\end{lstlisting}

se obtiene la respuesta

\begin{verbatim}
{s = [Deposit 100, Balance 100, Withdraw 30, Balance 70,
      Withdraw 30, Balance 40, Deposit 70, Balance 110,
      Withdraw 30, Balance 80, Withdraw 30, Balance 50]}
\end{verbatim}

que es el flujo de mensajes originado por el cliente, y que termina
con el balance de $50$ unidades monetarias.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Env�o concurrente de mensajes}

Un problema que presentan los objetos modelados en el apartado
anterior es c�mo permitir el env�o concurrente de mensajes desde
varios clientes. La soluci�n es emplear un mecanismo que combine
varios flujos de mensajes en un �nico flujo, con el requisito de que
la mezcla debe ser \emph{justa}, pues de lo contrario se estar�a
perjudicando la concurrencia de los clientes. Es aqu� donde tiene
sentido la utilizaci�n de la anotaci�n
\lstinline|choice|\footnote{Acerca del significado de la anotaci�n
\lstinline|choice| y de su disponibilidad en las implementaciones de
\Curry{}, v�ase el manual de usuario.}. La funci�n \lstinline|merge|
es un mezclador justo:

\begin{lstlisting}
merge :: [a] -> [a] -> [a]
merge eval choice
merge [] ys    = ys
merge xs []    = xs
merge (x:xs) ys = x : merge xs ys
merge xs (y:ys) = y : merge xs ys
\end{lstlisting}

Esta funci�n permite que dos clientes env�en concurrentemente mensajes
al objeto, como en el siguiente ejemplo:

\begin{lstlisting}
makeAccount (merge s1 s2) & client1 s1 & client2 s2
\end{lstlisting}

Si existen m�s de dos clientes, es obvio c�mo se pueden combinar los
mezcladores para soportar todos los flujos que sean necesarios. Sin
embargo, esta soluci�n tambi�n tiene problemas:

\begin{itemize}

\item Se produce una importante sobrecarga en el env�o de mensajes
debido al proceso de mezcla. El tiempo de env�o de un mensaje no es
constante con respecto al n�mero de clientes.

\item Se producen complicaciones en el caso de que el n�mero de
clientes cambie din�micamente.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Puertos para acceder a objetos}

Para resolver estos problemas, distintos autores han propuesto
mecanismos variados.
Un an�lisis cr�tico de estos
mecanismos se puede leer en \cite{janson93ports}, que al mismo tiempo
especifica los requisitos del medio de comunicaci�n que resolver�a el
problema. En dicho art�culo se presenta el concepto de
\emph{puerto} (\toriginal{port} en el original), como soluci�n que
verifica los requisitos especificados.

Un \emph{puerto} es un par formado por un multiconjunto\footnote{Un
multiconjunto es un conjunto en el cual no s�lo es posible encontrar
elementos repetidos, sino que adem�s el n�mero de instancias de �stos
es relevante.}
de mensajes $P$ y un flujo de mensajes $S$ que est�n conectados entre
si. Por extensi�n, en ocasiones se identifica el conjunto $P$ con el
propio puerto.

Las dos operaciones b�sicas que se realizan sobre un puerto son:

\begin{itemize}

\item Crearlo: esta operaci�n se denomina $open\_port(p,s)$.

\item Enviar un mensaje al puerto: se denota $send(p,m)$, donde $m$
es el mensaje.

\end{itemize}

La idea b�sica del puerto es que el multiconjunto y el flujo
contienen exactamente los mismos mensajes. Los clientes utilizan la
operaci�n $send$ para enviar sus mensajes, y los objetos reciben los
mensajes a trav�s del flujo, como suced�a en el ejemplo del apartado
anterior.

La utilizaci�n de puertos soluciona los problemas descritos
anteriormente, y tiene otras ventajas adicionales:

\begin{itemize}

\item El tiempo de env�o de un mensaje es constante, no depende del
n�mero de clientes.

\item No se requieren mezcladores, ya que los clientes acceden a
trav�s del multiconjunto.

\item La extensi�n del modelo para su utilizaci�n en programaci�n
distribuida es muy simple, siendo necesario tan s�lo un mecanismo para
asignar un nombre simb�lico al puerto y recuperar un puerto a partir
de su nombre simb�lico.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Utilizaci�n de puertos desde \Curry{}}

Los puertos no son una caracter�stica b�sica del lenguaje \Curry{} (no
aparecen en su especificaci�n) ni parte de su biblioteca est�ndar,
pero est�n descritos en \cite{Hanus99PPDP} y ya existe alguna
implementaci�n que permite usar puertos desde \Curry{}\footnote{No es
el caso del MCC ni, por consiguiente, del prototipo desarrollado para
este proyecto. Se trata, en concreto, de PAKCS.}.

Un puerto es un tipo polim�rfico \lstinline|Port a|, donde
\lstinline|a| es el
tipo de los mensajes que se quieren enviar a trav�s de �l. Las dos
operaciones fundamentales descritas en el apartado anterior son
restricciones cuyos tipos son:

\begin{lstlisting}
openPort :: Port a -> [a] -> Success

send :: a -> Port a -> Success
\end{lstlisting}

Al reescribir el ejemplo de la cuenta bancaria, se observa que deja de
ser necesario mezclar los flujos, y que los clientes reciben ahora el
puerto en lugar del flujo:

\begin{lstlisting}
openPort p s &> makeAccount s & client1 p & client2 p
\end{lstlisting}

Por supuesto, es necesario reescribir tambi�n los clientes para que en
lugar de concatenar mensajes en el flujo, utilicen la restricci�n
\lstinline|send|. En cambio, la implementaci�n del objeto no sufre ninguna
modificaci�n.

Desde el punto de vista del lenguaje y del programador, los puertos
ofrecen las siguientes ventajas:

\begin{itemize}

\item La comunicaci�n se realiza mediante instanciaci�n de variables y
resoluci�n de restricciones, no mediante funciones con efectos
laterales. La integraci�n, por tanto, con la sem�ntica b�sica del
lenguaje es perfecta.

\item Mediante mensajes (parcialmente instanciados) con variables
libres se consigue extraer informaci�n del objeto de una manera
elegante. Esta t�cnica se ilustra en el m�todo \lstinline|Balance| del
ejemplo de la cuenta bancaria.

\item El programador no debe preocuparse del n�mero de clientes.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\textsf{ObjectCurry}}

Se ha propuesto \cite{HanusHuchNiederau00IFL} una extensi�n de
\Curry{} denominada \textsf{ObjectCurry} y concebida para facilitar la
programaci�n orientada a objetos. Se trata de una extensi�n
conservadora, en el sentido de que cualquier programa en
\textsf{ObjectCurry} puede ser transformado en un programa que s�lo
use las caracter�sticas b�sicas del lenguaje, aunque al utilizar
herencia se rompe la seguridad de tipos.

Esta extensi�n proporciona una sintaxis m�s c�moda para el programador
y menos propensa a errores. Se utilizan \emph{plantillas}
(\toriginal{templates} en el original), que a grandes rasgos equivalen
a las clases en otros lenguajes, aunque se realiza el cambio de
terminolog�a para evitar confusiones con otros conceptos como las
clases de tipos. Usando la sintaxis propuesta, el ejemplo de la
cuenta bancaria resultar�a:

\begin{lstlisting}
template Account =
constructor
  account    = n := 0
methods
  Deposit  a = n := n + a
  Withdraw a = n := n - a
  Balance  b = b =:= n
\end{lstlisting}

Los \emph{atributos} de la plantilla reciben sus valores iniciales al
mismo tiempo que se declaran en el constructor. No es necesario
declarar un tipo de datos para los \emph{m�todos}, como suced�a antes,
ya que \textsf{ObjectCurry} lo hace autom�ticamente. Tan s�lo es
necesario declarar su significado. Para hacerlo, se utiliza una
notaci�n similar a una asignaci�n (operador \lstinline|:=|), pero que en
realidad representa una transformaci�n del estado del objeto (no
existe la asignaci�n destructiva). Todos los atributos cuyo valor no
sea transformado expl�citamente en un m�todo permanecen
inalterados. Para permitir que los m�todos expresen todo tipo de
comportamientos, pueden incluir tambi�n restricciones, como en el
m�todo \lstinline|Balance|.

Las plantillas se instancian para crear objetos con la restricci�n
\lstinline|new|, que hace uso del constructor. Los mensajes se env�an
utilizando la restricci�n \lstinline|send|. Por ejemplo:

\begin{lstlisting}
new account o & (send (Deposit 100) &>
                 send (Withdraw 30) &>
                 send (Balance   b))
\end{lstlisting}

Los m�todos pueden referirse al propio objeto mediante el
identificador impl�cito \lstinline|self|, por ejemplo, para enviarse un
mensaje. El procesamiento de un mensaje no comienza hasta que termina
el anterior (no existe anidamiento), debido a la naturaleza
declarativa del lenguaje, sin asignaci�n destructiva ni efectos
laterales.

Las plantillas soportan herencia, aunque esta caracter�stica no puede
ser trasladada al lenguaje b�sico \Curry{} debido a que su sistema de
tipos no permite subtipos. En el mismo art�culo, los autores de
\textsf{ObjectCurry} proponen un nuevo sistema de tipos
extendido que permite disponer de herencia sin sacrificar la seguridad
de tipos.

La sintaxis para la herencia est� ilustrada en el siguiente ejemplo:

\begin{lstlisting}
template InterestAccount extends Account =
constructor
  interestAccount interestInit = account
                                 interest := interestInit
methods
  GetInterest i = i =:= interest
\end{lstlisting}

La nueva plantilla hereda los m�todos de su superplantilla, y puede
redefinir algunos de ellos, permitiendo as� el polimorfismo.