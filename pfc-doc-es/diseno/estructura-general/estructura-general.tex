%
% estructura-general.tex
%

Este cap�tulo constituye el primer acercamiento a la estructura del
compilador \Zinc{}, el prototipo construido para este proyecto. El
objetivo es describir los principales componentes
de la aplicaci�n y c�mo se disponen e interact�an a lo largo del
proceso de compilaci�n.

El compilador es una herramienta no-interactiva, pues no mantiene
conversaci�n alguna con el usuario. Toma un cierto n�mero de datos de
entrada y trabaja aut�nomamente hasta producir un conjunto de
resultados, o bien detenerse tras detectar un error en el an�lisis de
los datos de entrada.

Estos datos de entrada son de dos tipos: ficheros (de c�digo fuente o
de interfaces) y opciones que afectan al proceso de
compilaci�n\footnote{El manual de usuario describe cada una de las
opciones existentes.}.

Los datos de salida son programas en determinado estado de compilaci�n
a elecci�n del usuario, que t�picamente desear� realizar la cadena de
compilaci�n en su totalidad, y obtener un fichero ejecutable. Tambi�n
son datos de salida los mensajes de error, las especificaciones de
interfaces y determinada informaci�n de depuraci�n.

El compilador \Zinc{} es, en realidad, un traductor a C. Convierte los
programas en \Curry{} con clases de tipos en programas equivalentes
en C que hacen uso de un entorno de ejecuci�n
(\toriginal{runtime}). El entorno de ejecuci�n es compartido entre
todas las aplicaciones, pues se trata de una
biblioteca. Por tanto, para producir aplicaciones ejecutables, el
compilador Zinc se sit�a en la c�spide del conocido proceso de
compilaci�n de programas en C (figura
\ref{figura:esquema-general-proceso}). En la figura, se distinguen dos
grandes bloques. El que se encuentra en la cima es el compilador Zinc,
que realiza el paso del programa en \Curry{} al programa en C. Debajo
de �l se sit�a el compilador de C y el enlazador. Estos �ltimos
elementos no
forman parte de Zinc, sino que son tomados del sistema\footnote{Para
ocultar al usuario estas etapas, existe un gui�n
(\toriginal{script}) llamado \texttt{cyc} que se encarga de ejecutar
consecutivamente el compilador \texttt{cycc}, el compilador de C y el
enlazador. La forma de utilizar \texttt{cyc} est� descrita en el
manual del usuario. Conviene resaltar la diferencia entre \texttt{cyc}
(el gui�n) y \texttt{cycc} (el aut�ntico compilador).}.


\begin{figure}[p]
\begin{center}
\input{estructura-general/diagrama-proceso-completo}
\end{center}
\caption{\label{figura:esquema-general-proceso}Esquema general del proceso de compilaci�n y ejecuci�n}
\end{figure}


Junto con el compilador, se proporcionan otras herramientas, como el
entorno interactivo. En realidad, se trata de
envoltorios o presentaciones diferentes de la misma aplicaci�n, que
invocan repetidamente al compilador para obtener la interactividad.

El M�nster Curry Compiler (MCC) se distribuye con \textsf{mach}, un
int�rprete del c�digo para la m�quina abstracta, es decir, una
implementaci�n independiente del entorno de ejecuci�n. No obstante, se
trata de una aplicaci�n experimental, creada para la investigaci�n.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Principales componentes del compilador}

En los p�rrafos anteriores, se ha situado el compilador \Zinc{} en su
posici�n, y se han caracterizado sus entradas y salidas. Se ha
realizado, pues, una descripci�n de caja negra. Ahora ha llegado el
momento de examinar su estructura interna.

Como es habitual\cite{Cueva98Conceptos},
se considera una divisi�n en dos grandes bloques: el
\toriginal{front-end} y el \toriginal{back-end} (figura
\ref{figura:diagrama-compilador}). El primero de ellos se ocupa de las
labores de an�lisis de las entradas, mientras que el segundo realiza
la mayor parte de la s�ntesis de las salidas.


\begin{figure}[p]
\begin{center}
\input{estructura-general/diagrama-compilador}
\end{center}
\caption[Esquema general del funcionamiento del compilador]
{\label{figura:diagrama-compilador}Esquema general del
funcionamiento del compilador, con sus
principales m�dulos, las estructuras de datos y los ficheros externos
que intervienen en una compilaci�n}
\end{figure}


A lo largo de esta obra se presta atenci�n tan s�lo al
\toriginal{front-end}, porque es la parte sobre la que se act�a
durante el proyecto. En ella se realizan todas las adaptaciones
necesarias para agregar las clases de tipos al lenguaje. En el
\toriginal{back-end} se producen tan s�lo ajustes m�nimos,
impuestos por la reorganizaci�n de algunos m�dulos o el renombrado de
ciertas funciones de uso com�n. Y en el entorno de ejecuci�n tambi�n
se acometen simples renombramientos de funciones (primitivas).

Entre los dos grandes bloques se sit�a el lenguaje intermedio. Se
trata de un lenguaje l�gico-funcional (de alto nivel), muy similar al
$\lambda$-c�lculo. La misi�n del \toriginal{back-end} consiste en
transformar el programa descrito en este lenguaje (siempre en el
sentido de simplificarlo), para posteriormente reformularlo usando
instrucciones de una m�quina abstracta definida
\toriginal{ad-hoc}\cite{LuxKuchen:ATPS99,Lux:WFLP98},
que por �ltimo se convierten en lenguaje C\footnote{Otra m�quina
abstracta para implementar \Curry{} est� descrita en
\cite{HanusSadre99JFLP}.}.

No obstante, como ya se ha se�alado, el inter�s de esta obra se
concentra en el \toriginal{front-end}. La figura
\ref{figura:diagrama-compilador} se�ala sus principales componentes,
que se describen ahora brevemente, pues los siguientes cap�tulos
los tratan en profundidad:

\begin{itemize}

\item En primer lugar de la cadena se encuentran los tres analizadores
habituales: l�xico, sint�ctico y sem�ntico. Pese a su representaci�n
en el diagrama, no puede hablarse de procesos encadenados, sino
enlazados. Por ejemplo, cada vez que se importa un interfaz, se
realiza un an�lisis l�xico y sint�ctico sobre el mismo.

\item La importaci�n y exportaci�n de interfaces son los procesos que
permiten dividir los programas en m�dulos. Su misi�n es cargar en
memoria la informaci�n de los m�dulos (importaci�n) y generar una
especificaci�n del m�dulo actual (exportaci�n) para permitir que pueda
ser usado por otros m�dulos.

\item La generaci�n del lenguaje intermedio forma parte del
\toriginal{front-end}, pero ya no es un componente de an�lisis sino de
s�ntesis. Con la informaci�n sint�ctica (�rbol sint�ctico) y sem�ntica
recogida en las fases anteriores, produce un programa en lenguaje
intermedio. Su funcionamiento no requiere de adaptaciones para este
proyecto, por lo que no se encuentra descrito aqu� con tanto detalle
como el resto del \toriginal{front-end}.

\item La tabla de s�mbolos es un almac�n
temporal de la informaci�n que se extrae mediante los procesos de
an�lisis. En la pr�ctica, est� compuesta de varias
estructuras tabulares, que en la jerga de los compiladores de
lenguajes funcionales se denominan \emph{entornos}.

\end{itemize}

El diagrama de la figura \ref{figura:diagrama-compilador} persigue
la sencillez. Por esa raz�n, est� fuertemente
simplificado, obviando los intercambios de informaci�n que se producen
entre los distintos componentes, y concentr�ndose s�lo en la cadena
de compilaci�n\footnote{El diagrama considera la cadena de compilaci�n
desde un punto de vista conceptual, pues en la pr�ctica, al estar
implementada en Haskell, un lenguaje con evaluaci�n perezosa, �se
eval�a desde el final hacia el inicio!}.