%
% especificacion.tex
%

\section{Especificaci�n l�xica}

La especificaci�n l�xica del lenguaje \Curry{} est� incluida en el
ap�ndice C del informe que lo
describe\cite{Hanus03CurryReport}. Para no introducir una nueva
nomenclatura e inducir a confusi�n, en esta obra se emplean los mismos
nombres para etiquetar los \toriginal{tokens} y los s�mbolos no
terminales de la gram�tica libre de contexto (en la siguiente
secci�n). La
inclusi�n de las clases de tipos apenas altera esta especificaci�n. La
presente especificaci�n corresponde al lenguaje reconocido por \Zinc{}, y
no al est�ndar de \Curry{}. Para facilitar el contraste, se se�alan
los lugares donde existen diferencias.

La primera caracter�stica l�xica que debe hacerse notar es que el
lenguaje distingue entre may�sculas y min�sculas (es
\toriginal{case-sensitive}) en los identificadores y las palabras
reservadas.

Desde un punto de vista l�xico, los programas est�n compuestos por
distintos elementos:

\begin{itemize}

\item Identificadores.

\item Operadores.

\item Palabras reservadas.

\item Constantes. La especificaci�n del lenguaje define cuatro
\toriginal{tokens} para representar constantes, denominados
\noterminal{Char}, \noterminal{Int}, \noterminal{Float} y
\noterminal{String} (caracteres, enteros,
reales y cadenas de caracteres). Tambi�n indica que sus reglas l�xicas son
id�nticas a las de Java, por ejemplo: \verb|'a'|, \verb|3|,
\verb|3.14| y \verb|"Curry"|.

\item Comentarios. Como es habitual, existen dos tipos: los de una
�nica linea (comienzan por la cadena \verb|--| y
concluyen en el siguiente salto de linea) y los de m�ltiples l�neas,
delimitados por las cadenas \verb|{-| y
\verb|-}|. Estos �ltimos pueden ser anidados, siendo este un
punto diferenciador respecto a otros lenguajes. Los comentarios son
filtrados durante el an�lisis l�xico y carecen de efecto alguno en las
siguientes etapas del procesamiento.

\item Caracteres de puntuaci�n. Cada uno de ellos constituye un
\toriginal{token}. Son los siguientes:

\begin{verbatim}
( ) { } [ ] , ; ` _
\end{verbatim}

\item Caracteres no imprimibles, como saltos de linea, espaciados o
tabuladores. Estos caracteres tienen una gran importancia, ya que su
disposici�n influye en la sintaxis del programa. El sangrado es un
reemplazo c�modo y elegante de los delimitaciones de comienzo y fin de
bloque, y de separaci�n de unidades sint�cticas. Para una descripci�n
de las reglas de sangrado desde el punto de vista del programador,
debe consultarse la descripci�n del lenguaje \Zinc{} presente en el
manual de usuario. No obstante, en la especificaci�n sint�ctica del
lenguaje, que se aborda en la siguiente secci�n, se asume que el
sangrado ha sido convertido en \toriginal{tokens} para indicar
expl�citamente los bloques sint�cticos (se utilizan los
\toriginal{tokens} virtuales \verb|{|, \verb|;| y \verb|}|). Debe
incidirse en que esta sustituci�n se produce s�lo con el objetivo de
aclarar la especificaci�n, pero no forma parte de la cadena real de
procesamiento del compilador.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Identificadores}

A diferencia de otros lenguajes, como Haskell, que imponen unas
convenciones para los identificadores relativas al uso de may�sculas y
min�sculas\footnote{Un par de muestras: los identificadores de tipos
definidos por el usuario en Haskell deben comenzar por una letra
may�scula, mientras que las funciones definidas por el usuario deben
comenzar por una letra min�scula}, \Curry{} cede el poder de elecci�n
al usuario programador. Define cuatro \emph{modos},
seleccionables por el usuario del compilador, si bien tanto el M�nster
Curry Compiler como \Zinc{} s�lo incorporan el llamado \emph{modo libre},
que indica que el compilador no debe forzar la adopci�n de ninguna
convenci�n espec�fica.

En lo siguiente, se emplean los siguientes sin�nimos del
\toriginal{token} identificador, para hacer m�s comprensibles las
gram�ticas:
\noterminal{TypeClassID}, \noterminal{TypeConstrID},
\noterminal{DataConstrID},
\noterminal{TypeVarID}, \noterminal{FunctionID} y
\noterminal{VariableID}. Solamente el primero no figura en la
especificaci�n del lenguaje \Curry{}, y ha sido a�adido expl�citamente
para este proyecto. En todos los casos, al no considerarse las
convenciones, la expresi�n regular que define este \toriginal{token}
es:

\begin{verbatim}
[a-zA-Z][a-zA-Z0-9_']*
\end{verbatim}

Obviamente, si el compilador tuviera en cuenta los otros tres modos a
los que se alud�a anteriormente, ya no se tratar�a de sin�nimos, ya
que la expresi�n regular ser�a distinta para cada uno de ellos.

El \toriginal{token} \noterminal{ModuleID} es levemente distinto, pues
puede contener puntos en su interior (no en sus extremos).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Operadores}

El \toriginal{token} \noterminal{InfixOpID} denota un operador, y
est� compuesto por una cadena formada con caracteres del
conjunto\footnote{En este caso, se prefiere esta especificaci�n menos
formal debido a que una expresi�n regular conteniendo tantos
caracteres de s�mbolos resulta dif�cil de interpretar}:

\begin{verbatim}
~ ! @ # $ % ^ & * + - = < > ? . / | \ :
\end{verbatim}

En realidad, un identificador tambi�n puede ser convertido, a nivel
l�xico, en un operador, rode�ndolo por comillas simples inversas, como
en \verb|`mod`| o en \verb|`div`|.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Palabras reservadas}

Las siguientes cadenas son palabras reservadas, y constituyen
\toriginal{tokens} por s� mismas. Quedan exclu�das, por tanto, de la
consideraci�n de identificadores v�lidos:

\begin{multicols}{4}
\begin{verbatim}
as
case
ccall
choice
class
data
deriving
do
else
eval
external
free
hiding
if
import
in
infix
infixl
infixr
instance
interface
let
module
newtype
of
primitive
qualified
rigid
then
type
where
\end{verbatim}
\end{multicols}

Las palabras reservadas \verb|class|, \verb|deriving| e \verb|instance|
no figuran en la especificaci�n del lenguaje \Curry{} y constituyen
una novedad introducida para este proyecto.

La palabras reservadas \verb|ccall|, \verb|interface|, \verb|newtype| y
\verb|primitive| tampoco figuran en la
especificaci�n actual de \Curry{}, pero han sido incorporadas como
extensiones por el MCC.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Operadores reservados}

De una manera similar, existen ciertos operadores que se consideran
reservados, y que constituyen por s� mismos un \toriginal{token}
particular. Son los siguientes:

\begin{multicols}{4}
\begin{verbatim}
@
:
::
..
~
==
\
|
->
<-
=>
\end{verbatim}
\end{multicols}

De todos ellos, s�lo \verb|=>| ha sido introducido en Zinc para poder
representar los contextos.
