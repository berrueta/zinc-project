%
% unitarias.tex
%

\section{Pruebas unitarias}

Las pruebas unitarias corresponden al nivel m�s bajo de las
comprobaciones, donde los elementos que se ejercitan son m�s
simples. En este caso, se trata de funciones aisladas, o peque�os
conjuntos de funciones que realizan una tarea bien definida. Dado que
las funciones a comprobar est�n escritas en lenguaje Haskell, las
pruebas tambi�n est�n escritas en Haskell para poder enlazarse
correctamente.

Las pruebas unitarias no son una novedad en la ingenier�a del
\toriginal{software}, pero han cobrado nuevo vigor con el auge de las
llamadas metodolog�as �giles o ligeras, de las cuales suena con fuerza
especial la Programaci�n Extrema. De su mano ha aparecido todo un
abanico de herramientas dirigidas a facilitar la escritura y
utilizaci�n de pruebas unitarias. JUnit\cite{HightowerLesiecki01Java}
pasa por ser la m�s conocida de todas ellas, y a su semejanza se han
creado otras para un gran n�mero de lenguajes, ya que JUnit es
espec�fica para Java. La que est� implementada en Haskell y para
Haskell se llama HUnit\cite{HUnitManual}.

HUnit es una sencilla biblioteca con dos prop�sitos b�sicos: ofrecer
una estructura que permita la especificaci�n de casos de prueba, y
proporcionar un controlador capaz de llevar a cabo las pruebas de
forma autom�tica.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Morfolog�a}

La especificaci�n de un caso de prueba se realiza mediante una
estructura de datos de Haskell. En ella se indican los siguientes
elementos:

\begin{itemize}

\item El tipo de verificador. Casi siempre se tratar� de
\lstinline{assertEqual}, que denota un caso de prueba en el que se
espera que el resultado de la evaluaci�n de una expresi�n sea igual a
un valor se�al (resultado esperado).

\item Un identificador para el caso de prueba, para poder
proporcionar, en caso necesario, una referencia al caso de error.

\item El valor esperado. Se trata de una expresi�n en Haskell.

\item La expresi�n a evaluar, cuyo resultado es el objeto de la
prueba.

\end{itemize}

En el siguiente ejemplo, correspondiente a un caso de prueba de la
funci�n \lstinline{unifyKinds}, se aprecian los cuatro componentes:

\begin{lstlisting}
TestCase (assertEqual
           "unifyKinds1"
           (Just [])
           (fmap substToList (unifyKinds Star Star))
         )
\end{lstlisting}

Los casos de prueba se pueden agrupar en listas, pero tambi�n en
estructuras jer�rquicas, formando un �rbol. De esta forma pueden
organizarse en varios niveles. Un nivel inferior agrupa todos los
casos de prueba de una funci�n concreta. Otro nivel superior agrupa
todos los casos de prueba de las funciones de un m�dulo. A cada nodo
de la estructura se le asigna un identificador, lo que permite
etiquetar de forma c�moda cada caso de prueba.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Ejecuci�n}

La segunda parte de HUnit proporciona una funci�n que toma una
especificaci�n de una bater�a de pruebas y la pone en pr�ctica. Se
trata de \lstinline{runTestTT}. Se trata de un controlador no gr�fico,
contraste con el que se puede encontrar en JUnit, que tiene una
representaci�n gr�fica. Su ejecuci�n produce un informe similar al
siguiente:

\begin{verbatim}
### Failure in: 19:KindUnification:0:unifyKinds:0
unifyKinds1
expected: Just []
 but got: Just [(a,Star)]
Cases: 563  Tried: 563  Errors: 0  Failures: 1
\end{verbatim}

En �l se aprecia que un caso de prueba se ha comportado de forma
an�mala. HUnit no s�lo informa del nombre del caso de prueba, sino de
su situaci�n en la estructura jer�rquica y la diferencia entre el
resultado esperado y el realmente obtenido. La �ltima l�nea informa
del resultado global de la bater�a.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Planificaci�n y escritura}

Las pruebas unitarias resultan de mayor utilidad en la primera fase
del desarrollo, cuando el foco de atenci�n est� puesto en modificar el
dise�o y el c�digo heredados. El motivo es que, al ser pruebas de
grano muy fino, permiten comprobar con precisi�n si el comportamiento
externo de las funciones se ve alterado seg�n se ejecutan los
redise�os y las recodificaciones.

En cambio, cuando se abordan las fases posteriores, en las que se
incorpora nueva funcionalidad, estas pruebas no
resultan tan convenientes, pues en estas situaciones es m�s importante
el comportamiento coordinado de un gran n�mero de funciones. Adem�s,
resulta m�s dificultoso escribir las pruebas antes que el c�digo,
porque hay que emplear prototipos o reemplazos.

Las pruebas unitarias, dada su proximidad l�gica al c�digo fuente,
invitan a mantener esa cercan�a en su distribuci�n f�sica. Por esa
raz�n, se encuentran en el mismo m�dulo del repositorio, en un
subdirectorio del �rbol del c�digo fuente.

El compilador \Zinc{} cuenta con m�s de 560 casos de prueba de tipo
unitario.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Limitaciones y dificultades}

La limitaci�n m�s importante encontrada al escribir las pruebas
unitarias consiste en la imposibilidad de comprobar las situaciones de
error. El origen del problema radica en que Haskell no dispone de un
mecanismo com�n
para tratar las condiciones anormales, es decir, un equivalente a las
excepciones (tal y como existen en lenguajes como Java) para las
funciones \emph{puras}. Esto hace que los programadores recurran al
uso de la primitiva \lstinline{error}, que es un mecanismo mucho
m�s rudimentario y similar a la funci�n \texttt{exit()} de C/C++ o
Java. Las dificultades se manifiestan cuando se pretende escribir un
caso de prueba para ejercitar la situaci�n anormal. Al evaluarse la
primitiva \lstinline{error}, se interrumpe bruscamente la computaci�n,
y por tanto, la prueba nunca llega a terminar, la bater�a de pruebas
se aborta y apenas puede extraerse informaci�n �til.

Aparte de resignarse a no poder comprobar mediante pruebas unitarias
este tipo de situaciones, s�lo quedan dos alternativas. Primeramente,
podr�a pensarse en realizar una captura de las evaluaciones de la
funci�n \lstinline{error}, para reconducirlas de una forma que no
aborte la computaci�n, para que la bater�a de pruebas pueda
continuar. Desgraciadamente, esto no se contempla en la especificaci�n
del lenguaje Haskell, por lo que tan s�lo existen aproximaciones
heterog�neas. Por otro lado, el inconveniente se puede circunvalar
program�ticamente, evitando las apariciones de la funci�n
\lstinline{error}. Esto requiere utilizar valores \emph{se�al} para
informar de situaciones de error (el tipo \lstinline{Maybe} de Haskell
resulta apropiado para hacerlo). Por desgracia, instrumentar de esta
forma los programas es costoso y ofusca el c�digo, provocando que su
sintaxis sea artificialmente compleja. Ese motivo hace de esta segunda
alternativa poco conveniente, salvo cuando determinadas circunstancias
justifiquen los costes.

Otra dificultad viene determinada por la necesidad, en muchas
ocasiones, de construir complejas estructuras de datos para alimentar
a las funciones que se ejercitan, o para verificar la correcci�n de
los resultados. Por ejemplo, para comparar el resultado de una funci�n
que realiza el an�lisis sint�ctico, es necesario construir el �rbol
sint�ctico que se considera correcto. Debido a ello, las bater�as de
pruebas ven c�mo su c�digo fuente crece para describir estas
estructuras de datos, con la incomodidad que supone para el
programador. Resulta fundamental, por tanto, la reutilizaci�n de las
estructuras de datos, y as� se ha realizado muy habitualmente en
este proyecto. En otras ocasiones, se hace uso de ciertas t�cnicas
para soslayar esta dificultad, como por ejemplo, encadenar distintas
funciones de transformaci�n de estructuras de datos, probar de forma
pareada las funciones con comportamientos rec�procos, aplicar
funciones para simplificar la construcci�n de estructuras de datos de
gran tama�o o utilizar funciones que simplifican las estructuras de
datos.

La presencia de estas dificultades en las pruebas unitarias hace
especialmente importante la presencia de otro tipo de pruebas para
complementarlas.